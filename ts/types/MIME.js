"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isLongMessage = exports.isAudio = exports.isVideo = exports.isImage = exports.isJPEG = exports.isGif = exports.isHeic = exports.LONG_MESSAGE = exports.VIDEO_QUICKTIME = exports.VIDEO_MP4 = exports.IMAGE_BMP = exports.IMAGE_ICO = exports.IMAGE_WEBP = exports.IMAGE_PNG = exports.IMAGE_JPEG = exports.IMAGE_GIF = exports.AUDIO_MP3 = exports.AUDIO_AAC = exports.APPLICATION_JSON = exports.APPLICATION_OCTET_STREAM = exports.MIMETypeToString = exports.stringToMIMEType = void 0;
const stringToMIMEType = (value) => {
    return value;
};
exports.stringToMIMEType = stringToMIMEType;
const MIMETypeToString = (value) => {
    return value;
};
exports.MIMETypeToString = MIMETypeToString;
exports.APPLICATION_OCTET_STREAM = (0, exports.stringToMIMEType)('application/octet-stream');
exports.APPLICATION_JSON = (0, exports.stringToMIMEType)('application/json');
exports.AUDIO_AAC = (0, exports.stringToMIMEType)('audio/aac');
exports.AUDIO_MP3 = (0, exports.stringToMIMEType)('audio/mp3');
exports.IMAGE_GIF = (0, exports.stringToMIMEType)('image/gif');
exports.IMAGE_JPEG = (0, exports.stringToMIMEType)('image/jpeg');
exports.IMAGE_PNG = (0, exports.stringToMIMEType)('image/png');
exports.IMAGE_WEBP = (0, exports.stringToMIMEType)('image/webp');
exports.IMAGE_ICO = (0, exports.stringToMIMEType)('image/x-icon');
exports.IMAGE_BMP = (0, exports.stringToMIMEType)('image/bmp');
exports.VIDEO_MP4 = (0, exports.stringToMIMEType)('video/mp4');
exports.VIDEO_QUICKTIME = (0, exports.stringToMIMEType)('video/quicktime');
exports.LONG_MESSAGE = (0, exports.stringToMIMEType)('text/x-signal-plain');
const isHeic = (value) => value === 'image/heic' || value === 'image/heif';
exports.isHeic = isHeic;
const isGif = (value) => value === 'image/gif';
exports.isGif = isGif;
const isJPEG = (value) => value === 'image/jpeg';
exports.isJPEG = isJPEG;
const isImage = (value) => Boolean(value) && value.startsWith('image/');
exports.isImage = isImage;
const isVideo = (value) => Boolean(value) && value.startsWith('video/');
exports.isVideo = isVideo;
// As of 2020-04-16 aif files do not play in Electron nor Chrome. We should only
// recognize them as file attachments.
const isAudio = (value) => Boolean(value) && value.startsWith('audio/') && !value.endsWith('aiff');
exports.isAudio = isAudio;
const isLongMessage = (value) => value === exports.LONG_MESSAGE;
exports.isLongMessage = isLongMessage;
