"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.UUID = exports.isValidUuid = exports.UUIDKind = void 0;
const uuid_1 = require("uuid");
const assert_1 = require("../util/assert");
var UUIDKind;
(function (UUIDKind) {
    UUIDKind["ACI"] = "ACI";
    UUIDKind["PNI"] = "PNI";
    UUIDKind["Unknown"] = "Unknown";
})(UUIDKind = exports.UUIDKind || (exports.UUIDKind = {}));
const isValidUuid = (value) => typeof value === 'string' &&
    /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(value);
exports.isValidUuid = isValidUuid;
class UUID {
    constructor(value) {
        this.value = value;
        (0, assert_1.strictAssert)((0, exports.isValidUuid)(value), `Invalid UUID: ${value}`);
    }
    toString() {
        return this.value;
    }
    isEqual(other) {
        return this.value === other.value;
    }
    static parse(value) {
        return new UUID(value);
    }
    static lookup(identifier) {
        const conversation = window.ConversationController.get(identifier);
        const uuid = conversation === null || conversation === void 0 ? void 0 : conversation.get('uuid');
        if (uuid === undefined) {
            return undefined;
        }
        return new UUID(uuid);
    }
    static checkedLookup(identifier) {
        const uuid = UUID.lookup(identifier);
        (0, assert_1.strictAssert)(uuid !== undefined, `Conversation ${identifier} not found or has no uuid`);
        return uuid;
    }
    static generate() {
        return new UUID((0, uuid_1.v4)());
    }
    static cast(value) {
        return new UUID(value.toLowerCase()).toString();
    }
    // For testing
    static fromPrefix(value) {
        let padded = value;
        while (padded.length < 8) {
            padded += '0';
        }
        return new UUID(`${padded}-0000-4000-8000-${'0'.repeat(12)}`);
    }
}
exports.UUID = UUID;
