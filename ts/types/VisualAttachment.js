"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.revokeObjectUrl = exports.makeObjectUrl = exports.makeVideoThumbnail = exports.makeVideoScreenshot = exports.makeImageThumbnail = exports.getImageDimensions = exports.blobToArrayBuffer = void 0;
const blueimp_load_image_1 = __importDefault(require("blueimp-load-image"));
const blob_util_1 = require("blob-util");
Object.defineProperty(exports, "blobToArrayBuffer", { enumerable: true, get: function () { return blob_util_1.blobToArrayBuffer; } });
const errors_1 = require("./errors");
const MIME_1 = require("./MIME");
const arrayBufferToObjectURL_1 = require("../util/arrayBufferToObjectURL");
const assert_1 = require("../util/assert");
const canvasToBlob_1 = require("../util/canvasToBlob");
function getImageDimensions({ objectUrl, logger, }) {
    return new Promise((resolve, reject) => {
        const image = document.createElement('img');
        image.addEventListener('load', () => {
            resolve({
                height: image.naturalHeight,
                width: image.naturalWidth,
            });
        });
        image.addEventListener('error', error => {
            logger.error('getImageDimensions error', (0, errors_1.toLogFormat)(error));
            reject(error);
        });
        image.src = objectUrl;
    });
}
exports.getImageDimensions = getImageDimensions;
function makeImageThumbnail({ size, objectUrl, contentType = MIME_1.IMAGE_PNG, logger, }) {
    return new Promise((resolve, reject) => {
        const image = document.createElement('img');
        image.addEventListener('load', async () => {
            // using components/blueimp-load-image
            // first, make the correct size
            let canvas = blueimp_load_image_1.default.scale(image, {
                canvas: true,
                cover: true,
                maxWidth: size,
                maxHeight: size,
                minWidth: size,
                minHeight: size,
            });
            // then crop
            canvas = blueimp_load_image_1.default.scale(canvas, {
                canvas: true,
                crop: true,
                maxWidth: size,
                maxHeight: size,
                minWidth: size,
                minHeight: size,
            });
            (0, assert_1.strictAssert)(canvas instanceof HTMLCanvasElement, 'loadImage must produce canvas');
            try {
                const blob = await (0, canvasToBlob_1.canvasToBlob)(canvas, contentType);
                resolve(blob);
            }
            catch (err) {
                reject(err);
            }
        });
        image.addEventListener('error', error => {
            logger.error('makeImageThumbnail error', (0, errors_1.toLogFormat)(error));
            reject(error);
        });
        image.src = objectUrl;
    });
}
exports.makeImageThumbnail = makeImageThumbnail;
function makeVideoScreenshot({ objectUrl, contentType = MIME_1.IMAGE_PNG, logger, }) {
    return new Promise((resolve, reject) => {
        const video = document.createElement('video');
        function seek() {
            video.currentTime = 1.0;
        }
        async function capture() {
            const canvas = document.createElement('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            const context = canvas.getContext('2d');
            (0, assert_1.strictAssert)(context, 'Failed to get canvas context');
            context.drawImage(video, 0, 0, canvas.width, canvas.height);
            video.addEventListener('loadeddata', seek);
            video.removeEventListener('seeked', capture);
            try {
                const image = (0, canvasToBlob_1.canvasToBlob)(canvas, contentType);
                resolve(image);
            }
            catch (err) {
                reject(err);
            }
        }
        video.addEventListener('loadeddata', seek);
        video.addEventListener('seeked', capture);
        video.addEventListener('error', error => {
            logger.error('makeVideoScreenshot error', (0, errors_1.toLogFormat)(error));
            reject(error);
        });
        video.src = objectUrl;
    });
}
exports.makeVideoScreenshot = makeVideoScreenshot;
async function makeVideoThumbnail({ size, videoObjectUrl, logger, contentType, }) {
    let screenshotObjectUrl;
    try {
        const blob = await makeVideoScreenshot({
            objectUrl: videoObjectUrl,
            contentType,
            logger,
        });
        const data = await (0, blob_util_1.blobToArrayBuffer)(blob);
        screenshotObjectUrl = (0, arrayBufferToObjectURL_1.arrayBufferToObjectURL)({
            data,
            type: contentType,
        });
        // We need to wait for this, otherwise the finally below will run first
        const resultBlob = await makeImageThumbnail({
            size,
            objectUrl: screenshotObjectUrl,
            contentType,
            logger,
        });
        return resultBlob;
    }
    finally {
        if (screenshotObjectUrl !== undefined) {
            revokeObjectUrl(screenshotObjectUrl);
        }
    }
}
exports.makeVideoThumbnail = makeVideoThumbnail;
function makeObjectUrl(data, contentType) {
    const blob = new Blob([data], {
        type: contentType,
    });
    return URL.createObjectURL(blob);
}
exports.makeObjectUrl = makeObjectUrl;
function revokeObjectUrl(objectUrl) {
    URL.revokeObjectURL(objectUrl);
}
exports.revokeObjectUrl = revokeObjectUrl;
