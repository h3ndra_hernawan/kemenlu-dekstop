"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports._validate = exports.parseAndWriteAvatar = exports.getName = exports.embeddedContactSelector = exports.AddressType = exports.ContactFormType = void 0;
const lodash_1 = require("lodash");
const protobuf_1 = require("../protobuf");
const isNotNil_1 = require("../util/isNotNil");
const PhoneNumber_1 = require("./PhoneNumber");
const errors_1 = require("./errors");
var ContactFormType;
(function (ContactFormType) {
    ContactFormType[ContactFormType["HOME"] = 1] = "HOME";
    ContactFormType[ContactFormType["MOBILE"] = 2] = "MOBILE";
    ContactFormType[ContactFormType["WORK"] = 3] = "WORK";
    ContactFormType[ContactFormType["CUSTOM"] = 4] = "CUSTOM";
})(ContactFormType = exports.ContactFormType || (exports.ContactFormType = {}));
var AddressType;
(function (AddressType) {
    AddressType[AddressType["HOME"] = 1] = "HOME";
    AddressType[AddressType["WORK"] = 2] = "WORK";
    AddressType[AddressType["CUSTOM"] = 3] = "CUSTOM";
})(AddressType = exports.AddressType || (exports.AddressType = {}));
const DEFAULT_PHONE_TYPE = protobuf_1.SignalService.DataMessage.Contact.Phone.Type.HOME;
const DEFAULT_EMAIL_TYPE = protobuf_1.SignalService.DataMessage.Contact.Email.Type.HOME;
const DEFAULT_ADDRESS_TYPE = protobuf_1.SignalService.DataMessage.Contact.PostalAddress.Type.HOME;
function embeddedContactSelector(contact, options) {
    const { getAbsoluteAttachmentPath, firstNumber, isNumberOnSignal, regionCode, } = options;
    let { avatar } = contact;
    if (avatar && avatar.avatar) {
        if (avatar.avatar.error) {
            avatar = undefined;
        }
        else {
            avatar = Object.assign(Object.assign({}, avatar), { avatar: Object.assign(Object.assign({}, avatar.avatar), { path: avatar.avatar.path
                        ? getAbsoluteAttachmentPath(avatar.avatar.path)
                        : undefined }) });
        }
    }
    return Object.assign(Object.assign({}, contact), { firstNumber,
        isNumberOnSignal,
        avatar, number: contact.number &&
            contact.number.map(item => (Object.assign(Object.assign({}, item), { value: (0, PhoneNumber_1.format)(item.value, {
                    ourRegionCode: regionCode,
                }) }))) });
}
exports.embeddedContactSelector = embeddedContactSelector;
function getName(contact) {
    const { name, organization } = contact;
    const displayName = (name && name.displayName) || undefined;
    const givenName = (name && name.givenName) || undefined;
    const familyName = (name && name.familyName) || undefined;
    const backupName = (givenName && familyName && `${givenName} ${familyName}`) || undefined;
    return displayName || organization || backupName || givenName || familyName;
}
exports.getName = getName;
function parseAndWriteAvatar(upgradeAttachment) {
    return async (contact, context) => {
        const { message, regionCode, logger } = context;
        const { avatar } = contact;
        const contactWithUpdatedAvatar = avatar && avatar.avatar
            ? Object.assign(Object.assign({}, contact), { avatar: Object.assign(Object.assign({}, avatar), { avatar: await upgradeAttachment(avatar.avatar, context) }) }) : (0, lodash_1.omit)(contact, ['avatar']);
        // eliminates empty numbers, emails, and addresses; adds type if not provided
        const parsedContact = parseContact(contactWithUpdatedAvatar, {
            regionCode,
        });
        const error = _validate(parsedContact, {
            messageId: idForLogging(message),
        });
        if (error) {
            logger.error('parseAndWriteAvatar: contact was malformed.', (0, errors_1.toLogFormat)(error));
        }
        return parsedContact;
    };
}
exports.parseAndWriteAvatar = parseAndWriteAvatar;
function parseContact(contact, { regionCode }) {
    const boundParsePhone = (phoneNumber) => parsePhoneItem(phoneNumber, { regionCode });
    const skipEmpty = (arr) => {
        const filtered = arr.filter(isNotNil_1.isNotNil);
        return filtered.length ? filtered : undefined;
    };
    const number = skipEmpty((contact.number || []).map(boundParsePhone));
    const email = skipEmpty((contact.email || []).map(parseEmailItem));
    const address = skipEmpty((contact.address || []).map(parseAddress));
    let result = Object.assign(Object.assign({}, (0, lodash_1.omit)(contact, ['avatar', 'number', 'email', 'address'])), parseAvatar(contact.avatar));
    if (number) {
        result = Object.assign(Object.assign({}, result), { number });
    }
    if (email) {
        result = Object.assign(Object.assign({}, result), { email });
    }
    if (address) {
        result = Object.assign(Object.assign({}, result), { address });
    }
    return result;
}
function idForLogging(message) {
    return `${message.source}.${message.sourceDevice} ${message.sent_at}`;
}
// Exported for testing
function _validate(contact, { messageId }) {
    const { name, number, email, address, organization } = contact;
    if ((!name || !name.displayName) && !organization) {
        return new Error(`Message ${messageId}: Contact had neither 'displayName' nor 'organization'`);
    }
    if ((!number || !number.length) &&
        (!email || !email.length) &&
        (!address || !address.length)) {
        return new Error(`Message ${messageId}: Contact had no included numbers, email or addresses`);
    }
    return undefined;
}
exports._validate = _validate;
function parsePhoneItem(item, { regionCode }) {
    if (!item.value) {
        return undefined;
    }
    return Object.assign(Object.assign({}, item), { type: item.type || DEFAULT_PHONE_TYPE, value: (0, PhoneNumber_1.parse)(item.value, { regionCode }) });
}
function parseEmailItem(item) {
    if (!item.value) {
        return undefined;
    }
    return Object.assign(Object.assign({}, item), { type: item.type || DEFAULT_EMAIL_TYPE });
}
function parseAddress(address) {
    if (!address) {
        return undefined;
    }
    if (!address.street &&
        !address.pobox &&
        !address.neighborhood &&
        !address.city &&
        !address.region &&
        !address.postcode &&
        !address.country) {
        return undefined;
    }
    return Object.assign(Object.assign({}, address), { type: address.type || DEFAULT_ADDRESS_TYPE });
}
function parseAvatar(avatar) {
    if (!avatar) {
        return undefined;
    }
    return {
        avatar: Object.assign(Object.assign({}, avatar), { isProfile: avatar.isProfile || false }),
    };
}
