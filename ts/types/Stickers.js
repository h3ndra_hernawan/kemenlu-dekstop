"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deletePack = exports.deletePackReference = exports.maybeDeletePack = exports.copyStickerToAttachments = exports.getSticker = exports.getStickerPackStatus = exports.getStickerPack = exports.downloadStickerPack = exports.downloadEphemeralPack = exports.removeEphemeralPack = exports.savePackMetadata = exports.redactPackId = exports.isPackIdValid = exports.getInitialState = exports.downloadQueuedPacks = exports.getInstalledStickerPacks = exports.getDataFromLink = exports.load = exports.BLESSED_PACKS = void 0;
const lodash_1 = require("lodash");
const p_map_1 = __importDefault(require("p-map"));
const p_queue_1 = __importDefault(require("p-queue"));
const assert_1 = require("../util/assert");
const dropNull_1 = require("../util/dropNull");
const makeLookup_1 = require("../util/makeLookup");
const url_1 = require("../util/url");
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const MIME_1 = require("./MIME");
const sniffImageMimeType_1 = require("../util/sniffImageMimeType");
const Client_1 = __importDefault(require("../sql/Client"));
const protobuf_1 = require("../protobuf");
const log = __importStar(require("../logging/log"));
exports.BLESSED_PACKS = {
    '9acc9e8aba563d26a4994e69263e3b25': {
        key: 'Wm3/OUjCjvubeq+T7MN1xp/DFueAd+0mhnoU0QoPahI=',
        status: 'downloaded',
    },
    fb535407d2f6497ec074df8b9c51dd1d: {
        key: 'F+lxwTQDViJ4HS7iSeZHO3dFg3ULaMEbuCt1CcaLbf0=',
        status: 'downloaded',
    },
    e61fa0867031597467ccc036cc65d403: {
        key: 'E657GnQHMYKA6bOMEmHe044OcTi5+WSmzLtz5A9zeps=',
        status: 'downloaded',
    },
    cca32f5b905208b7d0f1e17f23fdc185: {
        key: 'i/jpX3pFver+DI9bAC7wGrlbjxtbqsQBnM1ra+Cxg3o=',
        status: 'downloaded',
    },
    ccc89a05dc077856b57351e90697976c: {
        key: 'RXMOYPCdVWYRUiN0RTemt9nqmc7qy3eh+9aAG5YH+88=',
        status: 'downloaded',
    },
};
const STICKER_PACK_DEFAULTS = {
    id: '',
    key: '',
    author: '',
    coverStickerId: 0,
    createdAt: 0,
    downloadAttempts: 0,
    status: 'ephemeral',
    stickerCount: 0,
    stickers: {},
    title: '',
};
const VALID_PACK_ID_REGEXP = /^[0-9a-f]{32}$/i;
let initialState;
let packsToDownload;
const downloadQueue = new p_queue_1.default({ concurrency: 1, timeout: 1000 * 60 * 2 });
async function load() {
    const [packs, recentStickers] = await Promise.all([
        getPacksForRedux(),
        getRecentStickersForRedux(),
    ]);
    const blessedPacks = Object.create(null);
    for (const key of Object.keys(exports.BLESSED_PACKS)) {
        blessedPacks[key] = true;
    }
    initialState = {
        packs,
        recentStickers,
        blessedPacks,
    };
    packsToDownload = capturePacksToDownload(packs);
}
exports.load = load;
function getDataFromLink(link) {
    const url = (0, url_1.maybeParseUrl)(link);
    if (!url) {
        return undefined;
    }
    const { hash } = url;
    if (!hash) {
        return undefined;
    }
    let params;
    try {
        params = new URLSearchParams(hash.slice(1));
    }
    catch (err) {
        return undefined;
    }
    const id = params.get('pack_id');
    if (!isPackIdValid(id)) {
        return undefined;
    }
    const key = params.get('pack_key');
    if (!key) {
        return undefined;
    }
    return { id, key };
}
exports.getDataFromLink = getDataFromLink;
function getInstalledStickerPacks() {
    const state = window.reduxStore.getState();
    const { stickers } = state;
    const { packs } = stickers;
    if (!packs) {
        return [];
    }
    const items = Object.values(packs);
    return items.filter(pack => pack.status === 'installed');
}
exports.getInstalledStickerPacks = getInstalledStickerPacks;
function downloadQueuedPacks() {
    (0, assert_1.strictAssert)(packsToDownload, 'Stickers not initialized');
    const ids = Object.keys(packsToDownload);
    for (const id of ids) {
        const { key, status } = packsToDownload[id];
        // The queuing is done inside this function, no need to await here
        downloadStickerPack(id, key, { finalStatus: status });
    }
    packsToDownload = {};
}
exports.downloadQueuedPacks = downloadQueuedPacks;
function capturePacksToDownload(existingPackLookup) {
    const toDownload = Object.create(null);
    // First, ensure that blessed packs are in good shape
    const blessedIds = Object.keys(exports.BLESSED_PACKS);
    blessedIds.forEach(id => {
        const existing = existingPackLookup[id];
        if (!existing ||
            (existing.status !== 'downloaded' && existing.status !== 'installed')) {
            toDownload[id] = Object.assign({ id }, exports.BLESSED_PACKS[id]);
        }
    });
    // Then, find error cases in packs we already know about
    const existingIds = Object.keys(existingPackLookup);
    existingIds.forEach(id => {
        if (toDownload[id]) {
            return;
        }
        const existing = existingPackLookup[id];
        // These packs should never end up in the database, but if they do we'll delete them
        if (existing.status === 'ephemeral') {
            deletePack(id);
            return;
        }
        // We don't automatically download these; not until a user action kicks it off
        if (existing.status === 'known') {
            return;
        }
        if (doesPackNeedDownload(existing)) {
            const status = existing.attemptedStatus === 'installed' ? 'installed' : undefined;
            toDownload[id] = {
                id,
                key: existing.key,
                status,
            };
        }
    });
    return toDownload;
}
function doesPackNeedDownload(pack) {
    if (!pack) {
        return true;
    }
    const { status, stickerCount } = pack;
    const stickersDownloaded = Object.keys(pack.stickers || {}).length;
    if ((status === 'installed' || status === 'downloaded') &&
        stickerCount > 0 &&
        stickersDownloaded >= stickerCount) {
        return false;
    }
    // If we don't understand a pack's status, we'll download it
    // If a pack has any other status, we'll download it
    // If a pack has zero stickers in it, we'll download it
    // If a pack doesn't have enough downloaded stickers, we'll download it
    return true;
}
async function getPacksForRedux() {
    const [packs, stickers] = await Promise.all([
        Client_1.default.getAllStickerPacks(),
        Client_1.default.getAllStickers(),
    ]);
    const stickersByPack = (0, lodash_1.groupBy)(stickers, sticker => sticker.packId);
    const fullSet = packs.map(pack => (Object.assign(Object.assign({}, pack), { stickers: (0, makeLookup_1.makeLookup)(stickersByPack[pack.id] || [], 'id') })));
    return (0, makeLookup_1.makeLookup)(fullSet, 'id');
}
async function getRecentStickersForRedux() {
    const recent = await Client_1.default.getRecentStickers();
    return recent.map(sticker => ({
        packId: sticker.packId,
        stickerId: sticker.id,
    }));
}
function getInitialState() {
    (0, assert_1.strictAssert)(initialState !== undefined, 'Stickers not initialized');
    return initialState;
}
exports.getInitialState = getInitialState;
function isPackIdValid(packId) {
    return typeof packId === 'string' && VALID_PACK_ID_REGEXP.test(packId);
}
exports.isPackIdValid = isPackIdValid;
function redactPackId(packId) {
    return `[REDACTED]${packId.slice(-3)}`;
}
exports.redactPackId = redactPackId;
function getReduxStickerActions() {
    const actions = window.reduxActions;
    (0, assert_1.strictAssert)(actions && actions.stickers, 'Redux not ready');
    return actions.stickers;
}
function decryptSticker(packKey, ciphertext) {
    const binaryKey = Bytes.fromBase64(packKey);
    const derivedKey = (0, Crypto_1.deriveStickerPackKey)(binaryKey);
    const plaintext = (0, Crypto_1.decryptAttachment)(ciphertext, derivedKey);
    return plaintext;
}
async function downloadSticker(packId, packKey, proto, { ephemeral } = {}) {
    const { id, emoji } = proto;
    (0, assert_1.strictAssert)(id !== undefined && id !== null, "Sticker id can't be null");
    const ciphertext = await window.textsecure.messaging.getSticker(packId, id);
    const plaintext = decryptSticker(packKey, ciphertext);
    const sticker = ephemeral
        ? await window.Signal.Migrations.processNewEphemeralSticker(plaintext)
        : await window.Signal.Migrations.processNewSticker(plaintext);
    return Object.assign(Object.assign({ id, emoji: (0, dropNull_1.dropNull)(emoji) }, sticker), { packId });
}
async function savePackMetadata(packId, packKey, { messageId } = {}) {
    const existing = getStickerPack(packId);
    if (existing) {
        return;
    }
    const { stickerPackAdded } = getReduxStickerActions();
    const pack = Object.assign(Object.assign({}, STICKER_PACK_DEFAULTS), { id: packId, key: packKey, status: 'known' });
    stickerPackAdded(pack);
    await Client_1.default.createOrUpdateStickerPack(pack);
    if (messageId) {
        await Client_1.default.addStickerPackReference(messageId, packId);
    }
}
exports.savePackMetadata = savePackMetadata;
async function removeEphemeralPack(packId) {
    const existing = getStickerPack(packId);
    (0, assert_1.strictAssert)(existing, `No existing sticker pack with id: ${packId}`);
    if (existing.status !== 'ephemeral' &&
        !(existing.status === 'error' && existing.attemptedStatus === 'ephemeral')) {
        return;
    }
    const { removeStickerPack } = getReduxStickerActions();
    removeStickerPack(packId);
    const stickers = (0, lodash_1.values)(existing.stickers);
    const paths = stickers.map(sticker => sticker.path);
    await (0, p_map_1.default)(paths, window.Signal.Migrations.deleteTempFile, {
        concurrency: 3,
    });
    // Remove it from database in case it made it there
    await Client_1.default.deleteStickerPack(packId);
}
exports.removeEphemeralPack = removeEphemeralPack;
async function downloadEphemeralPack(packId, packKey) {
    const { stickerAdded, stickerPackAdded, stickerPackUpdated } = getReduxStickerActions();
    const existingPack = getStickerPack(packId);
    if (existingPack &&
        (existingPack.status === 'downloaded' ||
            existingPack.status === 'installed' ||
            existingPack.status === 'pending')) {
        log.warn(`Ephemeral download for pack ${redactPackId(packId)} requested, we already know about it. Skipping.`);
        return;
    }
    try {
        // Synchronous placeholder to help with race conditions
        const placeholder = Object.assign(Object.assign({}, STICKER_PACK_DEFAULTS), { id: packId, key: packKey, status: 'ephemeral' });
        stickerPackAdded(placeholder);
        const ciphertext = await window.textsecure.messaging.getStickerPackManifest(packId);
        const plaintext = decryptSticker(packKey, ciphertext);
        const proto = protobuf_1.SignalService.StickerPack.decode(plaintext);
        const firstStickerProto = proto.stickers ? proto.stickers[0] : null;
        const stickerCount = proto.stickers.length;
        const coverProto = proto.cover || firstStickerProto;
        const coverStickerId = coverProto ? coverProto.id : null;
        if (!coverProto || !(0, lodash_1.isNumber)(coverStickerId)) {
            throw new Error(`Sticker pack ${redactPackId(packId)} is malformed - it has no cover, and no stickers`);
        }
        const nonCoverStickers = (0, lodash_1.reject)(proto.stickers, sticker => !(0, lodash_1.isNumber)(sticker.id) || sticker.id === coverStickerId);
        const coverIncludedInList = nonCoverStickers.length < stickerCount;
        const pack = Object.assign(Object.assign(Object.assign({}, STICKER_PACK_DEFAULTS), { id: packId, key: packKey, coverStickerId,
            stickerCount, status: 'ephemeral' }), (0, lodash_1.pick)(proto, ['title', 'author']));
        stickerPackAdded(pack);
        const downloadStickerJob = async (stickerProto) => {
            const stickerInfo = await downloadSticker(packId, packKey, stickerProto, {
                ephemeral: true,
            });
            const sticker = Object.assign(Object.assign({}, stickerInfo), { isCoverOnly: !coverIncludedInList && stickerInfo.id === coverStickerId });
            const statusCheck = getStickerPackStatus(packId);
            if (statusCheck !== 'ephemeral') {
                throw new Error(`Ephemeral download for pack ${redactPackId(packId)} interrupted by status change. Status is now ${statusCheck}.`);
            }
            stickerAdded(sticker);
        };
        // Download the cover first
        await downloadStickerJob(coverProto);
        // Then the rest
        await (0, p_map_1.default)(nonCoverStickers, downloadStickerJob, {
            concurrency: 3,
        });
    }
    catch (error) {
        // Because the user could install this pack while we are still downloading this
        //   ephemeral pack, we don't want to go change its status unless we're still in
        //   ephemeral mode.
        const statusCheck = getStickerPackStatus(packId);
        if (statusCheck === 'ephemeral') {
            stickerPackUpdated(packId, {
                attemptedStatus: 'ephemeral',
                status: 'error',
            });
        }
        log.error(`Ephemeral download error for sticker pack ${redactPackId(packId)}:`, error && error.stack ? error.stack : error);
    }
}
exports.downloadEphemeralPack = downloadEphemeralPack;
async function downloadStickerPack(packId, packKey, options = {}) {
    // This will ensure that only one download process is in progress at any given time
    return downloadQueue.add(async () => {
        try {
            await doDownloadStickerPack(packId, packKey, options);
        }
        catch (error) {
            log.error('doDownloadStickerPack threw an error:', error && error.stack ? error.stack : error);
        }
    });
}
exports.downloadStickerPack = downloadStickerPack;
async function doDownloadStickerPack(packId, packKey, { finalStatus = 'downloaded', messageId, fromSync = false, }) {
    const { stickerAdded, stickerPackAdded, stickerPackUpdated, installStickerPack, } = getReduxStickerActions();
    if (finalStatus !== 'downloaded' && finalStatus !== 'installed') {
        throw new Error(`doDownloadStickerPack: invalid finalStatus of ${finalStatus} requested.`);
    }
    const existing = getStickerPack(packId);
    if (!doesPackNeedDownload(existing)) {
        log.warn(`Download for pack ${redactPackId(packId)} requested, but it does not need re-download. Skipping.`);
        return;
    }
    // We don't count this as an attempt if we're offline
    const attemptIncrement = navigator.onLine ? 1 : 0;
    const downloadAttempts = (existing ? existing.downloadAttempts || 0 : 0) + attemptIncrement;
    if (downloadAttempts > 3) {
        log.warn(`Refusing to attempt another download for pack ${redactPackId(packId)}, attempt number ${downloadAttempts}`);
        if (existing && existing.status !== 'error') {
            await Client_1.default.updateStickerPackStatus(packId, 'error');
            stickerPackUpdated(packId, {
                status: 'error',
            });
        }
        return;
    }
    let coverProto;
    let coverStickerId;
    let coverIncludedInList = false;
    let nonCoverStickers = [];
    try {
        // Synchronous placeholder to help with race conditions
        const placeholder = Object.assign(Object.assign({}, STICKER_PACK_DEFAULTS), { id: packId, key: packKey, attemptedStatus: finalStatus, downloadAttempts, status: 'pending' });
        stickerPackAdded(placeholder);
        const ciphertext = await window.textsecure.messaging.getStickerPackManifest(packId);
        const plaintext = decryptSticker(packKey, ciphertext);
        const proto = protobuf_1.SignalService.StickerPack.decode(plaintext);
        const firstStickerProto = proto.stickers ? proto.stickers[0] : undefined;
        const stickerCount = proto.stickers.length;
        coverProto = proto.cover || firstStickerProto;
        coverStickerId = (0, dropNull_1.dropNull)(coverProto ? coverProto.id : undefined);
        if (!coverProto || !(0, lodash_1.isNumber)(coverStickerId)) {
            throw new Error(`Sticker pack ${redactPackId(packId)} is malformed - it has no cover, and no stickers`);
        }
        nonCoverStickers = (0, lodash_1.reject)(proto.stickers, sticker => !(0, lodash_1.isNumber)(sticker.id) || sticker.id === coverStickerId);
        coverIncludedInList = nonCoverStickers.length < stickerCount;
        // status can be:
        //   - 'known'
        //   - 'ephemeral' (should not hit database)
        //   - 'pending'
        //   - 'downloaded'
        //   - 'error'
        //   - 'installed'
        const pack = Object.assign({ id: packId, key: packKey, attemptedStatus: finalStatus, coverStickerId,
            downloadAttempts,
            stickerCount, status: 'pending', createdAt: Date.now(), stickers: {} }, (0, lodash_1.pick)(proto, ['title', 'author']));
        await Client_1.default.createOrUpdateStickerPack(pack);
        stickerPackAdded(pack);
        if (messageId) {
            await Client_1.default.addStickerPackReference(messageId, packId);
        }
    }
    catch (error) {
        log.error(`Error downloading manifest for sticker pack ${redactPackId(packId)}:`, error && error.stack ? error.stack : error);
        const pack = Object.assign(Object.assign({}, STICKER_PACK_DEFAULTS), { id: packId, key: packKey, attemptedStatus: finalStatus, downloadAttempts, status: 'error' });
        await Client_1.default.createOrUpdateStickerPack(pack);
        stickerPackAdded(pack);
        return;
    }
    // We have a separate try/catch here because we're starting to download stickers here
    //   and we want to preserve more of the pack on an error.
    try {
        const downloadStickerJob = async (stickerProto) => {
            const stickerInfo = await downloadSticker(packId, packKey, stickerProto);
            const sticker = Object.assign(Object.assign({}, stickerInfo), { isCoverOnly: !coverIncludedInList && stickerInfo.id === coverStickerId });
            await Client_1.default.createOrUpdateSticker(sticker);
            stickerAdded(sticker);
        };
        // Download the cover first
        await downloadStickerJob(coverProto);
        // Then the rest
        await (0, p_map_1.default)(nonCoverStickers, downloadStickerJob, {
            concurrency: 3,
        });
        // Allow for the user marking this pack as installed in the middle of our download;
        //   don't overwrite that status.
        const existingStatus = getStickerPackStatus(packId);
        if (existingStatus === 'installed') {
            return;
        }
        if (finalStatus === 'installed') {
            await installStickerPack(packId, packKey, { fromSync });
        }
        else {
            // Mark the pack as complete
            await Client_1.default.updateStickerPackStatus(packId, finalStatus);
            stickerPackUpdated(packId, {
                status: finalStatus,
            });
        }
    }
    catch (error) {
        log.error(`Error downloading stickers for sticker pack ${redactPackId(packId)}:`, error && error.stack ? error.stack : error);
        const errorStatus = 'error';
        await Client_1.default.updateStickerPackStatus(packId, errorStatus);
        if (stickerPackUpdated) {
            stickerPackUpdated(packId, {
                attemptedStatus: finalStatus,
                status: errorStatus,
            });
        }
    }
}
function getStickerPack(packId) {
    const state = window.reduxStore.getState();
    const { stickers } = state;
    const { packs } = stickers;
    if (!packs) {
        return undefined;
    }
    return packs[packId];
}
exports.getStickerPack = getStickerPack;
function getStickerPackStatus(packId) {
    const pack = getStickerPack(packId);
    if (!pack) {
        return undefined;
    }
    return pack.status;
}
exports.getStickerPackStatus = getStickerPackStatus;
function getSticker(packId, stickerId) {
    const pack = getStickerPack(packId);
    if (!pack || !pack.stickers) {
        return undefined;
    }
    return pack.stickers[stickerId];
}
exports.getSticker = getSticker;
async function copyStickerToAttachments(packId, stickerId) {
    const sticker = getSticker(packId, stickerId);
    if (!sticker) {
        throw new Error(`copyStickerToAttachments: Failed to find sticker ${packId}/${stickerId}`);
    }
    const { path: stickerPath } = sticker;
    const absolutePath = window.Signal.Migrations.getAbsoluteStickerPath(stickerPath);
    const { path, size } = await window.Signal.Migrations.copyIntoAttachmentsDirectory(absolutePath);
    const { data } = await window.Signal.Migrations.loadAttachmentData({
        path,
    });
    let contentType;
    const sniffedMimeType = (0, sniffImageMimeType_1.sniffImageMimeType)(data);
    if (sniffedMimeType) {
        contentType = sniffedMimeType;
    }
    else {
        log.warn('copyStickerToAttachments: Unable to sniff sticker MIME type; falling back to WebP');
        contentType = MIME_1.IMAGE_WEBP;
    }
    return Object.assign(Object.assign({}, sticker), { contentType,
        path,
        size });
}
exports.copyStickerToAttachments = copyStickerToAttachments;
// In the case where a sticker pack is uninstalled, we want to delete it if there are no
//   more references left. We'll delete a nonexistent reference, then check if there are
//   any references left, just like usual.
async function maybeDeletePack(packId) {
    // This hardcoded string is fine because message ids are GUIDs
    await deletePackReference('NOT-USED', packId);
}
exports.maybeDeletePack = maybeDeletePack;
// We don't generally delete packs outright; we just remove references to them, and if
//   the last reference is deleted, we finally then remove the pack itself from database
//   and from disk.
async function deletePackReference(messageId, packId) {
    const isBlessed = Boolean(exports.BLESSED_PACKS[packId]);
    if (isBlessed) {
        return;
    }
    // This call uses locking to prevent race conditions with other reference removals,
    //   or an incoming message creating a new message->pack reference
    const paths = await Client_1.default.deleteStickerPackReference(messageId, packId);
    // If we don't get a list of paths back, then the sticker pack was not deleted
    if (!paths) {
        return;
    }
    const { removeStickerPack } = getReduxStickerActions();
    removeStickerPack(packId);
    await (0, p_map_1.default)(paths, window.Signal.Migrations.deleteSticker, {
        concurrency: 3,
    });
}
exports.deletePackReference = deletePackReference;
// The override; doesn't honor our ref-counting scheme - just deletes it all.
async function deletePack(packId) {
    const isBlessed = Boolean(exports.BLESSED_PACKS[packId]);
    if (isBlessed) {
        return;
    }
    // This call uses locking to prevent race conditions with other reference removals,
    //   or an incoming message creating a new message->pack reference
    const paths = await Client_1.default.deleteStickerPack(packId);
    const { removeStickerPack } = getReduxStickerActions();
    removeStickerPack(packId);
    await (0, p_map_1.default)(paths, window.Signal.Migrations.deleteSticker, {
        concurrency: 3,
    });
}
exports.deletePack = deletePack;
