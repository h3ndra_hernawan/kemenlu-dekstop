"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.AttachmentToastType = void 0;
var AttachmentToastType;
(function (AttachmentToastType) {
    AttachmentToastType[AttachmentToastType["ToastCannotMixImageAndNonImageAttachments"] = 0] = "ToastCannotMixImageAndNonImageAttachments";
    AttachmentToastType[AttachmentToastType["ToastDangerousFileType"] = 1] = "ToastDangerousFileType";
    AttachmentToastType[AttachmentToastType["ToastFileSize"] = 2] = "ToastFileSize";
    AttachmentToastType[AttachmentToastType["ToastMaxAttachments"] = 3] = "ToastMaxAttachments";
    AttachmentToastType[AttachmentToastType["ToastOneNonImageAtATime"] = 4] = "ToastOneNonImageAtATime";
    AttachmentToastType[AttachmentToastType["ToastUnableToLoadAttachment"] = 5] = "ToastUnableToLoadAttachment";
})(AttachmentToastType = exports.AttachmentToastType || (exports.AttachmentToastType = {}));
