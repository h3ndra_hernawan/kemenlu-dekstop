"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initializeAttachmentMetadata = void 0;
const Attachment = __importStar(require("../Attachment"));
const IndexedDB = __importStar(require("../IndexedDB"));
const hasAttachment = (predicate) => (message) => IndexedDB.toIndexablePresence(message.attachments.some(predicate));
const hasFileAttachment = hasAttachment(Attachment.isFile);
const hasVisualMediaAttachment = hasAttachment(Attachment.isVisualMedia);
const initializeAttachmentMetadata = async (message) => {
    if (message.type === 'verified-change') {
        return message;
    }
    if (message.type === 'message-history-unsynced') {
        return message;
    }
    if (message.type === 'profile-change') {
        return message;
    }
    if (message.messageTimer || message.isViewOnce) {
        return message;
    }
    const attachments = message.attachments.filter((attachment) => attachment.contentType !== 'text/x-signal-plain');
    const hasAttachments = IndexedDB.toIndexableBoolean(attachments.length > 0);
    const hasFileAttachments = hasFileAttachment(Object.assign(Object.assign({}, message), { attachments }));
    const hasVisualMediaAttachments = hasVisualMediaAttachment(Object.assign(Object.assign({}, message), { attachments }));
    return Object.assign(Object.assign({}, message), { hasAttachments,
        hasFileAttachments,
        hasVisualMediaAttachments });
};
exports.initializeAttachmentMetadata = initializeAttachmentMetadata;
