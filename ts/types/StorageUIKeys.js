"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.STORAGE_UI_KEYS = void 0;
// Configuration keys that only affect UI
exports.STORAGE_UI_KEYS = [
    'always-relay-calls',
    'audio-notification',
    'auto-download-update',
    'badge-count-muted-conversations',
    'call-ringtone-notification',
    'call-system-notification',
    'hide-menu-bar',
    'system-tray-setting',
    'incoming-call-notification',
    'notification-draw-attention',
    'notification-setting',
    'spell-check',
    'theme-setting',
    'defaultConversationColor',
    'customColors',
    'showStickerPickerHint',
    'showStickersIntroduction',
    'preferred-video-input-device',
    'preferred-audio-input-device',
    'preferred-audio-output-device',
    'preferredLeftPaneWidth',
    'preferredReactionEmoji',
    'previousAudioDeviceModule',
    'skinTone',
    'zoomFactor',
];
