"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseSystemTraySetting = exports.shouldMinimizeToSystemTray = exports.SystemTraySetting = void 0;
const enum_1 = require("../util/enum");
// Be careful when changing these values, as they are persisted.
var SystemTraySetting;
(function (SystemTraySetting) {
    SystemTraySetting["DoNotUseSystemTray"] = "DoNotUseSystemTray";
    SystemTraySetting["MinimizeToSystemTray"] = "MinimizeToSystemTray";
    SystemTraySetting["MinimizeToAndStartInSystemTray"] = "MinimizeToAndStartInSystemTray";
})(SystemTraySetting = exports.SystemTraySetting || (exports.SystemTraySetting = {}));
const shouldMinimizeToSystemTray = (setting) => setting === SystemTraySetting.MinimizeToSystemTray ||
    setting === SystemTraySetting.MinimizeToAndStartInSystemTray;
exports.shouldMinimizeToSystemTray = shouldMinimizeToSystemTray;
exports.parseSystemTraySetting = (0, enum_1.makeEnumParser)(SystemTraySetting, SystemTraySetting.DoNotUseSystemTray);
