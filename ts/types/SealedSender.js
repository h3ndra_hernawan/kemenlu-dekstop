"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SEALED_SENDER = void 0;
var SEALED_SENDER;
(function (SEALED_SENDER) {
    SEALED_SENDER[SEALED_SENDER["UNKNOWN"] = 0] = "UNKNOWN";
    SEALED_SENDER[SEALED_SENDER["ENABLED"] = 1] = "ENABLED";
    SEALED_SENDER[SEALED_SENDER["DISABLED"] = 2] = "DISABLED";
    SEALED_SENDER[SEALED_SENDER["UNRESTRICTED"] = 3] = "UNRESTRICTED";
})(SEALED_SENDER = exports.SEALED_SENDER || (exports.SEALED_SENDER = {}));
