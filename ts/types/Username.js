"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUsernameFromSearch = exports.isValidUsername = exports.MIN_USERNAME = exports.MAX_USERNAME = void 0;
exports.MAX_USERNAME = 26;
exports.MIN_USERNAME = 4;
function isValidUsername(searchTerm) {
    return /^[a-z_][0-9a-z_]{3,25}$/.test(searchTerm);
}
exports.isValidUsername = isValidUsername;
function getUsernameFromSearch(searchTerm) {
    if (/^[+0-9]+$/.test(searchTerm)) {
        return undefined;
    }
    const match = /^@?(.*?)@?$/.exec(searchTerm);
    if (match && match[1]) {
        return match[1];
    }
    return undefined;
}
exports.getUsernameFromSearch = getUsernameFromSearch;
