"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isValid = void 0;
const lodash_1 = require("lodash");
const isValid = (value) => {
    return Boolean((0, lodash_1.isNumber)(value) && value >= 0);
};
exports.isValid = isValid;
