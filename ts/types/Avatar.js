"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDefaultAvatars = exports.GroupAvatarIcons = exports.PersonalAvatarIcons = void 0;
const assert_1 = require("../util/assert");
exports.PersonalAvatarIcons = [
    'abstract_01',
    'abstract_02',
    'abstract_03',
    'cat',
    'dog',
    'fox',
    'tucan',
    'pig',
    'dinosour',
    'sloth',
    'incognito',
    'ghost',
];
exports.GroupAvatarIcons = [
    'balloon',
    'book',
    'briefcase',
    'celebration',
    'drink',
    'football',
    'heart',
    'house',
    'melon',
    'soccerball',
    'sunset',
    'surfboard',
];
const groupIconColors = [
    'A180',
    'A120',
    'A110',
    'A170',
    'A100',
    'A210',
    'A100',
    'A180',
    'A120',
    'A110',
    'A130',
    'A210',
];
const personalIconColors = [
    'A130',
    'A120',
    'A170',
    'A190',
    'A140',
    'A190',
    'A120',
    'A160',
    'A130',
    'A180',
    'A210',
    'A100',
];
(0, assert_1.strictAssert)(groupIconColors.length === exports.GroupAvatarIcons.length &&
    personalIconColors.length === exports.PersonalAvatarIcons.length, 'colors.length !== icons.length');
const groupDefaultAvatars = exports.GroupAvatarIcons.map((icon, index) => ({
    id: index,
    color: groupIconColors[index],
    icon,
}));
const personalDefaultAvatars = exports.PersonalAvatarIcons.map((icon, index) => ({
    id: index,
    color: personalIconColors[index],
    icon,
}));
function getDefaultAvatars(isGroup) {
    if (isGroup) {
        return groupDefaultAvatars;
    }
    return personalDefaultAvatars;
}
exports.getDefaultAvatars = getDefaultAvatars;
