"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScrollBehavior = exports.ThemeType = void 0;
var ThemeType;
(function (ThemeType) {
    ThemeType["light"] = "light";
    ThemeType["dark"] = "dark";
})(ThemeType = exports.ThemeType || (exports.ThemeType = {}));
// These are strings so they can be interpolated into class names.
var ScrollBehavior;
(function (ScrollBehavior) {
    ScrollBehavior["Default"] = "default";
    ScrollBehavior["Hard"] = "hard";
})(ScrollBehavior = exports.ScrollBehavior || (exports.ScrollBehavior = {}));
