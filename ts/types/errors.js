"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileDecryptError = exports.CapabilityError = exports.toLogFormat = void 0;
/* eslint-disable max-classes-per-file */
function toLogFormat(error) {
    if (error instanceof Error && error.stack) {
        return error.stack;
    }
    return String(error);
}
exports.toLogFormat = toLogFormat;
class CapabilityError extends Error {
}
exports.CapabilityError = CapabilityError;
class ProfileDecryptError extends Error {
}
exports.ProfileDecryptError = ProfileDecryptError;
