"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.CipherType = exports.HashType = void 0;
var HashType;
(function (HashType) {
    HashType["size256"] = "sha256";
    HashType["size512"] = "sha512";
})(HashType = exports.HashType || (exports.HashType = {}));
var CipherType;
(function (CipherType) {
    CipherType["AES256CBC"] = "aes-256-cbc";
    CipherType["AES256CTR"] = "aes-256-ctr";
    CipherType["AES256GCM"] = "aes-256-gcm";
})(CipherType = exports.CipherType || (exports.CipherType = {}));
