"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogLevel = void 0;
// These match [Pino's recommendations][0].
// [0]: https://getpino.io/#/docs/api?id=loggerlevels-object
var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["Fatal"] = 60] = "Fatal";
    LogLevel[LogLevel["Error"] = 50] = "Error";
    LogLevel[LogLevel["Warn"] = 40] = "Warn";
    LogLevel[LogLevel["Info"] = 30] = "Info";
    LogLevel[LogLevel["Debug"] = 20] = "Debug";
    LogLevel[LogLevel["Trace"] = 10] = "Trace";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
