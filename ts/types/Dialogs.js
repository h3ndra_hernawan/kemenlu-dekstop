"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.DialogType = void 0;
/* eslint-disable camelcase */
var DialogType;
(function (DialogType) {
    DialogType["None"] = "None";
    DialogType["Update"] = "Update";
    DialogType["Cannot_Update"] = "Cannot_Update";
    DialogType["MacOS_Read_Only"] = "MacOS_Read_Only";
    DialogType["DownloadReady"] = "DownloadReady";
    DialogType["Downloading"] = "Downloading";
})(DialogType = exports.DialogType || (exports.DialogType = {}));
