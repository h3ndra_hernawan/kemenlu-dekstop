"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isAutoDownloadUpdatesSupported = exports.isSystemTraySupported = exports.getTitleBarVisibility = exports.TitleBarVisibility = exports.isDrawAttentionSupported = exports.isHideMenuBarSupported = exports.isAutoLaunchSupported = exports.isNotificationGroupingSupported = exports.isAudioNotificationSupported = exports.getAudioNotificationSupport = exports.AudioNotificationSupport = void 0;
const OS = __importStar(require("../OS"));
const version_1 = require("../util/version");
const MIN_WINDOWS_VERSION = '8.0.0';
var AudioNotificationSupport;
(function (AudioNotificationSupport) {
    AudioNotificationSupport[AudioNotificationSupport["None"] = 0] = "None";
    AudioNotificationSupport[AudioNotificationSupport["Native"] = 1] = "Native";
    AudioNotificationSupport[AudioNotificationSupport["Custom"] = 2] = "Custom";
})(AudioNotificationSupport = exports.AudioNotificationSupport || (exports.AudioNotificationSupport = {}));
function getAudioNotificationSupport() {
    if (OS.isWindows(MIN_WINDOWS_VERSION) || OS.isMacOS()) {
        return AudioNotificationSupport.Native;
    }
    if (OS.isLinux()) {
        return AudioNotificationSupport.Custom;
    }
    return AudioNotificationSupport.None;
}
exports.getAudioNotificationSupport = getAudioNotificationSupport;
const isAudioNotificationSupported = () => getAudioNotificationSupport() !== AudioNotificationSupport.None;
exports.isAudioNotificationSupported = isAudioNotificationSupported;
// Using `Notification::tag` has a bug on Windows 7:
// https://github.com/electron/electron/issues/11189
const isNotificationGroupingSupported = () => !OS.isWindows() || OS.isWindows(MIN_WINDOWS_VERSION);
exports.isNotificationGroupingSupported = isNotificationGroupingSupported;
// Login item settings are only supported on macOS and Windows, according to [Electron's
//   docs][0].
// [0]: https://www.electronjs.org/docs/api/app#appsetloginitemsettingssettings-macos-windows
const isAutoLaunchSupported = () => OS.isWindows() || OS.isMacOS();
exports.isAutoLaunchSupported = isAutoLaunchSupported;
// the "hide menu bar" option is specific to Windows and Linux
const isHideMenuBarSupported = () => !OS.isMacOS();
exports.isHideMenuBarSupported = isHideMenuBarSupported;
// the "draw attention on notification" option is specific to Windows and Linux
const isDrawAttentionSupported = () => !OS.isMacOS();
exports.isDrawAttentionSupported = isDrawAttentionSupported;
var TitleBarVisibility;
(function (TitleBarVisibility) {
    TitleBarVisibility[TitleBarVisibility["Visible"] = 0] = "Visible";
    TitleBarVisibility[TitleBarVisibility["Hidden"] = 1] = "Hidden";
})(TitleBarVisibility = exports.TitleBarVisibility || (exports.TitleBarVisibility = {}));
// This should match the "logic" in `stylesheets/_global.scss`.
const getTitleBarVisibility = () => OS.isMacOS() ? TitleBarVisibility.Hidden : TitleBarVisibility.Visible;
exports.getTitleBarVisibility = getTitleBarVisibility;
/**
 * Returns `true` if you can minimize the app to the system tray. Users can override this
 * option with a command line flag, but that is not officially supported.
 */
const isSystemTraySupported = (appVersion) => 
// We eventually want to support Linux in production.
OS.isWindows() || (OS.isLinux() && !(0, version_1.isProduction)(appVersion));
exports.isSystemTraySupported = isSystemTraySupported;
const isAutoDownloadUpdatesSupported = () => OS.isWindows() || OS.isMacOS();
exports.isAutoDownloadUpdatesSupported = isAutoDownloadUpdatesSupported;
