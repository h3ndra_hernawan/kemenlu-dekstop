"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.hasExpiration = exports.isUserMessage = void 0;
const isUserMessage = (message) => message.type === 'incoming' || message.type === 'outgoing';
exports.isUserMessage = isUserMessage;
const hasExpiration = (message) => {
    if (!(0, exports.isUserMessage)(message)) {
        return false;
    }
    const { expireTimer } = message;
    return typeof expireTimer === 'number' && expireTimer > 0;
};
exports.hasExpiration = hasExpiration;
