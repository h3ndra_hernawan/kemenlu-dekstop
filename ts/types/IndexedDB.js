"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.toIndexablePresence = exports.toIndexableBoolean = exports.INDEXABLE_TRUE = exports.INDEXABLE_FALSE = void 0;
exports.INDEXABLE_FALSE = 0;
exports.INDEXABLE_TRUE = 1;
const toIndexableBoolean = (value) => value ? exports.INDEXABLE_TRUE : exports.INDEXABLE_FALSE;
exports.toIndexableBoolean = toIndexableBoolean;
const toIndexablePresence = (value) => value ? exports.INDEXABLE_TRUE : undefined;
exports.toIndexablePresence = toIndexablePresence;
