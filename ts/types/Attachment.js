"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultBlurHash = exports.getUploadSizeLimitKb = exports.getFileExtension = exports.getSuggestedFilename = exports.save = exports.isVoiceMessage = exports.isFile = exports.isVisualMedia = exports.getAlt = exports.getGridDimensions = exports.areAllAttachmentsVisual = exports.getImageDimensions = exports.hasVideoScreenshot = exports.hasVideoBlurHash = exports.hasNotDownloaded = exports.isGIF = exports.isVideoAttachment = exports.isVideo = exports.hasImage = exports.canBeTranscoded = exports.isImageAttachment = exports.isImage = exports.getUrl = exports.getThumbnailUrl = exports.canDisplayImage = exports.isAudio = exports.getExtensionForDisplay = exports.captureDimensionsAndScreenshot = exports.deleteData = exports.loadData = exports.hasData = exports.removeSchemaVersion = exports.replaceUnicodeV2 = exports.replaceUnicodeOrderOverrides = exports._replaceUnicodeOrderOverridesSync = exports.autoOrientJPEG = exports.isValid = exports.migrateDataToFileSystem = void 0;
const is_1 = __importDefault(require("@sindresorhus/is"));
const moment_1 = __importDefault(require("moment"));
const lodash_1 = require("lodash");
const blob_util_1 = require("blob-util");
const MIME = __importStar(require("./MIME"));
const errors_1 = require("./errors");
const protobuf_1 = require("../protobuf");
const GoogleChrome_1 = require("../util/GoogleChrome");
const Util_1 = require("./Util");
const GoogleChrome = __importStar(require("../util/GoogleChrome"));
const scaleImageToLevel_1 = require("../util/scaleImageToLevel");
const MAX_WIDTH = 300;
const MAX_HEIGHT = MAX_WIDTH * 1.5;
const MIN_WIDTH = 200;
const MIN_HEIGHT = 50;
async function migrateDataToFileSystem(attachment, { writeNewAttachmentData, }) {
    if (!(0, lodash_1.isFunction)(writeNewAttachmentData)) {
        throw new TypeError("'writeNewAttachmentData' must be a function");
    }
    const { data } = attachment;
    const attachmentHasData = !(0, lodash_1.isUndefined)(data);
    const shouldSkipSchemaUpgrade = !attachmentHasData;
    if (shouldSkipSchemaUpgrade) {
        return attachment;
    }
    if (!(0, lodash_1.isTypedArray)(data)) {
        throw new TypeError('Expected `attachment.data` to be a typed array;' +
            ` got: ${typeof attachment.data}`);
    }
    const path = await writeNewAttachmentData(data);
    const attachmentWithoutData = (0, lodash_1.omit)(Object.assign(Object.assign({}, attachment), { path }), ['data']);
    return attachmentWithoutData;
}
exports.migrateDataToFileSystem = migrateDataToFileSystem;
// // Incoming message attachment fields
// {
//   id: string
//   contentType: MIMEType
//   data: Uint8Array
//   digest: Uint8Array
//   fileName?: string
//   flags: null
//   key: Uint8Array
//   size: integer
//   thumbnail: Uint8Array
// }
// // Outgoing message attachment fields
// {
//   contentType: MIMEType
//   data: Uint8Array
//   fileName: string
//   size: integer
// }
// Returns true if `rawAttachment` is a valid attachment based on our current schema.
// Over time, we can expand this definition to become more narrow, e.g. require certain
// fields, etc.
function isValid(rawAttachment) {
    // NOTE: We cannot use `_.isPlainObject` because `rawAttachment` is
    // deserialized by protobuf:
    if (!rawAttachment) {
        return false;
    }
    return true;
}
exports.isValid = isValid;
// Upgrade steps
// NOTE: This step strips all EXIF metadata from JPEG images as
// part of re-encoding the image:
async function autoOrientJPEG(attachment, _, { sendHQImages = false, isIncoming = false, } = {}) {
    if (isIncoming && !MIME.isJPEG(attachment.contentType)) {
        return attachment;
    }
    if (!canBeTranscoded(attachment)) {
        return attachment;
    }
    // If we haven't downloaded the attachment yet, we won't have the data.
    // All images go through handleImageAttachment before being sent and thus have
    // already been scaled to level, oriented, stripped of exif data, and saved
    // in high quality format. If we want to send the image in HQ we can return
    // the attachement as-is. Otherwise we'll have to further scale it down.
    if (!attachment.data || sendHQImages) {
        return attachment;
    }
    const dataBlob = new Blob([attachment.data], {
        type: attachment.contentType,
    });
    const { blob: xcodedDataBlob } = await (0, scaleImageToLevel_1.scaleImageToLevel)(dataBlob, attachment.contentType, isIncoming);
    const xcodedDataArrayBuffer = await (0, blob_util_1.blobToArrayBuffer)(xcodedDataBlob);
    // IMPORTANT: We overwrite the existing `data` `Uint8Array` losing the original
    // image data. Ideally, we’d preserve the original image data for users who want to
    // retain it but due to reports of data loss, we don’t want to overburden IndexedDB
    // by potentially doubling stored image data.
    // See: https://github.com/signalapp/Signal-Desktop/issues/1589
    const xcodedAttachment = Object.assign(Object.assign({}, (0, lodash_1.omit)(attachment, 'digest')), { data: new Uint8Array(xcodedDataArrayBuffer), size: xcodedDataArrayBuffer.byteLength });
    return xcodedAttachment;
}
exports.autoOrientJPEG = autoOrientJPEG;
const UNICODE_LEFT_TO_RIGHT_OVERRIDE = '\u202D';
const UNICODE_RIGHT_TO_LEFT_OVERRIDE = '\u202E';
const UNICODE_REPLACEMENT_CHARACTER = '\uFFFD';
const INVALID_CHARACTERS_PATTERN = new RegExp(`[${UNICODE_LEFT_TO_RIGHT_OVERRIDE}${UNICODE_RIGHT_TO_LEFT_OVERRIDE}]`, 'g');
// NOTE: Expose synchronous version to do property-based testing using `testcheck`,
// which currently doesn’t support async testing:
// https://github.com/leebyron/testcheck-js/issues/45
function _replaceUnicodeOrderOverridesSync(attachment) {
    if (!is_1.default.string(attachment.fileName)) {
        return attachment;
    }
    const normalizedFilename = attachment.fileName.replace(INVALID_CHARACTERS_PATTERN, UNICODE_REPLACEMENT_CHARACTER);
    const newAttachment = Object.assign(Object.assign({}, attachment), { fileName: normalizedFilename });
    return newAttachment;
}
exports._replaceUnicodeOrderOverridesSync = _replaceUnicodeOrderOverridesSync;
const replaceUnicodeOrderOverrides = async (attachment) => {
    return _replaceUnicodeOrderOverridesSync(attachment);
};
exports.replaceUnicodeOrderOverrides = replaceUnicodeOrderOverrides;
// \u202A-\u202E is LRE, RLE, PDF, LRO, RLO
// \u2066-\u2069 is LRI, RLI, FSI, PDI
// \u200E is LRM
// \u200F is RLM
// \u061C is ALM
const V2_UNWANTED_UNICODE = /[\u202A-\u202E\u2066-\u2069\u200E\u200F\u061C]/g;
async function replaceUnicodeV2(attachment) {
    if (!is_1.default.string(attachment.fileName)) {
        return attachment;
    }
    const fileName = attachment.fileName.replace(V2_UNWANTED_UNICODE, UNICODE_REPLACEMENT_CHARACTER);
    return Object.assign(Object.assign({}, attachment), { fileName });
}
exports.replaceUnicodeV2 = replaceUnicodeV2;
function removeSchemaVersion({ attachment, logger, }) {
    if (!exports.isValid(attachment)) {
        logger.error('Attachment.removeSchemaVersion: Invalid input attachment:', attachment);
        return attachment;
    }
    return (0, lodash_1.omit)(attachment, 'schemaVersion');
}
exports.removeSchemaVersion = removeSchemaVersion;
function hasData(attachment) {
    return attachment.data instanceof Uint8Array;
}
exports.hasData = hasData;
function loadData(readAttachmentData) {
    if (!is_1.default.function_(readAttachmentData)) {
        throw new TypeError("'readAttachmentData' must be a function");
    }
    return async (attachment) => {
        if (!isValid(attachment)) {
            throw new TypeError("'attachment' is not valid");
        }
        const isAlreadyLoaded = Boolean(attachment.data);
        if (isAlreadyLoaded) {
            return attachment;
        }
        if (!is_1.default.string(attachment.path)) {
            throw new TypeError("'attachment.path' is required");
        }
        const data = await readAttachmentData(attachment.path);
        return Object.assign(Object.assign({}, attachment), { data, size: data.byteLength });
    };
}
exports.loadData = loadData;
function deleteData(deleteOnDisk) {
    if (!is_1.default.function_(deleteOnDisk)) {
        throw new TypeError('deleteData: deleteOnDisk must be a function');
    }
    return async (attachment) => {
        if (!isValid(attachment)) {
            throw new TypeError('deleteData: attachment is not valid');
        }
        const { path, thumbnail, screenshot } = attachment;
        if (is_1.default.string(path)) {
            await deleteOnDisk(path);
        }
        if (thumbnail && is_1.default.string(thumbnail.path)) {
            await deleteOnDisk(thumbnail.path);
        }
        if (screenshot && is_1.default.string(screenshot.path)) {
            await deleteOnDisk(screenshot.path);
        }
    };
}
exports.deleteData = deleteData;
const THUMBNAIL_SIZE = 150;
const THUMBNAIL_CONTENT_TYPE = MIME.IMAGE_PNG;
async function captureDimensionsAndScreenshot(attachment, params) {
    const { contentType } = attachment;
    const { writeNewAttachmentData, getAbsoluteAttachmentPath, makeObjectUrl, revokeObjectUrl, getImageDimensions: getImageDimensionsFromURL, makeImageThumbnail, makeVideoScreenshot, logger, } = params;
    if (!GoogleChrome.isImageTypeSupported(contentType) &&
        !GoogleChrome.isVideoTypeSupported(contentType)) {
        return attachment;
    }
    // If the attachment hasn't been downloaded yet, we won't have a path
    if (!attachment.path) {
        return attachment;
    }
    const absolutePath = await getAbsoluteAttachmentPath(attachment.path);
    if (GoogleChrome.isImageTypeSupported(contentType)) {
        try {
            const { width, height } = await getImageDimensionsFromURL({
                objectUrl: absolutePath,
                logger,
            });
            const thumbnailBuffer = await (0, blob_util_1.blobToArrayBuffer)(await makeImageThumbnail({
                size: THUMBNAIL_SIZE,
                objectUrl: absolutePath,
                contentType: THUMBNAIL_CONTENT_TYPE,
                logger,
            }));
            const thumbnailPath = await writeNewAttachmentData(new Uint8Array(thumbnailBuffer));
            return Object.assign(Object.assign({}, attachment), { width,
                height, thumbnail: {
                    path: thumbnailPath,
                    contentType: THUMBNAIL_CONTENT_TYPE,
                    width: THUMBNAIL_SIZE,
                    height: THUMBNAIL_SIZE,
                } });
        }
        catch (error) {
            logger.error('captureDimensionsAndScreenshot:', 'error processing image; skipping screenshot generation', (0, errors_1.toLogFormat)(error));
            return attachment;
        }
    }
    let screenshotObjectUrl;
    try {
        const screenshotBuffer = await (0, blob_util_1.blobToArrayBuffer)(await makeVideoScreenshot({
            objectUrl: absolutePath,
            contentType: THUMBNAIL_CONTENT_TYPE,
            logger,
        }));
        screenshotObjectUrl = makeObjectUrl(screenshotBuffer, THUMBNAIL_CONTENT_TYPE);
        const { width, height } = await getImageDimensionsFromURL({
            objectUrl: screenshotObjectUrl,
            logger,
        });
        const screenshotPath = await writeNewAttachmentData(new Uint8Array(screenshotBuffer));
        const thumbnailBuffer = await (0, blob_util_1.blobToArrayBuffer)(await makeImageThumbnail({
            size: THUMBNAIL_SIZE,
            objectUrl: screenshotObjectUrl,
            contentType: THUMBNAIL_CONTENT_TYPE,
            logger,
        }));
        const thumbnailPath = await writeNewAttachmentData(new Uint8Array(thumbnailBuffer));
        return Object.assign(Object.assign({}, attachment), { screenshot: {
                contentType: THUMBNAIL_CONTENT_TYPE,
                path: screenshotPath,
                width,
                height,
            }, thumbnail: {
                path: thumbnailPath,
                contentType: THUMBNAIL_CONTENT_TYPE,
                width: THUMBNAIL_SIZE,
                height: THUMBNAIL_SIZE,
            }, width,
            height });
    }
    catch (error) {
        logger.error('captureDimensionsAndScreenshot: error processing video; skipping screenshot generation', (0, errors_1.toLogFormat)(error));
        return attachment;
    }
    finally {
        if (screenshotObjectUrl !== undefined) {
            revokeObjectUrl(screenshotObjectUrl);
        }
    }
}
exports.captureDimensionsAndScreenshot = captureDimensionsAndScreenshot;
// UI-focused functions
function getExtensionForDisplay({ fileName, contentType, }) {
    if (fileName && fileName.indexOf('.') >= 0) {
        const lastPeriod = fileName.lastIndexOf('.');
        const extension = fileName.slice(lastPeriod + 1);
        if (extension.length) {
            return extension;
        }
    }
    if (!contentType) {
        return undefined;
    }
    const slash = contentType.indexOf('/');
    if (slash >= 0) {
        return contentType.slice(slash + 1);
    }
    return undefined;
}
exports.getExtensionForDisplay = getExtensionForDisplay;
function isAudio(attachments) {
    return Boolean(attachments &&
        attachments[0] &&
        attachments[0].contentType &&
        !attachments[0].isCorrupted &&
        MIME.isAudio(attachments[0].contentType));
}
exports.isAudio = isAudio;
function canDisplayImage(attachments) {
    const { height, width } = attachments && attachments[0] ? attachments[0] : { height: 0, width: 0 };
    return Boolean(height &&
        height > 0 &&
        height <= 4096 &&
        width &&
        width > 0 &&
        width <= 4096);
}
exports.canDisplayImage = canDisplayImage;
function getThumbnailUrl(attachment) {
    if (attachment.thumbnail) {
        return attachment.thumbnail.url;
    }
    return getUrl(attachment);
}
exports.getThumbnailUrl = getThumbnailUrl;
function getUrl(attachment) {
    if (attachment.screenshot) {
        return attachment.screenshot.url;
    }
    if (isVideoAttachment(attachment)) {
        return undefined;
    }
    return attachment.url;
}
exports.getUrl = getUrl;
function isImage(attachments) {
    return Boolean(attachments &&
        attachments[0] &&
        attachments[0].contentType &&
        (0, GoogleChrome_1.isImageTypeSupported)(attachments[0].contentType));
}
exports.isImage = isImage;
function isImageAttachment(attachment) {
    return Boolean(attachment &&
        attachment.contentType &&
        (0, GoogleChrome_1.isImageTypeSupported)(attachment.contentType));
}
exports.isImageAttachment = isImageAttachment;
function canBeTranscoded(attachment) {
    return Boolean(attachment &&
        isImageAttachment(attachment) &&
        !MIME.isGif(attachment.contentType));
}
exports.canBeTranscoded = canBeTranscoded;
function hasImage(attachments) {
    return Boolean(attachments &&
        attachments[0] &&
        (attachments[0].url || attachments[0].pending || attachments[0].blurHash));
}
exports.hasImage = hasImage;
function isVideo(attachments) {
    if (!attachments || attachments.length === 0) {
        return false;
    }
    return isVideoAttachment(attachments[0]);
}
exports.isVideo = isVideo;
function isVideoAttachment(attachment) {
    if (!attachment || !attachment.contentType) {
        return false;
    }
    return (0, GoogleChrome_1.isVideoTypeSupported)(attachment.contentType);
}
exports.isVideoAttachment = isVideoAttachment;
function isGIF(attachments) {
    if (!attachments || attachments.length !== 1) {
        return false;
    }
    const [attachment] = attachments;
    const flag = protobuf_1.SignalService.AttachmentPointer.Flags.GIF;
    const hasFlag = 
    // eslint-disable-next-line no-bitwise
    !is_1.default.undefined(attachment.flags) && (attachment.flags & flag) === flag;
    return hasFlag && isVideoAttachment(attachment);
}
exports.isGIF = isGIF;
function hasNotDownloaded(attachment) {
    return Boolean(attachment && !attachment.url);
}
exports.hasNotDownloaded = hasNotDownloaded;
function hasVideoBlurHash(attachments) {
    const firstAttachment = attachments ? attachments[0] : null;
    return Boolean(firstAttachment && firstAttachment.blurHash);
}
exports.hasVideoBlurHash = hasVideoBlurHash;
function hasVideoScreenshot(attachments) {
    const firstAttachment = attachments ? attachments[0] : null;
    return (firstAttachment &&
        firstAttachment.screenshot &&
        firstAttachment.screenshot.url);
}
exports.hasVideoScreenshot = hasVideoScreenshot;
function getImageDimensions(attachment, forcedWidth) {
    const { height, width } = attachment;
    if (!height || !width) {
        return {
            height: MIN_HEIGHT,
            width: MIN_WIDTH,
        };
    }
    const aspectRatio = height / width;
    const targetWidth = forcedWidth || Math.max(Math.min(MAX_WIDTH, width), MIN_WIDTH);
    const candidateHeight = Math.round(targetWidth * aspectRatio);
    return {
        width: targetWidth,
        height: Math.max(Math.min(MAX_HEIGHT, candidateHeight), MIN_HEIGHT),
    };
}
exports.getImageDimensions = getImageDimensions;
function areAllAttachmentsVisual(attachments) {
    if (!attachments) {
        return false;
    }
    const max = attachments.length;
    for (let i = 0; i < max; i += 1) {
        const attachment = attachments[i];
        if (!isImageAttachment(attachment) && !isVideoAttachment(attachment)) {
            return false;
        }
    }
    return true;
}
exports.areAllAttachmentsVisual = areAllAttachmentsVisual;
function getGridDimensions(attachments) {
    if (!attachments || !attachments.length) {
        return null;
    }
    if (!isImage(attachments) && !isVideo(attachments)) {
        return null;
    }
    if (attachments.length === 1) {
        return getImageDimensions(attachments[0]);
    }
    if (attachments.length === 2) {
        // A B
        return {
            height: 150,
            width: 300,
        };
    }
    if (attachments.length === 3) {
        // A A B
        // A A C
        return {
            height: 200,
            width: 300,
        };
    }
    if (attachments.length === 4) {
        // A B
        // C D
        return {
            height: 300,
            width: 300,
        };
    }
    // A A A B B B
    // A A A B B B
    // A A A B B B
    // C C D D E E
    // C C D D E E
    return {
        height: 250,
        width: 300,
    };
}
exports.getGridDimensions = getGridDimensions;
function getAlt(attachment, i18n) {
    if (isVideoAttachment(attachment)) {
        return i18n('videoAttachmentAlt');
    }
    return i18n('imageAttachmentAlt');
}
exports.getAlt = getAlt;
// Migration-related attachment stuff
const isVisualMedia = (attachment) => {
    const { contentType } = attachment;
    if (is_1.default.undefined(contentType)) {
        return false;
    }
    if ((0, exports.isVoiceMessage)(attachment)) {
        return false;
    }
    return MIME.isImage(contentType) || MIME.isVideo(contentType);
};
exports.isVisualMedia = isVisualMedia;
const isFile = (attachment) => {
    const { contentType } = attachment;
    if (is_1.default.undefined(contentType)) {
        return false;
    }
    if ((0, exports.isVisualMedia)(attachment)) {
        return false;
    }
    if ((0, exports.isVoiceMessage)(attachment)) {
        return false;
    }
    return true;
};
exports.isFile = isFile;
const isVoiceMessage = (attachment) => {
    const flag = protobuf_1.SignalService.AttachmentPointer.Flags.VOICE_MESSAGE;
    const hasFlag = 
    // eslint-disable-next-line no-bitwise
    !is_1.default.undefined(attachment.flags) && (attachment.flags & flag) === flag;
    if (hasFlag) {
        return true;
    }
    const isLegacyAndroidVoiceMessage = !is_1.default.undefined(attachment.contentType) &&
        MIME.isAudio(attachment.contentType) &&
        !attachment.fileName;
    if (isLegacyAndroidVoiceMessage) {
        return true;
    }
    return false;
};
exports.isVoiceMessage = isVoiceMessage;
const save = async ({ attachment, index, readAttachmentData, saveAttachmentToDisk, timestamp, }) => {
    let data;
    if (attachment.path) {
        data = await readAttachmentData(attachment.path);
    }
    else if (attachment.data) {
        data = attachment.data;
    }
    else {
        throw new Error('Attachment had neither path nor data');
    }
    const name = (0, exports.getSuggestedFilename)({ attachment, timestamp, index });
    const result = await saveAttachmentToDisk({
        data,
        name,
    });
    if (!result) {
        return null;
    }
    return result.fullPath;
};
exports.save = save;
const getSuggestedFilename = ({ attachment, timestamp, index, }) => {
    if (!(0, lodash_1.isNumber)(index) && attachment.fileName) {
        return attachment.fileName;
    }
    const prefix = 'signal';
    const suffix = timestamp
        ? (0, moment_1.default)(timestamp).format('-YYYY-MM-DD-HHmmss')
        : '';
    const fileType = (0, exports.getFileExtension)(attachment);
    const extension = fileType ? `.${fileType}` : '';
    const indexSuffix = index ? `_${(0, lodash_1.padStart)(index.toString(), 3, '0')}` : '';
    return `${prefix}${suffix}${indexSuffix}${extension}`;
};
exports.getSuggestedFilename = getSuggestedFilename;
const getFileExtension = (attachment) => {
    if (!attachment.contentType) {
        return undefined;
    }
    switch (attachment.contentType) {
        case 'video/quicktime':
            return 'mov';
        default:
            return attachment.contentType.split('/')[1];
    }
};
exports.getFileExtension = getFileExtension;
const getUploadSizeLimitKb = (contentType) => {
    if (MIME.isGif(contentType)) {
        return 25000;
    }
    if ((0, GoogleChrome_1.isImageTypeSupported)(contentType)) {
        return 6000;
    }
    return 100000;
};
exports.getUploadSizeLimitKb = getUploadSizeLimitKb;
const defaultBlurHash = (theme = Util_1.ThemeType.light) => {
    if (theme === Util_1.ThemeType.dark) {
        return 'L05OQnoffQofoffQfQfQfQfQfQfQ';
    }
    return 'L1Q]+w-;fQ-;~qfQfQfQfQfQfQfQ';
};
exports.defaultBlurHash = defaultBlurHash;
