"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.QualifiedAddress = void 0;
const assert_1 = require("../util/assert");
const UUID_1 = require("./UUID");
const Address_1 = require("./Address");
const QUALIFIED_ADDRESS_REGEXP = /^([0-9a-f-]+):([0-9a-f-]+).(\d+)$/i;
class QualifiedAddress {
    constructor(ourUuid, address) {
        this.ourUuid = ourUuid;
        this.address = address;
    }
    get uuid() {
        return this.address.uuid;
    }
    get deviceId() {
        return this.address.deviceId;
    }
    toString() {
        return `${this.ourUuid.toString()}:${this.address.toString()}`;
    }
    static parse(value) {
        const match = value.match(QUALIFIED_ADDRESS_REGEXP);
        (0, assert_1.strictAssert)(match !== null, `Invalid QualifiedAddress: ${value}`);
        const [whole, ourUuid, uuid, deviceId] = match;
        (0, assert_1.strictAssert)(whole === value, 'Integrity check');
        return new QualifiedAddress(new UUID_1.UUID(ourUuid), Address_1.Address.create(uuid, parseInt(deviceId, 10)));
    }
}
exports.QualifiedAddress = QualifiedAddress;
