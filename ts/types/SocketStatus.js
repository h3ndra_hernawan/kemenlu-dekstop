"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketStatus = void 0;
// Maps to values found here: https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/readyState
// which are returned by libtextsecure's MessageReceiver
var SocketStatus;
(function (SocketStatus) {
    SocketStatus["CONNECTING"] = "CONNECTING";
    SocketStatus["OPEN"] = "OPEN";
    SocketStatus["CLOSING"] = "CLOSING";
    SocketStatus["CLOSED"] = "CLOSED";
})(SocketStatus = exports.SocketStatus || (exports.SocketStatus = {}));
