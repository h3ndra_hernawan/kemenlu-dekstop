"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.Address = void 0;
const assert_1 = require("../util/assert");
const UUID_1 = require("./UUID");
const ADDRESS_REGEXP = /^([0-9a-f-]+).(\d+)$/i;
class Address {
    constructor(uuid, deviceId) {
        this.uuid = uuid;
        this.deviceId = deviceId;
    }
    toString() {
        return `${this.uuid.toString()}.${this.deviceId}`;
    }
    static parse(value) {
        const match = value.match(ADDRESS_REGEXP);
        (0, assert_1.strictAssert)(match !== null, `Invalid Address: ${value}`);
        const [whole, uuid, deviceId] = match;
        (0, assert_1.strictAssert)(whole === value, 'Integrity check');
        return Address.create(uuid, parseInt(deviceId, 10));
    }
    static create(uuid, deviceId) {
        return new Address(new UUID_1.UUID(uuid), deviceId);
    }
}
exports.Address = Address;
