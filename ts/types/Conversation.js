"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteExternalFiles = exports.maybeUpdateProfileAvatar = exports.maybeUpdateAvatar = void 0;
const Crypto_1 = require("../Crypto");
function buildAvatarUpdater({ field }) {
    return async (conversation, data, { deleteAttachmentData, doesAttachmentExist, writeNewAttachmentData, }) => {
        if (!conversation) {
            return conversation;
        }
        const avatar = conversation[field];
        const newHash = (0, Crypto_1.computeHash)(data);
        if (!avatar || !avatar.hash) {
            return Object.assign(Object.assign({}, conversation), { [field]: {
                    hash: newHash,
                    path: await writeNewAttachmentData(data),
                } });
        }
        const { hash, path } = avatar;
        const exists = await doesAttachmentExist(path);
        if (!exists) {
            window.SignalContext.log.warn(`Conversation.buildAvatarUpdater: attachment ${path} did not exist`);
        }
        if (exists && hash === newHash) {
            return conversation;
        }
        await deleteAttachmentData(path);
        return Object.assign(Object.assign({}, conversation), { [field]: {
                hash: newHash,
                path: await writeNewAttachmentData(data),
            } });
    };
}
exports.maybeUpdateAvatar = buildAvatarUpdater({ field: 'avatar' });
exports.maybeUpdateProfileAvatar = buildAvatarUpdater({
    field: 'profileAvatar',
});
async function deleteExternalFiles(conversation, { deleteAttachmentData, }) {
    if (!conversation) {
        return;
    }
    const { avatar, profileAvatar } = conversation;
    if (avatar && avatar.path) {
        await deleteAttachmentData(avatar.path);
    }
    if (profileAvatar && profileAvatar.path) {
        await deleteAttachmentData(profileAvatar.path);
    }
}
exports.deleteExternalFiles = deleteExternalFiles;
