"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemoveAllConfiguration = void 0;
var RemoveAllConfiguration;
(function (RemoveAllConfiguration) {
    RemoveAllConfiguration["Full"] = "Full";
    RemoveAllConfiguration["Soft"] = "Soft";
})(RemoveAllConfiguration = exports.RemoveAllConfiguration || (exports.RemoveAllConfiguration = {}));
