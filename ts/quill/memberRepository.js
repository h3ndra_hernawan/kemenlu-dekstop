"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberRepository = void 0;
const fuse_js_1 = __importDefault(require("fuse.js"));
const getOwn_1 = require("../util/getOwn");
const iterables_1 = require("../util/iterables");
const FUSE_OPTIONS = {
    location: 0,
    shouldSort: true,
    threshold: 0,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: ['name', 'firstName', 'profileName', 'title'],
    getFn(conversation, path) {
        // It'd be nice to avoid this cast, but Fuse's types don't allow it.
        const rawValue = (0, getOwn_1.getOwn)(conversation, path);
        if (typeof rawValue !== 'string') {
            // It might make more sense to return `undefined` here, but [Fuse's types don't
            //   allow it in newer versions][0] so we just return the empty string.
            //
            // [0]: https://github.com/krisk/Fuse/blob/e5e3abb44e004662c98750d0964d2d9a73b87848/src/index.d.ts#L117
            return '';
        }
        const segmenter = new Intl.Segmenter(undefined, { granularity: 'word' });
        const segments = segmenter.segment(rawValue);
        const wordlikeSegments = (0, iterables_1.filter)(segments, segment => segment.isWordLike);
        const wordlikes = (0, iterables_1.map)(wordlikeSegments, segment => segment.segment);
        return Array.from(wordlikes);
    },
};
class MemberRepository {
    constructor(members = []) {
        this.members = members;
        this.isFuseReady = false;
        this.fuse = new fuse_js_1.default([], FUSE_OPTIONS);
    }
    updateMembers(members) {
        this.members = members;
        this.isFuseReady = false;
    }
    getMembers(omit) {
        if (omit) {
            return this.members.filter(({ id }) => id !== omit.id);
        }
        return this.members;
    }
    getMemberById(id) {
        return id
            ? this.members.find(({ id: memberId }) => memberId === id)
            : undefined;
    }
    getMemberByUuid(uuid) {
        return uuid
            ? this.members.find(({ uuid: memberUuid }) => memberUuid === uuid)
            : undefined;
    }
    search(pattern, omit) {
        if (!this.isFuseReady) {
            this.fuse.setCollection(this.members);
            this.isFuseReady = true;
        }
        const results = this.fuse.search(`${pattern}`);
        if (omit) {
            return results.filter(({ id }) => id !== omit.id);
        }
        return results;
    }
}
exports.MemberRepository = MemberRepository;
