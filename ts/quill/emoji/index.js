"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmojiCompletion = exports.EmojiBlot = void 0;
var blot_1 = require("./blot");
Object.defineProperty(exports, "EmojiBlot", { enumerable: true, get: function () { return blot_1.EmojiBlot; } });
var completion_1 = require("./completion");
Object.defineProperty(exports, "EmojiCompletion", { enumerable: true, get: function () { return completion_1.EmojiCompletion; } });
