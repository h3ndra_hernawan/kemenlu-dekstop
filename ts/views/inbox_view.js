"use strict";
// Copyright 2014-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Backbone = __importStar(require("backbone"));
const log = __importStar(require("../logging/log"));
const showToast_1 = require("../util/showToast");
const ToastStickerPackInstallFailed_1 = require("../components/ToastStickerPackInstallFailed");
window.Whisper = window.Whisper || {};
const { Whisper } = window;
class ConversationStack extends Backbone.View {
    constructor() {
        super(...arguments);
        this.className = 'conversation-stack';
        this.conversationStack = [];
    }
    getTopConversation() {
        return this.conversationStack[this.conversationStack.length - 1];
    }
    open(conversation, messageId) {
        const topConversation = this.getTopConversation();
        if (!topConversation || topConversation.id !== conversation.id) {
            const view = new Whisper.ConversationView({
                model: conversation,
            });
            this.listenTo(conversation, 'unload', () => this.onUnload(conversation));
            this.listenTo(conversation, 'showSafetyNumber', () => view.showSafetyNumber());
            view.$el.appendTo(this.el);
            if (topConversation) {
                topConversation.trigger('unload', 'opened another conversation');
            }
            this.conversationStack.push(conversation);
            conversation.trigger('opened', messageId);
        }
        else if (messageId) {
            conversation.trigger('scroll-to-message', messageId);
        }
        this.render();
    }
    unload() {
        var _a;
        (_a = this.getTopConversation()) === null || _a === void 0 ? void 0 : _a.trigger('unload', 'force unload requested');
    }
    onUnload(conversation) {
        this.stopListening(conversation);
        this.conversationStack = this.conversationStack.filter((c) => c !== conversation);
        this.render();
    }
    render() {
        const isAnyConversationOpen = Boolean(this.conversationStack.length);
        this.$('.no-conversation-open').toggle(!isAnyConversationOpen);
        // Make sure poppers are positioned properly
        window.dispatchEvent(new Event('resize'));
        return this;
    }
}
const AppLoadingScreen = Whisper.View.extend({
    template: () => $('#app-loading-screen').html(),
    className: 'app-loading-screen',
    updateProgress(count) {
        if (count > 0) {
            const message = window.i18n('loadingMessages', [count.toString()]);
            this.$('.message').text(message);
        }
    },
    render_attributes: {
        message: window.i18n('loading'),
    },
});
Whisper.InboxView = Whisper.View.extend({
    template: () => $('#two-column').html(),
    className: 'Inbox',
    initialize(options = {}) {
        this.ready = false;
        this.render();
        this.conversation_stack = new ConversationStack({
            el: this.$('.conversation-stack'),
        });
        this.renderWhatsNew();
        Whisper.events.on('refreshConversation', ({ oldId, newId }) => {
            const convo = this.conversation_stack.lastConversation;
            if (convo && convo.get('id') === oldId) {
                this.conversation_stack.open(newId);
            }
        });
        // Close current opened conversation to reload the group information once
        // linked.
        Whisper.events.on('setupAsNewDevice', () => {
            this.conversation_stack.unload();
        });
        window.Whisper.events.on('showConversation', async (id, messageId, username) => {
            const conversation = await window.ConversationController.getOrCreateAndWait(id, 'private', { username });
            conversation.setMarkedUnread(false);
            const { openConversationExternal } = window.reduxActions.conversations;
            if (openConversationExternal) {
                openConversationExternal(conversation.id, messageId);
            }
            this.conversation_stack.open(conversation, messageId);
        });
        window.Whisper.events.on('loadingProgress', count => {
            const view = this.appLoadingScreen;
            if (view) {
                view.updateProgress(count);
            }
        });
        if (!options.initialLoadComplete) {
            this.appLoadingScreen = new AppLoadingScreen();
            this.appLoadingScreen.render();
            this.appLoadingScreen.$el.prependTo(this.el);
            this.startConnectionListener();
        }
        else {
            this.setupLeftPane();
        }
        Whisper.events.on('pack-install-failed', () => {
            (0, showToast_1.showToast)(ToastStickerPackInstallFailed_1.ToastStickerPackInstallFailed);
        });
    },
    render_attributes: {
        welcomeToSignal: window.i18n('welcomeToSignal'),
        // TODO DESKTOP-1451: add back the selectAContact message
        selectAContact: '',
    },
    events: {
        click: 'onClick',
    },
    renderWhatsNew() {
        if (this.whatsNewLink) {
            return;
        }
        const { showWhatsNewModal } = window.reduxActions.globalModals;
        this.whatsNewLink = new Whisper.ReactWrapperView({
            Component: window.Signal.Components.WhatsNewLink,
            props: {
                i18n: window.i18n,
                showWhatsNewModal,
            },
        });
        this.$('.whats-new-placeholder').append(this.whatsNewLink.el);
    },
    setupLeftPane() {
        if (this.leftPaneView) {
            return;
        }
        this.leftPaneView = new Whisper.ReactWrapperView({
            className: 'left-pane-wrapper',
            JSX: window.Signal.State.Roots.createLeftPane(window.reduxStore),
        });
        this.$('.left-pane-placeholder').replaceWith(this.leftPaneView.el);
    },
    startConnectionListener() {
        this.interval = setInterval(() => {
            const status = window.getSocketStatus();
            switch (status) {
                case 'CONNECTING':
                    break;
                case 'OPEN':
                    clearInterval(this.interval);
                    // if we've connected, we can wait for real empty event
                    this.interval = null;
                    break;
                case 'CLOSING':
                case 'CLOSED':
                    clearInterval(this.interval);
                    this.interval = null;
                    // if we failed to connect, we pretend we got an empty event
                    this.onEmpty();
                    break;
                default:
                    log.warn(`startConnectionListener: Found unexpected socket status ${status}; calling onEmpty() manually.`);
                    this.onEmpty();
                    break;
            }
        }, 1000);
    },
    onEmpty() {
        var _a;
        this.setupLeftPane();
        const view = this.appLoadingScreen;
        if (view) {
            this.appLoadingScreen = null;
            view.remove();
            const searchInput = document.querySelector('.LeftPaneSearchInput__input');
            (_a = searchInput === null || searchInput === void 0 ? void 0 : searchInput.focus) === null || _a === void 0 ? void 0 : _a.call(searchInput);
        }
    },
});
