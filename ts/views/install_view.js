"use strict";
// Copyright 2015-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const log = __importStar(require("../logging/log"));
const openLinkInWebBrowser_1 = require("../util/openLinkInWebBrowser");
const Errors_1 = require("../textsecure/Errors");
window.Whisper = window.Whisper || {};
const { Whisper } = window;
var Steps;
(function (Steps) {
    Steps[Steps["INSTALL_SIGNAL"] = 2] = "INSTALL_SIGNAL";
    Steps[Steps["SCAN_QR_CODE"] = 3] = "SCAN_QR_CODE";
    Steps[Steps["ENTER_NAME"] = 4] = "ENTER_NAME";
    Steps[Steps["PROGRESS_BAR"] = 5] = "PROGRESS_BAR";
    Steps["TOO_MANY_DEVICES"] = "TooManyDevices";
    Steps["NETWORK_ERROR"] = "NetworkError";
})(Steps || (Steps = {}));
const DEVICE_NAME_SELECTOR = 'input.device-name';
const CONNECTION_ERROR = -1;
const TOO_MANY_DEVICES = 411;
const TOO_OLD = 409;
Whisper.InstallView = Whisper.View.extend({
    template: () => $('#link-flow-template').html(),
    className: 'main full-screen-flow',
    events: {
        'click .try-again': 'connect',
        'click .second': 'shutdown',
        // the actual next step happens in confirmNumber() on submit form #link-phone
    },
    initialize(options = {}) {
        window.readyForUpdates();
        this.selectStep(Steps.SCAN_QR_CODE);
        this.connect();
        this.on('disconnected', this.reconnect);
        // Keep data around if it's a re-link, or the middle of a light import
        this.shouldRetainData =
            window.Signal.Util.Registration.everDone() || options.hasExistingData;
    },
    render_attributes() {
        let errorMessage;
        let errorButton = window.i18n('installTryAgain');
        let errorSecondButton = null;
        if (this.error) {
            if (this.error instanceof Errors_1.HTTPError &&
                this.error.code === TOO_MANY_DEVICES) {
                errorMessage = window.i18n('installTooManyDevices');
            }
            else if (this.error instanceof Errors_1.HTTPError &&
                this.error.code === TOO_OLD) {
                errorMessage = window.i18n('installTooOld');
                errorButton = window.i18n('upgrade');
                errorSecondButton = window.i18n('quit');
            }
            else if (this.error instanceof Errors_1.HTTPError &&
                this.error.code === CONNECTION_ERROR) {
                errorMessage = window.i18n('installConnectionFailed');
            }
            else if (this.error.message === 'websocket closed') {
                // AccountManager.registerSecondDevice uses this specific
                //   'websocket closed' error message
                errorMessage = window.i18n('installConnectionFailed');
            }
            return {
                isError: true,
                errorHeader: window.i18n('installErrorHeader'),
                errorMessage,
                errorButton,
                errorSecondButton,
            };
        }
        return {
            isStep3: this.step === Steps.SCAN_QR_CODE,
            linkYourPhone: window.i18n('linkYourPhone'),
            signalSettings: window.i18n('signalSettings'),
            linkedDevices: window.i18n('linkedDevices'),
            androidFinalStep: window.i18n('plusButton'),
            appleFinalStep: window.i18n('linkNewDevice'),
            isStep4: this.step === Steps.ENTER_NAME,
            chooseName: window.i18n('chooseDeviceName'),
            finishLinkingPhoneButton: window.i18n('finishLinkingPhone'),
            isStep5: this.step === Steps.PROGRESS_BAR,
            syncing: window.i18n('initialSync'),
        };
    },
    selectStep(step) {
        this.step = step;
        this.render();
    },
    shutdown() {
        window.shutdown();
    },
    async connect() {
        if (this.error instanceof Errors_1.HTTPError && this.error.code === TOO_OLD) {
            (0, openLinkInWebBrowser_1.openLinkInWebBrowser)('https://signal.org/download');
            return;
        }
        this.error = null;
        this.selectStep(Steps.SCAN_QR_CODE);
        this.clearQR();
        if (this.timeout) {
            clearTimeout(this.timeout);
            this.timeout = null;
        }
        const accountManager = window.getAccountManager();
        try {
            await accountManager.registerSecondDevice(this.setProvisioningUrl.bind(this), this.confirmNumber.bind(this));
        }
        catch (err) {
            this.handleDisconnect(err);
        }
    },
    handleDisconnect(error) {
        log.error('provisioning failed', error && error.stack ? error.stack : error);
        this.error = error;
        this.render();
        if (error.message === 'websocket closed') {
            this.trigger('disconnected');
        }
        else if (!(error instanceof Errors_1.HTTPError) ||
            (error.code !== CONNECTION_ERROR && error.code !== TOO_MANY_DEVICES)) {
            throw error;
        }
    },
    reconnect() {
        if (this.timeout) {
            clearTimeout(this.timeout);
            this.timeout = null;
        }
        this.timeout = setTimeout(this.connect.bind(this), 10000);
    },
    clearQR() {
        this.$('#qr img').remove();
        this.$('#qr canvas').remove();
        this.$('#qr .container').show();
        this.$('#qr').removeClass('ready');
    },
    setProvisioningUrl(url) {
        if ($('#qr').length === 0) {
            log.error('Did not find #qr element in the DOM!');
            return;
        }
        this.clearQR();
        this.$('#qr .container').hide();
        this.qr = new window.QRCode(this.$('#qr')[0]).makeCode(url);
        this.$('#qr').removeAttr('title');
        this.$('#qr').addClass('ready');
        this.$('#qr img').attr('alt', window.i18n('LinkScreen__scan-this-code'));
    },
    setDeviceNameDefault() {
        const deviceName = window.textsecure.storage.user.getDeviceName();
        this.$(DEVICE_NAME_SELECTOR).val(deviceName || window.getHostName());
        this.$(DEVICE_NAME_SELECTOR).focus();
    },
    confirmNumber() {
        window.removeSetupMenuItems();
        this.selectStep(Steps.ENTER_NAME);
        this.setDeviceNameDefault();
        return new Promise(resolve => {
            const onDeviceName = async (name) => {
                this.selectStep(Steps.PROGRESS_BAR);
                const finish = () => {
                    window.Signal.Util.postLinkExperience.start();
                    return resolve(name);
                };
                // Delete all data from database unless we're in the middle
                //   of a re-link, or we are finishing a light import. Without this,
                //   app restarts at certain times can cause weird things to happen,
                //   like data from a previous incomplete light import showing up
                //   after a new install.
                if (this.shouldRetainData) {
                    return finish();
                }
                try {
                    await window.textsecure.storage.protocol.removeAllData();
                }
                catch (error) {
                    log.error('confirmNumber: error clearing database', error && error.stack ? error.stack : error);
                }
                finally {
                    finish();
                }
            };
            if (window.CI) {
                onDeviceName(window.CI.deviceName);
                return;
            }
            // eslint-disable-next-line consistent-return
            this.$('#link-phone').submit((e) => {
                e.stopPropagation();
                e.preventDefault();
                let name = this.$(DEVICE_NAME_SELECTOR).val();
                name = name.replace(/\0/g, ''); // strip unicode null
                if (name.trim().length === 0) {
                    this.$(DEVICE_NAME_SELECTOR).focus();
                    return;
                }
                onDeviceName(name);
            });
        });
    },
});
