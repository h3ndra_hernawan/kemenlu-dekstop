"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationView = void 0;
/* eslint-disable camelcase */
const react_redux_1 = require("react-redux");
const lodash_1 = require("lodash");
const mustache_1 = require("mustache");
const Attachment_1 = require("../types/Attachment");
const Attachment = __importStar(require("../types/Attachment"));
const Stickers = __importStar(require("../types/Stickers"));
const MIME_1 = require("../types/MIME");
const sniffImageMimeType_1 = require("../util/sniffImageMimeType");
const assert_1 = require("../util/assert");
const url_1 = require("../util/url");
const enqueueReactionForSend_1 = require("../reactions/enqueueReactionForSend");
const addReportSpamJob_1 = require("../jobs/helpers/addReportSpamJob");
const reportSpamJobQueue_1 = require("../jobs/reportSpamJobQueue");
const whatTypeOfConversation_1 = require("../util/whatTypeOfConversation");
const findAndFormatContact_1 = require("../util/findAndFormatContact");
const Bytes = __importStar(require("../Bytes"));
const message_1 = require("../state/selectors/message");
const isMessageUnread_1 = require("../util/isMessageUnread");
const conversations_1 = require("../state/selectors/conversations");
const ConversationDetailsMembershipList_1 = require("../components/conversation/conversation-details/ConversationDetailsMembershipList");
const showSafetyNumberChangeDialog_1 = require("../shims/showSafetyNumberChangeDialog");
const LinkPreview = __importStar(require("../types/LinkPreview"));
const VisualAttachment = __importStar(require("../types/VisualAttachment"));
const log = __importStar(require("../logging/log"));
const createConversationView_1 = require("../state/roots/createConversationView");
const AttachmentToastType_1 = require("../types/AttachmentToastType");
const MessageReadStatus_1 = require("../messages/MessageReadStatus");
const protobuf_1 = require("../protobuf");
const ToastBlocked_1 = require("../components/ToastBlocked");
const ToastBlockedGroup_1 = require("../components/ToastBlockedGroup");
const ToastCannotMixImageAndNonImageAttachments_1 = require("../components/ToastCannotMixImageAndNonImageAttachments");
const ToastCannotStartGroupCall_1 = require("../components/ToastCannotStartGroupCall");
const ToastConversationArchived_1 = require("../components/ToastConversationArchived");
const ToastConversationMarkedUnread_1 = require("../components/ToastConversationMarkedUnread");
const ToastConversationUnarchived_1 = require("../components/ToastConversationUnarchived");
const ToastDangerousFileType_1 = require("../components/ToastDangerousFileType");
const ToastDeleteForEveryoneFailed_1 = require("../components/ToastDeleteForEveryoneFailed");
const ToastExpired_1 = require("../components/ToastExpired");
const ToastFileSaved_1 = require("../components/ToastFileSaved");
const ToastFileSize_1 = require("../components/ToastFileSize");
const ToastInvalidConversation_1 = require("../components/ToastInvalidConversation");
const ToastLeftGroup_1 = require("../components/ToastLeftGroup");
const ToastMaxAttachments_1 = require("../components/ToastMaxAttachments");
const ToastMessageBodyTooLong_1 = require("../components/ToastMessageBodyTooLong");
const ToastOneNonImageAtATime_1 = require("../components/ToastOneNonImageAtATime");
const ToastOriginalMessageNotFound_1 = require("../components/ToastOriginalMessageNotFound");
const ToastPinnedConversationsFull_1 = require("../components/ToastPinnedConversationsFull");
const ToastReactionFailed_1 = require("../components/ToastReactionFailed");
const ToastReportedSpamAndBlocked_1 = require("../components/ToastReportedSpamAndBlocked");
const ToastTapToViewExpiredIncoming_1 = require("../components/ToastTapToViewExpiredIncoming");
const ToastTapToViewExpiredOutgoing_1 = require("../components/ToastTapToViewExpiredOutgoing");
const ToastUnableToLoadAttachment_1 = require("../components/ToastUnableToLoadAttachment");
const handleImageAttachment_1 = require("../util/handleImageAttachment");
const copyGroupLink_1 = require("../util/copyGroupLink");
const deleteDraftAttachment_1 = require("../util/deleteDraftAttachment");
const markAllAsApproved_1 = require("../util/markAllAsApproved");
const markAllAsVerifiedDefault_1 = require("../util/markAllAsVerifiedDefault");
const retryMessageSend_1 = require("../util/retryMessageSend");
const dropNull_1 = require("../util/dropNull");
const fileToBytes_1 = require("../util/fileToBytes");
const isNotNil_1 = require("../util/isNotNil");
const MessageUpdater_1 = require("../services/MessageUpdater");
const openLinkInWebBrowser_1 = require("../util/openLinkInWebBrowser");
const resolveAttachmentDraftData_1 = require("../util/resolveAttachmentDraftData");
const showToast_1 = require("../util/showToast");
const viewSyncJobQueue_1 = require("../jobs/viewSyncJobQueue");
const viewedReceiptsJobQueue_1 = require("../jobs/viewedReceiptsJobQueue");
const FIVE_MINUTES = 1000 * 60 * 5;
const LINK_PREVIEW_TIMEOUT = 60 * 1000;
window.Whisper = window.Whisper || {};
const { Whisper } = window;
const { Message } = window.Signal.Types;
const { copyIntoTempDirectory, deleteTempFile, getAbsoluteAttachmentPath, getAbsoluteTempPath, loadAttachmentData, loadPreviewData, loadStickerData, openFileInFolder, readAttachmentData, saveAttachmentToDisk, upgradeMessageSchema, } = window.Signal.Migrations;
const { getOlderMessagesByConversation, getMessageMetricsForConversation, getMessageById, getMessagesBySentAt, getNewerMessagesByConversation, } = window.Signal.Data;
const MAX_MESSAGE_BODY_LENGTH = 64 * 1024;
class ConversationView extends window.Backbone.View {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor(...args) {
        super(...args);
        // Composing messages
        this.compositionApi = { current: undefined };
        this.excludedPreviewUrls = [];
        // Panel support
        this.panels = [];
        this.lazyUpdateVerified = (0, lodash_1.debounce)(this.model.updateVerified.bind(this.model), 1000 // one second
        );
        this.model.throttledGetProfiles =
            this.model.throttledGetProfiles ||
                (0, lodash_1.throttle)(this.model.getProfiles.bind(this.model), FIVE_MINUTES);
        this.debouncedMaybeGrabLinkPreview = (0, lodash_1.debounce)(this.maybeGrabLinkPreview.bind(this), 200);
        this.debouncedSaveDraft = (0, lodash_1.debounce)(this.saveDraft.bind(this), 200);
        // Events on Conversation model
        this.listenTo(this.model, 'destroy', this.stopListening);
        this.listenTo(this.model, 'newmessage', this.lazyUpdateVerified);
        // These are triggered by InboxView
        this.listenTo(this.model, 'opened', this.onOpened);
        this.listenTo(this.model, 'scroll-to-message', this.scrollToMessage);
        this.listenTo(this.model, 'unload', (reason) => this.unload(`model trigger - ${reason}`));
        // These are triggered by background.ts for keyboard handling
        this.listenTo(this.model, 'focus-composer', this.focusMessageField);
        this.listenTo(this.model, 'open-all-media', this.showAllMedia);
        this.listenTo(this.model, 'escape-pressed', this.resetPanel);
        this.listenTo(this.model, 'show-message-details', this.showMessageDetail);
        this.listenTo(this.model, 'show-contact-modal', this.showContactModal);
        this.listenTo(this.model, 'toggle-reply', (messageId) => {
            const target = this.quote || !messageId ? null : messageId;
            this.setQuoteMessage(target);
        });
        this.listenTo(this.model, 'save-attachment', this.downloadAttachmentWrapper);
        this.listenTo(this.model, 'delete-message', this.deleteMessage);
        this.listenTo(this.model, 'remove-link-review', this.removeLinkPreview);
        this.listenTo(this.model, 'remove-all-draft-attachments', this.clearAttachments);
        this.render();
        this.setupConversationView();
        this.updateAttachmentsView();
    }
    events() {
        return {
            drop: 'onDrop',
            paste: 'onPaste',
        };
    }
    // We need this ignore because the backbone types really want this to be a string
    //   property, but the property isn't set until after super() is run, meaning that this
    //   classname wouldn't be applied when Backbone creates our el.
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    className() {
        return 'conversation';
    }
    // Same situation as className().
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    id() {
        return `conversation-${this.model.cid}`;
    }
    // Backbone.View<ConversationModel> is demanded as the return type here, and we can't
    //   satisfy it because of the above difference in signature: className is a function
    //   when it should be a plain string property.
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    render() {
        const template = $('#conversation').html();
        this.$el.html((0, mustache_1.render)(template, {}));
        return this;
    }
    setMuteExpiration(ms = 0) {
        this.model.setMuteExpiration(ms >= Number.MAX_SAFE_INTEGER ? ms : Date.now() + ms);
    }
    setPin(value) {
        if (value) {
            const pinnedConversationIds = window.storage.get('pinnedConversationIds', new Array());
            if (pinnedConversationIds.length >= 4) {
                (0, showToast_1.showToast)(ToastPinnedConversationsFull_1.ToastPinnedConversationsFull);
                return;
            }
            this.model.pin();
        }
        else {
            this.model.unpin();
        }
    }
    setupConversationView() {
        // setupHeader
        const conversationHeaderProps = {
            id: this.model.id,
            onSetDisappearingMessages: (seconds) => this.setDisappearingMessages(seconds),
            onDeleteMessages: () => this.destroyMessages(),
            onSearchInConversation: () => {
                const { searchInConversation } = window.reduxActions.search;
                searchInConversation(this.model.id);
            },
            onSetMuteNotifications: this.setMuteExpiration.bind(this),
            onSetPin: this.setPin.bind(this),
            // These are view only and don't update the Conversation model, so they
            //   need a manual update call.
            onOutgoingAudioCallInConversation: this.onOutgoingAudioCallInConversation.bind(this),
            onOutgoingVideoCallInConversation: this.onOutgoingVideoCallInConversation.bind(this),
            onShowConversationDetails: () => {
                this.showConversationDetails();
            },
            onShowAllMedia: () => {
                this.showAllMedia();
            },
            onShowGroupMembers: () => {
                this.showGV1Members();
            },
            onGoBack: () => {
                this.resetPanel();
            },
            onArchive: () => {
                this.model.setArchived(true);
                this.model.trigger('unload', 'archive');
                (0, showToast_1.showToast)(ToastConversationArchived_1.ToastConversationArchived, {
                    undo: () => {
                        this.model.setArchived(false);
                        this.openConversation(this.model.get('id'));
                    },
                });
            },
            onMarkUnread: () => {
                this.model.setMarkedUnread(true);
                (0, showToast_1.showToast)(ToastConversationMarkedUnread_1.ToastConversationMarkedUnread);
            },
            onMoveToInbox: () => {
                this.model.setArchived(false);
                (0, showToast_1.showToast)(ToastConversationUnarchived_1.ToastConversationUnarchived);
            },
        };
        window.reduxActions.conversations.setSelectedConversationHeaderTitle();
        // setupTimeline
        const messageRequestEnum = protobuf_1.SignalService.SyncMessage.MessageRequestResponse.Type;
        const contactSupport = () => {
            const baseUrl = 'https://support.signal.org/hc/LOCALE/requests/new?desktop&chat_refreshed';
            const locale = window.getLocale();
            const supportLocale = window.Signal.Util.mapToSupportLocale(locale);
            const url = baseUrl.replace('LOCALE', supportLocale);
            (0, openLinkInWebBrowser_1.openLinkInWebBrowser)(url);
        };
        const learnMoreAboutDeliveryIssue = () => {
            (0, openLinkInWebBrowser_1.openLinkInWebBrowser)('https://support.signal.org/hc/articles/4404859745690');
        };
        const scrollToQuotedMessage = async (options) => {
            const { authorId, sentAt } = options;
            const conversationId = this.model.id;
            const messages = await getMessagesBySentAt(sentAt, {
                MessageCollection: Whisper.MessageCollection,
            });
            const message = messages.find(item => Boolean(item.get('conversationId') === conversationId &&
                authorId &&
                item.getContactId() === authorId));
            if (!message) {
                (0, showToast_1.showToast)(ToastOriginalMessageNotFound_1.ToastOriginalMessageNotFound);
                return;
            }
            this.scrollToMessage(message.id);
        };
        const loadOlderMessages = async (oldestMessageId) => {
            const { messagesAdded, setMessagesLoading, repairOldestMessage } = window.reduxActions.conversations;
            const conversationId = this.model.id;
            setMessagesLoading(conversationId, true);
            const finish = this.setInProgressFetch();
            try {
                const message = await getMessageById(oldestMessageId, {
                    Message: Whisper.Message,
                });
                if (!message) {
                    throw new Error(`loadOlderMessages: failed to load message ${oldestMessageId}`);
                }
                const receivedAt = message.get('received_at');
                const sentAt = message.get('sent_at');
                const models = await getOlderMessagesByConversation(conversationId, {
                    receivedAt,
                    sentAt,
                    messageId: oldestMessageId,
                    limit: 30,
                    MessageCollection: Whisper.MessageCollection,
                });
                if (models.length < 1) {
                    log.warn('loadOlderMessages: requested, but loaded no messages');
                    repairOldestMessage(conversationId);
                    return;
                }
                const cleaned = await this.cleanModels(models);
                const isNewMessage = false;
                messagesAdded(this.model.id, cleaned.map((messageModel) => (Object.assign({}, messageModel.attributes))), isNewMessage, window.isActive());
            }
            catch (error) {
                setMessagesLoading(conversationId, true);
                throw error;
            }
            finally {
                finish();
            }
        };
        const loadNewerMessages = async (newestMessageId) => {
            const { messagesAdded, setMessagesLoading, repairNewestMessage } = window.reduxActions.conversations;
            const conversationId = this.model.id;
            setMessagesLoading(conversationId, true);
            const finish = this.setInProgressFetch();
            try {
                const message = await getMessageById(newestMessageId, {
                    Message: Whisper.Message,
                });
                if (!message) {
                    throw new Error(`loadNewerMessages: failed to load message ${newestMessageId}`);
                }
                const receivedAt = message.get('received_at');
                const sentAt = message.get('sent_at');
                const models = await getNewerMessagesByConversation(conversationId, {
                    receivedAt,
                    sentAt,
                    limit: 30,
                    MessageCollection: Whisper.MessageCollection,
                });
                if (models.length < 1) {
                    log.warn('loadNewerMessages: requested, but loaded no messages');
                    repairNewestMessage(conversationId);
                    return;
                }
                const cleaned = await this.cleanModels(models);
                const isNewMessage = false;
                messagesAdded(conversationId, cleaned.map((messageModel) => (Object.assign({}, messageModel.attributes))), isNewMessage, window.isActive());
            }
            catch (error) {
                setMessagesLoading(conversationId, false);
                throw error;
            }
            finally {
                finish();
            }
        };
        const markMessageRead = async (messageId) => {
            if (!window.isActive()) {
                return;
            }
            const message = await getMessageById(messageId, {
                Message: Whisper.Message,
            });
            if (!message) {
                throw new Error(`markMessageRead: failed to load message ${messageId}`);
            }
            await this.model.markRead(message.get('received_at'));
        };
        const createMessageRequestResponseHandler = (name, enumValue) => conversationId => {
            const conversation = window.ConversationController.get(conversationId);
            if (!conversation) {
                log.error(`createMessageRequestResponseHandler: Expected a conversation to be found in ${name}. Doing nothing`);
                return;
            }
            this.syncMessageRequestResponse(name, conversation, enumValue);
        };
        const timelineProps = Object.assign(Object.assign({ id: this.model.id }, this.getMessageActions()), { acknowledgeGroupMemberNameCollisions: (groupNameCollisions) => {
                this.model.acknowledgeGroupMemberNameCollisions(groupNameCollisions);
            }, contactSupport,
            learnMoreAboutDeliveryIssue,
            loadNewerMessages, loadNewestMessages: this.loadNewestMessages.bind(this), loadAndScroll: this.loadAndScroll.bind(this), loadOlderMessages,
            markMessageRead, onBlock: createMessageRequestResponseHandler('onBlock', messageRequestEnum.BLOCK), onBlockAndReportSpam: (conversationId) => {
                const conversation = window.ConversationController.get(conversationId);
                if (!conversation) {
                    log.error(`onBlockAndReportSpam: Expected a conversation to be found for ${conversationId}. Doing nothing.`);
                    return;
                }
                this.blockAndReportSpam(conversation);
            }, onDelete: createMessageRequestResponseHandler('onDelete', messageRequestEnum.DELETE), onUnblock: createMessageRequestResponseHandler('onUnblock', messageRequestEnum.ACCEPT), removeMember: (conversationId) => {
                this.longRunningTaskWrapper({
                    name: 'removeMember',
                    task: () => this.model.removeFromGroupV2(conversationId),
                });
            }, scrollToQuotedMessage, unblurAvatar: () => {
                this.model.unblurAvatar();
            }, updateSharedGroups: () => { var _a, _b; return (_b = (_a = this.model).throttledUpdateSharedGroups) === null || _b === void 0 ? void 0 : _b.call(_a); } });
        // setupCompositionArea
        window.reduxActions.composer.resetComposer();
        const compositionAreaProps = {
            id: this.model.id,
            compositionApi: this.compositionApi,
            onClickAddPack: () => this.showStickerManager(),
            onPickSticker: (packId, stickerId) => this.sendStickerMessage({ packId, stickerId }),
            onEditorStateChange: (msg, bodyRanges, caretLocation) => this.onEditorStateChange(msg, bodyRanges, caretLocation),
            onTextTooLong: () => (0, showToast_1.showToast)(ToastMessageBodyTooLong_1.ToastMessageBodyTooLong),
            getQuotedMessage: () => this.model.get('quotedMessageId'),
            clearQuotedMessage: () => this.setQuoteMessage(null),
            onAccept: () => {
                this.syncMessageRequestResponse('onAccept', this.model, messageRequestEnum.ACCEPT);
            },
            onBlock: () => {
                this.syncMessageRequestResponse('onBlock', this.model, messageRequestEnum.BLOCK);
            },
            onUnblock: () => {
                this.syncMessageRequestResponse('onUnblock', this.model, messageRequestEnum.ACCEPT);
            },
            onDelete: () => {
                this.syncMessageRequestResponse('onDelete', this.model, messageRequestEnum.DELETE);
            },
            onBlockAndReportSpam: () => {
                this.blockAndReportSpam(this.model);
            },
            onStartGroupMigration: () => this.startMigrationToGV2(),
            onCancelJoinRequest: async () => {
                await window.showConfirmationDialog({
                    message: window.i18n('GroupV2--join--cancel-request-to-join--confirmation'),
                    okText: window.i18n('GroupV2--join--cancel-request-to-join--yes'),
                    cancelText: window.i18n('GroupV2--join--cancel-request-to-join--no'),
                    resolve: () => {
                        this.longRunningTaskWrapper({
                            name: 'onCancelJoinRequest',
                            task: async () => this.model.cancelJoinRequest(),
                        });
                    },
                });
            },
            onClearAttachments: this.clearAttachments.bind(this),
            onSelectMediaQuality: (isHQ) => {
                window.reduxActions.composer.setMediaQualitySetting(isHQ);
            },
            handleClickQuotedMessage: (id) => this.scrollToMessage(id),
            onCloseLinkPreview: () => {
                this.disableLinkPreviews = true;
                this.removeLinkPreview();
            },
            openConversation: this.openConversation.bind(this),
            onSendMessage: ({ draftAttachments, mentions = [], message = '', timestamp, voiceNoteAttachment, }) => {
                this.sendMessage(message, mentions, {
                    draftAttachments,
                    timestamp,
                    voiceNoteAttachment,
                });
            },
        };
        // createConversationView root
        const JSX = (0, createConversationView_1.createConversationView)(window.reduxStore, {
            compositionAreaProps,
            conversationHeaderProps,
            timelineProps,
        });
        this.conversationView = new Whisper.ReactWrapperView({ JSX });
        this.$('.ConversationView__template').append(this.conversationView.el);
    }
    async onOutgoingVideoCallInConversation() {
        log.info('onOutgoingVideoCallInConversation: about to start a video call');
        const isVideoCall = true;
        if (this.model.get('announcementsOnly') && !this.model.areWeAdmin()) {
            (0, showToast_1.showToast)(ToastCannotStartGroupCall_1.ToastCannotStartGroupCall);
            return;
        }
        if (await this.isCallSafe()) {
            log.info('onOutgoingVideoCallInConversation: call is deemed "safe". Making call');
            await window.Signal.Services.calling.startCallingLobby(this.model.id, isVideoCall);
            log.info('onOutgoingVideoCallInConversation: started the call');
        }
        else {
            log.info('onOutgoingVideoCallInConversation: call is deemed "unsafe". Stopping');
        }
    }
    async onOutgoingAudioCallInConversation() {
        log.info('onOutgoingAudioCallInConversation: about to start an audio call');
        const isVideoCall = false;
        if (await this.isCallSafe()) {
            log.info('onOutgoingAudioCallInConversation: call is deemed "safe". Making call');
            await window.Signal.Services.calling.startCallingLobby(this.model.id, isVideoCall);
            log.info('onOutgoingAudioCallInConversation: started the call');
        }
        else {
            log.info('onOutgoingAudioCallInConversation: call is deemed "unsafe". Stopping');
        }
    }
    async longRunningTaskWrapper({ name, task, }) {
        const idForLogging = this.model.idForLogging();
        return window.Signal.Util.longRunningTaskWrapper({
            name,
            idForLogging,
            task,
        });
    }
    getMessageActions() {
        const reactToMessage = async (messageId, reaction) => {
            const { emoji, remove } = reaction;
            try {
                await (0, enqueueReactionForSend_1.enqueueReactionForSend)({
                    messageId,
                    emoji,
                    remove,
                });
            }
            catch (error) {
                log.error('Error sending reaction', error, messageId, reaction);
                (0, showToast_1.showToast)(ToastReactionFailed_1.ToastReactionFailed);
            }
        };
        const replyToMessage = (messageId) => {
            this.setQuoteMessage(messageId);
        };
        const retrySend = retryMessageSend_1.retryMessageSend;
        const deleteMessage = (messageId) => {
            this.deleteMessage(messageId);
        };
        const deleteMessageForEveryone = (messageId) => {
            this.deleteMessageForEveryone(messageId);
        };
        const showMessageDetail = (messageId) => {
            this.showMessageDetail(messageId);
        };
        const showContactModal = (contactId) => {
            this.showContactModal(contactId);
        };
        const openConversation = (conversationId, messageId) => {
            this.openConversation(conversationId, messageId);
        };
        const showContactDetail = (options) => {
            this.showContactDetail(options);
        };
        const kickOffAttachmentDownload = async (options) => {
            const message = window.MessageController.getById(options.messageId);
            if (!message) {
                throw new Error(`kickOffAttachmentDownload: Message ${options.messageId} missing!`);
            }
            await message.queueAttachmentDownloads();
        };
        const markAttachmentAsCorrupted = (options) => {
            const message = window.MessageController.getById(options.messageId);
            if (!message) {
                throw new Error(`markAttachmentAsCorrupted: Message ${options.messageId} missing!`);
            }
            message.markAttachmentAsCorrupted(options.attachment);
        };
        const onMarkViewed = (messageId) => {
            const message = window.MessageController.getById(messageId);
            if (!message) {
                throw new Error(`onMarkViewed: Message ${messageId} missing!`);
            }
            if (message.get('readStatus') === MessageReadStatus_1.ReadStatus.Viewed) {
                return;
            }
            const senderE164 = message.get('source');
            const senderUuid = message.get('sourceUuid');
            const timestamp = message.get('sent_at');
            message.set((0, MessageUpdater_1.markViewed)(message.attributes, Date.now()));
            if ((0, message_1.isIncoming)(message.attributes)) {
                viewedReceiptsJobQueue_1.viewedReceiptsJobQueue.add({
                    viewedReceipt: {
                        messageId,
                        senderE164,
                        senderUuid,
                        timestamp,
                    },
                });
            }
            viewSyncJobQueue_1.viewSyncJobQueue.add({
                viewSyncs: [
                    {
                        messageId,
                        senderE164,
                        senderUuid,
                        timestamp,
                    },
                ],
            });
        };
        const showVisualAttachment = (options) => {
            this.showLightbox(options);
        };
        const downloadAttachment = (options) => {
            this.downloadAttachment(options);
        };
        const displayTapToViewMessage = (messageId) => this.displayTapToViewMessage(messageId);
        const showIdentity = (conversationId) => {
            this.showSafetyNumber(conversationId);
        };
        const openLink = openLinkInWebBrowser_1.openLinkInWebBrowser;
        const downloadNewVersion = () => {
            (0, openLinkInWebBrowser_1.openLinkInWebBrowser)('https://signal.org/download');
        };
        const showSafetyNumber = (contactId) => {
            this.showSafetyNumber(contactId);
        };
        const showExpiredIncomingTapToViewToast = () => {
            log.info('Showing expired tap-to-view toast for an incoming message');
            (0, showToast_1.showToast)(ToastTapToViewExpiredIncoming_1.ToastTapToViewExpiredIncoming);
        };
        const showExpiredOutgoingTapToViewToast = () => {
            log.info('Showing expired tap-to-view toast for an outgoing message');
            (0, showToast_1.showToast)(ToastTapToViewExpiredOutgoing_1.ToastTapToViewExpiredOutgoing);
        };
        const showForwardMessageModal = this.showForwardMessageModal.bind(this);
        return {
            deleteMessage,
            deleteMessageForEveryone,
            displayTapToViewMessage,
            downloadAttachment,
            downloadNewVersion,
            kickOffAttachmentDownload,
            markAttachmentAsCorrupted,
            markViewed: onMarkViewed,
            openConversation,
            openLink,
            reactToMessage,
            replyToMessage,
            retrySend,
            showContactDetail,
            showContactModal,
            showSafetyNumber,
            showExpiredIncomingTapToViewToast,
            showExpiredOutgoingTapToViewToast,
            showForwardMessageModal,
            showIdentity,
            showMessageDetail,
            showVisualAttachment,
        };
    }
    async cleanModels(collection) {
        const result = collection
            .filter((message) => Boolean(message.id))
            .map((message) => window.MessageController.register(message.id, message));
        const eliminated = collection.length - result.length;
        if (eliminated > 0) {
            log.warn(`cleanModels: Eliminated ${eliminated} messages without an id`);
        }
        for (let max = result.length, i = 0; i < max; i += 1) {
            const message = result[i];
            const { attributes } = message;
            const { schemaVersion } = attributes;
            if (schemaVersion < Message.VERSION_NEEDED_FOR_DISPLAY) {
                // Yep, we really do want to wait for each of these
                // eslint-disable-next-line no-await-in-loop
                const upgradedMessage = await upgradeMessageSchema(attributes);
                message.set(upgradedMessage);
                // eslint-disable-next-line no-await-in-loop
                await window.Signal.Data.saveMessage(upgradedMessage);
            }
        }
        return result;
    }
    async scrollToMessage(messageId) {
        const message = await getMessageById(messageId, {
            Message: Whisper.Message,
        });
        if (!message) {
            throw new Error(`scrollToMessage: failed to load message ${messageId}`);
        }
        const state = window.reduxStore.getState();
        let isInMemory = true;
        if (!window.MessageController.getById(messageId)) {
            isInMemory = false;
        }
        // Message might be in memory, but not in the redux anymore because
        // we call `messageReset()` in `loadAndScroll()`.
        const messagesByConversation = (0, conversations_1.getMessagesByConversation)(state)[this.model.id];
        if (!(messagesByConversation === null || messagesByConversation === void 0 ? void 0 : messagesByConversation.messageIds.includes(messageId))) {
            isInMemory = false;
        }
        if (isInMemory) {
            const { scrollToMessage } = window.reduxActions.conversations;
            scrollToMessage(this.model.id, messageId);
            return;
        }
        this.loadAndScroll(messageId);
    }
    setInProgressFetch() {
        let resolvePromise;
        this.model.inProgressFetch = new Promise(resolve => {
            resolvePromise = resolve;
        });
        const finish = () => {
            resolvePromise();
            this.model.inProgressFetch = undefined;
        };
        return finish;
    }
    async loadAndScroll(messageId, options) {
        const { messagesReset, setMessagesLoading } = window.reduxActions.conversations;
        const conversationId = this.model.id;
        setMessagesLoading(conversationId, true);
        const finish = this.setInProgressFetch();
        try {
            const message = await getMessageById(messageId, {
                Message: Whisper.Message,
            });
            if (!message) {
                throw new Error(`loadMoreAndScroll: failed to load message ${messageId}`);
            }
            const receivedAt = message.get('received_at');
            const sentAt = message.get('sent_at');
            const older = await getOlderMessagesByConversation(conversationId, {
                limit: 30,
                receivedAt,
                sentAt,
                messageId,
                MessageCollection: Whisper.MessageCollection,
            });
            const newer = await getNewerMessagesByConversation(conversationId, {
                limit: 30,
                receivedAt,
                sentAt,
                MessageCollection: Whisper.MessageCollection,
            });
            const metrics = await getMessageMetricsForConversation(conversationId);
            const all = [...older.models, message, ...newer.models];
            const cleaned = await this.cleanModels(all);
            const scrollToMessageId = options && options.disableScroll ? undefined : messageId;
            messagesReset(conversationId, cleaned.map((messageModel) => (Object.assign({}, messageModel.attributes))), metrics, scrollToMessageId);
        }
        catch (error) {
            setMessagesLoading(conversationId, false);
            throw error;
        }
        finally {
            finish();
        }
    }
    async loadNewestMessages(newestMessageId, setFocus) {
        const { messagesReset, setMessagesLoading } = window.reduxActions.conversations;
        const conversationId = this.model.id;
        setMessagesLoading(conversationId, true);
        const finish = this.setInProgressFetch();
        try {
            let scrollToLatestUnread = true;
            if (newestMessageId) {
                const newestInMemoryMessage = await getMessageById(newestMessageId, {
                    Message: Whisper.Message,
                });
                if (newestInMemoryMessage) {
                    // If newest in-memory message is unread, scrolling down would mean going to
                    //   the very bottom, not the oldest unread.
                    if ((0, isMessageUnread_1.isMessageUnread)(newestInMemoryMessage.attributes)) {
                        scrollToLatestUnread = false;
                    }
                }
                else {
                    log.warn(`loadNewestMessages: did not find message ${newestMessageId}`);
                }
            }
            const metrics = await getMessageMetricsForConversation(conversationId);
            // If this is a message request that has not yet been accepted, we always show the
            //   oldest messages, to ensure that the ConversationHero is shown. We don't want to
            //   scroll directly to the oldest message, because that could scroll the hero off
            //   the screen.
            if (!newestMessageId && !this.model.getAccepted() && metrics.oldest) {
                this.loadAndScroll(metrics.oldest.id, { disableScroll: true });
                return;
            }
            if (scrollToLatestUnread && metrics.oldestUnread) {
                this.loadAndScroll(metrics.oldestUnread.id, {
                    disableScroll: !setFocus,
                });
                return;
            }
            const messages = await getOlderMessagesByConversation(conversationId, {
                limit: 30,
                MessageCollection: Whisper.MessageCollection,
            });
            const cleaned = await this.cleanModels(messages);
            const scrollToMessageId = setFocus && metrics.newest ? metrics.newest.id : undefined;
            // Because our `getOlderMessages` fetch above didn't specify a receivedAt, we got
            //   the most recent 30 messages in the conversation. If it has a conflict with
            //   metrics, fetched a bit before, that's likely a race condition. So we tell our
            //   reducer to trust the message set we just fetched for determining if we have
            //   the newest message loaded.
            const unboundedFetch = true;
            messagesReset(conversationId, cleaned.map((messageModel) => (Object.assign({}, messageModel.attributes))), metrics, scrollToMessageId, unboundedFetch);
        }
        catch (error) {
            setMessagesLoading(conversationId, false);
            throw error;
        }
        finally {
            finish();
        }
    }
    async startMigrationToGV2() {
        const logId = this.model.idForLogging();
        if (!(0, whatTypeOfConversation_1.isGroupV1)(this.model.attributes)) {
            throw new Error(`startMigrationToGV2/${logId}: Cannot start, not a GroupV1 group`);
        }
        const onClose = () => {
            if (this.migrationDialog) {
                this.migrationDialog.remove();
                this.migrationDialog = undefined;
            }
        };
        onClose();
        const migrate = () => {
            onClose();
            this.longRunningTaskWrapper({
                name: 'initiateMigrationToGroupV2',
                task: () => window.Signal.Groups.initiateMigrationToGroupV2(this.model),
            });
        };
        // Note: this call will throw if, after generating member lists, we are no longer a
        //   member or are in the pending member list.
        const { droppedGV2MemberIds, pendingMembersV2 } = await this.longRunningTaskWrapper({
            name: 'getGroupMigrationMembers',
            task: () => window.Signal.Groups.getGroupMigrationMembers(this.model),
        });
        const invitedMemberIds = pendingMembersV2.map((item) => item.uuid);
        this.migrationDialog = new Whisper.ReactWrapperView({
            className: 'group-v1-migration-wrapper',
            JSX: window.Signal.State.Roots.createGroupV1MigrationModal(window.reduxStore, {
                areWeInvited: false,
                droppedMemberIds: droppedGV2MemberIds,
                hasMigrated: false,
                invitedMemberIds,
                migrate,
                onClose,
            }),
        });
    }
    // TODO DESKTOP-2426
    async processAttachments(files) {
        const { addAttachment, addPendingAttachment, processAttachments, removeAttachment, } = window.reduxActions.composer;
        await processAttachments({
            addAttachment,
            addPendingAttachment,
            conversationId: this.model.id,
            draftAttachments: this.model.get('draftAttachments') || [],
            files,
            onShowToast: (toastType) => {
                if (toastType === AttachmentToastType_1.AttachmentToastType.ToastFileSize) {
                    (0, showToast_1.showToast)(ToastFileSize_1.ToastFileSize, {
                        limit: 100,
                        units: 'MB',
                    });
                }
                else if (toastType === AttachmentToastType_1.AttachmentToastType.ToastDangerousFileType) {
                    (0, showToast_1.showToast)(ToastDangerousFileType_1.ToastDangerousFileType);
                }
                else if (toastType === AttachmentToastType_1.AttachmentToastType.ToastMaxAttachments) {
                    (0, showToast_1.showToast)(ToastMaxAttachments_1.ToastMaxAttachments);
                }
                else if (toastType === AttachmentToastType_1.AttachmentToastType.ToastOneNonImageAtATime) {
                    (0, showToast_1.showToast)(ToastOneNonImageAtATime_1.ToastOneNonImageAtATime);
                }
                else if (toastType ===
                    AttachmentToastType_1.AttachmentToastType.ToastCannotMixImageAndNonImageAttachments) {
                    (0, showToast_1.showToast)(ToastCannotMixImageAndNonImageAttachments_1.ToastCannotMixImageAndNonImageAttachments);
                }
                else if (toastType === AttachmentToastType_1.AttachmentToastType.ToastUnableToLoadAttachment) {
                    (0, showToast_1.showToast)(ToastUnableToLoadAttachment_1.ToastUnableToLoadAttachment);
                }
            },
            removeAttachment,
        });
    }
    unload(reason) {
        var _a;
        log.info('unloading conversation', this.model.idForLogging(), 'due to:', reason);
        const { conversationUnloaded } = window.reduxActions.conversations;
        if (conversationUnloaded) {
            conversationUnloaded(this.model.id);
        }
        if (this.model.get('draftChanged')) {
            if (this.model.hasDraft()) {
                const now = Date.now();
                const active_at = this.model.get('active_at') || now;
                this.model.set({
                    active_at,
                    draftChanged: false,
                    draftTimestamp: now,
                    timestamp: now,
                });
            }
            else {
                this.model.set({
                    draftChanged: false,
                    draftTimestamp: null,
                });
            }
            // We don't wait here; we need to take down the view
            this.saveModel();
            this.model.updateLastMessage();
        }
        (_a = this.conversationView) === null || _a === void 0 ? void 0 : _a.remove();
        if (this.contactModalView) {
            this.contactModalView.remove();
        }
        if (this.stickerPreviewModalView) {
            this.stickerPreviewModalView.remove();
        }
        if (this.lightboxView) {
            this.lightboxView.remove();
        }
        if (this.panels && this.panels.length) {
            for (let i = 0, max = this.panels.length; i < max; i += 1) {
                const panel = this.panels[i];
                panel.remove();
            }
            window.reduxActions.conversations.setSelectedConversationPanelDepth(0);
        }
        this.removeLinkPreview();
        this.disableLinkPreviews = true;
        this.remove();
    }
    async onDrop(e) {
        if (!e.originalEvent) {
            return;
        }
        const event = e.originalEvent;
        if (!event.dataTransfer) {
            return;
        }
        if (event.dataTransfer.types[0] !== 'Files') {
            return;
        }
        e.stopPropagation();
        e.preventDefault();
        const { files } = event.dataTransfer;
        this.processAttachments(Array.from(files));
    }
    onPaste(e) {
        if (!e.originalEvent) {
            return;
        }
        const event = e.originalEvent;
        if (!event.clipboardData) {
            return;
        }
        const { items } = event.clipboardData;
        const anyImages = [...items].some(item => item.type.split('/')[0] === 'image');
        if (!anyImages) {
            return;
        }
        e.stopPropagation();
        e.preventDefault();
        const files = [];
        for (let i = 0; i < items.length; i += 1) {
            if (items[i].type.split('/')[0] === 'image') {
                const file = items[i].getAsFile();
                if (file) {
                    files.push(file);
                }
            }
        }
        this.processAttachments(files);
    }
    syncMessageRequestResponse(name, model, messageRequestType) {
        return this.longRunningTaskWrapper({
            name,
            task: model.syncMessageRequestResponse.bind(model, messageRequestType),
        });
    }
    blockAndReportSpam(model) {
        const messageRequestEnum = protobuf_1.SignalService.SyncMessage.MessageRequestResponse.Type;
        return this.longRunningTaskWrapper({
            name: 'blockAndReportSpam',
            task: async () => {
                await Promise.all([
                    model.syncMessageRequestResponse(messageRequestEnum.BLOCK),
                    (0, addReportSpamJob_1.addReportSpamJob)({
                        conversation: model.format(),
                        getMessageServerGuidsForSpam: window.Signal.Data.getMessageServerGuidsForSpam,
                        jobQueue: reportSpamJobQueue_1.reportSpamJobQueue,
                    }),
                ]);
                (0, showToast_1.showToast)(ToastReportedSpamAndBlocked_1.ToastReportedSpamAndBlocked);
            },
        });
    }
    async saveModel() {
        window.Signal.Data.updateConversation(this.model.attributes);
    }
    async clearAttachments() {
        const draftAttachments = this.model.get('draftAttachments') || [];
        this.model.set({
            draftAttachments: [],
            draftChanged: true,
        });
        this.updateAttachmentsView();
        // We're fine doing this all at once; at most it should be 32 attachments
        await Promise.all([
            this.saveModel(),
            Promise.all(draftAttachments.map(attachment => (0, deleteDraftAttachment_1.deleteDraftAttachment)(attachment))),
        ]);
    }
    hasFiles(options) {
        const draftAttachments = this.model.get('draftAttachments') || [];
        if (options.includePending) {
            return draftAttachments.length > 0;
        }
        return draftAttachments.some(item => !item.pending);
    }
    updateAttachmentsView() {
        const draftAttachments = this.model.get('draftAttachments') || [];
        window.reduxActions.composer.replaceAttachments(this.model.get('id'), draftAttachments);
        if (this.hasFiles({ includePending: true })) {
            this.removeLinkPreview();
        }
    }
    async onOpened(messageId) {
        if (messageId) {
            const message = await getMessageById(messageId, {
                Message: Whisper.Message,
            });
            if (message) {
                this.loadAndScroll(messageId);
                return;
            }
            log.warn(`onOpened: Did not find message ${messageId}`);
        }
        const { retryPlaceholders } = window.Signal.Services;
        if (retryPlaceholders) {
            await retryPlaceholders.findByConversationAndMarkOpened(this.model.id);
        }
        this.loadNewestMessages(undefined, undefined);
        this.model.updateLastMessage();
        this.focusMessageField();
        const quotedMessageId = this.model.get('quotedMessageId');
        if (quotedMessageId) {
            this.setQuoteMessage(quotedMessageId);
        }
        this.model.fetchLatestGroupV2Data();
        (0, assert_1.strictAssert)(this.model.throttledMaybeMigrateV1Group !== undefined, 'Conversation model should be initialized');
        this.model.throttledMaybeMigrateV1Group();
        (0, assert_1.strictAssert)(this.model.throttledFetchSMSOnlyUUID !== undefined, 'Conversation model should be initialized');
        this.model.throttledFetchSMSOnlyUUID();
        (0, assert_1.strictAssert)(this.model.throttledGetProfiles !== undefined, 'Conversation model should be initialized');
        await this.model.throttledGetProfiles();
        this.model.updateVerified();
    }
    async showForwardMessageModal(messageId) {
        const messageFromCache = window.MessageController.getById(messageId);
        if (!messageFromCache) {
            log.info('showForwardMessageModal: Fetching message from database');
        }
        const message = messageFromCache ||
            (await window.Signal.Data.getMessageById(messageId, {
                Message: window.Whisper.Message,
            }));
        if (!message) {
            throw new Error(`showForwardMessageModal: Message ${messageId} missing!`);
        }
        const attachments = (0, message_1.getAttachmentsForMessage)(message.attributes);
        const draftAttachments = attachments
            .map((item) => {
            var _a;
            const { path } = item;
            if (!path) {
                return null;
            }
            return Object.assign(Object.assign({}, item), { path, pending: false, screenshotPath: (_a = item.screenshot) === null || _a === void 0 ? void 0 : _a.path });
        })
            .filter(isNotNil_1.isNotNil);
        this.forwardMessageModal = new Whisper.ReactWrapperView({
            JSX: window.Signal.State.Roots.createForwardMessageModal(window.reduxStore, {
                attachments: draftAttachments,
                conversationId: this.model.id,
                doForwardMessage: async (conversationIds, messageBody, includedAttachments, linkPreview) => {
                    try {
                        const didForwardSuccessfully = await this.maybeForwardMessage(message, conversationIds, messageBody, includedAttachments, linkPreview);
                        if (didForwardSuccessfully && this.forwardMessageModal) {
                            this.forwardMessageModal.remove();
                            this.forwardMessageModal = undefined;
                        }
                    }
                    catch (err) {
                        log.warn('doForwardMessage', err && err.stack ? err.stack : err);
                    }
                },
                isSticker: Boolean(message.get('sticker')),
                messageBody: message.getRawText(),
                onClose: () => {
                    if (this.forwardMessageModal) {
                        this.forwardMessageModal.remove();
                        this.forwardMessageModal = undefined;
                    }
                    this.resetLinkPreview();
                },
                onEditorStateChange: (messageText, _, caretLocation) => {
                    if (!attachments.length) {
                        this.debouncedMaybeGrabLinkPreview(messageText, caretLocation);
                    }
                },
                onTextTooLong: () => (0, showToast_1.showToast)(ToastMessageBodyTooLong_1.ToastMessageBodyTooLong),
            }),
        });
        this.forwardMessageModal.render();
    }
    async maybeForwardMessage(message, conversationIds, messageBody, attachments, linkPreview) {
        log.info(`maybeForwardMessage/${message.idForLogging()}: Starting...`);
        const attachmentLookup = new Set();
        if (attachments) {
            attachments.forEach(attachment => {
                attachmentLookup.add(`${attachment.fileName}/${attachment.contentType}`);
            });
        }
        const conversations = conversationIds.map(id => window.ConversationController.get(id));
        const cannotSend = conversations.some(conversation => (conversation === null || conversation === void 0 ? void 0 : conversation.get('announcementsOnly')) && !conversation.areWeAdmin());
        if (cannotSend) {
            throw new Error('Cannot send to group');
        }
        // Verify that all contacts that we're forwarding
        // to are verified and trusted
        const unverifiedContacts = [];
        const untrustedContacts = [];
        await Promise.all(conversations.map(async (conversation) => {
            if (conversation) {
                await conversation.updateVerified();
                const unverifieds = conversation.getUnverified();
                if (unverifieds.length) {
                    unverifieds.forEach(unverifiedConversation => unverifiedContacts.push(unverifiedConversation));
                }
                const untrusted = conversation.getUntrusted();
                if (untrusted.length) {
                    untrusted.forEach(untrustedConversation => untrustedContacts.push(untrustedConversation));
                }
            }
        }));
        // If there are any unverified or untrusted contacts, show the
        // SendAnywayDialog and if we're fine with sending then mark all as
        // verified and trusted and continue the send.
        const iffyConversations = [...unverifiedContacts, ...untrustedContacts];
        if (iffyConversations.length) {
            const forwardMessageModal = document.querySelector('.module-ForwardMessageModal');
            if (forwardMessageModal) {
                forwardMessageModal.style.display = 'none';
            }
            const sendAnyway = await this.showSendAnywayDialog(iffyConversations);
            if (!sendAnyway) {
                if (forwardMessageModal) {
                    forwardMessageModal.style.display = 'block';
                }
                return false;
            }
            let verifyPromise;
            let approvePromise;
            if (unverifiedContacts.length) {
                verifyPromise = (0, markAllAsVerifiedDefault_1.markAllAsVerifiedDefault)(unverifiedContacts);
            }
            if (untrustedContacts.length) {
                approvePromise = (0, markAllAsApproved_1.markAllAsApproved)(untrustedContacts);
            }
            await Promise.all([verifyPromise, approvePromise]);
        }
        const sendMessageOptions = { dontClearDraft: true };
        const baseTimestamp = Date.now();
        // Actually send the message
        // load any sticker data, attachments, or link previews that we need to
        // send along with the message and do the send to each conversation.
        await Promise.all(conversations.map(async (conversation, offset) => {
            const timestamp = baseTimestamp + offset;
            if (conversation) {
                const sticker = message.get('sticker');
                if (sticker) {
                    const stickerWithData = await loadStickerData(sticker);
                    const stickerNoPath = stickerWithData
                        ? Object.assign(Object.assign({}, stickerWithData), { data: Object.assign(Object.assign({}, stickerWithData.data), { path: undefined }) }) : undefined;
                    conversation.enqueueMessageForSend(undefined, // body
                    [], undefined, // quote
                    [], stickerNoPath, undefined, Object.assign(Object.assign({}, sendMessageOptions), { timestamp }));
                }
                else {
                    const preview = linkPreview
                        ? await loadPreviewData([linkPreview])
                        : [];
                    const attachmentsWithData = await Promise.all((attachments || []).map(async (item) => (Object.assign(Object.assign({}, (await loadAttachmentData(item))), { path: undefined }))));
                    const attachmentsToSend = attachmentsWithData.filter((attachment) => attachmentLookup.has(`${attachment.fileName}/${attachment.contentType}`));
                    conversation.enqueueMessageForSend(messageBody || undefined, attachmentsToSend, undefined, // quote
                    preview, undefined, // sticker
                    undefined, Object.assign(Object.assign({}, sendMessageOptions), { timestamp }));
                }
            }
        }));
        // Cancel any link still pending, even if it didn't make it into the message
        this.resetLinkPreview();
        return true;
    }
    showAllMedia() {
        if (document.querySelectorAll('.module-media-gallery').length) {
            return;
        }
        // We fetch more documents than media as they don’t require to be loaded
        // into memory right away. Revisit this once we have infinite scrolling:
        const DEFAULT_MEDIA_FETCH_COUNT = 50;
        const DEFAULT_DOCUMENTS_FETCH_COUNT = 150;
        const conversationId = this.model.get('id');
        const getProps = async () => {
            const rawMedia = await window.Signal.Data.getMessagesWithVisualMediaAttachments(conversationId, {
                limit: DEFAULT_MEDIA_FETCH_COUNT,
            });
            const rawDocuments = await window.Signal.Data.getMessagesWithFileAttachments(conversationId, {
                limit: DEFAULT_DOCUMENTS_FETCH_COUNT,
            });
            // First we upgrade these messages to ensure that they have thumbnails
            for (let max = rawMedia.length, i = 0; i < max; i += 1) {
                const message = rawMedia[i];
                const { schemaVersion } = message;
                if (schemaVersion &&
                    schemaVersion < Message.VERSION_NEEDED_FOR_DISPLAY) {
                    // Yep, we really do want to wait for each of these
                    // eslint-disable-next-line no-await-in-loop
                    rawMedia[i] = await upgradeMessageSchema(message);
                    // eslint-disable-next-line no-await-in-loop
                    await window.Signal.Data.saveMessage(rawMedia[i]);
                }
            }
            const media = (0, lodash_1.flatten)(rawMedia.map(message => {
                return (message.attachments || []).map((attachment, index) => {
                    var _a;
                    if (!attachment.path ||
                        !attachment.thumbnail ||
                        attachment.pending ||
                        attachment.error) {
                        return;
                    }
                    const { thumbnail } = attachment;
                    return {
                        path: attachment.path,
                        objectURL: getAbsoluteAttachmentPath(attachment.path),
                        thumbnailObjectUrl: thumbnail
                            ? getAbsoluteAttachmentPath(thumbnail.path)
                            : undefined,
                        contentType: attachment.contentType,
                        index,
                        attachment,
                        message: {
                            attachments: message.attachments || [],
                            conversationId: ((_a = window.ConversationController.get(window.ConversationController.ensureContactIds({
                                uuid: message.sourceUuid,
                                e164: message.source,
                            }))) === null || _a === void 0 ? void 0 : _a.id) || message.conversationId,
                            id: message.id,
                            received_at: message.received_at,
                            received_at_ms: Number(message.received_at_ms),
                            sent_at: message.sent_at,
                        },
                    };
                });
            })).filter(isNotNil_1.isNotNil);
            // Unlike visual media, only one non-image attachment is supported
            const documents = rawDocuments
                .filter(message => Boolean(message.attachments && message.attachments.length))
                .map(message => {
                const attachments = message.attachments || [];
                const attachment = attachments[0];
                return {
                    contentType: attachment.contentType,
                    index: 0,
                    attachment,
                    message,
                };
            });
            const saveAttachment = async ({ attachment, message, }) => {
                const timestamp = message.sent_at;
                const fullPath = await Attachment.save({
                    attachment,
                    readAttachmentData,
                    saveAttachmentToDisk,
                    timestamp,
                });
                if (fullPath) {
                    (0, showToast_1.showToast)(ToastFileSaved_1.ToastFileSaved, {
                        onOpenFile: () => {
                            openFileInFolder(fullPath);
                        },
                    });
                }
            };
            const onItemClick = async ({ message, attachment, type, }) => {
                switch (type) {
                    case 'documents': {
                        saveAttachment({ message, attachment });
                        break;
                    }
                    case 'media': {
                        const selectedMedia = media.find(item => attachment.path === item.path) || media[0];
                        this.showLightboxForMedia(selectedMedia, media);
                        break;
                    }
                    default:
                        throw new TypeError(`Unknown attachment type: '${type}'`);
                }
            };
            return {
                documents,
                media,
                onItemClick,
            };
        };
        function getMessageIds() {
            var _a;
            const state = window.reduxStore.getState();
            const byConversation = (_a = state === null || state === void 0 ? void 0 : state.conversations) === null || _a === void 0 ? void 0 : _a.messagesByConversation;
            const messages = byConversation && byConversation[conversationId];
            if (!messages || !messages.messageIds) {
                return undefined;
            }
            return messages.messageIds;
        }
        // Detect message changes in the current conversation
        let previousMessageList;
        previousMessageList = getMessageIds();
        const unsubscribe = window.reduxStore.subscribe(() => {
            const currentMessageList = getMessageIds();
            if (currentMessageList !== previousMessageList) {
                update();
                previousMessageList = currentMessageList;
            }
        });
        const view = new Whisper.ReactWrapperView({
            className: 'panel',
            Component: window.Signal.Components.MediaGallery,
            onClose: () => {
                unsubscribe();
            },
        });
        view.headerTitle = window.i18n('allMedia');
        const update = async () => {
            view.update(await getProps());
        };
        this.listenBack(view);
        update();
    }
    focusMessageField() {
        var _a;
        if (this.panels && this.panels.length) {
            return;
        }
        (_a = this.compositionApi.current) === null || _a === void 0 ? void 0 : _a.focusInput();
    }
    disableMessageField() {
        var _a;
        (_a = this.compositionApi.current) === null || _a === void 0 ? void 0 : _a.setDisabled(true);
    }
    enableMessageField() {
        var _a;
        (_a = this.compositionApi.current) === null || _a === void 0 ? void 0 : _a.setDisabled(false);
    }
    resetEmojiResults() {
        var _a;
        (_a = this.compositionApi.current) === null || _a === void 0 ? void 0 : _a.resetEmojiResults();
    }
    showGV1Members() {
        const { contactCollection } = this.model;
        const memberships = (contactCollection === null || contactCollection === void 0 ? void 0 : contactCollection.map((conversation) => {
            return {
                isAdmin: false,
                member: conversation.format(),
            };
        })) || [];
        const view = new Whisper.ReactWrapperView({
            className: 'group-member-list panel',
            Component: ConversationDetailsMembershipList_1.ConversationDetailsMembershipList,
            props: {
                canAddNewMembers: false,
                i18n: window.i18n,
                maxShownMemberCount: 32,
                memberships,
                showContactModal: this.showContactModal.bind(this),
            },
        });
        this.listenBack(view);
        view.render();
    }
    showSafetyNumber(id) {
        let conversation;
        if (!id && (0, whatTypeOfConversation_1.isDirectConversation)(this.model.attributes)) {
            // eslint-disable-next-line prefer-destructuring
            conversation = this.model;
        }
        else {
            conversation = window.ConversationController.get(id);
        }
        if (conversation) {
            window.reduxActions.globalModals.toggleSafetyNumberModal(conversation.get('id'));
        }
    }
    downloadAttachmentWrapper(messageId, providedAttachment) {
        const message = window.MessageController.getById(messageId);
        if (!message) {
            throw new Error(`downloadAttachmentWrapper: Message ${messageId} missing!`);
        }
        const { attachments, sent_at: timestamp } = message.attributes;
        if (!attachments || attachments.length < 1) {
            return;
        }
        const attachment = providedAttachment && attachments.includes(providedAttachment)
            ? providedAttachment
            : attachments[0];
        const { fileName } = attachment;
        const isDangerous = window.Signal.Util.isFileDangerous(fileName || '');
        this.downloadAttachment({ attachment, timestamp, isDangerous });
    }
    async downloadAttachment({ attachment, timestamp, isDangerous, }) {
        if (isDangerous) {
            (0, showToast_1.showToast)(ToastDangerousFileType_1.ToastDangerousFileType);
            return;
        }
        const fullPath = await Attachment.save({
            attachment,
            readAttachmentData,
            saveAttachmentToDisk,
            timestamp,
        });
        if (fullPath) {
            (0, showToast_1.showToast)(ToastFileSaved_1.ToastFileSaved, {
                onOpenFile: () => {
                    openFileInFolder(fullPath);
                },
            });
        }
    }
    async displayTapToViewMessage(messageId) {
        log.info('displayTapToViewMessage: attempting to display message');
        const message = window.MessageController.getById(messageId);
        if (!message) {
            throw new Error(`displayTapToViewMessage: Message ${messageId} missing!`);
        }
        if (!(0, message_1.isTapToView)(message.attributes)) {
            throw new Error(`displayTapToViewMessage: Message ${message.idForLogging()} is not a tap to view message`);
        }
        if (message.isErased()) {
            throw new Error(`displayTapToViewMessage: Message ${message.idForLogging()} is already erased`);
        }
        const firstAttachment = (message.get('attachments') || [])[0];
        if (!firstAttachment || !firstAttachment.path) {
            throw new Error(`displayTapToViewMessage: Message ${message.idForLogging()} had no first attachment with path`);
        }
        const absolutePath = getAbsoluteAttachmentPath(firstAttachment.path);
        const { path: tempPath } = await copyIntoTempDirectory(absolutePath);
        const tempAttachment = Object.assign(Object.assign({}, firstAttachment), { path: tempPath });
        await message.markViewOnceMessageViewed();
        const closeLightbox = async () => {
            log.info('displayTapToViewMessage: attempting to close lightbox');
            if (!this.lightboxView) {
                log.info('displayTapToViewMessage: lightbox was already closed');
                return;
            }
            const { lightboxView } = this;
            this.lightboxView = undefined;
            this.stopListening(message);
            window.Signal.Backbone.Views.Lightbox.hide();
            lightboxView.remove();
            await deleteTempFile(tempPath);
        };
        this.listenTo(message, 'expired', closeLightbox);
        this.listenTo(message, 'change', () => {
            if (this.lightboxView) {
                this.lightboxView.update(getProps());
            }
        });
        const getProps = () => {
            const { path, contentType } = tempAttachment;
            return {
                media: [
                    {
                        attachment: tempAttachment,
                        objectURL: getAbsoluteTempPath(path),
                        contentType,
                        index: 0,
                        message: {
                            attachments: message.get('attachments'),
                            id: message.get('id'),
                            conversationId: message.get('conversationId'),
                            received_at: message.get('received_at'),
                            received_at_ms: Number(message.get('received_at_ms')),
                            sent_at: message.get('sent_at'),
                        },
                    },
                ],
                isViewOnce: true,
            };
        };
        if (this.lightboxView) {
            this.lightboxView.remove();
            this.lightboxView = undefined;
        }
        this.lightboxView = new Whisper.ReactWrapperView({
            className: 'lightbox-wrapper',
            Component: window.Signal.Components.Lightbox,
            props: getProps(),
            onClose: closeLightbox,
        });
        window.Signal.Backbone.Views.Lightbox.show(this.lightboxView.el);
        log.info('displayTapToViewMessage: showed lightbox');
    }
    deleteMessage(messageId) {
        const message = window.MessageController.getById(messageId);
        if (!message) {
            throw new Error(`deleteMessage: Message ${messageId} missing!`);
        }
        window.showConfirmationDialog({
            confirmStyle: 'negative',
            message: window.i18n('deleteWarning'),
            okText: window.i18n('delete'),
            resolve: () => {
                window.Signal.Data.removeMessage(message.id, {
                    Message: Whisper.Message,
                });
                message.cleanup();
                if ((0, message_1.isOutgoing)(message.attributes)) {
                    this.model.decrementSentMessageCount();
                }
                else {
                    this.model.decrementMessageCount();
                }
                this.resetPanel();
            },
        });
    }
    deleteMessageForEveryone(messageId) {
        const message = window.MessageController.getById(messageId);
        if (!message) {
            throw new Error(`deleteMessageForEveryone: Message ${messageId} missing!`);
        }
        window.showConfirmationDialog({
            confirmStyle: 'negative',
            message: window.i18n('deleteForEveryoneWarning'),
            okText: window.i18n('delete'),
            resolve: async () => {
                try {
                    await this.model.sendDeleteForEveryoneMessage({
                        id: message.id,
                        timestamp: message.get('sent_at'),
                    });
                }
                catch (error) {
                    log.error('Error sending delete-for-everyone', error && error.stack, messageId);
                    (0, showToast_1.showToast)(ToastDeleteForEveryoneFailed_1.ToastDeleteForEveryoneFailed);
                }
                this.resetPanel();
            },
        });
    }
    showStickerPackPreview(packId, packKey) {
        Stickers.downloadEphemeralPack(packId, packKey);
        const props = {
            packId,
            onClose: async () => {
                if (this.stickerPreviewModalView) {
                    this.stickerPreviewModalView.remove();
                    this.stickerPreviewModalView = undefined;
                }
                await Stickers.removeEphemeralPack(packId);
            },
        };
        this.stickerPreviewModalView = new Whisper.ReactWrapperView({
            className: 'sticker-preview-modal-wrapper',
            JSX: window.Signal.State.Roots.createStickerPreviewModal(window.reduxStore, props),
        });
    }
    showLightboxForMedia(selectedMediaItem, media = []) {
        const onSave = async ({ attachment, message, index, }) => {
            const fullPath = await Attachment.save({
                attachment,
                index: index + 1,
                readAttachmentData,
                saveAttachmentToDisk,
                timestamp: message.sent_at,
            });
            if (fullPath) {
                (0, showToast_1.showToast)(ToastFileSaved_1.ToastFileSaved, {
                    onOpenFile: () => {
                        openFileInFolder(fullPath);
                    },
                });
            }
        };
        const selectedIndex = media.findIndex(mediaItem => mediaItem.attachment.path === selectedMediaItem.attachment.path);
        if (this.lightboxView) {
            this.lightboxView.remove();
            this.lightboxView = undefined;
        }
        this.lightboxView = new Whisper.ReactWrapperView({
            className: 'lightbox-wrapper',
            Component: window.Signal.Components.Lightbox,
            props: {
                getConversation: (0, conversations_1.getConversationSelector)(window.reduxStore.getState()),
                media,
                onForward: this.showForwardMessageModal.bind(this),
                onSave,
                selectedIndex: selectedIndex >= 0 ? selectedIndex : 0,
            },
            onClose: () => window.Signal.Backbone.Views.Lightbox.hide(),
        });
        window.Signal.Backbone.Views.Lightbox.show(this.lightboxView.el);
    }
    showLightbox({ attachment, messageId, }) {
        const message = window.MessageController.getById(messageId);
        if (!message) {
            throw new Error(`showLightbox: Message ${messageId} missing!`);
        }
        const sticker = message.get('sticker');
        if (sticker) {
            const { packId, packKey } = sticker;
            this.showStickerPackPreview(packId, packKey);
            return;
        }
        const { contentType } = attachment;
        if (!window.Signal.Util.GoogleChrome.isImageTypeSupported(contentType) &&
            !window.Signal.Util.GoogleChrome.isVideoTypeSupported(contentType)) {
            this.downloadAttachmentWrapper(messageId, attachment);
            return;
        }
        const attachments = message.get('attachments') || [];
        const loop = (0, Attachment_1.isGIF)(attachments);
        const media = attachments
            .filter(item => item.thumbnail && !item.pending && !item.error)
            .map((item, index) => {
            var _a, _b, _c, _d, _e;
            return ({
                objectURL: getAbsoluteAttachmentPath((_a = item.path) !== null && _a !== void 0 ? _a : ''),
                path: item.path,
                contentType: item.contentType,
                loop,
                index,
                message: {
                    attachments: message.get('attachments') || [],
                    id: message.get('id'),
                    conversationId: ((_b = window.ConversationController.get(window.ConversationController.ensureContactIds({
                        uuid: message.get('sourceUuid'),
                        e164: message.get('source'),
                    }))) === null || _b === void 0 ? void 0 : _b.id) || message.get('conversationId'),
                    received_at: message.get('received_at'),
                    received_at_ms: Number(message.get('received_at_ms')),
                    sent_at: message.get('sent_at'),
                },
                attachment: item,
                thumbnailObjectUrl: ((_c = item.thumbnail) === null || _c === void 0 ? void 0 : _c.objectUrl) ||
                    getAbsoluteAttachmentPath((_e = (_d = item.thumbnail) === null || _d === void 0 ? void 0 : _d.path) !== null && _e !== void 0 ? _e : ''),
            });
        });
        const selectedMedia = media.find(item => attachment.path === item.path) || media[0];
        this.showLightboxForMedia(selectedMedia, media);
    }
    showContactModal(contactId) {
        window.reduxActions.globalModals.showContactModal(contactId, this.model.id);
    }
    showGroupLinkManagement() {
        const view = new Whisper.ReactWrapperView({
            className: 'panel',
            JSX: window.Signal.State.Roots.createGroupLinkManagement(window.reduxStore, {
                changeHasGroupLink: this.changeHasGroupLink.bind(this),
                conversationId: this.model.id,
                copyGroupLink: copyGroupLink_1.copyGroupLink,
                generateNewGroupLink: this.generateNewGroupLink.bind(this),
                setAccessControlAddFromInviteLinkSetting: this.setAccessControlAddFromInviteLinkSetting.bind(this),
            }),
        });
        view.headerTitle = window.i18n('ConversationDetails--group-link');
        this.listenBack(view);
        view.render();
    }
    showGroupV2Permissions() {
        const view = new Whisper.ReactWrapperView({
            className: 'panel',
            JSX: window.Signal.State.Roots.createGroupV2Permissions(window.reduxStore, {
                conversationId: this.model.id,
                setAccessControlAttributesSetting: this.setAccessControlAttributesSetting.bind(this),
                setAccessControlMembersSetting: this.setAccessControlMembersSetting.bind(this),
                setAnnouncementsOnly: this.setAnnouncementsOnly.bind(this),
            }),
        });
        view.headerTitle = window.i18n('permissions');
        this.listenBack(view);
        view.render();
    }
    showPendingInvites() {
        const view = new Whisper.ReactWrapperView({
            className: 'panel',
            JSX: window.Signal.State.Roots.createPendingInvites(window.reduxStore, {
                conversationId: this.model.id,
                ourUuid: window.textsecure.storage.user.getCheckedUuid().toString(),
                approvePendingMembership: (conversationId) => {
                    this.model.approvePendingMembershipFromGroupV2(conversationId);
                },
                revokePendingMemberships: conversationIds => {
                    this.model.revokePendingMembershipsFromGroupV2(conversationIds);
                },
            }),
        });
        view.headerTitle = window.i18n('ConversationDetails--requests-and-invites');
        this.listenBack(view);
        view.render();
    }
    showConversationNotificationsSettings() {
        const view = new Whisper.ReactWrapperView({
            className: 'panel',
            JSX: window.Signal.State.Roots.createConversationNotificationsSettings(window.reduxStore, {
                conversationId: this.model.id,
                setDontNotifyForMentionsIfMuted: this.model.setDontNotifyForMentionsIfMuted.bind(this.model),
                setMuteExpiration: this.setMuteExpiration.bind(this),
            }),
        });
        view.headerTitle = window.i18n('ConversationDetails--notifications');
        this.listenBack(view);
        view.render();
    }
    showChatColorEditor() {
        const view = new Whisper.ReactWrapperView({
            className: 'panel',
            JSX: window.Signal.State.Roots.createChatColorPicker(window.reduxStore, {
                conversationId: this.model.get('id'),
            }),
        });
        view.headerTitle = window.i18n('ChatColorPicker__menu-title');
        this.listenBack(view);
        view.render();
    }
    showConversationDetails() {
        // Run a getProfiles in case member's capabilities have changed
        // Redux should cover us on the return here so no need to await this.
        if (this.model.throttledGetProfiles) {
            this.model.throttledGetProfiles();
        }
        const messageRequestEnum = protobuf_1.SignalService.SyncMessage.MessageRequestResponse.Type;
        // these methods are used in more than one place and should probably be
        // dried up and hoisted to methods on ConversationView
        const onLeave = () => {
            this.longRunningTaskWrapper({
                name: 'onLeave',
                task: () => this.model.leaveGroupV2(),
            });
        };
        const onBlock = () => {
            this.syncMessageRequestResponse('onBlock', this.model, messageRequestEnum.BLOCK);
        };
        const props = {
            addMembers: this.model.addMembersV2.bind(this.model),
            conversationId: this.model.get('id'),
            loadRecentMediaItems: this.loadRecentMediaItems.bind(this),
            setDisappearingMessages: this.setDisappearingMessages.bind(this),
            showAllMedia: this.showAllMedia.bind(this),
            showContactModal: this.showContactModal.bind(this),
            showChatColorEditor: this.showChatColorEditor.bind(this),
            showGroupLinkManagement: this.showGroupLinkManagement.bind(this),
            showGroupV2Permissions: this.showGroupV2Permissions.bind(this),
            showConversationNotificationsSettings: this.showConversationNotificationsSettings.bind(this),
            showPendingInvites: this.showPendingInvites.bind(this),
            showLightboxForMedia: this.showLightboxForMedia.bind(this),
            updateGroupAttributes: this.model.updateGroupAttributesV2.bind(this.model),
            onLeave,
            onBlock,
            onUnblock: () => {
                this.syncMessageRequestResponse('onUnblock', this.model, messageRequestEnum.ACCEPT);
            },
            setMuteExpiration: this.setMuteExpiration.bind(this),
            onOutgoingAudioCallInConversation: this.onOutgoingAudioCallInConversation.bind(this),
            onOutgoingVideoCallInConversation: this.onOutgoingVideoCallInConversation.bind(this),
        };
        const view = new Whisper.ReactWrapperView({
            className: 'conversation-details-pane panel',
            JSX: window.Signal.State.Roots.createConversationDetails(window.reduxStore, props),
        });
        view.headerTitle = '';
        this.listenBack(view);
        view.render();
    }
    showMessageDetail(messageId) {
        const message = window.MessageController.getById(messageId);
        if (!message) {
            throw new Error(`showMessageDetail: Message ${messageId} missing!`);
        }
        if (!message.isNormalBubble()) {
            return;
        }
        const getProps = () => (Object.assign(Object.assign({}, message.getPropsForMessageDetail(window.ConversationController.getOurConversationIdOrThrow())), this.getMessageActions()));
        const onClose = () => {
            this.stopListening(message, 'change', update);
            this.resetPanel();
        };
        const view = new Whisper.ReactWrapperView({
            className: 'panel message-detail-wrapper',
            JSX: window.Signal.State.Roots.createMessageDetail(window.reduxStore, getProps()),
            onClose,
        });
        const update = () => view.update(window.Signal.State.Roots.createMessageDetail(window.reduxStore, getProps()));
        this.listenTo(message, 'change', update);
        this.listenTo(message, 'expired', onClose);
        // We could listen to all involved contacts, but we'll call that overkill
        this.listenBack(view);
        view.render();
    }
    showStickerManager() {
        const view = new Whisper.ReactWrapperView({
            className: ['sticker-manager-wrapper', 'panel'].join(' '),
            JSX: window.Signal.State.Roots.createStickerManager(window.reduxStore),
            onClose: () => {
                this.resetPanel();
            },
        });
        this.listenBack(view);
        view.render();
    }
    showContactDetail({ contact, signalAccount, }) {
        const view = new Whisper.ReactWrapperView({
            Component: window.Signal.Components.ContactDetail,
            className: 'contact-detail-pane panel',
            props: {
                contact,
                hasSignalAccount: Boolean(signalAccount),
                onSendMessage: () => {
                    if (signalAccount) {
                        this.openConversation(signalAccount);
                    }
                },
            },
            onClose: () => {
                this.resetPanel();
            },
        });
        this.listenBack(view);
    }
    async openConversation(conversationId, messageId) {
        window.Whisper.events.trigger('showConversation', conversationId, messageId);
    }
    listenBack(view) {
        this.panels = this.panels || [];
        if (this.panels.length === 0) {
            this.previousFocus = document.activeElement;
        }
        this.panels.unshift(view);
        view.$el.insertAfter(this.$('.panel').last());
        view.$el.one('animationend', () => {
            view.$el.addClass('panel--static');
        });
        window.reduxActions.conversations.setSelectedConversationPanelDepth(this.panels.length);
        window.reduxActions.conversations.setSelectedConversationHeaderTitle(view.headerTitle);
    }
    resetPanel() {
        var _a;
        if (!this.panels || !this.panels.length) {
            return;
        }
        const view = this.panels.shift();
        if (this.panels.length === 0 &&
            this.previousFocus &&
            this.previousFocus.focus) {
            this.previousFocus.focus();
            this.previousFocus = undefined;
        }
        if (this.panels.length > 0) {
            this.panels[0].$el.fadeIn(250);
        }
        if (view) {
            view.$el.addClass('panel--remove').one('transitionend', () => {
                view.remove();
                if (this.panels.length === 0) {
                    // Make sure poppers are positioned properly
                    window.dispatchEvent(new Event('resize'));
                }
            });
        }
        window.reduxActions.conversations.setSelectedConversationPanelDepth(this.panels.length);
        window.reduxActions.conversations.setSelectedConversationHeaderTitle((_a = this.panels[0]) === null || _a === void 0 ? void 0 : _a.headerTitle);
    }
    async loadRecentMediaItems(limit) {
        const { model } = this;
        const messages = await window.Signal.Data.getMessagesWithVisualMediaAttachments(model.id, {
            limit,
        });
        const loadedRecentMediaItems = messages
            .filter(message => message.attachments !== undefined)
            .reduce((acc, message) => [
            ...acc,
            ...(message.attachments || []).map((attachment, index) => {
                var _a;
                const { thumbnail } = attachment;
                return {
                    objectURL: getAbsoluteAttachmentPath(attachment.path || ''),
                    thumbnailObjectUrl: thumbnail
                        ? getAbsoluteAttachmentPath(thumbnail.path)
                        : '',
                    contentType: attachment.contentType,
                    index,
                    attachment,
                    message: {
                        attachments: message.attachments || [],
                        conversationId: ((_a = window.ConversationController.get(message.sourceUuid)) === null || _a === void 0 ? void 0 : _a.id) ||
                            message.conversationId,
                        id: message.id,
                        received_at: message.received_at,
                        received_at_ms: Number(message.received_at_ms),
                        sent_at: message.sent_at,
                    },
                };
            }),
        ], []);
        window.reduxActions.conversations.setRecentMediaItems(model.id, loadedRecentMediaItems);
    }
    async setDisappearingMessages(seconds) {
        const { model } = this;
        const valueToSet = seconds > 0 ? seconds : undefined;
        await this.longRunningTaskWrapper({
            name: 'updateExpirationTimer',
            task: async () => model.updateExpirationTimer(valueToSet),
        });
    }
    async changeHasGroupLink(value) {
        const { model } = this;
        await this.longRunningTaskWrapper({
            name: 'toggleGroupLink',
            task: async () => model.toggleGroupLink(value),
        });
    }
    async generateNewGroupLink() {
        const { model } = this;
        window.showConfirmationDialog({
            confirmStyle: 'negative',
            message: window.i18n('GroupLinkManagement--confirm-reset'),
            okText: window.i18n('GroupLinkManagement--reset'),
            resolve: async () => {
                await this.longRunningTaskWrapper({
                    name: 'refreshGroupLink',
                    task: async () => model.refreshGroupLink(),
                });
            },
        });
    }
    async setAccessControlAddFromInviteLinkSetting(value) {
        const { model } = this;
        await this.longRunningTaskWrapper({
            name: 'updateAccessControlAddFromInviteLink',
            task: async () => model.updateAccessControlAddFromInviteLink(value),
        });
    }
    async setAccessControlAttributesSetting(value) {
        const { model } = this;
        await this.longRunningTaskWrapper({
            name: 'updateAccessControlAttributes',
            task: async () => model.updateAccessControlAttributes(value),
        });
    }
    async setAccessControlMembersSetting(value) {
        const { model } = this;
        await this.longRunningTaskWrapper({
            name: 'updateAccessControlMembers',
            task: async () => model.updateAccessControlMembers(value),
        });
    }
    async setAnnouncementsOnly(value) {
        const { model } = this;
        await this.longRunningTaskWrapper({
            name: 'updateAnnouncementsOnly',
            task: async () => model.updateAnnouncementsOnly(value),
        });
    }
    async destroyMessages() {
        const { model } = this;
        window.showConfirmationDialog({
            confirmStyle: 'negative',
            message: window.i18n('deleteConversationConfirmation'),
            okText: window.i18n('delete'),
            resolve: () => {
                this.longRunningTaskWrapper({
                    name: 'destroymessages',
                    task: async () => {
                        model.trigger('unload', 'delete messages');
                        await model.destroyMessages();
                        model.updateLastMessage();
                    },
                });
            },
            reject: () => {
                log.info('destroyMessages: User canceled delete');
            },
        });
    }
    async isCallSafe() {
        const contacts = await this.getUntrustedContacts();
        if (contacts && contacts.length) {
            const callAnyway = await this.showSendAnywayDialog(contacts.models, window.i18n('callAnyway'));
            if (!callAnyway) {
                log.info('Safety number change dialog not accepted, new call not allowed.');
                return false;
            }
        }
        return true;
    }
    showSendAnywayDialog(contacts, confirmText) {
        return new Promise(resolve => {
            (0, showSafetyNumberChangeDialog_1.showSafetyNumberChangeDialog)({
                confirmText,
                contacts,
                reject: () => {
                    resolve(false);
                },
                resolve: () => {
                    resolve(true);
                },
            });
        });
    }
    async sendStickerMessage(options) {
        const { model } = this;
        try {
            const contacts = await this.getUntrustedContacts(options);
            if (contacts && contacts.length) {
                const sendAnyway = await this.showSendAnywayDialog(contacts.models);
                if (sendAnyway) {
                    this.sendStickerMessage(Object.assign(Object.assign({}, options), { force: true }));
                }
                return;
            }
            if (this.showInvalidMessageToast()) {
                return;
            }
            const { packId, stickerId } = options;
            model.sendStickerMessage(packId, stickerId);
        }
        catch (error) {
            log.error('clickSend error:', error && error.stack ? error.stack : error);
        }
    }
    async getUntrustedContacts(options = {}) {
        const { model } = this;
        // This will go to the trust store for the latest identity key information,
        //   and may result in the display of a new banner for this conversation.
        await model.updateVerified();
        const unverifiedContacts = model.getUnverified();
        if (options.force) {
            if (unverifiedContacts.length) {
                await (0, markAllAsVerifiedDefault_1.markAllAsVerifiedDefault)(unverifiedContacts.models);
                // We only want force to break us through one layer of checks
                // eslint-disable-next-line no-param-reassign
                options.force = false;
            }
        }
        else if (unverifiedContacts.length) {
            return unverifiedContacts;
        }
        const untrustedContacts = model.getUntrusted();
        if (options.force) {
            if (untrustedContacts.length) {
                await (0, markAllAsApproved_1.markAllAsApproved)(untrustedContacts.models);
            }
        }
        else if (untrustedContacts.length) {
            return untrustedContacts;
        }
        return null;
    }
    async setQuoteMessage(messageId) {
        const { model } = this;
        const message = messageId
            ? await getMessageById(messageId, {
                Message: Whisper.Message,
            })
            : undefined;
        if (message &&
            !(0, message_1.canReply)(message.attributes, window.ConversationController.getOurConversationIdOrThrow(), findAndFormatContact_1.findAndFormatContact)) {
            return;
        }
        if (message && !message.isNormalBubble()) {
            return;
        }
        this.quote = undefined;
        this.quotedMessage = undefined;
        const existing = model.get('quotedMessageId');
        if (existing !== messageId) {
            const now = Date.now();
            let active_at = this.model.get('active_at');
            let timestamp = this.model.get('timestamp');
            if (!active_at && messageId) {
                active_at = now;
                timestamp = now;
            }
            this.model.set({
                active_at,
                draftChanged: true,
                quotedMessageId: messageId,
                timestamp,
            });
            await this.saveModel();
        }
        if (message) {
            const quotedMessage = window.MessageController.register(message.id, message);
            this.quotedMessage = quotedMessage;
            if (quotedMessage) {
                this.quote = await model.makeQuote(this.quotedMessage);
                this.enableMessageField();
                this.focusMessageField();
            }
        }
        this.renderQuotedMessage();
    }
    renderQuotedMessage() {
        const { model } = this;
        if (!this.quotedMessage) {
            window.reduxActions.composer.setQuotedMessage(undefined);
            return;
        }
        window.reduxActions.composer.setQuotedMessage({
            conversationId: model.id,
            quote: this.quote,
        });
    }
    showInvalidMessageToast(messageText) {
        const { model } = this;
        let toastView;
        if (window.reduxStore.getState().expiration.hasExpired) {
            toastView = ToastExpired_1.ToastExpired;
        }
        if (!model.isValid()) {
            toastView = ToastInvalidConversation_1.ToastInvalidConversation;
        }
        const e164 = this.model.get('e164');
        const uuid = this.model.get('uuid');
        if ((0, whatTypeOfConversation_1.isDirectConversation)(this.model.attributes) &&
            ((e164 && window.storage.blocked.isBlocked(e164)) ||
                (uuid && window.storage.blocked.isUuidBlocked(uuid)))) {
            toastView = ToastBlocked_1.ToastBlocked;
        }
        const groupId = this.model.get('groupId');
        if (!(0, whatTypeOfConversation_1.isDirectConversation)(this.model.attributes) &&
            groupId &&
            window.storage.blocked.isGroupBlocked(groupId)) {
            toastView = ToastBlockedGroup_1.ToastBlockedGroup;
        }
        if (!(0, whatTypeOfConversation_1.isDirectConversation)(model.attributes) && model.get('left')) {
            toastView = ToastLeftGroup_1.ToastLeftGroup;
        }
        if (messageText && messageText.length > MAX_MESSAGE_BODY_LENGTH) {
            toastView = ToastMessageBodyTooLong_1.ToastMessageBodyTooLong;
        }
        if (toastView) {
            (0, showToast_1.showToast)(toastView);
            return true;
        }
        return false;
    }
    async sendMessage(message = '', mentions = [], options = {}) {
        const { model } = this;
        const timestamp = options.timestamp || Date.now();
        this.sendStart = Date.now();
        try {
            const contacts = await this.getUntrustedContacts(options);
            this.disableMessageField();
            if (contacts && contacts.length) {
                const sendAnyway = await this.showSendAnywayDialog(contacts.models);
                if (sendAnyway) {
                    this.sendMessage(message, mentions, { force: true, timestamp });
                    return;
                }
                this.enableMessageField();
                return;
            }
        }
        catch (error) {
            this.enableMessageField();
            log.error('sendMessage error:', error && error.stack ? error.stack : error);
            return;
        }
        model.clearTypingTimers();
        if (this.showInvalidMessageToast(message)) {
            this.enableMessageField();
            return;
        }
        try {
            if (!message.length &&
                !this.hasFiles({ includePending: false }) &&
                !options.voiceNoteAttachment) {
                return;
            }
            let attachments = [];
            if (options.voiceNoteAttachment) {
                attachments = [options.voiceNoteAttachment];
            }
            else if (options.draftAttachments) {
                attachments = (await Promise.all(options.draftAttachments.map(resolveAttachmentDraftData_1.resolveAttachmentDraftData))).filter(isNotNil_1.isNotNil);
            }
            const sendHQImages = window.reduxStore &&
                window.reduxStore.getState().composer.shouldSendHighQualityAttachments;
            const sendDelta = Date.now() - this.sendStart;
            log.info('Send pre-checks took', sendDelta, 'milliseconds');
            model.enqueueMessageForSend(message, attachments, this.quote, this.getLinkPreviewForSend(message), undefined, // sticker
            mentions, {
                sendHQImages,
                timestamp,
            });
            (0, react_redux_1.batch)(() => {
                var _a;
                (_a = this.compositionApi.current) === null || _a === void 0 ? void 0 : _a.reset();
                model.setMarkedUnread(false);
                this.setQuoteMessage(null);
                this.resetLinkPreview();
                this.clearAttachments();
                window.reduxActions.composer.resetComposer();
            });
        }
        catch (error) {
            log.error('Error pulling attached files before send', error && error.stack ? error.stack : error);
        }
        finally {
            this.enableMessageField();
        }
    }
    onEditorStateChange(messageText, bodyRanges, caretLocation) {
        this.maybeBumpTyping(messageText);
        this.debouncedSaveDraft(messageText, bodyRanges);
        this.debouncedMaybeGrabLinkPreview(messageText, caretLocation);
    }
    async saveDraft(messageText, bodyRanges) {
        const { model } = this;
        const trimmed = messageText && messageText.length > 0 ? messageText.trim() : '';
        if (model.get('draft') && (!messageText || trimmed.length === 0)) {
            this.model.set({
                draft: null,
                draftChanged: true,
                draftBodyRanges: [],
            });
            await this.saveModel();
            return;
        }
        if (messageText !== model.get('draft')) {
            const now = Date.now();
            let active_at = this.model.get('active_at');
            let timestamp = this.model.get('timestamp');
            if (!active_at) {
                active_at = now;
                timestamp = now;
            }
            this.model.set({
                active_at,
                draft: messageText,
                draftBodyRanges: bodyRanges,
                draftChanged: true,
                timestamp,
            });
            await this.saveModel();
        }
    }
    maybeGrabLinkPreview(message, caretLocation) {
        // Don't generate link previews if user has turned them off
        if (!window.Events.getLinkPreviewSetting()) {
            return;
        }
        // Do nothing if we're offline
        if (!window.textsecure.messaging) {
            return;
        }
        // If we have attachments, don't add link preview
        if (this.hasFiles({ includePending: true })) {
            return;
        }
        // If we're behind a user-configured proxy, we don't support link previews
        if (window.isBehindProxy()) {
            return;
        }
        if (!message) {
            this.resetLinkPreview();
            return;
        }
        if (this.disableLinkPreviews) {
            return;
        }
        const links = LinkPreview.findLinks(message, caretLocation);
        const { currentlyMatchedLink } = this;
        if (currentlyMatchedLink && links.includes(currentlyMatchedLink)) {
            return;
        }
        this.currentlyMatchedLink = undefined;
        this.excludedPreviewUrls = this.excludedPreviewUrls || [];
        const link = links.find(item => LinkPreview.isLinkSafeToPreview(item) &&
            !this.excludedPreviewUrls.includes(item));
        if (!link) {
            this.removeLinkPreview();
            return;
        }
        this.addLinkPreview(link);
    }
    resetLinkPreview() {
        this.disableLinkPreviews = false;
        this.excludedPreviewUrls = [];
        this.removeLinkPreview();
    }
    removeLinkPreview() {
        var _a;
        (this.preview || []).forEach((item) => {
            if (item.url) {
                URL.revokeObjectURL(item.url);
            }
        });
        this.preview = undefined;
        this.currentlyMatchedLink = undefined;
        (_a = this.linkPreviewAbortController) === null || _a === void 0 ? void 0 : _a.abort();
        this.linkPreviewAbortController = undefined;
        window.reduxActions.linkPreviews.removeLinkPreview();
    }
    async getStickerPackPreview(url, abortSignal) {
        const isPackDownloaded = (pack) => {
            if (!pack) {
                return false;
            }
            return pack.status === 'downloaded' || pack.status === 'installed';
        };
        const isPackValid = (pack) => {
            if (!pack) {
                return false;
            }
            return (pack.status === 'ephemeral' ||
                pack.status === 'downloaded' ||
                pack.status === 'installed');
        };
        const dataFromLink = Stickers.getDataFromLink(url);
        if (!dataFromLink) {
            return null;
        }
        const { id, key } = dataFromLink;
        try {
            const keyBytes = Bytes.fromHex(key);
            const keyBase64 = Bytes.toBase64(keyBytes);
            const existing = Stickers.getStickerPack(id);
            if (!isPackDownloaded(existing)) {
                await Stickers.downloadEphemeralPack(id, keyBase64);
            }
            if (abortSignal.aborted) {
                return null;
            }
            const pack = Stickers.getStickerPack(id);
            if (!isPackValid(pack)) {
                return null;
            }
            if (pack.key !== keyBase64) {
                return null;
            }
            const { title, coverStickerId } = pack;
            const sticker = pack.stickers[coverStickerId];
            const data = pack.status === 'ephemeral'
                ? await window.Signal.Migrations.readTempData(sticker.path)
                : await window.Signal.Migrations.readStickerData(sticker.path);
            if (abortSignal.aborted) {
                return null;
            }
            let contentType;
            const sniffedMimeType = (0, sniffImageMimeType_1.sniffImageMimeType)(data);
            if (sniffedMimeType) {
                contentType = sniffedMimeType;
            }
            else {
                log.warn('getStickerPackPreview: Unable to sniff sticker MIME type; falling back to WebP');
                contentType = MIME_1.IMAGE_WEBP;
            }
            return {
                date: null,
                description: null,
                image: Object.assign(Object.assign({}, sticker), { data, size: data.byteLength, contentType }),
                title,
                url,
            };
        }
        catch (error) {
            log.error('getStickerPackPreview error:', error && error.stack ? error.stack : error);
            return null;
        }
        finally {
            if (id) {
                await Stickers.removeEphemeralPack(id);
            }
        }
    }
    async getGroupPreview(url, abortSignal) {
        const urlObject = (0, url_1.maybeParseUrl)(url);
        if (!urlObject) {
            return null;
        }
        const { hash } = urlObject;
        if (!hash) {
            return null;
        }
        const groupData = hash.slice(1);
        const { inviteLinkPassword, masterKey } = window.Signal.Groups.parseGroupLink(groupData);
        const fields = window.Signal.Groups.deriveGroupFields(Bytes.fromBase64(masterKey));
        const id = Bytes.toBase64(fields.id);
        const logId = `groupv2(${id})`;
        const secretParams = Bytes.toBase64(fields.secretParams);
        log.info(`getGroupPreview/${logId}: Fetching pre-join state`);
        const result = await window.Signal.Groups.getPreJoinGroupInfo(inviteLinkPassword, masterKey);
        if (abortSignal.aborted) {
            return null;
        }
        const title = window.Signal.Groups.decryptGroupTitle(result.title, secretParams) ||
            window.i18n('unknownGroup');
        const description = result.memberCount === 1 || result.memberCount === undefined
            ? window.i18n('GroupV2--join--member-count--single')
            : window.i18n('GroupV2--join--member-count--multiple', {
                count: result.memberCount.toString(),
            });
        let image;
        if (result.avatar) {
            try {
                const data = await window.Signal.Groups.decryptGroupAvatar(result.avatar, secretParams);
                image = {
                    data,
                    size: data.byteLength,
                    contentType: MIME_1.IMAGE_JPEG,
                    blurHash: await window.imageToBlurHash(new Blob([data], {
                        type: MIME_1.IMAGE_JPEG,
                    })),
                };
            }
            catch (error) {
                const errorString = error && error.stack ? error.stack : error;
                log.error(`getGroupPreview/${logId}: Failed to fetch avatar ${errorString}`);
            }
        }
        if (abortSignal.aborted) {
            return null;
        }
        return {
            date: null,
            description,
            image,
            title,
            url,
        };
    }
    async getPreview(url, abortSignal) {
        if (LinkPreview.isStickerPack(url)) {
            return this.getStickerPackPreview(url, abortSignal);
        }
        if (LinkPreview.isGroupLink(url)) {
            return this.getGroupPreview(url, abortSignal);
        }
        // This is already checked elsewhere, but we want to be extra-careful.
        if (!LinkPreview.isLinkSafeToPreview(url)) {
            return null;
        }
        const linkPreviewMetadata = await window.textsecure.messaging.fetchLinkPreviewMetadata(url, abortSignal);
        if (!linkPreviewMetadata || abortSignal.aborted) {
            return null;
        }
        const { title, imageHref, description, date } = linkPreviewMetadata;
        let image;
        if (imageHref && LinkPreview.isLinkSafeToPreview(imageHref)) {
            let objectUrl;
            try {
                const fullSizeImage = await window.textsecure.messaging.fetchLinkPreviewImage(imageHref, abortSignal);
                if (abortSignal.aborted) {
                    return null;
                }
                if (!fullSizeImage) {
                    throw new Error('Failed to fetch link preview image');
                }
                // Ensure that this file is either small enough or is resized to meet our
                //   requirements for attachments
                const withBlob = await (0, handleImageAttachment_1.autoScale)({
                    contentType: fullSizeImage.contentType,
                    file: new Blob([fullSizeImage.data], {
                        type: fullSizeImage.contentType,
                    }),
                    fileName: title,
                });
                const data = await (0, fileToBytes_1.fileToBytes)(withBlob.file);
                objectUrl = URL.createObjectURL(withBlob.file);
                const blurHash = await window.imageToBlurHash(withBlob.file);
                const dimensions = await VisualAttachment.getImageDimensions({
                    objectUrl,
                    logger: log,
                });
                image = Object.assign(Object.assign({ data, size: data.byteLength }, dimensions), { contentType: (0, MIME_1.stringToMIMEType)(withBlob.file.type), blurHash });
            }
            catch (error) {
                // We still want to show the preview if we failed to get an image
                log.error('getPreview failed to get image for link preview:', error.message);
            }
            finally {
                if (objectUrl) {
                    URL.revokeObjectURL(objectUrl);
                }
            }
        }
        if (abortSignal.aborted) {
            return null;
        }
        return {
            date: date || null,
            description: description || null,
            image,
            title,
            url,
        };
    }
    async addLinkPreview(url) {
        if (this.currentlyMatchedLink === url) {
            log.warn('addLinkPreview should not be called with the same URL like this');
            return;
        }
        (this.preview || []).forEach((item) => {
            if (item.url) {
                URL.revokeObjectURL(item.url);
            }
        });
        window.reduxActions.linkPreviews.removeLinkPreview();
        this.preview = undefined;
        // Cancel other in-flight link preview requests.
        if (this.linkPreviewAbortController) {
            log.info('addLinkPreview: canceling another in-flight link preview request');
            this.linkPreviewAbortController.abort();
        }
        const thisRequestAbortController = new AbortController();
        this.linkPreviewAbortController = thisRequestAbortController;
        const timeout = setTimeout(() => {
            thisRequestAbortController.abort();
        }, LINK_PREVIEW_TIMEOUT);
        this.currentlyMatchedLink = url;
        this.renderLinkPreview();
        try {
            const result = await this.getPreview(url, thisRequestAbortController.signal);
            if (!result) {
                log.info('addLinkPreview: failed to load preview (not necessarily a problem)');
                // This helps us disambiguate between two kinds of failure:
                //
                // 1. We failed to fetch the preview because of (1) a network failure (2) an
                //    invalid response (3) a timeout
                // 2. We failed to fetch the preview because we aborted the request because the
                //    user changed the link (e.g., by continuing to type the URL)
                const failedToFetch = this.currentlyMatchedLink === url;
                if (failedToFetch) {
                    this.excludedPreviewUrls.push(url);
                    this.removeLinkPreview();
                }
                return;
            }
            if (result.image && result.image.data) {
                const blob = new Blob([result.image.data], {
                    type: result.image.contentType,
                });
                result.image.url = URL.createObjectURL(blob);
            }
            else if (!result.title) {
                // A link preview isn't worth showing unless we have either a title or an image
                this.removeLinkPreview();
                return;
            }
            window.reduxActions.linkPreviews.addLinkPreview(Object.assign(Object.assign({}, result), { description: (0, dropNull_1.dropNull)(result.description), date: (0, dropNull_1.dropNull)(result.date), domain: LinkPreview.getDomain(result.url), isStickerPack: LinkPreview.isStickerPack(result.url) }));
            this.preview = [result];
            this.renderLinkPreview();
        }
        catch (error) {
            log.error('Problem loading link preview, disabling.', error && error.stack ? error.stack : error);
            this.disableLinkPreviews = true;
            this.removeLinkPreview();
        }
        finally {
            clearTimeout(timeout);
        }
    }
    renderLinkPreview() {
        if (this.forwardMessageModal) {
            return;
        }
        window.reduxActions.composer.setLinkPreviewResult(Boolean(this.currentlyMatchedLink), this.getLinkPreviewWithDomain());
    }
    getLinkPreviewForSend(message) {
        // Don't generate link previews if user has turned them off
        if (!window.storage.get('linkPreviews', false)) {
            return [];
        }
        if (!this.preview) {
            return [];
        }
        const urlsInMessage = new Set(LinkPreview.findLinks(message));
        return (this.preview
            // This bullet-proofs against sending link previews for URLs that are no longer in
            //   the message. This can happen if you have a link preview, then quickly delete
            //   the link and send the message.
            .filter(({ url }) => urlsInMessage.has(url))
            .map((item) => {
            if (item.image) {
                // We eliminate the ObjectURL here, unneeded for send or save
                return Object.assign(Object.assign({}, item), { image: (0, lodash_1.omit)(item.image, 'url'), description: (0, dropNull_1.dropNull)(item.description), date: (0, dropNull_1.dropNull)(item.date), domain: LinkPreview.getDomain(item.url), isStickerPack: LinkPreview.isStickerPack(item.url) });
            }
            return Object.assign(Object.assign({}, item), { description: (0, dropNull_1.dropNull)(item.description), date: (0, dropNull_1.dropNull)(item.date), domain: LinkPreview.getDomain(item.url), isStickerPack: LinkPreview.isStickerPack(item.url) });
        }));
    }
    getLinkPreviewWithDomain() {
        if (!this.preview || !this.preview.length) {
            return undefined;
        }
        const [preview] = this.preview;
        return Object.assign(Object.assign({}, preview), { domain: LinkPreview.getDomain(preview.url) });
    }
    // Called whenever the user changes the message composition field. But only
    //   fires if there's content in the message field after the change.
    maybeBumpTyping(messageText) {
        if (messageText.length && this.model.throttledBumpTyping) {
            this.model.throttledBumpTyping();
        }
    }
}
exports.ConversationView = ConversationView;
window.Whisper.ConversationView = ConversationView;
