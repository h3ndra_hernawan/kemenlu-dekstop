"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.clearTimeout = exports.setTimeout = void 0;
const { timers } = window.SignalContext;
function setTimeout(...args) {
    return timers.setTimeout(...args);
}
exports.setTimeout = setTimeout;
function clearTimeout(...args) {
    return timers.clearTimeout(...args);
}
exports.clearTimeout = clearTimeout;
