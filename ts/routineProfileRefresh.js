"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.routineProfileRefresh = void 0;
const lodash_1 = require("lodash");
const p_queue_1 = __importDefault(require("p-queue"));
const log = __importStar(require("./logging/log"));
const assert_1 = require("./util/assert");
const missingCaseError_1 = require("./util/missingCaseError");
const isNormalNumber_1 = require("./util/isNormalNumber");
const iterables_1 = require("./util/iterables");
const timestamp_1 = require("./util/timestamp");
// Imported this way so that sinon.sandbox can stub this properly
const profileGetter = __importStar(require("./util/getProfile"));
const STORAGE_KEY = 'lastAttemptedToRefreshProfilesAt';
const MAX_AGE_TO_BE_CONSIDERED_ACTIVE = 30 * 24 * 60 * 60 * 1000;
const MAX_AGE_TO_BE_CONSIDERED_RECENTLY_REFRESHED = 1 * 24 * 60 * 60 * 1000;
const MAX_CONVERSATIONS_TO_REFRESH = 50;
const MIN_ELAPSED_DURATION_TO_REFRESH_AGAIN = 12 * 3600 * 1000;
async function routineProfileRefresh({ allConversations, ourConversationId, storage, }) {
    log.info('routineProfileRefresh: starting');
    if (!hasEnoughTimeElapsedSinceLastRefresh(storage)) {
        log.info('routineProfileRefresh: too soon to refresh. Doing nothing');
        return;
    }
    log.info('routineProfileRefresh: updating last refresh time');
    await storage.put(STORAGE_KEY, Date.now());
    const conversationsToRefresh = getConversationsToRefresh(allConversations, ourConversationId);
    log.info('routineProfileRefresh: starting to refresh conversations');
    let totalCount = 0;
    let successCount = 0;
    async function refreshConversation(conversation) {
        log.info(`routineProfileRefresh: refreshing profile for ${conversation.idForLogging()}`);
        totalCount += 1;
        try {
            await profileGetter.getProfile(conversation.get('uuid'), conversation.get('e164'));
            log.info(`routineProfileRefresh: refreshed profile for ${conversation.idForLogging()}`);
            successCount += 1;
        }
        catch (err) {
            log.error(`routineProfileRefresh: refreshed profile for ${conversation.idForLogging()}`, (err === null || err === void 0 ? void 0 : err.stack) || err);
        }
    }
    const refreshQueue = new p_queue_1.default({ concurrency: 5, timeout: 1000 * 60 * 2 });
    for (const conversation of conversationsToRefresh) {
        refreshQueue.add(() => refreshConversation(conversation));
    }
    await refreshQueue.onIdle();
    log.info(`routineProfileRefresh: successfully refreshed ${successCount} out of ${totalCount} conversation(s)`);
}
exports.routineProfileRefresh = routineProfileRefresh;
function hasEnoughTimeElapsedSinceLastRefresh(storage) {
    const storedValue = storage.get(STORAGE_KEY);
    if ((0, lodash_1.isNil)(storedValue)) {
        return true;
    }
    if ((0, isNormalNumber_1.isNormalNumber)(storedValue)) {
        return (0, timestamp_1.isOlderThan)(storedValue, MIN_ELAPSED_DURATION_TO_REFRESH_AGAIN);
    }
    (0, assert_1.assert)(false, `An invalid value was stored in ${STORAGE_KEY}; treating it as nil`);
    return true;
}
function getConversationsToRefresh(conversations, ourConversationId) {
    const filteredConversations = getFilteredConversations(conversations, ourConversationId);
    return (0, iterables_1.take)(filteredConversations, MAX_CONVERSATIONS_TO_REFRESH);
}
function* getFilteredConversations(conversations, ourConversationId) {
    const sorted = (0, lodash_1.sortBy)(conversations, c => c.get('active_at'));
    const conversationIdsSeen = new Set([ourConversationId]);
    for (const conversation of sorted) {
        const type = conversation.get('type');
        switch (type) {
            case 'private':
                if (!conversationIdsSeen.has(conversation.id) &&
                    isConversationActive(conversation) &&
                    !hasRefreshedProfileRecently(conversation)) {
                    conversationIdsSeen.add(conversation.id);
                    yield conversation;
                }
                break;
            case 'group':
                for (const member of conversation.getMembers()) {
                    if (!conversationIdsSeen.has(member.id) &&
                        !hasRefreshedProfileRecently(member)) {
                        conversationIdsSeen.add(member.id);
                        yield member;
                    }
                }
                break;
            default:
                throw (0, missingCaseError_1.missingCaseError)(type);
        }
    }
}
function isConversationActive(conversation) {
    const activeAt = conversation.get('active_at');
    return ((0, isNormalNumber_1.isNormalNumber)(activeAt) &&
        activeAt + MAX_AGE_TO_BE_CONSIDERED_ACTIVE > Date.now());
}
function hasRefreshedProfileRecently(conversation) {
    const profileLastFetchedAt = conversation.get('profileLastFetchedAt');
    return ((0, isNormalNumber_1.isNormalNumber)(profileLastFetchedAt) &&
        profileLastFetchedAt + MAX_AGE_TO_BE_CONSIDERED_RECENTLY_REFRESHED >
            Date.now());
}
