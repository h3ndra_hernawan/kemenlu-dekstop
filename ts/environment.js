"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isTestEnvironment = exports.parseEnvironment = exports.setEnvironment = exports.getEnvironment = exports.Environment = void 0;
const enum_1 = require("./util/enum");
// Many places rely on this enum being a string.
var Environment;
(function (Environment) {
    Environment["Development"] = "development";
    Environment["Production"] = "production";
    Environment["Staging"] = "staging";
    Environment["Test"] = "test";
    Environment["TestLib"] = "test-lib";
})(Environment = exports.Environment || (exports.Environment = {}));
let environment;
function getEnvironment() {
    if (environment === undefined) {
        // This should never happen—we should always have initialized the environment by this
        //   point. It'd be nice to log here but the logger depends on the environment and we
        //   can't have circular dependencies.
        return Environment.Production;
    }
    return environment;
}
exports.getEnvironment = getEnvironment;
/**
 * Sets the current environment. Should be called early in a process's life, and can only
 * be called once.
 */
function setEnvironment(env) {
    if (environment !== undefined) {
        throw new Error('Environment has already been set');
    }
    environment = env;
}
exports.setEnvironment = setEnvironment;
exports.parseEnvironment = (0, enum_1.makeEnumParser)(Environment, Environment.Production);
const isTestEnvironment = (env) => env === Environment.Test || env === Environment.TestLib;
exports.isTestEnvironment = isTestEnvironment;
