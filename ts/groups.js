"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMembershipList = exports.decryptGroupDescription = exports.decryptGroupTitle = exports.applyNewAvatar = exports.decryptGroupAvatar = exports.maybeUpdateGroup = exports.waitThenMaybeUpdateGroup = exports.respondToGroupV2Migration = exports.joinGroupV2ViaLinkAndMigrate = exports.buildMigrationBubble = exports.waitThenRespondToGroupV2Migration = exports.wrapWithSyncMessageSend = exports.initiateMigrationToGroupV2 = exports.getGroupMigrationMembers = exports.isGroupEligibleToMigrate = exports.maybeDeriveGroupV2Id = exports.hasV1GroupBeenMigrated = exports.createGroupV2 = exports.fetchMembershipProof = exports.deriveGroupFields = exports.idForLogging = exports.modifyGroupV2 = exports.uploadGroupChange = exports.buildPromoteMemberChange = exports.buildPromotePendingAdminApprovalMemberChange = exports.buildModifyMemberRoleChange = exports.buildDeleteMemberChange = exports.buildDeletePendingMemberChange = exports.buildAddMember = exports.buildAddPendingAdminApprovalMemberChange = exports.buildDeletePendingAdminApprovalMemberChange = exports.buildAccessControlMembersChange = exports.buildAccessControlAttributesChange = exports.buildAnnouncementsOnlyChange = exports.buildAccessControlAddFromInviteLinkChange = exports.buildNewGroupLinkChange = exports.buildInviteLinkPasswordChange = exports.buildDisappearingMessagesTimerChange = exports.buildUpdateAttributesChange = exports.buildAddMembersChange = exports.parseGroupLink = exports.buildGroupLink = exports.getPreJoinGroupInfo = exports.generateGroupInviteLinkPassword = exports.LINK_VERSION_ERROR = exports.ID_LENGTH = exports.ID_V1_LENGTH = exports.MASTER_KEY_LENGTH = exports.joinViaLink = void 0;
const lodash_1 = require("lodash");
const uuid_1 = require("uuid");
const lru_cache_1 = __importDefault(require("lru-cache"));
const log = __importStar(require("./logging/log"));
const groupCredentialFetcher_1 = require("./services/groupCredentialFetcher");
const Client_1 = __importDefault(require("./sql/Client"));
const webSafeBase64_1 = require("./util/webSafeBase64");
const assert_1 = require("./util/assert");
const timestamp_1 = require("./util/timestamp");
const durations = __importStar(require("./util/durations"));
const normalizeUuid_1 = require("./util/normalizeUuid");
const dropNull_1 = require("./util/dropNull");
const zkgroup_1 = require("./util/zkgroup");
const Crypto_1 = require("./Crypto");
const message_1 = require("../js/modules/types/message");
const limits_1 = require("./groups/limits");
const ourProfileKey_1 = require("./services/ourProfileKey");
const whatTypeOfConversation_1 = require("./util/whatTypeOfConversation");
const handleMessageSend_1 = require("./util/handleMessageSend");
const getSendOptions_1 = require("./util/getSendOptions");
const Bytes = __importStar(require("./Bytes"));
const UUID_1 = require("./types/UUID");
const protobuf_1 = require("./protobuf");
var joinViaLink_1 = require("./groups/joinViaLink");
Object.defineProperty(exports, "joinViaLink", { enumerable: true, get: function () { return joinViaLink_1.joinViaLink; } });
const MAX_CACHED_GROUP_FIELDS = 100;
const groupFieldsCache = new lru_cache_1.default({
    max: MAX_CACHED_GROUP_FIELDS,
});
const { updateConversation } = Client_1.default;
if (!(0, lodash_1.isNumber)(message_1.CURRENT_SCHEMA_VERSION)) {
    throw new Error('groups.ts: Unable to capture max message schema from js/modules/types/message');
}
// Constants
exports.MASTER_KEY_LENGTH = 32;
const GROUP_TITLE_MAX_ENCRYPTED_BYTES = 1024;
const GROUP_DESC_MAX_ENCRYPTED_BYTES = 8192;
exports.ID_V1_LENGTH = 16;
exports.ID_LENGTH = 32;
const TEMPORAL_AUTH_REJECTED_CODE = 401;
const GROUP_ACCESS_DENIED_CODE = 403;
const GROUP_NONEXISTENT_CODE = 404;
const SUPPORTED_CHANGE_EPOCH = 2;
exports.LINK_VERSION_ERROR = 'LINK_VERSION_ERROR';
const GROUP_INVITE_LINK_PASSWORD_LENGTH = 16;
// Group Links
function generateGroupInviteLinkPassword() {
    return (0, Crypto_1.getRandomBytes)(GROUP_INVITE_LINK_PASSWORD_LENGTH);
}
exports.generateGroupInviteLinkPassword = generateGroupInviteLinkPassword;
// Group Links
async function getPreJoinGroupInfo(inviteLinkPasswordBase64, masterKeyBase64) {
    const data = window.Signal.Groups.deriveGroupFields(Bytes.fromBase64(masterKeyBase64));
    return makeRequestWithTemporalRetry({
        logId: `groupv2(${data.id})`,
        publicParams: Bytes.toBase64(data.publicParams),
        secretParams: Bytes.toBase64(data.secretParams),
        request: (sender, options) => sender.getGroupFromLink(inviteLinkPasswordBase64, options),
    });
}
exports.getPreJoinGroupInfo = getPreJoinGroupInfo;
function buildGroupLink(conversation) {
    const { masterKey, groupInviteLinkPassword } = conversation.attributes;
    const bytes = protobuf_1.SignalService.GroupInviteLink.encode({
        v1Contents: {
            groupMasterKey: Bytes.fromBase64(masterKey),
            inviteLinkPassword: Bytes.fromBase64(groupInviteLinkPassword),
        },
    }).finish();
    const hash = (0, webSafeBase64_1.toWebSafeBase64)(Bytes.toBase64(bytes));
    return `https://signal.group/#${hash}`;
}
exports.buildGroupLink = buildGroupLink;
function parseGroupLink(hash) {
    const base64 = (0, webSafeBase64_1.fromWebSafeBase64)(hash);
    const buffer = Bytes.fromBase64(base64);
    const inviteLinkProto = protobuf_1.SignalService.GroupInviteLink.decode(buffer);
    if (inviteLinkProto.contents !== 'v1Contents' ||
        !inviteLinkProto.v1Contents) {
        const error = new Error('parseGroupLink: Parsed proto is missing v1Contents');
        error.name = exports.LINK_VERSION_ERROR;
        throw error;
    }
    const { groupMasterKey: groupMasterKeyRaw, inviteLinkPassword: inviteLinkPasswordRaw, } = inviteLinkProto.v1Contents;
    if (!groupMasterKeyRaw || !groupMasterKeyRaw.length) {
        throw new Error('v1Contents.groupMasterKey had no data!');
    }
    if (!inviteLinkPasswordRaw || !inviteLinkPasswordRaw.length) {
        throw new Error('v1Contents.inviteLinkPassword had no data!');
    }
    const masterKey = Bytes.toBase64(groupMasterKeyRaw);
    if (masterKey.length !== 44) {
        throw new Error(`masterKey had unexpected length ${masterKey.length}`);
    }
    const inviteLinkPassword = Bytes.toBase64(inviteLinkPasswordRaw);
    if (inviteLinkPassword.length === 0) {
        throw new Error(`inviteLinkPassword had unexpected length ${inviteLinkPassword.length}`);
    }
    return { masterKey, inviteLinkPassword };
}
exports.parseGroupLink = parseGroupLink;
// Group Modifications
async function uploadAvatar(options) {
    const { logId, publicParams, secretParams } = options;
    try {
        const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(secretParams);
        let data;
        if ('data' in options) {
            ({ data } = options);
        }
        else {
            data = await window.Signal.Migrations.readAttachmentData(options.path);
        }
        const hash = (0, Crypto_1.computeHash)(data);
        const blobPlaintext = protobuf_1.SignalService.GroupAttributeBlob.encode({
            avatar: data,
        }).finish();
        const ciphertext = (0, zkgroup_1.encryptGroupBlob)(clientZkGroupCipher, blobPlaintext);
        const key = await makeRequestWithTemporalRetry({
            logId: `uploadGroupAvatar/${logId}`,
            publicParams,
            secretParams,
            request: (sender, requestOptions) => sender.uploadGroupAvatar(ciphertext, requestOptions),
        });
        return {
            data,
            hash,
            key,
        };
    }
    catch (error) {
        log.warn(`uploadAvatar/${logId} Failed to upload avatar`, error.stack);
        throw error;
    }
}
function buildGroupTitleBuffer(clientZkGroupCipher, title) {
    const titleBlobPlaintext = protobuf_1.SignalService.GroupAttributeBlob.encode({
        title,
    }).finish();
    const result = (0, zkgroup_1.encryptGroupBlob)(clientZkGroupCipher, titleBlobPlaintext);
    if (result.byteLength > GROUP_TITLE_MAX_ENCRYPTED_BYTES) {
        throw new Error('buildGroupTitleBuffer: encrypted group title is too long');
    }
    return result;
}
function buildGroupDescriptionBuffer(clientZkGroupCipher, description) {
    const attrsBlobPlaintext = protobuf_1.SignalService.GroupAttributeBlob.encode({
        descriptionText: description,
    }).finish();
    const result = (0, zkgroup_1.encryptGroupBlob)(clientZkGroupCipher, attrsBlobPlaintext);
    if (result.byteLength > GROUP_DESC_MAX_ENCRYPTED_BYTES) {
        throw new Error('buildGroupDescriptionBuffer: encrypted group title is too long');
    }
    return result;
}
function buildGroupProto(attributes) {
    const MEMBER_ROLE_ENUM = protobuf_1.SignalService.Member.Role;
    const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
    const logId = `groupv2(${attributes.id})`;
    const { publicParams, secretParams } = attributes;
    if (!publicParams) {
        throw new Error(`buildGroupProto/${logId}: attributes were missing publicParams!`);
    }
    if (!secretParams) {
        throw new Error(`buildGroupProto/${logId}: attributes were missing secretParams!`);
    }
    const serverPublicParamsBase64 = window.getServerPublicParams();
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(secretParams);
    const clientZkProfileCipher = (0, zkgroup_1.getClientZkProfileOperations)(serverPublicParamsBase64);
    const proto = new protobuf_1.SignalService.Group();
    proto.publicKey = Bytes.fromBase64(publicParams);
    proto.version = attributes.revision || 0;
    if (attributes.name) {
        proto.title = buildGroupTitleBuffer(clientZkGroupCipher, attributes.name);
    }
    if (attributes.avatarUrl) {
        proto.avatar = attributes.avatarUrl;
    }
    if (attributes.expireTimer) {
        const timerBlobPlaintext = protobuf_1.SignalService.GroupAttributeBlob.encode({
            disappearingMessagesDuration: attributes.expireTimer,
        }).finish();
        proto.disappearingMessagesTimer = (0, zkgroup_1.encryptGroupBlob)(clientZkGroupCipher, timerBlobPlaintext);
    }
    const accessControl = new protobuf_1.SignalService.AccessControl();
    if (attributes.accessControl) {
        accessControl.attributes =
            attributes.accessControl.attributes || ACCESS_ENUM.MEMBER;
        accessControl.members =
            attributes.accessControl.members || ACCESS_ENUM.MEMBER;
    }
    else {
        accessControl.attributes = ACCESS_ENUM.MEMBER;
        accessControl.members = ACCESS_ENUM.MEMBER;
    }
    proto.accessControl = accessControl;
    proto.members = (attributes.membersV2 || []).map(item => {
        const member = new protobuf_1.SignalService.Member();
        const conversation = window.ConversationController.get(item.uuid);
        if (!conversation) {
            throw new Error(`buildGroupProto/${logId}: no conversation for member!`);
        }
        const profileKeyCredentialBase64 = conversation.get('profileKeyCredential');
        if (!profileKeyCredentialBase64) {
            throw new Error(`buildGroupProto/${logId}: member was missing profileKeyCredential!`);
        }
        const presentation = (0, zkgroup_1.createProfileKeyCredentialPresentation)(clientZkProfileCipher, profileKeyCredentialBase64, secretParams);
        member.role = item.role || MEMBER_ROLE_ENUM.DEFAULT;
        member.presentation = presentation;
        return member;
    });
    const ourUuid = window.storage.user.getCheckedUuid();
    const ourUuidCipherTextBuffer = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, ourUuid.toString());
    proto.membersPendingProfileKey = (attributes.pendingMembersV2 || []).map(item => {
        const pendingMember = new protobuf_1.SignalService.MemberPendingProfileKey();
        const member = new protobuf_1.SignalService.Member();
        const conversation = window.ConversationController.get(item.uuid);
        if (!conversation) {
            throw new Error('buildGroupProto: no conversation for pending member!');
        }
        const uuid = conversation.get('uuid');
        if (!uuid) {
            throw new Error('buildGroupProto: pending member was missing uuid!');
        }
        const uuidCipherTextBuffer = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, uuid);
        member.userId = uuidCipherTextBuffer;
        member.role = item.role || MEMBER_ROLE_ENUM.DEFAULT;
        pendingMember.member = member;
        pendingMember.timestamp = item.timestamp;
        pendingMember.addedByUserId = ourUuidCipherTextBuffer;
        return pendingMember;
    });
    return proto;
}
async function buildAddMembersChange(conversation, conversationIds) {
    const MEMBER_ROLE_ENUM = protobuf_1.SignalService.Member.Role;
    const { id, publicParams, revision, secretParams } = conversation;
    const logId = `groupv2(${id})`;
    if (!publicParams) {
        throw new Error(`buildAddMembersChange/${logId}: attributes were missing publicParams!`);
    }
    if (!secretParams) {
        throw new Error(`buildAddMembersChange/${logId}: attributes were missing secretParams!`);
    }
    const newGroupVersion = (revision || 0) + 1;
    const serverPublicParamsBase64 = window.getServerPublicParams();
    const clientZkProfileCipher = (0, zkgroup_1.getClientZkProfileOperations)(serverPublicParamsBase64);
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(secretParams);
    const ourUuid = window.storage.user.getCheckedUuid();
    const ourUuidCipherTextBuffer = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, ourUuid.toString());
    const now = Date.now();
    const addMembers = [];
    const addPendingMembers = [];
    await Promise.all(conversationIds.map(async (conversationId) => {
        var _a, _b;
        const contact = window.ConversationController.get(conversationId);
        if (!contact) {
            (0, assert_1.assert)(false, `buildAddMembersChange/${logId}: missing local contact, skipping`);
            return;
        }
        const uuid = contact.get('uuid');
        if (!uuid) {
            (0, assert_1.assert)(false, `buildAddMembersChange/${logId}: missing UUID; skipping`);
            return;
        }
        // Refresh our local data to be sure
        if (!((_a = contact.get('capabilities')) === null || _a === void 0 ? void 0 : _a.gv2) ||
            !contact.get('profileKey') ||
            !contact.get('profileKeyCredential')) {
            await contact.getProfiles();
        }
        if (!((_b = contact.get('capabilities')) === null || _b === void 0 ? void 0 : _b.gv2)) {
            (0, assert_1.assert)(false, `buildAddMembersChange/${logId}: member is missing GV2 capability; skipping`);
            return;
        }
        const profileKey = contact.get('profileKey');
        const profileKeyCredential = contact.get('profileKeyCredential');
        if (!profileKey) {
            (0, assert_1.assert)(false, `buildAddMembersChange/${logId}: member is missing profile key; skipping`);
            return;
        }
        const member = new protobuf_1.SignalService.Member();
        member.userId = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, uuid);
        member.role = MEMBER_ROLE_ENUM.DEFAULT;
        member.joinedAtVersion = newGroupVersion;
        // This is inspired by [Android's equivalent code][0].
        //
        // [0]: https://github.com/signalapp/Signal-Android/blob/2be306867539ab1526f0e49d1aa7bd61e783d23f/libsignal/service/src/main/java/org/whispersystems/signalservice/api/groupsv2/GroupsV2Operations.java#L152-L174
        if (profileKey && profileKeyCredential) {
            member.presentation = (0, zkgroup_1.createProfileKeyCredentialPresentation)(clientZkProfileCipher, profileKeyCredential, secretParams);
            const addMemberAction = new protobuf_1.SignalService.GroupChange.Actions.AddMemberAction();
            addMemberAction.added = member;
            addMemberAction.joinFromInviteLink = false;
            addMembers.push(addMemberAction);
        }
        else {
            const memberPendingProfileKey = new protobuf_1.SignalService.MemberPendingProfileKey();
            memberPendingProfileKey.member = member;
            memberPendingProfileKey.addedByUserId = ourUuidCipherTextBuffer;
            memberPendingProfileKey.timestamp = now;
            const addPendingMemberAction = new protobuf_1.SignalService.GroupChange.Actions.AddMemberPendingProfileKeyAction();
            addPendingMemberAction.added = memberPendingProfileKey;
            addPendingMembers.push(addPendingMemberAction);
        }
    }));
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    if (!addMembers.length && !addPendingMembers.length) {
        // This shouldn't happen. When these actions are passed to `modifyGroupV2`, a warning
        //   will be logged.
        return undefined;
    }
    if (addMembers.length) {
        actions.addMembers = addMembers;
    }
    if (addPendingMembers.length) {
        actions.addPendingMembers = addPendingMembers;
    }
    actions.version = newGroupVersion;
    return actions;
}
exports.buildAddMembersChange = buildAddMembersChange;
async function buildUpdateAttributesChange(conversation, attributes) {
    const { publicParams, secretParams, revision, id } = conversation;
    const logId = `groupv2(${id})`;
    if (!publicParams) {
        throw new Error(`buildUpdateAttributesChange/${logId}: attributes were missing publicParams!`);
    }
    if (!secretParams) {
        throw new Error(`buildUpdateAttributesChange/${logId}: attributes were missing secretParams!`);
    }
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    let hasChangedSomething = false;
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(secretParams);
    // There are three possible states here:
    //
    // 1. 'avatar' not in attributes: we don't want to change the avatar.
    // 2. attributes.avatar === undefined: we want to clear the avatar.
    // 3. attributes.avatar !== undefined: we want to update the avatar.
    if ('avatar' in attributes) {
        hasChangedSomething = true;
        actions.modifyAvatar = new protobuf_1.SignalService.GroupChange.Actions.ModifyAvatarAction();
        const { avatar } = attributes;
        if (avatar) {
            const uploadedAvatar = await uploadAvatar({
                data: avatar,
                logId,
                publicParams,
                secretParams,
            });
            actions.modifyAvatar.avatar = uploadedAvatar.key;
        }
        // If we don't set `actions.modifyAvatar.avatar`, it will be cleared.
    }
    const { title } = attributes;
    if (title) {
        hasChangedSomething = true;
        actions.modifyTitle = new protobuf_1.SignalService.GroupChange.Actions.ModifyTitleAction();
        actions.modifyTitle.title = buildGroupTitleBuffer(clientZkGroupCipher, title);
    }
    const { description } = attributes;
    if (typeof description === 'string') {
        hasChangedSomething = true;
        actions.modifyDescription =
            new protobuf_1.SignalService.GroupChange.Actions.ModifyDescriptionAction();
        actions.modifyDescription.descriptionBytes = buildGroupDescriptionBuffer(clientZkGroupCipher, description);
    }
    if (!hasChangedSomething) {
        // This shouldn't happen. When these actions are passed to `modifyGroupV2`, a warning
        //   will be logged.
        return undefined;
    }
    actions.version = (revision || 0) + 1;
    return actions;
}
exports.buildUpdateAttributesChange = buildUpdateAttributesChange;
function buildDisappearingMessagesTimerChange({ expireTimer, group, }) {
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    const blob = new protobuf_1.SignalService.GroupAttributeBlob();
    blob.disappearingMessagesDuration = expireTimer;
    if (!group.secretParams) {
        throw new Error('buildDisappearingMessagesTimerChange: group was missing secretParams!');
    }
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(group.secretParams);
    const blobPlaintext = protobuf_1.SignalService.GroupAttributeBlob.encode(blob).finish();
    const blobCipherText = (0, zkgroup_1.encryptGroupBlob)(clientZkGroupCipher, blobPlaintext);
    const timerAction = new protobuf_1.SignalService.GroupChange.Actions.ModifyDisappearingMessagesTimerAction();
    timerAction.timer = blobCipherText;
    actions.version = (group.revision || 0) + 1;
    actions.modifyDisappearingMessagesTimer = timerAction;
    return actions;
}
exports.buildDisappearingMessagesTimerChange = buildDisappearingMessagesTimerChange;
function buildInviteLinkPasswordChange(group, inviteLinkPassword) {
    const inviteLinkPasswordAction = new protobuf_1.SignalService.GroupChange.Actions.ModifyInviteLinkPasswordAction();
    inviteLinkPasswordAction.inviteLinkPassword =
        Bytes.fromBase64(inviteLinkPassword);
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    actions.version = (group.revision || 0) + 1;
    actions.modifyInviteLinkPassword = inviteLinkPasswordAction;
    return actions;
}
exports.buildInviteLinkPasswordChange = buildInviteLinkPasswordChange;
function buildNewGroupLinkChange(group, inviteLinkPassword, addFromInviteLinkAccess) {
    const accessControlAction = new protobuf_1.SignalService.GroupChange.Actions.ModifyAddFromInviteLinkAccessControlAction();
    accessControlAction.addFromInviteLinkAccess = addFromInviteLinkAccess;
    const inviteLinkPasswordAction = new protobuf_1.SignalService.GroupChange.Actions.ModifyInviteLinkPasswordAction();
    inviteLinkPasswordAction.inviteLinkPassword =
        Bytes.fromBase64(inviteLinkPassword);
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    actions.version = (group.revision || 0) + 1;
    actions.modifyAddFromInviteLinkAccess = accessControlAction;
    actions.modifyInviteLinkPassword = inviteLinkPasswordAction;
    return actions;
}
exports.buildNewGroupLinkChange = buildNewGroupLinkChange;
function buildAccessControlAddFromInviteLinkChange(group, value) {
    const accessControlAction = new protobuf_1.SignalService.GroupChange.Actions.ModifyAddFromInviteLinkAccessControlAction();
    accessControlAction.addFromInviteLinkAccess = value;
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    actions.version = (group.revision || 0) + 1;
    actions.modifyAddFromInviteLinkAccess = accessControlAction;
    return actions;
}
exports.buildAccessControlAddFromInviteLinkChange = buildAccessControlAddFromInviteLinkChange;
function buildAnnouncementsOnlyChange(group, value) {
    const action = new protobuf_1.SignalService.GroupChange.Actions.ModifyAnnouncementsOnlyAction();
    action.announcementsOnly = value;
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    actions.version = (group.revision || 0) + 1;
    actions.modifyAnnouncementsOnly = action;
    return actions;
}
exports.buildAnnouncementsOnlyChange = buildAnnouncementsOnlyChange;
function buildAccessControlAttributesChange(group, value) {
    const accessControlAction = new protobuf_1.SignalService.GroupChange.Actions.ModifyAttributesAccessControlAction();
    accessControlAction.attributesAccess = value;
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    actions.version = (group.revision || 0) + 1;
    actions.modifyAttributesAccess = accessControlAction;
    return actions;
}
exports.buildAccessControlAttributesChange = buildAccessControlAttributesChange;
function buildAccessControlMembersChange(group, value) {
    const accessControlAction = new protobuf_1.SignalService.GroupChange.Actions.ModifyMembersAccessControlAction();
    accessControlAction.membersAccess = value;
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    actions.version = (group.revision || 0) + 1;
    actions.modifyMemberAccess = accessControlAction;
    return actions;
}
exports.buildAccessControlMembersChange = buildAccessControlMembersChange;
// TODO AND-1101
function buildDeletePendingAdminApprovalMemberChange({ group, uuid, }) {
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    if (!group.secretParams) {
        throw new Error('buildDeletePendingAdminApprovalMemberChange: group was missing secretParams!');
    }
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(group.secretParams);
    const uuidCipherTextBuffer = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, uuid);
    const deleteMemberPendingAdminApproval = new protobuf_1.SignalService.GroupChange.Actions.DeleteMemberPendingAdminApprovalAction();
    deleteMemberPendingAdminApproval.deletedUserId = uuidCipherTextBuffer;
    actions.version = (group.revision || 0) + 1;
    actions.deleteMemberPendingAdminApprovals = [
        deleteMemberPendingAdminApproval,
    ];
    return actions;
}
exports.buildDeletePendingAdminApprovalMemberChange = buildDeletePendingAdminApprovalMemberChange;
function buildAddPendingAdminApprovalMemberChange({ group, profileKeyCredentialBase64, serverPublicParamsBase64, }) {
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    if (!group.secretParams) {
        throw new Error('buildAddPendingAdminApprovalMemberChange: group was missing secretParams!');
    }
    const clientZkProfileCipher = (0, zkgroup_1.getClientZkProfileOperations)(serverPublicParamsBase64);
    const addMemberPendingAdminApproval = new protobuf_1.SignalService.GroupChange.Actions.AddMemberPendingAdminApprovalAction();
    const presentation = (0, zkgroup_1.createProfileKeyCredentialPresentation)(clientZkProfileCipher, profileKeyCredentialBase64, group.secretParams);
    const added = new protobuf_1.SignalService.MemberPendingAdminApproval();
    added.presentation = presentation;
    addMemberPendingAdminApproval.added = added;
    actions.version = (group.revision || 0) + 1;
    actions.addMemberPendingAdminApprovals = [addMemberPendingAdminApproval];
    return actions;
}
exports.buildAddPendingAdminApprovalMemberChange = buildAddPendingAdminApprovalMemberChange;
function buildAddMember({ group, profileKeyCredentialBase64, serverPublicParamsBase64, }) {
    const MEMBER_ROLE_ENUM = protobuf_1.SignalService.Member.Role;
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    if (!group.secretParams) {
        throw new Error('buildAddMember: group was missing secretParams!');
    }
    const clientZkProfileCipher = (0, zkgroup_1.getClientZkProfileOperations)(serverPublicParamsBase64);
    const addMember = new protobuf_1.SignalService.GroupChange.Actions.AddMemberAction();
    const presentation = (0, zkgroup_1.createProfileKeyCredentialPresentation)(clientZkProfileCipher, profileKeyCredentialBase64, group.secretParams);
    const added = new protobuf_1.SignalService.Member();
    added.presentation = presentation;
    added.role = MEMBER_ROLE_ENUM.DEFAULT;
    addMember.added = added;
    actions.version = (group.revision || 0) + 1;
    actions.addMembers = [addMember];
    return actions;
}
exports.buildAddMember = buildAddMember;
function buildDeletePendingMemberChange({ uuids, group, }) {
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    if (!group.secretParams) {
        throw new Error('buildDeletePendingMemberChange: group was missing secretParams!');
    }
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(group.secretParams);
    const deletePendingMembers = uuids.map(uuid => {
        const uuidCipherTextBuffer = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, uuid);
        const deletePendingMember = new protobuf_1.SignalService.GroupChange.Actions.DeleteMemberPendingProfileKeyAction();
        deletePendingMember.deletedUserId = uuidCipherTextBuffer;
        return deletePendingMember;
    });
    actions.version = (group.revision || 0) + 1;
    actions.deletePendingMembers = deletePendingMembers;
    return actions;
}
exports.buildDeletePendingMemberChange = buildDeletePendingMemberChange;
function buildDeleteMemberChange({ uuid, group, }) {
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    if (!group.secretParams) {
        throw new Error('buildDeleteMemberChange: group was missing secretParams!');
    }
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(group.secretParams);
    const uuidCipherTextBuffer = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, uuid);
    const deleteMember = new protobuf_1.SignalService.GroupChange.Actions.DeleteMemberAction();
    deleteMember.deletedUserId = uuidCipherTextBuffer;
    actions.version = (group.revision || 0) + 1;
    actions.deleteMembers = [deleteMember];
    return actions;
}
exports.buildDeleteMemberChange = buildDeleteMemberChange;
function buildModifyMemberRoleChange({ uuid, group, role, }) {
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    if (!group.secretParams) {
        throw new Error('buildMakeAdminChange: group was missing secretParams!');
    }
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(group.secretParams);
    const uuidCipherTextBuffer = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, uuid);
    const toggleAdmin = new protobuf_1.SignalService.GroupChange.Actions.ModifyMemberRoleAction();
    toggleAdmin.userId = uuidCipherTextBuffer;
    toggleAdmin.role = role;
    actions.version = (group.revision || 0) + 1;
    actions.modifyMemberRoles = [toggleAdmin];
    return actions;
}
exports.buildModifyMemberRoleChange = buildModifyMemberRoleChange;
function buildPromotePendingAdminApprovalMemberChange({ group, uuid, }) {
    const MEMBER_ROLE_ENUM = protobuf_1.SignalService.Member.Role;
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    if (!group.secretParams) {
        throw new Error('buildAddPendingAdminApprovalMemberChange: group was missing secretParams!');
    }
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(group.secretParams);
    const uuidCipherTextBuffer = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, uuid);
    const promotePendingMember = new protobuf_1.SignalService.GroupChange.Actions.PromoteMemberPendingAdminApprovalAction();
    promotePendingMember.userId = uuidCipherTextBuffer;
    promotePendingMember.role = MEMBER_ROLE_ENUM.DEFAULT;
    actions.version = (group.revision || 0) + 1;
    actions.promoteMemberPendingAdminApprovals = [promotePendingMember];
    return actions;
}
exports.buildPromotePendingAdminApprovalMemberChange = buildPromotePendingAdminApprovalMemberChange;
function buildPromoteMemberChange({ group, profileKeyCredentialBase64, serverPublicParamsBase64, }) {
    const actions = new protobuf_1.SignalService.GroupChange.Actions();
    if (!group.secretParams) {
        throw new Error('buildDisappearingMessagesTimerChange: group was missing secretParams!');
    }
    const clientZkProfileCipher = (0, zkgroup_1.getClientZkProfileOperations)(serverPublicParamsBase64);
    const presentation = (0, zkgroup_1.createProfileKeyCredentialPresentation)(clientZkProfileCipher, profileKeyCredentialBase64, group.secretParams);
    const promotePendingMember = new protobuf_1.SignalService.GroupChange.Actions.PromoteMemberPendingProfileKeyAction();
    promotePendingMember.presentation = presentation;
    actions.version = (group.revision || 0) + 1;
    actions.promotePendingMembers = [promotePendingMember];
    return actions;
}
exports.buildPromoteMemberChange = buildPromoteMemberChange;
async function uploadGroupChange({ actions, group, inviteLinkPassword, }) {
    const logId = idForLogging(group.groupId);
    // Ensure we have the credentials we need before attempting GroupsV2 operations
    await (0, groupCredentialFetcher_1.maybeFetchNewCredentials)();
    if (!group.secretParams) {
        throw new Error('uploadGroupChange: group was missing secretParams!');
    }
    if (!group.publicParams) {
        throw new Error('uploadGroupChange: group was missing publicParams!');
    }
    return makeRequestWithTemporalRetry({
        logId: `uploadGroupChange/${logId}`,
        publicParams: group.publicParams,
        secretParams: group.secretParams,
        request: (sender, options) => sender.modifyGroup(actions, options, inviteLinkPassword),
    });
}
exports.uploadGroupChange = uploadGroupChange;
async function modifyGroupV2({ conversation, createGroupChange, extraConversationsForSend, inviteLinkPassword, name, }) {
    const idLog = `${name}/${conversation.idForLogging()}`;
    if (!(0, whatTypeOfConversation_1.isGroupV2)(conversation.attributes)) {
        throw new Error(`modifyGroupV2/${idLog}: Called for non-GroupV2 conversation`);
    }
    const startTime = Date.now();
    const timeoutTime = startTime + durations.MINUTE;
    const MAX_ATTEMPTS = 5;
    for (let attempt = 0; attempt < MAX_ATTEMPTS; attempt += 1) {
        log.info(`modifyGroupV2/${idLog}: Starting attempt ${attempt}`);
        try {
            // eslint-disable-next-line no-await-in-loop
            await window.waitForEmptyEventQueue();
            log.info(`modifyGroupV2/${idLog}: Queuing attempt ${attempt}`);
            // eslint-disable-next-line no-await-in-loop
            await conversation.queueJob('modifyGroupV2', async () => {
                log.info(`modifyGroupV2/${idLog}: Running attempt ${attempt}`);
                const actions = await createGroupChange();
                if (!actions) {
                    log.warn(`modifyGroupV2/${idLog}: No change actions. Returning early.`);
                    return;
                }
                // The new revision has to be exactly one more than the current revision
                //   or it won't upload properly, and it won't apply in maybeUpdateGroup
                const currentRevision = conversation.get('revision');
                const newRevision = actions.version;
                if ((currentRevision || 0) + 1 !== newRevision) {
                    throw new Error(`modifyGroupV2/${idLog}: Revision mismatch - ${currentRevision} to ${newRevision}.`);
                }
                // Upload. If we don't have permission, the server will return an error here.
                const groupChange = await window.Signal.Groups.uploadGroupChange({
                    actions,
                    inviteLinkPassword,
                    group: conversation.attributes,
                });
                const groupChangeBuffer = protobuf_1.SignalService.GroupChange.encode(groupChange).finish();
                const groupChangeBase64 = Bytes.toBase64(groupChangeBuffer);
                // Apply change locally, just like we would with an incoming change. This will
                //   change conversation state and add change notifications to the timeline.
                await window.Signal.Groups.maybeUpdateGroup({
                    conversation,
                    groupChangeBase64,
                    newRevision,
                });
                // Send message to notify group members (including pending members) of change
                const profileKey = conversation.get('profileSharing')
                    ? await ourProfileKey_1.ourProfileKeyService.get()
                    : undefined;
                const sendOptions = await (0, getSendOptions_1.getSendOptions)(conversation.attributes);
                const timestamp = Date.now();
                const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
                const promise = (0, handleMessageSend_1.handleMessageSend)(window.Signal.Util.sendToGroup({
                    groupSendOptions: {
                        groupV2: conversation.getGroupV2Info({
                            groupChange: groupChangeBuffer,
                            includePendingMembers: true,
                            extraConversationsForSend,
                        }),
                        timestamp,
                        profileKey,
                    },
                    conversation,
                    contentHint: ContentHint.RESENDABLE,
                    messageId: undefined,
                    sendOptions,
                    sendType: 'groupChange',
                }), { messageIds: [], sendType: 'groupChange' });
                // We don't save this message; we just use it to ensure that a sync message is
                //   sent to our linked devices.
                const m = new window.Whisper.Message({
                    conversationId: conversation.id,
                    type: 'not-to-save',
                    sent_at: timestamp,
                    received_at: timestamp,
                    // TODO: DESKTOP-722
                    // this type does not fully implement the interface it is expected to
                });
                // This is to ensure that the functions in send() and sendSyncMessage()
                //   don't save anything to the database.
                m.doNotSave = true;
                await m.send(promise);
            });
            // If we've gotten here with no error, we exit!
            log.info(`modifyGroupV2/${idLog}: Update complete, with attempt ${attempt}!`);
            break;
        }
        catch (error) {
            if (error.code === 409 && Date.now() <= timeoutTime) {
                log.info(`modifyGroupV2/${idLog}: Conflict while updating. Trying again...`);
                // eslint-disable-next-line no-await-in-loop
                await conversation.fetchLatestGroupV2Data({ force: true });
            }
            else if (error.code === 409) {
                log.error(`modifyGroupV2/${idLog}: Conflict while updating. Timed out; not retrying.`);
                // We don't wait here because we're breaking out of the loop immediately.
                conversation.fetchLatestGroupV2Data({ force: true });
                throw error;
            }
            else {
                const errorString = error && error.stack ? error.stack : error;
                log.error(`modifyGroupV2/${idLog}: Error updating: ${errorString}`);
                throw error;
            }
        }
    }
}
exports.modifyGroupV2 = modifyGroupV2;
// Utility
function idForLogging(groupId) {
    return `groupv2(${groupId})`;
}
exports.idForLogging = idForLogging;
function deriveGroupFields(masterKey) {
    if (masterKey.length !== exports.MASTER_KEY_LENGTH) {
        throw new Error(`deriveGroupFields: masterKey had length ${masterKey.length}, ` +
            `expected ${exports.MASTER_KEY_LENGTH}`);
    }
    const cacheKey = Bytes.toBase64(masterKey);
    const cached = groupFieldsCache.get(cacheKey);
    if (cached) {
        return cached;
    }
    log.info('deriveGroupFields: cache miss');
    const secretParams = (0, zkgroup_1.deriveGroupSecretParams)(masterKey);
    const publicParams = (0, zkgroup_1.deriveGroupPublicParams)(secretParams);
    const id = (0, zkgroup_1.deriveGroupID)(secretParams);
    const fresh = {
        id,
        secretParams,
        publicParams,
    };
    groupFieldsCache.set(cacheKey, fresh);
    return fresh;
}
exports.deriveGroupFields = deriveGroupFields;
async function makeRequestWithTemporalRetry({ logId, publicParams, secretParams, request, }) {
    const data = window.storage.get(groupCredentialFetcher_1.GROUP_CREDENTIALS_KEY);
    if (!data) {
        throw new Error(`makeRequestWithTemporalRetry/${logId}: No group credentials!`);
    }
    const groupCredentials = (0, groupCredentialFetcher_1.getCredentialsForToday)(data);
    const sender = window.textsecure.messaging;
    if (!sender) {
        throw new Error(`makeRequestWithTemporalRetry/${logId}: textsecure.messaging is not available!`);
    }
    const todayOptions = getGroupCredentials({
        authCredentialBase64: groupCredentials.today.credential,
        groupPublicParamsBase64: publicParams,
        groupSecretParamsBase64: secretParams,
        serverPublicParamsBase64: window.getServerPublicParams(),
    });
    try {
        return await request(sender, todayOptions);
    }
    catch (todayError) {
        if (todayError.code === TEMPORAL_AUTH_REJECTED_CODE) {
            log.warn(`makeRequestWithTemporalRetry/${logId}: Trying again with tomorrow's credentials`);
            const tomorrowOptions = getGroupCredentials({
                authCredentialBase64: groupCredentials.tomorrow.credential,
                groupPublicParamsBase64: publicParams,
                groupSecretParamsBase64: secretParams,
                serverPublicParamsBase64: window.getServerPublicParams(),
            });
            return request(sender, tomorrowOptions);
        }
        throw todayError;
    }
}
async function fetchMembershipProof({ publicParams, secretParams, }) {
    // Ensure we have the credentials we need before attempting GroupsV2 operations
    await (0, groupCredentialFetcher_1.maybeFetchNewCredentials)();
    if (!publicParams) {
        throw new Error('fetchMembershipProof: group was missing publicParams!');
    }
    if (!secretParams) {
        throw new Error('fetchMembershipProof: group was missing secretParams!');
    }
    const response = await makeRequestWithTemporalRetry({
        logId: 'fetchMembershipProof',
        publicParams,
        secretParams,
        request: (sender, options) => sender.getGroupMembershipToken(options),
    });
    return response.token;
}
exports.fetchMembershipProof = fetchMembershipProof;
// Creating a group
async function createGroupV2({ name, avatar, expireTimer, conversationIds, avatars, }) {
    // Ensure we have the credentials we need before attempting GroupsV2 operations
    await (0, groupCredentialFetcher_1.maybeFetchNewCredentials)();
    const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
    const MEMBER_ROLE_ENUM = protobuf_1.SignalService.Member.Role;
    const masterKeyBuffer = (0, Crypto_1.getRandomBytes)(32);
    const fields = deriveGroupFields(masterKeyBuffer);
    const groupId = Bytes.toBase64(fields.id);
    const logId = `groupv2(${groupId})`;
    const masterKey = Bytes.toBase64(masterKeyBuffer);
    const secretParams = Bytes.toBase64(fields.secretParams);
    const publicParams = Bytes.toBase64(fields.publicParams);
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    const membersV2 = [
        {
            uuid: ourUuid,
            role: MEMBER_ROLE_ENUM.ADMINISTRATOR,
            joinedAtVersion: 0,
        },
    ];
    const pendingMembersV2 = [];
    let uploadedAvatar;
    await Promise.all([
        ...conversationIds.map(async (conversationId) => {
            var _a, _b;
            const contact = window.ConversationController.get(conversationId);
            if (!contact) {
                (0, assert_1.assert)(false, `createGroupV2/${logId}: missing local contact, skipping`);
                return;
            }
            const contactUuid = contact.get('uuid');
            if (!contactUuid) {
                (0, assert_1.assert)(false, `createGroupV2/${logId}: missing UUID; skipping`);
                return;
            }
            // Refresh our local data to be sure
            if (!((_a = contact.get('capabilities')) === null || _a === void 0 ? void 0 : _a.gv2) ||
                !contact.get('profileKey') ||
                !contact.get('profileKeyCredential')) {
                await contact.getProfiles();
            }
            if (!((_b = contact.get('capabilities')) === null || _b === void 0 ? void 0 : _b.gv2)) {
                (0, assert_1.assert)(false, `createGroupV2/${logId}: member is missing GV2 capability; skipping`);
                return;
            }
            if (contact.get('profileKey') && contact.get('profileKeyCredential')) {
                membersV2.push({
                    uuid: contactUuid,
                    role: MEMBER_ROLE_ENUM.DEFAULT,
                    joinedAtVersion: 0,
                });
            }
            else {
                pendingMembersV2.push({
                    addedByUserId: ourUuid,
                    uuid: contactUuid,
                    timestamp: Date.now(),
                    role: MEMBER_ROLE_ENUM.DEFAULT,
                });
            }
        }),
        (async () => {
            if (!avatar) {
                return;
            }
            uploadedAvatar = await uploadAvatar({
                data: avatar,
                logId,
                publicParams,
                secretParams,
            });
        })(),
    ]);
    if (membersV2.length + pendingMembersV2.length > (0, limits_1.getGroupSizeHardLimit)()) {
        throw new Error(`createGroupV2/${logId}: Too many members! Member count: ${membersV2.length}, Pending member count: ${pendingMembersV2.length}`);
    }
    const protoAndConversationAttributes = {
        name,
        // Core GroupV2 info
        revision: 0,
        publicParams,
        secretParams,
        // GroupV2 state
        accessControl: {
            attributes: ACCESS_ENUM.MEMBER,
            members: ACCESS_ENUM.MEMBER,
            addFromInviteLink: ACCESS_ENUM.UNSATISFIABLE,
        },
        membersV2,
        pendingMembersV2,
    };
    const groupProto = await buildGroupProto(Object.assign({ id: groupId, avatarUrl: uploadedAvatar === null || uploadedAvatar === void 0 ? void 0 : uploadedAvatar.key }, protoAndConversationAttributes));
    await makeRequestWithTemporalRetry({
        logId: `createGroupV2/${logId}`,
        publicParams,
        secretParams,
        request: (sender, options) => sender.createGroup(groupProto, options),
    });
    let avatarAttribute;
    if (uploadedAvatar) {
        try {
            avatarAttribute = {
                url: uploadedAvatar.key,
                path: await window.Signal.Migrations.writeNewAttachmentData(uploadedAvatar.data),
                hash: uploadedAvatar.hash,
            };
        }
        catch (err) {
            log.warn(`createGroupV2/${logId}: avatar failed to save to disk. Continuing on`);
        }
    }
    const now = Date.now();
    const conversation = await window.ConversationController.getOrCreateAndWait(groupId, 'group', Object.assign(Object.assign({}, protoAndConversationAttributes), { active_at: now, addedBy: ourUuid, avatar: avatarAttribute, avatars, groupVersion: 2, masterKey, profileSharing: true, timestamp: now, needsStorageServiceSync: true }));
    await conversation.queueJob('storageServiceUploadJob', async () => {
        await window.Signal.Services.storageServiceUploadJob();
    });
    const timestamp = Date.now();
    const profileKey = await ourProfileKey_1.ourProfileKeyService.get();
    const groupV2Info = conversation.getGroupV2Info({
        includePendingMembers: true,
    });
    const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
    const sendOptions = await (0, getSendOptions_1.getSendOptions)(conversation.attributes);
    await wrapWithSyncMessageSend({
        conversation,
        logId: `sendToGroup/${logId}`,
        messageIds: [],
        send: async () => window.Signal.Util.sendToGroup({
            groupSendOptions: {
                groupV2: groupV2Info,
                timestamp,
                profileKey,
            },
            conversation,
            contentHint: ContentHint.RESENDABLE,
            messageId: undefined,
            sendOptions,
            sendType: 'groupChange',
        }),
        sendType: 'groupChange',
        timestamp,
    });
    const createdTheGroupMessage = Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v2-change', sourceUuid: ourUuid, conversationId: conversation.id, received_at: window.Signal.Util.incrementMessageCounter(), received_at_ms: timestamp, sent_at: timestamp, groupV2Change: {
            from: ourUuid,
            details: [{ type: 'create' }],
        } });
    await window.Signal.Data.saveMessages([createdTheGroupMessage], {
        forceSave: true,
    });
    const model = new window.Whisper.Message(createdTheGroupMessage);
    window.MessageController.register(model.id, model);
    conversation.trigger('newmessage', model);
    if (expireTimer) {
        await conversation.updateExpirationTimer(expireTimer);
    }
    return conversation;
}
exports.createGroupV2 = createGroupV2;
// Migrating a group
async function hasV1GroupBeenMigrated(conversation) {
    const logId = conversation.idForLogging();
    const isGroupV1 = (0, whatTypeOfConversation_1.isGroupV1)(conversation.attributes);
    if (!isGroupV1) {
        log.warn(`checkForGV2Existence/${logId}: Called for non-GroupV1 conversation!`);
        return false;
    }
    // Ensure we have the credentials we need before attempting GroupsV2 operations
    await (0, groupCredentialFetcher_1.maybeFetchNewCredentials)();
    const groupId = conversation.get('groupId');
    if (!groupId) {
        throw new Error(`checkForGV2Existence/${logId}: No groupId!`);
    }
    const idBuffer = Bytes.fromBinary(groupId);
    const masterKeyBuffer = (0, Crypto_1.deriveMasterKeyFromGroupV1)(idBuffer);
    const fields = deriveGroupFields(masterKeyBuffer);
    try {
        await makeRequestWithTemporalRetry({
            logId: `getGroup/${logId}`,
            publicParams: Bytes.toBase64(fields.publicParams),
            secretParams: Bytes.toBase64(fields.secretParams),
            request: (sender, options) => sender.getGroup(options),
        });
        return true;
    }
    catch (error) {
        const { code } = error;
        return code !== GROUP_NONEXISTENT_CODE;
    }
}
exports.hasV1GroupBeenMigrated = hasV1GroupBeenMigrated;
function maybeDeriveGroupV2Id(conversation) {
    const isGroupV1 = (0, whatTypeOfConversation_1.isGroupV1)(conversation.attributes);
    const groupV1Id = conversation.get('groupId');
    const derived = conversation.get('derivedGroupV2Id');
    if (!isGroupV1 || !groupV1Id || derived) {
        return false;
    }
    const v1IdBuffer = Bytes.fromBinary(groupV1Id);
    const masterKeyBuffer = (0, Crypto_1.deriveMasterKeyFromGroupV1)(v1IdBuffer);
    const fields = deriveGroupFields(masterKeyBuffer);
    const derivedGroupV2Id = Bytes.toBase64(fields.id);
    conversation.set({
        derivedGroupV2Id,
    });
    return true;
}
exports.maybeDeriveGroupV2Id = maybeDeriveGroupV2Id;
async function isGroupEligibleToMigrate(conversation) {
    if (!(0, whatTypeOfConversation_1.isGroupV1)(conversation.attributes)) {
        return false;
    }
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    const areWeMember = !conversation.get('left') && conversation.hasMember(ourUuid);
    if (!areWeMember) {
        return false;
    }
    const members = conversation.get('members') || [];
    for (let i = 0, max = members.length; i < max; i += 1) {
        const identifier = members[i];
        const contact = window.ConversationController.get(identifier);
        if (!contact) {
            return false;
        }
        if (!contact.get('uuid')) {
            return false;
        }
    }
    return true;
}
exports.isGroupEligibleToMigrate = isGroupEligibleToMigrate;
async function getGroupMigrationMembers(conversation) {
    const logId = conversation.idForLogging();
    const MEMBER_ROLE_ENUM = protobuf_1.SignalService.Member.Role;
    const ourConversationId = window.ConversationController.getOurConversationId();
    if (!ourConversationId) {
        throw new Error(`getGroupMigrationMembers/${logId}: Couldn't fetch our own conversationId!`);
    }
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    let areWeMember = false;
    let areWeInvited = false;
    const previousGroupV1Members = conversation.get('members') || [];
    const now = Date.now();
    const memberLookup = {};
    const membersV2 = (0, lodash_1.compact)(await Promise.all(previousGroupV1Members.map(async (e164) => {
        const contact = window.ConversationController.get(e164);
        if (!contact) {
            throw new Error(`getGroupMigrationMembers/${logId}: membersV2 - missing local contact for ${e164}, skipping.`);
        }
        if (!(0, whatTypeOfConversation_1.isMe)(contact.attributes) && window.GV2_MIGRATION_DISABLE_ADD) {
            log.warn(`getGroupMigrationMembers/${logId}: membersV2 - skipping ${e164} due to GV2_MIGRATION_DISABLE_ADD flag`);
            return null;
        }
        const contactUuid = contact.get('uuid');
        if (!contactUuid) {
            log.warn(`getGroupMigrationMembers/${logId}: membersV2 - missing uuid for ${e164}, skipping.`);
            return null;
        }
        if (!contact.get('profileKey')) {
            log.warn(`getGroupMigrationMembers/${logId}: membersV2 - missing profileKey for member ${e164}, skipping.`);
            return null;
        }
        let capabilities = contact.get('capabilities');
        // Refresh our local data to be sure
        if (!capabilities ||
            !capabilities.gv2 ||
            !capabilities['gv1-migration'] ||
            !contact.get('profileKeyCredential')) {
            await contact.getProfiles();
        }
        capabilities = contact.get('capabilities');
        if (!capabilities || !capabilities.gv2) {
            log.warn(`getGroupMigrationMembers/${logId}: membersV2 - member ${e164} is missing gv2 capability, skipping.`);
            return null;
        }
        if (!capabilities || !capabilities['gv1-migration']) {
            log.warn(`getGroupMigrationMembers/${logId}: membersV2 - member ${e164} is missing gv1-migration capability, skipping.`);
            return null;
        }
        if (!contact.get('profileKeyCredential')) {
            log.warn(`getGroupMigrationMembers/${logId}: membersV2 - no profileKeyCredential for ${e164}, skipping.`);
            return null;
        }
        const conversationId = contact.id;
        if (conversationId === ourConversationId) {
            areWeMember = true;
        }
        memberLookup[conversationId] = true;
        return {
            uuid: contactUuid,
            role: MEMBER_ROLE_ENUM.ADMINISTRATOR,
            joinedAtVersion: 0,
        };
    })));
    const droppedGV2MemberIds = [];
    const pendingMembersV2 = (0, lodash_1.compact)((previousGroupV1Members || []).map(e164 => {
        const contact = window.ConversationController.get(e164);
        if (!contact) {
            throw new Error(`getGroupMigrationMembers/${logId}: pendingMembersV2 - missing local contact for ${e164}, skipping.`);
        }
        const conversationId = contact.id;
        // If we've already added this contact above, we'll skip here
        if (memberLookup[conversationId]) {
            return null;
        }
        if (!(0, whatTypeOfConversation_1.isMe)(contact.attributes) && window.GV2_MIGRATION_DISABLE_INVITE) {
            log.warn(`getGroupMigrationMembers/${logId}: pendingMembersV2 - skipping ${e164} due to GV2_MIGRATION_DISABLE_INVITE flag`);
            droppedGV2MemberIds.push(conversationId);
            return null;
        }
        const contactUuid = contact.get('uuid');
        if (!contactUuid) {
            log.warn(`getGroupMigrationMembers/${logId}: pendingMembersV2 - missing uuid for ${e164}, skipping.`);
            droppedGV2MemberIds.push(conversationId);
            return null;
        }
        const capabilities = contact.get('capabilities');
        if (!capabilities || !capabilities.gv2) {
            log.warn(`getGroupMigrationMembers/${logId}: pendingMembersV2 - member ${e164} is missing gv2 capability, skipping.`);
            droppedGV2MemberIds.push(conversationId);
            return null;
        }
        if (!capabilities || !capabilities['gv1-migration']) {
            log.warn(`getGroupMigrationMembers/${logId}: pendingMembersV2 - member ${e164} is missing gv1-migration capability, skipping.`);
            droppedGV2MemberIds.push(conversationId);
            return null;
        }
        if (conversationId === ourConversationId) {
            areWeInvited = true;
        }
        return {
            uuid: contactUuid,
            timestamp: now,
            addedByUserId: ourUuid,
            role: MEMBER_ROLE_ENUM.ADMINISTRATOR,
        };
    }));
    if (!areWeMember) {
        throw new Error(`getGroupMigrationMembers/${logId}: We are not a member!`);
    }
    if (areWeInvited) {
        throw new Error(`getGroupMigrationMembers/${logId}: We are invited!`);
    }
    return {
        droppedGV2MemberIds,
        membersV2,
        pendingMembersV2,
        previousGroupV1Members,
    };
}
exports.getGroupMigrationMembers = getGroupMigrationMembers;
// This is called when the user chooses to migrate a GroupV1. It will update the server,
//   then let all members know about the new group.
async function initiateMigrationToGroupV2(conversation) {
    // Ensure we have the credentials we need before attempting GroupsV2 operations
    await (0, groupCredentialFetcher_1.maybeFetchNewCredentials)();
    try {
        await conversation.queueJob('initiateMigrationToGroupV2', async () => {
            var _a;
            const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
            const isEligible = isGroupEligibleToMigrate(conversation);
            const previousGroupV1Id = conversation.get('groupId');
            if (!isEligible || !previousGroupV1Id) {
                throw new Error(`initiateMigrationToGroupV2: conversation is not eligible to migrate! ${conversation.idForLogging()}`);
            }
            const groupV1IdBuffer = Bytes.fromBinary(previousGroupV1Id);
            const masterKeyBuffer = (0, Crypto_1.deriveMasterKeyFromGroupV1)(groupV1IdBuffer);
            const fields = deriveGroupFields(masterKeyBuffer);
            const groupId = Bytes.toBase64(fields.id);
            const logId = `groupv2(${groupId})`;
            log.info(`initiateMigrationToGroupV2/${logId}: Migrating from ${conversation.idForLogging()}`);
            const masterKey = Bytes.toBase64(masterKeyBuffer);
            const secretParams = Bytes.toBase64(fields.secretParams);
            const publicParams = Bytes.toBase64(fields.publicParams);
            const ourConversationId = window.ConversationController.getOurConversationId();
            if (!ourConversationId) {
                throw new Error(`initiateMigrationToGroupV2/${logId}: Couldn't fetch our own conversationId!`);
            }
            const ourConversation = window.ConversationController.get(ourConversationId);
            if (!ourConversation) {
                throw new Error(`initiateMigrationToGroupV2/${logId}: cannot get our own conversation. Cannot migrate`);
            }
            const { membersV2, pendingMembersV2, droppedGV2MemberIds, previousGroupV1Members, } = await getGroupMigrationMembers(conversation);
            if (membersV2.length + pendingMembersV2.length >
                (0, limits_1.getGroupSizeHardLimit)()) {
                throw new Error(`initiateMigrationToGroupV2/${logId}: Too many members! Member count: ${membersV2.length}, Pending member count: ${pendingMembersV2.length}`);
            }
            // Note: A few group elements don't need to change here:
            //   - name
            //   - expireTimer
            let avatarAttribute;
            const avatarPath = (_a = conversation.attributes.avatar) === null || _a === void 0 ? void 0 : _a.path;
            if (avatarPath) {
                const { hash, key } = await uploadAvatar({
                    logId,
                    publicParams,
                    secretParams,
                    path: avatarPath,
                });
                avatarAttribute = {
                    url: key,
                    path: avatarPath,
                    hash,
                };
            }
            const newAttributes = Object.assign(Object.assign({}, conversation.attributes), { avatar: avatarAttribute, 
                // Core GroupV2 info
                revision: 0, groupId, groupVersion: 2, masterKey,
                publicParams,
                secretParams, 
                // GroupV2 state
                accessControl: {
                    attributes: ACCESS_ENUM.MEMBER,
                    members: ACCESS_ENUM.MEMBER,
                    addFromInviteLink: ACCESS_ENUM.UNSATISFIABLE,
                }, membersV2,
                pendingMembersV2,
                // Capture previous GroupV1 data for future use
                previousGroupV1Id,
                previousGroupV1Members, 
                // Clear storage ID, since we need to start over on the storage service
                storageID: undefined, 
                // Clear obsolete data
                derivedGroupV2Id: undefined, members: undefined });
            const groupProto = buildGroupProto(Object.assign(Object.assign({}, newAttributes), { avatarUrl: avatarAttribute === null || avatarAttribute === void 0 ? void 0 : avatarAttribute.url }));
            try {
                await makeRequestWithTemporalRetry({
                    logId: `createGroup/${logId}`,
                    publicParams,
                    secretParams,
                    request: (sender, options) => sender.createGroup(groupProto, options),
                });
            }
            catch (error) {
                log.error(`initiateMigrationToGroupV2/${logId}: Error creating group:`, error.stack);
                throw error;
            }
            const groupChangeMessages = [];
            groupChangeMessages.push(Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v1-migration', invitedGV2Members: pendingMembersV2, droppedGV2MemberIds }));
            await updateGroup({
                conversation,
                updates: {
                    newAttributes,
                    groupChangeMessages,
                    members: [],
                },
            });
            if (window.storage.blocked.isGroupBlocked(previousGroupV1Id)) {
                window.storage.blocked.addBlockedGroup(groupId);
            }
            // Save these most recent updates to conversation
            updateConversation(conversation.attributes);
        });
    }
    catch (error) {
        const logId = conversation.idForLogging();
        if (!(0, whatTypeOfConversation_1.isGroupV1)(conversation.attributes)) {
            throw error;
        }
        const alreadyMigrated = await hasV1GroupBeenMigrated(conversation);
        if (!alreadyMigrated) {
            log.error(`initiateMigrationToGroupV2/${logId}: Group has not already been migrated, re-throwing error`);
            throw error;
        }
        await respondToGroupV2Migration({
            conversation,
        });
        return;
    }
    // We've migrated the group, now we need to let all other group members know about it
    const logId = conversation.idForLogging();
    const timestamp = Date.now();
    const ourProfileKey = await ourProfileKey_1.ourProfileKeyService.get();
    const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
    const sendOptions = await (0, getSendOptions_1.getSendOptions)(conversation.attributes);
    await wrapWithSyncMessageSend({
        conversation,
        logId: `sendToGroup/${logId}`,
        messageIds: [],
        send: async () => 
        // Minimal message to notify group members about migration
        window.Signal.Util.sendToGroup({
            groupSendOptions: {
                groupV2: conversation.getGroupV2Info({
                    includePendingMembers: true,
                }),
                timestamp,
                profileKey: ourProfileKey,
            },
            conversation,
            contentHint: ContentHint.RESENDABLE,
            messageId: undefined,
            sendOptions,
            sendType: 'groupChange',
        }),
        sendType: 'groupChange',
        timestamp,
    });
}
exports.initiateMigrationToGroupV2 = initiateMigrationToGroupV2;
async function wrapWithSyncMessageSend({ conversation, logId, messageIds, send, sendType, timestamp, }) {
    const sender = window.textsecure.messaging;
    if (!sender) {
        throw new Error(`initiateMigrationToGroupV2/${logId}: textsecure.messaging is not available!`);
    }
    let response;
    try {
        response = await (0, handleMessageSend_1.handleMessageSend)(send(sender), { messageIds, sendType });
    }
    catch (error) {
        if (conversation.processSendResponse(error)) {
            response = error;
        }
    }
    if (!response) {
        throw new Error(`wrapWithSyncMessageSend/${logId}: message send didn't return result!!`);
    }
    // Minimal implementation of sending same message to linked devices
    const { dataMessage } = response;
    if (!dataMessage) {
        throw new Error(`wrapWithSyncMessageSend/${logId}: dataMessage was not returned by send!`);
    }
    const ourConversationId = window.ConversationController.getOurConversationId();
    if (!ourConversationId) {
        throw new Error(`wrapWithSyncMessageSend/${logId}: Cannot get our conversationId!`);
    }
    const ourConversation = window.ConversationController.get(ourConversationId);
    if (!ourConversation) {
        throw new Error(`wrapWithSyncMessageSend/${logId}: Cannot get our conversation!`);
    }
    if (window.ConversationController.areWePrimaryDevice()) {
        log.warn(`wrapWithSyncMessageSend/${logId}: We are primary device; not sync message`);
        return;
    }
    const options = await (0, getSendOptions_1.getSendOptions)(ourConversation.attributes);
    await (0, handleMessageSend_1.handleMessageSend)(sender.sendSyncMessage({
        destination: ourConversation.get('e164'),
        destinationUuid: ourConversation.get('uuid'),
        encodedDataMessage: dataMessage,
        expirationStartTimestamp: null,
        options,
        timestamp,
    }), { messageIds, sendType });
}
exports.wrapWithSyncMessageSend = wrapWithSyncMessageSend;
async function waitThenRespondToGroupV2Migration(options) {
    // First wait to process all incoming messages on the websocket
    await window.waitForEmptyEventQueue();
    // Then wait to process all outstanding messages for this conversation
    const { conversation } = options;
    await conversation.queueJob('waitThenRespondToGroupV2Migration', async () => {
        try {
            // And finally try to migrate the group
            await respondToGroupV2Migration(options);
        }
        catch (error) {
            log.error(`waitThenRespondToGroupV2Migration/${conversation.idForLogging()}: respondToGroupV2Migration failure:`, error && error.stack ? error.stack : error);
        }
    });
}
exports.waitThenRespondToGroupV2Migration = waitThenRespondToGroupV2Migration;
function buildMigrationBubble(previousGroupV1MembersIds, newAttributes) {
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    const ourConversationId = window.ConversationController.getOurConversationId();
    // Assemble items to commemorate this event for the timeline..
    const combinedConversationIds = [
        ...(newAttributes.membersV2 || []).map(item => item.uuid),
        ...(newAttributes.pendingMembersV2 || []).map(item => item.uuid),
    ].map(uuid => {
        const conversationId = window.ConversationController.ensureContactIds({
            uuid,
        });
        (0, assert_1.strictAssert)(conversationId, `Conversation not found for ${uuid}`);
        return conversationId;
    });
    const droppedMemberIds = (0, lodash_1.difference)(previousGroupV1MembersIds, combinedConversationIds).filter(id => id && id !== ourConversationId);
    const invitedMembers = (newAttributes.pendingMembersV2 || []).filter(item => item.uuid !== ourUuid);
    const areWeInvited = (newAttributes.pendingMembersV2 || []).some(item => item.uuid === ourUuid);
    return Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v1-migration', groupMigration: {
            areWeInvited,
            invitedMembers,
            droppedMemberIds,
        } });
}
exports.buildMigrationBubble = buildMigrationBubble;
async function joinGroupV2ViaLinkAndMigrate({ approvalRequired, conversation, inviteLinkPassword, revision, }) {
    const isGroupV1 = (0, whatTypeOfConversation_1.isGroupV1)(conversation.attributes);
    const previousGroupV1Id = conversation.get('groupId');
    if (!isGroupV1 || !previousGroupV1Id) {
        throw new Error(`joinGroupV2ViaLinkAndMigrate: Conversation is not GroupV1! ${conversation.idForLogging()}`);
    }
    // Derive GroupV2 fields
    const groupV1IdBuffer = Bytes.fromBinary(previousGroupV1Id);
    const masterKeyBuffer = (0, Crypto_1.deriveMasterKeyFromGroupV1)(groupV1IdBuffer);
    const fields = deriveGroupFields(masterKeyBuffer);
    const groupId = Bytes.toBase64(fields.id);
    const logId = idForLogging(groupId);
    log.info(`joinGroupV2ViaLinkAndMigrate/${logId}: Migrating from ${conversation.idForLogging()}`);
    const masterKey = Bytes.toBase64(masterKeyBuffer);
    const secretParams = Bytes.toBase64(fields.secretParams);
    const publicParams = Bytes.toBase64(fields.publicParams);
    // A mini-migration, which will not show dropped/invited members
    const newAttributes = Object.assign(Object.assign({}, conversation.attributes), { 
        // Core GroupV2 info
        revision,
        groupId, groupVersion: 2, masterKey,
        publicParams,
        secretParams, groupInviteLinkPassword: inviteLinkPassword, left: true, 
        // Capture previous GroupV1 data for future use
        previousGroupV1Id: conversation.get('groupId'), previousGroupV1Members: conversation.get('members'), 
        // Clear storage ID, since we need to start over on the storage service
        storageID: undefined, 
        // Clear obsolete data
        derivedGroupV2Id: undefined, members: undefined });
    const groupChangeMessages = [
        Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v1-migration', groupMigration: {
                areWeInvited: false,
                invitedMembers: [],
                droppedMemberIds: [],
            } }),
    ];
    await updateGroup({
        conversation,
        updates: {
            newAttributes,
            groupChangeMessages,
            members: [],
        },
    });
    // Now things are set up, so we can go through normal channels
    await conversation.joinGroupV2ViaLink({
        inviteLinkPassword,
        approvalRequired,
    });
}
exports.joinGroupV2ViaLinkAndMigrate = joinGroupV2ViaLinkAndMigrate;
// This may be called from storage service, an out-of-band check, or an incoming message.
//   If this is kicked off via an incoming message, we want to do the right thing and hit
//   the log endpoint - the parameters beyond conversation are needed in that scenario.
async function respondToGroupV2Migration({ conversation, groupChangeBase64, newRevision, receivedAt, sentAt, }) {
    var _a, _b, _c;
    // Ensure we have the credentials we need before attempting GroupsV2 operations
    await (0, groupCredentialFetcher_1.maybeFetchNewCredentials)();
    const isGroupV1 = (0, whatTypeOfConversation_1.isGroupV1)(conversation.attributes);
    const previousGroupV1Id = conversation.get('groupId');
    if (!isGroupV1 || !previousGroupV1Id) {
        throw new Error(`respondToGroupV2Migration: Conversation is not GroupV1! ${conversation.idForLogging()}`);
    }
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    const wereWePreviouslyAMember = !conversation.get('left') && conversation.hasMember(ourUuid);
    // Derive GroupV2 fields
    const groupV1IdBuffer = Bytes.fromBinary(previousGroupV1Id);
    const masterKeyBuffer = (0, Crypto_1.deriveMasterKeyFromGroupV1)(groupV1IdBuffer);
    const fields = deriveGroupFields(masterKeyBuffer);
    const groupId = Bytes.toBase64(fields.id);
    const logId = idForLogging(groupId);
    log.info(`respondToGroupV2Migration/${logId}: Migrating from ${conversation.idForLogging()}`);
    const masterKey = Bytes.toBase64(masterKeyBuffer);
    const secretParams = Bytes.toBase64(fields.secretParams);
    const publicParams = Bytes.toBase64(fields.publicParams);
    const previousGroupV1Members = conversation.get('members');
    const previousGroupV1MembersIds = conversation.getMemberIds();
    // Skeleton of the new group state - not useful until we add the group's server state
    const attributes = Object.assign(Object.assign({}, conversation.attributes), { 
        // Core GroupV2 info
        revision: 0, groupId, groupVersion: 2, masterKey,
        publicParams,
        secretParams,
        // Capture previous GroupV1 data for future use
        previousGroupV1Id,
        previousGroupV1Members, 
        // Clear storage ID, since we need to start over on the storage service
        storageID: undefined, 
        // Clear obsolete data
        derivedGroupV2Id: undefined, members: undefined });
    let firstGroupState;
    try {
        const response = await makeRequestWithTemporalRetry({
            logId: `getGroupLog/${logId}`,
            publicParams,
            secretParams,
            request: (sender, options) => sender.getGroupLog(0, options),
        });
        // Attempt to start with the first group state, only later processing future updates
        firstGroupState = (_c = (_b = (_a = response === null || response === void 0 ? void 0 : response.changes) === null || _a === void 0 ? void 0 : _a.groupChanges) === null || _b === void 0 ? void 0 : _b[0]) === null || _c === void 0 ? void 0 : _c.groupState;
    }
    catch (error) {
        if (error.code === GROUP_ACCESS_DENIED_CODE) {
            log.info(`respondToGroupV2Migration/${logId}: Failed to access log endpoint; fetching full group state`);
            try {
                firstGroupState = await makeRequestWithTemporalRetry({
                    logId: `getGroup/${logId}`,
                    publicParams,
                    secretParams,
                    request: (sender, options) => sender.getGroup(options),
                });
            }
            catch (secondError) {
                if (secondError.code === GROUP_ACCESS_DENIED_CODE) {
                    log.info(`respondToGroupV2Migration/${logId}: Failed to access state endpoint; user is no longer part of group`);
                    // We don't want to add another event to the timeline
                    if (wereWePreviouslyAMember) {
                        const ourNumber = window.textsecure.storage.user.getNumber();
                        await updateGroup({
                            conversation,
                            receivedAt,
                            sentAt,
                            updates: {
                                newAttributes: Object.assign(Object.assign({}, conversation.attributes), { left: true, members: (conversation.get('members') || []).filter(item => item !== ourUuid && item !== ourNumber) }),
                                groupChangeMessages: [
                                    Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v2-change', groupV2Change: {
                                            details: [
                                                {
                                                    type: 'member-remove',
                                                    uuid: ourUuid,
                                                },
                                            ],
                                        } }),
                                ],
                                members: [],
                            },
                        });
                        return;
                    }
                }
                throw secondError;
            }
        }
        else {
            throw error;
        }
    }
    if (!firstGroupState) {
        throw new Error(`respondToGroupV2Migration/${logId}: Couldn't get a first group state!`);
    }
    const groupState = decryptGroupState(firstGroupState, attributes.secretParams, logId);
    const { newAttributes, newProfileKeys } = await applyGroupState({
        group: attributes,
        groupState,
    });
    // Generate notifications into the timeline
    const groupChangeMessages = [];
    groupChangeMessages.push(buildMigrationBubble(previousGroupV1MembersIds, newAttributes));
    const areWeInvited = (newAttributes.pendingMembersV2 || []).some(item => item.uuid === ourUuid);
    const areWeMember = (newAttributes.membersV2 || []).some(item => item.uuid === ourUuid);
    if (!areWeInvited && !areWeMember) {
        // Add a message to the timeline saying the user was removed. This shouldn't happen.
        groupChangeMessages.push(Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v2-change', groupV2Change: {
                details: [
                    {
                        type: 'member-remove',
                        uuid: ourUuid,
                    },
                ],
            } }));
    }
    // This buffer ensures that all migration-related messages are sorted above
    //   any initiating message. We need to do this because groupChangeMessages are
    //   already sorted via updates to sentAt inside of updateGroup().
    const SORT_BUFFER = 1000;
    await updateGroup({
        conversation,
        receivedAt,
        sentAt: sentAt ? sentAt - SORT_BUFFER : undefined,
        updates: {
            newAttributes,
            groupChangeMessages,
            members: profileKeysToMembers(newProfileKeys),
        },
    });
    if (window.storage.blocked.isGroupBlocked(previousGroupV1Id)) {
        window.storage.blocked.addBlockedGroup(groupId);
    }
    // Save these most recent updates to conversation
    updateConversation(conversation.attributes);
    // Finally, check for any changes to the group since its initial creation using normal
    //   group update codepaths.
    await maybeUpdateGroup({
        conversation,
        groupChangeBase64,
        newRevision,
        receivedAt,
        sentAt,
    });
}
exports.respondToGroupV2Migration = respondToGroupV2Migration;
const FIVE_MINUTES = 5 * durations.MINUTE;
async function waitThenMaybeUpdateGroup(options, { viaSync = false } = {}) {
    const { conversation } = options;
    if (conversation.isBlocked()) {
        log.info(`waitThenMaybeUpdateGroup: Group ${conversation.idForLogging()} is blocked, returning early`);
        return;
    }
    // First wait to process all incoming messages on the websocket
    await window.waitForEmptyEventQueue();
    // Then make sure we haven't fetched this group too recently
    const { lastSuccessfulGroupFetch = 0 } = conversation;
    if (!options.force &&
        (0, timestamp_1.isMoreRecentThan)(lastSuccessfulGroupFetch, FIVE_MINUTES)) {
        const waitTime = lastSuccessfulGroupFetch + FIVE_MINUTES - Date.now();
        log.info(`waitThenMaybeUpdateGroup/${conversation.idForLogging()}: group update ` +
            `was fetched recently, skipping for ${waitTime}ms`);
        return;
    }
    // Then wait to process all outstanding messages for this conversation
    await conversation.queueJob('waitThenMaybeUpdateGroup', async () => {
        try {
            // And finally try to update the group
            await maybeUpdateGroup(options, { viaSync });
            conversation.lastSuccessfulGroupFetch = Date.now();
        }
        catch (error) {
            log.error(`waitThenMaybeUpdateGroup/${conversation.idForLogging()}: maybeUpdateGroup failure:`, error && error.stack ? error.stack : error);
        }
    });
}
exports.waitThenMaybeUpdateGroup = waitThenMaybeUpdateGroup;
async function maybeUpdateGroup({ conversation, dropInitialJoinMessage, groupChangeBase64, newRevision, receivedAt, sentAt, }, { viaSync = false } = {}) {
    const logId = conversation.idForLogging();
    try {
        // Ensure we have the credentials we need before attempting GroupsV2 operations
        await (0, groupCredentialFetcher_1.maybeFetchNewCredentials)();
        const updates = await getGroupUpdates({
            group: conversation.attributes,
            serverPublicParamsBase64: window.getServerPublicParams(),
            newRevision,
            groupChangeBase64,
            dropInitialJoinMessage,
        });
        await updateGroup({ conversation, receivedAt, sentAt, updates }, { viaSync });
    }
    catch (error) {
        log.error(`maybeUpdateGroup/${logId}: Failed to update group:`, error && error.stack ? error.stack : error);
        throw error;
    }
}
exports.maybeUpdateGroup = maybeUpdateGroup;
async function updateGroup({ conversation, receivedAt, sentAt, updates, }, { viaSync = false } = {}) {
    const { newAttributes, groupChangeMessages, members } = updates;
    const ourUuid = window.textsecure.storage.user.getCheckedUuid();
    const startingRevision = conversation.get('revision');
    const endingRevision = newAttributes.revision;
    const isInitialDataFetch = !(0, lodash_1.isNumber)(startingRevision) && (0, lodash_1.isNumber)(endingRevision);
    const isInGroup = !updates.newAttributes.left;
    const justJoinedGroup = !conversation.hasMember(ourUuid.toString()) && isInGroup;
    // Ensure that all generated messages are ordered properly.
    // Before the provided timestamp so update messages appear before the
    //   initiating message, or after now().
    const finalReceivedAt = receivedAt || window.Signal.Util.incrementMessageCounter();
    const initialSentAt = sentAt || Date.now();
    // GroupV1 -> GroupV2 migration changes the groupId, and we need to update our id-based
    //   lookups if there's a change on that field.
    const previousId = conversation.get('groupId');
    const idChanged = previousId && previousId !== newAttributes.groupId;
    // We force this conversation into the left pane if this is the first time we've
    //   fetched data about it, and we were able to fetch its name. Nobody likes to see
    //   Unknown Group in the left pane.
    let activeAt = null;
    if (viaSync) {
        activeAt = null;
    }
    else if ((isInitialDataFetch || justJoinedGroup) && newAttributes.name) {
        activeAt = initialSentAt;
    }
    else {
        activeAt = newAttributes.active_at;
    }
    conversation.set(Object.assign(Object.assign({}, newAttributes), { active_at: activeAt, temporaryMemberCount: isInGroup
            ? undefined
            : newAttributes.temporaryMemberCount }));
    if (idChanged) {
        conversation.trigger('idUpdated', conversation, 'groupId', previousId);
    }
    // Save all synthetic messages describing group changes
    let syntheticSentAt = initialSentAt - (groupChangeMessages.length + 1);
    const changeMessagesToSave = groupChangeMessages.map(changeMessage => {
        // We do this to preserve the order of the timeline. We only update sentAt to ensure
        //   that we don't stomp on messages received around the same time as the message
        //   which initiated this group fetch and in-conversation messages.
        syntheticSentAt += 1;
        return Object.assign(Object.assign({}, changeMessage), { conversationId: conversation.id, received_at: finalReceivedAt, received_at_ms: syntheticSentAt, sent_at: syntheticSentAt });
    });
    if (changeMessagesToSave.length > 0) {
        await window.Signal.Data.saveMessages(changeMessagesToSave, {
            forceSave: true,
        });
        changeMessagesToSave.forEach(changeMessage => {
            const model = new window.Whisper.Message(changeMessage);
            window.MessageController.register(model.id, model);
            conversation.trigger('newmessage', model);
        });
    }
    // Capture profile key for each member in the group, if we don't have it yet
    members.forEach(member => {
        const contact = window.ConversationController.get(member.uuid);
        if (member.profileKey && contact && !contact.get('profileKey')) {
            contact.setProfileKey(member.profileKey);
        }
    });
    // No need for convo.updateLastMessage(), 'newmessage' handler does that
}
async function getGroupUpdates({ dropInitialJoinMessage, group, serverPublicParamsBase64, newRevision, groupChangeBase64, }) {
    const logId = idForLogging(group.groupId);
    log.info(`getGroupUpdates/${logId}: Starting...`);
    const currentRevision = group.revision;
    const isFirstFetch = !(0, lodash_1.isNumber)(group.revision);
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    const isInitialCreationMessage = isFirstFetch && newRevision === 0;
    const weAreAwaitingApproval = (group.pendingAdminApprovalV2 || []).find(item => item.uuid === ourUuid);
    const isOneVersionUp = (0, lodash_1.isNumber)(currentRevision) &&
        (0, lodash_1.isNumber)(newRevision) &&
        newRevision === currentRevision + 1;
    if (window.GV2_ENABLE_SINGLE_CHANGE_PROCESSING &&
        groupChangeBase64 &&
        (0, lodash_1.isNumber)(newRevision) &&
        (isInitialCreationMessage || weAreAwaitingApproval || isOneVersionUp)) {
        log.info(`getGroupUpdates/${logId}: Processing just one change`);
        const groupChangeBuffer = Bytes.fromBase64(groupChangeBase64);
        const groupChange = protobuf_1.SignalService.GroupChange.decode(groupChangeBuffer);
        const isChangeSupported = !(0, lodash_1.isNumber)(groupChange.changeEpoch) ||
            groupChange.changeEpoch <= SUPPORTED_CHANGE_EPOCH;
        if (isChangeSupported) {
            return updateGroupViaSingleChange({
                group,
                newRevision,
                groupChange,
                serverPublicParamsBase64,
            });
        }
        log.info(`getGroupUpdates/${logId}: Failing over; group change unsupported`);
    }
    if ((0, lodash_1.isNumber)(newRevision) && window.GV2_ENABLE_CHANGE_PROCESSING) {
        try {
            const result = await updateGroupViaLogs({
                group,
                serverPublicParamsBase64,
                newRevision,
            });
            return result;
        }
        catch (error) {
            if (error.code === TEMPORAL_AUTH_REJECTED_CODE) {
                // We will fail over to the updateGroupViaState call below
                log.info(`getGroupUpdates/${logId}: Temporal credential failure, now fetching full group state`);
            }
            else if (error.code === GROUP_ACCESS_DENIED_CODE) {
                // We will fail over to the updateGroupViaState call below
                log.info(`getGroupUpdates/${logId}: Log access denied, now fetching full group state`);
            }
            else {
                throw error;
            }
        }
    }
    if (window.GV2_ENABLE_STATE_PROCESSING) {
        return updateGroupViaState({
            dropInitialJoinMessage,
            group,
            serverPublicParamsBase64,
        });
    }
    log.warn(`getGroupUpdates/${logId}: No processing was legal! Returning empty changeset.`);
    return {
        newAttributes: group,
        groupChangeMessages: [],
        members: [],
    };
}
async function updateGroupViaState({ dropInitialJoinMessage, group, serverPublicParamsBase64, }) {
    const logId = idForLogging(group.groupId);
    const data = window.storage.get(groupCredentialFetcher_1.GROUP_CREDENTIALS_KEY);
    if (!data) {
        throw new Error('updateGroupViaState: No group credentials!');
    }
    const groupCredentials = (0, groupCredentialFetcher_1.getCredentialsForToday)(data);
    const stateOptions = {
        dropInitialJoinMessage,
        group,
        serverPublicParamsBase64,
        authCredentialBase64: groupCredentials.today.credential,
    };
    try {
        log.info(`updateGroupViaState/${logId}: Getting full group state...`);
        // We await this here so our try/catch below takes effect
        const result = await getCurrentGroupState(stateOptions);
        return result;
    }
    catch (error) {
        if (error.code === GROUP_ACCESS_DENIED_CODE) {
            return generateLeftGroupChanges(group);
        }
        if (error.code === TEMPORAL_AUTH_REJECTED_CODE) {
            log.info(`updateGroupViaState/${logId}: Credential for today failed, failing over to tomorrow...`);
            try {
                const result = await getCurrentGroupState(Object.assign(Object.assign({}, stateOptions), { authCredentialBase64: groupCredentials.tomorrow.credential }));
                return result;
            }
            catch (subError) {
                if (subError.code === GROUP_ACCESS_DENIED_CODE) {
                    return generateLeftGroupChanges(group);
                }
            }
        }
        throw error;
    }
}
async function updateGroupViaSingleChange({ group, groupChange, newRevision, serverPublicParamsBase64, }) {
    const wasInGroup = !group.left;
    const result = await integrateGroupChange({
        group,
        groupChange,
        newRevision,
    });
    const nowInGroup = !result.newAttributes.left;
    // If we were just added to the group (for example, via a join link), we go fetch the
    //   entire group state to make sure we're up to date.
    if (!wasInGroup && nowInGroup) {
        const { newAttributes, members } = await updateGroupViaState({
            group: result.newAttributes,
            serverPublicParamsBase64,
        });
        // We discard any change events that come out of this full group fetch, but we do
        //   keep the final group attributes generated, as well as any new members.
        return Object.assign(Object.assign({}, result), { members: [...result.members, ...members], newAttributes });
    }
    return result;
}
async function updateGroupViaLogs({ group, serverPublicParamsBase64, newRevision, }) {
    const logId = idForLogging(group.groupId);
    const data = window.storage.get(groupCredentialFetcher_1.GROUP_CREDENTIALS_KEY);
    if (!data) {
        throw new Error('getGroupUpdates: No group credentials!');
    }
    const groupCredentials = (0, groupCredentialFetcher_1.getCredentialsForToday)(data);
    const deltaOptions = {
        group,
        newRevision,
        serverPublicParamsBase64,
        authCredentialBase64: groupCredentials.today.credential,
    };
    try {
        log.info(`updateGroupViaLogs/${logId}: Getting group delta from ${group.revision} to ${newRevision} for group groupv2(${group.groupId})...`);
        const result = await getGroupDelta(deltaOptions);
        return result;
    }
    catch (error) {
        if (error.code === TEMPORAL_AUTH_REJECTED_CODE) {
            log.info(`updateGroupViaLogs/${logId}: Credential for today failed, failing over to tomorrow...`);
            return getGroupDelta(Object.assign(Object.assign({}, deltaOptions), { authCredentialBase64: groupCredentials.tomorrow.credential }));
        }
        throw error;
    }
}
function generateBasicMessage() {
    return {
        id: (0, uuid_1.v4)(),
        schemaVersion: message_1.CURRENT_SCHEMA_VERSION,
        // this is missing most properties to fulfill this type
    };
}
async function generateLeftGroupChanges(group) {
    const logId = idForLogging(group.groupId);
    log.info(`generateLeftGroupChanges/${logId}: Starting...`);
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    const { masterKey, groupInviteLinkPassword } = group;
    let { revision } = group;
    try {
        if (masterKey && groupInviteLinkPassword) {
            log.info(`generateLeftGroupChanges/${logId}: Have invite link. Attempting to fetch latest revision with it.`);
            const preJoinInfo = await getPreJoinGroupInfo(groupInviteLinkPassword, masterKey);
            revision = preJoinInfo.version;
        }
    }
    catch (error) {
        log.warn('generateLeftGroupChanges: Failed to fetch latest revision via group link. Code:', error.code);
    }
    const existingMembers = group.membersV2 || [];
    const newAttributes = Object.assign(Object.assign({}, group), { membersV2: existingMembers.filter(member => member.uuid !== ourUuid), left: true, revision });
    const isNewlyRemoved = existingMembers.length > (newAttributes.membersV2 || []).length;
    const youWereRemovedMessage = Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v2-change', groupV2Change: {
            details: [
                {
                    type: 'member-remove',
                    uuid: ourUuid,
                },
            ],
        } });
    return {
        newAttributes,
        groupChangeMessages: isNewlyRemoved ? [youWereRemovedMessage] : [],
        members: [],
    };
}
function getGroupCredentials({ authCredentialBase64, groupPublicParamsBase64, groupSecretParamsBase64, serverPublicParamsBase64, }) {
    const authOperations = (0, zkgroup_1.getClientZkAuthOperations)(serverPublicParamsBase64);
    const presentation = (0, zkgroup_1.getAuthCredentialPresentation)(authOperations, authCredentialBase64, groupSecretParamsBase64);
    return {
        groupPublicParamsHex: Bytes.toHex(Bytes.fromBase64(groupPublicParamsBase64)),
        authCredentialPresentationHex: Bytes.toHex(presentation),
    };
}
async function getGroupDelta({ group, newRevision, serverPublicParamsBase64, authCredentialBase64, }) {
    const sender = window.textsecure.messaging;
    if (!sender) {
        throw new Error('getGroupDelta: textsecure.messaging is not available!');
    }
    if (!group.publicParams) {
        throw new Error('getGroupDelta: group was missing publicParams!');
    }
    if (!group.secretParams) {
        throw new Error('getGroupDelta: group was missing secretParams!');
    }
    const options = getGroupCredentials({
        authCredentialBase64,
        groupPublicParamsBase64: group.publicParams,
        groupSecretParamsBase64: group.secretParams,
        serverPublicParamsBase64,
    });
    const currentRevision = group.revision;
    let revisionToFetch = (0, lodash_1.isNumber)(currentRevision) ? currentRevision + 1 : 0;
    let response;
    const changes = [];
    do {
        // eslint-disable-next-line no-await-in-loop
        response = await sender.getGroupLog(revisionToFetch, options);
        changes.push(response.changes);
        if (response.end) {
            revisionToFetch = response.end + 1;
        }
    } while (response.end && response.end < newRevision);
    // Would be nice to cache the unused groupChanges here, to reduce server roundtrips
    return integrateGroupChanges({
        changes,
        group,
        newRevision,
    });
}
async function integrateGroupChanges({ group, newRevision, changes, }) {
    const logId = idForLogging(group.groupId);
    let attributes = group;
    const finalMessages = [];
    const finalMembers = [];
    const imax = changes.length;
    for (let i = 0; i < imax; i += 1) {
        const { groupChanges } = changes[i];
        if (!groupChanges) {
            continue;
        }
        const jmax = groupChanges.length;
        for (let j = 0; j < jmax; j += 1) {
            const changeState = groupChanges[j];
            const { groupChange, groupState } = changeState;
            if (!groupChange && !groupState) {
                log.warn('integrateGroupChanges: item had neither groupState nor groupChange. Skipping.');
                continue;
            }
            try {
                const { newAttributes, groupChangeMessages, members,
                // eslint-disable-next-line no-await-in-loop
                 } = await integrateGroupChange({
                    group: attributes,
                    newRevision,
                    groupChange: (0, dropNull_1.dropNull)(groupChange),
                    groupState: (0, dropNull_1.dropNull)(groupState),
                });
                attributes = newAttributes;
                finalMessages.push(groupChangeMessages);
                finalMembers.push(members);
            }
            catch (error) {
                log.error(`integrateGroupChanges/${logId}: Failed to apply change log, continuing to apply remaining change logs.`, error && error.stack ? error.stack : error);
            }
        }
    }
    // If this is our first fetch, we will collapse this down to one set of messages
    const isFirstFetch = !(0, lodash_1.isNumber)(group.revision);
    if (isFirstFetch) {
        // The first array in finalMessages is from the first revision we could process. It
        //   should contain a message about how we joined the group.
        const joinMessages = finalMessages[0];
        const alreadyHaveJoinMessage = joinMessages && joinMessages.length > 0;
        // There have been other changes since that first revision, so we generate diffs for
        //   the whole of the change since then, likely without the initial join message.
        const otherMessages = extractDiffs({
            old: group,
            current: attributes,
            dropInitialJoinMessage: alreadyHaveJoinMessage,
        });
        const groupChangeMessages = alreadyHaveJoinMessage
            ? [joinMessages[0], ...otherMessages]
            : otherMessages;
        return {
            newAttributes: attributes,
            groupChangeMessages,
            members: (0, lodash_1.flatten)(finalMembers),
        };
    }
    return {
        newAttributes: attributes,
        groupChangeMessages: (0, lodash_1.flatten)(finalMessages),
        members: (0, lodash_1.flatten)(finalMembers),
    };
}
async function integrateGroupChange({ group, groupChange, groupState, newRevision, }) {
    const logId = idForLogging(group.groupId);
    if (!group.secretParams) {
        throw new Error(`integrateGroupChange/${logId}: Group was missing secretParams!`);
    }
    if (!groupChange && !groupState) {
        throw new Error(`integrateGroupChange/${logId}: Neither groupChange nor groupState received!`);
    }
    const isFirstFetch = !(0, lodash_1.isNumber)(group.revision);
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    const weAreAwaitingApproval = (group.pendingAdminApprovalV2 || []).find(item => item.uuid === ourUuid);
    // These need to be populated from the groupChange. But we might not get one!
    let isChangeSupported = false;
    let isMoreThanOneVersionUp = false;
    let groupChangeActions;
    let decryptedChangeActions;
    let sourceUuid;
    if (groupChange) {
        groupChangeActions = protobuf_1.SignalService.GroupChange.Actions.decode(groupChange.actions || new Uint8Array(0));
        if (groupChangeActions.version &&
            groupChangeActions.version > newRevision) {
            return {
                newAttributes: group,
                groupChangeMessages: [],
                members: [],
            };
        }
        decryptedChangeActions = decryptGroupChange(groupChangeActions, group.secretParams, logId);
        (0, assert_1.strictAssert)(decryptedChangeActions !== undefined, 'Should have decrypted group actions');
        ({ sourceUuid } = decryptedChangeActions);
        (0, assert_1.strictAssert)(sourceUuid, 'Should have source UUID');
        isChangeSupported =
            !(0, lodash_1.isNumber)(groupChange.changeEpoch) ||
                groupChange.changeEpoch <= SUPPORTED_CHANGE_EPOCH;
        isMoreThanOneVersionUp = Boolean(groupChangeActions.version &&
            (0, lodash_1.isNumber)(group.revision) &&
            groupChangeActions.version > group.revision + 1);
    }
    if (!groupChange ||
        !isChangeSupported ||
        isFirstFetch ||
        (isMoreThanOneVersionUp && !weAreAwaitingApproval)) {
        if (!groupState) {
            throw new Error(`integrateGroupChange/${logId}: No group state, but we can't apply changes!`);
        }
        log.info(`integrateGroupChange/${logId}: Applying full group state, from version ${group.revision} to ${groupState.version}`, {
            isChangePresent: Boolean(groupChange),
            isChangeSupported,
            isFirstFetch,
            isMoreThanOneVersionUp,
            weAreAwaitingApproval,
        });
        const decryptedGroupState = decryptGroupState(groupState, group.secretParams, logId);
        const { newAttributes, newProfileKeys } = await applyGroupState({
            group,
            groupState: decryptedGroupState,
            sourceUuid: isFirstFetch ? sourceUuid : undefined,
        });
        return {
            newAttributes,
            groupChangeMessages: extractDiffs({
                old: group,
                current: newAttributes,
                sourceUuid: isFirstFetch ? sourceUuid : undefined,
            }),
            members: profileKeysToMembers(newProfileKeys),
        };
    }
    if (!sourceUuid || !groupChangeActions || !decryptedChangeActions) {
        throw new Error(`integrateGroupChange/${logId}: Missing necessary information that should have come from group actions`);
    }
    log.info(`integrateGroupChange/${logId}: Applying group change actions, from version ${group.revision} to ${groupChangeActions.version}`);
    const { newAttributes, newProfileKeys } = await applyGroupChange({
        group,
        actions: decryptedChangeActions,
        sourceUuid,
    });
    const groupChangeMessages = extractDiffs({
        old: group,
        current: newAttributes,
        sourceUuid,
    });
    return {
        newAttributes,
        groupChangeMessages,
        members: profileKeysToMembers(newProfileKeys),
    };
}
async function getCurrentGroupState({ authCredentialBase64, dropInitialJoinMessage, group, serverPublicParamsBase64, }) {
    const logId = idForLogging(group.groupId);
    const sender = window.textsecure.messaging;
    if (!sender) {
        throw new Error('textsecure.messaging is not available!');
    }
    if (!group.secretParams) {
        throw new Error('getCurrentGroupState: group was missing secretParams!');
    }
    if (!group.publicParams) {
        throw new Error('getCurrentGroupState: group was missing publicParams!');
    }
    const options = getGroupCredentials({
        authCredentialBase64,
        groupPublicParamsBase64: group.publicParams,
        groupSecretParamsBase64: group.secretParams,
        serverPublicParamsBase64,
    });
    const groupState = await sender.getGroup(options);
    const decryptedGroupState = decryptGroupState(groupState, group.secretParams, logId);
    const oldVersion = group.revision;
    const newVersion = decryptedGroupState.version;
    log.info(`getCurrentGroupState/${logId}: Applying full group state, from version ${oldVersion} to ${newVersion}.`);
    const { newAttributes, newProfileKeys } = await applyGroupState({
        group,
        groupState: decryptedGroupState,
    });
    return {
        newAttributes,
        groupChangeMessages: extractDiffs({
            old: group,
            current: newAttributes,
            dropInitialJoinMessage,
        }),
        members: profileKeysToMembers(newProfileKeys),
    };
}
function extractDiffs({ current, dropInitialJoinMessage, old, sourceUuid, }) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
    const logId = idForLogging(old.groupId);
    const details = [];
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
    let areWeInGroup = false;
    let areWeInvitedToGroup = false;
    let whoInvitedUsUserId = null;
    // access control
    if (current.accessControl &&
        old.accessControl &&
        old.accessControl.attributes !== undefined &&
        old.accessControl.attributes !== current.accessControl.attributes) {
        details.push({
            type: 'access-attributes',
            newPrivilege: current.accessControl.attributes,
        });
    }
    if (current.accessControl &&
        old.accessControl &&
        old.accessControl.members !== undefined &&
        old.accessControl.members !== current.accessControl.members) {
        details.push({
            type: 'access-members',
            newPrivilege: current.accessControl.members,
        });
    }
    const linkPreviouslyEnabled = ((_a = old.accessControl) === null || _a === void 0 ? void 0 : _a.addFromInviteLink) === ACCESS_ENUM.ANY ||
        ((_b = old.accessControl) === null || _b === void 0 ? void 0 : _b.addFromInviteLink) === ACCESS_ENUM.ADMINISTRATOR;
    const linkCurrentlyEnabled = ((_c = current.accessControl) === null || _c === void 0 ? void 0 : _c.addFromInviteLink) === ACCESS_ENUM.ANY ||
        ((_d = current.accessControl) === null || _d === void 0 ? void 0 : _d.addFromInviteLink) === ACCESS_ENUM.ADMINISTRATOR;
    if (!linkPreviouslyEnabled && linkCurrentlyEnabled) {
        details.push({
            type: 'group-link-add',
            privilege: ((_e = current.accessControl) === null || _e === void 0 ? void 0 : _e.addFromInviteLink) || ACCESS_ENUM.ANY,
        });
    }
    else if (linkPreviouslyEnabled && !linkCurrentlyEnabled) {
        details.push({
            type: 'group-link-remove',
        });
    }
    else if (linkPreviouslyEnabled &&
        linkCurrentlyEnabled &&
        ((_f = old.accessControl) === null || _f === void 0 ? void 0 : _f.addFromInviteLink) !==
            ((_g = current.accessControl) === null || _g === void 0 ? void 0 : _g.addFromInviteLink)) {
        details.push({
            type: 'access-invite-link',
            newPrivilege: ((_h = current.accessControl) === null || _h === void 0 ? void 0 : _h.addFromInviteLink) || ACCESS_ENUM.ANY,
        });
    }
    // avatar
    if (Boolean(old.avatar) !== Boolean(current.avatar) ||
        ((_j = old.avatar) === null || _j === void 0 ? void 0 : _j.hash) !== ((_k = current.avatar) === null || _k === void 0 ? void 0 : _k.hash)) {
        details.push({
            type: 'avatar',
            removed: !current.avatar,
        });
    }
    // name
    if (old.name !== current.name) {
        details.push({
            type: 'title',
            newTitle: current.name,
        });
    }
    // groupInviteLinkPassword
    // Note: we only capture link resets here. Enable/disable are controlled by the
    //   accessControl.addFromInviteLink
    if (old.groupInviteLinkPassword &&
        current.groupInviteLinkPassword &&
        old.groupInviteLinkPassword !== current.groupInviteLinkPassword) {
        details.push({
            type: 'group-link-reset',
        });
    }
    // description
    if (old.description !== current.description) {
        details.push({
            type: 'description',
            removed: !current.description,
            description: current.description,
        });
    }
    // No disappearing message timer check here - see below
    // membersV2
    const oldMemberLookup = new Map((old.membersV2 || []).map(member => [member.uuid, member]));
    const oldPendingMemberLookup = new Map((old.pendingMembersV2 || []).map(member => [member.uuid, member]));
    const oldPendingAdminApprovalLookup = new Map((old.pendingAdminApprovalV2 || []).map(member => [member.uuid, member]));
    (current.membersV2 || []).forEach(currentMember => {
        const { uuid } = currentMember;
        if (uuid === ourUuid) {
            areWeInGroup = true;
        }
        const oldMember = oldMemberLookup.get(uuid);
        if (!oldMember) {
            const pendingMember = oldPendingMemberLookup.get(uuid);
            if (pendingMember) {
                details.push({
                    type: 'member-add-from-invite',
                    uuid,
                    inviter: pendingMember.addedByUserId,
                });
            }
            else if (currentMember.joinedFromLink) {
                details.push({
                    type: 'member-add-from-link',
                    uuid,
                });
            }
            else if (currentMember.approvedByAdmin) {
                details.push({
                    type: 'member-add-from-admin-approval',
                    uuid,
                });
            }
            else {
                details.push({
                    type: 'member-add',
                    uuid,
                });
            }
        }
        else if (oldMember.role !== currentMember.role) {
            details.push({
                type: 'member-privilege',
                uuid,
                newPrivilege: currentMember.role,
            });
        }
        // We don't want to generate an admin-approval-remove event for this newly-added
        //   member. But we don't know for sure if this is an admin approval; for that we
        //   consulted the approvedByAdmin flag saved on the member.
        oldPendingAdminApprovalLookup.delete(uuid);
        // If we capture a pending remove here, it's an 'accept invitation', and we don't
        //   want to generate a pending-remove event for it
        oldPendingMemberLookup.delete(uuid);
        // This deletion makes it easier to capture removals
        oldMemberLookup.delete(uuid);
    });
    const removedMemberIds = Array.from(oldMemberLookup.keys());
    removedMemberIds.forEach(uuid => {
        details.push({
            type: 'member-remove',
            uuid,
        });
    });
    // pendingMembersV2
    let lastPendingUuid;
    let pendingCount = 0;
    (current.pendingMembersV2 || []).forEach(currentPendingMember => {
        const { uuid } = currentPendingMember;
        const oldPendingMember = oldPendingMemberLookup.get(uuid);
        if (uuid === ourUuid) {
            areWeInvitedToGroup = true;
            whoInvitedUsUserId = currentPendingMember.addedByUserId;
        }
        if (!oldPendingMember) {
            lastPendingUuid = uuid;
            pendingCount += 1;
        }
        // This deletion makes it easier to capture removals
        oldPendingMemberLookup.delete(uuid);
    });
    if (pendingCount > 1) {
        details.push({
            type: 'pending-add-many',
            count: pendingCount,
        });
    }
    else if (pendingCount === 1) {
        if (lastPendingUuid) {
            details.push({
                type: 'pending-add-one',
                uuid: lastPendingUuid,
            });
        }
        else {
            log.warn(`extractDiffs/${logId}: pendingCount was 1, no last conversationId available`);
        }
    }
    // Note: The only members left over here should be people who were moved from the
    //   pending list but also not added to the group at the same time.
    const removedPendingMemberIds = Array.from(oldPendingMemberLookup.keys());
    if (removedPendingMemberIds.length > 1) {
        const firstUuid = removedPendingMemberIds[0];
        const firstRemovedMember = oldPendingMemberLookup.get(firstUuid);
        (0, assert_1.strictAssert)(firstRemovedMember !== undefined, 'First removed member not found');
        const inviter = firstRemovedMember.addedByUserId;
        const allSameInviter = removedPendingMemberIds.every(id => { var _a; return ((_a = oldPendingMemberLookup.get(id)) === null || _a === void 0 ? void 0 : _a.addedByUserId) === inviter; });
        details.push({
            type: 'pending-remove-many',
            count: removedPendingMemberIds.length,
            inviter: allSameInviter ? inviter : undefined,
        });
    }
    else if (removedPendingMemberIds.length === 1) {
        const uuid = removedPendingMemberIds[0];
        const removedMember = oldPendingMemberLookup.get(uuid);
        (0, assert_1.strictAssert)(removedMember !== undefined, 'Removed member not found');
        details.push({
            type: 'pending-remove-one',
            uuid,
            inviter: removedMember.addedByUserId,
        });
    }
    // pendingAdminApprovalV2
    (current.pendingAdminApprovalV2 || []).forEach(currentPendingAdminAprovalMember => {
        const { uuid } = currentPendingAdminAprovalMember;
        const oldPendingMember = oldPendingAdminApprovalLookup.get(uuid);
        if (!oldPendingMember) {
            details.push({
                type: 'admin-approval-add-one',
                uuid,
            });
        }
        // This deletion makes it easier to capture removals
        oldPendingAdminApprovalLookup.delete(uuid);
    });
    // Note: The only members left over here should be people who were moved from the
    //   pendingAdminApproval list but also not added to the group at the same time.
    const removedPendingAdminApprovalIds = Array.from(oldPendingAdminApprovalLookup.keys());
    removedPendingAdminApprovalIds.forEach(uuid => {
        details.push({
            type: 'admin-approval-remove-one',
            uuid,
        });
    });
    // announcementsOnly
    if (Boolean(old.announcementsOnly) !== Boolean(current.announcementsOnly)) {
        details.push({
            type: 'announcements-only',
            announcementsOnly: Boolean(current.announcementsOnly),
        });
    }
    // final processing
    let message;
    let timerNotification;
    const firstUpdate = !(0, lodash_1.isNumber)(old.revision);
    // Here we hardcode initial messages if this is our first time processing data this
    //   group. Ideally we can collapse it down to just one of: 'you were added',
    //   'you were invited', or 'you created.'
    if (firstUpdate && areWeInvitedToGroup) {
        // Note, we will add 'you were invited' to group even if dropInitialJoinMessage = true
        message = Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v2-change', groupV2Change: {
                from: whoInvitedUsUserId || sourceUuid,
                details: [
                    {
                        type: 'pending-add-one',
                        uuid: ourUuid,
                    },
                ],
            } });
    }
    else if (firstUpdate && dropInitialJoinMessage) {
        // None of the rest of the messages should be added if dropInitialJoinMessage = true
        message = undefined;
    }
    else if (firstUpdate && sourceUuid && sourceUuid === ourUuid) {
        message = Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v2-change', groupV2Change: {
                from: sourceUuid,
                details: [
                    {
                        type: 'create',
                    },
                ],
            } });
    }
    else if (firstUpdate && areWeInGroup) {
        message = Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v2-change', groupV2Change: {
                from: sourceUuid,
                details: [
                    {
                        type: 'member-add',
                        uuid: ourUuid,
                    },
                ],
            } });
    }
    else if (firstUpdate) {
        message = Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v2-change', groupV2Change: {
                from: sourceUuid,
                details: [
                    {
                        type: 'create',
                    },
                ],
            } });
    }
    else if (details.length > 0) {
        message = Object.assign(Object.assign({}, generateBasicMessage()), { type: 'group-v2-change', sourceUuid, groupV2Change: {
                from: sourceUuid,
                details,
            } });
    }
    // This is checked differently, because it needs to be its own entry in the timeline,
    //   with its own icon, etc.
    if (
    // Turn on or turned off
    Boolean(old.expireTimer) !== Boolean(current.expireTimer) ||
        // Still on, but changed value
        (Boolean(old.expireTimer) &&
            Boolean(current.expireTimer) &&
            old.expireTimer !== current.expireTimer)) {
        timerNotification = Object.assign(Object.assign({}, generateBasicMessage()), { type: 'timer-notification', sourceUuid, flags: protobuf_1.SignalService.DataMessage.Flags.EXPIRATION_TIMER_UPDATE, expirationTimerUpdate: {
                expireTimer: current.expireTimer || 0,
                sourceUuid,
            } });
    }
    const result = (0, lodash_1.compact)([message, timerNotification]);
    log.info(`extractDiffs/${logId} complete, generated ${result.length} change messages`);
    return result;
}
function profileKeysToMembers(items) {
    return items.map(item => ({
        profileKey: Bytes.toBase64(item.profileKey),
        uuid: item.uuid,
    }));
}
async function applyGroupChange({ actions, group, sourceUuid, }) {
    var _a;
    const logId = idForLogging(group.groupId);
    const ourUuid = (_a = window.storage.user.getUuid()) === null || _a === void 0 ? void 0 : _a.toString();
    const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
    const MEMBER_ROLE_ENUM = protobuf_1.SignalService.Member.Role;
    const version = actions.version || 0;
    const result = Object.assign({}, group);
    const newProfileKeys = [];
    const members = (0, lodash_1.fromPairs)((result.membersV2 || []).map(member => [member.uuid, member]));
    const pendingMembers = (0, lodash_1.fromPairs)((result.pendingMembersV2 || []).map(member => [member.uuid, member]));
    const pendingAdminApprovalMembers = (0, lodash_1.fromPairs)((result.pendingAdminApprovalV2 || []).map(member => [member.uuid, member]));
    // version?: number;
    result.revision = version;
    // addMembers?: Array<GroupChange.Actions.AddMemberAction>;
    (actions.addMembers || []).forEach(addMember => {
        const { added } = addMember;
        if (!added || !added.userId) {
            throw new Error('applyGroupChange: addMember.added is missing');
        }
        const addedUuid = UUID_1.UUID.cast(added.userId);
        if (members[addedUuid]) {
            log.warn(`applyGroupChange/${logId}: Attempt to add member failed; already in members.`);
            return;
        }
        members[addedUuid] = {
            uuid: addedUuid,
            role: added.role || MEMBER_ROLE_ENUM.DEFAULT,
            joinedAtVersion: version,
            joinedFromLink: addMember.joinFromInviteLink || false,
        };
        if (pendingMembers[addedUuid]) {
            log.warn(`applyGroupChange/${logId}: Removing newly-added member from pendingMembers.`);
            delete pendingMembers[addedUuid];
        }
        // Capture who added us
        if (ourUuid && sourceUuid && addedUuid === ourUuid) {
            result.addedBy = sourceUuid;
        }
        if (added.profileKey) {
            newProfileKeys.push({
                profileKey: added.profileKey,
                uuid: UUID_1.UUID.cast(added.userId),
            });
        }
    });
    // deleteMembers?: Array<GroupChange.Actions.DeleteMemberAction>;
    (actions.deleteMembers || []).forEach(deleteMember => {
        const { deletedUserId } = deleteMember;
        if (!deletedUserId) {
            throw new Error('applyGroupChange: deleteMember.deletedUserId is missing');
        }
        const deletedUuid = UUID_1.UUID.cast(deletedUserId);
        if (members[deletedUuid]) {
            delete members[deletedUuid];
        }
        else {
            log.warn(`applyGroupChange/${logId}: Attempt to remove member failed; was not in members.`);
        }
    });
    // modifyMemberRoles?: Array<GroupChange.Actions.ModifyMemberRoleAction>;
    (actions.modifyMemberRoles || []).forEach(modifyMemberRole => {
        const { role, userId } = modifyMemberRole;
        if (!role || !userId) {
            throw new Error('applyGroupChange: modifyMemberRole had a missing value');
        }
        const userUuid = UUID_1.UUID.cast(userId);
        if (members[userUuid]) {
            members[userUuid] = Object.assign(Object.assign({}, members[userUuid]), { role });
        }
        else {
            throw new Error('applyGroupChange: modifyMemberRole tried to modify nonexistent member');
        }
    });
    // modifyMemberProfileKeys?:
    // Array<GroupChange.Actions.ModifyMemberProfileKeyAction>;
    (actions.modifyMemberProfileKeys || []).forEach(modifyMemberProfileKey => {
        const { profileKey, uuid } = modifyMemberProfileKey;
        if (!profileKey || !uuid) {
            throw new Error('applyGroupChange: modifyMemberProfileKey had a missing value');
        }
        newProfileKeys.push({
            profileKey,
            uuid: UUID_1.UUID.cast(uuid),
        });
    });
    // addPendingMembers?: Array<
    //   GroupChange.Actions.AddMemberPendingProfileKeyAction
    // >;
    (actions.addPendingMembers || []).forEach(addPendingMember => {
        const { added } = addPendingMember;
        if (!added || !added.member || !added.member.userId) {
            throw new Error('applyGroupChange: addPendingMembers had a missing value');
        }
        const addedUuid = UUID_1.UUID.cast(added.member.userId);
        if (members[addedUuid]) {
            log.warn(`applyGroupChange/${logId}: Attempt to add pendingMember failed; was already in members.`);
            return;
        }
        if (pendingMembers[addedUuid]) {
            log.warn(`applyGroupChange/${logId}: Attempt to add pendingMember failed; was already in pendingMembers.`);
            return;
        }
        pendingMembers[addedUuid] = {
            uuid: addedUuid,
            addedByUserId: UUID_1.UUID.cast(added.addedByUserId),
            timestamp: added.timestamp,
            role: added.member.role || MEMBER_ROLE_ENUM.DEFAULT,
        };
        if (added.member && added.member.profileKey) {
            newProfileKeys.push({
                profileKey: added.member.profileKey,
                uuid: addedUuid,
            });
        }
    });
    // deletePendingMembers?: Array<
    //   GroupChange.Actions.DeleteMemberPendingProfileKeyAction
    // >;
    (actions.deletePendingMembers || []).forEach(deletePendingMember => {
        const { deletedUserId } = deletePendingMember;
        if (!deletedUserId) {
            throw new Error('applyGroupChange: deletePendingMember.deletedUserId is null!');
        }
        const deletedUuid = UUID_1.UUID.cast(deletedUserId);
        if (pendingMembers[deletedUuid]) {
            delete pendingMembers[deletedUuid];
        }
        else {
            log.warn(`applyGroupChange/${logId}: Attempt to remove pendingMember failed; was not in pendingMembers.`);
        }
    });
    // promotePendingMembers?: Array<
    //   GroupChange.Actions.PromoteMemberPendingProfileKeyAction
    // >;
    (actions.promotePendingMembers || []).forEach(promotePendingMember => {
        const { profileKey, uuid: rawUuid } = promotePendingMember;
        if (!profileKey || !rawUuid) {
            throw new Error('applyGroupChange: promotePendingMember had a missing value');
        }
        const uuid = UUID_1.UUID.cast(rawUuid);
        const previousRecord = pendingMembers[uuid];
        if (pendingMembers[uuid]) {
            delete pendingMembers[uuid];
        }
        else {
            log.warn(`applyGroupChange/${logId}: Attempt to promote pendingMember failed; was not in pendingMembers.`);
        }
        if (members[uuid]) {
            log.warn(`applyGroupChange/${logId}: Attempt to promote pendingMember failed; was already in members.`);
            return;
        }
        members[uuid] = {
            uuid,
            joinedAtVersion: version,
            role: previousRecord.role || MEMBER_ROLE_ENUM.DEFAULT,
        };
        newProfileKeys.push({
            profileKey,
            uuid,
        });
    });
    // modifyTitle?: GroupChange.Actions.ModifyTitleAction;
    if (actions.modifyTitle) {
        const { title } = actions.modifyTitle;
        if (title && title.content === 'title') {
            result.name = title.title;
        }
        else {
            log.warn(`applyGroupChange/${logId}: Clearing group title due to missing data.`);
            result.name = undefined;
        }
    }
    // modifyAvatar?: GroupChange.Actions.ModifyAvatarAction;
    if (actions.modifyAvatar) {
        const { avatar } = actions.modifyAvatar;
        await applyNewAvatar((0, dropNull_1.dropNull)(avatar), result, logId);
    }
    // modifyDisappearingMessagesTimer?:
    //   GroupChange.Actions.ModifyDisappearingMessagesTimerAction;
    if (actions.modifyDisappearingMessagesTimer) {
        const disappearingMessagesTimer = actions.modifyDisappearingMessagesTimer.timer;
        if (disappearingMessagesTimer &&
            disappearingMessagesTimer.content === 'disappearingMessagesDuration') {
            result.expireTimer =
                disappearingMessagesTimer.disappearingMessagesDuration;
        }
        else {
            log.warn(`applyGroupChange/${logId}: Clearing group expireTimer due to missing data.`);
            result.expireTimer = undefined;
        }
    }
    result.accessControl = result.accessControl || {
        members: ACCESS_ENUM.MEMBER,
        attributes: ACCESS_ENUM.MEMBER,
        addFromInviteLink: ACCESS_ENUM.UNSATISFIABLE,
    };
    // modifyAttributesAccess?:
    // GroupChange.Actions.ModifyAttributesAccessControlAction;
    if (actions.modifyAttributesAccess) {
        result.accessControl = Object.assign(Object.assign({}, result.accessControl), { attributes: actions.modifyAttributesAccess.attributesAccess || ACCESS_ENUM.MEMBER });
    }
    // modifyMemberAccess?: GroupChange.Actions.ModifyMembersAccessControlAction;
    if (actions.modifyMemberAccess) {
        result.accessControl = Object.assign(Object.assign({}, result.accessControl), { members: actions.modifyMemberAccess.membersAccess || ACCESS_ENUM.MEMBER });
    }
    // modifyAddFromInviteLinkAccess?:
    //   GroupChange.Actions.ModifyAddFromInviteLinkAccessControlAction;
    if (actions.modifyAddFromInviteLinkAccess) {
        result.accessControl = Object.assign(Object.assign({}, result.accessControl), { addFromInviteLink: actions.modifyAddFromInviteLinkAccess.addFromInviteLinkAccess ||
                ACCESS_ENUM.UNSATISFIABLE });
    }
    // addMemberPendingAdminApprovals?: Array<
    //   GroupChange.Actions.AddMemberPendingAdminApprovalAction
    // >;
    (actions.addMemberPendingAdminApprovals || []).forEach(pendingAdminApproval => {
        const { added } = pendingAdminApproval;
        if (!added) {
            throw new Error('applyGroupChange: modifyMemberProfileKey had a missing value');
        }
        const addedUuid = UUID_1.UUID.cast(added.userId);
        if (members[addedUuid]) {
            log.warn(`applyGroupChange/${logId}: Attempt to add pending admin approval failed; was already in members.`);
            return;
        }
        if (pendingMembers[addedUuid]) {
            log.warn(`applyGroupChange/${logId}: Attempt to add pending admin approval failed; was already in pendingMembers.`);
            return;
        }
        if (pendingAdminApprovalMembers[addedUuid]) {
            log.warn(`applyGroupChange/${logId}: Attempt to add pending admin approval failed; was already in pendingAdminApprovalMembers.`);
            return;
        }
        pendingAdminApprovalMembers[addedUuid] = {
            uuid: addedUuid,
            timestamp: added.timestamp,
        };
        if (added.profileKey) {
            newProfileKeys.push({
                profileKey: added.profileKey,
                uuid: addedUuid,
            });
        }
    });
    // deleteMemberPendingAdminApprovals?: Array<
    //   GroupChange.Actions.DeleteMemberPendingAdminApprovalAction
    // >;
    (actions.deleteMemberPendingAdminApprovals || []).forEach(deleteAdminApproval => {
        const { deletedUserId } = deleteAdminApproval;
        if (!deletedUserId) {
            throw new Error('applyGroupChange: deleteAdminApproval.deletedUserId is null!');
        }
        const deletedUuid = UUID_1.UUID.cast(deletedUserId);
        if (pendingAdminApprovalMembers[deletedUuid]) {
            delete pendingAdminApprovalMembers[deletedUuid];
        }
        else {
            log.warn(`applyGroupChange/${logId}: Attempt to remove pendingAdminApproval failed; was not in pendingAdminApprovalMembers.`);
        }
    });
    // promoteMemberPendingAdminApprovals?: Array<
    //   GroupChange.Actions.PromoteMemberPendingAdminApprovalAction
    // >;
    (actions.promoteMemberPendingAdminApprovals || []).forEach(promoteAdminApproval => {
        const { userId, role } = promoteAdminApproval;
        if (!userId) {
            throw new Error('applyGroupChange: promoteAdminApproval had a missing value');
        }
        const userUuid = UUID_1.UUID.cast(userId);
        if (pendingAdminApprovalMembers[userUuid]) {
            delete pendingAdminApprovalMembers[userUuid];
        }
        else {
            log.warn(`applyGroupChange/${logId}: Attempt to promote pendingAdminApproval failed; was not in pendingAdminApprovalMembers.`);
        }
        if (pendingMembers[userUuid]) {
            delete pendingAdminApprovalMembers[userUuid];
            log.warn(`applyGroupChange/${logId}: Deleted pendingAdminApproval from pendingMembers.`);
        }
        if (members[userUuid]) {
            log.warn(`applyGroupChange/${logId}: Attempt to promote pendingMember failed; was already in members.`);
            return;
        }
        members[userUuid] = {
            uuid: userUuid,
            joinedAtVersion: version,
            role: role || MEMBER_ROLE_ENUM.DEFAULT,
            approvedByAdmin: true,
        };
    });
    // modifyInviteLinkPassword?: GroupChange.Actions.ModifyInviteLinkPasswordAction;
    if (actions.modifyInviteLinkPassword) {
        const { inviteLinkPassword } = actions.modifyInviteLinkPassword;
        if (inviteLinkPassword) {
            result.groupInviteLinkPassword = inviteLinkPassword;
        }
        else {
            result.groupInviteLinkPassword = undefined;
        }
    }
    // modifyDescription?: GroupChange.Actions.ModifyDescriptionAction;
    if (actions.modifyDescription) {
        const { descriptionBytes } = actions.modifyDescription;
        if (descriptionBytes && descriptionBytes.content === 'descriptionText') {
            result.description = descriptionBytes.descriptionText;
        }
        else {
            log.warn(`applyGroupChange/${logId}: Clearing group description due to missing data.`);
            result.description = undefined;
        }
    }
    if (actions.modifyAnnouncementsOnly) {
        const { announcementsOnly } = actions.modifyAnnouncementsOnly;
        result.announcementsOnly = announcementsOnly;
    }
    if (ourUuid) {
        result.left = !members[ourUuid];
    }
    // Go from lookups back to arrays
    result.membersV2 = (0, lodash_1.values)(members);
    result.pendingMembersV2 = (0, lodash_1.values)(pendingMembers);
    result.pendingAdminApprovalV2 = (0, lodash_1.values)(pendingAdminApprovalMembers);
    return {
        newAttributes: result,
        newProfileKeys,
    };
}
async function decryptGroupAvatar(avatarKey, secretParamsBase64) {
    const sender = window.textsecure.messaging;
    if (!sender) {
        throw new Error('decryptGroupAvatar: textsecure.messaging is not available!');
    }
    const ciphertext = await sender.getGroupAvatar(avatarKey);
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(secretParamsBase64);
    const plaintext = (0, zkgroup_1.decryptGroupBlob)(clientZkGroupCipher, ciphertext);
    const blob = protobuf_1.SignalService.GroupAttributeBlob.decode(plaintext);
    if (blob.content !== 'avatar') {
        throw new Error(`decryptGroupAvatar: Returned blob had incorrect content: ${blob.content}`);
    }
    return blob.avatar;
}
exports.decryptGroupAvatar = decryptGroupAvatar;
// Ovewriting result.avatar as part of functionality
/* eslint-disable no-param-reassign */
async function applyNewAvatar(newAvatar, result, logId) {
    try {
        // Avatar has been dropped
        if (!newAvatar && result.avatar) {
            await window.Signal.Migrations.deleteAttachmentData(result.avatar.path);
            result.avatar = undefined;
        }
        // Group has avatar; has it changed?
        if (newAvatar && (!result.avatar || result.avatar.url !== newAvatar)) {
            if (!result.secretParams) {
                throw new Error('applyNewAvatar: group was missing secretParams!');
            }
            const data = await decryptGroupAvatar(newAvatar, result.secretParams);
            const hash = (0, Crypto_1.computeHash)(data);
            if (result.avatar && result.avatar.path && result.avatar.hash !== hash) {
                await window.Signal.Migrations.deleteAttachmentData(result.avatar.path);
                result.avatar = undefined;
            }
            if (!result.avatar) {
                const path = await window.Signal.Migrations.writeNewAttachmentData(data);
                result.avatar = {
                    url: newAvatar,
                    path,
                    hash,
                };
            }
        }
    }
    catch (error) {
        log.warn(`applyNewAvatar/${logId} Failed to handle avatar, clearing it`, error.stack);
        if (result.avatar && result.avatar.path) {
            await window.Signal.Migrations.deleteAttachmentData(result.avatar.path);
        }
        result.avatar = undefined;
    }
}
exports.applyNewAvatar = applyNewAvatar;
/* eslint-enable no-param-reassign */
async function applyGroupState({ group, groupState, sourceUuid, }) {
    const logId = idForLogging(group.groupId);
    const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
    const MEMBER_ROLE_ENUM = protobuf_1.SignalService.Member.Role;
    const version = groupState.version || 0;
    const result = Object.assign({}, group);
    const newProfileKeys = [];
    // version
    result.revision = version;
    // title
    // Note: During decryption, title becomes a GroupAttributeBlob
    const { title } = groupState;
    if (title && title.content === 'title') {
        result.name = title.title;
    }
    else {
        result.name = undefined;
    }
    // avatar
    await applyNewAvatar((0, dropNull_1.dropNull)(groupState.avatar), result, logId);
    // disappearingMessagesTimer
    // Note: during decryption, disappearingMessageTimer becomes a GroupAttributeBlob
    const { disappearingMessagesTimer } = groupState;
    if (disappearingMessagesTimer &&
        disappearingMessagesTimer.content === 'disappearingMessagesDuration') {
        result.expireTimer = disappearingMessagesTimer.disappearingMessagesDuration;
    }
    else {
        result.expireTimer = undefined;
    }
    // accessControl
    const { accessControl } = groupState;
    result.accessControl = {
        attributes: (accessControl && accessControl.attributes) || ACCESS_ENUM.MEMBER,
        members: (accessControl && accessControl.members) || ACCESS_ENUM.MEMBER,
        addFromInviteLink: (accessControl && accessControl.addFromInviteLink) ||
            ACCESS_ENUM.UNSATISFIABLE,
    };
    // Optimization: we assume we have left the group unless we are found in members
    result.left = true;
    const ourUuid = window.storage.user.getCheckedUuid().toString();
    // members
    if (groupState.members) {
        result.membersV2 = groupState.members.map(member => {
            if (member.userId === ourUuid) {
                result.left = false;
                // Capture who added us if we were previously not in group
                if (sourceUuid &&
                    (result.membersV2 || []).every(item => item.uuid !== ourUuid)) {
                    result.addedBy = sourceUuid;
                }
            }
            if (!isValidRole(member.role)) {
                throw new Error(`applyGroupState: Member had invalid role ${member.role}`);
            }
            if (member.profileKey) {
                newProfileKeys.push({
                    profileKey: member.profileKey,
                    uuid: UUID_1.UUID.cast(member.userId),
                });
            }
            return {
                role: member.role || MEMBER_ROLE_ENUM.DEFAULT,
                joinedAtVersion: member.joinedAtVersion || version,
                uuid: UUID_1.UUID.cast(member.userId),
            };
        });
    }
    // membersPendingProfileKey
    if (groupState.membersPendingProfileKey) {
        result.pendingMembersV2 = groupState.membersPendingProfileKey.map(member => {
            if (!member.member || !member.member.userId) {
                throw new Error('applyGroupState: Member pending profile key did not have an associated userId');
            }
            if (!member.addedByUserId) {
                throw new Error('applyGroupState: Member pending profile key did not have an addedByUserID');
            }
            if (!isValidRole(member.member.role)) {
                throw new Error(`applyGroupState: Member pending profile key had invalid role ${member.member.role}`);
            }
            if (member.member.profileKey) {
                newProfileKeys.push({
                    profileKey: member.member.profileKey,
                    uuid: UUID_1.UUID.cast(member.member.userId),
                });
            }
            return {
                addedByUserId: UUID_1.UUID.cast(member.addedByUserId),
                uuid: UUID_1.UUID.cast(member.member.userId),
                timestamp: member.timestamp,
                role: member.member.role || MEMBER_ROLE_ENUM.DEFAULT,
            };
        });
    }
    // membersPendingAdminApproval
    if (groupState.membersPendingAdminApproval) {
        result.pendingAdminApprovalV2 = groupState.membersPendingAdminApproval.map(member => {
            return {
                uuid: UUID_1.UUID.cast(member.userId),
                timestamp: member.timestamp,
            };
        });
    }
    // inviteLinkPassword
    const { inviteLinkPassword } = groupState;
    if (inviteLinkPassword) {
        result.groupInviteLinkPassword = inviteLinkPassword;
    }
    else {
        result.groupInviteLinkPassword = undefined;
    }
    // descriptionBytes
    const { descriptionBytes } = groupState;
    if (descriptionBytes && descriptionBytes.content === 'descriptionText') {
        result.description = descriptionBytes.descriptionText;
    }
    else {
        result.description = undefined;
    }
    // announcementsOnly
    result.announcementsOnly = groupState.announcementsOnly;
    return {
        newAttributes: result,
        newProfileKeys,
    };
}
function isValidRole(role) {
    const MEMBER_ROLE_ENUM = protobuf_1.SignalService.Member.Role;
    return (role === MEMBER_ROLE_ENUM.ADMINISTRATOR || role === MEMBER_ROLE_ENUM.DEFAULT);
}
function isValidAccess(access) {
    const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
    return access === ACCESS_ENUM.ADMINISTRATOR || access === ACCESS_ENUM.MEMBER;
}
function isValidLinkAccess(access) {
    const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
    return (access === ACCESS_ENUM.UNKNOWN ||
        access === ACCESS_ENUM.ANY ||
        access === ACCESS_ENUM.ADMINISTRATOR ||
        access === ACCESS_ENUM.UNSATISFIABLE);
}
function isValidProfileKey(buffer) {
    return Boolean(buffer && buffer.length === 32);
}
function normalizeTimestamp(timestamp) {
    if (!timestamp) {
        return 0;
    }
    if (typeof timestamp === 'number') {
        return timestamp;
    }
    const asNumber = timestamp.toNumber();
    const now = Date.now();
    if (!asNumber || asNumber > now) {
        return now;
    }
    return asNumber;
}
function decryptGroupChange(actions, groupSecretParams, logId) {
    const result = {
        version: (0, dropNull_1.dropNull)(actions.version),
    };
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(groupSecretParams);
    if (actions.sourceUuid && actions.sourceUuid.length !== 0) {
        try {
            result.sourceUuid = UUID_1.UUID.cast((0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, actions.sourceUuid), 'actions.sourceUuid'));
        }
        catch (error) {
            log.warn(`decryptGroupChange/${logId}: Unable to decrypt sourceUuid.`, error && error.stack ? error.stack : error);
        }
        if (!(0, UUID_1.isValidUuid)(result.sourceUuid)) {
            log.warn(`decryptGroupChange/${logId}: Invalid sourceUuid. Clearing sourceUuid.`);
            result.sourceUuid = undefined;
        }
    }
    else {
        throw new Error('decryptGroupChange: Missing sourceUuid');
    }
    // addMembers?: Array<GroupChange.Actions.AddMemberAction>;
    result.addMembers = (0, lodash_1.compact)((actions.addMembers || []).map(addMember => {
        (0, assert_1.strictAssert)(addMember.added, 'decryptGroupChange: AddMember was missing added field!');
        const decrypted = decryptMember(clientZkGroupCipher, addMember.added, logId);
        if (!decrypted) {
            return null;
        }
        return {
            added: decrypted,
            joinFromInviteLink: Boolean(addMember.joinFromInviteLink),
        };
    }));
    // deleteMembers?: Array<GroupChange.Actions.DeleteMemberAction>;
    result.deleteMembers = (0, lodash_1.compact)((actions.deleteMembers || []).map(deleteMember => {
        const { deletedUserId } = deleteMember;
        (0, assert_1.strictAssert)(Bytes.isNotEmpty(deletedUserId), 'decryptGroupChange: deleteMember.deletedUserId was missing');
        let userId;
        try {
            userId = (0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, deletedUserId), 'actions.deleteMembers.deletedUserId');
        }
        catch (error) {
            log.warn(`decryptGroupChange/${logId}: Unable to decrypt deleteMembers.deletedUserId. Dropping member.`, error && error.stack ? error.stack : error);
            return null;
        }
        if (!(0, UUID_1.isValidUuid)(userId)) {
            log.warn(`decryptGroupChange/${logId}: Dropping deleteMember due to invalid userId`);
            return null;
        }
        return { deletedUserId: userId };
    }));
    // modifyMemberRoles?: Array<GroupChange.Actions.ModifyMemberRoleAction>;
    result.modifyMemberRoles = (0, lodash_1.compact)((actions.modifyMemberRoles || []).map(modifyMember => {
        (0, assert_1.strictAssert)(Bytes.isNotEmpty(modifyMember.userId), 'decryptGroupChange: modifyMemberRole.userId was missing');
        let userId;
        try {
            userId = (0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, modifyMember.userId), 'actions.modifyMemberRoles.userId');
        }
        catch (error) {
            log.warn(`decryptGroupChange/${logId}: Unable to decrypt modifyMemberRole.userId. Dropping member.`, error && error.stack ? error.stack : error);
            return null;
        }
        if (!(0, UUID_1.isValidUuid)(userId)) {
            log.warn(`decryptGroupChange/${logId}: Dropping modifyMemberRole due to invalid userId`);
            return null;
        }
        const role = (0, dropNull_1.dropNull)(modifyMember.role);
        if (!isValidRole(role)) {
            throw new Error(`decryptGroupChange: modifyMemberRole had invalid role ${modifyMember.role}`);
        }
        return {
            role,
            userId,
        };
    }));
    // modifyMemberProfileKeys?: Array<
    //   GroupChange.Actions.ModifyMemberProfileKeyAction
    // >;
    result.modifyMemberProfileKeys = (0, lodash_1.compact)((actions.modifyMemberProfileKeys || []).map(modifyMemberProfileKey => {
        const { presentation } = modifyMemberProfileKey;
        (0, assert_1.strictAssert)(Bytes.isNotEmpty(presentation), 'decryptGroupChange: modifyMemberProfileKey.presentation was missing');
        const decryptedPresentation = (0, zkgroup_1.decryptProfileKeyCredentialPresentation)(clientZkGroupCipher, presentation);
        if (!decryptedPresentation.uuid || !decryptedPresentation.profileKey) {
            throw new Error('decryptGroupChange: uuid or profileKey missing after modifyMemberProfileKey decryption!');
        }
        if (!(0, UUID_1.isValidUuid)(decryptedPresentation.uuid)) {
            log.warn(`decryptGroupChange/${logId}: Dropping modifyMemberProfileKey due to invalid userId`);
            return null;
        }
        if (!isValidProfileKey(decryptedPresentation.profileKey)) {
            throw new Error('decryptGroupChange: modifyMemberProfileKey had invalid profileKey');
        }
        return decryptedPresentation;
    }));
    // addPendingMembers?: Array<
    //   GroupChange.Actions.AddMemberPendingProfileKeyAction
    // >;
    result.addPendingMembers = (0, lodash_1.compact)((actions.addPendingMembers || []).map(addPendingMember => {
        (0, assert_1.strictAssert)(addPendingMember.added, 'decryptGroupChange: addPendingMember was missing added field!');
        const decrypted = decryptMemberPendingProfileKey(clientZkGroupCipher, addPendingMember.added, logId);
        if (!decrypted) {
            return null;
        }
        return {
            added: decrypted,
        };
    }));
    // deletePendingMembers?: Array<
    //   GroupChange.Actions.DeleteMemberPendingProfileKeyAction
    // >;
    result.deletePendingMembers = (0, lodash_1.compact)((actions.deletePendingMembers || []).map(deletePendingMember => {
        const { deletedUserId } = deletePendingMember;
        (0, assert_1.strictAssert)(Bytes.isNotEmpty(deletedUserId), 'decryptGroupChange: deletePendingMembers.deletedUserId was missing');
        let userId;
        try {
            userId = (0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, deletedUserId), 'actions.deletePendingMembers.deletedUserId');
        }
        catch (error) {
            log.warn(`decryptGroupChange/${logId}: Unable to decrypt deletePendingMembers.deletedUserId. Dropping member.`, error && error.stack ? error.stack : error);
            return null;
        }
        if (!(0, UUID_1.isValidUuid)(userId)) {
            log.warn(`decryptGroupChange/${logId}: Dropping deletePendingMember due to invalid deletedUserId`);
            return null;
        }
        return {
            deletedUserId: userId,
        };
    }));
    // promotePendingMembers?: Array<
    //   GroupChange.Actions.PromoteMemberPendingProfileKeyAction
    // >;
    result.promotePendingMembers = (0, lodash_1.compact)((actions.promotePendingMembers || []).map(promotePendingMember => {
        const { presentation } = promotePendingMember;
        (0, assert_1.strictAssert)(Bytes.isNotEmpty(presentation), 'decryptGroupChange: promotePendingMember.presentation was missing');
        const decryptedPresentation = (0, zkgroup_1.decryptProfileKeyCredentialPresentation)(clientZkGroupCipher, presentation);
        if (!decryptedPresentation.uuid || !decryptedPresentation.profileKey) {
            throw new Error('decryptGroupChange: uuid or profileKey missing after promotePendingMember decryption!');
        }
        if (!(0, UUID_1.isValidUuid)(decryptedPresentation.uuid)) {
            log.warn(`decryptGroupChange/${logId}: Dropping modifyMemberProfileKey due to invalid userId`);
            return null;
        }
        if (!isValidProfileKey(decryptedPresentation.profileKey)) {
            throw new Error('decryptGroupChange: modifyMemberProfileKey had invalid profileKey');
        }
        return decryptedPresentation;
    }));
    // modifyTitle?: GroupChange.Actions.ModifyTitleAction;
    if (actions.modifyTitle) {
        const { title } = actions.modifyTitle;
        if (Bytes.isNotEmpty(title)) {
            try {
                result.modifyTitle = {
                    title: protobuf_1.SignalService.GroupAttributeBlob.decode((0, zkgroup_1.decryptGroupBlob)(clientZkGroupCipher, title)),
                };
            }
            catch (error) {
                log.warn(`decryptGroupChange/${logId}: Unable to decrypt modifyTitle.title`, error && error.stack ? error.stack : error);
            }
        }
        else {
            result.modifyTitle = {};
        }
    }
    // modifyAvatar?: GroupChange.Actions.ModifyAvatarAction;
    // Note: decryption happens during application of the change, on download of the avatar
    result.modifyAvatar = actions.modifyAvatar;
    // modifyDisappearingMessagesTimer?:
    // GroupChange.Actions.ModifyDisappearingMessagesTimerAction;
    if (actions.modifyDisappearingMessagesTimer) {
        const { timer } = actions.modifyDisappearingMessagesTimer;
        if (Bytes.isNotEmpty(timer)) {
            try {
                result.modifyDisappearingMessagesTimer = {
                    timer: protobuf_1.SignalService.GroupAttributeBlob.decode((0, zkgroup_1.decryptGroupBlob)(clientZkGroupCipher, timer)),
                };
            }
            catch (error) {
                log.warn(`decryptGroupChange/${logId}: Unable to decrypt modifyDisappearingMessagesTimer.timer`, error && error.stack ? error.stack : error);
            }
        }
        else {
            result.modifyDisappearingMessagesTimer = {};
        }
    }
    // modifyAttributesAccess?:
    // GroupChange.Actions.ModifyAttributesAccessControlAction;
    if (actions.modifyAttributesAccess) {
        const attributesAccess = (0, dropNull_1.dropNull)(actions.modifyAttributesAccess.attributesAccess);
        (0, assert_1.strictAssert)(isValidAccess(attributesAccess), `decryptGroupChange: modifyAttributesAccess.attributesAccess was not valid: ${actions.modifyAttributesAccess.attributesAccess}`);
        result.modifyAttributesAccess = {
            attributesAccess,
        };
    }
    // modifyMemberAccess?: GroupChange.Actions.ModifyMembersAccessControlAction;
    if (actions.modifyMemberAccess) {
        const membersAccess = (0, dropNull_1.dropNull)(actions.modifyMemberAccess.membersAccess);
        (0, assert_1.strictAssert)(isValidAccess(membersAccess), `decryptGroupChange: modifyMemberAccess.membersAccess was not valid: ${actions.modifyMemberAccess.membersAccess}`);
        result.modifyMemberAccess = {
            membersAccess,
        };
    }
    // modifyAddFromInviteLinkAccess?:
    //   GroupChange.Actions.ModifyAddFromInviteLinkAccessControlAction;
    if (actions.modifyAddFromInviteLinkAccess) {
        const addFromInviteLinkAccess = (0, dropNull_1.dropNull)(actions.modifyAddFromInviteLinkAccess.addFromInviteLinkAccess);
        (0, assert_1.strictAssert)(isValidLinkAccess(addFromInviteLinkAccess), `decryptGroupChange: modifyAddFromInviteLinkAccess.addFromInviteLinkAccess was not valid: ${actions.modifyAddFromInviteLinkAccess.addFromInviteLinkAccess}`);
        result.modifyAddFromInviteLinkAccess = {
            addFromInviteLinkAccess,
        };
    }
    // addMemberPendingAdminApprovals?: Array<
    //   GroupChange.Actions.AddMemberPendingAdminApprovalAction
    // >;
    result.addMemberPendingAdminApprovals = (0, lodash_1.compact)((actions.addMemberPendingAdminApprovals || []).map(addPendingAdminApproval => {
        const { added } = addPendingAdminApproval;
        (0, assert_1.strictAssert)(added, 'decryptGroupChange: addPendingAdminApproval was missing added field!');
        const decrypted = decryptMemberPendingAdminApproval(clientZkGroupCipher, added, logId);
        if (!decrypted) {
            log.warn(`decryptGroupChange/${logId}: Unable to decrypt addPendingAdminApproval.added. Dropping member.`);
            return null;
        }
        return { added: decrypted };
    }));
    // deleteMemberPendingAdminApprovals?: Array<
    //   GroupChange.Actions.DeleteMemberPendingAdminApprovalAction
    // >;
    result.deleteMemberPendingAdminApprovals = (0, lodash_1.compact)((actions.deleteMemberPendingAdminApprovals || []).map(deletePendingApproval => {
        const { deletedUserId } = deletePendingApproval;
        (0, assert_1.strictAssert)(Bytes.isNotEmpty(deletedUserId), 'decryptGroupChange: deletePendingApproval.deletedUserId was missing');
        let userId;
        try {
            userId = (0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, deletedUserId), 'actions.deleteMemberPendingAdminApprovals');
        }
        catch (error) {
            log.warn(`decryptGroupChange/${logId}: Unable to decrypt deletePendingApproval.deletedUserId. Dropping member.`, error && error.stack ? error.stack : error);
            return null;
        }
        if (!(0, UUID_1.isValidUuid)(userId)) {
            log.warn(`decryptGroupChange/${logId}: Dropping deletePendingApproval due to invalid deletedUserId`);
            return null;
        }
        return { deletedUserId: userId };
    }));
    // promoteMemberPendingAdminApprovals?: Array<
    //   GroupChange.Actions.PromoteMemberPendingAdminApprovalAction
    // >;
    result.promoteMemberPendingAdminApprovals = (0, lodash_1.compact)((actions.promoteMemberPendingAdminApprovals || []).map(promoteAdminApproval => {
        const { userId } = promoteAdminApproval;
        (0, assert_1.strictAssert)(Bytes.isNotEmpty(userId), 'decryptGroupChange: promoteAdminApproval.userId was missing');
        let decryptedUserId;
        try {
            decryptedUserId = (0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, userId), 'actions.promoteMemberPendingAdminApprovals.userId');
        }
        catch (error) {
            log.warn(`decryptGroupChange/${logId}: Unable to decrypt promoteAdminApproval.userId. Dropping member.`, error && error.stack ? error.stack : error);
            return null;
        }
        const role = (0, dropNull_1.dropNull)(promoteAdminApproval.role);
        if (!isValidRole(role)) {
            throw new Error(`decryptGroupChange: promoteAdminApproval had invalid role ${promoteAdminApproval.role}`);
        }
        return { role, userId: decryptedUserId };
    }));
    // modifyInviteLinkPassword?: GroupChange.Actions.ModifyInviteLinkPasswordAction;
    if (actions.modifyInviteLinkPassword) {
        const { inviteLinkPassword: password } = actions.modifyInviteLinkPassword;
        if (Bytes.isNotEmpty(password)) {
            result.modifyInviteLinkPassword = {
                inviteLinkPassword: Bytes.toBase64(password),
            };
        }
        else {
            result.modifyInviteLinkPassword = {};
        }
    }
    // modifyDescription?: GroupChange.Actions.ModifyDescriptionAction;
    if (actions.modifyDescription) {
        const { descriptionBytes } = actions.modifyDescription;
        if (Bytes.isNotEmpty(descriptionBytes)) {
            try {
                result.modifyDescription = {
                    descriptionBytes: protobuf_1.SignalService.GroupAttributeBlob.decode((0, zkgroup_1.decryptGroupBlob)(clientZkGroupCipher, descriptionBytes)),
                };
            }
            catch (error) {
                log.warn(`decryptGroupChange/${logId}: Unable to decrypt modifyDescription.descriptionBytes`, error && error.stack ? error.stack : error);
            }
        }
        else {
            result.modifyDescription = {};
        }
    }
    // modifyAnnouncementsOnly
    if (actions.modifyAnnouncementsOnly) {
        const { announcementsOnly } = actions.modifyAnnouncementsOnly;
        result.modifyAnnouncementsOnly = {
            announcementsOnly: Boolean(announcementsOnly),
        };
    }
    return result;
}
function decryptGroupTitle(title, secretParams) {
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(secretParams);
    if (!title || !title.length) {
        return undefined;
    }
    const blob = protobuf_1.SignalService.GroupAttributeBlob.decode((0, zkgroup_1.decryptGroupBlob)(clientZkGroupCipher, title));
    if (blob && blob.content === 'title') {
        return blob.title;
    }
    return undefined;
}
exports.decryptGroupTitle = decryptGroupTitle;
function decryptGroupDescription(description, secretParams) {
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(secretParams);
    if (!description || !description.length) {
        return undefined;
    }
    const blob = protobuf_1.SignalService.GroupAttributeBlob.decode((0, zkgroup_1.decryptGroupBlob)(clientZkGroupCipher, description));
    if (blob && blob.content === 'descriptionText') {
        return blob.descriptionText;
    }
    return undefined;
}
exports.decryptGroupDescription = decryptGroupDescription;
function decryptGroupState(groupState, groupSecretParams, logId) {
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(groupSecretParams);
    const result = {};
    // title
    if (Bytes.isNotEmpty(groupState.title)) {
        try {
            result.title = protobuf_1.SignalService.GroupAttributeBlob.decode((0, zkgroup_1.decryptGroupBlob)(clientZkGroupCipher, groupState.title));
        }
        catch (error) {
            log.warn(`decryptGroupState/${logId}: Unable to decrypt title. Clearing it.`, error && error.stack ? error.stack : error);
        }
    }
    // avatar
    // Note: decryption happens during application of the change, on download of the avatar
    // disappearing message timer
    if (groupState.disappearingMessagesTimer &&
        groupState.disappearingMessagesTimer.length) {
        try {
            result.disappearingMessagesTimer = protobuf_1.SignalService.GroupAttributeBlob.decode((0, zkgroup_1.decryptGroupBlob)(clientZkGroupCipher, groupState.disappearingMessagesTimer));
        }
        catch (error) {
            log.warn(`decryptGroupState/${logId}: Unable to decrypt disappearing message timer. Clearing it.`, error && error.stack ? error.stack : error);
        }
    }
    // accessControl
    {
        const { accessControl } = groupState;
        (0, assert_1.strictAssert)(accessControl, 'No accessControl field found');
        const attributes = (0, dropNull_1.dropNull)(accessControl.attributes);
        const members = (0, dropNull_1.dropNull)(accessControl.members);
        const addFromInviteLink = (0, dropNull_1.dropNull)(accessControl.addFromInviteLink);
        (0, assert_1.strictAssert)(isValidAccess(attributes), `decryptGroupState: Access control for attributes is invalid: ${attributes}`);
        (0, assert_1.strictAssert)(isValidAccess(members), `decryptGroupState: Access control for members is invalid: ${members}`);
        (0, assert_1.strictAssert)(isValidLinkAccess(addFromInviteLink), `decryptGroupState: Access control for invite link is invalid: ${addFromInviteLink}`);
        result.accessControl = {
            attributes,
            members,
            addFromInviteLink,
        };
    }
    // version
    (0, assert_1.strictAssert)((0, lodash_1.isNumber)(groupState.version), `decryptGroupState: Expected version to be a number; it was ${groupState.version}`);
    result.version = groupState.version;
    // members
    if (groupState.members) {
        result.members = (0, lodash_1.compact)(groupState.members.map((member) => decryptMember(clientZkGroupCipher, member, logId)));
    }
    // membersPendingProfileKey
    if (groupState.membersPendingProfileKey) {
        result.membersPendingProfileKey = (0, lodash_1.compact)(groupState.membersPendingProfileKey.map((member) => decryptMemberPendingProfileKey(clientZkGroupCipher, member, logId)));
    }
    // membersPendingAdminApproval
    if (groupState.membersPendingAdminApproval) {
        result.membersPendingAdminApproval = (0, lodash_1.compact)(groupState.membersPendingAdminApproval.map((member) => decryptMemberPendingAdminApproval(clientZkGroupCipher, member, logId)));
    }
    // inviteLinkPassword
    if (Bytes.isNotEmpty(groupState.inviteLinkPassword)) {
        result.inviteLinkPassword = Bytes.toBase64(groupState.inviteLinkPassword);
    }
    // descriptionBytes
    if (Bytes.isNotEmpty(groupState.descriptionBytes)) {
        try {
            result.descriptionBytes = protobuf_1.SignalService.GroupAttributeBlob.decode((0, zkgroup_1.decryptGroupBlob)(clientZkGroupCipher, groupState.descriptionBytes));
        }
        catch (error) {
            log.warn(`decryptGroupState/${logId}: Unable to decrypt descriptionBytes. Clearing it.`, error && error.stack ? error.stack : error);
        }
    }
    // announcementsOnly
    const { announcementsOnly } = groupState;
    result.announcementsOnly = Boolean(announcementsOnly);
    result.avatar = (0, dropNull_1.dropNull)(groupState.avatar);
    return result;
}
function decryptMember(clientZkGroupCipher, member, logId) {
    // userId
    (0, assert_1.strictAssert)(Bytes.isNotEmpty(member.userId), 'decryptMember: Member had missing userId');
    let userId;
    try {
        userId = (0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, member.userId), 'decryptMember.userId');
    }
    catch (error) {
        log.warn(`decryptMember/${logId}: Unable to decrypt member userid. Dropping member.`, error && error.stack ? error.stack : error);
        return undefined;
    }
    if (!(0, UUID_1.isValidUuid)(userId)) {
        log.warn(`decryptMember/${logId}: Dropping member due to invalid userId`);
        return undefined;
    }
    // profileKey
    (0, assert_1.strictAssert)(Bytes.isNotEmpty(member.profileKey), 'decryptMember: Member had missing profileKey');
    const profileKey = (0, zkgroup_1.decryptProfileKey)(clientZkGroupCipher, member.profileKey, UUID_1.UUID.cast(userId));
    if (!isValidProfileKey(profileKey)) {
        throw new Error('decryptMember: Member had invalid profileKey');
    }
    // role
    const role = (0, dropNull_1.dropNull)(member.role);
    if (!isValidRole(role)) {
        throw new Error(`decryptMember: Member had invalid role ${member.role}`);
    }
    return {
        userId,
        profileKey,
        role,
        joinedAtVersion: (0, dropNull_1.dropNull)(member.joinedAtVersion),
    };
}
function decryptMemberPendingProfileKey(clientZkGroupCipher, member, logId) {
    // addedByUserId
    (0, assert_1.strictAssert)(Bytes.isNotEmpty(member.addedByUserId), 'decryptMemberPendingProfileKey: Member had missing addedByUserId');
    let addedByUserId;
    try {
        addedByUserId = (0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, member.addedByUserId), 'decryptMemberPendingProfileKey.addedByUserId');
    }
    catch (error) {
        log.warn(`decryptMemberPendingProfileKey/${logId}: Unable to decrypt pending member addedByUserId. Dropping member.`, error && error.stack ? error.stack : error);
        return undefined;
    }
    if (!(0, UUID_1.isValidUuid)(addedByUserId)) {
        log.warn(`decryptMemberPendingProfileKey/${logId}: Dropping pending member due to invalid addedByUserId`);
        return undefined;
    }
    // timestamp
    const timestamp = normalizeTimestamp(member.timestamp);
    if (!member.member) {
        log.warn(`decryptMemberPendingProfileKey/${logId}: Dropping pending member due to missing member details`);
        return undefined;
    }
    const { userId, profileKey } = member.member;
    // userId
    (0, assert_1.strictAssert)(Bytes.isNotEmpty(userId), 'decryptMemberPendingProfileKey: Member had missing member.userId');
    let decryptedUserId;
    try {
        decryptedUserId = (0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, userId), 'decryptMemberPendingProfileKey.member.userId');
    }
    catch (error) {
        log.warn(`decryptMemberPendingProfileKey/${logId}: Unable to decrypt pending member userId. Dropping member.`, error && error.stack ? error.stack : error);
        return undefined;
    }
    if (!(0, UUID_1.isValidUuid)(decryptedUserId)) {
        log.warn(`decryptMemberPendingProfileKey/${logId}: Dropping pending member due to invalid member.userId`);
        return undefined;
    }
    // profileKey
    let decryptedProfileKey;
    if (Bytes.isNotEmpty(profileKey)) {
        try {
            decryptedProfileKey = (0, zkgroup_1.decryptProfileKey)(clientZkGroupCipher, profileKey, UUID_1.UUID.cast(decryptedUserId));
        }
        catch (error) {
            log.warn(`decryptMemberPendingProfileKey/${logId}: Unable to decrypt pending member profileKey. Dropping profileKey.`, error && error.stack ? error.stack : error);
        }
        if (!isValidProfileKey(decryptedProfileKey)) {
            log.warn(`decryptMemberPendingProfileKey/${logId}: Dropping profileKey, since it was invalid`);
            decryptedProfileKey = undefined;
        }
    }
    // role
    const role = (0, dropNull_1.dropNull)(member.member.role);
    (0, assert_1.strictAssert)(isValidRole(role), `decryptMemberPendingProfileKey: Member had invalid role ${role}`);
    return {
        addedByUserId,
        timestamp,
        member: {
            userId: decryptedUserId,
            profileKey: decryptedProfileKey,
            role,
        },
    };
}
function decryptMemberPendingAdminApproval(clientZkGroupCipher, member, logId) {
    // timestamp
    const timestamp = normalizeTimestamp(member.timestamp);
    const { userId, profileKey } = member;
    // userId
    (0, assert_1.strictAssert)(Bytes.isNotEmpty(userId), 'decryptMemberPendingAdminApproval: Missing userId');
    let decryptedUserId;
    try {
        decryptedUserId = (0, normalizeUuid_1.normalizeUuid)((0, zkgroup_1.decryptUuid)(clientZkGroupCipher, userId), 'decryptMemberPendingAdminApproval.userId');
    }
    catch (error) {
        log.warn(`decryptMemberPendingAdminApproval/${logId}: Unable to decrypt pending member userId. Dropping member.`, error && error.stack ? error.stack : error);
        return undefined;
    }
    if (!(0, UUID_1.isValidUuid)(decryptedUserId)) {
        log.warn(`decryptMemberPendingAdminApproval/${logId}: Invalid userId. Dropping member.`);
        return undefined;
    }
    // profileKey
    let decryptedProfileKey;
    if (Bytes.isNotEmpty(profileKey)) {
        try {
            decryptedProfileKey = (0, zkgroup_1.decryptProfileKey)(clientZkGroupCipher, profileKey, UUID_1.UUID.cast(decryptedUserId));
        }
        catch (error) {
            log.warn(`decryptMemberPendingAdminApproval/${logId}: Unable to decrypt profileKey. Dropping profileKey.`, error && error.stack ? error.stack : error);
        }
        if (!isValidProfileKey(decryptedProfileKey)) {
            log.warn(`decryptMemberPendingAdminApproval/${logId}: Dropping profileKey, since it was invalid`);
            decryptedProfileKey = undefined;
        }
    }
    return {
        timestamp,
        userId: decryptedUserId,
        profileKey: decryptedProfileKey,
    };
}
function getMembershipList(conversationId) {
    const conversation = window.ConversationController.get(conversationId);
    if (!conversation) {
        throw new Error('getMembershipList: cannot find conversation');
    }
    const secretParams = conversation.get('secretParams');
    if (!secretParams) {
        throw new Error('getMembershipList: no secretParams');
    }
    const clientZkGroupCipher = (0, zkgroup_1.getClientZkGroupCipher)(secretParams);
    return conversation.getMembers().map(member => {
        const uuid = member.get('uuid');
        if (!uuid) {
            throw new Error('getMembershipList: member has no UUID');
        }
        const uuidCiphertext = (0, zkgroup_1.encryptUuid)(clientZkGroupCipher, uuid);
        return { uuid, uuidCiphertext };
    });
}
exports.getMembershipList = getMembershipList;
