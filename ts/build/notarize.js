"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const fs_1 = require("fs");
const pify_1 = __importDefault(require("pify"));
const electron_notarize_1 = require("electron-notarize");
const packageJson = __importStar(require("../../package.json"));
const readdir = (0, pify_1.default)(fs_1.readdir);
/* eslint-disable no-console */
go().catch(error => {
    console.error(error.stack);
    process.exit(1);
});
async function go() {
    if (process.platform !== 'darwin') {
        console.log('notarize: Skipping, not on macOS');
        return;
    }
    const appPath = await findDMG();
    const appBundleId = packageJson.build.appId;
    if (!appBundleId) {
        throw new Error('appBundleId must be provided in package.json: build.appId');
    }
    const appleId = process.env.APPLE_USERNAME;
    if (!appleId) {
        throw new Error('appleId must be provided in environment variable APPLE_USERNAME');
    }
    const appleIdPassword = process.env.APPLE_PASSWORD;
    if (!appleIdPassword) {
        throw new Error('appleIdPassword must be provided in environment variable APPLE_PASSWORD');
    }
    console.log('Notarizing with...');
    console.log(`  file: ${appPath}`);
    console.log(`  primaryBundleId: ${appBundleId}`);
    console.log(`  username: ${appleId}`);
    await (0, electron_notarize_1.notarize)({
        appBundleId,
        appPath,
        appleId,
        appleIdPassword,
    });
}
const IS_DMG = /\.dmg$/;
async function findDMG() {
    const releaseDir = (0, path_1.resolve)('release');
    const files = await readdir(releaseDir);
    const max = files.length;
    for (let i = 0; i < max; i += 1) {
        const file = files[i];
        const fullPath = (0, path_1.join)(releaseDir, file);
        if (IS_DMG.test(file)) {
            return fullPath;
        }
    }
    throw new Error("No suitable file found in 'release' folder!");
}
