"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const environment_1 = require("../environment");
describe('environment utilities', () => {
    describe('parseEnvironment', () => {
        it('returns Environment.Production for non-strings', () => {
            chai_1.assert.equal((0, environment_1.parseEnvironment)(undefined), environment_1.Environment.Production);
            chai_1.assert.equal((0, environment_1.parseEnvironment)(0), environment_1.Environment.Production);
        });
        it('returns Environment.Production for invalid strings', () => {
            chai_1.assert.equal((0, environment_1.parseEnvironment)(''), environment_1.Environment.Production);
            chai_1.assert.equal((0, environment_1.parseEnvironment)(' development '), environment_1.Environment.Production);
            chai_1.assert.equal((0, environment_1.parseEnvironment)('PRODUCTION'), environment_1.Environment.Production);
        });
        it('parses "development" as Environment.Development', () => {
            chai_1.assert.equal((0, environment_1.parseEnvironment)('development'), environment_1.Environment.Development);
        });
        it('parses "production" as Environment.Production', () => {
            chai_1.assert.equal((0, environment_1.parseEnvironment)('production'), environment_1.Environment.Production);
        });
        it('parses "staging" as Environment.Staging', () => {
            chai_1.assert.equal((0, environment_1.parseEnvironment)('staging'), environment_1.Environment.Staging);
        });
        it('parses "test" as Environment.Test', () => {
            chai_1.assert.equal((0, environment_1.parseEnvironment)('test'), environment_1.Environment.Test);
        });
        it('parses "test-lib" as Environment.TestLib', () => {
            chai_1.assert.equal((0, environment_1.parseEnvironment)('test-lib'), environment_1.Environment.TestLib);
        });
    });
    describe('isTestEnvironment', () => {
        it('returns false for non-test environments', () => {
            chai_1.assert.isFalse((0, environment_1.isTestEnvironment)(environment_1.Environment.Development));
            chai_1.assert.isFalse((0, environment_1.isTestEnvironment)(environment_1.Environment.Production));
            chai_1.assert.isFalse((0, environment_1.isTestEnvironment)(environment_1.Environment.Staging));
        });
        it('returns true for test environments', () => {
            chai_1.assert.isTrue((0, environment_1.isTestEnvironment)(environment_1.Environment.Test));
            chai_1.assert.isTrue((0, environment_1.isTestEnvironment)(environment_1.Environment.TestLib));
        });
    });
});
