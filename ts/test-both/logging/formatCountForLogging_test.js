"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const formatCountForLogging_1 = require("../../logging/formatCountForLogging");
describe('formatCountForLogging', () => {
    it('returns "0" if passed zero', () => {
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(0), '0');
    });
    it('returns "NaN" if passed NaN', () => {
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(0 / 0), 'NaN');
    });
    it('returns "at least X", where X is a power of 10, for other numbers', () => {
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(1), 'at least 1');
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(2), 'at least 1');
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(9), 'at least 1');
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(10), 'at least 10');
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(99), 'at least 10');
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(100), 'at least 100');
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(999), 'at least 100');
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(1000), 'at least 1000');
        chai_1.assert.strictEqual((0, formatCountForLogging_1.formatCountForLogging)(9999), 'at least 1000');
    });
});
