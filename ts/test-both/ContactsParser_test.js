"use strict";
// Copyright 2015-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const protobufjs_1 = require("protobufjs");
const Bytes = __importStar(require("../Bytes"));
const protobuf_1 = require("../protobuf");
const ContactsParser_1 = require("../textsecure/ContactsParser");
describe('ContactsParser', () => {
    function generateAvatar() {
        const result = new Uint8Array(255);
        for (let i = 0; i < result.length; i += 1) {
            result[i] = i;
        }
        return result;
    }
    describe('ContactBuffer', () => {
        function getTestBuffer() {
            const avatarBuffer = generateAvatar();
            const contactInfoBuffer = protobuf_1.SignalService.ContactDetails.encode({
                name: 'Zero Cool',
                number: '+10000000000',
                uuid: '7198E1BD-1293-452A-A098-F982FF201902',
                avatar: { contentType: 'image/jpeg', length: avatarBuffer.length },
            }).finish();
            const writer = new protobufjs_1.Writer();
            writer.bytes(contactInfoBuffer);
            const prefixedContact = writer.finish();
            const chunks = [];
            for (let i = 0; i < 3; i += 1) {
                chunks.push(prefixedContact);
                chunks.push(avatarBuffer);
            }
            return Bytes.concatenate(chunks);
        }
        it('parses an array buffer of contacts', () => {
            var _a, _b, _c, _d;
            const bytes = getTestBuffer();
            const contactBuffer = new ContactsParser_1.ContactBuffer(bytes);
            let contact = contactBuffer.next();
            let count = 0;
            while (contact !== undefined) {
                count += 1;
                chai_1.assert.strictEqual(contact.name, 'Zero Cool');
                chai_1.assert.strictEqual(contact.number, '+10000000000');
                chai_1.assert.strictEqual(contact.uuid, '7198e1bd-1293-452a-a098-f982ff201902');
                chai_1.assert.strictEqual((_a = contact.avatar) === null || _a === void 0 ? void 0 : _a.contentType, 'image/jpeg');
                chai_1.assert.strictEqual((_b = contact.avatar) === null || _b === void 0 ? void 0 : _b.length, 255);
                chai_1.assert.strictEqual((_c = contact.avatar) === null || _c === void 0 ? void 0 : _c.data.byteLength, 255);
                const avatarBytes = new Uint8Array(((_d = contact.avatar) === null || _d === void 0 ? void 0 : _d.data) || new Uint8Array(0));
                for (let j = 0; j < 255; j += 1) {
                    chai_1.assert.strictEqual(avatarBytes[j], j);
                }
                contact = contactBuffer.next();
            }
            chai_1.assert.strictEqual(count, 3);
        });
    });
    describe('GroupBuffer', () => {
        function getTestBuffer() {
            const avatarBuffer = generateAvatar();
            const groupInfoBuffer = protobuf_1.SignalService.GroupDetails.encode({
                id: new Uint8Array([1, 3, 3, 7]),
                name: 'Hackers',
                membersE164: ['cereal', 'burn', 'phreak', 'joey'],
                avatar: { contentType: 'image/jpeg', length: avatarBuffer.length },
            }).finish();
            const writer = new protobufjs_1.Writer();
            writer.bytes(groupInfoBuffer);
            const prefixedGroup = writer.finish();
            const chunks = [];
            for (let i = 0; i < 3; i += 1) {
                chunks.push(prefixedGroup);
                chunks.push(avatarBuffer);
            }
            return Bytes.concatenate(chunks);
        }
        it('parses an array buffer of groups', () => {
            var _a, _b, _c, _d;
            const bytes = getTestBuffer();
            const groupBuffer = new ContactsParser_1.GroupBuffer(bytes);
            let group = groupBuffer.next();
            let count = 0;
            while (group !== undefined) {
                count += 1;
                chai_1.assert.strictEqual(group.name, 'Hackers');
                chai_1.assert.deepEqual(group.id, new Uint8Array([1, 3, 3, 7]));
                chai_1.assert.sameMembers(group.membersE164, [
                    'cereal',
                    'burn',
                    'phreak',
                    'joey',
                ]);
                chai_1.assert.strictEqual((_a = group.avatar) === null || _a === void 0 ? void 0 : _a.contentType, 'image/jpeg');
                chai_1.assert.strictEqual((_b = group.avatar) === null || _b === void 0 ? void 0 : _b.length, 255);
                chai_1.assert.strictEqual((_c = group.avatar) === null || _c === void 0 ? void 0 : _c.data.byteLength, 255);
                const avatarBytes = new Uint8Array(((_d = group.avatar) === null || _d === void 0 ? void 0 : _d.data) || new Uint8Array(0));
                for (let j = 0; j < 255; j += 1) {
                    chai_1.assert.strictEqual(avatarBytes[j], j);
                }
                group = groupBuffer.next();
            }
            chai_1.assert.strictEqual(count, 3);
        });
    });
});
