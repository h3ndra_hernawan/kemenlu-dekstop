"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const v4_1 = __importDefault(require("uuid/v4"));
const processSyncMessage_1 = require("../textsecure/processSyncMessage");
describe('processSyncMessage', () => {
    it('should normalize UUIDs in sent', () => {
        const destinationUuid = (0, v4_1.default)();
        const out = (0, processSyncMessage_1.processSyncMessage)({
            sent: {
                destinationUuid: destinationUuid.toUpperCase(),
                unidentifiedStatus: [
                    {
                        destinationUuid: destinationUuid.toUpperCase(),
                    },
                ],
            },
        });
        chai_1.assert.deepStrictEqual(out, {
            sent: {
                destinationUuid,
                unidentifiedStatus: [
                    {
                        destinationUuid,
                    },
                ],
            },
        });
    });
});
