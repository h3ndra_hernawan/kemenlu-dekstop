"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const lodash_1 = require("lodash");
const sleep_1 = require("../../util/sleep");
const Crypto_1 = require("../../Crypto");
const ourProfileKey_1 = require("../../services/ourProfileKey");
describe('"our profile key" service', () => {
    const createFakeStorage = () => ({
        get: sinon.stub(),
        put: sinon.stub().resolves(),
        remove: sinon.stub().resolves(),
        onready: sinon.stub().callsArg(0),
    });
    describe('get', () => {
        it("fetches the key from storage if it's there", async () => {
            const fakeProfileKey = new Uint8Array(2);
            const fakeStorage = createFakeStorage();
            fakeStorage.get.withArgs('profileKey').returns(fakeProfileKey);
            const service = new ourProfileKey_1.OurProfileKeyService();
            service.initialize(fakeStorage);
            const profileKey = await service.get();
            chai_1.assert.isTrue(profileKey && (0, Crypto_1.constantTimeEqual)(profileKey, fakeProfileKey));
        });
        it('resolves with undefined if the key is not in storage', async () => {
            const service = new ourProfileKey_1.OurProfileKeyService();
            service.initialize(createFakeStorage());
            chai_1.assert.isUndefined(await service.get());
        });
        it("doesn't grab the profile key from storage until storage is ready", async () => {
            let onReadyCallback = lodash_1.noop;
            const fakeStorage = Object.assign(Object.assign({}, createFakeStorage()), { get: sinon.stub().returns(new Uint8Array(2)), onready: sinon.stub().callsFake(callback => {
                    onReadyCallback = callback;
                }) });
            const service = new ourProfileKey_1.OurProfileKeyService();
            service.initialize(fakeStorage);
            const getPromise = service.get();
            // We want to make sure this isn't called even after a tick of the event loop.
            await (0, sleep_1.sleep)(1);
            sinon.assert.notCalled(fakeStorage.get);
            onReadyCallback();
            await getPromise;
            sinon.assert.calledOnce(fakeStorage.get);
        });
        it("doesn't grab the profile key until all blocking promises are ready", async () => {
            const fakeStorage = createFakeStorage();
            const service = new ourProfileKey_1.OurProfileKeyService();
            service.initialize(fakeStorage);
            let resolve1 = lodash_1.noop;
            service.blockGetWithPromise(new Promise(resolve => {
                resolve1 = resolve;
            }));
            let reject2 = lodash_1.noop;
            service.blockGetWithPromise(new Promise((_resolve, reject) => {
                reject2 = reject;
            }));
            let reject3 = lodash_1.noop;
            service.blockGetWithPromise(new Promise((_resolve, reject) => {
                reject3 = reject;
            }));
            const getPromise = service.get();
            resolve1();
            await (0, sleep_1.sleep)(1);
            sinon.assert.notCalled(fakeStorage.get);
            reject2(new Error('uh oh'));
            await (0, sleep_1.sleep)(1);
            sinon.assert.notCalled(fakeStorage.get);
            reject3(new Error('oh no'));
            await getPromise;
            sinon.assert.calledOnce(fakeStorage.get);
        });
        it("if there are blocking promises, doesn't grab the profile key from storage more than once (in other words, subsequent calls piggyback)", async () => {
            const fakeStorage = createFakeStorage();
            fakeStorage.get.returns(new Uint8Array(2));
            const service = new ourProfileKey_1.OurProfileKeyService();
            service.initialize(fakeStorage);
            let resolve = lodash_1.noop;
            service.blockGetWithPromise(new Promise(innerResolve => {
                resolve = innerResolve;
            }));
            const getPromises = [service.get(), service.get(), service.get()];
            resolve();
            const results = await Promise.all(getPromises);
            (0, chai_1.assert)(new Set(results).size === 1, 'All results should be the same');
            sinon.assert.calledOnce(fakeStorage.get);
        });
        it('removes all of the blocking promises after waiting for them once', async () => {
            const fakeStorage = createFakeStorage();
            const service = new ourProfileKey_1.OurProfileKeyService();
            service.initialize(fakeStorage);
            let resolve = lodash_1.noop;
            service.blockGetWithPromise(new Promise(innerResolve => {
                resolve = innerResolve;
            }));
            const getPromise = service.get();
            sinon.assert.notCalled(fakeStorage.get);
            resolve();
            await getPromise;
            sinon.assert.calledOnce(fakeStorage.get);
            await service.get();
            sinon.assert.calledTwice(fakeStorage.get);
        });
    });
    describe('set', () => {
        it('updates the key in storage', async () => {
            const fakeProfileKey = new Uint8Array(2);
            const fakeStorage = createFakeStorage();
            const service = new ourProfileKey_1.OurProfileKeyService();
            service.initialize(fakeStorage);
            await service.set(fakeProfileKey);
            sinon.assert.calledOnce(fakeStorage.put);
            sinon.assert.calledWith(fakeStorage.put, 'profileKey', fakeProfileKey);
        });
        it('clears the key in storage', async () => {
            const fakeStorage = createFakeStorage();
            const service = new ourProfileKey_1.OurProfileKeyService();
            service.initialize(fakeStorage);
            await service.set(undefined);
            sinon.assert.calledOnce(fakeStorage.remove);
            sinon.assert.calledWith(fakeStorage.remove, 'profileKey');
        });
    });
});
