"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const preferredReactionEmoji_1 = require("../../reactions/preferredReactionEmoji");
describe('preferred reaction emoji utilities', () => {
    describe('getPreferredReactionEmoji', () => {
        const defaultsForSkinTone2 = ['❤️', '👍🏼', '👎🏼', '😂', '😮', '😢'];
        it('returns the default set if passed a non-array', () => {
            [undefined, null, '❤️👍🏼👎🏼😂😮😢'].forEach(input => {
                chai_1.assert.deepStrictEqual((0, preferredReactionEmoji_1.getPreferredReactionEmoji)(input, 2), defaultsForSkinTone2);
            });
        });
        it('returns the default set if passed an empty array', () => {
            chai_1.assert.deepStrictEqual((0, preferredReactionEmoji_1.getPreferredReactionEmoji)([], 2), defaultsForSkinTone2);
        });
        it('falls back to defaults if passed an array that is too short', () => {
            const input = ['✨', '❇️'];
            const expected = ['✨', '❇️', '👎🏽', '😂', '😮', '😢'];
            chai_1.assert.deepStrictEqual((0, preferredReactionEmoji_1.getPreferredReactionEmoji)(input, 3), expected);
        });
        it('falls back to defaults when passed an array with some invalid values', () => {
            const input = ['✨', 'invalid', '🎇', '🦈', undefined, ''];
            const expected = ['✨', '👍🏼', '🎇', '🦈', '😮', '😢'];
            chai_1.assert.deepStrictEqual((0, preferredReactionEmoji_1.getPreferredReactionEmoji)(input, 2), expected);
        });
        it('returns a custom set if passed a valid value', () => {
            const input = ['✨', '❇️', '🎇', '🦈', '💖', '🅿️'];
            chai_1.assert.deepStrictEqual((0, preferredReactionEmoji_1.getPreferredReactionEmoji)(input, 3), input);
        });
        it('only returns the first few emoji if passed a value that is too long', () => {
            const expected = ['✨', '❇️', '🎇', '🦈', '💖', '🅿️'];
            const input = [...expected, '💅', '💅', '💅', '💅'];
            chai_1.assert.deepStrictEqual((0, preferredReactionEmoji_1.getPreferredReactionEmoji)(input, 3), expected);
        });
    });
    describe('canBeSynced', () => {
        it('returns false for non-arrays', () => {
            chai_1.assert.isFalse((0, preferredReactionEmoji_1.canBeSynced)(undefined));
            chai_1.assert.isFalse((0, preferredReactionEmoji_1.canBeSynced)(null));
            chai_1.assert.isFalse((0, preferredReactionEmoji_1.canBeSynced)('❤️👍🏼👎🏼😂😮😢'));
        });
        it('returns false for arrays that are too long', () => {
            chai_1.assert.isFalse((0, preferredReactionEmoji_1.canBeSynced)(Array(21).fill('🦊')));
        });
        it('returns false for arrays that have items that are too long', () => {
            const input = ['✨', '❇️', 'x'.repeat(21), '🦈', '💖', '🅿️'];
            chai_1.assert.isFalse((0, preferredReactionEmoji_1.canBeSynced)(input));
        });
        it('returns true for valid values', () => {
            [
                [],
                ['💅'],
                ['✨', '❇️', '🎇', '🦈', '💖', '🅿️'],
                ['this', 'array', 'has', 'no', 'emoji', 'but', "that's", 'okay'],
            ].forEach(input => {
                chai_1.assert.isTrue((0, preferredReactionEmoji_1.canBeSynced)(input));
            });
        });
    });
});
