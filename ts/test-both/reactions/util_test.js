"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const uuid_1 = require("uuid");
const lodash_1 = require("lodash");
const iterables_1 = require("../../util/iterables");
const util_1 = require("../../reactions/util");
describe('reaction utilities', () => {
    const OUR_CONVO_ID = (0, uuid_1.v4)();
    const rxn = (emoji, { isPending = false } = {}) => (Object.assign({ emoji, fromId: OUR_CONVO_ID, targetAuthorUuid: (0, uuid_1.v4)(), targetTimestamp: Date.now(), timestamp: Date.now() }, (isPending ? { isSentByConversationId: { [(0, uuid_1.v4)()]: false } } : {})));
    describe('addOutgoingReaction', () => {
        it('adds the reaction to the end of an empty list', () => {
            const reaction = rxn('💅');
            const result = (0, util_1.addOutgoingReaction)([], reaction);
            chai_1.assert.deepStrictEqual(result, [reaction]);
        });
        it('removes all pending reactions', () => {
            const oldReactions = [
                Object.assign(Object.assign({}, rxn('😭', { isPending: true })), { timestamp: 3 }),
                Object.assign(Object.assign({}, rxn('💬')), { fromId: (0, uuid_1.v4)() }),
                Object.assign(Object.assign({}, rxn('🥀', { isPending: true })), { timestamp: 1 }),
                Object.assign(Object.assign({}, rxn('🌹', { isPending: true })), { timestamp: 2 }),
            ];
            const reaction = rxn('😀');
            const newReactions = (0, util_1.addOutgoingReaction)(oldReactions, reaction);
            chai_1.assert.deepStrictEqual(newReactions, [oldReactions[1], reaction]);
        });
    });
    describe('getNewestPendingOutgoingReaction', () => {
        it('returns undefined if there are no pending outgoing reactions', () => {
            [[], [rxn('🔔')], [rxn('😭'), Object.assign(Object.assign({}, rxn('💬')), { fromId: (0, uuid_1.v4)() })]].forEach(oldReactions => {
                chai_1.assert.deepStrictEqual((0, util_1.getNewestPendingOutgoingReaction)(oldReactions, OUR_CONVO_ID), {});
            });
        });
        it("returns undefined if there's a pending reaction before a fully sent one", () => {
            const oldReactions = [
                Object.assign(Object.assign({}, rxn('⭐️')), { timestamp: 2 }),
                Object.assign(Object.assign({}, rxn('🔥', { isPending: true })), { timestamp: 1 }),
            ];
            const { pendingReaction, emojiToRemove } = (0, util_1.getNewestPendingOutgoingReaction)(oldReactions, OUR_CONVO_ID);
            chai_1.assert.isUndefined(pendingReaction);
            chai_1.assert.isUndefined(emojiToRemove);
        });
        it('returns the newest pending reaction', () => {
            [
                [rxn('⭐️', { isPending: true })],
                [
                    Object.assign(Object.assign({}, rxn('🥀', { isPending: true })), { timestamp: 1 }),
                    Object.assign(Object.assign({}, rxn('⭐️', { isPending: true })), { timestamp: 2 }),
                ],
            ].forEach(oldReactions => {
                const { pendingReaction, emojiToRemove } = (0, util_1.getNewestPendingOutgoingReaction)(oldReactions, OUR_CONVO_ID);
                chai_1.assert.strictEqual(pendingReaction === null || pendingReaction === void 0 ? void 0 : pendingReaction.emoji, '⭐️');
                chai_1.assert.isUndefined(emojiToRemove);
            });
        });
        it('makes its best guess of an emoji to remove, if applicable', () => {
            const oldReactions = [
                Object.assign(Object.assign({}, rxn('⭐️')), { timestamp: 1 }),
                Object.assign(Object.assign({}, rxn(undefined, { isPending: true })), { timestamp: 3 }),
                Object.assign(Object.assign({}, rxn('🔥', { isPending: true })), { timestamp: 2 }),
            ];
            const { pendingReaction, emojiToRemove } = (0, util_1.getNewestPendingOutgoingReaction)(oldReactions, OUR_CONVO_ID);
            chai_1.assert.isDefined(pendingReaction);
            chai_1.assert.isUndefined(pendingReaction === null || pendingReaction === void 0 ? void 0 : pendingReaction.emoji);
            chai_1.assert.strictEqual(emojiToRemove, '⭐️');
        });
    });
    describe('getUnsentConversationIds', () => {
        it("returns an empty iterable if there's nothing to send", () => {
            (0, chai_1.assert)((0, iterables_1.isEmpty)((0, util_1.getUnsentConversationIds)({})));
            (0, chai_1.assert)((0, iterables_1.isEmpty)((0, util_1.getUnsentConversationIds)({
                isSentByConversationId: { [(0, uuid_1.v4)()]: true },
            })));
        });
        it('returns an iterable of all unsent conversation IDs', () => {
            const unsent1 = (0, uuid_1.v4)();
            const unsent2 = (0, uuid_1.v4)();
            const fakeReaction = {
                isSentByConversationId: {
                    [unsent1]: false,
                    [unsent2]: false,
                    [(0, uuid_1.v4)()]: true,
                    [(0, uuid_1.v4)()]: true,
                },
            };
            chai_1.assert.sameMembers([...(0, util_1.getUnsentConversationIds)(fakeReaction)], [unsent1, unsent2]);
        });
    });
    describe('markReactionFailed', () => {
        const fullySent = rxn('⭐️');
        const partiallySent = Object.assign(Object.assign({}, rxn('🔥')), { isSentByConversationId: { [(0, uuid_1.v4)()]: true, [(0, uuid_1.v4)()]: false } });
        const unsent = rxn('🤫', { isPending: true });
        const reactions = [fullySent, partiallySent, unsent];
        it('removes the pending state if the reaction, with emoji, was partially sent', () => {
            chai_1.assert.deepStrictEqual((0, util_1.markOutgoingReactionFailed)(reactions, partiallySent), [fullySent, (0, lodash_1.omit)(partiallySent, 'isSentByConversationId'), unsent]);
        });
        it('removes the removal reaction', () => {
            const none = rxn(undefined, { isPending: true });
            chai_1.assert.isEmpty((0, util_1.markOutgoingReactionFailed)([none], none));
        });
        it('does nothing if the reaction is not in the list', () => {
            chai_1.assert.deepStrictEqual((0, util_1.markOutgoingReactionFailed)(reactions, rxn('🥀', { isPending: true })), reactions);
        });
        it('removes the completely-unsent emoji reaction', () => {
            chai_1.assert.deepStrictEqual((0, util_1.markOutgoingReactionFailed)(reactions, unsent), [
                fullySent,
                partiallySent,
            ]);
        });
    });
    describe('markOutgoingReactionSent', () => {
        const uuid1 = (0, uuid_1.v4)();
        const uuid2 = (0, uuid_1.v4)();
        const uuid3 = (0, uuid_1.v4)();
        const star = Object.assign(Object.assign({}, rxn('⭐️')), { timestamp: 2, isSentByConversationId: {
                [uuid1]: false,
                [uuid2]: false,
                [uuid3]: false,
            } });
        const none = Object.assign(Object.assign({}, rxn(undefined)), { timestamp: 3, isSentByConversationId: {
                [uuid1]: false,
                [uuid2]: false,
                [uuid3]: false,
            } });
        const reactions = [star, none, Object.assign(Object.assign({}, rxn('🔕')), { timestamp: 1 })];
        it("does nothing if the reaction isn't in the list", () => {
            const result = (0, util_1.markOutgoingReactionSent)(reactions, rxn('🥀', { isPending: true }), [(0, uuid_1.v4)()]);
            chai_1.assert.deepStrictEqual(result, reactions);
        });
        it('updates reactions to be partially sent', () => {
            [star, none].forEach(reaction => {
                var _a;
                const result = (0, util_1.markOutgoingReactionSent)(reactions, reaction, [
                    uuid1,
                    uuid2,
                ]);
                chai_1.assert.deepStrictEqual((_a = result.find(re => re.emoji === reaction.emoji)) === null || _a === void 0 ? void 0 : _a.isSentByConversationId, {
                    [uuid1]: true,
                    [uuid2]: true,
                    [uuid3]: false,
                });
            });
        });
        it('removes sent state if a reaction with emoji is fully sent', () => {
            const result = (0, util_1.markOutgoingReactionSent)(reactions, star, [
                uuid1,
                uuid2,
                uuid3,
            ]);
            const newReaction = result.find(re => re.emoji === '⭐️');
            chai_1.assert.isDefined(newReaction);
            chai_1.assert.isUndefined(newReaction === null || newReaction === void 0 ? void 0 : newReaction.isSentByConversationId);
        });
        it('removes a fully-sent reaction removal', () => {
            const result = (0, util_1.markOutgoingReactionSent)(reactions, none, [
                uuid1,
                uuid2,
                uuid3,
            ]);
            (0, chai_1.assert)(result.every(({ emoji }) => typeof emoji === 'string'), 'Expected the emoji removal to be gone');
        });
        it('removes older reactions of mine', () => {
            const result = (0, util_1.markOutgoingReactionSent)(reactions, star, [
                uuid1,
                uuid2,
                uuid3,
            ]);
            chai_1.assert.isUndefined(result.find(re => re.emoji === '🔕'));
        });
    });
});
