"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isValidReactionEmoji_1 = require("../../reactions/isValidReactionEmoji");
describe('isValidReactionEmoji', () => {
    it('returns false for non-strings', () => {
        chai_1.assert.isFalse((0, isValidReactionEmoji_1.isValidReactionEmoji)(undefined));
        chai_1.assert.isFalse((0, isValidReactionEmoji_1.isValidReactionEmoji)(null));
        chai_1.assert.isFalse((0, isValidReactionEmoji_1.isValidReactionEmoji)(1));
    });
    it("returns false for strings that aren't a single emoji", () => {
        chai_1.assert.isFalse((0, isValidReactionEmoji_1.isValidReactionEmoji)(''));
        chai_1.assert.isFalse((0, isValidReactionEmoji_1.isValidReactionEmoji)('a'));
        chai_1.assert.isFalse((0, isValidReactionEmoji_1.isValidReactionEmoji)('abc'));
        chai_1.assert.isFalse((0, isValidReactionEmoji_1.isValidReactionEmoji)('💩💩'));
        chai_1.assert.isFalse((0, isValidReactionEmoji_1.isValidReactionEmoji)('🇸'));
        chai_1.assert.isFalse((0, isValidReactionEmoji_1.isValidReactionEmoji)('‍'));
    });
    it('returns true for strings that are exactly 1 emoji', () => {
        chai_1.assert.isTrue((0, isValidReactionEmoji_1.isValidReactionEmoji)('🇺🇸'));
        chai_1.assert.isTrue((0, isValidReactionEmoji_1.isValidReactionEmoji)('👍'));
        chai_1.assert.isTrue((0, isValidReactionEmoji_1.isValidReactionEmoji)('👍🏾'));
        chai_1.assert.isTrue((0, isValidReactionEmoji_1.isValidReactionEmoji)('👩‍❤️‍👩'));
    });
});
