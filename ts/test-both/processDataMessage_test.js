"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const long_1 = __importDefault(require("long"));
const processDataMessage_1 = require("../textsecure/processDataMessage");
const protobuf_1 = require("../protobuf");
const FLAGS = protobuf_1.SignalService.DataMessage.Flags;
const TIMESTAMP = Date.now();
const UNPROCESSED_ATTACHMENT = {
    cdnId: 123,
    key: new Uint8Array([1, 2, 3]),
    digest: new Uint8Array([4, 5, 6]),
};
const PROCESSED_ATTACHMENT = {
    cdnId: '123',
    key: 'AQID',
    digest: 'BAUG',
};
const GROUP_ID = new Uint8Array([0x68, 0x65, 0x79]);
const DERIVED_GROUPV2_ID = '7qQUi8Wa6Jm3Rl+l63saATGeciEqokbHpP+lV3F5t9o=';
describe('processDataMessage', () => {
    const check = (message) => (0, processDataMessage_1.processDataMessage)(Object.assign({ timestamp: TIMESTAMP }, message), TIMESTAMP);
    it('should process attachments', async () => {
        const out = await check({
            attachments: [UNPROCESSED_ATTACHMENT],
        });
        chai_1.assert.deepStrictEqual(out.attachments, [PROCESSED_ATTACHMENT]);
    });
    it('should process attachments with 0 cdnId', async () => {
        const out = await check({
            attachments: [
                Object.assign(Object.assign({}, UNPROCESSED_ATTACHMENT), { cdnId: new long_1.default(0) }),
            ],
        });
        chai_1.assert.deepStrictEqual(out.attachments, [
            Object.assign(Object.assign({}, PROCESSED_ATTACHMENT), { cdnId: undefined }),
        ]);
    });
    it('should throw on too many attachments', async () => {
        const attachments = [];
        for (let i = 0; i < processDataMessage_1.ATTACHMENT_MAX + 1; i += 1) {
            attachments.push(UNPROCESSED_ATTACHMENT);
        }
        await chai_1.assert.isRejected(check({ attachments }), `Too many attachments: ${processDataMessage_1.ATTACHMENT_MAX + 1} included in one message` +
            `, max is ${processDataMessage_1.ATTACHMENT_MAX}`);
    });
    it('should process group context UPDATE/QUIT message', async () => {
        const { UPDATE, QUIT } = protobuf_1.SignalService.GroupContext.Type;
        for (const type of [UPDATE, QUIT]) {
            // eslint-disable-next-line no-await-in-loop
            const out = await check({
                body: 'should be deleted',
                attachments: [UNPROCESSED_ATTACHMENT],
                group: {
                    id: GROUP_ID,
                    name: 'Group',
                    avatar: UNPROCESSED_ATTACHMENT,
                    type,
                    membersE164: ['+1'],
                },
            });
            chai_1.assert.isUndefined(out.body);
            chai_1.assert.strictEqual(out.attachments.length, 0);
            chai_1.assert.deepStrictEqual(out.group, {
                id: 'hey',
                name: 'Group',
                avatar: PROCESSED_ATTACHMENT,
                type,
                membersE164: ['+1'],
                derivedGroupV2Id: DERIVED_GROUPV2_ID,
            });
        }
    });
    it('should process group context DELIVER message', async () => {
        const out = await check({
            body: 'should not be deleted',
            attachments: [UNPROCESSED_ATTACHMENT],
            group: {
                id: GROUP_ID,
                name: 'should be deleted',
                membersE164: ['should be deleted'],
                avatar: {},
                type: protobuf_1.SignalService.GroupContext.Type.DELIVER,
            },
        });
        chai_1.assert.strictEqual(out.body, 'should not be deleted');
        chai_1.assert.strictEqual(out.attachments.length, 1);
        chai_1.assert.deepStrictEqual(out.group, {
            id: 'hey',
            type: protobuf_1.SignalService.GroupContext.Type.DELIVER,
            membersE164: [],
            derivedGroupV2Id: DERIVED_GROUPV2_ID,
            avatar: undefined,
            name: undefined,
        });
    });
    it('should process groupv2 context', async () => {
        const out = await check({
            groupV2: {
                masterKey: new Uint8Array(32),
                revision: 1,
                groupChange: new Uint8Array([4, 5, 6]),
            },
        });
        chai_1.assert.deepStrictEqual(out.groupV2, {
            masterKey: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=',
            revision: 1,
            groupChange: 'BAUG',
            id: 'd/rq8//fR4RzhvN3G9KcKlQoj7cguQFjTOqLV6JUSbo=',
            secretParams: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAd/rq8//fR' +
                '4RzhvN3G9KcKlQoj7cguQFjTOqLV6JUSbrURzeILsUmsymGJmHt3kpBJ2zosqp4ex' +
                'sg+qwF1z6YdB/rxKnxKRLZZP/V0F7bERslYILy2lUh3Sh3iA98yO4CGfzjjFVo1SI' +
                '7U8XApLeVNQHJo7nkflf/JyBrqPft5gEucbKW/h+S3OYjfQ5zl2Cpw3XrV7N6OKEu' +
                'tLUWPHQuJx11A4xDPrmtAOnGy2NBxoOybDNlWipeNbn1WQJqOjMF7YA80oEm+5qnM' +
                'kEYcFVqbYaSzPcMhg3mQ0SYfQpxYgSOJpwp9f/8EDnwJV4ISPBOo2CiaSqVfnd8Dw' +
                'ZOc58gQA==',
            publicParams: 'AHf66vP/30eEc4bzdxvSnCpUKI+3ILkBY0zqi1eiVEm6LnGylv4fk' +
                'tzmI30Oc5dgqcN161ezejihLrS1Fjx0LieOJpwp9f/8EDnwJV4ISPBOo2CiaSqVfn' +
                'd8DwZOc58gQA==',
        });
    });
    it('should base64 profileKey', async () => {
        const out = await check({
            profileKey: new Uint8Array([42, 23, 55]),
        });
        chai_1.assert.strictEqual(out.profileKey, 'Khc3');
    });
    it('should process quote', async () => {
        const out = await check({
            quote: {
                id: 1,
                authorUuid: 'author',
                text: 'text',
                attachments: [
                    {
                        contentType: 'image/jpeg',
                        fileName: 'image.jpg',
                        thumbnail: UNPROCESSED_ATTACHMENT,
                    },
                ],
            },
        });
        chai_1.assert.deepStrictEqual(out.quote, {
            id: 1,
            authorUuid: 'author',
            text: 'text',
            attachments: [
                {
                    contentType: 'image/jpeg',
                    fileName: 'image.jpg',
                    thumbnail: PROCESSED_ATTACHMENT,
                },
            ],
            bodyRanges: [],
        });
    });
    it('should process contact', async () => {
        const out = await check({
            contact: [
                {
                    avatar: {
                        avatar: UNPROCESSED_ATTACHMENT,
                    },
                },
                {
                    avatar: {
                        avatar: UNPROCESSED_ATTACHMENT,
                        isProfile: true,
                    },
                },
            ],
        });
        chai_1.assert.deepStrictEqual(out.contact, [
            {
                avatar: { avatar: PROCESSED_ATTACHMENT, isProfile: false },
            },
            {
                avatar: { avatar: PROCESSED_ATTACHMENT, isProfile: true },
            },
        ]);
    });
    it('should process reaction', async () => {
        chai_1.assert.deepStrictEqual((await check({
            reaction: {
                emoji: '😎',
                targetTimestamp: TIMESTAMP,
            },
        })).reaction, {
            emoji: '😎',
            remove: false,
            targetAuthorUuid: undefined,
            targetTimestamp: TIMESTAMP,
        });
        chai_1.assert.deepStrictEqual((await check({
            reaction: {
                emoji: '😎',
                remove: true,
                targetTimestamp: TIMESTAMP,
            },
        })).reaction, {
            emoji: '😎',
            remove: true,
            targetAuthorUuid: undefined,
            targetTimestamp: TIMESTAMP,
        });
    });
    it('should process preview', async () => {
        const out = await check({
            preview: [
                {
                    date: TIMESTAMP,
                    image: UNPROCESSED_ATTACHMENT,
                },
            ],
        });
        chai_1.assert.deepStrictEqual(out.preview, [
            {
                date: TIMESTAMP,
                description: undefined,
                title: undefined,
                url: undefined,
                image: PROCESSED_ATTACHMENT,
            },
        ]);
    });
    it('should process sticker', async () => {
        const out = await check({
            sticker: {
                packId: new Uint8Array([1, 2, 3]),
                packKey: new Uint8Array([4, 5, 6]),
                stickerId: 1,
                data: UNPROCESSED_ATTACHMENT,
            },
        });
        chai_1.assert.deepStrictEqual(out.sticker, {
            packId: '010203',
            packKey: 'BAUG',
            stickerId: 1,
            data: PROCESSED_ATTACHMENT,
        });
    });
    it('should process FLAGS=END_SESSION', async () => {
        const out = await check({
            flags: FLAGS.END_SESSION,
            body: 'should be deleted',
            group: {
                id: GROUP_ID,
                type: protobuf_1.SignalService.GroupContext.Type.DELIVER,
            },
            attachments: [UNPROCESSED_ATTACHMENT],
        });
        chai_1.assert.isUndefined(out.body);
        chai_1.assert.isUndefined(out.group);
        chai_1.assert.deepStrictEqual(out.attachments, []);
    });
    it('should process FLAGS=EXPIRATION_TIMER_UPDATE,PROFILE_KEY_UPDATE', async () => {
        const values = [FLAGS.EXPIRATION_TIMER_UPDATE, FLAGS.PROFILE_KEY_UPDATE];
        for (const flags of values) {
            // eslint-disable-next-line no-await-in-loop
            const out = await check({
                flags,
                body: 'should be deleted',
                attachments: [UNPROCESSED_ATTACHMENT],
            });
            chai_1.assert.isUndefined(out.body);
            chai_1.assert.deepStrictEqual(out.attachments, []);
        }
    });
    it('processes trivial fields', async () => {
        chai_1.assert.strictEqual((await check({ flags: null })).flags, 0);
        chai_1.assert.strictEqual((await check({ flags: 1 })).flags, 1);
        chai_1.assert.strictEqual((await check({ expireTimer: null })).expireTimer, 0);
        chai_1.assert.strictEqual((await check({ expireTimer: 123 })).expireTimer, 123);
        chai_1.assert.isFalse((await check({ isViewOnce: null })).isViewOnce);
        chai_1.assert.isFalse((await check({ isViewOnce: false })).isViewOnce);
        chai_1.assert.isTrue((await check({ isViewOnce: true })).isViewOnce);
    });
});
