"use strict";
// Copyright 2017-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const sleep_1 = require("../util/sleep");
const explodePromise_1 = require("../util/explodePromise");
const TaskWithTimeout_1 = __importStar(require("../textsecure/TaskWithTimeout"));
describe('createTaskWithTimeout', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });
    afterEach(() => {
        sandbox.restore();
    });
    it('resolves when promise resolves', async () => {
        const task = () => Promise.resolve('hi!');
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(task, 'resolving-task');
        const result = await taskWithTimeout();
        chai_1.assert.strictEqual(result, 'hi!');
    });
    it('flows error from promise back', async () => {
        const error = new Error('original');
        const task = () => Promise.reject(error);
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(task, 'rejecting-task');
        await chai_1.assert.isRejected(taskWithTimeout(), 'original');
    });
    it('rejects if promise takes too long (this one logs error to console)', async () => {
        const clock = sandbox.useFakeTimers();
        const { promise: pause } = (0, explodePromise_1.explodePromise)();
        // Never resolves
        const task = () => pause;
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(task, 'slow-task');
        const promise = chai_1.assert.isRejected(taskWithTimeout());
        await clock.runToLastAsync();
        await promise;
    });
    it('rejects if task throws (and does not log about taking too long)', async () => {
        const clock = sandbox.useFakeTimers();
        const error = new Error('Task is throwing!');
        const task = () => {
            throw error;
        };
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(task, 'throwing-task');
        await clock.runToLastAsync();
        await chai_1.assert.isRejected(taskWithTimeout(), 'Task is throwing!');
    });
    it('passes arguments to the underlying function', async () => {
        const task = (arg) => Promise.resolve(arg);
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(task, 'arguments-task');
        const result = await taskWithTimeout('hi!');
        chai_1.assert.strictEqual(result, 'hi!');
    });
    it('suspends and resumes tasks', async () => {
        const clock = sandbox.useFakeTimers();
        let state = 0;
        const task = async () => {
            state = 1;
            await (0, sleep_1.sleep)(900);
            state = 2;
            await (0, sleep_1.sleep)(900);
            state = 3;
        };
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(task, 'suspend-task', {
            timeout: 1000,
        });
        const promise = taskWithTimeout();
        chai_1.assert.strictEqual(state, 1);
        (0, TaskWithTimeout_1.suspendTasksWithTimeout)();
        await clock.tickAsync(900);
        chai_1.assert.strictEqual(state, 2);
        (0, TaskWithTimeout_1.resumeTasksWithTimeout)();
        await clock.tickAsync(900);
        chai_1.assert.strictEqual(state, 3);
        await promise;
    });
    it('suspends and resumes timing out task', async () => {
        const clock = sandbox.useFakeTimers();
        const { promise: pause } = (0, explodePromise_1.explodePromise)();
        // Never resolves
        const task = () => pause;
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(task, 'suspend-slow-task');
        const promise = chai_1.assert.isRejected(taskWithTimeout());
        (0, TaskWithTimeout_1.suspendTasksWithTimeout)();
        await clock.runToLastAsync();
        (0, TaskWithTimeout_1.resumeTasksWithTimeout)();
        await clock.runToLastAsync();
        await promise;
    });
});
