"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const BadgeCategory_1 = require("../../badges/BadgeCategory");
const BadgeImageTheme_1 = require("../../badges/BadgeImageTheme");
const getBadgeImageFileLocalPath_1 = require("../../badges/getBadgeImageFileLocalPath");
describe('getBadgeImageFileLocalPath', () => {
    const image = (localPath) => ({
        localPath,
        url: 'https://example.com/ignored.svg',
    });
    const badge = {
        category: BadgeCategory_1.BadgeCategory.Donor,
        descriptionTemplate: 'foo bar',
        id: 'foo',
        images: ['small', 'medium', 'large', 'huge'].map(size => ({
            [BadgeImageTheme_1.BadgeImageTheme.Dark]: image(`/${size}-dark.svg`),
            [BadgeImageTheme_1.BadgeImageTheme.Light]: image(undefined),
            [BadgeImageTheme_1.BadgeImageTheme.Transparent]: image(`/${size}-trns.svg`),
        })),
        name: 'Test Badge',
    };
    it('returns undefined if passed no badge', () => {
        const result = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(undefined, 123, BadgeImageTheme_1.BadgeImageTheme.Transparent);
        chai_1.assert.isUndefined(result);
    });
    it('returns the first image if passed a small size', () => {
        const darkResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 10, BadgeImageTheme_1.BadgeImageTheme.Dark);
        chai_1.assert.strictEqual(darkResult, '/small-dark.svg');
        const lightResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 11, BadgeImageTheme_1.BadgeImageTheme.Light);
        chai_1.assert.isUndefined(lightResult);
        const transparentResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 12, BadgeImageTheme_1.BadgeImageTheme.Transparent);
        chai_1.assert.strictEqual(transparentResult, '/small-trns.svg');
    });
    it('returns the second image if passed a size between 24 and 36', () => {
        const darkResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 24, BadgeImageTheme_1.BadgeImageTheme.Dark);
        chai_1.assert.strictEqual(darkResult, '/medium-dark.svg');
        const lightResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 30, BadgeImageTheme_1.BadgeImageTheme.Light);
        chai_1.assert.isUndefined(lightResult);
        const transparentResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 35, BadgeImageTheme_1.BadgeImageTheme.Transparent);
        chai_1.assert.strictEqual(transparentResult, '/medium-trns.svg');
    });
    it('returns the third image if passed a size between 36 and 160', () => {
        const darkResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 36, BadgeImageTheme_1.BadgeImageTheme.Dark);
        chai_1.assert.strictEqual(darkResult, '/large-dark.svg');
        const lightResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 100, BadgeImageTheme_1.BadgeImageTheme.Light);
        chai_1.assert.isUndefined(lightResult);
        const transparentResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 159, BadgeImageTheme_1.BadgeImageTheme.Transparent);
        chai_1.assert.strictEqual(transparentResult, '/large-trns.svg');
    });
    it('returns the last image if passed a size above 159', () => {
        const darkResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 160, BadgeImageTheme_1.BadgeImageTheme.Dark);
        chai_1.assert.strictEqual(darkResult, '/huge-dark.svg');
        const lightResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 200, BadgeImageTheme_1.BadgeImageTheme.Light);
        chai_1.assert.isUndefined(lightResult);
        const transparentResult = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, 999, BadgeImageTheme_1.BadgeImageTheme.Transparent);
        chai_1.assert.strictEqual(transparentResult, '/huge-trns.svg');
    });
});
