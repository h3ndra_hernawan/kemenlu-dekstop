"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isBadgeVisible_1 = require("../../badges/isBadgeVisible");
const BadgeCategory_1 = require("../../badges/BadgeCategory");
describe('isBadgeVisible', () => {
    const fakeBadge = (isVisible) => (Object.assign({ category: BadgeCategory_1.BadgeCategory.Donor, descriptionTemplate: 'test', id: 'TEST', images: [], name: 'test' }, (typeof isVisible === 'boolean' ? { expiresAt: 123, isVisible } : {})));
    it("returns true if the visibility is unspecified (someone else's badge)", () => {
        chai_1.assert.isTrue((0, isBadgeVisible_1.isBadgeVisible)(fakeBadge()));
    });
    it('returns false if not visible', () => {
        chai_1.assert.isFalse((0, isBadgeVisible_1.isBadgeVisible)(fakeBadge(false)));
    });
    it('returns true if visible', () => {
        chai_1.assert.isTrue((0, isBadgeVisible_1.isBadgeVisible)(fakeBadge(true)));
    });
});
