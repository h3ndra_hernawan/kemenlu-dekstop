"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isBadgeImageFileUrlValid_1 = require("../../badges/isBadgeImageFileUrlValid");
describe('isBadgeImageFileUrlValid', () => {
    const UPDATES_URL = 'https://updates2.signal.org/desktop';
    it('returns false for invalid URLs', () => {
        ['', 'uhh', 'http:'].forEach(url => {
            chai_1.assert.isFalse((0, isBadgeImageFileUrlValid_1.isBadgeImageFileUrlValid)(url, UPDATES_URL));
        });
    });
    it("returns false if the URL doesn't start with the right prefix", () => {
        [
            'https://user:pass@updates2.signal.org/static/badges/foo',
            'https://signal.org/static/badges/foo',
            'https://updates.signal.org/static/badges/foo',
            'http://updates2.signal.org/static/badges/foo',
            'http://updates2.signal.org/static/badges/foo',
        ].forEach(url => {
            chai_1.assert.isFalse((0, isBadgeImageFileUrlValid_1.isBadgeImageFileUrlValid)(url, UPDATES_URL));
        });
    });
    it('returns true for valid URLs', () => {
        [
            'https://updates2.signal.org/static/badges/foo',
            'https://updates2.signal.org/static/badges/foo.svg',
            'https://updates2.signal.org/static/badges/foo.txt',
        ].forEach(url => {
            chai_1.assert.isTrue((0, isBadgeImageFileUrlValid_1.isBadgeImageFileUrlValid)(url, UPDATES_URL));
        });
    });
});
