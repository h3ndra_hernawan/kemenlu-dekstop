"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const lodash_1 = require("lodash");
const BadgeCategory_1 = require("../../badges/BadgeCategory");
const BadgeImageTheme_1 = require("../../badges/BadgeImageTheme");
const parseBadgesFromServer_1 = require("../../badges/parseBadgesFromServer");
describe('parseBadgesFromServer', () => {
    const UPDATES_URL = 'https://updates2.signal.org/desktop';
    const validBadgeData = {
        id: 'fake-badge-id',
        category: 'donor',
        name: 'Cool Donor',
        description: 'Hello {short_name}',
        svg: 'huge badge.svg',
        svgs: ['small', 'medium', 'large'].map(size => ({
            dark: `${size} badge dark.svg`,
            light: `${size} badge light.svg`,
            transparent: `${size} badge transparent.svg`,
        })),
    };
    const validBadge = {
        id: validBadgeData.id,
        category: BadgeCategory_1.BadgeCategory.Donor,
        name: 'Cool Donor',
        descriptionTemplate: 'Hello {short_name}',
        images: [
            ...['small', 'medium', 'large'].map(size => ({
                [BadgeImageTheme_1.BadgeImageTheme.Dark]: {
                    url: `https://updates2.signal.org/static/badges/${size}%20badge%20dark.svg`,
                },
                [BadgeImageTheme_1.BadgeImageTheme.Light]: {
                    url: `https://updates2.signal.org/static/badges/${size}%20badge%20light.svg`,
                },
                [BadgeImageTheme_1.BadgeImageTheme.Transparent]: {
                    url: `https://updates2.signal.org/static/badges/${size}%20badge%20transparent.svg`,
                },
            })),
            {
                [BadgeImageTheme_1.BadgeImageTheme.Transparent]: {
                    url: 'https://updates2.signal.org/static/badges/huge%20badge.svg',
                },
            },
        ],
    };
    it('returns an empty array if passed a non-array', () => {
        [undefined, null, 'foo.svg', validBadgeData].forEach(input => {
            chai_1.assert.isEmpty((0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL));
        });
    });
    it('returns an empty array if passed one', () => {
        chai_1.assert.isEmpty((0, parseBadgesFromServer_1.parseBadgesFromServer)([], UPDATES_URL));
    });
    it('parses valid badge data', () => {
        const input = [validBadgeData];
        chai_1.assert.deepStrictEqual((0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL), [
            validBadge,
        ]);
    });
    it('only returns the first 1000 badges', () => {
        const input = Array(1234).fill(validBadgeData);
        chai_1.assert.lengthOf((0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL), 1000);
    });
    it('discards badges with invalid IDs', () => {
        [undefined, null, 123].forEach(id => {
            const invalidBadgeData = Object.assign(Object.assign({}, validBadgeData), { name: 'Should be missing', id });
            const input = [validBadgeData, invalidBadgeData];
            chai_1.assert.deepStrictEqual((0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL), [
                validBadge,
            ]);
        });
    });
    it('discards badges with invalid names', () => {
        [undefined, null, 123].forEach(name => {
            const invalidBadgeData = Object.assign(Object.assign({}, validBadgeData), { description: 'Should be missing', name });
            const input = [validBadgeData, invalidBadgeData];
            chai_1.assert.deepStrictEqual((0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL), [
                validBadge,
            ]);
        });
    });
    it('discards badges with invalid description templates', () => {
        [undefined, null, 123].forEach(description => {
            const invalidBadgeData = Object.assign(Object.assign({}, validBadgeData), { name: 'Hello', description });
            const input = [validBadgeData, invalidBadgeData];
            chai_1.assert.deepStrictEqual((0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL), [
                validBadge,
            ]);
        });
    });
    it('discards badges that lack a valid "huge" SVG', () => {
        const input = [
            validBadgeData,
            (0, lodash_1.omit)(validBadgeData, 'svg'),
            Object.assign(Object.assign({}, validBadgeData), { svg: 123 }),
        ];
        chai_1.assert.deepStrictEqual((0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL), [
            validBadge,
        ]);
    });
    it('discards badges that lack exactly 3 valid "normal" SVGs', () => {
        const input = [
            validBadgeData,
            (0, lodash_1.omit)(validBadgeData, 'svgs'),
            Object.assign(Object.assign({}, validBadgeData), { svgs: 'bad!' }),
            Object.assign(Object.assign({}, validBadgeData), { svgs: [] }),
            Object.assign(Object.assign({}, validBadgeData), { svgs: validBadgeData.svgs.slice(0, 2) }),
            Object.assign(Object.assign({}, validBadgeData), { svgs: [{}, ...validBadgeData.svgs.slice(1)] }),
            Object.assign(Object.assign({}, validBadgeData), { svgs: [{ dark: 123 }, ...validBadgeData.svgs.slice(1)] }),
            Object.assign(Object.assign({}, validBadgeData), { svgs: [
                    ...validBadgeData.svgs,
                    {
                        dark: 'too.svg',
                        light: 'many.svg',
                        transparent: 'badges.svg',
                    },
                ] }),
        ];
        chai_1.assert.deepStrictEqual((0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL), [
            validBadge,
        ]);
    });
    it('converts "donor" to the Donor category', () => {
        var _a;
        const input = [validBadgeData];
        chai_1.assert.strictEqual((_a = (0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL)[0]) === null || _a === void 0 ? void 0 : _a.category, BadgeCategory_1.BadgeCategory.Donor);
    });
    it('converts "other" to the Other category', () => {
        var _a;
        const input = [
            Object.assign(Object.assign({}, validBadgeData), { category: 'other' }),
        ];
        chai_1.assert.strictEqual((_a = (0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL)[0]) === null || _a === void 0 ? void 0 : _a.category, BadgeCategory_1.BadgeCategory.Other);
    });
    it('converts unexpected categories to Other', () => {
        var _a;
        const input = [
            Object.assign(Object.assign({}, validBadgeData), { category: 'garbage' }),
        ];
        chai_1.assert.strictEqual((_a = (0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL)[0]) === null || _a === void 0 ? void 0 : _a.category, BadgeCategory_1.BadgeCategory.Other);
    });
    it('parses your own badges', () => {
        const input = [
            Object.assign(Object.assign({}, validBadgeData), { expiration: 1234, visible: true }),
        ];
        const badge = (0, parseBadgesFromServer_1.parseBadgesFromServer)(input, UPDATES_URL)[0];
        if (!badge || !('expiresAt' in badge) || !('isVisible' in badge)) {
            throw new Error('Badge is invalid');
        }
        chai_1.assert.strictEqual(badge.expiresAt, 1234 * 1000);
        chai_1.assert.isTrue(badge.isVisible);
    });
});
