"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const audioDeviceModule_1 = require("../../calling/audioDeviceModule");
const findBestMatchingDevice_1 = require("../../calling/findBestMatchingDevice");
describe('"find best matching device" helpers', () => {
    describe('findBestMatchingAudioDeviceIndex', () => {
        const itReturnsUndefinedIfNoDevicesAreAvailable = (admOptions) => {
            it('returns undefined if no devices are available', () => {
                [
                    undefined,
                    { name: 'Big Microphone', index: 1, uniqueId: 'abc123' },
                ].forEach(preferred => {
                    chai_1.assert.isUndefined((0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)(Object.assign({ available: [], preferred }, admOptions)));
                });
            });
        };
        const itReturnsTheFirstAvailableDeviceIfNoneIsPreferred = (admOptions) => {
            it('returns the first available device if none is preferred', () => {
                chai_1.assert.strictEqual((0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)(Object.assign({ available: [
                        { name: 'A', index: 123, uniqueId: 'device-A' },
                        { name: 'B', index: 456, uniqueId: 'device-B' },
                        { name: 'C', index: 789, uniqueId: 'device-C' },
                    ], preferred: undefined }, admOptions)), 0);
            });
        };
        const testUniqueIdMatch = (admOptions) => {
            chai_1.assert.strictEqual((0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)(Object.assign({ available: [
                    { name: 'A', index: 123, uniqueId: 'device-A' },
                    { name: 'B', index: 456, uniqueId: 'device-B' },
                    { name: 'C', index: 789, uniqueId: 'device-C' },
                ], preferred: { name: 'Ignored', index: 99, uniqueId: 'device-C' } }, admOptions)), 2);
        };
        const testNameMatch = (admOptions) => {
            chai_1.assert.strictEqual((0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)(Object.assign({ available: [
                    { name: 'A', index: 123, uniqueId: 'device-A' },
                    { name: 'B', index: 456, uniqueId: 'device-B' },
                    { name: 'C', index: 789, uniqueId: 'device-C' },
                ], preferred: { name: 'C', index: 99, uniqueId: 'ignored' } }, admOptions)), 2);
        };
        const itReturnsTheFirstAvailableDeviceIfThePreferredDeviceIsNotFound = (admOptions) => {
            it('returns the first available device if the preferred device is not found', () => {
                chai_1.assert.strictEqual((0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)(Object.assign({ available: [
                        { name: 'A', index: 123, uniqueId: 'device-A' },
                        { name: 'B', index: 456, uniqueId: 'device-B' },
                        { name: 'C', index: 789, uniqueId: 'device-C' },
                    ], preferred: { name: 'X', index: 123, uniqueId: 'Y' } }, admOptions)), 0);
            });
        };
        describe('with default audio device module', () => {
            const admOptions = {
                previousAudioDeviceModule: audioDeviceModule_1.AudioDeviceModule.Default,
                currentAudioDeviceModule: audioDeviceModule_1.AudioDeviceModule.Default,
            };
            itReturnsUndefinedIfNoDevicesAreAvailable(admOptions);
            itReturnsTheFirstAvailableDeviceIfNoneIsPreferred(admOptions);
            it('returns a unique ID match if it exists', () => {
                testUniqueIdMatch(admOptions);
            });
            it('returns a name match if it exists', () => {
                testNameMatch(admOptions);
            });
            itReturnsTheFirstAvailableDeviceIfThePreferredDeviceIsNotFound(admOptions);
        });
        describe('when going from the default to Windows ADM2', () => {
            const admOptions = {
                previousAudioDeviceModule: audioDeviceModule_1.AudioDeviceModule.Default,
                currentAudioDeviceModule: audioDeviceModule_1.AudioDeviceModule.WindowsAdm2,
            };
            itReturnsUndefinedIfNoDevicesAreAvailable(admOptions);
            itReturnsTheFirstAvailableDeviceIfNoneIsPreferred(admOptions);
            it('returns 0 if that was the previous preferred index (and a device is available)', () => {
                chai_1.assert.strictEqual((0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)(Object.assign({ available: [
                        { name: 'A', index: 123, uniqueId: 'device-A' },
                        { name: 'B', index: 456, uniqueId: 'device-B' },
                    ], preferred: { name: 'B', index: 0, uniqueId: 'device-B' } }, admOptions)), 0);
            });
            it('returns a unique ID match if it exists and the preferred index is not 0', () => {
                testUniqueIdMatch(admOptions);
            });
            it('returns a name match if it exists and the preferred index is not 0', () => {
                testNameMatch(admOptions);
            });
            itReturnsTheFirstAvailableDeviceIfThePreferredDeviceIsNotFound(admOptions);
        });
        describe('when going "backwards" from Windows ADM2 to the default', () => {
            const admOptions = {
                previousAudioDeviceModule: audioDeviceModule_1.AudioDeviceModule.WindowsAdm2,
                currentAudioDeviceModule: audioDeviceModule_1.AudioDeviceModule.Default,
            };
            itReturnsUndefinedIfNoDevicesAreAvailable(admOptions);
            itReturnsTheFirstAvailableDeviceIfNoneIsPreferred(admOptions);
            it('returns a unique ID match if it exists', () => {
                testUniqueIdMatch(admOptions);
            });
            it('returns a name match if it exists', () => {
                testNameMatch(admOptions);
            });
            itReturnsTheFirstAvailableDeviceIfThePreferredDeviceIsNotFound(admOptions);
        });
        describe('with Windows ADM2', () => {
            const admOptions = {
                previousAudioDeviceModule: audioDeviceModule_1.AudioDeviceModule.WindowsAdm2,
                currentAudioDeviceModule: audioDeviceModule_1.AudioDeviceModule.WindowsAdm2,
            };
            itReturnsUndefinedIfNoDevicesAreAvailable(admOptions);
            itReturnsTheFirstAvailableDeviceIfNoneIsPreferred(admOptions);
            [0, 1].forEach(index => {
                it(`returns ${index} if that was the previous preferred index (and a device is available)`, () => {
                    chai_1.assert.strictEqual((0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)(Object.assign({ available: [
                            { name: 'A', index: 123, uniqueId: 'device-A' },
                            { name: 'B', index: 456, uniqueId: 'device-B' },
                            { name: 'C', index: 789, uniqueId: 'device-C' },
                        ], preferred: { name: 'C', index, uniqueId: 'device-C' } }, admOptions)), index);
                });
            });
            it("returns 0 if the previous preferred index was 1 but there's only 1 audio device", () => {
                chai_1.assert.strictEqual((0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)(Object.assign({ available: [{ name: 'A', index: 123, uniqueId: 'device-A' }], preferred: { name: 'C', index: 1, uniqueId: 'device-C' } }, admOptions)), 0);
            });
            it('returns a unique ID match if it exists and the preferred index is not 0 or 1', () => {
                testUniqueIdMatch(admOptions);
            });
            it('returns a name match if it exists and the preferred index is not 0 or 1', () => {
                testNameMatch(admOptions);
            });
            itReturnsTheFirstAvailableDeviceIfThePreferredDeviceIsNotFound(admOptions);
        });
    });
});
