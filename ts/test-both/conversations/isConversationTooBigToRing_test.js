"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const lodash_1 = require("lodash");
const remoteConfig = __importStar(require("../../RemoteConfig"));
const UUID_1 = require("../../types/UUID");
const isConversationTooBigToRing_1 = require("../../conversations/isConversationTooBigToRing");
describe('isConversationTooBigToRing', () => {
    let sinonSandbox;
    let getMaxGroupCallRingSizeStub;
    beforeEach(() => {
        sinonSandbox = sinon.createSandbox();
        const getValueStub = sinonSandbox.stub(remoteConfig, 'getValue');
        getMaxGroupCallRingSizeStub = getValueStub.withArgs('global.calling.maxGroupCallRingSize');
    });
    const fakeMemberships = (count) => (0, lodash_1.times)(count, () => ({ uuid: UUID_1.UUID.generate().toString(), isAdmin: false }));
    afterEach(() => {
        sinonSandbox.restore();
    });
    it('returns false if there are no memberships (i.e., for a direct conversation)', () => {
        chai_1.assert.isFalse((0, isConversationTooBigToRing_1.isConversationTooBigToRing)({}));
        chai_1.assert.isFalse((0, isConversationTooBigToRing_1.isConversationTooBigToRing)({ memberships: [] }));
    });
    const textMaximum = (max) => {
        for (let count = 1; count < max; count += 1) {
            const memberships = fakeMemberships(count);
            chai_1.assert.isFalse((0, isConversationTooBigToRing_1.isConversationTooBigToRing)({ memberships }));
        }
        for (let count = max; count < max + 5; count += 1) {
            const memberships = fakeMemberships(count);
            chai_1.assert.isTrue((0, isConversationTooBigToRing_1.isConversationTooBigToRing)({ memberships }));
        }
    };
    it('returns whether there are 16 or more people in the group, if there is nothing in remote config', () => {
        textMaximum(16);
    });
    it('returns whether there are 16 or more people in the group, if the remote config value is bogus', () => {
        getMaxGroupCallRingSizeStub.returns('uh oh');
        textMaximum(16);
    });
    it('returns whether there are 9 or more people in the group, if the remote config value is 9', () => {
        getMaxGroupCallRingSizeStub.returns('9');
        textMaximum(9);
    });
});
