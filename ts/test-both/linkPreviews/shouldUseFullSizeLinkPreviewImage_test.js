"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const MIME_1 = require("../../types/MIME");
const fakeAttachment_1 = require("../helpers/fakeAttachment");
const shouldUseFullSizeLinkPreviewImage_1 = require("../../linkPreviews/shouldUseFullSizeLinkPreviewImage");
describe('shouldUseFullSizeLinkPreviewImage', () => {
    const baseLinkPreview = {
        title: 'Foo Bar',
        domain: 'example.com',
        url: 'https://example.com/foo.html',
        isStickerPack: false,
    };
    it('returns false if there is no image', () => {
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign({}, baseLinkPreview)));
    });
    it('returns false is the preview is a sticker pack', () => {
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { isStickerPack: true, image: (0, fakeAttachment_1.fakeAttachment)() })));
    });
    it("returns false if either of the image's dimensions are missing", () => {
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ width: undefined }) })));
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ height: undefined }) })));
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ width: undefined, height: undefined }) })));
    });
    it("returns false if either of the image's dimensions are <200px", () => {
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ width: 199 }) })));
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ height: 199 }) })));
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ width: 150, height: 199 }) })));
    });
    it('returns false if the image is square', () => {
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ width: 200, height: 200 }) })));
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ width: 500, height: 500 }) })));
    });
    it('returns false if the image is roughly square', () => {
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ width: 200, height: 201 }) })));
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ width: 497, height: 501 }) })));
    });
    it("returns false for large attachments that aren't images", () => {
        chai_1.assert.isFalse((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                fileName: 'foo.mp4',
                url: '/tmp/foo.mp4',
            }) })));
    });
    it('returns true for larger images', () => {
        chai_1.assert.isTrue((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)({ width: 200, height: 500 }) })));
        chai_1.assert.isTrue((0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(Object.assign(Object.assign({}, baseLinkPreview), { image: (0, fakeAttachment_1.fakeAttachment)() })));
    });
});
