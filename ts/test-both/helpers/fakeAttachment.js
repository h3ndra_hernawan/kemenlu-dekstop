"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.fakeDraftAttachment = exports.fakeAttachment = void 0;
const MIME_1 = require("../../types/MIME");
const fakeAttachment = (overrides = {}) => (Object.assign({ contentType: MIME_1.IMAGE_JPEG, width: 800, height: 600, size: 10304 }, overrides));
exports.fakeAttachment = fakeAttachment;
const fakeDraftAttachment = (overrides = {}) => (Object.assign({ pending: false, contentType: MIME_1.IMAGE_JPEG, path: 'file.jpg', size: 10304 }, overrides));
exports.fakeDraftAttachment = fakeDraftAttachment;
