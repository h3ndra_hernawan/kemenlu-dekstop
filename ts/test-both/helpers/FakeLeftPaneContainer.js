"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FakeLeftPaneContainer = void 0;
const react_1 = __importDefault(require("react"));
const _util_1 = require("../../components/_util");
const WIDTHS = {
    [_util_1.WidthBreakpoint.Wide]: 350,
    [_util_1.WidthBreakpoint.Medium]: 280,
    [_util_1.WidthBreakpoint.Narrow]: 130,
};
const FakeLeftPaneContainer = ({ children, containerWidthBreakpoint, }) => {
    return (react_1.default.createElement("div", { className: `module-left-pane--width-${containerWidthBreakpoint}`, style: { width: WIDTHS[containerWidthBreakpoint] } }, children));
};
exports.FakeLeftPaneContainer = FakeLeftPaneContainer;
