"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFakeBadges = exports.getFakeBadge = void 0;
const lodash_1 = require("lodash");
const BadgeCategory_1 = require("../../badges/BadgeCategory");
const BadgeImageTheme_1 = require("../../badges/BadgeImageTheme");
const iterables_1 = require("../../util/iterables");
function getFakeBadge({ alternate = false, id = 'test-badge', } = {}) {
    const imageFile = {
        localPath: `/fixtures/${alternate ? 'blue' : 'orange'}-heart.svg`,
        url: 'https://example.com/ignored.svg',
    };
    return {
        id,
        category: alternate ? BadgeCategory_1.BadgeCategory.Other : BadgeCategory_1.BadgeCategory.Donor,
        name: `Test Badge ${alternate ? 'B' : 'A'}`,
        descriptionTemplate: "{short_name} got this badge because they're cool. Signal is a nonprofit with no advertisers or investors, supported only by people like you.",
        images: [
            ...Array(3).fill((0, iterables_1.zipObject)(Object.values(BadgeImageTheme_1.BadgeImageTheme), (0, iterables_1.repeat)(imageFile))),
            { [BadgeImageTheme_1.BadgeImageTheme.Transparent]: imageFile },
        ],
    };
}
exports.getFakeBadge = getFakeBadge;
const getFakeBadges = (count) => (0, lodash_1.times)(count, index => getFakeBadge({
    alternate: index % 2 !== 0,
    id: `test-badge-${index}`,
}));
exports.getFakeBadges = getFakeBadges;
