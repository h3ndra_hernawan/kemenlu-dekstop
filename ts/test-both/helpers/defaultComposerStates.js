"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultSetGroupMetadataComposerState = exports.defaultChooseGroupMembersComposerState = exports.defaultStartDirectConversationComposerState = void 0;
const conversationsEnums_1 = require("../../state/ducks/conversationsEnums");
const toggleSelectedContactForGroupAddition_1 = require("../../groups/toggleSelectedContactForGroupAddition");
exports.defaultStartDirectConversationComposerState = {
    step: conversationsEnums_1.ComposerStep.StartDirectConversation,
    searchTerm: '',
    isFetchingUsername: false,
};
exports.defaultChooseGroupMembersComposerState = {
    step: conversationsEnums_1.ComposerStep.ChooseGroupMembers,
    searchTerm: '',
    cantAddContactIdForModal: undefined,
    groupAvatar: undefined,
    groupName: '',
    groupExpireTimer: 0,
    maximumGroupSizeModalState: toggleSelectedContactForGroupAddition_1.OneTimeModalState.NeverShown,
    recommendedGroupSizeModalState: toggleSelectedContactForGroupAddition_1.OneTimeModalState.NeverShown,
    selectedConversationIds: [],
    userAvatarData: [],
};
exports.defaultSetGroupMetadataComposerState = {
    step: conversationsEnums_1.ComposerStep.SetGroupMetadata,
    isEditingAvatar: false,
    groupAvatar: undefined,
    groupName: '',
    groupExpireTimer: 0,
    maximumGroupSizeModalState: toggleSelectedContactForGroupAddition_1.OneTimeModalState.NeverShown,
    recommendedGroupSizeModalState: toggleSelectedContactForGroupAddition_1.OneTimeModalState.NeverShown,
    selectedConversationIds: [],
    userAvatarData: [],
    isCreating: false,
    hasError: false,
};
