"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRandomColor = void 0;
const lodash_1 = require("lodash");
const Colors_1 = require("../../types/Colors");
function getRandomColor() {
    return (0, lodash_1.sample)(Colors_1.AvatarColors) || Colors_1.AvatarColors[0];
}
exports.getRandomColor = getRandomColor;
