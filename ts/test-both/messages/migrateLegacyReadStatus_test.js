"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
// We want to cast to `any` because we're passing an unexpected field.
/* eslint-disable @typescript-eslint/no-explicit-any */
const chai_1 = require("chai");
const MessageReadStatus_1 = require("../../messages/MessageReadStatus");
const migrateLegacyReadStatus_1 = require("../../messages/migrateLegacyReadStatus");
describe('migrateLegacyReadStatus', () => {
    it("doesn't migrate messages that already have the modern read state", () => {
        chai_1.assert.isUndefined((0, migrateLegacyReadStatus_1.migrateLegacyReadStatus)({ readStatus: MessageReadStatus_1.ReadStatus.Read }));
        chai_1.assert.isUndefined((0, migrateLegacyReadStatus_1.migrateLegacyReadStatus)({ readStatus: MessageReadStatus_1.ReadStatus.Unread }));
    });
    it('converts legacy read values to "read"', () => {
        chai_1.assert.strictEqual((0, migrateLegacyReadStatus_1.migrateLegacyReadStatus)({}), MessageReadStatus_1.ReadStatus.Read);
        chai_1.assert.strictEqual((0, migrateLegacyReadStatus_1.migrateLegacyReadStatus)({ unread: 0 }), MessageReadStatus_1.ReadStatus.Read);
        chai_1.assert.strictEqual((0, migrateLegacyReadStatus_1.migrateLegacyReadStatus)({ unread: false }), MessageReadStatus_1.ReadStatus.Read);
    });
    it('converts legacy unread values to "unread"', () => {
        chai_1.assert.strictEqual((0, migrateLegacyReadStatus_1.migrateLegacyReadStatus)({ unread: 1 }), MessageReadStatus_1.ReadStatus.Unread);
        chai_1.assert.strictEqual((0, migrateLegacyReadStatus_1.migrateLegacyReadStatus)({ unread: true }), MessageReadStatus_1.ReadStatus.Unread);
    });
    it('converts unexpected truthy values to "unread"', () => {
        chai_1.assert.strictEqual((0, migrateLegacyReadStatus_1.migrateLegacyReadStatus)({ unread: 99 }), MessageReadStatus_1.ReadStatus.Unread);
        chai_1.assert.strictEqual((0, migrateLegacyReadStatus_1.migrateLegacyReadStatus)({ unread: 'wow!' }), MessageReadStatus_1.ReadStatus.Unread);
    });
});
