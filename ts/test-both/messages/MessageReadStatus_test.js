"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const MessageReadStatus_1 = require("../../messages/MessageReadStatus");
describe('message read status utilities', () => {
    describe('maxReadStatus', () => {
        it('returns the status if passed the same status twice', () => {
            chai_1.assert.strictEqual((0, MessageReadStatus_1.maxReadStatus)(MessageReadStatus_1.ReadStatus.Unread, MessageReadStatus_1.ReadStatus.Unread), MessageReadStatus_1.ReadStatus.Unread);
        });
        it('sorts Unread < Read', () => {
            chai_1.assert.strictEqual((0, MessageReadStatus_1.maxReadStatus)(MessageReadStatus_1.ReadStatus.Unread, MessageReadStatus_1.ReadStatus.Read), MessageReadStatus_1.ReadStatus.Read);
            chai_1.assert.strictEqual((0, MessageReadStatus_1.maxReadStatus)(MessageReadStatus_1.ReadStatus.Read, MessageReadStatus_1.ReadStatus.Unread), MessageReadStatus_1.ReadStatus.Read);
        });
        it('sorts Read < Viewed', () => {
            chai_1.assert.strictEqual((0, MessageReadStatus_1.maxReadStatus)(MessageReadStatus_1.ReadStatus.Read, MessageReadStatus_1.ReadStatus.Viewed), MessageReadStatus_1.ReadStatus.Viewed);
            chai_1.assert.strictEqual((0, MessageReadStatus_1.maxReadStatus)(MessageReadStatus_1.ReadStatus.Viewed, MessageReadStatus_1.ReadStatus.Read), MessageReadStatus_1.ReadStatus.Viewed);
        });
        it('sorts Unread < Viewed', () => {
            chai_1.assert.strictEqual((0, MessageReadStatus_1.maxReadStatus)(MessageReadStatus_1.ReadStatus.Unread, MessageReadStatus_1.ReadStatus.Viewed), MessageReadStatus_1.ReadStatus.Viewed);
            chai_1.assert.strictEqual((0, MessageReadStatus_1.maxReadStatus)(MessageReadStatus_1.ReadStatus.Viewed, MessageReadStatus_1.ReadStatus.Unread), MessageReadStatus_1.ReadStatus.Viewed);
        });
    });
});
