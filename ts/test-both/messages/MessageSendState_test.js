"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const lodash_1 = require("lodash");
const uuid_1 = require("uuid");
const MessageSendState_1 = require("../../messages/MessageSendState");
describe('message send state utilities', () => {
    describe('maxStatus', () => {
        const expectedOrder = [
            MessageSendState_1.SendStatus.Failed,
            MessageSendState_1.SendStatus.Pending,
            MessageSendState_1.SendStatus.Sent,
            MessageSendState_1.SendStatus.Delivered,
            MessageSendState_1.SendStatus.Read,
            MessageSendState_1.SendStatus.Viewed,
        ];
        it('returns the input if arguments are equal', () => {
            expectedOrder.forEach(status => {
                chai_1.assert.strictEqual((0, MessageSendState_1.maxStatus)(status, status), status);
            });
        });
        it('orders the statuses', () => {
            (0, lodash_1.times)(100, () => {
                const [a, b] = (0, lodash_1.sampleSize)(expectedOrder, 2);
                const isABigger = expectedOrder.indexOf(a) > expectedOrder.indexOf(b);
                const expected = isABigger ? a : b;
                const actual = (0, MessageSendState_1.maxStatus)(a, b);
                chai_1.assert.strictEqual(actual, expected);
            });
        });
    });
    describe('isViewed', () => {
        it('returns true for viewed statuses', () => {
            chai_1.assert.isTrue((0, MessageSendState_1.isViewed)(MessageSendState_1.SendStatus.Viewed));
        });
        it('returns false for non-viewed statuses', () => {
            chai_1.assert.isFalse((0, MessageSendState_1.isViewed)(MessageSendState_1.SendStatus.Read));
            chai_1.assert.isFalse((0, MessageSendState_1.isViewed)(MessageSendState_1.SendStatus.Delivered));
            chai_1.assert.isFalse((0, MessageSendState_1.isViewed)(MessageSendState_1.SendStatus.Sent));
            chai_1.assert.isFalse((0, MessageSendState_1.isViewed)(MessageSendState_1.SendStatus.Pending));
            chai_1.assert.isFalse((0, MessageSendState_1.isViewed)(MessageSendState_1.SendStatus.Failed));
        });
    });
    describe('isRead', () => {
        it('returns true for read and viewed statuses', () => {
            chai_1.assert.isTrue((0, MessageSendState_1.isRead)(MessageSendState_1.SendStatus.Read));
            chai_1.assert.isTrue((0, MessageSendState_1.isRead)(MessageSendState_1.SendStatus.Viewed));
        });
        it('returns false for non-read statuses', () => {
            chai_1.assert.isFalse((0, MessageSendState_1.isRead)(MessageSendState_1.SendStatus.Delivered));
            chai_1.assert.isFalse((0, MessageSendState_1.isRead)(MessageSendState_1.SendStatus.Sent));
            chai_1.assert.isFalse((0, MessageSendState_1.isRead)(MessageSendState_1.SendStatus.Pending));
            chai_1.assert.isFalse((0, MessageSendState_1.isRead)(MessageSendState_1.SendStatus.Failed));
        });
    });
    describe('isDelivered', () => {
        it('returns true for delivered, read, and viewed statuses', () => {
            chai_1.assert.isTrue((0, MessageSendState_1.isDelivered)(MessageSendState_1.SendStatus.Delivered));
            chai_1.assert.isTrue((0, MessageSendState_1.isDelivered)(MessageSendState_1.SendStatus.Read));
            chai_1.assert.isTrue((0, MessageSendState_1.isDelivered)(MessageSendState_1.SendStatus.Viewed));
        });
        it('returns false for non-delivered statuses', () => {
            chai_1.assert.isFalse((0, MessageSendState_1.isDelivered)(MessageSendState_1.SendStatus.Sent));
            chai_1.assert.isFalse((0, MessageSendState_1.isDelivered)(MessageSendState_1.SendStatus.Pending));
            chai_1.assert.isFalse((0, MessageSendState_1.isDelivered)(MessageSendState_1.SendStatus.Failed));
        });
    });
    describe('isSent', () => {
        it('returns true for all statuses sent and "above"', () => {
            chai_1.assert.isTrue((0, MessageSendState_1.isSent)(MessageSendState_1.SendStatus.Sent));
            chai_1.assert.isTrue((0, MessageSendState_1.isSent)(MessageSendState_1.SendStatus.Delivered));
            chai_1.assert.isTrue((0, MessageSendState_1.isSent)(MessageSendState_1.SendStatus.Read));
            chai_1.assert.isTrue((0, MessageSendState_1.isSent)(MessageSendState_1.SendStatus.Viewed));
        });
        it('returns false for non-sent statuses', () => {
            chai_1.assert.isFalse((0, MessageSendState_1.isSent)(MessageSendState_1.SendStatus.Pending));
            chai_1.assert.isFalse((0, MessageSendState_1.isSent)(MessageSendState_1.SendStatus.Failed));
        });
    });
    describe('isFailed', () => {
        it('returns true for failed statuses', () => {
            chai_1.assert.isTrue((0, MessageSendState_1.isFailed)(MessageSendState_1.SendStatus.Failed));
        });
        it('returns false for non-failed statuses', () => {
            chai_1.assert.isFalse((0, MessageSendState_1.isFailed)(MessageSendState_1.SendStatus.Viewed));
            chai_1.assert.isFalse((0, MessageSendState_1.isFailed)(MessageSendState_1.SendStatus.Read));
            chai_1.assert.isFalse((0, MessageSendState_1.isFailed)(MessageSendState_1.SendStatus.Delivered));
            chai_1.assert.isFalse((0, MessageSendState_1.isFailed)(MessageSendState_1.SendStatus.Sent));
            chai_1.assert.isFalse((0, MessageSendState_1.isFailed)(MessageSendState_1.SendStatus.Pending));
        });
    });
    describe('someSendStatus', () => {
        it('returns false if there are no send states', () => {
            const alwaysTrue = () => true;
            chai_1.assert.isFalse((0, MessageSendState_1.someSendStatus)(undefined, alwaysTrue));
            chai_1.assert.isFalse((0, MessageSendState_1.someSendStatus)({}, alwaysTrue));
        });
        it('returns false if no send states match', () => {
            const sendStateByConversationId = {
                abc: {
                    status: MessageSendState_1.SendStatus.Sent,
                    updatedAt: Date.now(),
                },
                def: {
                    status: MessageSendState_1.SendStatus.Read,
                    updatedAt: Date.now(),
                },
            };
            chai_1.assert.isFalse((0, MessageSendState_1.someSendStatus)(sendStateByConversationId, (status) => status === MessageSendState_1.SendStatus.Delivered));
        });
        it('returns true if at least one send state matches', () => {
            const sendStateByConversationId = {
                abc: {
                    status: MessageSendState_1.SendStatus.Sent,
                    updatedAt: Date.now(),
                },
                def: {
                    status: MessageSendState_1.SendStatus.Read,
                    updatedAt: Date.now(),
                },
            };
            chai_1.assert.isTrue((0, MessageSendState_1.someSendStatus)(sendStateByConversationId, (status) => status === MessageSendState_1.SendStatus.Read));
        });
    });
    describe('isMessageJustForMe', () => {
        const ourConversationId = (0, uuid_1.v4)();
        it('returns false if the conversation has an empty send state', () => {
            chai_1.assert.isFalse((0, MessageSendState_1.isMessageJustForMe)(undefined, ourConversationId));
            chai_1.assert.isFalse((0, MessageSendState_1.isMessageJustForMe)({}, ourConversationId));
        });
        it('returns false if the message is for anyone else', () => {
            chai_1.assert.isFalse((0, MessageSendState_1.isMessageJustForMe)({
                [ourConversationId]: {
                    status: MessageSendState_1.SendStatus.Sent,
                    updatedAt: 123,
                },
                [(0, uuid_1.v4)()]: {
                    status: MessageSendState_1.SendStatus.Pending,
                    updatedAt: 123,
                },
            }, ourConversationId));
            // This is an invalid state, but we still want to test the behavior.
            chai_1.assert.isFalse((0, MessageSendState_1.isMessageJustForMe)({
                [(0, uuid_1.v4)()]: {
                    status: MessageSendState_1.SendStatus.Pending,
                    updatedAt: 123,
                },
            }, ourConversationId));
        });
        it('returns true if the message is just for you', () => {
            chai_1.assert.isTrue((0, MessageSendState_1.isMessageJustForMe)({
                [ourConversationId]: {
                    status: MessageSendState_1.SendStatus.Sent,
                    updatedAt: 123,
                },
            }, ourConversationId));
        });
    });
    describe('sendStateReducer', () => {
        const assertTransition = (startStatus, actionType, expectedStatus) => {
            const startState = {
                status: startStatus,
                updatedAt: 1,
            };
            const action = {
                type: actionType,
                updatedAt: 2,
            };
            const result = (0, MessageSendState_1.sendStateReducer)(startState, action);
            chai_1.assert.strictEqual(result.status, expectedStatus);
            chai_1.assert.strictEqual(result.updatedAt, startStatus === expectedStatus ? 1 : 2);
        };
        describe('transitions from Pending', () => {
            it('goes from Pending → Failed with a failure', () => {
                const result = (0, MessageSendState_1.sendStateReducer)({ status: MessageSendState_1.SendStatus.Pending, updatedAt: 999 }, { type: MessageSendState_1.SendActionType.Failed, updatedAt: 123 });
                chai_1.assert.deepEqual(result, {
                    status: MessageSendState_1.SendStatus.Failed,
                    updatedAt: 123,
                });
            });
            it('does nothing when receiving ManuallyRetried', () => {
                assertTransition(MessageSendState_1.SendStatus.Pending, MessageSendState_1.SendActionType.ManuallyRetried, MessageSendState_1.SendStatus.Pending);
            });
            it('goes from Pending to all other sent states', () => {
                assertTransition(MessageSendState_1.SendStatus.Pending, MessageSendState_1.SendActionType.Sent, MessageSendState_1.SendStatus.Sent);
                assertTransition(MessageSendState_1.SendStatus.Pending, MessageSendState_1.SendActionType.GotDeliveryReceipt, MessageSendState_1.SendStatus.Delivered);
                assertTransition(MessageSendState_1.SendStatus.Pending, MessageSendState_1.SendActionType.GotReadReceipt, MessageSendState_1.SendStatus.Read);
                assertTransition(MessageSendState_1.SendStatus.Pending, MessageSendState_1.SendActionType.GotViewedReceipt, MessageSendState_1.SendStatus.Viewed);
            });
        });
        describe('transitions from Failed', () => {
            it('does nothing when receiving a Failed action', () => {
                const result = (0, MessageSendState_1.sendStateReducer)({
                    status: MessageSendState_1.SendStatus.Failed,
                    updatedAt: 123,
                }, {
                    type: MessageSendState_1.SendActionType.Failed,
                    updatedAt: 999,
                });
                chai_1.assert.deepEqual(result, {
                    status: MessageSendState_1.SendStatus.Failed,
                    updatedAt: 123,
                });
            });
            it('goes from Failed to all other states', () => {
                assertTransition(MessageSendState_1.SendStatus.Failed, MessageSendState_1.SendActionType.ManuallyRetried, MessageSendState_1.SendStatus.Pending);
                assertTransition(MessageSendState_1.SendStatus.Failed, MessageSendState_1.SendActionType.Sent, MessageSendState_1.SendStatus.Sent);
                assertTransition(MessageSendState_1.SendStatus.Failed, MessageSendState_1.SendActionType.GotDeliveryReceipt, MessageSendState_1.SendStatus.Delivered);
                assertTransition(MessageSendState_1.SendStatus.Failed, MessageSendState_1.SendActionType.GotReadReceipt, MessageSendState_1.SendStatus.Read);
                assertTransition(MessageSendState_1.SendStatus.Failed, MessageSendState_1.SendActionType.GotViewedReceipt, MessageSendState_1.SendStatus.Viewed);
            });
        });
        describe('transitions from Sent', () => {
            it('does nothing when trying to go "backwards"', () => {
                [MessageSendState_1.SendActionType.Failed, MessageSendState_1.SendActionType.ManuallyRetried].forEach(type => {
                    assertTransition(MessageSendState_1.SendStatus.Sent, type, MessageSendState_1.SendStatus.Sent);
                });
            });
            it('does nothing when receiving a Sent action', () => {
                assertTransition(MessageSendState_1.SendStatus.Sent, MessageSendState_1.SendActionType.Sent, MessageSendState_1.SendStatus.Sent);
            });
            it('can go forward to other states', () => {
                assertTransition(MessageSendState_1.SendStatus.Sent, MessageSendState_1.SendActionType.GotDeliveryReceipt, MessageSendState_1.SendStatus.Delivered);
                assertTransition(MessageSendState_1.SendStatus.Sent, MessageSendState_1.SendActionType.GotReadReceipt, MessageSendState_1.SendStatus.Read);
                assertTransition(MessageSendState_1.SendStatus.Sent, MessageSendState_1.SendActionType.GotViewedReceipt, MessageSendState_1.SendStatus.Viewed);
            });
        });
        describe('transitions from Delivered', () => {
            it('does nothing when trying to go "backwards"', () => {
                [
                    MessageSendState_1.SendActionType.Failed,
                    MessageSendState_1.SendActionType.ManuallyRetried,
                    MessageSendState_1.SendActionType.Sent,
                ].forEach(type => {
                    assertTransition(MessageSendState_1.SendStatus.Delivered, type, MessageSendState_1.SendStatus.Delivered);
                });
            });
            it('does nothing when receiving a delivery receipt', () => {
                assertTransition(MessageSendState_1.SendStatus.Delivered, MessageSendState_1.SendActionType.GotDeliveryReceipt, MessageSendState_1.SendStatus.Delivered);
            });
            it('can go forward to other states', () => {
                assertTransition(MessageSendState_1.SendStatus.Delivered, MessageSendState_1.SendActionType.GotReadReceipt, MessageSendState_1.SendStatus.Read);
                assertTransition(MessageSendState_1.SendStatus.Delivered, MessageSendState_1.SendActionType.GotViewedReceipt, MessageSendState_1.SendStatus.Viewed);
            });
        });
        describe('transitions from Read', () => {
            it('does nothing when trying to go "backwards"', () => {
                [
                    MessageSendState_1.SendActionType.Failed,
                    MessageSendState_1.SendActionType.ManuallyRetried,
                    MessageSendState_1.SendActionType.Sent,
                    MessageSendState_1.SendActionType.GotDeliveryReceipt,
                ].forEach(type => {
                    assertTransition(MessageSendState_1.SendStatus.Read, type, MessageSendState_1.SendStatus.Read);
                });
            });
            it('does nothing when receiving a read receipt', () => {
                assertTransition(MessageSendState_1.SendStatus.Read, MessageSendState_1.SendActionType.GotReadReceipt, MessageSendState_1.SendStatus.Read);
            });
            it('can go forward to the "viewed" state', () => {
                assertTransition(MessageSendState_1.SendStatus.Read, MessageSendState_1.SendActionType.GotViewedReceipt, MessageSendState_1.SendStatus.Viewed);
            });
        });
        describe('transitions from Viewed', () => {
            it('ignores all actions', () => {
                [
                    MessageSendState_1.SendActionType.Failed,
                    MessageSendState_1.SendActionType.ManuallyRetried,
                    MessageSendState_1.SendActionType.Sent,
                    MessageSendState_1.SendActionType.GotDeliveryReceipt,
                    MessageSendState_1.SendActionType.GotReadReceipt,
                    MessageSendState_1.SendActionType.GotViewedReceipt,
                ].forEach(type => {
                    assertTransition(MessageSendState_1.SendStatus.Viewed, type, MessageSendState_1.SendStatus.Viewed);
                });
            });
        });
        describe('legacy transitions', () => {
            it('allows actions without timestamps', () => {
                const startState = {
                    status: MessageSendState_1.SendStatus.Pending,
                    updatedAt: Date.now(),
                };
                const action = {
                    type: MessageSendState_1.SendActionType.Sent,
                    updatedAt: undefined,
                };
                const result = (0, MessageSendState_1.sendStateReducer)(startState, action);
                chai_1.assert.isUndefined(result.updatedAt);
            });
        });
    });
});
