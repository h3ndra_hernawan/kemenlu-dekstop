"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const uuid_1 = require("uuid");
const getDefaultConversation_1 = require("../helpers/getDefaultConversation");
const MessageSendState_1 = require("../../messages/MessageSendState");
const migrateLegacySendAttributes_1 = require("../../messages/migrateLegacySendAttributes");
describe('migrateLegacySendAttributes', () => {
    const defaultMessage = {
        type: 'outgoing',
        sent_at: 123,
        sent: true,
    };
    const createGetConversation = (...conversations) => {
        const lookup = new Map();
        conversations.forEach(conversation => {
            [conversation.id, conversation.uuid, conversation.e164].forEach(property => {
                if (property) {
                    lookup.set(property, conversation);
                }
            });
        });
        return (id) => (id ? lookup.get(id) : undefined);
    };
    it("doesn't migrate messages that already have the modern send state", () => {
        const ourConversationId = (0, uuid_1.v4)();
        const message = Object.assign(Object.assign({}, defaultMessage), { sendStateByConversationId: {
                [ourConversationId]: {
                    status: MessageSendState_1.SendStatus.Sent,
                    updatedAt: 123,
                },
            } });
        const getConversation = () => undefined;
        chai_1.assert.isUndefined((0, migrateLegacySendAttributes_1.migrateLegacySendAttributes)(message, getConversation, ourConversationId));
    });
    it("doesn't migrate messages that aren't outgoing", () => {
        const ourConversationId = (0, uuid_1.v4)();
        const message = Object.assign(Object.assign({}, defaultMessage), { type: 'incoming' });
        const getConversation = () => undefined;
        chai_1.assert.isUndefined((0, migrateLegacySendAttributes_1.migrateLegacySendAttributes)(message, getConversation, ourConversationId));
    });
    it('advances the send state machine, starting from "pending", for different state types', () => {
        let e164Counter = 0;
        const getTestConversation = () => {
            const last4Digits = e164Counter.toString().padStart(4);
            chai_1.assert.strictEqual(last4Digits.length, 4, 'Test setup failure: E164 is too long');
            e164Counter += 1;
            return (0, getDefaultConversation_1.getDefaultConversation)({ e164: `+1999555${last4Digits}` });
        };
        // This is aliased for clarity.
        const ignoredUuid = uuid_1.v4;
        const failedConversationByUuid = getTestConversation();
        const failedConversationByE164 = getTestConversation();
        const pendingConversation = getTestConversation();
        const sentConversation = getTestConversation();
        const deliveredConversation = getTestConversation();
        const readConversation = getTestConversation();
        const conversationNotInRecipientsList = getTestConversation();
        const ourConversation = getTestConversation();
        const message = Object.assign(Object.assign({}, defaultMessage), { recipients: [
                failedConversationByUuid.uuid,
                failedConversationByE164.uuid,
                pendingConversation.uuid,
                sentConversation.uuid,
                deliveredConversation.uuid,
                readConversation.uuid,
                ignoredUuid(),
                ourConversation.uuid,
            ], errors: [
                Object.assign(new Error('looked up by UUID'), {
                    identifier: failedConversationByUuid.uuid,
                }),
                Object.assign(new Error('looked up by E164'), {
                    number: failedConversationByE164.e164,
                }),
                Object.assign(new Error('ignored error'), {
                    identifier: ignoredUuid(),
                }),
                new Error('a different error'),
            ], sent_to: [
                sentConversation.e164,
                conversationNotInRecipientsList.uuid,
                ignoredUuid(),
                ourConversation.uuid,
            ], delivered_to: [
                deliveredConversation.uuid,
                ignoredUuid(),
                ourConversation.uuid,
            ], read_by: [readConversation.uuid, ignoredUuid()] });
        const getConversation = createGetConversation(failedConversationByUuid, failedConversationByE164, pendingConversation, sentConversation, deliveredConversation, readConversation, conversationNotInRecipientsList, ourConversation);
        chai_1.assert.deepEqual((0, migrateLegacySendAttributes_1.migrateLegacySendAttributes)(message, getConversation, ourConversation.id), {
            [ourConversation.id]: {
                status: MessageSendState_1.SendStatus.Delivered,
                updatedAt: undefined,
            },
            [failedConversationByUuid.id]: {
                status: MessageSendState_1.SendStatus.Failed,
                updatedAt: undefined,
            },
            [failedConversationByE164.id]: {
                status: MessageSendState_1.SendStatus.Failed,
                updatedAt: undefined,
            },
            [pendingConversation.id]: {
                status: MessageSendState_1.SendStatus.Pending,
                updatedAt: message.sent_at,
            },
            [sentConversation.id]: {
                status: MessageSendState_1.SendStatus.Sent,
                updatedAt: undefined,
            },
            [conversationNotInRecipientsList.id]: {
                status: MessageSendState_1.SendStatus.Sent,
                updatedAt: undefined,
            },
            [deliveredConversation.id]: {
                status: MessageSendState_1.SendStatus.Delivered,
                updatedAt: undefined,
            },
            [readConversation.id]: {
                status: MessageSendState_1.SendStatus.Read,
                updatedAt: undefined,
            },
        });
    });
    it('considers our own conversation sent if the "sent" attribute is set', () => {
        var _a;
        const ourConversation = (0, getDefaultConversation_1.getDefaultConversation)();
        const conversation1 = (0, getDefaultConversation_1.getDefaultConversation)();
        const conversation2 = (0, getDefaultConversation_1.getDefaultConversation)();
        const message = Object.assign(Object.assign({}, defaultMessage), { recipients: [conversation1.id, conversation2.id], sent: true });
        const getConversation = createGetConversation(ourConversation, conversation1, conversation2);
        chai_1.assert.deepEqual((_a = (0, migrateLegacySendAttributes_1.migrateLegacySendAttributes)(message, getConversation, ourConversation.id)) === null || _a === void 0 ? void 0 : _a[ourConversation.id], {
            status: MessageSendState_1.SendStatus.Sent,
            updatedAt: undefined,
        });
    });
    it("considers our own conversation failed if the message isn't marked sent and we aren't elsewhere in the recipients list", () => {
        var _a;
        const ourConversation = (0, getDefaultConversation_1.getDefaultConversation)();
        const conversation1 = (0, getDefaultConversation_1.getDefaultConversation)();
        const conversation2 = (0, getDefaultConversation_1.getDefaultConversation)();
        const message = Object.assign(Object.assign({}, defaultMessage), { recipients: [conversation1.id, conversation2.id], sent: false });
        const getConversation = createGetConversation(ourConversation, conversation1, conversation2);
        chai_1.assert.deepEqual((_a = (0, migrateLegacySendAttributes_1.migrateLegacySendAttributes)(message, getConversation, ourConversation.id)) === null || _a === void 0 ? void 0 : _a[ourConversation.id], {
            status: MessageSendState_1.SendStatus.Failed,
            updatedAt: undefined,
        });
    });
    it('migrates a typical legacy note to self message', () => {
        const ourConversation = (0, getDefaultConversation_1.getDefaultConversation)();
        const message = Object.assign(Object.assign({}, defaultMessage), { conversationId: ourConversation.id, recipients: [], destination: ourConversation.uuid, sent_to: [ourConversation.uuid], sent: true, synced: true, unidentifiedDeliveries: [], delivered_to: [ourConversation.id], read_by: [ourConversation.id] });
        const getConversation = createGetConversation(ourConversation);
        chai_1.assert.deepEqual((0, migrateLegacySendAttributes_1.migrateLegacySendAttributes)(message, getConversation, ourConversation.id), {
            [ourConversation.id]: {
                status: MessageSendState_1.SendStatus.Read,
                updatedAt: undefined,
            },
        });
    });
});
