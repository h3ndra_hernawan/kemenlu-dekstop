"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const remoteConfig = __importStar(require("../../RemoteConfig"));
const limits_1 = require("../../groups/limits");
describe('group limit utilities', () => {
    let sinonSandbox;
    let getRecommendedLimitStub;
    let getHardLimitStub;
    beforeEach(() => {
        sinonSandbox = sinon.createSandbox();
        const getValueStub = sinonSandbox.stub(remoteConfig, 'getValue');
        getRecommendedLimitStub = getValueStub.withArgs('global.groupsv2.maxGroupSize');
        getHardLimitStub = getValueStub.withArgs('global.groupsv2.groupSizeHardLimit');
    });
    afterEach(() => {
        sinonSandbox.restore();
    });
    describe('getGroupSizeRecommendedLimit', () => {
        it('throws if the value in remote config is not defined', () => {
            getRecommendedLimitStub.returns(undefined);
            chai_1.assert.throws(limits_1.getGroupSizeRecommendedLimit);
        });
        it('throws if the value in remote config is not a parseable integer', () => {
            getRecommendedLimitStub.returns('uh oh');
            chai_1.assert.throws(limits_1.getGroupSizeRecommendedLimit);
        });
        it('returns the value in remote config, parsed as an integer', () => {
            getRecommendedLimitStub.returns('123');
            chai_1.assert.strictEqual((0, limits_1.getGroupSizeRecommendedLimit)(), 123);
        });
    });
    describe('getGroupSizeHardLimit', () => {
        it('throws if the value in remote config is not defined', () => {
            getHardLimitStub.returns(undefined);
            chai_1.assert.throws(limits_1.getGroupSizeHardLimit);
        });
        it('throws if the value in remote config is not a parseable integer', () => {
            getHardLimitStub.returns('uh oh');
            chai_1.assert.throws(limits_1.getGroupSizeHardLimit);
        });
        it('returns the value in remote config, parsed as an integer', () => {
            getHardLimitStub.returns('123');
            chai_1.assert.strictEqual((0, limits_1.getGroupSizeHardLimit)(), 123);
        });
    });
});
