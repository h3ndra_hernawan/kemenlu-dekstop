"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const replaceIndex_1 = require("../../util/replaceIndex");
describe('replaceIndex', () => {
    it('returns a new array with an index replaced', () => {
        const original = ['a', 'b', 'c', 'd'];
        const replaced = (0, replaceIndex_1.replaceIndex)(original, 2, 'X');
        chai_1.assert.deepStrictEqual(replaced, ['a', 'b', 'X', 'd']);
    });
    it("doesn't modify the original array", () => {
        const original = ['a', 'b', 'c', 'd'];
        (0, replaceIndex_1.replaceIndex)(original, 2, 'X');
        chai_1.assert.deepStrictEqual(original, ['a', 'b', 'c', 'd']);
    });
    it('throws if the index is out of range', () => {
        const original = ['a', 'b', 'c'];
        [-1, 1.2, 4, Infinity, NaN].forEach(index => {
            chai_1.assert.throws(() => {
                (0, replaceIndex_1.replaceIndex)(original, index, 'X');
            });
        });
    });
});
