"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isValidE164_1 = require("../../util/isValidE164");
describe('isValidE164', () => {
    it('returns false for non-strings', () => {
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)(undefined, false));
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)(18885551234, false));
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)(['+18885551234'], false));
    });
    it('returns false for invalid E164s', () => {
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)('', false));
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)('+05551234', false));
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)('+1800ENCRYPT', false));
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)('+1-888-555-1234', false));
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)('+1 (888) 555-1234', false));
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)('+1012345678901234', false));
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)('+18885551234extra', false));
    });
    it('returns true for E164s that look valid', () => {
        chai_1.assert.isTrue((0, isValidE164_1.isValidE164)('+18885551234', false));
        chai_1.assert.isTrue((0, isValidE164_1.isValidE164)('+123456789012', false));
        chai_1.assert.isTrue((0, isValidE164_1.isValidE164)('+12', false));
    });
    it('can make the leading + optional or required', () => {
        chai_1.assert.isTrue((0, isValidE164_1.isValidE164)('18885551234', false));
        chai_1.assert.isFalse((0, isValidE164_1.isValidE164)('18885551234', true));
    });
});
