"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const timestamp_1 = require("../../util/timestamp");
const ONE_HOUR = 3600 * 1000;
const ONE_DAY = 24 * ONE_HOUR;
describe('timestamp', () => {
    describe('isOlderThan', () => {
        it('returns false on recent and future timestamps', () => {
            chai_1.assert.isFalse((0, timestamp_1.isOlderThan)(Date.now(), ONE_DAY));
            chai_1.assert.isFalse((0, timestamp_1.isOlderThan)(Date.now() + ONE_DAY, ONE_DAY));
        });
        it('returns true on old enough timestamps', () => {
            chai_1.assert.isFalse((0, timestamp_1.isOlderThan)(Date.now() - ONE_DAY + ONE_HOUR, ONE_DAY));
            chai_1.assert.isTrue((0, timestamp_1.isOlderThan)(Date.now() - ONE_DAY - ONE_HOUR, ONE_DAY));
        });
    });
    describe('isMoreRecentThan', () => {
        it('returns true on recent and future timestamps', () => {
            chai_1.assert.isTrue((0, timestamp_1.isMoreRecentThan)(Date.now(), ONE_DAY));
            chai_1.assert.isTrue((0, timestamp_1.isMoreRecentThan)(Date.now() + ONE_DAY, ONE_DAY));
        });
        it('returns false on old enough timestamps', () => {
            chai_1.assert.isTrue((0, timestamp_1.isMoreRecentThan)(Date.now() - ONE_DAY + ONE_HOUR, ONE_DAY));
            chai_1.assert.isFalse((0, timestamp_1.isMoreRecentThan)(Date.now() - ONE_DAY - ONE_HOUR, ONE_DAY));
        });
    });
    describe('toDayMillis', () => {
        const now = new Date();
        const today = new Date((0, timestamp_1.toDayMillis)(now.valueOf()));
        chai_1.assert.strictEqual(today.getUTCMilliseconds(), 0);
        chai_1.assert.strictEqual(today.getUTCHours(), 0);
        chai_1.assert.strictEqual(today.getUTCMinutes(), 0);
        chai_1.assert.strictEqual(today.getUTCSeconds(), 0);
        chai_1.assert.strictEqual(today.getUTCDate(), now.getUTCDate());
        chai_1.assert.strictEqual(today.getUTCMonth(), now.getUTCMonth());
        chai_1.assert.strictEqual(today.getUTCFullYear(), now.getUTCFullYear());
    });
});
