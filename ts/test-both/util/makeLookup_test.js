"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const makeLookup_1 = require("../../util/makeLookup");
describe('makeLookup', () => {
    it('returns an empty object if passed an empty array', () => {
        const result = (0, makeLookup_1.makeLookup)([], 'foo');
        chai_1.assert.deepEqual(result, {});
    });
    it('returns an object that lets you look up objects by string key', () => {
        const arr = [{ foo: 'bar' }, { foo: 'baz' }, { foo: 'qux' }];
        const result = (0, makeLookup_1.makeLookup)(arr, 'foo');
        chai_1.assert.hasAllKeys(result, ['bar', 'baz', 'qux']);
        chai_1.assert.strictEqual(result.bar, arr[0]);
        chai_1.assert.strictEqual(result.baz, arr[1]);
        chai_1.assert.strictEqual(result.qux, arr[2]);
    });
    it('if there are duplicates, the last one wins', () => {
        const arr = [
            { foo: 'bar', first: true },
            { foo: 'bar', first: false },
        ];
        const result = (0, makeLookup_1.makeLookup)(arr, 'foo');
        chai_1.assert.deepEqual(result, {
            bar: { foo: 'bar', first: false },
        });
    });
    it('ignores undefined properties', () => {
        const arr = [{}, { foo: undefined }];
        const result = (0, makeLookup_1.makeLookup)(arr, 'foo');
        chai_1.assert.deepEqual(result, {});
    });
    it('allows key of 0', () => {
        const arr = [{}, { id: 0 }, { id: 1 }, { id: 2 }];
        const result = (0, makeLookup_1.makeLookup)(arr, 'id');
        chai_1.assert.deepEqual(result, {
            0: { id: 0 },
            1: { id: 1 },
            2: { id: 2 },
        });
    });
    it('converts the lookup to a string', () => {
        const arr = [
            { foo: 'bar' },
            { foo: 123 },
            { foo: {} },
            {
                foo: {
                    toString() {
                        return 'baz';
                    },
                },
            },
            {},
        ];
        const result = (0, makeLookup_1.makeLookup)(arr, 'foo');
        chai_1.assert.hasAllKeys(result, ['bar', '123', '[object Object]', 'baz']);
        chai_1.assert.strictEqual(result.bar, arr[0]);
        chai_1.assert.strictEqual(result['123'], arr[1]);
        chai_1.assert.strictEqual(result['[object Object]'], arr[2]);
        chai_1.assert.strictEqual(result.baz, arr[3]);
    });
    it('looks up own and inherited properties', () => {
        const prototype = { foo: 'baz' };
        const arr = [{ foo: 'bar' }, Object.create(prototype)];
        const result = (0, makeLookup_1.makeLookup)(arr, 'foo');
        chai_1.assert.strictEqual(result.bar, arr[0]);
        chai_1.assert.strictEqual(result.baz, arr[1]);
    });
});
