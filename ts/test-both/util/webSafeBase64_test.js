"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const webSafeBase64_1 = require("../../util/webSafeBase64");
describe('both/util/webSafeBase64', () => {
    it('roundtrips with all elements', () => {
        const base64 = 'X0KjoAj3h7Tu9YjJ++PamFc4kAg//D4FKommANpP41I=';
        const webSafe = (0, webSafeBase64_1.toWebSafeBase64)(base64);
        const actual = (0, webSafeBase64_1.fromWebSafeBase64)(webSafe);
        chai_1.assert.strictEqual(base64, actual);
    });
    describe('#toWebSafeBase64', () => {
        it('replaces +', () => {
            const base64 = 'X++y';
            const expected = 'X--y';
            const actual = (0, webSafeBase64_1.toWebSafeBase64)(base64);
            chai_1.assert.strictEqual(expected, actual);
        });
        it('replaces /', () => {
            const base64 = 'X//y';
            const expected = 'X__y';
            const actual = (0, webSafeBase64_1.toWebSafeBase64)(base64);
            chai_1.assert.strictEqual(expected, actual);
        });
        it('removes =', () => {
            const base64 = 'X===';
            const expected = 'X';
            const actual = (0, webSafeBase64_1.toWebSafeBase64)(base64);
            chai_1.assert.strictEqual(expected, actual);
        });
    });
    describe('#fromWebSafeBase64', () => {
        it('replaces -', () => {
            const webSafeBase64 = 'X--y';
            const expected = 'X++y';
            const actual = (0, webSafeBase64_1.fromWebSafeBase64)(webSafeBase64);
            chai_1.assert.strictEqual(expected, actual);
        });
        it('replaces _', () => {
            const webSafeBase64 = 'X__y';
            const expected = 'X//y';
            const actual = (0, webSafeBase64_1.fromWebSafeBase64)(webSafeBase64);
            chai_1.assert.strictEqual(expected, actual);
        });
        it('adds ===', () => {
            const webSafeBase64 = 'X';
            const expected = 'X===';
            const actual = (0, webSafeBase64_1.fromWebSafeBase64)(webSafeBase64);
            chai_1.assert.strictEqual(expected, actual);
        });
        it('adds ==', () => {
            const webSafeBase64 = 'Xy';
            const expected = 'Xy==';
            const actual = (0, webSafeBase64_1.fromWebSafeBase64)(webSafeBase64);
            chai_1.assert.strictEqual(expected, actual);
        });
        it('adds =', () => {
            const webSafeBase64 = 'XyZ';
            const expected = 'XyZ=';
            const actual = (0, webSafeBase64_1.fromWebSafeBase64)(webSafeBase64);
            chai_1.assert.strictEqual(expected, actual);
        });
    });
});
