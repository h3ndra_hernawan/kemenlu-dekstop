"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const stream_1 = require("stream");
const sinon = __importStar(require("sinon"));
const lodash_1 = require("lodash");
const getStreamWithTimeout_1 = require("../../util/getStreamWithTimeout");
describe('getStreamWithTimeout', () => {
    let sandbox;
    let clock;
    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sandbox.useFakeTimers();
    });
    afterEach(() => {
        sandbox.restore();
    });
    it('resolves on finished stream', async () => {
        const stream = new stream_1.Readable({
            read: lodash_1.noop,
        });
        stream.push('hello');
        stream.push(' ');
        stream.push('world');
        stream.push(null);
        const abort = sinon.stub();
        const data = await (0, getStreamWithTimeout_1.getStreamWithTimeout)(stream, {
            name: 'test',
            timeout: 1000,
            abortController: { abort },
        });
        chai_1.assert.strictEqual(Buffer.from(data).toString(), 'hello world');
        sinon.assert.notCalled(abort);
    });
    it('does not timeout on slow but steady stream', async () => {
        const stream = new stream_1.Readable({
            read: lodash_1.noop,
        });
        const abort = sinon.stub();
        const data = (0, getStreamWithTimeout_1.getStreamWithTimeout)(stream, {
            name: 'test',
            timeout: 1000,
            abortController: { abort },
        });
        await clock.tickAsync(500);
        stream.push('hello ');
        await clock.tickAsync(500);
        stream.push('world');
        await clock.tickAsync(500);
        stream.push(null);
        await clock.nextAsync();
        chai_1.assert.strictEqual(Buffer.from(await data).toString(), 'hello world');
        sinon.assert.notCalled(abort);
    });
    it('does timeout on slow but unsteady stream', async () => {
        const stream = new stream_1.Readable({
            read: lodash_1.noop,
        });
        const abort = sinon.stub();
        const data = (0, getStreamWithTimeout_1.getStreamWithTimeout)(stream, {
            name: 'test',
            timeout: 1000,
            abortController: { abort },
        });
        await clock.tickAsync(500);
        stream.push('hello ');
        await clock.tickAsync(500);
        stream.push('world');
        const promise = chai_1.assert.isRejected(data, 'getStreamWithTimeout(test) timed out');
        await clock.tickAsync(1000);
        await promise;
        sinon.assert.called(abort);
    });
    it('rejects on timeout', async () => {
        const stream = new stream_1.Readable({
            read: lodash_1.noop,
        });
        const abort = sinon.stub();
        const promise = chai_1.assert.isRejected((0, getStreamWithTimeout_1.getStreamWithTimeout)(stream, {
            name: 'test',
            timeout: 1000,
            abortController: { abort },
        }), 'getStreamWithTimeout(test) timed out');
        await clock.tickAsync(1000);
        await promise;
        sinon.assert.called(abort);
    });
    it('rejects on stream error', async () => {
        const stream = new stream_1.Readable({
            read: lodash_1.noop,
        });
        const abort = sinon.stub();
        const promise = chai_1.assert.isRejected((0, getStreamWithTimeout_1.getStreamWithTimeout)(stream, {
            name: 'test',
            timeout: 1000,
            abortController: { abort },
        }), 'welp');
        stream.emit('error', new Error('welp'));
        await promise;
        sinon.assert.notCalled(abort);
    });
});
