"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const iterables_1 = require("../../util/iterables");
const url_1 = require("../../util/url");
describe('URL utilities', () => {
    describe('maybeParseUrl', () => {
        it('parses valid URLs', () => {
            [
                'https://example.com',
                'https://example.com:123/pathname?query=string#hash',
                'file:///path/to/file.txt',
            ].forEach(href => {
                chai_1.assert.deepEqual((0, url_1.maybeParseUrl)(href), new URL(href));
            });
        });
        it('returns undefined for invalid URLs', () => {
            ['', 'example.com'].forEach(href => {
                chai_1.assert.isUndefined((0, url_1.maybeParseUrl)(href));
            });
        });
        it('handles non-strings for compatibility, returning undefined', () => {
            [undefined, null, 123, ['https://example.com']].forEach(value => {
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                chai_1.assert.isUndefined((0, url_1.maybeParseUrl)(value));
            });
        });
    });
    describe('setUrlSearchParams', () => {
        it('returns a new URL with updated search params', () => {
            const params = {
                normal_string: 'foo',
                empty_string: '',
                number: 123,
                true_bool: true,
                false_bool: false,
                null_value: null,
                undefined_value: undefined,
                array: ['ok', 'wow'],
                stringified: { toString: () => 'bar' },
            };
            const newUrl = (0, url_1.setUrlSearchParams)(new URL('https://example.com/path?should_be=overwritten#hash'), params);
            (0, chai_1.assert)(newUrl.href.startsWith('https://example.com/path?'));
            chai_1.assert.strictEqual(newUrl.hash, '#hash');
            chai_1.assert.strictEqual((0, iterables_1.size)(newUrl.searchParams.entries()), Object.keys(params).length);
            chai_1.assert.strictEqual(newUrl.searchParams.get('normal_string'), 'foo');
            chai_1.assert.strictEqual(newUrl.searchParams.get('empty_string'), '');
            chai_1.assert.strictEqual(newUrl.searchParams.get('number'), '123');
            chai_1.assert.strictEqual(newUrl.searchParams.get('true_bool'), 'true');
            chai_1.assert.strictEqual(newUrl.searchParams.get('false_bool'), 'false');
            chai_1.assert.strictEqual(newUrl.searchParams.get('null_value'), '');
            chai_1.assert.strictEqual(newUrl.searchParams.get('undefined_value'), '');
            chai_1.assert.strictEqual(newUrl.searchParams.get('array'), 'ok,wow');
            chai_1.assert.strictEqual(newUrl.searchParams.get('stringified'), 'bar');
        });
        it("doesn't touch the original URL or its params", () => {
            const originalHref = 'https://example.com/path?query=string';
            const originalUrl = new URL(originalHref);
            const params = { foo: 'bar' };
            const newUrl = (0, url_1.setUrlSearchParams)(originalUrl, params);
            chai_1.assert.notStrictEqual(originalUrl, newUrl);
            chai_1.assert.strictEqual(originalUrl.href, originalHref);
            params.foo = 'should be ignored';
            chai_1.assert.strictEqual(newUrl.search, '?foo=bar');
        });
    });
});
