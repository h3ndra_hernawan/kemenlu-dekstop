"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isConversationSMSOnly_1 = require("../../util/isConversationSMSOnly");
describe('isConversationSMSOnly', () => {
    it('returns false if passed an undefined type', () => {
        chai_1.assert.isFalse((0, isConversationSMSOnly_1.isConversationSMSOnly)({
            type: undefined,
        }));
    });
    ['direct', 'private'].forEach(type => {
        it('returns false if passed an undefined discoveredUnregisteredAt', () => {
            chai_1.assert.isFalse((0, isConversationSMSOnly_1.isConversationSMSOnly)({ type, discoveredUnregisteredAt: undefined }));
        });
        it('returns true if passed a very old discoveredUnregisteredAt', () => {
            chai_1.assert.isTrue((0, isConversationSMSOnly_1.isConversationSMSOnly)({
                type,
                e164: 'e164',
                uuid: 'uuid',
                discoveredUnregisteredAt: 1,
            }));
        });
        it(`returns true if passed a time fewer than 6 hours ago and is ${type}`, () => {
            chai_1.assert.isTrue((0, isConversationSMSOnly_1.isConversationSMSOnly)({
                type,
                e164: 'e164',
                uuid: 'uuid',
                discoveredUnregisteredAt: Date.now(),
            }));
            const fiveHours = 1000 * 60 * 60 * 5;
            chai_1.assert.isTrue((0, isConversationSMSOnly_1.isConversationSMSOnly)({
                type,
                e164: 'e164',
                uuid: 'uuid',
                discoveredUnregisteredAt: Date.now() - fiveHours,
            }));
        });
        it(`returns true conversation is ${type} and has no uuid`, () => {
            chai_1.assert.isTrue((0, isConversationSMSOnly_1.isConversationSMSOnly)({ type, e164: 'e164' }));
            chai_1.assert.isFalse((0, isConversationSMSOnly_1.isConversationSMSOnly)({ type }));
        });
    });
    it('returns false for groups', () => {
        chai_1.assert.isFalse((0, isConversationSMSOnly_1.isConversationSMSOnly)({ type: 'group' }));
    });
});
