"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const parseIntOrThrow_1 = require("../../util/parseIntOrThrow");
describe('parseIntOrThrow', () => {
    describe('when passed a number argument', () => {
        it('returns the number when passed an integer', () => {
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)(0, "shouldn't happen"), 0);
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)(123, "shouldn't happen"), 123);
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)(-123, "shouldn't happen"), -123);
        });
        it('throws when passed a decimal value', () => {
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)(0.2, 'uh oh'), 'uh oh');
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)(1.23, 'uh oh'), 'uh oh');
        });
        it('throws when passed NaN', () => {
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)(NaN, 'uh oh'), 'uh oh');
        });
        it('throws when passed ∞', () => {
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)(Infinity, 'uh oh'), 'uh oh');
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)(-Infinity, 'uh oh'), 'uh oh');
        });
    });
    describe('when passed a string argument', () => {
        it('returns the number when passed an integer', () => {
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)('0', "shouldn't happen"), 0);
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)('123', "shouldn't happen"), 123);
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)('-123', "shouldn't happen"), -123);
        });
        it('parses decimal values like parseInt', () => {
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)('0.2', "shouldn't happen"), 0);
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)('12.34', "shouldn't happen"), 12);
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)('-12.34', "shouldn't happen"), -12);
        });
        it('parses values in base 10', () => {
            chai_1.assert.strictEqual((0, parseIntOrThrow_1.parseIntOrThrow)('0x12', "shouldn't happen"), 0);
        });
        it('throws when passed non-parseable strings', () => {
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)('', 'uh oh'), 'uh oh');
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)('uh 123', 'uh oh'), 'uh oh');
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)('uh oh', 'uh oh'), 'uh oh');
        });
    });
    describe('when passed other arguments', () => {
        it("throws when passed arguments that aren't strings or numbers", () => {
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)(null, 'uh oh'), 'uh oh');
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)(undefined, 'uh oh'), 'uh oh');
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)(['123'], 'uh oh'), 'uh oh');
        });
        it('throws when passed a stringifiable argument, unlike parseInt', () => {
            const obj = {
                toString() {
                    return '123';
                },
            };
            chai_1.assert.throws(() => (0, parseIntOrThrow_1.parseIntOrThrow)(obj, 'uh oh'), 'uh oh');
        });
    });
});
