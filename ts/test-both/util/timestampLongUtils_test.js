"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const long_1 = __importDefault(require("long"));
const timestampLongUtils_1 = require("../../util/timestampLongUtils");
describe('getSafeLongFromTimestamp', () => {
    it('returns zero when passed undefined', () => {
        (0, chai_1.assert)((0, timestampLongUtils_1.getSafeLongFromTimestamp)(undefined).isZero());
    });
    it('returns the number as a Long when passed a "normal" number', () => {
        (0, chai_1.assert)((0, timestampLongUtils_1.getSafeLongFromTimestamp)(0).isZero());
        chai_1.assert.strictEqual((0, timestampLongUtils_1.getSafeLongFromTimestamp)(123).toString(), '123');
        chai_1.assert.strictEqual((0, timestampLongUtils_1.getSafeLongFromTimestamp)(-456).toString(), '-456');
    });
    it('returns Long.MAX_VALUE when passed Infinity', () => {
        (0, chai_1.assert)((0, timestampLongUtils_1.getSafeLongFromTimestamp)(Infinity).equals(long_1.default.MAX_VALUE));
    });
    it("returns Long.MAX_VALUE when passed very large numbers, outside of JavaScript's safely representable range", () => {
        chai_1.assert.equal((0, timestampLongUtils_1.getSafeLongFromTimestamp)(Number.MAX_VALUE), long_1.default.MAX_VALUE);
    });
});
describe('getTimestampFromLong', () => {
    it('returns zero when passed 0 Long', () => {
        chai_1.assert.equal((0, timestampLongUtils_1.getTimestampFromLong)(long_1.default.fromNumber(0)), 0);
    });
    it('returns Number.MAX_SAFE_INTEGER when passed Long.MAX_VALUE', () => {
        chai_1.assert.equal((0, timestampLongUtils_1.getTimestampFromLong)(long_1.default.MAX_VALUE), Number.MAX_SAFE_INTEGER);
    });
    it('returns a normal number', () => {
        chai_1.assert.equal((0, timestampLongUtils_1.getTimestampFromLong)(long_1.default.fromNumber(16)), 16);
    });
    it('returns 0 for null value', () => {
        chai_1.assert.equal((0, timestampLongUtils_1.getTimestampFromLong)(null), 0);
    });
});
