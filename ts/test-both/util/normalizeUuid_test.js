"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const uuid_1 = require("uuid");
const normalizeUuid_1 = require("../../util/normalizeUuid");
describe('normalizeUuid', () => {
    it('converts uuid to lower case', () => {
        const uuid = (0, uuid_1.v4)();
        chai_1.assert.strictEqual((0, normalizeUuid_1.normalizeUuid)(uuid, 'context 1'), uuid);
        chai_1.assert.strictEqual((0, normalizeUuid_1.normalizeUuid)(uuid.toUpperCase(), 'context 2'), uuid);
    });
    it("throws if passed a string that's not a UUID", () => {
        chai_1.assert.throws(() => (0, normalizeUuid_1.normalizeUuid)('not-uuid-at-all', 'context 3'), 'Normalizing invalid uuid: not-uuid-at-all in context "context 3"');
    });
});
