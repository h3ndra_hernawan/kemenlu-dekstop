"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai = __importStar(require("chai"));
const assert_1 = require("../../util/assert");
describe('assert utilities', () => {
    describe('assert', () => {
        it('does nothing if the assertion passes', () => {
            (0, assert_1.assert)(true, 'foo bar');
        });
        it("throws if the assertion fails, because we're in a test environment", () => {
            chai.assert.throws(() => {
                (0, assert_1.assert)(false, 'foo bar');
            }, 'foo bar');
        });
    });
    describe('strictAssert', () => {
        it('does nothing if the assertion passes', () => {
            (0, assert_1.strictAssert)(true, 'foo bar');
        });
        it('throws if the assertion fails', () => {
            chai.assert.throws(() => {
                (0, assert_1.strictAssert)(false, 'foo bar');
            }, 'foo bar');
        });
    });
});
