"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const memoizeByRoot_1 = require("../../util/memoizeByRoot");
class Root {
}
describe('memoizeByRoot', () => {
    it('should memoize by last passed arguments', () => {
        const root = new Root();
        const stub = sinon.stub();
        stub.withArgs(sinon.match.same(root), 1).returns(1);
        stub.withArgs(sinon.match.same(root), 2).returns(2);
        const fn = (0, memoizeByRoot_1.memoizeByRoot)(stub);
        chai_1.assert.strictEqual(fn(root, 1), 1);
        chai_1.assert.strictEqual(fn(root, 1), 1);
        chai_1.assert.isTrue(stub.calledOnce);
        chai_1.assert.strictEqual(fn(root, 2), 2);
        chai_1.assert.strictEqual(fn(root, 2), 2);
        chai_1.assert.isTrue(stub.calledTwice);
        chai_1.assert.strictEqual(fn(root, 1), 1);
        chai_1.assert.strictEqual(fn(root, 1), 1);
        chai_1.assert.isTrue(stub.calledThrice);
    });
    it('should memoize results by root', () => {
        const rootA = new Root();
        const rootB = new Root();
        const stub = sinon.stub();
        stub.withArgs(sinon.match.same(rootA), 1).returns(1);
        stub.withArgs(sinon.match.same(rootA), 2).returns(2);
        stub.withArgs(sinon.match.same(rootB), 1).returns(3);
        stub.withArgs(sinon.match.same(rootB), 2).returns(4);
        const fn = (0, memoizeByRoot_1.memoizeByRoot)(stub);
        chai_1.assert.strictEqual(fn(rootA, 1), 1);
        chai_1.assert.strictEqual(fn(rootB, 1), 3);
        chai_1.assert.strictEqual(fn(rootA, 1), 1);
        chai_1.assert.strictEqual(fn(rootB, 1), 3);
        chai_1.assert.isTrue(stub.calledTwice);
        chai_1.assert.strictEqual(fn(rootA, 2), 2);
        chai_1.assert.strictEqual(fn(rootB, 2), 4);
        chai_1.assert.strictEqual(fn(rootA, 2), 2);
        chai_1.assert.strictEqual(fn(rootB, 2), 4);
        chai_1.assert.strictEqual(stub.callCount, 4);
        chai_1.assert.strictEqual(fn(rootA, 1), 1);
        chai_1.assert.strictEqual(fn(rootB, 1), 3);
        chai_1.assert.strictEqual(stub.callCount, 6);
    });
});
