"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const grapheme_1 = require("../../util/grapheme");
describe('grapheme utilities', () => {
    describe('getGraphemes', () => {
        it('returns extended graphemes in a string', () => {
            chai_1.assert.deepEqual([...(0, grapheme_1.getGraphemes)('')], []);
            chai_1.assert.deepEqual([...(0, grapheme_1.getGraphemes)('hello')], [...'hello']);
            chai_1.assert.deepEqual([...(0, grapheme_1.getGraphemes)('Bokmål')], ['B', 'o', 'k', 'm', 'å', 'l']);
            chai_1.assert.deepEqual([...(0, grapheme_1.getGraphemes)('💩💩💩')], ['💩', '💩', '💩']);
            chai_1.assert.deepEqual([...(0, grapheme_1.getGraphemes)('👩‍❤️‍👩')], ['👩‍❤️‍👩']);
            chai_1.assert.deepEqual([...(0, grapheme_1.getGraphemes)('👌🏽👌🏾👌🏿')], ['👌🏽', '👌🏾', '👌🏿']);
            chai_1.assert.deepEqual([...(0, grapheme_1.getGraphemes)('L̷̳͔̲͝Ģ̵̮̯̤̩̙͍̬̟͉̹̘̹͍͈̮̦̰̣͟͝O̶̴̮̻̮̗͘͡!̴̷̟͓͓')], ['L̷̳͔̲͝', 'Ģ̵̮̯̤̩̙͍̬̟͉̹̘̹͍͈̮̦̰̣͟͝', 'O̶̴̮̻̮̗͘͡', '!̴̷̟͓͓']);
        });
    });
    describe('count', () => {
        it('returns the number of extended graphemes in a string (not necessarily the length)', () => {
            // These tests modified [from iOS][0].
            // [0]: https://github.com/signalapp/Signal-iOS/blob/800930110b0386a4c351716c001940a3e8fac942/Signal/test/util/DisplayableTextFilterTest.swift#L40-L71
            // Plain text
            chai_1.assert.strictEqual((0, grapheme_1.count)(''), 0);
            chai_1.assert.strictEqual((0, grapheme_1.count)('boring text'), 11);
            chai_1.assert.strictEqual((0, grapheme_1.count)('Bokmål'), 6);
            // Emojis
            chai_1.assert.strictEqual((0, grapheme_1.count)('💩💩💩'), 3);
            chai_1.assert.strictEqual((0, grapheme_1.count)('👩‍❤️‍👩'), 1);
            chai_1.assert.strictEqual((0, grapheme_1.count)('🇹🇹🌼🇹🇹🌼🇹🇹'), 5);
            chai_1.assert.strictEqual((0, grapheme_1.count)('🇹🇹'), 1);
            chai_1.assert.strictEqual((0, grapheme_1.count)('🇹🇹 '), 2);
            chai_1.assert.strictEqual((0, grapheme_1.count)('👌🏽👌🏾👌🏿'), 3);
            chai_1.assert.strictEqual((0, grapheme_1.count)('😍'), 1);
            chai_1.assert.strictEqual((0, grapheme_1.count)('👩🏽'), 1);
            chai_1.assert.strictEqual((0, grapheme_1.count)('👾🙇💁🙅🙆🙋🙎🙍'), 8);
            chai_1.assert.strictEqual((0, grapheme_1.count)('🐵🙈🙉🙊'), 4);
            chai_1.assert.strictEqual((0, grapheme_1.count)('❤️💔💌💕💞💓💗💖💘💝💟💜💛💚💙'), 15);
            chai_1.assert.strictEqual((0, grapheme_1.count)('✋🏿💪🏿👐🏿🙌🏿👏🏿🙏🏿'), 6);
            chai_1.assert.strictEqual((0, grapheme_1.count)('🚾🆒🆓🆕🆖🆗🆙🏧'), 8);
            chai_1.assert.strictEqual((0, grapheme_1.count)('0️⃣1️⃣2️⃣3️⃣4️⃣5️⃣6️⃣7️⃣8️⃣9️⃣🔟'), 11);
            chai_1.assert.strictEqual((0, grapheme_1.count)('🇺🇸🇷🇺🇦🇫🇦🇲'), 4);
            chai_1.assert.strictEqual((0, grapheme_1.count)('🇺🇸🇷🇺🇸 🇦🇫🇦🇲🇸'), 7);
            chai_1.assert.strictEqual((0, grapheme_1.count)('🇺🇸🇷🇺🇸🇦🇫🇦🇲'), 5);
            chai_1.assert.strictEqual((0, grapheme_1.count)('🇺🇸🇷🇺🇸🇦'), 3);
            chai_1.assert.strictEqual((0, grapheme_1.count)('１２３'), 3);
            // Normal diacritic usage
            chai_1.assert.strictEqual((0, grapheme_1.count)('Příliš žluťoučký kůň úpěl ďábelské ódy.'), 39);
            // Excessive diacritics
            chai_1.assert.strictEqual((0, grapheme_1.count)('Z͑ͫ̓ͪ̂ͫ̽͏̴̙̤̞͉͚̯̞̠͍A̴̵̜̰͔ͫ͗͢L̠ͨͧͩ͘G̴̻͈͍͔̹̑͗̎̅͛́Ǫ̵̹̻̝̳͂̌̌͘'), 5);
            chai_1.assert.strictEqual((0, grapheme_1.count)('H҉̸̧͘͠A͢͞V̛̛I̴̸N͏̕͏G҉̵͜͏͢ ̧̧́T̶̛͘͡R̸̵̨̢̀O̷̡U͡҉B̶̛͢͞L̸̸͘͢͟É̸ ̸̛͘͏R͟È͠͞A̸͝Ḑ̕͘͜I̵͘҉͜͞N̷̡̢͠G̴͘͠ ͟͞T͏̢́͡È̀X̕҉̢̀T̢͠?̕͏̢͘͢'), 28);
            chai_1.assert.strictEqual((0, grapheme_1.count)('L̷̳͔̲͝Ģ̵̮̯̤̩̙͍̬̟͉̹̘̹͍͈̮̦̰̣͟͝O̶̴̮̻̮̗͘͡!̴̷̟͓͓'), 4);
        });
    });
});
