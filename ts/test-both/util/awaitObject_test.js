"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const awaitObject_1 = require("../../util/awaitObject");
describe('awaitObject', () => {
    it('returns correct result', async () => {
        chai_1.assert.deepStrictEqual(await (0, awaitObject_1.awaitObject)({
            a: Promise.resolve(1),
            b: Promise.resolve('b'),
            c: Promise.resolve(null),
        }), {
            a: 1,
            b: 'b',
            c: null,
        });
    });
});
