"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon_1 = __importDefault(require("sinon"));
const retryPlaceholders_1 = require("../../util/retryPlaceholders");
/* eslint-disable @typescript-eslint/no-explicit-any */
describe('RetryPlaceholders', () => {
    const NOW = 1000000;
    let clock;
    beforeEach(() => {
        window.storage.put(retryPlaceholders_1.STORAGE_KEY, undefined);
        clock = sinon_1.default.useFakeTimers({
            now: NOW,
        });
    });
    afterEach(() => {
        clock.restore();
    });
    function getDefaultItem() {
        return {
            conversationId: 'conversation-id',
            sentAt: NOW - 10,
            receivedAt: NOW - 5,
            receivedAtCounter: 4,
            senderUuid: 'sender-uuid',
        };
    }
    describe('constructor', () => {
        it('loads previously-saved data on creation', () => {
            const items = [
                getDefaultItem(),
                Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-2' }),
            ];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(2, placeholders.getCount());
        });
        it('starts with no data if provided data fails to parse', () => {
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, [
                { item: 'is wrong shape!' },
                { bad: 'is not good!' },
            ]);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(0, placeholders.getCount());
        });
    });
    describe('#add', () => {
        it('adds one item', async () => {
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            await placeholders.add(getDefaultItem());
            chai_1.assert.strictEqual(1, placeholders.getCount());
        });
        it('throws if provided data fails to parse', () => {
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.isRejected(placeholders.add({
                item: 'is wrong shape!',
            }), 'Item did not match schema');
        });
    });
    describe('#getNextToExpire', () => {
        it('returns nothing if no items', () => {
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(0, placeholders.getCount());
            chai_1.assert.isUndefined(placeholders.getNextToExpire());
        });
        it('returns only item if just one item', () => {
            const item = getDefaultItem();
            const items = [item];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(1, placeholders.getCount());
            chai_1.assert.deepEqual(item, placeholders.getNextToExpire());
        });
        it('returns soonest expiration given a list, and after add', async () => {
            const older = Object.assign(Object.assign({}, getDefaultItem()), { receivedAt: NOW });
            const newer = Object.assign(Object.assign({}, getDefaultItem()), { receivedAt: NOW + 10 });
            const items = [older, newer];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(2, placeholders.getCount());
            chai_1.assert.deepEqual(older, placeholders.getNextToExpire());
            const oldest = Object.assign(Object.assign({}, getDefaultItem()), { receivedAt: NOW - 5 });
            await placeholders.add(oldest);
            chai_1.assert.strictEqual(3, placeholders.getCount());
            chai_1.assert.deepEqual(oldest, placeholders.getNextToExpire());
        });
    });
    describe('#getExpiredAndRemove', () => {
        it('does nothing if no item expired', async () => {
            const older = Object.assign(Object.assign({}, getDefaultItem()), { receivedAt: NOW + 10 });
            const newer = Object.assign(Object.assign({}, getDefaultItem()), { receivedAt: NOW + 15 });
            const items = [older, newer];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(2, placeholders.getCount());
            chai_1.assert.deepEqual([], await placeholders.getExpiredAndRemove());
            chai_1.assert.strictEqual(2, placeholders.getCount());
        });
        it('removes just one if expired', async () => {
            const older = Object.assign(Object.assign({}, getDefaultItem()), { receivedAt: (0, retryPlaceholders_1.getDeltaIntoPast)() - 1000 });
            const newer = Object.assign(Object.assign({}, getDefaultItem()), { receivedAt: NOW + 15 });
            const items = [older, newer];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(2, placeholders.getCount());
            chai_1.assert.deepEqual([older], await placeholders.getExpiredAndRemove());
            chai_1.assert.strictEqual(1, placeholders.getCount());
            chai_1.assert.deepEqual(newer, placeholders.getNextToExpire());
        });
        it('removes all if expired', async () => {
            const older = Object.assign(Object.assign({}, getDefaultItem()), { receivedAt: (0, retryPlaceholders_1.getDeltaIntoPast)() - 1000 });
            const newer = Object.assign(Object.assign({}, getDefaultItem()), { receivedAt: (0, retryPlaceholders_1.getDeltaIntoPast)() - 900 });
            const items = [older, newer];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(2, placeholders.getCount());
            chai_1.assert.deepEqual([older, newer], await placeholders.getExpiredAndRemove());
            chai_1.assert.strictEqual(0, placeholders.getCount());
        });
    });
    describe('#findByConversationAndMarkOpened', () => {
        it('does nothing if no items found matching conversation', async () => {
            const older = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-1' });
            const newer = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-2' });
            const items = [older, newer];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(2, placeholders.getCount());
            await placeholders.findByConversationAndMarkOpened('conversation-id-3');
            chai_1.assert.strictEqual(2, placeholders.getCount());
            const saveItems = window.storage.get(retryPlaceholders_1.STORAGE_KEY);
            chai_1.assert.deepEqual([older, newer], saveItems);
        });
        it('updates all items matching conversation', async () => {
            const convo1a = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-1', receivedAt: NOW - 5 });
            const convo1b = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-1', receivedAt: NOW - 4 });
            const convo2a = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-2', receivedAt: NOW + 15 });
            const items = [convo1a, convo1b, convo2a];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(3, placeholders.getCount());
            await placeholders.findByConversationAndMarkOpened('conversation-id-1');
            chai_1.assert.strictEqual(3, placeholders.getCount());
            const firstSaveItems = window.storage.get(retryPlaceholders_1.STORAGE_KEY);
            chai_1.assert.deepEqual([
                Object.assign(Object.assign({}, convo1a), { wasOpened: true }),
                Object.assign(Object.assign({}, convo1b), { wasOpened: true }),
                convo2a,
            ], firstSaveItems);
            const convo2b = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-2', receivedAt: NOW + 16 });
            await placeholders.add(convo2b);
            chai_1.assert.strictEqual(4, placeholders.getCount());
            await placeholders.findByConversationAndMarkOpened('conversation-id-2');
            chai_1.assert.strictEqual(4, placeholders.getCount());
            const secondSaveItems = window.storage.get(retryPlaceholders_1.STORAGE_KEY);
            chai_1.assert.deepEqual([
                Object.assign(Object.assign({}, convo1a), { wasOpened: true }),
                Object.assign(Object.assign({}, convo1b), { wasOpened: true }),
                Object.assign(Object.assign({}, convo2a), { wasOpened: true }),
                Object.assign(Object.assign({}, convo2b), { wasOpened: true }),
            ], secondSaveItems);
        });
    });
    describe('#findByMessageAndRemove', () => {
        it('does nothing if no item matching message found', async () => {
            const sentAt = NOW - 20;
            const older = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-1', sentAt: NOW - 10 });
            const newer = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-1', sentAt: NOW - 11 });
            const items = [older, newer];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(2, placeholders.getCount());
            chai_1.assert.isUndefined(await placeholders.findByMessageAndRemove('conversation-id-1', sentAt));
            chai_1.assert.strictEqual(2, placeholders.getCount());
        });
        it('removes the item matching message', async () => {
            const sentAt = NOW - 20;
            const older = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-1', sentAt: NOW - 10 });
            const newer = Object.assign(Object.assign({}, getDefaultItem()), { conversationId: 'conversation-id-1', sentAt });
            const items = [older, newer];
            window.storage.put(retryPlaceholders_1.STORAGE_KEY, items);
            const placeholders = new retryPlaceholders_1.RetryPlaceholders();
            chai_1.assert.strictEqual(2, placeholders.getCount());
            chai_1.assert.deepEqual(newer, await placeholders.findByMessageAndRemove('conversation-id-1', sentAt));
            chai_1.assert.strictEqual(1, placeholders.getCount());
        });
    });
});
