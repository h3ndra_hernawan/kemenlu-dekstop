"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const parseIntWithFallback_1 = require("../../util/parseIntWithFallback");
describe('parseIntWithFallback', () => {
    describe('when passed a number argument', () => {
        it('returns the number when passed an integer', () => {
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(0, -1), 0);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(123, -1), 123);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(-123, -1), -123);
        });
        it('returns the fallback when passed a decimal value', () => {
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(0.2, -1), -1);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(1.23, -1), -1);
        });
        it('returns the fallback when passed NaN', () => {
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(NaN, -1), -1);
        });
        it('returns the fallback when passed ∞', () => {
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(Infinity, -1), -1);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(-Infinity, -1), -1);
        });
    });
    describe('when passed a string argument', () => {
        it('returns the number when passed an integer', () => {
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('0', -1), 0);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('123', -1), 123);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('-123', -1), -123);
        });
        it('parses decimal values like parseInt', () => {
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('0.2', -1), 0);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('12.34', -1), 12);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('-12.34', -1), -12);
        });
        it('parses values in base 10', () => {
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('0x12', -1), 0);
        });
        it('returns the fallback when passed non-parseable strings', () => {
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('', -1), -1);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('uh 123', -1), -1);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)('uh oh', -1), -1);
        });
    });
    describe('when passed other arguments', () => {
        it("returns the fallback when passed arguments that aren't strings or numbers", () => {
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(null, -1), -1);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(undefined, -1), -1);
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(['123'], -1), -1);
        });
        it('returns the fallback when passed a stringifiable argument, unlike parseInt', () => {
            const obj = {
                toString() {
                    return '123';
                },
            };
            chai_1.assert.strictEqual((0, parseIntWithFallback_1.parseIntWithFallback)(obj, -1), -1);
        });
    });
});
