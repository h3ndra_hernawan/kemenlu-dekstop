"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const callingNotification_1 = require("../../util/callingNotification");
const Calling_1 = require("../../types/Calling");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
describe('calling notification helpers', () => {
    const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
    describe('getCallingNotificationText', () => {
        // Direct call behavior is not tested here.
        it('says that the call has ended', () => {
            chai_1.assert.strictEqual((0, callingNotification_1.getCallingNotificationText)({
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                ended: true,
                deviceCount: 1,
                maxDevices: 23,
                startedTime: Date.now(),
            }, i18n), 'The group call has ended');
        });
        it("includes the creator's first name when describing a call", () => {
            chai_1.assert.strictEqual((0, callingNotification_1.getCallingNotificationText)({
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                creator: {
                    firstName: 'Luigi',
                    isMe: false,
                    title: 'Luigi Mario',
                },
                ended: false,
                deviceCount: 1,
                maxDevices: 23,
                startedTime: Date.now(),
            }, i18n), 'Luigi started a group call');
        });
        it("if the creator doesn't have a first name, falls back to their title", () => {
            chai_1.assert.strictEqual((0, callingNotification_1.getCallingNotificationText)({
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                creator: {
                    isMe: false,
                    title: 'Luigi Mario',
                },
                ended: false,
                deviceCount: 1,
                maxDevices: 23,
                startedTime: Date.now(),
            }, i18n), 'Luigi Mario started a group call');
        });
        it('has a special message if you were the one to start the call', () => {
            chai_1.assert.strictEqual((0, callingNotification_1.getCallingNotificationText)({
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                creator: {
                    firstName: 'ShouldBeIgnored',
                    isMe: true,
                    title: 'ShouldBeIgnored Smith',
                },
                ended: false,
                deviceCount: 1,
                maxDevices: 23,
                startedTime: Date.now(),
            }, i18n), 'You started a group call');
        });
        it('handles an unknown creator', () => {
            chai_1.assert.strictEqual((0, callingNotification_1.getCallingNotificationText)({
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                ended: false,
                deviceCount: 1,
                maxDevices: 23,
                startedTime: Date.now(),
            }, i18n), 'A group call was started');
        });
    });
});
