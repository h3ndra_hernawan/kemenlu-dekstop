"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const cleanSearchTerm_1 = require("../../util/cleanSearchTerm");
describe('cleanSearchTerm', () => {
    it('should remove \\ from a search term', () => {
        const searchTerm = '\\search\\term';
        const sanitizedSearchTerm = (0, cleanSearchTerm_1.cleanSearchTerm)(searchTerm);
        chai_1.assert.strictEqual(sanitizedSearchTerm, 'search* term*');
    });
});
