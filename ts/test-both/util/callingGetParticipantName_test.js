"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const callingGetParticipantName_1 = require("../../util/callingGetParticipantName");
describe('getParticipantName', () => {
    it('returns the first name if available', () => {
        const participant = {
            firstName: 'Foo',
            title: 'Foo Bar',
        };
        chai_1.assert.strictEqual((0, callingGetParticipantName_1.getParticipantName)(participant), 'Foo');
    });
    it('returns the title if the first name is unavailable', () => {
        const participant = { title: 'Foo Bar' };
        chai_1.assert.strictEqual((0, callingGetParticipantName_1.getParticipantName)(participant), 'Foo Bar');
    });
});
