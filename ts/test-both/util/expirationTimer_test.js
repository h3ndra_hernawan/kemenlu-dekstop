"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const moment = __importStar(require("moment"));
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const messages_json_2 = __importDefault(require("../../../_locales/es/messages.json"));
const messages_json_3 = __importDefault(require("../../../_locales/nb/messages.json"));
const messages_json_4 = __importDefault(require("../../../_locales/nn/messages.json"));
const messages_json_5 = __importDefault(require("../../../_locales/pt_BR/messages.json"));
const messages_json_6 = __importDefault(require("../../../_locales/zh_CN/messages.json"));
const expirationTimer = __importStar(require("../../util/expirationTimer"));
describe('expiration timer utilities', () => {
    const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
    describe('DEFAULT_DURATIONS_IN_SECONDS', () => {
        const { DEFAULT_DURATIONS_IN_SECONDS } = expirationTimer;
        it('includes at least 3 durations', () => {
            chai_1.assert.isAtLeast(DEFAULT_DURATIONS_IN_SECONDS.length, 3);
        });
        it('includes 1 hour as seconds', () => {
            const oneHour = moment.duration(1, 'hour').asSeconds();
            chai_1.assert.include(DEFAULT_DURATIONS_IN_SECONDS, oneHour);
        });
    });
    describe('format', () => {
        const { format } = expirationTimer;
        it('handles an undefined duration', () => {
            chai_1.assert.strictEqual(format(i18n, undefined), 'off');
        });
        it('handles no duration', () => {
            chai_1.assert.strictEqual(format(i18n, 0), 'off');
        });
        it('formats durations', () => {
            new Map([
                [1, '1 second'],
                [2, '2 seconds'],
                [30, '30 seconds'],
                [59, '59 seconds'],
                [moment.duration(1, 'm').asSeconds(), '1 minute'],
                [moment.duration(5, 'm').asSeconds(), '5 minutes'],
                [moment.duration(1, 'h').asSeconds(), '1 hour'],
                [moment.duration(8, 'h').asSeconds(), '8 hours'],
                [moment.duration(1, 'd').asSeconds(), '1 day'],
                [moment.duration(6, 'd').asSeconds(), '6 days'],
                [moment.duration(8, 'd').asSeconds(), '8 days'],
                [moment.duration(30, 'd').asSeconds(), '30 days'],
                [moment.duration(365, 'd').asSeconds(), '365 days'],
                [moment.duration(1, 'w').asSeconds(), '1 week'],
                [moment.duration(3, 'w').asSeconds(), '3 weeks'],
                [moment.duration(52, 'w').asSeconds(), '52 weeks'],
            ]).forEach((expected, input) => {
                chai_1.assert.strictEqual(format(i18n, input), expected);
            });
        });
        it('formats other languages successfully', () => {
            const esI18n = (0, setupI18n_1.setupI18n)('es', messages_json_2.default);
            chai_1.assert.strictEqual(format(esI18n, 120), '2 minutos');
            const zhCnI18n = (0, setupI18n_1.setupI18n)('zh_CN', messages_json_6.default);
            chai_1.assert.strictEqual(format(zhCnI18n, 60), '1 分钟');
            // The underlying library supports the "pt" locale, not the "pt_BR" locale. That's
            //   what we're testing here.
            const ptBrI18n = (0, setupI18n_1.setupI18n)('pt_BR', messages_json_5.default);
            chai_1.assert.strictEqual(format(ptBrI18n, moment.duration(5, 'days').asSeconds()), '5 dias');
            // The underlying library supports the Norwegian language, which is a macrolanguage
            //   for Bokmål and Nynorsk.
            [(0, setupI18n_1.setupI18n)('nb', messages_json_3.default), (0, setupI18n_1.setupI18n)('nn', messages_json_4.default)].forEach(norwegianI18n => {
                chai_1.assert.strictEqual(format(norwegianI18n, moment.duration(6, 'hours').asSeconds()), '6 timer');
            });
        });
        it('falls back to English if the locale is not supported', () => {
            const badI18n = (0, setupI18n_1.setupI18n)('bogus', {});
            chai_1.assert.strictEqual(format(badI18n, 120), '2 minutes');
        });
        it('handles a "mix" of units gracefully', () => {
            // We don't expect there to be a "mix" of units, but we shouldn't choke if a bad
            //   client gives us an unexpected timestamp.
            const mix = moment
                .duration(6, 'days')
                .add(moment.duration(2, 'hours'))
                .asSeconds();
            chai_1.assert.strictEqual(format(i18n, mix), '6 days, 2 hours');
        });
        it('handles negative numbers gracefully', () => {
            // The proto helps enforce non-negative numbers by specifying a u32, but because
            //   JavaScript lacks such a type, we test it here.
            chai_1.assert.strictEqual(format(i18n, -1), '1 second');
            chai_1.assert.strictEqual(format(i18n, -120), '2 minutes');
            chai_1.assert.strictEqual(format(i18n, -0), 'off');
        });
        it('handles fractional seconds gracefully', () => {
            // The proto helps enforce integer numbers by specifying a u32, but this function
            //   shouldn't choke if bad data is passed somehow.
            chai_1.assert.strictEqual(format(i18n, 4.2), '4 seconds');
            chai_1.assert.strictEqual(format(i18n, 4.8), '4 seconds');
            chai_1.assert.strictEqual(format(i18n, 0.2), '1 second');
            chai_1.assert.strictEqual(format(i18n, 0.8), '1 second');
            // If multiple things go wrong and we pass a fractional negative number, we still
            //   shouldn't explode.
            chai_1.assert.strictEqual(format(i18n, -4.2), '4 seconds');
            chai_1.assert.strictEqual(format(i18n, -4.8), '4 seconds');
            chai_1.assert.strictEqual(format(i18n, -0.2), '1 second');
            chai_1.assert.strictEqual(format(i18n, -0.8), '1 second');
        });
    });
});
