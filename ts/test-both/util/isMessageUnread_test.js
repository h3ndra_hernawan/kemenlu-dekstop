"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const MessageReadStatus_1 = require("../../messages/MessageReadStatus");
const isMessageUnread_1 = require("../../util/isMessageUnread");
describe('isMessageUnread', () => {
    it("returns false if the message's `readStatus` field is undefined", () => {
        chai_1.assert.isFalse((0, isMessageUnread_1.isMessageUnread)({}));
        chai_1.assert.isFalse((0, isMessageUnread_1.isMessageUnread)({ readStatus: undefined }));
    });
    it('returns false if the message is read or viewed', () => {
        chai_1.assert.isFalse((0, isMessageUnread_1.isMessageUnread)({ readStatus: MessageReadStatus_1.ReadStatus.Read }));
        chai_1.assert.isFalse((0, isMessageUnread_1.isMessageUnread)({ readStatus: MessageReadStatus_1.ReadStatus.Viewed }));
    });
    it('returns true if the message is unread', () => {
        chai_1.assert.isTrue((0, isMessageUnread_1.isMessageUnread)({ readStatus: MessageReadStatus_1.ReadStatus.Unread }));
    });
});
