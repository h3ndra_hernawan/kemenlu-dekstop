"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const getDefaultConversation_1 = require("../helpers/getDefaultConversation");
const filterAndSortConversations_1 = require("../../util/filterAndSortConversations");
describe('filterAndSortConversationsByTitle', () => {
    const conversations = [
        (0, getDefaultConversation_1.getDefaultConversation)({
            title: '+16505551234',
            e164: '+16505551234',
            name: undefined,
            profileName: undefined,
        }),
        (0, getDefaultConversation_1.getDefaultConversation)({
            name: 'Carlos Santana',
            title: 'Carlos Santana',
            e164: '+16505559876',
            username: 'thisismyusername',
        }),
        (0, getDefaultConversation_1.getDefaultConversation)({
            name: 'Aaron Aardvark',
            title: 'Aaron Aardvark',
        }),
        (0, getDefaultConversation_1.getDefaultConversation)({
            name: 'Belinda Beetle',
            title: 'Belinda Beetle',
        }),
        (0, getDefaultConversation_1.getDefaultConversation)({
            name: 'Belinda Zephyr',
            title: 'Belinda Zephyr',
        }),
    ];
    it('without a search term, sorts conversations by title (but puts no-name contacts at the bottom)', () => {
        const titles = (0, filterAndSortConversations_1.filterAndSortConversationsByTitle)(conversations, '').map(contact => contact.title);
        chai_1.assert.deepEqual(titles, [
            'Aaron Aardvark',
            'Belinda Beetle',
            'Belinda Zephyr',
            'Carlos Santana',
            '+16505551234',
        ]);
    });
    it('can search for contacts by title', () => {
        const titles = (0, filterAndSortConversations_1.filterAndSortConversationsByTitle)(conversations, 'belind').map(contact => contact.title);
        chai_1.assert.sameMembers(titles, ['Belinda Beetle', 'Belinda Zephyr']);
    });
    it('can search for contacts by phone number (and puts no-name contacts at the bottom)', () => {
        const titles = (0, filterAndSortConversations_1.filterAndSortConversationsByTitle)(conversations, '650555').map(contact => contact.title);
        chai_1.assert.sameMembers(titles, ['Carlos Santana', '+16505551234']);
    });
    it('can search for contacts by username', () => {
        const titles = (0, filterAndSortConversations_1.filterAndSortConversationsByTitle)(conversations, 'thisis').map(contact => contact.title);
        chai_1.assert.sameMembers(titles, ['Carlos Santana']);
    });
});
describe('filterAndSortConversationsByRecent', () => {
    const conversations = [
        (0, getDefaultConversation_1.getDefaultConversation)({
            title: '+16505551234',
            activeAt: 1,
        }),
        (0, getDefaultConversation_1.getDefaultConversation)({
            title: 'Abraham Lincoln',
            activeAt: 4,
        }),
        (0, getDefaultConversation_1.getDefaultConversation)({
            title: 'Boxing Club',
            activeAt: 3,
        }),
        (0, getDefaultConversation_1.getDefaultConversation)({
            title: 'Not recent',
        }),
        (0, getDefaultConversation_1.getDefaultConversation)({
            title: 'George Washington',
            e164: '+16505559876',
            activeAt: 2,
        }),
    ];
    it('sorts by recency when no search term is provided', () => {
        const titles = (0, filterAndSortConversations_1.filterAndSortConversationsByRecent)(conversations, '').map(contact => contact.title);
        chai_1.assert.sameMembers(titles, [
            '+16505551234',
            'George Washington',
            'Boxing Club',
            'Abraham Lincoln',
            'Not recent',
        ]);
    });
});
