"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const shouldBlurAvatar_1 = require("../../util/shouldBlurAvatar");
describe('shouldBlurAvatar', () => {
    it('returns false for me', () => {
        chai_1.assert.isFalse((0, shouldBlurAvatar_1.shouldBlurAvatar)({
            isMe: true,
            acceptedMessageRequest: false,
            avatarPath: '/path/to/avatar.jpg',
            sharedGroupNames: [],
            unblurredAvatarPath: undefined,
        }));
    });
    it('returns false if the message request has been accepted', () => {
        chai_1.assert.isFalse((0, shouldBlurAvatar_1.shouldBlurAvatar)({
            acceptedMessageRequest: true,
            avatarPath: '/path/to/avatar.jpg',
            isMe: false,
            sharedGroupNames: [],
            unblurredAvatarPath: undefined,
        }));
    });
    it('returns false if there are any shared groups', () => {
        chai_1.assert.isFalse((0, shouldBlurAvatar_1.shouldBlurAvatar)({
            sharedGroupNames: ['Tahoe Trip'],
            acceptedMessageRequest: false,
            avatarPath: '/path/to/avatar.jpg',
            isMe: false,
            unblurredAvatarPath: undefined,
        }));
    });
    it('returns false if there is no avatar', () => {
        chai_1.assert.isFalse((0, shouldBlurAvatar_1.shouldBlurAvatar)({
            acceptedMessageRequest: false,
            isMe: false,
            sharedGroupNames: [],
            unblurredAvatarPath: undefined,
        }));
        chai_1.assert.isFalse((0, shouldBlurAvatar_1.shouldBlurAvatar)({
            avatarPath: undefined,
            acceptedMessageRequest: false,
            isMe: false,
            sharedGroupNames: [],
            unblurredAvatarPath: undefined,
        }));
        chai_1.assert.isFalse((0, shouldBlurAvatar_1.shouldBlurAvatar)({
            avatarPath: undefined,
            unblurredAvatarPath: '/some/other/path',
            acceptedMessageRequest: false,
            isMe: false,
            sharedGroupNames: [],
        }));
    });
    it('returns false if the avatar was unblurred', () => {
        chai_1.assert.isFalse((0, shouldBlurAvatar_1.shouldBlurAvatar)({
            avatarPath: '/path/to/avatar.jpg',
            unblurredAvatarPath: '/path/to/avatar.jpg',
            acceptedMessageRequest: false,
            isMe: false,
            sharedGroupNames: [],
        }));
    });
    it('returns true if the stars align (i.e., not everything above)', () => {
        chai_1.assert.isTrue((0, shouldBlurAvatar_1.shouldBlurAvatar)({
            avatarPath: '/path/to/avatar.jpg',
            unblurredAvatarPath: '/different/path.jpg',
            acceptedMessageRequest: false,
            isMe: false,
            sharedGroupNames: [],
        }));
    });
});
