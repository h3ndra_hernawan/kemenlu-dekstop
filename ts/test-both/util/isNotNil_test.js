"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isNotNil_1 = require("../../util/isNotNil");
describe('isNotNil', () => {
    it('returns false if provided null value', () => {
        chai_1.assert.isFalse((0, isNotNil_1.isNotNil)(null));
    });
    it('returns false is provided undefined value', () => {
        chai_1.assert.isFalse((0, isNotNil_1.isNotNil)(undefined));
    });
    it('returns false is provided any other value', () => {
        chai_1.assert.isTrue((0, isNotNil_1.isNotNil)(0));
        chai_1.assert.isTrue((0, isNotNil_1.isNotNil)(4));
        chai_1.assert.isTrue((0, isNotNil_1.isNotNil)(''));
        chai_1.assert.isTrue((0, isNotNil_1.isNotNil)('string value'));
        chai_1.assert.isTrue((0, isNotNil_1.isNotNil)({}));
    });
});
