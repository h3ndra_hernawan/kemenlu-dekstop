"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isConversationUnread_1 = require("../../util/isConversationUnread");
describe('isConversationUnread', () => {
    it('returns false if both markedUnread and unreadCount are undefined', () => {
        chai_1.assert.isFalse((0, isConversationUnread_1.isConversationUnread)({}));
        chai_1.assert.isFalse((0, isConversationUnread_1.isConversationUnread)({
            markedUnread: undefined,
            unreadCount: undefined,
        }));
    });
    it('returns false if markedUnread is false', () => {
        chai_1.assert.isFalse((0, isConversationUnread_1.isConversationUnread)({ markedUnread: false }));
    });
    it('returns false if unreadCount is 0', () => {
        chai_1.assert.isFalse((0, isConversationUnread_1.isConversationUnread)({ unreadCount: 0 }));
    });
    it('returns true if markedUnread is true, regardless of unreadCount', () => {
        chai_1.assert.isTrue((0, isConversationUnread_1.isConversationUnread)({ markedUnread: true }));
        chai_1.assert.isTrue((0, isConversationUnread_1.isConversationUnread)({ markedUnread: true, unreadCount: 0 }));
        chai_1.assert.isTrue((0, isConversationUnread_1.isConversationUnread)({ markedUnread: true, unreadCount: 100 }));
    });
    it('returns true if unreadCount is positive, regardless of markedUnread', () => {
        chai_1.assert.isTrue((0, isConversationUnread_1.isConversationUnread)({ unreadCount: 1 }));
        chai_1.assert.isTrue((0, isConversationUnread_1.isConversationUnread)({ unreadCount: 99 }));
        chai_1.assert.isTrue((0, isConversationUnread_1.isConversationUnread)({ markedUnread: false, unreadCount: 2 }));
    });
    it('returns true if both markedUnread is true and unreadCount is positive', () => {
        chai_1.assert.isTrue((0, isConversationUnread_1.isConversationUnread)({ markedUnread: true, unreadCount: 1 }));
    });
});
