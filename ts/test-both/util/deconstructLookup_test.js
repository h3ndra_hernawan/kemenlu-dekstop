"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const deconstructLookup_1 = require("../../util/deconstructLookup");
describe('deconstructLookup', () => {
    it('looks up an array of properties in a lookup', () => {
        const lookup = {
            high: 5,
            seven: 89,
            big: 999,
        };
        const keys = ['seven', 'high'];
        chai_1.assert.deepEqual((0, deconstructLookup_1.deconstructLookup)(lookup, keys), [89, 5]);
    });
});
