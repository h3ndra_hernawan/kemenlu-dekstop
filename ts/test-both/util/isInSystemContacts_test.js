"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isInSystemContacts_1 = require("../../util/isInSystemContacts");
describe('isInSystemContacts', () => {
    it('returns true for direct conversations that have a `name` property', () => {
        chai_1.assert.isTrue((0, isInSystemContacts_1.isInSystemContacts)({
            type: 'direct',
            name: 'Jane Doe',
        }));
        chai_1.assert.isTrue((0, isInSystemContacts_1.isInSystemContacts)({
            type: 'private',
            name: 'Jane Doe',
        }));
    });
    it('returns true for direct conversations that have an empty string `name`', () => {
        chai_1.assert.isTrue((0, isInSystemContacts_1.isInSystemContacts)({
            type: 'direct',
            name: '',
        }));
    });
    it('returns false for direct conversations that lack a `name` property', () => {
        chai_1.assert.isFalse((0, isInSystemContacts_1.isInSystemContacts)({ type: 'direct' }));
    });
    it('returns false for group conversations', () => {
        chai_1.assert.isFalse((0, isInSystemContacts_1.isInSystemContacts)({ type: 'group' }));
        chai_1.assert.isFalse((0, isInSystemContacts_1.isInSystemContacts)({
            type: 'group',
            name: 'Tahoe Trip',
        }));
    });
});
