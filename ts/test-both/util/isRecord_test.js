"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isRecord_1 = require("../../util/isRecord");
describe('isRecord', () => {
    it('returns false for primitives', () => {
        ['hello', 123, BigInt(123), true, undefined, Symbol('test'), null].forEach(value => {
            chai_1.assert.isFalse((0, isRecord_1.isRecord)(value));
        });
    });
    it('returns false for arrays', () => {
        chai_1.assert.isFalse((0, isRecord_1.isRecord)([]));
    });
    it('returns true for "plain" objects', () => {
        chai_1.assert.isTrue((0, isRecord_1.isRecord)({}));
        chai_1.assert.isTrue((0, isRecord_1.isRecord)({ foo: 'bar' }));
        chai_1.assert.isTrue((0, isRecord_1.isRecord)(Object.create(null)));
    });
});
