"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const characters_1 = require("../../util/characters");
describe('character utilities', () => {
    describe('count', () => {
        it('returns the number of characters in a string (not necessarily the length)', () => {
            chai_1.assert.strictEqual((0, characters_1.count)(''), 0);
            chai_1.assert.strictEqual((0, characters_1.count)('hello'), 5);
            chai_1.assert.strictEqual((0, characters_1.count)('Bokmål'), 6);
            chai_1.assert.strictEqual((0, characters_1.count)('💩💩💩'), 3);
            chai_1.assert.strictEqual((0, characters_1.count)('👩‍❤️‍👩'), 6);
            chai_1.assert.strictEqual((0, characters_1.count)('Z͑ͫ̓ͪ̂ͫ̽͏̴̙̤̞͉͚̯̞̠͍A̴̵̜̰͔ͫ͗͢L̠ͨͧͩ͘G̴̻͈͍͔̹̑͗̎̅͛́Ǫ̵̹̻̝̳͂̌̌͘'), 58);
        });
    });
});
