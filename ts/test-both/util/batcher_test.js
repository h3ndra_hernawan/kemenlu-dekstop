"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const batcher_1 = require("../../util/batcher");
const sleep_1 = require("../../util/sleep");
describe('batcher', () => {
    it('should schedule a full batch', async () => {
        const processBatch = sinon.fake.resolves(undefined);
        const batcher = (0, batcher_1.createBatcher)({
            name: 'test',
            wait: 10,
            maxSize: 2,
            processBatch,
        });
        batcher.add(1);
        batcher.add(2);
        chai_1.assert.ok(processBatch.calledOnceWith([1, 2]), 'Full batch on first call');
    });
    it('should schedule a partial batch', async () => {
        const processBatch = sinon.fake.resolves(undefined);
        const batcher = (0, batcher_1.createBatcher)({
            name: 'test',
            wait: 5,
            maxSize: 2,
            processBatch,
        });
        batcher.add(1);
        await (0, sleep_1.sleep)(10);
        chai_1.assert.ok(processBatch.calledOnceWith([1]), 'Partial batch after timeout');
    });
    it('should remove scheduled items from a batch', async () => {
        const processBatch = sinon.fake.resolves(undefined);
        const batcher = (0, batcher_1.createBatcher)({
            name: 'test',
            wait: 5,
            maxSize: 100,
            processBatch,
        });
        batcher.add(1);
        batcher.add(1);
        batcher.add(2);
        batcher.removeAll(1);
        await (0, sleep_1.sleep)(10);
        chai_1.assert.ok(processBatch.calledOnceWith([2]), 'Remove all');
    });
    it('should flushAndWait a partial batch', async () => {
        const processBatch = sinon.fake.resolves(undefined);
        const batcher = (0, batcher_1.createBatcher)({
            name: 'test',
            wait: 10000,
            maxSize: 1000,
            processBatch,
        });
        batcher.add(1);
        await batcher.flushAndWait();
        chai_1.assert.ok(processBatch.calledOnceWith([1]), 'Partial batch after flushAndWait');
    });
    it('should flushAndWait a partial batch with new items added', async () => {
        let calledTimes = 0;
        const processBatch = async (batch) => {
            calledTimes += 1;
            if (calledTimes === 1) {
                chai_1.assert.deepEqual(batch, [1], 'First partial batch');
                batcher.add(2);
            }
            else if (calledTimes === 2) {
                chai_1.assert.deepEqual(batch, [2], 'Second partial batch');
            }
            else {
                chai_1.assert.strictEqual(calledTimes, 2);
            }
        };
        const batcher = (0, batcher_1.createBatcher)({
            name: 'test',
            wait: 10000,
            maxSize: 1000,
            processBatch,
        });
        batcher.add(1);
        await batcher.flushAndWait();
        chai_1.assert.strictEqual(calledTimes, 2);
    });
});
