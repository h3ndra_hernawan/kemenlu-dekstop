"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
const chai_1 = require("chai");
const emoji_1 = require("../../util/emoji");
describe('emoji', () => {
    describe('replaceEmojiWithSpaces', () => {
        it('replaces emoji and pictograms with a single space', () => {
            chai_1.assert.strictEqual((0, emoji_1.replaceEmojiWithSpaces)('hello🌀🐀🔀😀world'), 'hello    world');
        });
        it('leaves regular text as it is', () => {
            chai_1.assert.strictEqual((0, emoji_1.replaceEmojiWithSpaces)('Привет 嘿 հեյ העלא مرحبا '), 'Привет 嘿 հեյ העלא مرحبا ');
        });
    });
    describe('splitByEmoji', () => {
        it('replaces emoji and pictograms with a single space', () => {
            chai_1.assert.deepStrictEqual((0, emoji_1.splitByEmoji)('hello😛world😎😛!'), [
                { type: 'text', value: 'hello' },
                { type: 'emoji', value: '😛' },
                { type: 'text', value: 'world' },
                { type: 'emoji', value: '😎' },
                { type: 'emoji', value: '😛' },
                { type: 'text', value: '!' },
            ]);
        });
        it('returns emojis as text after 5,000 emojis are found', () => {
            chai_1.assert.deepStrictEqual((0, emoji_1.splitByEmoji)('💬'.repeat(5002)), [
                ...Array(5000).fill({ type: 'emoji', value: '💬' }),
                { type: 'text', value: '💬💬' },
            ]);
        });
    });
});
