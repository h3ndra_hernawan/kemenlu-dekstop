"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const explodePromise_1 = require("../../util/explodePromise");
describe('explodePromise', () => {
    it('resolves the promise', async () => {
        const { promise, resolve } = (0, explodePromise_1.explodePromise)();
        resolve(42);
        chai_1.assert.strictEqual(await promise, 42);
    });
    it('rejects the promise', async () => {
        const { promise, reject } = (0, explodePromise_1.explodePromise)();
        reject(new Error('rejected'));
        await chai_1.assert.isRejected(promise, 'rejected');
    });
});
