"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon_1 = require("sinon");
const semver = __importStar(require("semver"));
const version_1 = require("../../util/version");
describe('version utilities', () => {
    describe('isProduction', () => {
        it('returns false for anything non-basic version number', () => {
            chai_1.assert.isFalse((0, version_1.isProduction)('1.2.3-1'));
            chai_1.assert.isFalse((0, version_1.isProduction)('1.2.3-alpha.1'));
            chai_1.assert.isFalse((0, version_1.isProduction)('1.2.3-beta.1'));
            chai_1.assert.isFalse((0, version_1.isProduction)('1.2.3-rc'));
        });
        it('returns true for production version strings', () => {
            chai_1.assert.isTrue((0, version_1.isProduction)('1.2.3'));
            chai_1.assert.isTrue((0, version_1.isProduction)('5.10.0'));
        });
    });
    describe('isBeta', () => {
        it('returns false for non-beta version strings', () => {
            chai_1.assert.isFalse((0, version_1.isBeta)('1.2.3'));
            chai_1.assert.isFalse((0, version_1.isBeta)('1.2.3-alpha'));
            chai_1.assert.isFalse((0, version_1.isBeta)('1.2.3-alpha.1'));
            chai_1.assert.isFalse((0, version_1.isBeta)('1.2.3-rc.1'));
        });
        it('returns true for beta version strings', () => {
            chai_1.assert.isTrue((0, version_1.isBeta)('1.2.3-beta'));
            chai_1.assert.isTrue((0, version_1.isBeta)('1.2.3-beta.1'));
        });
    });
    describe('isAlpha', () => {
        it('returns false for non-alpha version strings', () => {
            chai_1.assert.isFalse((0, version_1.isAlpha)('1.2.3'));
            chai_1.assert.isFalse((0, version_1.isAlpha)('1.2.3-beta'));
            chai_1.assert.isFalse((0, version_1.isAlpha)('1.2.3-beta.1'));
            chai_1.assert.isFalse((0, version_1.isAlpha)('1.2.3-rc.1'));
        });
        it('returns true for Alpha version strings', () => {
            chai_1.assert.isTrue((0, version_1.isAlpha)('1.2.3-alpha'));
            chai_1.assert.isTrue((0, version_1.isAlpha)('1.2.3-alpha.1'));
        });
    });
    describe('generateAlphaVersion', () => {
        beforeEach(function beforeEach() {
            // This isn't a hook.
            // eslint-disable-next-line react-hooks/rules-of-hooks
            this.clock = (0, sinon_1.useFakeTimers)();
        });
        afterEach(function afterEach() {
            this.clock.restore();
        });
        it('uses the current date and provided shortSha', function test() {
            this.clock.setSystemTime(new Date('2021-07-23T01:22:55.692Z').getTime());
            const currentVersion = '5.12.0-beta.1';
            const shortSha = '07f0efc45';
            const expected = '5.12.0-alpha.20210723.01-07f0efc45';
            const actual = (0, version_1.generateAlphaVersion)({ currentVersion, shortSha });
            chai_1.assert.strictEqual(expected, actual);
        });
        it('same production version is semver.gt', function test() {
            const currentVersion = '5.12.0-beta.1';
            const shortSha = '07f0efc45';
            this.clock.setSystemTime(new Date('2021-07-23T01:22:55.692Z').getTime());
            const actual = (0, version_1.generateAlphaVersion)({ currentVersion, shortSha });
            chai_1.assert.isTrue(semver.gt('5.12.0', actual));
        });
        it('same beta version is semver.gt', function test() {
            const currentVersion = '5.12.0-beta.1';
            const shortSha = '07f0efc45';
            this.clock.setSystemTime(new Date('2021-07-23T01:22:55.692Z').getTime());
            const actual = (0, version_1.generateAlphaVersion)({ currentVersion, shortSha });
            chai_1.assert.isTrue(semver.gt(currentVersion, actual));
        });
        it('build earlier same day is semver.lt', function test() {
            const currentVersion = '5.12.0-beta.1';
            const shortSha = '07f0efc45';
            this.clock.setSystemTime(new Date('2021-07-23T00:22:55.692Z').getTime());
            const actualEarlier = (0, version_1.generateAlphaVersion)({ currentVersion, shortSha });
            this.clock.setSystemTime(new Date('2021-07-23T01:22:55.692Z').getTime());
            const actualLater = (0, version_1.generateAlphaVersion)({ currentVersion, shortSha });
            chai_1.assert.isTrue(semver.lt(actualEarlier, actualLater));
        });
        it('build previous day is semver.lt', function test() {
            const currentVersion = '5.12.0-beta.1';
            const shortSha = '07f0efc45';
            this.clock.setSystemTime(new Date('2021-07-22T01:22:55.692Z').getTime());
            const actualEarlier = (0, version_1.generateAlphaVersion)({ currentVersion, shortSha });
            this.clock.setSystemTime(new Date('2021-07-23T01:22:55.692Z').getTime());
            const actualLater = (0, version_1.generateAlphaVersion)({ currentVersion, shortSha });
            chai_1.assert.isTrue(semver.lt(actualEarlier, actualLater));
        });
    });
});
