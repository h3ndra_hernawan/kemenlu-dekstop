"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-await-in-loop */
const chai_1 = require("chai");
const asyncIterables_1 = require("../../util/asyncIterables");
describe('async iterable utilities', () => {
    describe('concat', () => {
        it('returns an empty async iterable if called with an empty list', async () => {
            const result = (0, asyncIterables_1.concat)([]);
            chai_1.assert.isEmpty(await collect(result));
        });
        it('concatenates synchronous and asynchronous iterables', async () => {
            function* makeSync() {
                yield 'sync 1';
                yield 'sync 2';
            }
            function makeAsync() {
                return __asyncGenerator(this, arguments, function* makeAsync_1() {
                    yield yield __await('async 1');
                    yield yield __await('async 2');
                });
            }
            const syncIterable = makeSync();
            const asyncIterable1 = makeAsync();
            const asyncIterable2 = makeAsync();
            const result = (0, asyncIterables_1.concat)([
                syncIterable,
                asyncIterable1,
                ['array 1', 'array 2'],
                asyncIterable2,
            ]);
            chai_1.assert.deepEqual(await collect(result), [
                'sync 1',
                'sync 2',
                'async 1',
                'async 2',
                'array 1',
                'array 2',
                'async 1',
                'async 2',
            ]);
        });
    });
    describe('wrapPromise', () => {
        it('resolves to an array when wrapping a synchronous iterable', async () => {
            const iterable = new Set([1, 2, 3]);
            const result = (0, asyncIterables_1.wrapPromise)(Promise.resolve(iterable));
            chai_1.assert.sameMembers(await collect(result), [1, 2, 3]);
        });
        it('resolves to an array when wrapping an asynchronous iterable', async () => {
            const iterable = (function test() {
                return __asyncGenerator(this, arguments, function* test_1() {
                    yield yield __await(1);
                    yield yield __await(2);
                    yield yield __await(3);
                });
            })();
            const result = (0, asyncIterables_1.wrapPromise)(Promise.resolve(iterable));
            chai_1.assert.deepEqual(await collect(result), [1, 2, 3]);
        });
    });
});
/**
 * Turns an iterable into a fully-realized array.
 *
 * If we want this outside of tests, we could make it into a "real" function.
 */
async function collect(iterable) {
    var e_1, _a;
    const result = [];
    try {
        for (var iterable_1 = __asyncValues(iterable), iterable_1_1; iterable_1_1 = await iterable_1.next(), !iterable_1_1.done;) {
            const value = iterable_1_1.value;
            result.push(value);
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (iterable_1_1 && !iterable_1_1.done && (_a = iterable_1.return)) await _a.call(iterable_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return result;
}
