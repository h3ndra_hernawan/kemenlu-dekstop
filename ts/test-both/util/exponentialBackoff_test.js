"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const durations = __importStar(require("../../util/durations"));
const exponentialBackoff_1 = require("../../util/exponentialBackoff");
describe('exponential backoff utilities', () => {
    describe('exponentialBackoffSleepTime', () => {
        it('returns slowly growing values', () => {
            chai_1.assert.strictEqual((0, exponentialBackoff_1.exponentialBackoffSleepTime)(1), 0);
            chai_1.assert.strictEqual((0, exponentialBackoff_1.exponentialBackoffSleepTime)(2), 190);
            chai_1.assert.strictEqual((0, exponentialBackoff_1.exponentialBackoffSleepTime)(3), 361);
            chai_1.assert.approximately((0, exponentialBackoff_1.exponentialBackoffSleepTime)(4), 686, 1);
            chai_1.assert.approximately((0, exponentialBackoff_1.exponentialBackoffSleepTime)(5), 1303, 1);
        });
        it('plateaus at a maximum after 15 attempts', () => {
            const maximum = 15 * durations.MINUTE;
            for (let attempt = 16; attempt < 100; attempt += 1) {
                chai_1.assert.strictEqual((0, exponentialBackoff_1.exponentialBackoffSleepTime)(attempt), maximum);
            }
        });
    });
    describe('exponentialBackoffMaxAttempts', () => {
        it('returns 2 attempts for a short period of time', () => {
            chai_1.assert.strictEqual((0, exponentialBackoff_1.exponentialBackoffMaxAttempts)(1), 2);
            chai_1.assert.strictEqual((0, exponentialBackoff_1.exponentialBackoffMaxAttempts)(99), 2);
        });
        it('returns 6 attempts for a 5 seconds', () => {
            chai_1.assert.strictEqual((0, exponentialBackoff_1.exponentialBackoffMaxAttempts)(5000), 6);
        });
        it('returns 110 attempts for 1 day', () => {
            // This is a test case that is lifted from iOS's codebase.
            chai_1.assert.strictEqual((0, exponentialBackoff_1.exponentialBackoffMaxAttempts)(durations.DAY), 110);
        });
    });
});
