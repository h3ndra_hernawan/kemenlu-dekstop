"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const arePinnedConversationsEqual_1 = require("../../util/arePinnedConversationsEqual");
describe('arePinnedConversationsEqual', () => {
    it('is equal if both have same values at same indices', () => {
        const localValue = [
            {
                contact: {
                    uuid: '72313cde-2784-4a6f-a92a-abbe23763a60',
                    e164: '+13055551234',
                },
            },
            {
                groupMasterKey: new Uint8Array(32),
            },
        ];
        const remoteValue = [
            {
                contact: {
                    uuid: '72313cde-2784-4a6f-a92a-abbe23763a60',
                    e164: '+13055551234',
                },
            },
            {
                groupMasterKey: new Uint8Array(32),
            },
        ];
        chai_1.assert.isTrue((0, arePinnedConversationsEqual_1.arePinnedConversationsEqual)(localValue, remoteValue));
    });
    it('is not equal if values are mixed', () => {
        const localValue = [
            {
                contact: {
                    uuid: '72313cde-2784-4a6f-a92a-abbe23763a60',
                    e164: '+13055551234',
                },
            },
            {
                contact: {
                    uuid: 'f59a9fed-9e91-4bb4-a015-d49e58b47e25',
                    e164: '+17865554321',
                },
            },
        ];
        const remoteValue = [
            {
                contact: {
                    uuid: 'f59a9fed-9e91-4bb4-a015-d49e58b47e25',
                    e164: '+17865554321',
                },
            },
            {
                contact: {
                    uuid: '72313cde-2784-4a6f-a92a-abbe23763a60',
                    e164: '+13055551234',
                },
            },
        ];
        chai_1.assert.isFalse((0, arePinnedConversationsEqual_1.arePinnedConversationsEqual)(localValue, remoteValue));
    });
    it('is not equal if lengths are not same', () => {
        const localValue = [
            {
                contact: {
                    uuid: '72313cde-2784-4a6f-a92a-abbe23763a60',
                    e164: '+13055551234',
                },
            },
        ];
        const remoteValue = [];
        chai_1.assert.isFalse((0, arePinnedConversationsEqual_1.arePinnedConversationsEqual)(localValue, remoteValue));
    });
    it('is not equal if content does not match', () => {
        const localValue = [
            {
                contact: {
                    uuid: '72313cde-2784-4a6f-a92a-abbe23763a60',
                    e164: '+13055551234',
                },
            },
        ];
        const remoteValue = [
            {
                groupMasterKey: new Uint8Array(32),
            },
        ];
        chai_1.assert.isFalse((0, arePinnedConversationsEqual_1.arePinnedConversationsEqual)(localValue, remoteValue));
    });
});
