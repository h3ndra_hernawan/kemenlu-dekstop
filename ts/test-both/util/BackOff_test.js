"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const BackOff_1 = require("../../util/BackOff");
describe('BackOff', () => {
    it('should return increasing timeouts', () => {
        const b = new BackOff_1.BackOff([1, 2, 3]);
        chai_1.assert.strictEqual(b.getIndex(), 0);
        chai_1.assert.strictEqual(b.isFull(), false);
        chai_1.assert.strictEqual(b.get(), 1);
        chai_1.assert.strictEqual(b.getAndIncrement(), 1);
        chai_1.assert.strictEqual(b.get(), 2);
        chai_1.assert.strictEqual(b.getIndex(), 1);
        chai_1.assert.strictEqual(b.isFull(), false);
        chai_1.assert.strictEqual(b.getAndIncrement(), 2);
        chai_1.assert.strictEqual(b.getIndex(), 2);
        chai_1.assert.strictEqual(b.isFull(), true);
        chai_1.assert.strictEqual(b.getAndIncrement(), 3);
        chai_1.assert.strictEqual(b.getIndex(), 2);
        chai_1.assert.strictEqual(b.isFull(), true);
        chai_1.assert.strictEqual(b.getAndIncrement(), 3);
        chai_1.assert.strictEqual(b.getIndex(), 2);
        chai_1.assert.strictEqual(b.isFull(), true);
    });
    it('should reset', () => {
        const b = new BackOff_1.BackOff([1, 2, 3]);
        chai_1.assert.strictEqual(b.getAndIncrement(), 1);
        chai_1.assert.strictEqual(b.getAndIncrement(), 2);
        b.reset();
        chai_1.assert.strictEqual(b.getAndIncrement(), 1);
        chai_1.assert.strictEqual(b.getAndIncrement(), 2);
    });
    it('should apply jitter', () => {
        const b = new BackOff_1.BackOff([1, 2, 3], {
            jitter: 1,
            random: () => 0.5,
        });
        chai_1.assert.strictEqual(b.getIndex(), 0);
        chai_1.assert.strictEqual(b.isFull(), false);
        chai_1.assert.strictEqual(b.get(), 1);
        chai_1.assert.strictEqual(b.getAndIncrement(), 1);
        chai_1.assert.strictEqual(b.get(), 2.5);
        chai_1.assert.strictEqual(b.getIndex(), 1);
        chai_1.assert.strictEqual(b.isFull(), false);
        chai_1.assert.strictEqual(b.getAndIncrement(), 2.5);
        chai_1.assert.strictEqual(b.getIndex(), 2);
        chai_1.assert.strictEqual(b.isFull(), true);
        chai_1.assert.strictEqual(b.getAndIncrement(), 3.5);
        chai_1.assert.strictEqual(b.getIndex(), 2);
        chai_1.assert.strictEqual(b.isFull(), true);
        chai_1.assert.strictEqual(b.getAndIncrement(), 3.5);
        chai_1.assert.strictEqual(b.getIndex(), 2);
        chai_1.assert.strictEqual(b.isFull(), true);
    });
});
