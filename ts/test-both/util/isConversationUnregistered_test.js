"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const durations = __importStar(require("../../util/durations"));
const isConversationUnregistered_1 = require("../../util/isConversationUnregistered");
describe('isConversationUnregistered', () => {
    it('returns false if passed an undefined discoveredUnregisteredAt', () => {
        chai_1.assert.isFalse((0, isConversationUnregistered_1.isConversationUnregistered)({}));
        chai_1.assert.isFalse((0, isConversationUnregistered_1.isConversationUnregistered)({ discoveredUnregisteredAt: undefined }));
    });
    it('returns true if passed a time fewer than 6 hours ago', () => {
        chai_1.assert.isTrue((0, isConversationUnregistered_1.isConversationUnregistered)({ discoveredUnregisteredAt: Date.now() }));
        const fiveHours = 1000 * 60 * 60 * 5;
        chai_1.assert.isTrue((0, isConversationUnregistered_1.isConversationUnregistered)({
            discoveredUnregisteredAt: Date.now() - fiveHours,
        }));
    });
    it('returns true if passed a time in the future', () => {
        chai_1.assert.isTrue((0, isConversationUnregistered_1.isConversationUnregistered)({ discoveredUnregisteredAt: Date.now() + 123 }));
    });
    it('returns false if passed a time more than 6 hours ago', () => {
        chai_1.assert.isFalse((0, isConversationUnregistered_1.isConversationUnregistered)({
            discoveredUnregisteredAt: Date.now() - 6 * durations.HOUR - durations.MINUTE,
        }));
        chai_1.assert.isFalse((0, isConversationUnregistered_1.isConversationUnregistered)({
            discoveredUnregisteredAt: new Date(1999, 3, 20).getTime(),
        }));
    });
});
