"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isConversationNameKnown_1 = require("../../util/isConversationNameKnown");
describe('isConversationNameKnown', () => {
    describe('for direct conversations', () => {
        it('returns true if the conversation has a name', () => {
            chai_1.assert.isTrue((0, isConversationNameKnown_1.isConversationNameKnown)({
                type: 'direct',
                name: 'Jane Doe',
            }));
        });
        it('returns true if the conversation has a profile name', () => {
            chai_1.assert.isTrue((0, isConversationNameKnown_1.isConversationNameKnown)({
                type: 'direct',
                profileName: 'Jane Doe',
            }));
        });
        it('returns true if the conversation has an E164', () => {
            chai_1.assert.isTrue((0, isConversationNameKnown_1.isConversationNameKnown)({
                type: 'direct',
                e164: '+16505551234',
            }));
        });
        it('returns false if the conversation has none of the above', () => {
            chai_1.assert.isFalse((0, isConversationNameKnown_1.isConversationNameKnown)({ type: 'direct' }));
        });
    });
    describe('for group conversations', () => {
        it('returns true if the conversation has a name', () => {
            chai_1.assert.isTrue((0, isConversationNameKnown_1.isConversationNameKnown)({
                type: 'group',
                name: 'Tahoe Trip',
            }));
        });
        it('returns true if the conversation lacks a name', () => {
            chai_1.assert.isFalse((0, isConversationNameKnown_1.isConversationNameKnown)({ type: 'group' }));
        });
    });
});
