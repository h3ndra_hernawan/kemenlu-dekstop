"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const getMuteOptions_1 = require("../../util/getMuteOptions");
describe('getMuteOptions', () => {
    const HOUR = 3600000;
    const DAY = HOUR * 24;
    const WEEK = DAY * 7;
    const EXPECTED_DEFAULT_OPTIONS = [
        {
            name: 'Mute for one hour',
            value: HOUR,
        },
        {
            name: 'Mute for eight hours',
            value: HOUR * 8,
        },
        {
            name: 'Mute for one day',
            value: DAY,
        },
        {
            name: 'Mute for one week',
            value: WEEK,
        },
        {
            name: 'Mute always',
            value: Number.MAX_SAFE_INTEGER,
        },
    ];
    const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
    describe('when not muted', () => {
        it('returns the 5 default options', () => {
            chai_1.assert.deepStrictEqual((0, getMuteOptions_1.getMuteOptions)(undefined, i18n), EXPECTED_DEFAULT_OPTIONS);
        });
    });
    describe('when muted', () => {
        let sandbox;
        beforeEach(() => {
            sandbox = sinon.createSandbox();
            sandbox.useFakeTimers({
                now: new Date(2000, 3, 20, 12, 0, 0),
            });
        });
        afterEach(() => {
            sandbox.restore();
        });
        it('returns a current mute label, an "Unmute" option, and then the 5 default options', () => {
            chai_1.assert.deepStrictEqual((0, getMuteOptions_1.getMuteOptions)(new Date(2000, 3, 20, 18, 30, 0).valueOf(), i18n), [
                {
                    disabled: true,
                    name: 'Muted until 6:30 PM',
                    value: -1,
                },
                {
                    name: 'Unmute',
                    value: 0,
                },
                ...EXPECTED_DEFAULT_OPTIONS,
            ]);
        });
        it("renders the current mute label with a date if it's on a different day", () => {
            chai_1.assert.deepStrictEqual((0, getMuteOptions_1.getMuteOptions)(new Date(2000, 3, 21, 18, 30, 0).valueOf(), i18n)[0], {
                disabled: true,
                name: 'Muted until 04/21/2000, 6:30 PM',
                value: -1,
            });
        });
    });
});
