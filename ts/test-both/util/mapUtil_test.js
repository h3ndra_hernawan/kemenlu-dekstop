"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const mapUtil_1 = require("../../util/mapUtil");
describe('map utilities', () => {
    describe('groupBy', () => {
        it('returns an empty map when passed an empty iterable', () => {
            const fn = sinon.fake();
            chai_1.assert.isEmpty((0, mapUtil_1.groupBy)([], fn));
            sinon.assert.notCalled(fn);
        });
        it('groups the iterable', () => {
            chai_1.assert.deepEqual((0, mapUtil_1.groupBy)([2.3, 1.3, 2.9, 1.1, 3.4], Math.floor), new Map([
                [1, [1.3, 1.1]],
                [2, [2.3, 2.9]],
                [3, [3.4]],
            ]));
        });
    });
});
