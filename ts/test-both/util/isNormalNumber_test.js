"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isNormalNumber_1 = require("../../util/isNormalNumber");
describe('isNormalNumber', () => {
    it('returns false for non-numbers', () => {
        chai_1.assert.isFalse((0, isNormalNumber_1.isNormalNumber)(undefined));
        chai_1.assert.isFalse((0, isNormalNumber_1.isNormalNumber)(null));
        chai_1.assert.isFalse((0, isNormalNumber_1.isNormalNumber)('123'));
        chai_1.assert.isFalse((0, isNormalNumber_1.isNormalNumber)(BigInt(123)));
    });
    it('returns false for Number objects, which should never be used', () => {
        // eslint-disable-next-line no-new-wrappers
        chai_1.assert.isFalse((0, isNormalNumber_1.isNormalNumber)(new Number(123)));
    });
    it('returns false for values that can be converted to numbers', () => {
        const obj = {
            [Symbol.toPrimitive]() {
                return 123;
            },
        };
        chai_1.assert.isFalse((0, isNormalNumber_1.isNormalNumber)(obj));
    });
    it('returns false for NaN', () => {
        chai_1.assert.isFalse((0, isNormalNumber_1.isNormalNumber)(NaN));
    });
    it('returns false for Infinity', () => {
        chai_1.assert.isFalse((0, isNormalNumber_1.isNormalNumber)(Infinity));
        chai_1.assert.isFalse((0, isNormalNumber_1.isNormalNumber)(-Infinity));
    });
    it('returns true for other numbers', () => {
        chai_1.assert.isTrue((0, isNormalNumber_1.isNormalNumber)(123));
        chai_1.assert.isTrue((0, isNormalNumber_1.isNormalNumber)(0));
        chai_1.assert.isTrue((0, isNormalNumber_1.isNormalNumber)(-1));
        chai_1.assert.isTrue((0, isNormalNumber_1.isNormalNumber)(0.12));
    });
});
