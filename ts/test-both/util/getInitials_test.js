"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const getInitials_1 = require("../../util/getInitials");
describe('getInitials', () => {
    it('returns undefined when passed undefined', () => {
        chai_1.assert.isUndefined((0, getInitials_1.getInitials)(undefined));
    });
    it('returns undefined when passed an empty string', () => {
        chai_1.assert.isUndefined((0, getInitials_1.getInitials)(''));
    });
    it('returns undefined when passed a string with no letters', () => {
        chai_1.assert.isUndefined((0, getInitials_1.getInitials)('123 !@#$%'));
    });
    it('returns the first letter of a name that is one ASCII word', () => {
        chai_1.assert.strictEqual((0, getInitials_1.getInitials)('Foo'), 'F');
        chai_1.assert.strictEqual((0, getInitials_1.getInitials)('Bo'), 'B');
    });
    [
        'Foo Bar',
        'foo bar',
        'F Bar',
        'Foo B',
        'FB',
        'F.B.',
        '0Foo 1Bar',
        "Foo B'Ar",
        'Foo Q Bar',
        'Foo Q. Bar',
        'Foo Qux Bar',
        'Foo "Qux" Bar',
        'Foo-Qux Bar',
        'Foo Bar-Qux',
        "Foo b'Arr",
    ].forEach(name => {
        it(`returns 'FB' for '${name}'`, () => {
            chai_1.assert.strictEqual((0, getInitials_1.getInitials)(name), 'FB');
        });
    });
    it('returns initials for languages with non-Latin alphabets', () => {
        chai_1.assert.strictEqual((0, getInitials_1.getInitials)('Иван Иванов'), 'ИИ');
        chai_1.assert.strictEqual((0, getInitials_1.getInitials)('山田 太郎'), '山太');
        chai_1.assert.strictEqual((0, getInitials_1.getInitials)('王五'), '王五');
    });
    it('returns initials for right-to-left languages', () => {
        chai_1.assert.strictEqual((0, getInitials_1.getInitials)('فلانة الفلانية'), 'فا');
        chai_1.assert.strictEqual((0, getInitials_1.getInitials)('ישראלה ישראלי'), 'יי');
    });
    it('returns initials with diacritical marks', () => {
        chai_1.assert.strictEqual((0, getInitials_1.getInitials)('Ḟoo Ḅar'), 'ḞḄ');
    });
});
