"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const parseRetryAfter_1 = require("../../util/parseRetryAfter");
describe('parseRetryAfter', () => {
    it('should return 1 second when passed non-strings', () => {
        chai_1.assert.equal((0, parseRetryAfter_1.parseRetryAfter)(undefined), 1000);
        chai_1.assert.equal((0, parseRetryAfter_1.parseRetryAfter)(1234), 1000);
    });
    it('should return 1 second with invalid strings', () => {
        chai_1.assert.equal((0, parseRetryAfter_1.parseRetryAfter)('nope'), 1000);
        chai_1.assert.equal((0, parseRetryAfter_1.parseRetryAfter)('1ff'), 1000);
    });
    it('should return milliseconds on valid input', () => {
        chai_1.assert.equal((0, parseRetryAfter_1.parseRetryAfter)('100'), 100000);
    });
    it('should return 1 second at minimum', () => {
        chai_1.assert.equal((0, parseRetryAfter_1.parseRetryAfter)('0'), 1000);
        chai_1.assert.equal((0, parseRetryAfter_1.parseRetryAfter)('-1'), 1000);
    });
});
