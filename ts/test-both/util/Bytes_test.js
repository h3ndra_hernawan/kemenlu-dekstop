"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Bytes = __importStar(require("../../Bytes"));
describe('Bytes', () => {
    it('converts to base64 and back', () => {
        const bytes = new Uint8Array([1, 2, 3]);
        const base64 = Bytes.toBase64(bytes);
        chai_1.assert.strictEqual(base64, 'AQID');
        chai_1.assert.deepEqual(Bytes.fromBase64(base64), bytes);
    });
    it('converts to hex and back', () => {
        const bytes = new Uint8Array([1, 2, 3]);
        const hex = Bytes.toHex(bytes);
        chai_1.assert.strictEqual(hex, '010203');
        chai_1.assert.deepEqual(Bytes.fromHex(hex), bytes);
    });
    it('converts to string and back', () => {
        const bytes = new Uint8Array([0x61, 0x62, 0x63]);
        const binary = Bytes.toString(bytes);
        chai_1.assert.strictEqual(binary, 'abc');
        chai_1.assert.deepEqual(Bytes.fromString(binary), bytes);
    });
    it('converts to binary and back', () => {
        const bytes = new Uint8Array([0xff, 0x01]);
        const binary = Bytes.toBinary(bytes);
        chai_1.assert.strictEqual(binary, '\xff\x01');
        chai_1.assert.deepEqual(Bytes.fromBinary(binary), bytes);
    });
    it('concatenates bytes', () => {
        const result = Bytes.concatenate([
            Bytes.fromString('hello'),
            Bytes.fromString(' '),
            Bytes.fromString('world'),
        ]);
        chai_1.assert.strictEqual(Bytes.toString(result), 'hello world');
    });
    describe('isEmpty', () => {
        it('returns true for `undefined`', () => {
            chai_1.assert.strictEqual(Bytes.isEmpty(undefined), true);
        });
        it('returns true for `null`', () => {
            chai_1.assert.strictEqual(Bytes.isEmpty(null), true);
        });
        it('returns true for an empty Uint8Array', () => {
            chai_1.assert.strictEqual(Bytes.isEmpty(new Uint8Array(0)), true);
        });
        it('returns false for not empty Uint8Array', () => {
            chai_1.assert.strictEqual(Bytes.isEmpty(new Uint8Array(123)), false);
        });
    });
    describe('isNotEmpty', () => {
        it('returns false for `undefined`', () => {
            chai_1.assert.strictEqual(Bytes.isNotEmpty(undefined), false);
        });
        it('returns false for `null`', () => {
            chai_1.assert.strictEqual(Bytes.isNotEmpty(null), false);
        });
        it('returns false for an empty Uint8Array', () => {
            chai_1.assert.strictEqual(Bytes.isNotEmpty(new Uint8Array(0)), false);
        });
        it('returns true for not empty Uint8Array', () => {
            chai_1.assert.strictEqual(Bytes.isNotEmpty(new Uint8Array(123)), true);
        });
    });
});
