"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const userLanguages_1 = require("../../util/userLanguages");
describe('user language utilities', () => {
    describe('formatAcceptLanguageHeader', () => {
        it('returns * if no languages are provided', () => {
            chai_1.assert.strictEqual((0, userLanguages_1.formatAcceptLanguageHeader)([]), '*');
        });
        it('formats one provided language', () => {
            chai_1.assert.strictEqual((0, userLanguages_1.formatAcceptLanguageHeader)(['en-US']), 'en-US');
        });
        it('formats three provided languages', () => {
            chai_1.assert.strictEqual((0, userLanguages_1.formatAcceptLanguageHeader)('abc'.split('')), 'a, b;q=0.9, c;q=0.8');
        });
        it('formats 10 provided languages', () => {
            chai_1.assert.strictEqual((0, userLanguages_1.formatAcceptLanguageHeader)('abcdefghij'.split('')), 'a, b;q=0.9, c;q=0.8, d;q=0.7, e;q=0.6, f;q=0.5, g;q=0.4, h;q=0.3, i;q=0.2, j;q=0.1');
        });
        it('formats 11 provided languages', () => {
            chai_1.assert.strictEqual((0, userLanguages_1.formatAcceptLanguageHeader)('abcdefghijk'.split('')), 'a, b;q=0.9, c;q=0.8, d;q=0.7, e;q=0.6, f;q=0.5, g;q=0.4, h;q=0.3, i;q=0.2, j;q=0.1, k;q=0.09');
        });
        it('formats 19 provided languages', () => {
            chai_1.assert.strictEqual((0, userLanguages_1.formatAcceptLanguageHeader)('abcdefghijklmnopqrs'.split('')), 'a, b;q=0.9, c;q=0.8, d;q=0.7, e;q=0.6, f;q=0.5, g;q=0.4, h;q=0.3, i;q=0.2, j;q=0.1, k;q=0.09, l;q=0.08, m;q=0.07, n;q=0.06, o;q=0.05, p;q=0.04, q;q=0.03, r;q=0.02, s;q=0.01');
        });
        it('formats 20 provided languages', () => {
            chai_1.assert.strictEqual((0, userLanguages_1.formatAcceptLanguageHeader)('abcdefghijklmnopqrst'.split('')), 'a, b;q=0.9, c;q=0.8, d;q=0.7, e;q=0.6, f;q=0.5, g;q=0.4, h;q=0.3, i;q=0.2, j;q=0.1, k;q=0.09, l;q=0.08, m;q=0.07, n;q=0.06, o;q=0.05, p;q=0.04, q;q=0.03, r;q=0.02, s;q=0.01, t;q=0.009');
        });
        it('only formats the first 28 languages', () => {
            const result = (0, userLanguages_1.formatAcceptLanguageHeader)('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''));
            chai_1.assert.include(result, 'B;q=0.001');
            chai_1.assert.notInclude(result, 'C');
            chai_1.assert.notInclude(result, 'D');
            chai_1.assert.notInclude(result, 'E');
            chai_1.assert.notInclude(result, 'Z');
        });
    });
    describe('getUserLanguages', () => {
        it('returns the fallback if no languages are provided', () => {
            chai_1.assert.deepEqual((0, userLanguages_1.getUserLanguages)([], 'fallback'), ['fallback']);
            chai_1.assert.deepEqual((0, userLanguages_1.getUserLanguages)(undefined, 'fallback'), ['fallback']);
        });
        it('returns the provided languages', () => {
            chai_1.assert.deepEqual((0, userLanguages_1.getUserLanguages)(['a', 'b', 'c'], 'x'), ['a', 'b', 'c']);
        });
    });
});
