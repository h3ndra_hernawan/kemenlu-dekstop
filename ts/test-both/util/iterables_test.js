"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const iterables_1 = require("../../util/iterables");
describe('iterable utilities', () => {
    describe('isIterable', () => {
        it('returns false for non-iterables', () => {
            chai_1.assert.isFalse((0, iterables_1.isIterable)(undefined));
            chai_1.assert.isFalse((0, iterables_1.isIterable)(null));
            chai_1.assert.isFalse((0, iterables_1.isIterable)(123));
            chai_1.assert.isFalse((0, iterables_1.isIterable)({ foo: 'bar' }));
            chai_1.assert.isFalse((0, iterables_1.isIterable)({
                length: 2,
                '0': 'fake',
                '1': 'array',
            }));
        });
        it('returns true for iterables', () => {
            chai_1.assert.isTrue((0, iterables_1.isIterable)('strings are iterable'));
            chai_1.assert.isTrue((0, iterables_1.isIterable)(['arrays too']));
            chai_1.assert.isTrue((0, iterables_1.isIterable)(new Set('and sets')));
            chai_1.assert.isTrue((0, iterables_1.isIterable)(new Map([['and', 'maps']])));
            chai_1.assert.isTrue((0, iterables_1.isIterable)({
                [Symbol.iterator]() {
                    return {
                        next() {
                            return {
                                value: 'endless iterable',
                                done: false,
                            };
                        },
                    };
                },
            }));
            chai_1.assert.isTrue((0, iterables_1.isIterable)((function* generators() {
                yield 123;
            })()));
        });
    });
    describe('repeat', () => {
        it('repeats the same value forever', () => {
            const result = (0, iterables_1.repeat)('foo');
            const truncated = [...(0, iterables_1.take)(result, 10)];
            chai_1.assert.deepEqual(truncated, Array(10).fill('foo'));
        });
    });
    describe('size', () => {
        it('returns the length of a string', () => {
            chai_1.assert.strictEqual((0, iterables_1.size)(''), 0);
            chai_1.assert.strictEqual((0, iterables_1.size)('hello world'), 11);
        });
        it('returns the length of an array', () => {
            chai_1.assert.strictEqual((0, iterables_1.size)([]), 0);
            chai_1.assert.strictEqual((0, iterables_1.size)(['hello', 'world']), 2);
        });
        it('returns the size of a set', () => {
            chai_1.assert.strictEqual((0, iterables_1.size)(new Set()), 0);
            chai_1.assert.strictEqual((0, iterables_1.size)(new Set([1, 2, 3])), 3);
        });
        it('returns the length (not byte length) of typed arrays', () => {
            chai_1.assert.strictEqual((0, iterables_1.size)(new Uint8Array(3)), 3);
            chai_1.assert.strictEqual((0, iterables_1.size)(new Uint32Array(3)), 3);
        });
        it('returns the size of arbitrary iterables', () => {
            function* someNumbers() {
                yield 3;
                yield 6;
                yield 9;
            }
            chai_1.assert.strictEqual((0, iterables_1.size)(someNumbers()), 3);
        });
    });
    describe('concat', () => {
        it('returns an empty iterable when passed nothing', () => {
            chai_1.assert.deepEqual([...(0, iterables_1.concat)()], []);
        });
        it('returns an empty iterable when passed empty iterables', () => {
            chai_1.assert.deepEqual([...(0, iterables_1.concat)([])], []);
            chai_1.assert.deepEqual([...(0, iterables_1.concat)(new Set())], []);
            chai_1.assert.deepEqual([...(0, iterables_1.concat)(new Set(), [], new Map())], []);
        });
        it('concatenates multiple iterables', () => {
            const everyNumber = {
                *[Symbol.iterator]() {
                    for (let i = 4; true; i += 1) {
                        yield i;
                    }
                },
            };
            const result = (0, iterables_1.concat)([1, 2], new Set([3]), [], everyNumber);
            const iterator = result[Symbol.iterator]();
            chai_1.assert.deepEqual(iterator.next(), { value: 1, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 2, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 3, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 4, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 5, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 6, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 7, done: false });
        });
        it("doesn't start the iterable until the last minute", () => {
            const oneTwoThree = {
                [Symbol.iterator]: sinon.fake(() => {
                    let n = 0;
                    return {
                        next() {
                            if (n > 3) {
                                return { done: true };
                            }
                            n += 1;
                            return { value: n, done: false };
                        },
                    };
                }),
            };
            const result = (0, iterables_1.concat)([1, 2], oneTwoThree);
            const iterator = result[Symbol.iterator]();
            sinon.assert.notCalled(oneTwoThree[Symbol.iterator]);
            iterator.next();
            sinon.assert.notCalled(oneTwoThree[Symbol.iterator]);
            iterator.next();
            sinon.assert.notCalled(oneTwoThree[Symbol.iterator]);
            iterator.next();
            sinon.assert.calledOnce(oneTwoThree[Symbol.iterator]);
            iterator.next();
            sinon.assert.calledOnce(oneTwoThree[Symbol.iterator]);
        });
    });
    describe('filter', () => {
        it('returns an empty iterable when passed an empty iterable', () => {
            const fn = sinon.fake();
            chai_1.assert.deepEqual([...(0, iterables_1.filter)([], fn)], []);
            chai_1.assert.deepEqual([...(0, iterables_1.filter)(new Set(), fn)], []);
            chai_1.assert.deepEqual([...(0, iterables_1.filter)(new Map(), fn)], []);
            sinon.assert.notCalled(fn);
        });
        it('returns a new iterator with some values removed', () => {
            const isOdd = sinon.fake((n) => Boolean(n % 2));
            const result = (0, iterables_1.filter)([1, 2, 3, 4], isOdd);
            sinon.assert.notCalled(isOdd);
            chai_1.assert.deepEqual([...result], [1, 3]);
            chai_1.assert.notInstanceOf(result, Array);
            sinon.assert.callCount(isOdd, 4);
        });
        it('can filter an infinite iterable', () => {
            const everyNumber = {
                *[Symbol.iterator]() {
                    for (let i = 0; true; i += 1) {
                        yield i;
                    }
                },
            };
            const isOdd = (n) => Boolean(n % 2);
            const result = (0, iterables_1.filter)(everyNumber, isOdd);
            const iterator = result[Symbol.iterator]();
            chai_1.assert.deepEqual(iterator.next(), { value: 1, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 3, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 5, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 7, done: false });
        });
        it('respects TypeScript type assertion signatures', () => {
            // This tests TypeScript, not the actual runtime behavior.
            function isString(value) {
                return typeof value === 'string';
            }
            const input = [1, 'two', 3, 'four'];
            const result = (0, iterables_1.filter)(input, isString);
            chai_1.assert.deepEqual([...result], ['two', 'four']);
        });
    });
    describe('find', () => {
        const isOdd = (n) => Boolean(n % 2);
        it('returns undefined if the value is not found', () => {
            chai_1.assert.isUndefined((0, iterables_1.find)([], isOdd));
            chai_1.assert.isUndefined((0, iterables_1.find)([2, 4], isOdd));
        });
        it('returns the first matching value', () => {
            chai_1.assert.strictEqual((0, iterables_1.find)([0, 1, 2, 3], isOdd), 1);
        });
        it('only iterates until a value is found', () => {
            function* numbers() {
                yield 2;
                yield 3;
                throw new Error('this should never happen');
            }
            (0, iterables_1.find)(numbers(), isOdd);
        });
    });
    describe('groupBy', () => {
        it('returns an empty object if passed an empty iterable', () => {
            const fn = sinon.fake();
            chai_1.assert.deepEqual((0, iterables_1.groupBy)([], fn), {});
            chai_1.assert.deepEqual((0, iterables_1.groupBy)(new Set(), fn), {});
            sinon.assert.notCalled(fn);
        });
        it('returns a map of groups', () => {
            chai_1.assert.deepEqual((0, iterables_1.groupBy)(['apple', 'aardvark', 'orange', 'orange', 'zebra'], str => str[0]), {
                a: ['apple', 'aardvark'],
                o: ['orange', 'orange'],
                z: ['zebra'],
            });
        });
    });
    describe('isEmpty', () => {
        it('returns true for empty iterables', () => {
            chai_1.assert.isTrue((0, iterables_1.isEmpty)(''));
            chai_1.assert.isTrue((0, iterables_1.isEmpty)([]));
            chai_1.assert.isTrue((0, iterables_1.isEmpty)(new Set()));
        });
        it('returns false for non-empty iterables', () => {
            chai_1.assert.isFalse((0, iterables_1.isEmpty)(' '));
            chai_1.assert.isFalse((0, iterables_1.isEmpty)([1, 2]));
            chai_1.assert.isFalse((0, iterables_1.isEmpty)(new Set([3, 4])));
        });
        it('does not "look past" the first element', () => {
            function* numbers() {
                yield 1;
                throw new Error('this should never happen');
            }
            chai_1.assert.isFalse((0, iterables_1.isEmpty)(numbers()));
        });
    });
    describe('map', () => {
        it('returns an empty iterable when passed an empty iterable', () => {
            const fn = sinon.fake();
            chai_1.assert.deepEqual([...(0, iterables_1.map)([], fn)], []);
            chai_1.assert.deepEqual([...(0, iterables_1.map)(new Set(), fn)], []);
            chai_1.assert.deepEqual([...(0, iterables_1.map)(new Map(), fn)], []);
            sinon.assert.notCalled(fn);
        });
        it('returns a new iterator with values mapped', () => {
            const fn = sinon.fake((n) => n * n);
            const result = (0, iterables_1.map)([1, 2, 3], fn);
            sinon.assert.notCalled(fn);
            chai_1.assert.deepEqual([...result], [1, 4, 9]);
            chai_1.assert.notInstanceOf(result, Array);
            sinon.assert.calledThrice(fn);
        });
        it('iterating doesn\'t "spend" the iterable', () => {
            const result = (0, iterables_1.map)([1, 2, 3], n => n * n);
            chai_1.assert.deepEqual([...result], [1, 4, 9]);
            chai_1.assert.deepEqual([...result], [1, 4, 9]);
            chai_1.assert.deepEqual([...result], [1, 4, 9]);
        });
        it('can map over an infinite iterable', () => {
            const everyNumber = {
                *[Symbol.iterator]() {
                    for (let i = 0; true; i += 1) {
                        yield i;
                    }
                },
            };
            const fn = sinon.fake((n) => n * n);
            const result = (0, iterables_1.map)(everyNumber, fn);
            const iterator = result[Symbol.iterator]();
            chai_1.assert.deepEqual(iterator.next(), { value: 0, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 1, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 4, done: false });
            chai_1.assert.deepEqual(iterator.next(), { value: 9, done: false });
        });
    });
    describe('reduce', () => {
        it('returns the accumulator when passed an empty iterable', () => {
            const fn = sinon.fake();
            chai_1.assert.strictEqual((0, iterables_1.reduce)([], fn, 123), 123);
            sinon.assert.notCalled(fn);
        });
        it('iterates over the iterable, ultimately returning a result', () => {
            chai_1.assert.strictEqual((0, iterables_1.reduce)(new Set([1, 2, 3, 4]), (result, n) => result + n, 89), 99);
        });
    });
    describe('take', () => {
        it('returns the first n elements from an iterable', () => {
            const everyNumber = {
                *[Symbol.iterator]() {
                    for (let i = 0; true; i += 1) {
                        yield i;
                    }
                },
            };
            chai_1.assert.deepEqual([...(0, iterables_1.take)(everyNumber, 0)], []);
            chai_1.assert.deepEqual([...(0, iterables_1.take)(everyNumber, 1)], [0]);
            chai_1.assert.deepEqual([...(0, iterables_1.take)(everyNumber, 7)], [0, 1, 2, 3, 4, 5, 6]);
        });
        it('stops after the iterable has been exhausted', () => {
            const set = new Set([1, 2, 3]);
            chai_1.assert.deepEqual([...(0, iterables_1.take)(set, 3)], [1, 2, 3]);
            chai_1.assert.deepEqual([...(0, iterables_1.take)(set, 4)], [1, 2, 3]);
            chai_1.assert.deepEqual([...(0, iterables_1.take)(set, 10000)], [1, 2, 3]);
        });
    });
    describe('zipObject', () => {
        it('zips up an object', () => {
            chai_1.assert.deepEqual((0, iterables_1.zipObject)(['foo', 'bar'], [1, 2]), { foo: 1, bar: 2 });
        });
        it('stops if the keys "run out" first', () => {
            chai_1.assert.deepEqual((0, iterables_1.zipObject)(['foo', 'bar'], [1, 2, 3, 4, 5, 6]), {
                foo: 1,
                bar: 2,
            });
        });
        it('stops if the values "run out" first', () => {
            chai_1.assert.deepEqual((0, iterables_1.zipObject)(['foo', 'bar', 'baz'], [1]), {
                foo: 1,
            });
        });
    });
});
