"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const waitBatcher_1 = require("../../util/waitBatcher");
describe('waitBatcher', () => {
    it('should schedule a full batch', async () => {
        const processBatch = sinon.fake.resolves(undefined);
        const batcher = (0, waitBatcher_1.createWaitBatcher)({
            name: 'test',
            wait: 10,
            maxSize: 2,
            processBatch,
        });
        await Promise.all([batcher.add(1), batcher.add(2)]);
        chai_1.assert.ok(processBatch.calledOnceWith([1, 2]), 'Full batch on first call');
    });
    it('should schedule a partial batch', async () => {
        const processBatch = sinon.fake.resolves(undefined);
        const batcher = (0, waitBatcher_1.createWaitBatcher)({
            name: 'test',
            wait: 10,
            maxSize: 2,
            processBatch,
        });
        await batcher.add(1);
        chai_1.assert.ok(processBatch.calledOnceWith([1]), 'Partial batch on timeout');
    });
    it('should flush a partial batch', async () => {
        const processBatch = sinon.fake.resolves(undefined);
        const batcher = (0, waitBatcher_1.createWaitBatcher)({
            name: 'test',
            wait: 10000,
            maxSize: 1000,
            processBatch,
        });
        await Promise.all([batcher.add(1), batcher.flushAndWait()]);
        chai_1.assert.ok(processBatch.calledOnceWith([1]), 'Partial batch on flushAndWait');
    });
    it('should flush a partial batch with new items added', async () => {
        const processBatch = sinon.fake.resolves(undefined);
        const batcher = (0, waitBatcher_1.createWaitBatcher)({
            name: 'test',
            wait: 10000,
            maxSize: 1000,
            processBatch,
        });
        await Promise.all([
            (async () => {
                await batcher.add(1);
                await batcher.add(2);
            })(),
            batcher.flushAndWait(),
        ]);
        (0, chai_1.assert)(processBatch.firstCall.calledWith([1]), 'First partial batch');
        (0, chai_1.assert)(processBatch.secondCall.calledWith([2]), 'Second partial batch');
        (0, chai_1.assert)(!processBatch.thirdCall);
    });
});
