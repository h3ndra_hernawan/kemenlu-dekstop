"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const enum_1 = require("../../util/enum");
describe('enum utils', () => {
    describe('makeEnumParser', () => {
        let Color;
        (function (Color) {
            Color["Red"] = "red";
            Color["Green"] = "green";
            Color["Blue"] = "blue";
        })(Color || (Color = {}));
        const parse = (0, enum_1.makeEnumParser)(Color, Color.Blue);
        it('returns a parser that returns the default value if passed a non-string', () => {
            [undefined, null, 0, 1, 123].forEach(serializedValue => {
                const result = parse(serializedValue);
                chai_1.assert.strictEqual(result, Color.Blue);
            });
        });
        it('returns a parser that returns the default value if passed a string not in the enum', () => {
            ['', 'garbage', 'RED'].forEach(serializedValue => {
                const result = parse(serializedValue);
                chai_1.assert.strictEqual(result, Color.Blue);
            });
        });
        it('returns a parser that parses enum values', () => {
            const result = parse('green');
            chai_1.assert.strictEqual(result, Color.Green);
        });
    });
});
