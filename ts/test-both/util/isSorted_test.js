"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isSorted_1 = require("../../util/isSorted");
describe('isSorted', () => {
    it('returns true for empty lists', () => {
        chai_1.assert.isTrue((0, isSorted_1.isSorted)([]));
    });
    it('returns true for one-element lists', () => {
        chai_1.assert.isTrue((0, isSorted_1.isSorted)([5]));
    });
    it('returns true for sorted lists', () => {
        chai_1.assert.isTrue((0, isSorted_1.isSorted)([1, 2]));
        chai_1.assert.isTrue((0, isSorted_1.isSorted)([1, 2, 2, 3]));
    });
    it('returns false for out-of-order lists', () => {
        chai_1.assert.isFalse((0, isSorted_1.isSorted)([2, 1]));
        chai_1.assert.isFalse((0, isSorted_1.isSorted)([1, 2, 2, 3, 0]));
    });
});
