"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const reallyJsonStringify_1 = require("../../util/reallyJsonStringify");
describe('reallyJsonStringify', () => {
    it('returns the same thing as JSON.stringify when JSON.stringify returns a string', () => {
        [
            null,
            true,
            false,
            0,
            -0,
            123,
            -Infinity,
            Infinity,
            NaN,
            '',
            'foo',
            [],
            [1],
            {},
            { hi: 5 },
            new Date(),
            new Set([1, 2, 3]),
            new Map([['foo', 'bar']]),
            Promise.resolve(123),
            {
                toJSON() {
                    return 'foo';
                },
            },
        ].forEach(value => {
            const expected = JSON.stringify(value);
            const actual = (0, reallyJsonStringify_1.reallyJsonStringify)(value);
            chai_1.assert.strictEqual(actual, expected);
            chai_1.assert.isString(actual);
        });
    });
    it('returns a string when JSON.stringify returns undefined', () => {
        const check = (value, expected) => {
            const actual = (0, reallyJsonStringify_1.reallyJsonStringify)(value);
            chai_1.assert.strictEqual(actual, expected);
            // This ensures that our test is set up correctly, not the code under test.
            chai_1.assert.isUndefined(JSON.stringify(value));
        };
        check(undefined, '[object Undefined]');
        check(Symbol('foo'), '[object Symbol]');
        check({
            toJSON() {
                return undefined;
            },
        }, '[object Object]');
    });
    it('returns a string when JSON.stringify would error', () => {
        const check = (value, expected) => {
            const actual = (0, reallyJsonStringify_1.reallyJsonStringify)(value);
            chai_1.assert.strictEqual(actual, expected);
            // This ensures that our test is set up correctly, not the code under test.
            chai_1.assert.throws(() => JSON.stringify(value));
        };
        check(BigInt(123), '[object BigInt]');
        const a = {};
        const b = { a };
        a.b = b;
        check(a, '[object Object]');
        check([a], '[object Array]');
        const bad = {
            toJSON() {
                throw new Error("don't even try to stringify me");
            },
        };
        check(bad, '[object Object]');
        check([bad], '[object Array]');
    });
});
