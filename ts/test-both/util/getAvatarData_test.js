"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const uuid_1 = require("uuid");
const getRandomColor_1 = require("../helpers/getRandomColor");
const getAvatarData_1 = require("../../util/getAvatarData");
describe('getAvatarData', () => {
    it('returns existing avatars if present', () => {
        const avatars = [
            {
                id: (0, uuid_1.v4)(),
                color: (0, getRandomColor_1.getRandomColor)(),
                text: 'Avatar A',
            },
            {
                id: (0, uuid_1.v4)(),
                color: (0, getRandomColor_1.getRandomColor)(),
                text: 'Avatar B',
            },
        ];
        chai_1.assert.strictEqual((0, getAvatarData_1.getAvatarData)({ avatars, type: 'private' }), avatars);
        chai_1.assert.strictEqual((0, getAvatarData_1.getAvatarData)({ avatars, type: 'group' }), avatars);
    });
    it('returns a non-empty array if no avatars are provided', () => {
        chai_1.assert.isNotEmpty((0, getAvatarData_1.getAvatarData)({ type: 'private' }));
        chai_1.assert.isNotEmpty((0, getAvatarData_1.getAvatarData)({ type: 'group' }));
        chai_1.assert.isNotEmpty((0, getAvatarData_1.getAvatarData)({ avatars: [], type: 'private' }));
        chai_1.assert.isNotEmpty((0, getAvatarData_1.getAvatarData)({ avatars: [], type: 'group' }));
    });
});
