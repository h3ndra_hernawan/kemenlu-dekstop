"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const theme_1 = require("../../util/theme");
describe('themeClassName', () => {
    it('returns "light-theme" when passed a light theme', () => {
        chai_1.assert.strictEqual((0, theme_1.themeClassName)(theme_1.Theme.Light), 'light-theme');
    });
    it('returns "dark-theme" when passed a dark theme', () => {
        chai_1.assert.strictEqual((0, theme_1.themeClassName)(theme_1.Theme.Dark), 'dark-theme');
    });
});
