"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const assignWithNoUnnecessaryAllocation_1 = require("../../util/assignWithNoUnnecessaryAllocation");
describe('assignWithNoUnnecessaryAllocation', () => {
    it('returns the same object if there are no modifications', () => {
        const empty = {};
        chai_1.assert.strictEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(empty, {}), empty);
        const obj = {
            foo: 'bar',
            baz: 'qux',
            und: undefined,
        };
        chai_1.assert.strictEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, {}), obj);
        chai_1.assert.strictEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, { foo: 'bar' }), obj);
        chai_1.assert.strictEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, { baz: 'qux' }), obj);
        chai_1.assert.strictEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, { und: undefined }), obj);
    });
    it('returns a new object if there are modifications', () => {
        const empty = {};
        chai_1.assert.deepEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(empty, { name: 'Bert' }), { name: 'Bert' });
        chai_1.assert.deepEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(empty, { age: 8 }), {
            age: 8,
        });
        chai_1.assert.deepEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(empty, { name: undefined }), {
            name: undefined,
        });
        const obj = { name: 'Ernie' };
        chai_1.assert.deepEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, { name: 'Big Bird' }), {
            name: 'Big Bird',
        });
        chai_1.assert.deepEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, { age: 9 }), {
            name: 'Ernie',
            age: 9,
        });
        chai_1.assert.deepEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, { age: undefined }), {
            name: 'Ernie',
            age: undefined,
        });
    });
    it('only performs a shallow comparison', () => {
        const obj = { foo: { bar: 'baz' } };
        chai_1.assert.notStrictEqual((0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, { foo: { bar: 'baz' } }), obj);
    });
    it("doesn't modify the original object when there are no modifications", () => {
        const empty = {};
        (0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(empty, {});
        chai_1.assert.deepEqual(empty, {});
        const obj = { foo: 'bar' };
        (0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, { foo: 'bar' });
        chai_1.assert.deepEqual(obj, { foo: 'bar' });
    });
    it("doesn't modify the original object when there are modifications", () => {
        const empty = {};
        (0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(empty, { name: 'Bert' });
        chai_1.assert.deepEqual(empty, {});
        const obj = { foo: 'bar' };
        (0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(obj, { foo: 'baz' });
        chai_1.assert.deepEqual(obj, { foo: 'bar' });
    });
});
