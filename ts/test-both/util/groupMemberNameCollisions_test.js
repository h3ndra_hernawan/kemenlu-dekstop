"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const getDefaultConversation_1 = require("../helpers/getDefaultConversation");
const groupMemberNameCollisions_1 = require("../../util/groupMemberNameCollisions");
describe('group member name collision utilities', () => {
    describe('dehydrateCollisionsWithConversations', () => {
        it('turns conversations into "plain" IDs', () => {
            const conversation1 = (0, getDefaultConversation_1.getDefaultConversation)();
            const conversation2 = (0, getDefaultConversation_1.getDefaultConversation)();
            const conversation3 = (0, getDefaultConversation_1.getDefaultConversation)();
            const conversation4 = (0, getDefaultConversation_1.getDefaultConversation)();
            const result = (0, groupMemberNameCollisions_1.dehydrateCollisionsWithConversations)({
                Alice: [conversation1, conversation2],
                Bob: [conversation3, conversation4],
            });
            chai_1.assert.deepEqual(result, {
                Alice: [conversation1.id, conversation2.id],
                Bob: [conversation3.id, conversation4.id],
            });
        });
    });
    describe('getCollisionsFromMemberships', () => {
        it('finds collisions by title, omitting some conversations', () => {
            const alice1 = (0, getDefaultConversation_1.getDefaultConversation)({
                profileName: 'Alice',
                title: 'Alice',
            });
            const alice2 = (0, getDefaultConversation_1.getDefaultConversation)({
                profileName: 'Alice',
                title: 'Alice',
            });
            const bob1 = (0, getDefaultConversation_1.getDefaultConversation)({
                profileName: 'Bob',
                title: 'Bob',
            });
            const bob2 = (0, getDefaultConversation_1.getDefaultConversation)({
                name: 'Bob In Your Contacts',
                profileName: 'Bob',
                title: 'Bob',
            });
            const bob3 = (0, getDefaultConversation_1.getDefaultConversation)({
                profileName: 'Bob',
                title: 'Bob',
            });
            // Ignored, because Bob is not in your contacts (lacks `name`), has no profile name,
            //   and has no E164.
            const ignoredBob = (0, getDefaultConversation_1.getDefaultConversation)({
                e164: undefined,
                title: 'Bob',
            });
            // Ignored, because there's only one Charlie.
            const charlie = (0, getDefaultConversation_1.getDefaultConversation)({
                profileName: 'Charlie',
                title: 'Charlie',
            });
            // Ignored, because all Donnas are in your contacts (they have a `name`).
            const donna1 = (0, getDefaultConversation_1.getDefaultConversation)({
                name: 'Donna One',
                profileName: 'Donna',
                title: 'Donna',
            });
            const donna2 = (0, getDefaultConversation_1.getDefaultConversation)({
                name: 'Donna Two',
                profileName: 'Donna',
                title: 'Donna',
            });
            const donna3 = (0, getDefaultConversation_1.getDefaultConversation)({
                name: 'Donna Three',
                profileName: 'Donna',
                title: 'Donna',
            });
            // Ignored, because you're not included.
            const me = (0, getDefaultConversation_1.getDefaultConversation)({
                isMe: true,
                profileName: 'Alice',
                title: 'Alice',
            });
            const memberships = [
                alice1,
                alice2,
                bob1,
                bob2,
                bob3,
                ignoredBob,
                charlie,
                donna1,
                donna2,
                donna3,
                me,
            ].map(member => ({ member }));
            const result = (0, groupMemberNameCollisions_1.getCollisionsFromMemberships)(memberships);
            chai_1.assert.deepEqual(result, {
                Alice: [alice1, alice2],
                Bob: [bob1, bob2, bob3],
            });
        });
    });
    describe('hasUnacknowledgedCollisions', () => {
        it('returns false if the collisions are identical', () => {
            chai_1.assert.isFalse((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({}, {}));
            chai_1.assert.isFalse((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({ Alice: ['abc', 'def'] }, { Alice: ['abc', 'def'] }));
            chai_1.assert.isFalse((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({ Alice: ['abc', 'def'] }, { Alice: ['def', 'abc'] }));
        });
        it('returns false if the acknowledged collisions are a superset of the current collisions', () => {
            chai_1.assert.isFalse((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({ Alice: ['abc', 'def'] }, {}));
            chai_1.assert.isFalse((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({ Alice: ['abc', 'def', 'geh'] }, { Alice: ['abc', 'geh'] }));
            chai_1.assert.isFalse((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({ Alice: ['abc', 'def'], Bob: ['ghi', 'jkl'] }, { Alice: ['abc', 'def'] }));
        });
        it('returns true if the current collisions has a title that was not acknowledged', () => {
            chai_1.assert.isTrue((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({ Alice: ['abc', 'def'], Bob: ['ghi', 'jkl'] }, {
                Alice: ['abc', 'def'],
                Bob: ['ghi', 'jkl'],
                Charlie: ['mno', 'pqr'],
            }));
            chai_1.assert.isTrue((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({ Alice: ['abc', 'def'], Bob: ['ghi', 'jkl'] }, {
                Alice: ['abc', 'def'],
                Charlie: ['mno', 'pqr'],
            }));
        });
        it('returns true if any title has a new ID', () => {
            chai_1.assert.isTrue((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({ Alice: ['abc', 'def'] }, { Alice: ['abc', 'def', 'ghi'] }));
            chai_1.assert.isTrue((0, groupMemberNameCollisions_1.hasUnacknowledgedCollisions)({ Alice: ['abc', 'def'] }, { Alice: ['abc', 'ghi'] }));
        });
    });
    describe('invertIdsByTitle', () => {
        it('returns an empty object if passed no IDs', () => {
            chai_1.assert.deepEqual((0, groupMemberNameCollisions_1.invertIdsByTitle)({}), {});
            chai_1.assert.deepEqual((0, groupMemberNameCollisions_1.invertIdsByTitle)({ Alice: [] }), {});
        });
        it('returns an object with ID keys and title values', () => {
            chai_1.assert.deepEqual((0, groupMemberNameCollisions_1.invertIdsByTitle)({ Alice: ['abc', 'def'], Bob: ['ghi', 'jkl', 'mno'] }), {
                abc: 'Alice',
                def: 'Alice',
                ghi: 'Bob',
                jkl: 'Bob',
                mno: 'Bob',
            });
        });
    });
});
