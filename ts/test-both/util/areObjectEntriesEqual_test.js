"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const areObjectEntriesEqual_1 = require("../../util/areObjectEntriesEqual");
describe('areObjectEntriesEqual', () => {
    const empty = {};
    const foo = { foo: 1 };
    const bar = { bar: 2 };
    const undefinedEntries = { foo: undefined, bar: undefined };
    it('returns true for an empty list of keys', () => {
        chai_1.assert.isTrue((0, areObjectEntriesEqual_1.areObjectEntriesEqual)({}, {}, []));
        chai_1.assert.isTrue((0, areObjectEntriesEqual_1.areObjectEntriesEqual)(foo, foo, []));
        chai_1.assert.isTrue((0, areObjectEntriesEqual_1.areObjectEntriesEqual)(foo, bar, []));
    });
    it('returns true for empty objects', () => {
        chai_1.assert.isTrue((0, areObjectEntriesEqual_1.areObjectEntriesEqual)(empty, empty, ['foo']));
    });
    it('considers missing keys equal to undefined keys', () => {
        chai_1.assert.isTrue((0, areObjectEntriesEqual_1.areObjectEntriesEqual)(empty, undefinedEntries, ['foo', 'bar']));
    });
    it('ignores unspecified properties', () => {
        chai_1.assert.isTrue((0, areObjectEntriesEqual_1.areObjectEntriesEqual)({ x: 1, y: 2 }, { x: 1, y: 3 }, ['x']));
    });
    it('returns false for different objects', () => {
        chai_1.assert.isFalse((0, areObjectEntriesEqual_1.areObjectEntriesEqual)({ x: 1 }, { x: 2 }, ['x']));
        chai_1.assert.isFalse((0, areObjectEntriesEqual_1.areObjectEntriesEqual)({ x: 1, y: 2 }, { x: 1, y: 3 }, ['x', 'y']));
    });
    it('only performs a shallow check', () => {
        chai_1.assert.isFalse((0, areObjectEntriesEqual_1.areObjectEntriesEqual)({ x: [1, 2] }, { x: [1, 2] }, ['x']));
    });
});
