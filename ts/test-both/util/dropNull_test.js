"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
const chai_1 = require("chai");
const dropNull_1 = require("../../util/dropNull");
describe('dropNull', () => {
    it('swaps null with undefined', () => {
        chai_1.assert.strictEqual((0, dropNull_1.dropNull)(null), undefined);
    });
    it('leaves undefined be', () => {
        chai_1.assert.strictEqual((0, dropNull_1.dropNull)(undefined), undefined);
    });
    it('non-null values undefined be', () => {
        chai_1.assert.strictEqual((0, dropNull_1.dropNull)('test'), 'test');
    });
    describe('shallowDropNull', () => {
        it('return undefined with given null', () => {
            chai_1.assert.strictEqual((0, dropNull_1.shallowDropNull)(null), undefined);
        });
        it('return undefined with given undefined', () => {
            chai_1.assert.strictEqual((0, dropNull_1.shallowDropNull)(undefined), undefined);
        });
        it('swaps null with undefined', () => {
            const result = (0, dropNull_1.shallowDropNull)({
                a: null,
                b: 1,
            });
            chai_1.assert.deepStrictEqual(result, { a: undefined, b: 1 });
        });
        it('leaves undefined be', () => {
            const result = (0, dropNull_1.shallowDropNull)({
                a: 1,
                b: undefined,
            });
            chai_1.assert.deepStrictEqual(result, { a: 1, b: undefined });
        });
    });
});
