"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const lodash_1 = require("lodash");
const AbortableProcess_1 = require("../../util/AbortableProcess");
describe('AbortableProcess', () => {
    it('resolves the result normally', async () => {
        const process = new AbortableProcess_1.AbortableProcess('process', { abort: lodash_1.noop }, Promise.resolve(42));
        chai_1.assert.strictEqual(await process.getResult(), 42);
    });
    it('rejects normally', async () => {
        const process = new AbortableProcess_1.AbortableProcess('process', { abort: lodash_1.noop }, Promise.reject(new Error('rejected')));
        await chai_1.assert.isRejected(process.getResult(), 'rejected');
    });
    it('rejects on abort', async () => {
        let calledAbort = false;
        const process = new AbortableProcess_1.AbortableProcess('A', {
            abort() {
                calledAbort = true;
            },
        }, new Promise(lodash_1.noop));
        process.abort();
        await chai_1.assert.isRejected(process.getResult(), 'Process "A" was aborted');
        chai_1.assert.isTrue(calledAbort);
    });
});
