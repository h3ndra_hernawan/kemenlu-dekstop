"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const getHSL_1 = require("../../util/getHSL");
describe('getHSL', () => {
    it('returns expected lightness values', () => {
        const saturation = 100;
        chai_1.assert.equal((0, getHSL_1.getHSL)({ hue: 0, saturation }), 'hsl(0, 100%, 45%)');
        chai_1.assert.equal((0, getHSL_1.getHSL)({ hue: 60, saturation }), 'hsl(60, 100%, 30%)');
        chai_1.assert.equal((0, getHSL_1.getHSL)({ hue: 90, saturation }), 'hsl(90, 100%, 30%)');
        chai_1.assert.equal((0, getHSL_1.getHSL)({ hue: 180, saturation }), 'hsl(180, 100%, 30%)');
        chai_1.assert.equal((0, getHSL_1.getHSL)({ hue: 240, saturation }), 'hsl(240, 100%, 50%)');
        chai_1.assert.equal((0, getHSL_1.getHSL)({ hue: 300, saturation }), 'hsl(300, 100%, 40%)');
        chai_1.assert.equal((0, getHSL_1.getHSL)({ hue: 360, saturation }), 'hsl(360, 100%, 45%)');
    });
    it('calculates lightness between values', () => {
        chai_1.assert.equal((0, getHSL_1.getHSL)({ hue: 210, saturation: 100 }), 'hsl(210, 100%, 40%)');
    });
});
