"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const getClassNamesFor_1 = require("../../util/getClassNamesFor");
describe('getClassNamesFor', () => {
    it('returns a function', () => {
        const f = (0, getClassNamesFor_1.getClassNamesFor)('hello-world');
        chai_1.assert.isFunction(f);
    });
    it('returns a function that adds a modifier', () => {
        const f = (0, getClassNamesFor_1.getClassNamesFor)('module');
        chai_1.assert.equal(f('__modifier'), 'module__modifier');
    });
    it('does not add anything if there is no modifier', () => {
        const f = (0, getClassNamesFor_1.getClassNamesFor)('module');
        chai_1.assert.equal(f(), '');
        chai_1.assert.equal(f(undefined && '__modifier'), '');
    });
    it('but does return the top level module if the modifier is empty string', () => {
        const f = (0, getClassNamesFor_1.getClassNamesFor)('module1', 'module2');
        chai_1.assert.equal(f(''), 'module1 module2');
    });
    it('adds multiple class names', () => {
        const f = (0, getClassNamesFor_1.getClassNamesFor)('module1', 'module2', 'module3');
        chai_1.assert.equal(f('__modifier'), 'module1__modifier module2__modifier module3__modifier');
    });
    it('skips parent modules that are undefined', () => {
        const f = (0, getClassNamesFor_1.getClassNamesFor)('module1', undefined, 'module3');
        chai_1.assert.equal(f('__modifier'), 'module1__modifier module3__modifier');
    });
});
