"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const UUID_1 = require("../../types/UUID");
const getDefaultConversation_1 = require("../helpers/getDefaultConversation");
const getGroupMemberships_1 = require("../../util/getGroupMemberships");
describe('getGroupMemberships', () => {
    const normalConversation1 = (0, getDefaultConversation_1.getDefaultConversationWithUuid)();
    const normalConversation2 = (0, getDefaultConversation_1.getDefaultConversationWithUuid)();
    const unregisteredConversation = (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
        discoveredUnregisteredAt: Date.now(),
    });
    function getConversationByUuid(uuid) {
        return [
            normalConversation1,
            normalConversation2,
            unregisteredConversation,
        ].find(conversation => conversation.uuid === uuid);
    }
    describe('memberships', () => {
        it('returns an empty array if passed undefined', () => {
            const conversation = {};
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).memberships;
            chai_1.assert.isEmpty(result);
        });
        it('returns an empty array if passed an empty array', () => {
            const conversation = { memberships: [] };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).memberships;
            chai_1.assert.isEmpty(result);
        });
        it("filters out conversation IDs that don't exist", () => {
            const conversation = {
                memberships: [
                    {
                        uuid: UUID_1.UUID.generate().toString(),
                        isAdmin: true,
                    },
                ],
            };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).memberships;
            chai_1.assert.isEmpty(result);
        });
        it('does not filter out unregistered conversations', () => {
            const conversation = {
                memberships: [
                    {
                        uuid: unregisteredConversation.uuid,
                        isAdmin: true,
                    },
                ],
            };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).memberships;
            chai_1.assert.lengthOf(result, 1);
            chai_1.assert.deepEqual(result[0], {
                isAdmin: true,
                member: unregisteredConversation,
            });
        });
        it('hydrates memberships', () => {
            const conversation = {
                memberships: [
                    {
                        uuid: normalConversation2.uuid,
                        isAdmin: false,
                    },
                    {
                        uuid: normalConversation1.uuid,
                        isAdmin: true,
                    },
                ],
            };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).memberships;
            chai_1.assert.lengthOf(result, 2);
            chai_1.assert.deepEqual(result[0], {
                isAdmin: false,
                member: normalConversation2,
            });
            chai_1.assert.deepEqual(result[1], {
                isAdmin: true,
                member: normalConversation1,
            });
        });
    });
    describe('pendingApprovalMemberships', () => {
        it('returns an empty array if passed undefined', () => {
            const conversation = {};
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingApprovalMemberships;
            chai_1.assert.isEmpty(result);
        });
        it('returns an empty array if passed an empty array', () => {
            const conversation = { pendingApprovalMemberships: [] };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingApprovalMemberships;
            chai_1.assert.isEmpty(result);
        });
        it("filters out conversation IDs that don't exist", () => {
            const conversation = {
                pendingApprovalMemberships: [{ uuid: UUID_1.UUID.generate().toString() }],
            };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingApprovalMemberships;
            chai_1.assert.isEmpty(result);
        });
        it('filters out unregistered conversations', () => {
            const conversation = {
                pendingApprovalMemberships: [{ uuid: unregisteredConversation.uuid }],
            };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingApprovalMemberships;
            chai_1.assert.isEmpty(result);
        });
        it('hydrates pending-approval memberships', () => {
            const conversation = {
                pendingApprovalMemberships: [
                    { uuid: normalConversation2.uuid },
                    { uuid: normalConversation1.uuid },
                ],
            };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingApprovalMemberships;
            chai_1.assert.lengthOf(result, 2);
            chai_1.assert.deepEqual(result[0], { member: normalConversation2 });
            chai_1.assert.deepEqual(result[1], { member: normalConversation1 });
        });
    });
    describe('pendingMemberships', () => {
        it('returns an empty array if passed undefined', () => {
            const conversation = {};
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingMemberships;
            chai_1.assert.isEmpty(result);
        });
        it('returns an empty array if passed an empty array', () => {
            const conversation = { pendingMemberships: [] };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingMemberships;
            chai_1.assert.isEmpty(result);
        });
        it("filters out conversation IDs that don't exist", () => {
            const conversation = {
                pendingMemberships: [
                    {
                        uuid: UUID_1.UUID.generate().toString(),
                        addedByUserId: normalConversation1.uuid,
                    },
                ],
            };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingMemberships;
            chai_1.assert.isEmpty(result);
        });
        it('filters out unregistered conversations', () => {
            const conversation = {
                pendingMemberships: [
                    {
                        uuid: unregisteredConversation.uuid,
                        addedByUserId: normalConversation1.uuid,
                    },
                ],
            };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingMemberships;
            chai_1.assert.isEmpty(result);
        });
        it('hydrates pending memberships', () => {
            const abc = UUID_1.UUID.generate().toString();
            const xyz = UUID_1.UUID.generate().toString();
            const conversation = {
                pendingMemberships: [
                    { uuid: normalConversation2.uuid, addedByUserId: abc },
                    { uuid: normalConversation1.uuid, addedByUserId: xyz },
                ],
            };
            const result = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid).pendingMemberships;
            chai_1.assert.lengthOf(result, 2);
            chai_1.assert.deepEqual(result[0], {
                member: normalConversation2,
                metadata: { addedByUserId: abc },
            });
            chai_1.assert.deepEqual(result[1], {
                member: normalConversation1,
                metadata: { addedByUserId: xyz },
            });
        });
    });
});
