"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Privacy = __importStar(require("../../util/privacy"));
const privacy_1 = require("../../util/privacy");
Privacy.addSensitivePath('sensitive-path');
describe('Privacy', () => {
    describe('redactPhoneNumbers', () => {
        it('should redact all phone numbers', () => {
            const text = 'This is a log line with a phone number +12223334455\n' +
                'and another one +13334445566';
            const actual = Privacy.redactPhoneNumbers(text);
            const expected = 'This is a log line with a phone number +[REDACTED]455\n' +
                'and another one +[REDACTED]566';
            chai_1.assert.equal(actual, expected);
        });
    });
    describe('redactUuids', () => {
        it('should redact all uuids', () => {
            const text = 'This is a log line with a uuid 9e420799-acdf-4bf4-8dee-353d7e2096b4\n' +
                'and another one IN ALL UPPERCASE 340727FB-E43A-413B-941B-AADA033B6CA3';
            const actual = Privacy.redactUuids(text);
            const expected = 'This is a log line with a uuid [REDACTED]6b4\n' +
                'and another one IN ALL UPPERCASE [REDACTED]CA3';
            chai_1.assert.equal(actual, expected);
        });
    });
    describe('redactGroupIds', () => {
        it('should redact all group IDs', () => {
            const text = 'This is a log line with two group IDs: group(123456789)\n' +
                'and group(abcdefghij)';
            const actual = Privacy.redactGroupIds(text);
            const expected = 'This is a log line with two group IDs: group([REDACTED]789)\n' +
                'and group([REDACTED]hij)';
            chai_1.assert.equal(actual, expected);
        });
        it('should remove newlines from redacted group IDs', () => {
            const text = 'This is a log line with two group IDs: group(12345678\n9)\n' +
                'and group(abc\ndefghij)';
            const actual = Privacy.redactGroupIds(text);
            const expected = 'This is a log line with two group IDs: group([REDACTED]789)\n' +
                'and group([REDACTED]hij)';
            chai_1.assert.equal(actual, expected);
        });
        it('should remove newlines from redacted group V2 IDs', () => {
            const text = 'This is a log line with three group IDs: groupv2(abcd32341a==)\n' +
                'and groupv2(abcd32341ad=) and and groupv2(abcd32341ade)';
            const actual = Privacy.redactGroupIds(text);
            const expected = 'This is a log line with three group IDs: groupv2([REDACTED]41a==)\n' +
                'and groupv2([REDACTED]1ad=) and and groupv2([REDACTED]ade)';
            chai_1.assert.equal(actual, expected);
        });
    });
    describe('redactAll', () => {
        it('should redact all sensitive information', () => {
            const encodedAppRootPath = privacy_1.APP_ROOT_PATH.replace(/ /g, '%20');
            const text = 'This is a log line with sensitive information:\n' +
                `path1 ${privacy_1.APP_ROOT_PATH}/main.js\n` +
                'phone1 +12223334455 ipsum\n' +
                'group1 group(123456789) doloret\n' +
                `path2 file:///${encodedAppRootPath}/js/background.js.` +
                'phone2 +13334445566 lorem\n' +
                'group2 group(abcdefghij) doloret\n' +
                'path3 sensitive-path/attachment.noindex\n';
            const actual = Privacy.redactAll(text);
            const expected = 'This is a log line with sensitive information:\n' +
                'path1 [REDACTED]/main.js\n' +
                'phone1 +[REDACTED]455 ipsum\n' +
                'group1 group([REDACTED]789) doloret\n' +
                'path2 file:///[REDACTED]/js/background.js.' +
                'phone2 +[REDACTED]566 lorem\n' +
                'group2 group([REDACTED]hij) doloret\n' +
                'path3 [REDACTED]/attachment.noindex\n';
            chai_1.assert.equal(actual, expected);
        });
    });
    describe('_redactPath', () => {
        it('should redact file paths', () => {
            const testPath = '/Users/meow/Library/Application Support/Signal Beta';
            const text = 'This is a log line with sensitive information:\n' +
                `path1 ${testPath}/main.js\n` +
                'phone1 +12223334455 ipsum\n';
            const actual = Privacy._redactPath(testPath)(text);
            const expected = 'This is a log line with sensitive information:\n' +
                'path1 [REDACTED]/main.js\n' +
                'phone1 +12223334455 ipsum\n';
            chai_1.assert.equal(actual, expected);
        });
        it('should redact URL-encoded paths', () => {
            const testPath = '/Users/meow/Library/Application Support/Signal Beta';
            const encodedTestPath = encodeURI(testPath);
            const text = 'This is a log line with sensitive information:\n' +
                `path1 ${testPath}/main.js\n` +
                'phone1 +12223334455 ipsum\n' +
                'group1 group(123456789) doloret\n' +
                `path2 file:///${encodedTestPath}/js/background.js.`;
            const actual = Privacy._redactPath(testPath)(text);
            const expected = 'This is a log line with sensitive information:\n' +
                'path1 [REDACTED]/main.js\n' +
                'phone1 +12223334455 ipsum\n' +
                'group1 group(123456789) doloret\n' +
                'path2 file:///[REDACTED]/js/background.js.';
            chai_1.assert.equal(actual, expected);
        });
        it('should redact stack traces with both forward and backslashes', () => {
            const testPath = 'C:/Users/Meow/AppData/Local/Programs/signal-desktop-beta';
            const modifiedTestPath = 'C:\\Users\\Meow\\AppData\\Local\\Programs\\signal-desktop-beta';
            const text = 'This is a log line with sensitive information:\n' +
                `path1 ${testPath}\\main.js\n` +
                'phone1 +12223334455 ipsum\n' +
                'group1 group(123456789) doloret\n' +
                `path2 ${modifiedTestPath}\\js\\background.js.`;
            const actual = Privacy._redactPath(testPath)(text);
            const expected = 'This is a log line with sensitive information:\n' +
                'path1 [REDACTED]\\main.js\n' +
                'phone1 +12223334455 ipsum\n' +
                'group1 group(123456789) doloret\n' +
                'path2 [REDACTED]\\js\\background.js.';
            chai_1.assert.equal(actual, expected);
        });
        it('should redact stack traces with escaped backslashes', () => {
            const testPath = 'C:\\Users\\Meow\\AppData\\Local\\Programs\\signal-desktop-beta';
            const modifiedTestPath = 'C:\\\\Users\\\\Meow\\\\AppData\\\\Local\\\\Programs\\\\signal-desktop-beta';
            const text = 'This is a log line with sensitive information:\n' +
                `path1 ${testPath}\\main.js\n` +
                'phone1 +12223334455 ipsum\n' +
                'group1 group(123456789) doloret\n' +
                `path2 ${modifiedTestPath}\\js\\background.js.`;
            const actual = Privacy._redactPath(testPath)(text);
            const expected = 'This is a log line with sensitive information:\n' +
                'path1 [REDACTED]\\main.js\n' +
                'phone1 +12223334455 ipsum\n' +
                'group1 group(123456789) doloret\n' +
                'path2 [REDACTED]\\js\\background.js.';
            chai_1.assert.equal(actual, expected);
        });
    });
});
