"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const getCustomColorStyle_1 = require("../../util/getCustomColorStyle");
describe('getCustomColorStyle', () => {
    it('returns undefined if no color passed in', () => {
        chai_1.assert.isUndefined((0, getCustomColorStyle_1.getCustomColorStyle)());
    });
    it('returns backgroundColor for solid colors', () => {
        const color = {
            start: {
                hue: 90,
                saturation: 100,
            },
        };
        chai_1.assert.deepEqual((0, getCustomColorStyle_1.getCustomColorStyle)(color), {
            backgroundColor: 'hsl(90, 100%, 30%)',
        });
    });
    it('returns backgroundImage with linear-gradient for gradients', () => {
        const color = {
            start: {
                hue: 90,
                saturation: 100,
            },
            end: {
                hue: 180,
                saturation: 50,
            },
            deg: 270,
        };
        chai_1.assert.deepEqual((0, getCustomColorStyle_1.getCustomColorStyle)(color), {
            backgroundImage: 'linear-gradient(0deg, hsl(90, 100%, 30%), hsl(180, 50%, 30%))',
        });
    });
});
