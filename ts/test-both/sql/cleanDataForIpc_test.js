"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const lodash_1 = require("lodash");
const cleanDataForIpc_1 = require("../../sql/cleanDataForIpc");
describe('cleanDataForIpc', () => {
    it('does nothing to JSON primitives', () => {
        ['', 'foo bar', 0, 123, true, false, null].forEach(value => {
            chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(value), {
                cleaned: value,
                pathsChanged: [],
            });
        });
    });
    it('does nothing to undefined', () => {
        // Though `undefined` is not technically JSON-serializable, we don't clean it because
        //   its key is dropped.
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(undefined), {
            cleaned: undefined,
            pathsChanged: [],
        });
    });
    it('converts BigInts to strings', () => {
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(BigInt(0)), {
            cleaned: '0',
            pathsChanged: ['root'],
        });
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(BigInt(123)), {
            cleaned: '123',
            pathsChanged: ['root'],
        });
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(BigInt(-123)), {
            cleaned: '-123',
            pathsChanged: ['root'],
        });
    });
    it('converts functions to `undefined` but does not mark them as cleaned, for backwards compatibility', () => {
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(lodash_1.noop), {
            cleaned: undefined,
            pathsChanged: [],
        });
    });
    it('converts symbols to `undefined`', () => {
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(Symbol('test')), {
            cleaned: undefined,
            pathsChanged: ['root'],
        });
    });
    it('converts ArrayBuffers to `undefined`', () => {
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(new ArrayBuffer(2)), {
            cleaned: undefined,
            pathsChanged: ['root'],
        });
    });
    it('keeps Buffers in a field', () => {
        const buffer = new Uint8Array([0xaa, 0xbb, 0xcc]);
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(buffer), {
            cleaned: buffer,
            pathsChanged: [],
        });
    });
    it('converts valid dates to ISO strings', () => {
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(new Date(924588548000)), {
            cleaned: '1999-04-20T06:09:08.000Z',
            pathsChanged: ['root'],
        });
    });
    it('converts invalid dates to `undefined`', () => {
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(new Date(NaN)), {
            cleaned: undefined,
            pathsChanged: ['root'],
        });
    });
    it('converts other iterables to arrays', () => {
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(new Float32Array([1, 2, 3])), {
            cleaned: [1, 2, 3],
            pathsChanged: ['root'],
        });
        function* generator() {
            yield 1;
            yield 2;
        }
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)(generator()), {
            cleaned: [1, 2],
            pathsChanged: ['root'],
        });
    });
    it('deeply cleans arrays, removing `undefined` and `null`s', () => {
        const result = (0, cleanDataForIpc_1.cleanDataForIpc)([
            12,
            Symbol('top level symbol'),
            { foo: 3, symb: Symbol('nested symbol 1') },
            [45, Symbol('nested symbol 2')],
            undefined,
            null,
        ]);
        chai_1.assert.deepEqual(result.cleaned, [
            12,
            undefined,
            {
                foo: 3,
                symb: undefined,
            },
            [45, undefined],
        ]);
        chai_1.assert.sameMembers(result.pathsChanged, [
            'root.1',
            'root.2.symb',
            'root.3.1',
            'root.4',
            'root.5',
        ]);
    });
    it('deeply cleans sets and converts them to arrays', () => {
        const result = (0, cleanDataForIpc_1.cleanDataForIpc)(new Set([
            12,
            Symbol('top level symbol'),
            { foo: 3, symb: Symbol('nested symbol 1') },
            [45, Symbol('nested symbol 2')],
        ]));
        chai_1.assert.isArray(result.cleaned);
        chai_1.assert.sameDeepMembers(result.cleaned, [
            12,
            undefined,
            {
                foo: 3,
                symb: undefined,
            },
            [45, undefined],
        ]);
        chai_1.assert.sameMembers(result.pathsChanged, [
            'root',
            'root.<iterator index 1>',
            'root.<iterator index 2>.symb',
            'root.<iterator index 3>.1',
        ]);
    });
    it('deeply cleans maps and converts them to objects', () => {
        const result = (0, cleanDataForIpc_1.cleanDataForIpc)(new Map([
            ['key 1', 'value'],
            [Symbol('symbol key'), 'dropped'],
            ['key 2', ['foo', Symbol('nested symbol')]],
            [3, 'dropped'],
            [BigInt(4), 'dropped'],
        ]));
        chai_1.assert.deepEqual(result.cleaned, {
            'key 1': 'value',
            'key 2': ['foo', undefined],
        });
        chai_1.assert.sameMembers(result.pathsChanged, [
            'root',
            'root.<map key Symbol(symbol key)>',
            'root.<map value at key 2>.1',
            'root.<map key 3>',
            'root.<map key 4>',
        ]);
    });
    it('calls `toNumber` when available', () => {
        chai_1.assert.deepEqual((0, cleanDataForIpc_1.cleanDataForIpc)([
            {
                toNumber() {
                    return 5;
                },
            },
            {
                toNumber() {
                    return Symbol('bogus');
                },
            },
        ]), {
            cleaned: [5, undefined],
            pathsChanged: ['root.1'],
        });
    });
    it('deeply cleans objects with a `null` prototype', () => {
        const value = Object.assign(Object.create(null), {
            'key 1': 'value',
            [Symbol('symbol key')]: 'dropped',
            'key 2': ['foo', Symbol('nested symbol')],
        });
        const result = (0, cleanDataForIpc_1.cleanDataForIpc)(value);
        chai_1.assert.deepEqual(result.cleaned, {
            'key 1': 'value',
            'key 2': ['foo', undefined],
        });
        chai_1.assert.sameMembers(result.pathsChanged, ['root.key 2.1']);
    });
    it('deeply cleans objects with a prototype of `Object.prototype`', () => {
        const value = {
            'key 1': 'value',
            [Symbol('symbol key')]: 'dropped',
            'key 2': ['foo', Symbol('nested symbol')],
        };
        const result = (0, cleanDataForIpc_1.cleanDataForIpc)(value);
        chai_1.assert.deepEqual(result.cleaned, {
            'key 1': 'value',
            'key 2': ['foo', undefined],
        });
        chai_1.assert.sameMembers(result.pathsChanged, ['root.key 2.1']);
    });
    it('deeply cleans class instances', () => {
        class Person {
            constructor(firstName, lastName) {
                this.firstName = firstName;
                this.lastName = lastName;
                this.toBeDiscarded = Symbol('to be discarded');
            }
            get name() {
                return this.getName();
            }
            getName() {
                return `${this.firstName} ${this.lastName}`;
            }
        }
        const person = new Person('Selena', 'Gomez');
        const result = (0, cleanDataForIpc_1.cleanDataForIpc)(person);
        chai_1.assert.deepEqual(result.cleaned, {
            firstName: 'Selena',
            lastName: 'Gomez',
            toBeDiscarded: undefined,
        });
        chai_1.assert.sameMembers(result.pathsChanged, ['root', 'root.toBeDiscarded']);
    });
});
