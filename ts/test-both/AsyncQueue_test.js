"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const AsyncQueue_1 = require("../util/AsyncQueue");
describe('AsyncQueue', () => {
    it('yields values as they are added, even if they were added before consuming', async () => {
        const queue = new AsyncQueue_1.AsyncQueue();
        queue.add(1);
        queue.add(2);
        const resultPromise = (async () => {
            var e_1, _a;
            const results = [];
            try {
                for (var queue_1 = __asyncValues(queue), queue_1_1; queue_1_1 = await queue_1.next(), !queue_1_1.done;) {
                    const value = queue_1_1.value;
                    results.push(value);
                    if (value === 4) {
                        break;
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (queue_1_1 && !queue_1_1.done && (_a = queue_1.return)) await _a.call(queue_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            return results;
        })();
        queue.add(3);
        queue.add(4);
        // Ignored, because we should've stopped iterating.
        queue.add(5);
        chai_1.assert.deepEqual(await resultPromise, [1, 2, 3, 4]);
    });
});
