"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Avatar_1 = require("../../types/Avatar");
describe('Avatar', () => {
    describe('getDefaultAvatars', () => {
        it('returns an array of valid avatars for direct conversations', () => {
            chai_1.assert.isNotEmpty((0, Avatar_1.getDefaultAvatars)(false));
        });
        it('returns an array of valid avatars for group conversations', () => {
            chai_1.assert.isNotEmpty((0, Avatar_1.getDefaultAvatars)(true));
        });
        it('defaults to returning avatars for direct conversations', () => {
            const defaultResult = (0, Avatar_1.getDefaultAvatars)();
            const directResult = (0, Avatar_1.getDefaultAvatars)(false);
            const groupResult = (0, Avatar_1.getDefaultAvatars)(true);
            chai_1.assert.deepEqual(defaultResult, directResult);
            chai_1.assert.notDeepEqual(defaultResult, groupResult);
        });
    });
});
