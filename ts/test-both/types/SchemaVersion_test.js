"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const SchemaVersion_1 = require("../../types/SchemaVersion");
describe('SchemaVersion', () => {
    describe('isValid', () => {
        it('should return true for positive integers', () => {
            chai_1.assert.isTrue((0, SchemaVersion_1.isValid)(0));
            chai_1.assert.isTrue((0, SchemaVersion_1.isValid)(1));
            chai_1.assert.isTrue((0, SchemaVersion_1.isValid)(2));
        });
        it('should return false for any other value', () => {
            chai_1.assert.isFalse((0, SchemaVersion_1.isValid)(null));
            chai_1.assert.isFalse((0, SchemaVersion_1.isValid)(-1));
            chai_1.assert.isFalse((0, SchemaVersion_1.isValid)(''));
        });
    });
});
