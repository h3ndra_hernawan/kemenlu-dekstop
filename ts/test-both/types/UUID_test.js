"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const UUID_1 = require("../../types/UUID");
describe('isValidUuid', () => {
    const LOWERCASE_V4_UUID = '9cb737ce-2bb3-4c21-9fe0-d286caa0ca68';
    it('returns false for non-strings', () => {
        chai_1.assert.isFalse((0, UUID_1.isValidUuid)(undefined));
        chai_1.assert.isFalse((0, UUID_1.isValidUuid)(null));
        chai_1.assert.isFalse((0, UUID_1.isValidUuid)(1234));
    });
    it('returns false for non-UUID strings', () => {
        chai_1.assert.isFalse((0, UUID_1.isValidUuid)(''));
        chai_1.assert.isFalse((0, UUID_1.isValidUuid)('hello world'));
        chai_1.assert.isFalse((0, UUID_1.isValidUuid)(` ${LOWERCASE_V4_UUID}`));
        chai_1.assert.isFalse((0, UUID_1.isValidUuid)(`${LOWERCASE_V4_UUID} `));
    });
    it("returns false for UUIDs that aren't version 4", () => {
        chai_1.assert.isFalse((0, UUID_1.isValidUuid)('a200a6e0-d2d9-11eb-bda7-dd5936a30ddf'));
        chai_1.assert.isFalse((0, UUID_1.isValidUuid)('2adb8b83-4f2c-55ca-a481-7f98b716e615'));
    });
    it('returns true for v4 UUIDs', () => {
        chai_1.assert.isTrue((0, UUID_1.isValidUuid)(LOWERCASE_V4_UUID));
        chai_1.assert.isTrue((0, UUID_1.isValidUuid)(LOWERCASE_V4_UUID.toUpperCase()));
    });
});
