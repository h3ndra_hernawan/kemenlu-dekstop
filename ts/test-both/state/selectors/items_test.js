"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const items_1 = require("../../../state/selectors/items");
describe('both/state/selectors/items', () => {
    // Note: we would like to use the full reducer here, to get a real empty state object
    //   but we cannot load the full reducer inside of electron-mocha.
    function getRootState(items) {
        return {
            items,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        };
    }
    describe('#getEmojiSkinTone', () => {
        it('returns 0 if passed anything invalid', () => {
            [
                // Invalid types
                undefined,
                null,
                '2',
                [2],
                // Numbers out of range
                -1,
                6,
                Infinity,
                // Invalid numbers
                0.1,
                1.2,
                NaN,
            ].forEach(skinTone => {
                const state = getRootState({ skinTone });
                chai_1.assert.strictEqual((0, items_1.getEmojiSkinTone)(state), 0);
            });
        });
        it('returns all valid skin tones', () => {
            [0, 1, 2, 3, 4, 5].forEach(skinTone => {
                const state = getRootState({ skinTone });
                chai_1.assert.strictEqual((0, items_1.getEmojiSkinTone)(state), skinTone);
            });
        });
    });
    describe('#getPreferredLeftPaneWidth', () => {
        it('returns a default if no value is present', () => {
            const state = getRootState({});
            chai_1.assert.strictEqual((0, items_1.getPreferredLeftPaneWidth)(state), 320);
        });
        it('returns a default value if passed something invalid', () => {
            [undefined, null, '250', [250], 250.123].forEach(preferredLeftPaneWidth => {
                const state = getRootState({
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    preferredLeftPaneWidth: preferredLeftPaneWidth,
                });
                chai_1.assert.strictEqual((0, items_1.getPreferredLeftPaneWidth)(state), 320);
            });
        });
        it('returns the value in storage if it is valid', () => {
            const state = getRootState({
                preferredLeftPaneWidth: 345,
            });
            chai_1.assert.strictEqual((0, items_1.getPreferredLeftPaneWidth)(state), 345);
        });
    });
    describe('#getPinnedConversationIds', () => {
        it('returns pinnedConversationIds key from items', () => {
            const expected = ['one', 'two'];
            const state = getRootState({
                pinnedConversationIds: expected,
            });
            const actual = (0, items_1.getPinnedConversationIds)(state);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('returns empty array if no saved data', () => {
            const expected = [];
            const state = getRootState({});
            const actual = (0, items_1.getPinnedConversationIds)(state);
            chai_1.assert.deepEqual(actual, expected);
        });
    });
    describe('#getPreferredReactionEmoji', () => {
        // See also: the tests for the `getPreferredReactionEmoji` helper.
        const expectedDefault = ['❤️', '👍🏿', '👎🏿', '😂', '😮', '😢'];
        it('returns the default set if no value is stored', () => {
            const state = getRootState({ skinTone: 5 });
            const actual = (0, items_1.getPreferredReactionEmoji)(state);
            chai_1.assert.deepStrictEqual(actual, expectedDefault);
        });
        it('returns the default set if the stored value is invalid', () => {
            const state = getRootState({
                skinTone: 5,
                preferredReactionEmoji: ['garbage!!'],
            });
            const actual = (0, items_1.getPreferredReactionEmoji)(state);
            chai_1.assert.deepStrictEqual(actual, expectedDefault);
        });
        it('returns a custom set of emoji', () => {
            const preferredReactionEmoji = ['✨', '❇️', '🤙🏻', '🦈', '💖', '🅿️'];
            const state = getRootState({ skinTone: 5, preferredReactionEmoji });
            const actual = (0, items_1.getPreferredReactionEmoji)(state);
            chai_1.assert.deepStrictEqual(actual, preferredReactionEmoji);
        });
    });
});
