"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const reducer_1 = require("../../../state/reducer");
const noop_1 = require("../../../state/ducks/noop");
const preferredReactions_1 = require("../../../state/selectors/preferredReactions");
describe('both/state/selectors/preferredReactions', () => {
    const getEmptyRootState = () => (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
    const getRootState = (preferredReactions) => (Object.assign(Object.assign({}, getEmptyRootState()), { preferredReactions }));
    describe('getIsCustomizingPreferredReactions', () => {
        it('returns false if the modal is closed', () => {
            chai_1.assert.isFalse((0, preferredReactions_1.getIsCustomizingPreferredReactions)(getEmptyRootState()));
        });
        it('returns true if the modal is open', () => {
            chai_1.assert.isTrue((0, preferredReactions_1.getIsCustomizingPreferredReactions)(getRootState({
                customizePreferredReactionsModal: {
                    draftPreferredReactions: ['✨', '❇️', '🎇', '🦈', '💖', '🅿️'],
                    originalPreferredReactions: ['💙', '👍', '👎', '😂', '😮', '😢'],
                    selectedDraftEmojiIndex: undefined,
                    isSaving: false,
                    hadSaveError: false,
                },
            })));
        });
    });
});
