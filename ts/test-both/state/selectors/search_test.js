"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon_1 = __importDefault(require("sinon"));
const conversations_1 = require("../../../state/ducks/conversations");
const noop_1 = require("../../../state/ducks/noop");
const search_1 = require("../../../state/ducks/search");
const user_1 = require("../../../state/ducks/user");
const search_2 = require("../../../state/selectors/search");
const makeLookup_1 = require("../../../util/makeLookup");
const UUID_1 = require("../../../types/UUID");
const getDefaultConversation_1 = require("../../helpers/getDefaultConversation");
const MessageReadStatus_1 = require("../../../messages/MessageReadStatus");
const reducer_1 = require("../../../state/reducer");
describe('both/state/selectors/search', () => {
    const NOW = 1000000;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let clock;
    beforeEach(() => {
        clock = sinon_1.default.useFakeTimers({
            now: NOW,
        });
    });
    afterEach(() => {
        clock.restore();
    });
    const getEmptyRootState = () => {
        return (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
    };
    function getDefaultMessage(id) {
        return {
            attachments: [],
            conversationId: 'conversationId',
            id,
            received_at: NOW,
            sent_at: NOW,
            source: 'source',
            sourceUuid: UUID_1.UUID.generate().toString(),
            timestamp: NOW,
            type: 'incoming',
            readStatus: MessageReadStatus_1.ReadStatus.Read,
        };
    }
    function getDefaultSearchMessage(id) {
        return Object.assign(Object.assign({}, getDefaultMessage(id)), { body: 'foo bar', bodyRanges: [], snippet: 'foo bar' });
    }
    describe('#getIsSearchingInAConversation', () => {
        it('returns false if not searching in a conversation', () => {
            const state = getEmptyRootState();
            chai_1.assert.isFalse((0, search_2.getIsSearchingInAConversation)(state));
        });
        it('returns true if searching in a conversation', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { search: Object.assign(Object.assign({}, (0, search_1.getEmptyState)()), { searchConversationId: 'abc123', searchConversationName: 'Test Conversation' }) });
            chai_1.assert.isTrue((0, search_2.getIsSearchingInAConversation)(state));
        });
    });
    describe('#getMessageSearchResultSelector', () => {
        it('returns undefined if message not found in lookup', () => {
            const state = getEmptyRootState();
            const selector = (0, search_2.getMessageSearchResultSelector)(state);
            const actual = selector('random-id');
            chai_1.assert.strictEqual(actual, undefined);
        });
        it('returns undefined if type is unexpected', () => {
            const id = 'message-id';
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { search: Object.assign(Object.assign({}, (0, search_1.getEmptyState)()), { messageLookup: {
                        [id]: Object.assign(Object.assign({}, getDefaultMessage(id)), { type: 'keychange', snippet: 'snippet', body: 'snippet', bodyRanges: [] }),
                    } }) });
            const selector = (0, search_2.getMessageSearchResultSelector)(state);
            const actual = selector(id);
            chai_1.assert.strictEqual(actual, undefined);
        });
        it('returns incoming message', () => {
            const searchId = 'search-id';
            const toId = 'to-id';
            const from = (0, getDefaultConversation_1.getDefaultConversationWithUuid)();
            const to = (0, getDefaultConversation_1.getDefaultConversation)({ id: toId });
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [from.id]: from,
                        [toId]: to,
                    }, conversationsByUuid: {
                        [from.uuid]: from,
                    } }), search: Object.assign(Object.assign({}, (0, search_1.getEmptyState)()), { messageLookup: {
                        [searchId]: Object.assign(Object.assign({}, getDefaultMessage(searchId)), { type: 'incoming', sourceUuid: from.uuid, conversationId: toId, snippet: 'snippet', body: 'snippet', bodyRanges: [] }),
                    } }) });
            const selector = (0, search_2.getMessageSearchResultSelector)(state);
            const actual = selector(searchId);
            const expected = {
                from,
                to,
                id: searchId,
                conversationId: toId,
                sentAt: NOW,
                snippet: 'snippet',
                body: 'snippet',
                bodyRanges: [],
                isSelected: false,
                isSearchingInConversation: false,
            };
            chai_1.assert.deepEqual(actual, expected);
        });
        it('returns the correct "from" and "to" when sent to me', () => {
            const searchId = 'search-id';
            const myId = 'my-id';
            const from = (0, getDefaultConversation_1.getDefaultConversationWithUuid)();
            const toId = from.uuid;
            const meAsRecipient = (0, getDefaultConversation_1.getDefaultConversation)({ id: myId });
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [from.id]: from,
                        [myId]: meAsRecipient,
                    }, conversationsByUuid: {
                        [from.uuid]: from,
                    } }), ourConversationId: myId, search: Object.assign(Object.assign({}, (0, search_1.getEmptyState)()), { messageLookup: {
                        [searchId]: Object.assign(Object.assign({}, getDefaultMessage(searchId)), { type: 'incoming', sourceUuid: from.uuid, conversationId: toId, snippet: 'snippet', body: 'snippet', bodyRanges: [] }),
                    } }), user: Object.assign(Object.assign({}, (0, user_1.getEmptyState)()), { ourConversationId: myId }) });
            const selector = (0, search_2.getMessageSearchResultSelector)(state);
            const actual = selector(searchId);
            chai_1.assert.deepEqual(actual === null || actual === void 0 ? void 0 : actual.from, from);
            chai_1.assert.deepEqual(actual === null || actual === void 0 ? void 0 : actual.to, meAsRecipient);
        });
        it('returns outgoing message and caches appropriately', () => {
            const searchId = 'search-id';
            const toId = 'to-id';
            const from = (0, getDefaultConversation_1.getDefaultConversationWithUuid)();
            const to = (0, getDefaultConversation_1.getDefaultConversation)({ id: toId });
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { user: Object.assign(Object.assign({}, (0, user_1.getEmptyState)()), { ourConversationId: from.id }), conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [from.id]: from,
                        [toId]: to,
                    }, conversationsByUuid: {
                        [from.uuid]: from,
                    } }), search: Object.assign(Object.assign({}, (0, search_1.getEmptyState)()), { messageLookup: {
                        [searchId]: Object.assign(Object.assign({}, getDefaultMessage(searchId)), { type: 'outgoing', conversationId: toId, snippet: 'snippet', body: 'snippet', bodyRanges: [] }),
                    } }) });
            const selector = (0, search_2.getMessageSearchResultSelector)(state);
            const actual = selector(searchId);
            const expected = {
                from,
                to,
                id: searchId,
                conversationId: toId,
                sentAt: NOW,
                snippet: 'snippet',
                body: 'snippet',
                bodyRanges: [],
                isSelected: false,
                isSearchingInConversation: false,
            };
            chai_1.assert.deepEqual(actual, expected);
            // Update the conversation lookup, but not the conversations in question
            const secondState = Object.assign(Object.assign({}, state), { conversations: Object.assign({}, state.conversations) });
            const secondSelector = (0, search_2.getMessageSearchResultSelector)(secondState);
            const secondActual = secondSelector(searchId);
            chai_1.assert.strictEqual(secondActual, actual);
            // Update a conversation involved in rendering this search result
            const thirdState = Object.assign(Object.assign({}, state), { conversations: Object.assign(Object.assign({}, state.conversations), { conversationsByUuid: Object.assign(Object.assign({}, state.conversations.conversationsByUuid), { [from.uuid]: Object.assign(Object.assign({}, from), { name: 'new-name' }) }) }) });
            const thirdSelector = (0, search_2.getMessageSearchResultSelector)(thirdState);
            const thirdActual = thirdSelector(searchId);
            chai_1.assert.notStrictEqual(actual, thirdActual);
        });
    });
    describe('#getSearchResults', () => {
        it("returns loading search results when they're loading", () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { search: Object.assign(Object.assign({}, (0, search_1.getEmptyState)()), { query: 'foo bar', discussionsLoading: true, messagesLoading: true }) });
            chai_1.assert.deepEqual((0, search_2.getSearchResults)(state), {
                conversationResults: { isLoading: true },
                contactResults: { isLoading: true },
                messageResults: { isLoading: true },
                searchConversationName: undefined,
                searchTerm: 'foo bar',
            });
        });
        it('returns loaded search results', () => {
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)({ id: '1' }),
                (0, getDefaultConversation_1.getDefaultConversation)({ id: '2' }),
            ];
            const contacts = [
                (0, getDefaultConversation_1.getDefaultConversation)({ id: '3' }),
                (0, getDefaultConversation_1.getDefaultConversation)({ id: '4' }),
                (0, getDefaultConversation_1.getDefaultConversation)({ id: '5' }),
            ];
            const messages = [
                getDefaultSearchMessage('a'),
                getDefaultSearchMessage('b'),
                getDefaultSearchMessage('c'),
            ];
            const getId = ({ id }) => id;
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: (0, makeLookup_1.makeLookup)([...conversations, ...contacts], 'id') }), search: Object.assign(Object.assign({}, (0, search_1.getEmptyState)()), { query: 'foo bar', conversationIds: conversations.map(getId), contactIds: contacts.map(getId), messageIds: messages.map(getId), messageLookup: (0, makeLookup_1.makeLookup)(messages, 'id'), discussionsLoading: false, messagesLoading: false }) });
            chai_1.assert.deepEqual((0, search_2.getSearchResults)(state), {
                conversationResults: {
                    isLoading: false,
                    results: conversations,
                },
                contactResults: {
                    isLoading: false,
                    results: contacts,
                },
                messageResults: {
                    isLoading: false,
                    results: messages,
                },
                searchConversationName: undefined,
                searchTerm: 'foo bar',
            });
        });
    });
});
