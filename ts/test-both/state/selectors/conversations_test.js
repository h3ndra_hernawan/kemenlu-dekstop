"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const conversationsEnums_1 = require("../../../state/ducks/conversationsEnums");
const conversations_1 = require("../../../state/ducks/conversations");
const conversations_2 = require("../../../state/selectors/conversations");
const noop_1 = require("../../../state/ducks/noop");
const reducer_1 = require("../../../state/reducer");
const setupI18n_1 = require("../../../util/setupI18n");
const UUID_1 = require("../../../types/UUID");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../../helpers/getDefaultConversation");
const defaultComposerStates_1 = require("../../helpers/defaultComposerStates");
describe('both/state/selectors/conversations', () => {
    const getEmptyRootState = () => {
        return (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
    };
    function makeConversation(id) {
        return (0, getDefaultConversation_1.getDefaultConversation)({
            id,
            searchableTitle: `${id} title`,
            title: `${id} title`,
        });
    }
    function makeConversationWithUuid(id) {
        return (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
            id,
            searchableTitle: `${id} title`,
            title: `${id} title`,
        }, UUID_1.UUID.fromPrefix(id).toString());
    }
    const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
    describe('#getConversationByIdSelector', () => {
        const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: { abc123: makeConversation('abc123') } }) });
        it('returns undefined if the conversation is not in the lookup', () => {
            const selector = (0, conversations_2.getConversationByIdSelector)(state);
            const actual = selector('xyz');
            chai_1.assert.isUndefined(actual);
        });
        it('returns the conversation in the lookup if it exists', () => {
            const selector = (0, conversations_2.getConversationByIdSelector)(state);
            const actual = selector('abc123');
            chai_1.assert.strictEqual(actual === null || actual === void 0 ? void 0 : actual.title, 'abc123 title');
        });
    });
    describe('#getConversationSelector', () => {
        it('returns empty placeholder if falsey id provided', () => {
            const state = getEmptyRootState();
            const selector = (0, conversations_2.getConversationSelector)(state);
            const actual = selector(undefined);
            chai_1.assert.deepEqual(actual, (0, conversations_2.getPlaceholderContact)());
        });
        it('returns empty placeholder if no match', () => {
            const state = Object.assign({}, getEmptyRootState());
            const selector = (0, conversations_2.getConversationSelector)(state);
            const actual = selector('random-id');
            chai_1.assert.deepEqual(actual, (0, conversations_2.getPlaceholderContact)());
        });
        it('returns conversation by uuid first', () => {
            const id = 'id';
            const conversation = makeConversation(id);
            const wrongConversation = makeConversation('wrong');
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [id]: wrongConversation,
                    }, conversationsByE164: {
                        [id]: wrongConversation,
                    }, conversationsByUuid: {
                        [id]: conversation,
                    }, conversationsByGroupId: {
                        [id]: wrongConversation,
                    } }) });
            const selector = (0, conversations_2.getConversationSelector)(state);
            const actual = selector(id);
            chai_1.assert.strictEqual(actual, conversation);
        });
        it('returns conversation by e164', () => {
            const id = 'id';
            const conversation = makeConversation(id);
            const wrongConversation = makeConversation('wrong');
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [id]: wrongConversation,
                    }, conversationsByE164: {
                        [id]: conversation,
                    }, conversationsByGroupId: {
                        [id]: wrongConversation,
                    } }) });
            const selector = (0, conversations_2.getConversationSelector)(state);
            const actual = selector(id);
            chai_1.assert.strictEqual(actual, conversation);
        });
        it('returns conversation by groupId', () => {
            const id = 'id';
            const conversation = makeConversation(id);
            const wrongConversation = makeConversation('wrong');
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [id]: wrongConversation,
                    }, conversationsByGroupId: {
                        [id]: conversation,
                    } }) });
            const selector = (0, conversations_2.getConversationSelector)(state);
            const actual = selector(id);
            chai_1.assert.strictEqual(actual, conversation);
        });
        it('returns conversation by conversationId', () => {
            const id = 'id';
            const conversation = makeConversation(id);
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [id]: conversation,
                    } }) });
            const selector = (0, conversations_2.getConversationSelector)(state);
            const actual = selector(id);
            chai_1.assert.strictEqual(actual, conversation);
        });
        // Less important now, given that all prop-generation for conversations is in
        //   models/conversation.getProps() and not here.
        it('does proper caching of result', () => {
            const id = 'id';
            const conversation = makeConversation(id);
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [id]: conversation,
                    } }) });
            const selector = (0, conversations_2.getConversationSelector)(state);
            const actual = selector(id);
            const secondState = Object.assign(Object.assign({}, state), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [id]: conversation,
                    } }) });
            const secondSelector = (0, conversations_2.getConversationSelector)(secondState);
            const secondActual = secondSelector(id);
            chai_1.assert.strictEqual(actual, secondActual);
            const thirdState = Object.assign(Object.assign({}, state), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        [id]: makeConversation('third'),
                    } }) });
            const thirdSelector = (0, conversations_2.getConversationSelector)(thirdState);
            const thirdActual = thirdSelector(id);
            chai_1.assert.notStrictEqual(actual, thirdActual);
        });
    });
    describe('#getConversationsStoppingMessageSendBecauseOfVerification', () => {
        it('returns an empty array if there are no conversations stopping send', () => {
            const state = getEmptyRootState();
            chai_1.assert.isEmpty((0, conversations_2.getConversationsStoppingMessageSendBecauseOfVerification)(state));
        });
        it('returns all conversations stopping message send', () => {
            const convo1 = makeConversation('abc');
            const convo2 = makeConversation('def');
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        def: convo2,
                        abc: convo1,
                    }, outboundMessagesPendingConversationVerification: {
                        def: ['message 2', 'message 3'],
                        abc: ['message 1', 'message 2'],
                    } }) });
            chai_1.assert.deepEqual((0, conversations_2.getConversationsStoppingMessageSendBecauseOfVerification)(state), [convo1, convo2]);
        });
    });
    describe('#getMessageIdsPendingBecauseOfVerification', () => {
        it('returns an empty set if there are no conversations stopping send', () => {
            const state = getEmptyRootState();
            chai_1.assert.deepEqual((0, conversations_2.getMessageIdsPendingBecauseOfVerification)(state), new Set());
        });
        it('returns a set of unique pending messages', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { outboundMessagesPendingConversationVerification: {
                        abc: ['message 2', 'message 3'],
                        def: ['message 1', 'message 2'],
                        ghi: ['message 4'],
                    } }) });
            chai_1.assert.deepEqual((0, conversations_2.getMessageIdsPendingBecauseOfVerification)(state), new Set(['message 1', 'message 2', 'message 3', 'message 4']));
        });
    });
    describe('#getNumberOfMessagesPendingBecauseOfVerification', () => {
        it('returns 0 if there are no conversations stopping send', () => {
            const state = getEmptyRootState();
            chai_1.assert.strictEqual((0, conversations_2.getNumberOfMessagesPendingBecauseOfVerification)(state), 0);
        });
        it('returns a count of unique pending messages', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { outboundMessagesPendingConversationVerification: {
                        abc: ['message 2', 'message 3'],
                        def: ['message 1', 'message 2'],
                        ghi: ['message 4'],
                    } }) });
            chai_1.assert.strictEqual((0, conversations_2.getNumberOfMessagesPendingBecauseOfVerification)(state), 4);
        });
    });
    describe('#getInvitedContactsForNewlyCreatedGroup', () => {
        it('returns an empty array if there are no invited contacts', () => {
            const state = getEmptyRootState();
            chai_1.assert.deepEqual((0, conversations_2.getInvitedContactsForNewlyCreatedGroup)(state), []);
        });
        it('returns "hydrated" invited contacts', () => {
            const abc = makeConversationWithUuid('abc');
            const def = makeConversationWithUuid('def');
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationsByUuid: {
                        [abc.uuid]: abc,
                        [def.uuid]: def,
                    }, invitedUuidsForNewlyCreatedGroup: [def.uuid, abc.uuid] }) });
            const result = (0, conversations_2.getInvitedContactsForNewlyCreatedGroup)(state);
            const titles = result.map(conversation => conversation.title);
            chai_1.assert.deepEqual(titles, ['def title', 'abc title']);
        });
    });
    describe('#getComposerStep', () => {
        it("returns undefined if the composer isn't open", () => {
            const state = getEmptyRootState();
            const result = (0, conversations_2.getComposerStep)(state);
            chai_1.assert.isUndefined(result);
        });
        it('returns the first step of the composer', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultStartDirectConversationComposerState }) });
            const result = (0, conversations_2.getComposerStep)(state);
            chai_1.assert.strictEqual(result, conversationsEnums_1.ComposerStep.StartDirectConversation);
        });
        it('returns the second step of the composer', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState }) });
            const result = (0, conversations_2.getComposerStep)(state);
            chai_1.assert.strictEqual(result, conversationsEnums_1.ComposerStep.ChooseGroupMembers);
        });
        it('returns the third step of the composer', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultSetGroupMetadataComposerState }) });
            const result = (0, conversations_2.getComposerStep)(state);
            chai_1.assert.strictEqual(result, conversationsEnums_1.ComposerStep.SetGroupMetadata);
        });
    });
    describe('#hasGroupCreationError', () => {
        it('returns false if not in the "set group metadata" composer step', () => {
            chai_1.assert.isFalse((0, conversations_2.hasGroupCreationError)(getEmptyRootState()));
            chai_1.assert.isFalse((0, conversations_2.hasGroupCreationError)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultStartDirectConversationComposerState }) })));
        });
        it('returns false if there is no group creation error', () => {
            chai_1.assert.isFalse((0, conversations_2.hasGroupCreationError)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultSetGroupMetadataComposerState }) })));
        });
        it('returns true if there is a group creation error', () => {
            chai_1.assert.isTrue((0, conversations_2.hasGroupCreationError)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { hasError: true }) }) })));
        });
    });
    describe('#isCreatingGroup', () => {
        it('returns false if not in the "set group metadata" composer step', () => {
            chai_1.assert.isFalse((0, conversations_2.hasGroupCreationError)(getEmptyRootState()));
            chai_1.assert.isFalse((0, conversations_2.isCreatingGroup)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultStartDirectConversationComposerState }) })));
        });
        it('returns false if the group is not being created', () => {
            chai_1.assert.isFalse((0, conversations_2.isCreatingGroup)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultSetGroupMetadataComposerState }) })));
        });
        it('returns true if the group is being created', () => {
            chai_1.assert.isTrue((0, conversations_2.isCreatingGroup)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { isCreating: true, hasError: false }) }) })));
        });
    });
    describe('#getAllComposableConversations', () => {
        const getRootState = () => {
            const rootState = getEmptyRootState();
            return Object.assign(Object.assign({}, rootState), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'our-conversation-id': Object.assign(Object.assign({}, makeConversation('our-conversation-id')), { isMe: true, profileName: 'My own name' }),
                    } }), user: Object.assign(Object.assign({}, rootState.user), { ourConversationId: 'our-conversation-id', i18n }) });
        };
        const getRootStateWithConversations = () => {
            const result = getRootState();
            Object.assign(result.conversations.conversationLookup, {
                'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { type: 'direct', profileName: 'A', title: 'A' }),
                'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { type: 'group', isGroupV1AndDisabled: true, name: '2', title: 'Should Be Dropped (GV1)' }),
                'convo-3': Object.assign(Object.assign({}, makeConversation('convo-3')), { type: 'group', name: 'B', title: 'B' }),
                'convo-4': Object.assign(Object.assign({}, makeConversation('convo-4')), { isBlocked: true, name: '4', title: 'Should Be Dropped (blocked)' }),
                'convo-5': Object.assign(Object.assign({}, makeConversation('convo-5')), { discoveredUnregisteredAt: new Date(1999, 3, 20).getTime(), name: 'C', title: 'C' }),
                'convo-6': Object.assign(Object.assign({}, makeConversation('convo-6')), { profileSharing: true, name: 'Should Be Droped (no title)', title: null }),
                'convo-7': Object.assign(Object.assign({}, makeConversation('convo-7')), { discoveredUnregisteredAt: Date.now(), name: '7', title: 'Should Be Dropped (unregistered)' }),
            });
            return result;
        };
        it('returns no gv1, no blocked, no missing titles', () => {
            const state = getRootStateWithConversations();
            const result = (0, conversations_2.getAllComposableConversations)(state);
            const ids = result.map(contact => contact.id);
            chai_1.assert.deepEqual(ids, [
                'our-conversation-id',
                'convo-1',
                'convo-3',
                'convo-5',
            ]);
        });
    });
    describe('#getComposableContacts', () => {
        const getRootState = () => {
            const rootState = getEmptyRootState();
            return Object.assign(Object.assign({}, rootState), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'our-conversation-id': Object.assign(Object.assign({}, makeConversation('our-conversation-id')), { isMe: true }),
                    } }), user: Object.assign(Object.assign({}, rootState.user), { ourConversationId: 'our-conversation-id', i18n }) });
        };
        it('returns only direct contacts, including me', () => {
            const state = Object.assign(Object.assign({}, getRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'convo-0': Object.assign(Object.assign({}, makeConversation('convo-0')), { isMe: true, profileSharing: false }),
                        'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { type: 'group', name: 'Friends!', sharedGroupNames: [] }),
                        'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { name: 'Alice' }),
                    } }) });
            const result = (0, conversations_2.getComposableContacts)(state);
            const ids = result.map(group => group.id);
            chai_1.assert.deepEqual(ids, ['convo-0', 'convo-2']);
        });
        it('excludes blocked, unregistered, and missing name/profileSharing', () => {
            const state = Object.assign(Object.assign({}, getRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'convo-0': Object.assign(Object.assign({}, makeConversation('convo-0')), { name: 'Ex', isBlocked: true }),
                        'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { name: 'Bob', discoveredUnregisteredAt: Date.now() }),
                        'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { name: 'Charlie' }),
                    } }) });
            const result = (0, conversations_2.getComposableContacts)(state);
            const ids = result.map(group => group.id);
            chai_1.assert.deepEqual(ids, ['convo-2']);
        });
    });
    describe('#getCandidateContactsForNewGroup', () => {
        const getRootState = () => {
            const rootState = getEmptyRootState();
            return Object.assign(Object.assign({}, rootState), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'our-conversation-id': Object.assign(Object.assign({}, makeConversation('our-conversation-id')), { isMe: true }),
                    } }), user: Object.assign(Object.assign({}, rootState.user), { ourConversationId: 'our-conversation-id', i18n }) });
        };
        it('returns only direct contacts, without me', () => {
            const state = Object.assign(Object.assign({}, getRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'convo-0': Object.assign(Object.assign({}, makeConversation('convo-0')), { isMe: true, name: 'Me!' }),
                        'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { type: 'group', name: 'Friends!', sharedGroupNames: [] }),
                        'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { name: 'Alice' }),
                    } }) });
            const result = (0, conversations_2.getCandidateContactsForNewGroup)(state);
            const ids = result.map(group => group.id);
            chai_1.assert.deepEqual(ids, ['convo-2']);
        });
        it('excludes blocked, unregistered, and missing name/profileSharing', () => {
            const state = Object.assign(Object.assign({}, getRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'convo-0': Object.assign(Object.assign({}, makeConversation('convo-0')), { name: 'Ex', isBlocked: true }),
                        'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { name: 'Bob', discoveredUnregisteredAt: Date.now() }),
                        'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { name: 'Charlie' }),
                    } }) });
            const result = (0, conversations_2.getCandidateContactsForNewGroup)(state);
            const ids = result.map(group => group.id);
            chai_1.assert.deepEqual(ids, ['convo-2']);
        });
    });
    describe('#getComposableGroups', () => {
        const getRootState = () => {
            const rootState = getEmptyRootState();
            return Object.assign(Object.assign({}, rootState), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'our-conversation-id': Object.assign(Object.assign({}, makeConversation('our-conversation-id')), { isMe: true }),
                    } }), user: Object.assign(Object.assign({}, rootState.user), { ourConversationId: 'our-conversation-id', i18n }) });
        };
        it('returns only groups with name', () => {
            const state = Object.assign(Object.assign({}, getRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'convo-0': Object.assign(Object.assign({}, makeConversation('convo-0')), { isMe: true, name: 'Me!' }),
                        'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { type: 'group', name: 'Friends!', sharedGroupNames: [] }),
                        'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { type: 'group', sharedGroupNames: [] }),
                    } }) });
            const result = (0, conversations_2.getComposableGroups)(state);
            const ids = result.map(group => group.id);
            chai_1.assert.deepEqual(ids, ['convo-1']);
        });
        it('excludes blocked, and missing name/profileSharing', () => {
            const state = Object.assign(Object.assign({}, getRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'convo-0': Object.assign(Object.assign({}, makeConversation('convo-0')), { type: 'group', name: 'Family!', isBlocked: true, sharedGroupNames: [] }),
                        'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { type: 'group', name: 'Friends!', sharedGroupNames: [] }),
                        'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { type: 'group', sharedGroupNames: [] }),
                    } }) });
            const result = (0, conversations_2.getComposableGroups)(state);
            const ids = result.map(group => group.id);
            chai_1.assert.deepEqual(ids, ['convo-1']);
        });
    });
    describe('#getFilteredComposeContacts', () => {
        const getRootState = (searchTerm = '') => {
            const rootState = getEmptyRootState();
            return Object.assign(Object.assign({}, rootState), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'our-conversation-id': Object.assign(Object.assign({}, makeConversation('our-conversation-id')), { name: 'Me, Myself, and I', title: 'Me, Myself, and I', searchableTitle: 'Note to Self', isMe: true }),
                    }, composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultStartDirectConversationComposerState), { searchTerm }) }), user: Object.assign(Object.assign({}, rootState.user), { ourConversationId: 'our-conversation-id', i18n }) });
        };
        const getRootStateWithConversations = (searchTerm = '') => {
            const result = getRootState(searchTerm);
            Object.assign(result.conversations.conversationLookup, {
                'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { name: 'In System Contacts', title: 'A. Sorted First' }),
                'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { title: 'Should Be Dropped (no name, no profile sharing)' }),
                'convo-3': Object.assign(Object.assign({}, makeConversation('convo-3')), { type: 'group', title: 'Should Be Dropped (group)' }),
                'convo-4': Object.assign(Object.assign({}, makeConversation('convo-4')), { isBlocked: true, title: 'Should Be Dropped (blocked)' }),
                'convo-5': Object.assign(Object.assign({}, makeConversation('convo-5')), { discoveredUnregisteredAt: new Date(1999, 3, 20).getTime(), name: 'In System Contacts (and unregistered too long ago)', title: 'B. Sorted Second' }),
                'convo-6': Object.assign(Object.assign({}, makeConversation('convo-6')), { profileSharing: true, profileName: 'C. Has Profile Sharing', title: 'C. Has Profile Sharing' }),
                'convo-7': Object.assign(Object.assign({}, makeConversation('convo-7')), { discoveredUnregisteredAt: Date.now(), title: 'Should Be Dropped (unregistered)' }),
            });
            return result;
        };
        it('returns no results when there are no contacts', () => {
            const state = getRootState('foo bar baz');
            const result = (0, conversations_2.getFilteredComposeContacts)(state);
            chai_1.assert.isEmpty(result);
        });
        it('includes Note to Self with no search term', () => {
            const state = getRootStateWithConversations();
            const result = (0, conversations_2.getFilteredComposeContacts)(state);
            const ids = result.map(contact => contact.id);
            chai_1.assert.deepEqual(ids, [
                'convo-1',
                'convo-5',
                'convo-6',
                'our-conversation-id',
            ]);
        });
        it('can search for contacts', () => {
            const state = getRootStateWithConversations('in system');
            const result = (0, conversations_2.getFilteredComposeContacts)(state);
            const ids = result.map(contact => contact.id);
            // NOTE: convo-6 matches because you can't write "Sharing" without "in"
            chai_1.assert.deepEqual(ids, ['convo-1', 'convo-5', 'convo-6']);
        });
        it('can search for note to self', () => {
            const state = getRootStateWithConversations('note');
            const result = (0, conversations_2.getFilteredComposeContacts)(state);
            const ids = result.map(contact => contact.id);
            chai_1.assert.deepEqual(ids, ['our-conversation-id']);
        });
        it('returns note to self when searching for your own name', () => {
            const state = getRootStateWithConversations('Myself');
            const result = (0, conversations_2.getFilteredComposeContacts)(state);
            const ids = result.map(contact => contact.id);
            chai_1.assert.deepEqual(ids, ['our-conversation-id']);
        });
    });
    describe('#getFilteredComposeGroups', () => {
        const getState = (searchTerm = '') => {
            const rootState = getEmptyRootState();
            return Object.assign(Object.assign({}, rootState), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'our-conversation-id': Object.assign(Object.assign({}, makeConversation('our-conversation-id')), { isMe: true }),
                        'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { name: 'In System Contacts', title: 'Should be dropped (contact)' }),
                        'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { title: 'Should be dropped (contact)' }),
                        'convo-3': Object.assign(Object.assign({}, makeConversation('convo-3')), { type: 'group', name: 'Hello World', title: 'Hello World', sharedGroupNames: [] }),
                        'convo-4': Object.assign(Object.assign({}, makeConversation('convo-4')), { type: 'group', isBlocked: true, title: 'Should be dropped (blocked)', sharedGroupNames: [] }),
                        'convo-5': Object.assign(Object.assign({}, makeConversation('convo-5')), { type: 'group', title: 'Unknown Group', sharedGroupNames: [] }),
                        'convo-6': Object.assign(Object.assign({}, makeConversation('convo-6')), { type: 'group', name: 'Signal', title: 'Signal', sharedGroupNames: [] }),
                        'convo-7': Object.assign(Object.assign({}, makeConversation('convo-7')), { profileSharing: false, type: 'group', name: 'Signal Fake', title: 'Signal Fake', sharedGroupNames: [] }),
                    }, composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultStartDirectConversationComposerState), { searchTerm }) }), user: Object.assign(Object.assign({}, rootState.user), { ourConversationId: 'our-conversation-id', i18n }) });
        };
        it('can search for groups', () => {
            const state = getState('hello');
            const result = (0, conversations_2.getFilteredComposeGroups)(state);
            const ids = result.map(group => group.id);
            chai_1.assert.deepEqual(ids, ['convo-3']);
        });
        it('does not return unknown groups when getting all groups (no search term)', () => {
            const state = getState();
            const result = (0, conversations_2.getFilteredComposeGroups)(state);
            const ids = result.map(group => group.id);
            chai_1.assert.deepEqual(ids, ['convo-3', 'convo-6', 'convo-7']);
        });
    });
    describe('#getFilteredCandidateContactsForNewGroup', () => {
        const getRootState = (searchTerm = '') => {
            const rootState = getEmptyRootState();
            return Object.assign(Object.assign({}, rootState), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'our-conversation-id': Object.assign(Object.assign({}, makeConversation('our-conversation-id')), { isMe: true }),
                        'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { name: 'In System Contacts', title: 'A. Sorted First' }),
                        'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { title: 'Should be dropped (has no name)' }),
                        'convo-3': Object.assign(Object.assign({}, makeConversation('convo-3')), { type: 'group', title: 'Should Be Dropped (group)', sharedGroupNames: [] }),
                        'convo-4': Object.assign(Object.assign({}, makeConversation('convo-4')), { isBlocked: true, name: 'My Name', title: 'Should Be Dropped (blocked)' }),
                        'convo-5': Object.assign(Object.assign({}, makeConversation('convo-5')), { discoveredUnregisteredAt: new Date(1999, 3, 20).getTime(), name: 'In System Contacts (and unregistered too long ago)', title: 'C. Sorted Third' }),
                        'convo-6': Object.assign(Object.assign({}, makeConversation('convo-6')), { discoveredUnregisteredAt: Date.now(), name: 'My Name', title: 'Should Be Dropped (unregistered)' }),
                    }, composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { searchTerm }) }), user: Object.assign(Object.assign({}, rootState.user), { ourConversationId: 'our-conversation-id', i18n }) });
        };
        it('returns sorted contacts when there is no search term', () => {
            const state = getRootState();
            const result = (0, conversations_2.getFilteredCandidateContactsForNewGroup)(state);
            const ids = result.map(contact => contact.id);
            chai_1.assert.deepEqual(ids, ['convo-1', 'convo-5']);
        });
        it('can search for contacts', () => {
            const state = getRootState('system contacts');
            const result = (0, conversations_2.getFilteredCandidateContactsForNewGroup)(state);
            const ids = result.map(contact => contact.id);
            chai_1.assert.deepEqual(ids, ['convo-1', 'convo-5']);
        });
    });
    describe('#getCantAddContactForModal', () => {
        it('returns undefined if not in the "choose group members" composer step', () => {
            chai_1.assert.isUndefined((0, conversations_2.getCantAddContactForModal)(getEmptyRootState()));
            chai_1.assert.isUndefined((0, conversations_2.getCantAddContactForModal)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultStartDirectConversationComposerState }) })));
        });
        it("returns undefined if there's no contact marked", () => {
            chai_1.assert.isUndefined((0, conversations_2.getCantAddContactForModal)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState }) })));
        });
        it('returns the marked contact', () => {
            const conversation = makeConversation('abc123');
            chai_1.assert.deepEqual((0, conversations_2.getCantAddContactForModal)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: { abc123: conversation }, composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { cantAddContactIdForModal: 'abc123' }) }) })), conversation);
        });
    });
    describe('#getComposerConversationSearchTerm', () => {
        it("returns the composer's contact search term", () => {
            chai_1.assert.strictEqual((0, conversations_2.getComposerConversationSearchTerm)(Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultStartDirectConversationComposerState), { searchTerm: 'foo bar' }) }) })), 'foo bar');
        });
    });
    describe('#_getLeftPaneLists', () => {
        it('sorts conversations based on timestamp then by intl-friendly title', () => {
            const data = {
                id1: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: 'id1',
                    e164: '+18005551111',
                    activeAt: Date.now(),
                    name: 'No timestamp',
                    timestamp: 0,
                    inboxPosition: 0,
                    phoneNumber: 'notused',
                    isArchived: false,
                    markedUnread: false,
                    type: 'direct',
                    isMe: false,
                    lastUpdated: Date.now(),
                    title: 'No timestamp',
                    unreadCount: 1,
                    isSelected: false,
                    typingContactId: UUID_1.UUID.generate().toString(),
                    acceptedMessageRequest: true,
                }),
                id2: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: 'id2',
                    e164: '+18005551111',
                    activeAt: Date.now(),
                    name: 'B',
                    timestamp: 20,
                    inboxPosition: 21,
                    phoneNumber: 'notused',
                    isArchived: false,
                    markedUnread: false,
                    type: 'direct',
                    isMe: false,
                    lastUpdated: Date.now(),
                    title: 'B',
                    unreadCount: 1,
                    isSelected: false,
                    typingContactId: UUID_1.UUID.generate().toString(),
                    acceptedMessageRequest: true,
                }),
                id3: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: 'id3',
                    e164: '+18005551111',
                    activeAt: Date.now(),
                    name: 'C',
                    timestamp: 20,
                    inboxPosition: 22,
                    phoneNumber: 'notused',
                    isArchived: false,
                    markedUnread: false,
                    type: 'direct',
                    isMe: false,
                    lastUpdated: Date.now(),
                    title: 'C',
                    unreadCount: 1,
                    isSelected: false,
                    typingContactId: UUID_1.UUID.generate().toString(),
                    acceptedMessageRequest: true,
                }),
                id4: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: 'id4',
                    e164: '+18005551111',
                    activeAt: Date.now(),
                    name: 'Á',
                    timestamp: 20,
                    inboxPosition: 20,
                    phoneNumber: 'notused',
                    isArchived: false,
                    markedUnread: false,
                    type: 'direct',
                    isMe: false,
                    lastUpdated: Date.now(),
                    title: 'A',
                    unreadCount: 1,
                    isSelected: false,
                    typingContactId: UUID_1.UUID.generate().toString(),
                    acceptedMessageRequest: true,
                }),
                id5: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: 'id5',
                    e164: '+18005551111',
                    activeAt: Date.now(),
                    name: 'First!',
                    timestamp: 30,
                    inboxPosition: 30,
                    phoneNumber: 'notused',
                    isArchived: false,
                    markedUnread: false,
                    type: 'direct',
                    isMe: false,
                    lastUpdated: Date.now(),
                    title: 'First!',
                    unreadCount: 1,
                    isSelected: false,
                    typingContactId: UUID_1.UUID.generate().toString(),
                    acceptedMessageRequest: true,
                }),
            };
            const comparator = (0, conversations_2._getConversationComparator)();
            const { archivedConversations, conversations, pinnedConversations } = (0, conversations_2._getLeftPaneLists)(data, comparator);
            chai_1.assert.strictEqual(conversations[0].name, 'First!');
            chai_1.assert.strictEqual(conversations[1].name, 'Á');
            chai_1.assert.strictEqual(conversations[2].name, 'B');
            chai_1.assert.strictEqual(conversations[3].name, 'C');
            chai_1.assert.strictEqual(conversations[4].name, 'No timestamp');
            chai_1.assert.strictEqual(conversations.length, 5);
            chai_1.assert.strictEqual(archivedConversations.length, 0);
            chai_1.assert.strictEqual(pinnedConversations.length, 0);
        });
        describe('given pinned conversations', () => {
            it('sorts pinned conversations based on order in storage', () => {
                const data = {
                    pin2: (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'pin2',
                        e164: '+18005551111',
                        activeAt: Date.now(),
                        name: 'Pin Two',
                        timestamp: 30,
                        inboxPosition: 30,
                        phoneNumber: 'notused',
                        isArchived: false,
                        isPinned: true,
                        markedUnread: false,
                        type: 'direct',
                        isMe: false,
                        lastUpdated: Date.now(),
                        title: 'Pin Two',
                        unreadCount: 1,
                        isSelected: false,
                        typingContactId: UUID_1.UUID.generate().toString(),
                        acceptedMessageRequest: true,
                    }),
                    pin3: (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'pin3',
                        e164: '+18005551111',
                        activeAt: Date.now(),
                        name: 'Pin Three',
                        timestamp: 30,
                        inboxPosition: 30,
                        phoneNumber: 'notused',
                        isArchived: false,
                        isPinned: true,
                        markedUnread: false,
                        type: 'direct',
                        isMe: false,
                        lastUpdated: Date.now(),
                        title: 'Pin Three',
                        unreadCount: 1,
                        isSelected: false,
                        typingContactId: UUID_1.UUID.generate().toString(),
                        acceptedMessageRequest: true,
                    }),
                    pin1: (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'pin1',
                        e164: '+18005551111',
                        activeAt: Date.now(),
                        name: 'Pin One',
                        timestamp: 30,
                        inboxPosition: 30,
                        phoneNumber: 'notused',
                        isArchived: false,
                        isPinned: true,
                        markedUnread: false,
                        type: 'direct',
                        isMe: false,
                        lastUpdated: Date.now(),
                        title: 'Pin One',
                        unreadCount: 1,
                        isSelected: false,
                        typingContactId: UUID_1.UUID.generate().toString(),
                        acceptedMessageRequest: true,
                    }),
                };
                const pinnedConversationIds = ['pin1', 'pin2', 'pin3'];
                const comparator = (0, conversations_2._getConversationComparator)();
                const { archivedConversations, conversations, pinnedConversations } = (0, conversations_2._getLeftPaneLists)(data, comparator, undefined, pinnedConversationIds);
                chai_1.assert.strictEqual(pinnedConversations[0].name, 'Pin One');
                chai_1.assert.strictEqual(pinnedConversations[1].name, 'Pin Two');
                chai_1.assert.strictEqual(pinnedConversations[2].name, 'Pin Three');
                chai_1.assert.strictEqual(archivedConversations.length, 0);
                chai_1.assert.strictEqual(conversations.length, 0);
            });
            it('includes archived and pinned conversations with no active_at', () => {
                const data = {
                    pin2: (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'pin2',
                        e164: '+18005551111',
                        name: 'Pin Two',
                        timestamp: 30,
                        inboxPosition: 30,
                        phoneNumber: 'notused',
                        isArchived: false,
                        isPinned: true,
                        markedUnread: false,
                        type: 'direct',
                        isMe: false,
                        lastUpdated: Date.now(),
                        title: 'Pin Two',
                        unreadCount: 1,
                        isSelected: false,
                        typingContactId: UUID_1.UUID.generate().toString(),
                        acceptedMessageRequest: true,
                    }),
                    pin3: (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'pin3',
                        e164: '+18005551111',
                        name: 'Pin Three',
                        timestamp: 30,
                        inboxPosition: 30,
                        phoneNumber: 'notused',
                        isArchived: false,
                        isPinned: true,
                        markedUnread: false,
                        type: 'direct',
                        isMe: false,
                        lastUpdated: Date.now(),
                        title: 'Pin Three',
                        unreadCount: 1,
                        isSelected: false,
                        typingContactId: UUID_1.UUID.generate().toString(),
                        acceptedMessageRequest: true,
                    }),
                    pin1: (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'pin1',
                        e164: '+18005551111',
                        name: 'Pin One',
                        timestamp: 30,
                        inboxPosition: 30,
                        phoneNumber: 'notused',
                        isArchived: true,
                        isPinned: true,
                        markedUnread: false,
                        type: 'direct',
                        isMe: false,
                        lastUpdated: Date.now(),
                        title: 'Pin One',
                        unreadCount: 1,
                        isSelected: false,
                        typingContactId: UUID_1.UUID.generate().toString(),
                        acceptedMessageRequest: true,
                    }),
                    pin4: (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'pin1',
                        e164: '+18005551111',
                        name: 'Pin Four',
                        timestamp: 30,
                        inboxPosition: 30,
                        phoneNumber: 'notused',
                        activeAt: Date.now(),
                        isArchived: true,
                        isPinned: false,
                        markedUnread: false,
                        type: 'direct',
                        isMe: false,
                        lastUpdated: Date.now(),
                        title: 'Pin One',
                        unreadCount: 1,
                        isSelected: false,
                        typingContactId: UUID_1.UUID.generate().toString(),
                        acceptedMessageRequest: true,
                    }),
                    pin5: (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'pin1',
                        e164: '+18005551111',
                        name: 'Pin Five',
                        timestamp: 30,
                        inboxPosition: 30,
                        phoneNumber: 'notused',
                        isArchived: false,
                        isPinned: false,
                        markedUnread: false,
                        type: 'direct',
                        isMe: false,
                        lastUpdated: Date.now(),
                        title: 'Pin One',
                        unreadCount: 1,
                        isSelected: false,
                        typingContactId: UUID_1.UUID.generate().toString(),
                        acceptedMessageRequest: true,
                    }),
                };
                const pinnedConversationIds = ['pin1', 'pin2', 'pin3'];
                const comparator = (0, conversations_2._getConversationComparator)();
                const { archivedConversations, conversations, pinnedConversations } = (0, conversations_2._getLeftPaneLists)(data, comparator, undefined, pinnedConversationIds);
                chai_1.assert.strictEqual(pinnedConversations[0].name, 'Pin One');
                chai_1.assert.strictEqual(pinnedConversations[1].name, 'Pin Two');
                chai_1.assert.strictEqual(pinnedConversations[2].name, 'Pin Three');
                chai_1.assert.strictEqual(pinnedConversations.length, 3);
                chai_1.assert.strictEqual(archivedConversations[0].name, 'Pin Four');
                chai_1.assert.strictEqual(archivedConversations.length, 1);
                chai_1.assert.strictEqual(conversations.length, 0);
            });
        });
    });
    describe('#getMaximumGroupSizeModalState', () => {
        it('returns the modal state', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { maximumGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Showing }) }) });
            chai_1.assert.strictEqual((0, conversations_2.getMaximumGroupSizeModalState)(state), conversationsEnums_1.OneTimeModalState.Showing);
        });
    });
    describe('#getRecommendedGroupSizeModalState', () => {
        it('returns the modal state', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Showing }) }) });
            chai_1.assert.strictEqual((0, conversations_2.getRecommendedGroupSizeModalState)(state), conversationsEnums_1.OneTimeModalState.Showing);
        });
    });
    describe('#getComposeGroupAvatar', () => {
        it('returns undefined if there is no group avatar', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { groupAvatar: undefined }) }) });
            chai_1.assert.isUndefined((0, conversations_2.getComposeGroupAvatar)(state));
        });
        it('returns the group avatar', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { groupAvatar: new Uint8Array([1, 2, 3]) }) }) });
            chai_1.assert.deepEqual((0, conversations_2.getComposeGroupAvatar)(state), new Uint8Array([1, 2, 3]));
        });
    });
    describe('#getComposeGroupName', () => {
        it('returns the group name', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { groupName: 'foo bar' }) }) });
            chai_1.assert.deepEqual((0, conversations_2.getComposeGroupName)(state), 'foo bar');
        });
    });
    describe('#getComposeSelectedContacts', () => {
        it("returns the composer's selected contacts", () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        'convo-1': Object.assign(Object.assign({}, makeConversation('convo-1')), { title: 'Person One' }),
                        'convo-2': Object.assign(Object.assign({}, makeConversation('convo-2')), { title: 'Person Two' }),
                    }, composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { selectedConversationIds: ['convo-2', 'convo-1'] }) }) });
            const titles = (0, conversations_2.getComposeSelectedContacts)(state).map(contact => contact.title);
            chai_1.assert.deepEqual(titles, ['Person Two', 'Person One']);
        });
    });
    describe('#getConversationsByTitleSelector', () => {
        it('returns a selector that finds conversations by title', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        abc: Object.assign(Object.assign({}, makeConversation('abc')), { title: 'Janet' }),
                        def: Object.assign(Object.assign({}, makeConversation('def')), { title: 'Janet' }),
                        geh: Object.assign(Object.assign({}, makeConversation('geh')), { title: 'Rick' }),
                    } }) });
            const selector = (0, conversations_2.getConversationsByTitleSelector)(state);
            chai_1.assert.sameMembers(selector('Janet').map(c => c.id), ['abc', 'def']);
            chai_1.assert.sameMembers(selector('Rick').map(c => c.id), ['geh']);
            chai_1.assert.isEmpty(selector('abc'));
            chai_1.assert.isEmpty(selector('xyz'));
        });
    });
    describe('#getSelectedConversationId', () => {
        it('returns undefined if no conversation is selected', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        abc123: makeConversation('abc123'),
                    } }) });
            chai_1.assert.isUndefined((0, conversations_2.getSelectedConversationId)(state));
        });
        it('returns the selected conversation ID', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        abc123: makeConversation('abc123'),
                    }, selectedConversationId: 'abc123' }) });
            chai_1.assert.strictEqual((0, conversations_2.getSelectedConversationId)(state), 'abc123');
        });
    });
    describe('#getSelectedConversation', () => {
        it('returns undefined if no conversation is selected', () => {
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        abc123: makeConversation('abc123'),
                    } }) });
            chai_1.assert.isUndefined((0, conversations_2.getSelectedConversation)(state));
        });
        it('returns the selected conversation', () => {
            const conversation = makeConversation('abc123');
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        abc123: conversation,
                    }, selectedConversationId: 'abc123' }) });
            chai_1.assert.strictEqual((0, conversations_2.getSelectedConversation)(state), conversation);
        });
    });
    describe('#getContactNameColorSelector', () => {
        it('returns the right color order sorted by UUID ASC', () => {
            const group = makeConversation('group');
            group.type = 'group';
            group.sortedGroupMembers = [
                makeConversationWithUuid('fff'),
                makeConversationWithUuid('f00'),
                makeConversationWithUuid('e00'),
                makeConversationWithUuid('d00'),
                makeConversationWithUuid('c00'),
                makeConversationWithUuid('b00'),
                makeConversationWithUuid('a00'),
            ];
            const state = Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        group,
                    } }) });
            const contactNameColorSelector = (0, conversations_2.getContactNameColorSelector)(state);
            chai_1.assert.equal(contactNameColorSelector('group', 'a00'), '200');
            chai_1.assert.equal(contactNameColorSelector('group', 'b00'), '120');
            chai_1.assert.equal(contactNameColorSelector('group', 'c00'), '300');
            chai_1.assert.equal(contactNameColorSelector('group', 'd00'), '010');
            chai_1.assert.equal(contactNameColorSelector('group', 'e00'), '210');
            chai_1.assert.equal(contactNameColorSelector('group', 'f00'), '330');
            chai_1.assert.equal(contactNameColorSelector('group', 'fff'), '230');
        });
        it('returns the right colors for direct conversation', () => {
            const direct = makeConversation('theirId');
            const emptyState = getEmptyRootState();
            const state = Object.assign(Object.assign({}, emptyState), { user: Object.assign(Object.assign({}, emptyState.user), { ourConversationId: 'us' }), conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                        direct,
                    } }) });
            const contactNameColorSelector = (0, conversations_2.getContactNameColorSelector)(state);
            chai_1.assert.equal(contactNameColorSelector('direct', 'theirId'), '200');
            chai_1.assert.equal(contactNameColorSelector('direct', 'us'), '200');
        });
    });
});
