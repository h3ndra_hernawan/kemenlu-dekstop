"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const reducer_1 = require("../../../state/reducer");
const noop_1 = require("../../../state/ducks/noop");
const preferredReactions_1 = require("../../../state/ducks/preferredReactions");
describe('preferred reactions duck', () => {
    const getEmptyRootState = () => (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
    const getRootState = (preferredReactions) => (Object.assign(Object.assign({}, getEmptyRootState()), { preferredReactions }));
    const stateWithOpenCustomizationModal = Object.assign(Object.assign({}, (0, preferredReactions_1.getInitialState)()), { customizePreferredReactionsModal: {
            draftPreferredReactions: ['✨', '❇️', '🎇', '🦈', '💖', '🅿️'],
            originalPreferredReactions: ['💙', '👍', '👎', '😂', '😮', '😢'],
            selectedDraftEmojiIndex: undefined,
            isSaving: false,
            hadSaveError: false,
        } });
    const stateWithOpenCustomizationModalAndSelectedEmoji = Object.assign(Object.assign({}, stateWithOpenCustomizationModal), { customizePreferredReactionsModal: Object.assign(Object.assign({}, stateWithOpenCustomizationModal.customizePreferredReactionsModal), { selectedDraftEmojiIndex: 1 }) });
    let sinonSandbox;
    beforeEach(() => {
        sinonSandbox = sinon.createSandbox();
    });
    afterEach(() => {
        sinonSandbox.restore();
    });
    describe('cancelCustomizePreferredReactionsModal', () => {
        const { cancelCustomizePreferredReactionsModal } = preferredReactions_1.actions;
        it("does nothing if the modal isn't open", () => {
            const action = cancelCustomizePreferredReactionsModal();
            const result = (0, preferredReactions_1.reducer)((0, preferredReactions_1.getInitialState)(), action);
            chai_1.assert.notProperty(result, 'customizePreferredReactionsModal');
        });
        it('closes the modal if open', () => {
            const action = cancelCustomizePreferredReactionsModal();
            const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModal, action);
            chai_1.assert.notProperty(result, 'customizePreferredReactionsModal');
        });
    });
    describe('deselectDraftEmoji', () => {
        const { deselectDraftEmoji } = preferredReactions_1.actions;
        it('is a no-op if the customization modal is not open', () => {
            const state = (0, preferredReactions_1.getInitialState)();
            const action = deselectDraftEmoji();
            const result = (0, preferredReactions_1.reducer)(state, action);
            chai_1.assert.strictEqual(result, state);
        });
        it('is a no-op if no emoji is selected', () => {
            var _a;
            const action = deselectDraftEmoji();
            const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModal, action);
            chai_1.assert.isUndefined((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.selectedDraftEmojiIndex);
        });
        it('deselects a currently-selected emoji', () => {
            var _a;
            const action = deselectDraftEmoji();
            const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModalAndSelectedEmoji, action);
            chai_1.assert.isUndefined((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.selectedDraftEmojiIndex);
        });
    });
    describe('openCustomizePreferredReactionsModal', () => {
        const { openCustomizePreferredReactionsModal } = preferredReactions_1.actions;
        it('opens the customization modal with defaults if no value was stored', () => {
            const emptyRootState = getEmptyRootState();
            const rootState = Object.assign(Object.assign({}, emptyRootState), { items: Object.assign(Object.assign({}, emptyRootState.items), { skinTone: 5 }) });
            const dispatch = sinon.spy();
            openCustomizePreferredReactionsModal()(dispatch, () => rootState, null);
            const [action] = dispatch.getCall(0).args;
            const result = (0, preferredReactions_1.reducer)(rootState.preferredReactions, action);
            const expectedEmoji = ['❤️', '👍🏿', '👎🏿', '😂', '😮', '😢'];
            chai_1.assert.deepEqual(result.customizePreferredReactionsModal, {
                draftPreferredReactions: expectedEmoji,
                originalPreferredReactions: expectedEmoji,
                selectedDraftEmojiIndex: undefined,
                isSaving: false,
                hadSaveError: false,
            });
        });
        it('opens the customization modal with stored values', () => {
            const storedPreferredReactionEmoji = ['✨', '❇️', '🎇', '🦈', '💖', '🅿️'];
            const emptyRootState = getEmptyRootState();
            const state = Object.assign(Object.assign({}, emptyRootState), { items: Object.assign(Object.assign({}, emptyRootState.items), { preferredReactionEmoji: storedPreferredReactionEmoji }) });
            const dispatch = sinon.spy();
            openCustomizePreferredReactionsModal()(dispatch, () => state, null);
            const [action] = dispatch.getCall(0).args;
            const result = (0, preferredReactions_1.reducer)(state.preferredReactions, action);
            chai_1.assert.deepEqual(result.customizePreferredReactionsModal, {
                draftPreferredReactions: storedPreferredReactionEmoji,
                originalPreferredReactions: storedPreferredReactionEmoji,
                selectedDraftEmojiIndex: undefined,
                isSaving: false,
                hadSaveError: false,
            });
        });
    });
    describe('replaceSelectedDraftEmoji', () => {
        const { replaceSelectedDraftEmoji } = preferredReactions_1.actions;
        it('is a no-op if the customization modal is not open', () => {
            const state = (0, preferredReactions_1.getInitialState)();
            const action = replaceSelectedDraftEmoji('🦈');
            const result = (0, preferredReactions_1.reducer)(state, action);
            chai_1.assert.strictEqual(result, state);
        });
        it('is a no-op if no emoji is selected', () => {
            const action = replaceSelectedDraftEmoji('💅');
            const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModal, action);
            chai_1.assert.strictEqual(result, stateWithOpenCustomizationModal);
        });
        it('replaces the selected draft emoji and deselects', () => {
            var _a, _b;
            const action = replaceSelectedDraftEmoji('🐱');
            const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModalAndSelectedEmoji, action);
            chai_1.assert.deepStrictEqual((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.draftPreferredReactions, ['✨', '🐱', '🎇', '🦈', '💖', '🅿️']);
            chai_1.assert.isUndefined((_b = result.customizePreferredReactionsModal) === null || _b === void 0 ? void 0 : _b.selectedDraftEmojiIndex);
        });
    });
    describe('resetDraftEmoji', () => {
        const { resetDraftEmoji } = preferredReactions_1.actions;
        function getAction(rootState) {
            const dispatch = sinon.spy();
            resetDraftEmoji()(dispatch, () => rootState, null);
            const [action] = dispatch.getCall(0).args;
            return action;
        }
        it('is a no-op if the customization modal is not open', () => {
            const rootState = getEmptyRootState();
            const state = rootState.preferredReactions;
            const action = getAction(rootState);
            const result = (0, preferredReactions_1.reducer)(state, action);
            chai_1.assert.strictEqual(result, state);
        });
        it('resets the draft emoji to the defaults', () => {
            var _a;
            const rootState = getRootState(stateWithOpenCustomizationModal);
            const action = getAction(rootState);
            const result = (0, preferredReactions_1.reducer)(rootState.preferredReactions, action);
            chai_1.assert.deepEqual((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.draftPreferredReactions, ['❤️', '👍', '👎', '😂', '😮', '😢']);
        });
        it('deselects any selected emoji', () => {
            var _a;
            const rootState = getRootState(stateWithOpenCustomizationModalAndSelectedEmoji);
            const action = getAction(rootState);
            const result = (0, preferredReactions_1.reducer)(rootState.preferredReactions, action);
            chai_1.assert.isUndefined((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.selectedDraftEmojiIndex);
        });
    });
    describe('savePreferredReactions', () => {
        const { savePreferredReactions } = preferredReactions_1.actions;
        // We want to create a fake ConversationController for testing purposes, and we need
        //   to sidestep typechecking to do that.
        /* eslint-disable @typescript-eslint/no-explicit-any */
        let storagePutStub;
        let captureChangeStub;
        let oldConversationController;
        beforeEach(() => {
            storagePutStub = sinonSandbox.stub(window.storage, 'put').resolves();
            oldConversationController = window.ConversationController;
            captureChangeStub = sinonSandbox.stub();
            window.ConversationController = {
                getOurConversationOrThrow: () => ({
                    captureChange: captureChangeStub,
                }),
            };
        });
        afterEach(() => {
            window.ConversationController = oldConversationController;
        });
        describe('thunk', () => {
            it('saves the preferred reaction emoji to local storage', async () => {
                await savePreferredReactions()(sinon.spy(), () => getRootState(stateWithOpenCustomizationModal), null);
                sinon.assert.calledWith(storagePutStub, 'preferredReactionEmoji', stateWithOpenCustomizationModal.customizePreferredReactionsModal
                    .draftPreferredReactions);
            });
            it('on success, enqueues a storage service upload', async () => {
                await savePreferredReactions()(sinon.spy(), () => getRootState(stateWithOpenCustomizationModal), null);
                sinon.assert.calledOnce(captureChangeStub);
            });
            it('on success, dispatches a pending action followed by a fulfilled action', async () => {
                const dispatch = sinon.spy();
                await savePreferredReactions()(dispatch, () => getRootState(stateWithOpenCustomizationModal), null);
                sinon.assert.calledTwice(dispatch);
                sinon.assert.calledWith(dispatch, {
                    type: 'preferredReactions/SAVE_PREFERRED_REACTIONS_PENDING',
                });
                sinon.assert.calledWith(dispatch, {
                    type: 'preferredReactions/SAVE_PREFERRED_REACTIONS_FULFILLED',
                });
            });
            it('on failure, dispatches a pending action followed by a rejected action', async () => {
                storagePutStub.rejects(new Error('something went wrong'));
                const dispatch = sinon.spy();
                await savePreferredReactions()(dispatch, () => getRootState(stateWithOpenCustomizationModal), null);
                sinon.assert.calledTwice(dispatch);
                sinon.assert.calledWith(dispatch, {
                    type: 'preferredReactions/SAVE_PREFERRED_REACTIONS_PENDING',
                });
                sinon.assert.calledWith(dispatch, {
                    type: 'preferredReactions/SAVE_PREFERRED_REACTIONS_REJECTED',
                });
            });
            it('on failure, does not enqueue a storage service upload', async () => {
                storagePutStub.rejects(new Error('something went wrong'));
                await savePreferredReactions()(sinon.spy(), () => getRootState(stateWithOpenCustomizationModal), null);
                sinon.assert.notCalled(captureChangeStub);
            });
        });
        describe('SAVE_PREFERRED_REACTIONS_FULFILLED', () => {
            const action = {
                type: 'preferredReactions/SAVE_PREFERRED_REACTIONS_FULFILLED',
            };
            it("does nothing if the modal isn't open", () => {
                const result = (0, preferredReactions_1.reducer)((0, preferredReactions_1.getInitialState)(), action);
                chai_1.assert.notProperty(result, 'customizePreferredReactionsModal');
            });
            it('closes the modal if open', () => {
                const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModal, action);
                chai_1.assert.notProperty(result, 'customizePreferredReactionsModal');
            });
        });
        describe('SAVE_PREFERRED_REACTIONS_PENDING', () => {
            const action = {
                type: 'preferredReactions/SAVE_PREFERRED_REACTIONS_PENDING',
            };
            it('marks the modal as "saving"', () => {
                var _a;
                const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModal, action);
                chai_1.assert.isTrue((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.isSaving);
            });
            it('clears any previous errors', () => {
                var _a;
                const state = Object.assign(Object.assign({}, stateWithOpenCustomizationModal), { customizePreferredReactionsModal: Object.assign(Object.assign({}, stateWithOpenCustomizationModal.customizePreferredReactionsModal), { hadSaveError: true }) });
                const result = (0, preferredReactions_1.reducer)(state, action);
                chai_1.assert.isFalse((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.hadSaveError);
            });
            it('deselects any selected emoji', () => {
                var _a;
                const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModalAndSelectedEmoji, action);
                chai_1.assert.isUndefined((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.selectedDraftEmojiIndex);
            });
        });
        describe('SAVE_PREFERRED_REACTIONS_REJECTED', () => {
            const action = {
                type: 'preferredReactions/SAVE_PREFERRED_REACTIONS_REJECTED',
            };
            it("does nothing if the modal isn't open", () => {
                const state = (0, preferredReactions_1.getInitialState)();
                const result = (0, preferredReactions_1.reducer)(state, action);
                chai_1.assert.strictEqual(result, state);
            });
            it('stops loading', () => {
                var _a;
                const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModal, action);
                chai_1.assert.isFalse((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.isSaving);
            });
            it('saves that there was an error', () => {
                var _a;
                const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModal, action);
                chai_1.assert.isTrue((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.hadSaveError);
            });
        });
    });
    describe('selectDraftEmojiToBeReplaced', () => {
        const { selectDraftEmojiToBeReplaced } = preferredReactions_1.actions;
        it('is a no-op if the customization modal is not open', () => {
            const state = (0, preferredReactions_1.getInitialState)();
            const action = selectDraftEmojiToBeReplaced(2);
            const result = (0, preferredReactions_1.reducer)(state, action);
            chai_1.assert.strictEqual(result, state);
        });
        it('is a no-op if the index is out of range', () => {
            const action = selectDraftEmojiToBeReplaced(99);
            const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModal, action);
            chai_1.assert.strictEqual(result, stateWithOpenCustomizationModal);
        });
        it('sets the index as the selected one', () => {
            var _a;
            const action = selectDraftEmojiToBeReplaced(3);
            const result = (0, preferredReactions_1.reducer)(stateWithOpenCustomizationModal, action);
            chai_1.assert.strictEqual((_a = result.customizePreferredReactionsModal) === null || _a === void 0 ? void 0 : _a.selectedDraftEmojiIndex, 3);
        });
    });
});
