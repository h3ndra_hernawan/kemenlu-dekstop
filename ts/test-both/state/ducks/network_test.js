"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const network_1 = require("../../../state/ducks/network");
describe('both/state/ducks/network', () => {
    describe('setChallengeStatus', () => {
        const { setChallengeStatus } = network_1.actions;
        it('updates whether we need to complete a server challenge', () => {
            const idleState = (0, network_1.reducer)((0, network_1.getEmptyState)(), setChallengeStatus('idle'));
            chai_1.assert.equal(idleState.challengeStatus, 'idle');
            const requiredState = (0, network_1.reducer)(idleState, setChallengeStatus('required'));
            chai_1.assert.equal(requiredState.challengeStatus, 'required');
            const pendingState = (0, network_1.reducer)(requiredState, setChallengeStatus('pending'));
            chai_1.assert.equal(pendingState.challengeStatus, 'pending');
        });
    });
});
