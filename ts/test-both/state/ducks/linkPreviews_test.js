"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const linkPreviews_1 = require("../../../state/ducks/linkPreviews");
describe('both/state/ducks/linkPreviews', () => {
    function getMockLinkPreview() {
        return {
            title: 'Hello World',
            domain: 'signal.org',
            url: 'https://www.signal.org',
            isStickerPack: false,
        };
    }
    describe('addLinkPreview', () => {
        const { addLinkPreview } = linkPreviews_1.actions;
        it('updates linkPreview', () => {
            const state = (0, linkPreviews_1.getEmptyState)();
            const linkPreview = getMockLinkPreview();
            const nextState = (0, linkPreviews_1.reducer)(state, addLinkPreview(linkPreview));
            chai_1.assert.strictEqual(nextState.linkPreview, linkPreview);
        });
    });
    describe('removeLinkPreview', () => {
        const { removeLinkPreview } = linkPreviews_1.actions;
        it('removes linkPreview', () => {
            const state = Object.assign(Object.assign({}, (0, linkPreviews_1.getEmptyState)()), { linkPreview: getMockLinkPreview() });
            const nextState = (0, linkPreviews_1.reducer)(state, removeLinkPreview());
            chai_1.assert.isUndefined(nextState.linkPreview);
        });
    });
});
