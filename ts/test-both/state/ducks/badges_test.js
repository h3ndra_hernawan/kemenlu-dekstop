"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const getFakeBadge_1 = require("../../helpers/getFakeBadge");
const iterables_1 = require("../../../util/iterables");
const BadgeImageTheme_1 = require("../../../badges/BadgeImageTheme");
const badges_1 = require("../../../state/ducks/badges");
describe('both/state/ducks/badges', () => {
    describe('badgeImageFileDownloaded', () => {
        const { badgeImageFileDownloaded } = badges_1.actions;
        it("does nothing if the URL isn't in the list of badges", () => {
            const state = {
                byId: { foo: (0, getFakeBadge_1.getFakeBadge)({ id: 'foo' }) },
            };
            const action = badgeImageFileDownloaded('https://foo.example.com/image.svg', '/path/to/file.svg');
            const result = (0, badges_1.reducer)(state, action);
            chai_1.assert.deepStrictEqual(result, state);
        });
        it('updates all badge image files with matching URLs', () => {
            var _a, _b;
            const state = {
                byId: {
                    badge1: Object.assign(Object.assign({}, (0, getFakeBadge_1.getFakeBadge)({ id: 'badge1' })), { images: [
                            ...Array(3).fill((0, iterables_1.zipObject)(Object.values(BadgeImageTheme_1.BadgeImageTheme), (0, iterables_1.repeat)({ url: 'https://example.com/a.svg' }))),
                            {
                                [BadgeImageTheme_1.BadgeImageTheme.Transparent]: {
                                    url: 'https://example.com/b.svg',
                                },
                            },
                        ] }),
                    badge2: (0, getFakeBadge_1.getFakeBadge)({ id: 'badge2' }),
                    badge3: Object.assign(Object.assign({}, (0, getFakeBadge_1.getFakeBadge)({ id: 'badge3' })), { images: Array(4).fill({
                            [BadgeImageTheme_1.BadgeImageTheme.Dark]: {
                                localPath: 'to be overridden',
                                url: 'https://example.com/a.svg',
                            },
                            [BadgeImageTheme_1.BadgeImageTheme.Light]: {
                                localPath: 'to be overridden',
                                url: 'https://example.com/a.svg',
                            },
                            [BadgeImageTheme_1.BadgeImageTheme.Transparent]: {
                                localPath: '/path/should/be/unchanged',
                                url: 'https://example.com/b.svg',
                            },
                        }) }),
                },
            };
            const action = badgeImageFileDownloaded('https://example.com/a.svg', '/path/to/file.svg');
            const result = (0, badges_1.reducer)(state, action);
            chai_1.assert.deepStrictEqual((_a = result.byId.badge1) === null || _a === void 0 ? void 0 : _a.images, [
                ...Array(3).fill((0, iterables_1.zipObject)(Object.values(BadgeImageTheme_1.BadgeImageTheme), (0, iterables_1.repeat)({
                    localPath: '/path/to/file.svg',
                    url: 'https://example.com/a.svg',
                }))),
                {
                    [BadgeImageTheme_1.BadgeImageTheme.Transparent]: {
                        url: 'https://example.com/b.svg',
                    },
                },
            ]);
            chai_1.assert.deepStrictEqual(result.byId.badge2, (0, getFakeBadge_1.getFakeBadge)({ id: 'badge2' }));
            chai_1.assert.deepStrictEqual((_b = result.byId.badge3) === null || _b === void 0 ? void 0 : _b.images, Array(4).fill({
                [BadgeImageTheme_1.BadgeImageTheme.Dark]: {
                    localPath: '/path/to/file.svg',
                    url: 'https://example.com/a.svg',
                },
                [BadgeImageTheme_1.BadgeImageTheme.Light]: {
                    localPath: '/path/to/file.svg',
                    url: 'https://example.com/a.svg',
                },
                [BadgeImageTheme_1.BadgeImageTheme.Transparent]: {
                    localPath: '/path/should/be/unchanged',
                    url: 'https://example.com/b.svg',
                },
            }));
        });
    });
});
