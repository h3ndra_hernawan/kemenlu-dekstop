"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const globalModals_1 = require("../../../state/ducks/globalModals");
describe('both/state/ducks/globalModals', () => {
    describe('toggleProfileEditor', () => {
        const { toggleProfileEditor } = globalModals_1.actions;
        it('toggles isProfileEditorVisible', () => {
            const state = (0, globalModals_1.getEmptyState)();
            const nextState = (0, globalModals_1.reducer)(state, toggleProfileEditor());
            chai_1.assert.isTrue(nextState.isProfileEditorVisible);
            const nextNextState = (0, globalModals_1.reducer)(nextState, toggleProfileEditor());
            chai_1.assert.isFalse(nextNextState.isProfileEditorVisible);
        });
    });
    describe('showWhatsNewModal/hideWhatsNewModal', () => {
        const { showWhatsNewModal, hideWhatsNewModal } = globalModals_1.actions;
        it('toggles isWhatsNewVisible to true', () => {
            const state = (0, globalModals_1.getEmptyState)();
            const nextState = (0, globalModals_1.reducer)(state, showWhatsNewModal());
            chai_1.assert.isTrue(nextState.isWhatsNewVisible);
            const nextNextState = (0, globalModals_1.reducer)(nextState, hideWhatsNewModal());
            chai_1.assert.isFalse(nextNextState.isWhatsNewVisible);
        });
    });
});
