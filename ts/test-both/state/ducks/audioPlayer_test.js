"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const audioPlayer_1 = require("../../../state/ducks/audioPlayer");
const conversations_1 = require("../../../state/ducks/conversations");
const noop_1 = require("../../../state/ducks/noop");
const reducer_1 = require("../../../state/reducer");
const { messageDeleted, messageChanged } = conversations_1.actions;
const MESSAGE_ID = 'message-id';
describe('both/state/ducks/audioPlayer', () => {
    const getEmptyRootState = () => {
        return (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
    };
    const getInitializedState = () => {
        const state = getEmptyRootState();
        const updated = (0, reducer_1.reducer)(state, audioPlayer_1.actions.setActiveAudioID(MESSAGE_ID, 'context'));
        chai_1.assert.strictEqual(updated.audioPlayer.activeAudioID, MESSAGE_ID);
        chai_1.assert.strictEqual(updated.audioPlayer.activeAudioContext, 'context');
        return updated;
    };
    describe('setActiveAudioID', () => {
        it("updates `activeAudioID` in the audioPlayer's state", () => {
            const state = getEmptyRootState();
            chai_1.assert.strictEqual(state.audioPlayer.activeAudioID, undefined);
            const updated = (0, reducer_1.reducer)(state, audioPlayer_1.actions.setActiveAudioID('test', 'context'));
            chai_1.assert.strictEqual(updated.audioPlayer.activeAudioID, 'test');
            chai_1.assert.strictEqual(updated.audioPlayer.activeAudioContext, 'context');
        });
    });
    it('resets activeAudioID when changing the conversation', () => {
        const state = getInitializedState();
        const updated = (0, reducer_1.reducer)(state, {
            type: 'SWITCH_TO_ASSOCIATED_VIEW',
            payload: { conversationId: 'any' },
        });
        chai_1.assert.strictEqual(updated.audioPlayer.activeAudioID, undefined);
        chai_1.assert.strictEqual(updated.audioPlayer.activeAudioContext, 'context');
    });
    it('resets activeAudioID when message was deleted', () => {
        const state = getInitializedState();
        const updated = (0, reducer_1.reducer)(state, messageDeleted(MESSAGE_ID, 'conversation-id'));
        chai_1.assert.strictEqual(updated.audioPlayer.activeAudioID, undefined);
        chai_1.assert.strictEqual(updated.audioPlayer.activeAudioContext, 'context');
    });
    it('resets activeAudioID when message was erased', () => {
        const state = getInitializedState();
        const updated = (0, reducer_1.reducer)(state, messageChanged(MESSAGE_ID, 'conversation-id', {
            id: MESSAGE_ID,
            type: 'incoming',
            sent_at: 1,
            received_at: 1,
            timestamp: 1,
            conversationId: 'conversation-id',
            deletedForEveryone: true,
        }));
        chai_1.assert.strictEqual(updated.audioPlayer.activeAudioID, undefined);
        chai_1.assert.strictEqual(updated.audioPlayer.activeAudioContext, 'context');
    });
});
