"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const composer_1 = require("../../../state/ducks/composer");
const noop_1 = require("../../../state/ducks/noop");
const reducer_1 = require("../../../state/reducer");
const MIME_1 = require("../../../types/MIME");
const fakeAttachment_1 = require("../../helpers/fakeAttachment");
describe('both/state/ducks/composer', () => {
    const QUOTED_MESSAGE = {
        conversationId: '123',
        quote: {
            attachments: [],
            id: 456,
            isViewOnce: false,
            messageId: '789',
            referencedMessageNotFound: false,
        },
    };
    const getRootStateFunction = (selectedConversationId) => {
        const state = (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
        return () => (Object.assign(Object.assign({}, state), { conversations: Object.assign(Object.assign({}, state.conversations), { selectedConversationId }) }));
    };
    describe('replaceAttachments', () => {
        it('replaces the attachments state', () => {
            const { replaceAttachments } = composer_1.actions;
            const dispatch = sinon.spy();
            const attachments = [
                {
                    contentType: MIME_1.IMAGE_JPEG,
                    pending: true,
                    size: 2433,
                    path: 'image.jpg',
                },
            ];
            replaceAttachments('123', attachments)(dispatch, getRootStateFunction('123'), null);
            const action = dispatch.getCall(0).args[0];
            const state = (0, composer_1.reducer)((0, composer_1.getEmptyState)(), action);
            chai_1.assert.deepEqual(state.attachments, attachments);
        });
        it('sets the high quality setting to false when there are no attachments', () => {
            const { replaceAttachments } = composer_1.actions;
            const dispatch = sinon.spy();
            const attachments = [];
            replaceAttachments('123', attachments)(dispatch, getRootStateFunction('123'), null);
            const action = dispatch.getCall(0).args[0];
            const state = (0, composer_1.reducer)(Object.assign(Object.assign({}, (0, composer_1.getEmptyState)()), { shouldSendHighQualityAttachments: true }), action);
            chai_1.assert.deepEqual(state.attachments, attachments);
            chai_1.assert.deepEqual(state.attachments, attachments);
            chai_1.assert.isFalse(state.shouldSendHighQualityAttachments);
        });
        it('does not update redux if the conversation is not selected', () => {
            const { replaceAttachments } = composer_1.actions;
            const dispatch = sinon.spy();
            const attachments = [(0, fakeAttachment_1.fakeDraftAttachment)()];
            replaceAttachments('123', attachments)(dispatch, getRootStateFunction('456'), null);
            chai_1.assert.isNull(dispatch.getCall(0));
        });
    });
    describe('resetComposer', () => {
        it('returns composer back to empty state', () => {
            const { resetComposer } = composer_1.actions;
            const nextState = (0, composer_1.reducer)({
                attachments: [],
                linkPreviewLoading: true,
                quotedMessage: QUOTED_MESSAGE,
                shouldSendHighQualityAttachments: true,
            }, resetComposer());
            chai_1.assert.deepEqual(nextState, (0, composer_1.getEmptyState)());
        });
    });
    describe('setLinkPreviewResult', () => {
        it('sets loading state when loading', () => {
            const { setLinkPreviewResult } = composer_1.actions;
            const state = (0, composer_1.getEmptyState)();
            const nextState = (0, composer_1.reducer)(state, setLinkPreviewResult(true));
            chai_1.assert.isTrue(nextState.linkPreviewLoading);
        });
        it('sets the link preview result', () => {
            var _a;
            const { setLinkPreviewResult } = composer_1.actions;
            const state = (0, composer_1.getEmptyState)();
            const nextState = (0, composer_1.reducer)(state, setLinkPreviewResult(false, {
                domain: 'https://www.signal.org/',
                title: 'Signal >> Careers',
                url: 'https://www.signal.org/workworkwork',
                description: 'Join an organization that empowers users by making private communication simple.',
                date: null,
            }));
            chai_1.assert.isFalse(nextState.linkPreviewLoading);
            chai_1.assert.equal((_a = nextState.linkPreviewResult) === null || _a === void 0 ? void 0 : _a.title, 'Signal >> Careers');
        });
    });
    describe('setMediaQualitySetting', () => {
        it('toggles the media quality setting', () => {
            const { setMediaQualitySetting } = composer_1.actions;
            const state = (0, composer_1.getEmptyState)();
            chai_1.assert.isFalse(state.shouldSendHighQualityAttachments);
            const nextState = (0, composer_1.reducer)(state, setMediaQualitySetting(true));
            chai_1.assert.isTrue(nextState.shouldSendHighQualityAttachments);
            const nextNextState = (0, composer_1.reducer)(nextState, setMediaQualitySetting(false));
            chai_1.assert.isFalse(nextNextState.shouldSendHighQualityAttachments);
        });
    });
    describe('setQuotedMessage', () => {
        it('sets the quoted message', () => {
            var _a, _b, _c;
            const { setQuotedMessage } = composer_1.actions;
            const state = (0, composer_1.getEmptyState)();
            const nextState = (0, composer_1.reducer)(state, setQuotedMessage(QUOTED_MESSAGE));
            chai_1.assert.equal((_a = nextState.quotedMessage) === null || _a === void 0 ? void 0 : _a.conversationId, '123');
            chai_1.assert.equal((_c = (_b = nextState.quotedMessage) === null || _b === void 0 ? void 0 : _b.quote) === null || _c === void 0 ? void 0 : _c.id, 456);
        });
    });
});
