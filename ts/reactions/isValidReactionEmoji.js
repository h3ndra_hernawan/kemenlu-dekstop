"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isValidReactionEmoji = void 0;
const RGI_Emoji_1 = __importDefault(require("emoji-regex/es2015/RGI_Emoji"));
const grapheme_1 = require("../util/grapheme");
const iterables_1 = require("../util/iterables");
function isValidReactionEmoji(value) {
    if (typeof value !== 'string') {
        return false;
    }
    // This is effectively `countGraphemes(value) === 1`, but doesn't require iterating
    //   through an extremely long string.
    const graphemes = (0, grapheme_1.getGraphemes)(value);
    const truncatedGraphemes = (0, iterables_1.take)(graphemes, 2);
    if ((0, iterables_1.size)(truncatedGraphemes) !== 1) {
        return false;
    }
    return (0, RGI_Emoji_1.default)().test(value);
}
exports.isValidReactionEmoji = isValidReactionEmoji;
