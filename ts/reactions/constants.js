"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.DEFAULT_PREFERRED_REACTION_EMOJI_SHORT_NAMES = void 0;
exports.DEFAULT_PREFERRED_REACTION_EMOJI_SHORT_NAMES = [
    'heart',
    'thumbsup',
    'thumbsdown',
    'joy',
    'open_mouth',
    'cry',
];
