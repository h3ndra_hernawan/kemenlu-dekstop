"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReactionSource = void 0;
var ReactionSource;
(function (ReactionSource) {
    ReactionSource[ReactionSource["FromSomeoneElse"] = 0] = "FromSomeoneElse";
    ReactionSource[ReactionSource["FromSync"] = 1] = "FromSync";
    ReactionSource[ReactionSource["FromThisDevice"] = 2] = "FromThisDevice";
})(ReactionSource = exports.ReactionSource || (exports.ReactionSource = {}));
