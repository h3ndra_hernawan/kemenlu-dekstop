"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.canBeSynced = exports.getPreferredReactionEmoji = void 0;
const lodash_1 = require("lodash");
const log = __importStar(require("../logging/log"));
const constants_1 = require("./constants");
const lib_1 = require("../components/emoji/lib");
const isValidReactionEmoji_1 = require("./isValidReactionEmoji");
const MAX_STORED_LENGTH = 20;
const MAX_ITEM_LENGTH = 20;
const PREFERRED_REACTION_EMOJI_COUNT = constants_1.DEFAULT_PREFERRED_REACTION_EMOJI_SHORT_NAMES.length;
function getPreferredReactionEmoji(storedValue, skinTone) {
    const storedValueAsArray = Array.isArray(storedValue)
        ? storedValue
        : [];
    return (0, lodash_1.times)(PREFERRED_REACTION_EMOJI_COUNT, index => {
        const storedItem = storedValueAsArray[index];
        if ((0, isValidReactionEmoji_1.isValidReactionEmoji)(storedItem)) {
            return storedItem;
        }
        const fallbackShortName = constants_1.DEFAULT_PREFERRED_REACTION_EMOJI_SHORT_NAMES[index];
        if (!fallbackShortName) {
            log.error('Index is out of range. Is the preferred count larger than the list of fallbacks?');
            return '❤️';
        }
        const fallbackEmoji = (0, lib_1.convertShortName)(fallbackShortName, skinTone);
        if (!fallbackEmoji) {
            log.error('No fallback emoji. Does the fallback list contain an invalid short name?');
            return '❤️';
        }
        return fallbackEmoji;
    });
}
exports.getPreferredReactionEmoji = getPreferredReactionEmoji;
const canBeSynced = (value) => Array.isArray(value) &&
    value.length <= MAX_STORED_LENGTH &&
    value.every(item => typeof item === 'string' && item.length <= MAX_ITEM_LENGTH);
exports.canBeSynced = canBeSynced;
