"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.markOutgoingReactionSent = exports.markOutgoingReactionFailed = exports.getUnsentConversationIds = exports.getNewestPendingOutgoingReaction = exports.addOutgoingReaction = void 0;
const lodash_1 = require("lodash");
const areObjectEntriesEqual_1 = require("../util/areObjectEntriesEqual");
const isReactionEqual = (a, b) => a === b ||
    Boolean(a && b && (0, areObjectEntriesEqual_1.areObjectEntriesEqual)(a, b, ['emoji', 'fromId', 'timestamp']));
const isOutgoingReactionFullySent = ({ isSentByConversationId = {}, }) => !isSentByConversationId ||
    Object.values(isSentByConversationId).every(lodash_1.identity);
const isOutgoingReactionPending = (0, lodash_1.negate)(isOutgoingReactionFullySent);
const isOutgoingReactionCompletelyUnsent = ({ isSentByConversationId = {}, }) => {
    const sendStates = Object.values(isSentByConversationId);
    return sendStates.length > 0 && sendStates.every(state => state === false);
};
function addOutgoingReaction(oldReactions, newReaction) {
    const pendingOutgoingReactions = new Set(oldReactions.filter(isOutgoingReactionPending));
    return [
        ...oldReactions.filter(re => !pendingOutgoingReactions.has(re)),
        newReaction,
    ];
}
exports.addOutgoingReaction = addOutgoingReaction;
function getNewestPendingOutgoingReaction(reactions, ourConversationId) {
    const ourReactions = reactions
        .filter(({ fromId }) => fromId === ourConversationId)
        .sort((a, b) => a.timestamp - b.timestamp);
    const newestFinishedReactionIndex = (0, lodash_1.findLastIndex)(ourReactions, re => re.emoji && isOutgoingReactionFullySent(re));
    const newestFinishedReaction = ourReactions[newestFinishedReactionIndex];
    const newestPendingReactionIndex = (0, lodash_1.findLastIndex)(ourReactions, isOutgoingReactionPending);
    const pendingReaction = newestPendingReactionIndex > newestFinishedReactionIndex
        ? ourReactions[newestPendingReactionIndex]
        : undefined;
    return pendingReaction
        ? {
            pendingReaction,
            // This might not be right in some cases. For example, imagine the following
            //   sequence:
            //
            // 1. I send reaction A to Alice and Bob, but it was only delivered to Alice.
            // 2. I send reaction B to Alice and Bob, but it was only delivered to Bob.
            // 3. I remove the reaction.
            //
            // Android and iOS don't care what your previous reaction is. Old Desktop versions
            //   *do* care, so we make our best guess. We should be able to remove this after
            //   Desktop has ignored this field for awhile. See commit
            //   `1dc353f08910389ad8cc5487949e6998e90038e2`.
            emojiToRemove: newestFinishedReaction === null || newestFinishedReaction === void 0 ? void 0 : newestFinishedReaction.emoji,
        }
        : {};
}
exports.getNewestPendingOutgoingReaction = getNewestPendingOutgoingReaction;
function* getUnsentConversationIds({ isSentByConversationId = {}, }) {
    for (const [id, isSent] of Object.entries(isSentByConversationId)) {
        if (!isSent) {
            yield id;
        }
    }
}
exports.getUnsentConversationIds = getUnsentConversationIds;
const markOutgoingReactionFailed = (reactions, reaction) => isOutgoingReactionCompletelyUnsent(reaction) || !reaction.emoji
    ? reactions.filter(re => !isReactionEqual(re, reaction))
    : reactions.map(re => isReactionEqual(re, reaction)
        ? (0, lodash_1.omit)(re, ['isSentByConversationId'])
        : re);
exports.markOutgoingReactionFailed = markOutgoingReactionFailed;
const markOutgoingReactionSent = (reactions, reaction, conversationIdsSentTo) => {
    const result = [];
    const newIsSentByConversationId = Object.assign({}, (reaction.isSentByConversationId || {}));
    for (const id of conversationIdsSentTo) {
        if ((0, lodash_1.has)(newIsSentByConversationId, id)) {
            newIsSentByConversationId[id] = true;
        }
    }
    const isFullySent = Object.values(newIsSentByConversationId).every(lodash_1.identity);
    for (const re of reactions) {
        if (!isReactionEqual(re, reaction)) {
            const shouldKeep = !isFullySent
                ? true
                : re.fromId !== reaction.fromId || re.timestamp > reaction.timestamp;
            if (shouldKeep) {
                result.push(re);
            }
            continue;
        }
        if (isFullySent) {
            if (re.emoji) {
                result.push((0, lodash_1.omit)(re, ['isSentByConversationId']));
            }
        }
        else {
            result.push(Object.assign(Object.assign({}, re), { isSentByConversationId: newIsSentByConversationId }));
        }
    }
    return result;
};
exports.markOutgoingReactionSent = markOutgoingReactionSent;
