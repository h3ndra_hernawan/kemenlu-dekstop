"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.enqueueReactionForSend = void 0;
const Reactions_1 = require("../messageModifiers/Reactions");
const ReactionSource_1 = require("./ReactionSource");
const getMessageById_1 = require("../messages/getMessageById");
const assert_1 = require("../util/assert");
async function enqueueReactionForSend({ emoji, messageId, remove, }) {
    var _a;
    const message = await (0, getMessageById_1.getMessageById)(messageId);
    (0, assert_1.strictAssert)(message, 'enqueueReactionForSend: no message found');
    const targetAuthorUuid = message.getSourceUuid();
    (0, assert_1.strictAssert)(targetAuthorUuid, `enqueueReactionForSend: message ${message.idForLogging()} had no source UUID`);
    const targetTimestamp = message.get('sent_at') || message.get('timestamp');
    (0, assert_1.strictAssert)(targetTimestamp, `enqueueReactionForSend: message ${message.idForLogging()} had no timestamp`);
    const reaction = new Reactions_1.ReactionModel({
        emoji,
        remove,
        targetAuthorUuid,
        targetTimestamp,
        fromId: window.ConversationController.getOurConversationIdOrThrow(),
        timestamp: Date.now(),
        source: ReactionSource_1.ReactionSource.FromThisDevice,
    });
    await ((_a = message.getConversation()) === null || _a === void 0 ? void 0 : _a.maybeApplyUniversalTimer(false));
    await message.handleReaction(reaction);
}
exports.enqueueReactionForSend = enqueueReactionForSend;
