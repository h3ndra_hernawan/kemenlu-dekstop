"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getKeysForIdentifier = void 0;
const signal_client_1 = require("@signalapp/signal-client");
const Errors_1 = require("./Errors");
const LibSignalStores_1 = require("../LibSignalStores");
const Address_1 = require("../types/Address");
const QualifiedAddress_1 = require("../types/QualifiedAddress");
const UUID_1 = require("../types/UUID");
const log = __importStar(require("../logging/log"));
async function getKeysForIdentifier(identifier, server, devicesToUpdate, accessKey) {
    try {
        const { keys, accessKeyFailed } = await getServerKeys(identifier, server, accessKey);
        await handleServerKeys(identifier, keys, devicesToUpdate);
        return {
            accessKeyFailed,
        };
    }
    catch (error) {
        if (error instanceof Errors_1.HTTPError && error.code === 404) {
            const theirUuid = UUID_1.UUID.lookup(identifier);
            if (theirUuid) {
                await window.textsecure.storage.protocol.archiveAllSessions(theirUuid);
            }
        }
        throw new Errors_1.UnregisteredUserError(identifier, error);
    }
}
exports.getKeysForIdentifier = getKeysForIdentifier;
async function getServerKeys(identifier, server, accessKey) {
    if (!accessKey) {
        return {
            keys: await server.getKeysForIdentifier(identifier),
        };
    }
    try {
        return {
            keys: await server.getKeysForIdentifierUnauth(identifier, undefined, {
                accessKey,
            }),
        };
    }
    catch (error) {
        if (error.code === 401 || error.code === 403) {
            return {
                accessKeyFailed: true,
                keys: await server.getKeysForIdentifier(identifier),
            };
        }
        throw error;
    }
}
async function handleServerKeys(identifier, response, devicesToUpdate) {
    const ourUuid = window.textsecure.storage.user.getCheckedUuid();
    const sessionStore = new LibSignalStores_1.Sessions({ ourUuid });
    const identityKeyStore = new LibSignalStores_1.IdentityKeys({ ourUuid });
    await Promise.all(response.devices.map(async (device) => {
        const { deviceId, registrationId, preKey, signedPreKey } = device;
        if (devicesToUpdate !== undefined &&
            !devicesToUpdate.includes(deviceId)) {
            return;
        }
        if (device.registrationId === 0) {
            log.info(`handleServerKeys/${identifier}: Got device registrationId zero!`);
        }
        if (!signedPreKey) {
            throw new Error(`getKeysForIdentifier/${identifier}: Missing signed prekey for deviceId ${deviceId}`);
        }
        const theirUuid = UUID_1.UUID.checkedLookup(identifier);
        const protocolAddress = signal_client_1.ProtocolAddress.new(theirUuid.toString(), deviceId);
        const preKeyId = (preKey === null || preKey === void 0 ? void 0 : preKey.keyId) || null;
        const preKeyObject = preKey
            ? signal_client_1.PublicKey.deserialize(Buffer.from(preKey.publicKey))
            : null;
        const signedPreKeyObject = signal_client_1.PublicKey.deserialize(Buffer.from(signedPreKey.publicKey));
        const identityKey = signal_client_1.PublicKey.deserialize(Buffer.from(response.identityKey));
        const preKeyBundle = signal_client_1.PreKeyBundle.new(registrationId, deviceId, preKeyId, preKeyObject, signedPreKey.keyId, signedPreKeyObject, Buffer.from(signedPreKey.signature), identityKey);
        const address = new QualifiedAddress_1.QualifiedAddress(ourUuid, new Address_1.Address(theirUuid, deviceId));
        await window.textsecure.storage.protocol
            .enqueueSessionJob(address, () => (0, signal_client_1.processPreKeyBundle)(preKeyBundle, protocolAddress, sessionStore, identityKeyStore))
            .catch(error => {
            var _a;
            if ((_a = error === null || error === void 0 ? void 0 : error.message) === null || _a === void 0 ? void 0 : _a.includes('untrusted identity for address')) {
                // eslint-disable-next-line no-param-reassign
                error.identityKey = response.identityKey;
            }
            throw error;
        });
    }));
}
