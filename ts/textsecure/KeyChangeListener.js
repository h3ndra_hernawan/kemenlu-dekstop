"use strict";
// Copyright 2017-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.init = void 0;
function init(signalProtocolStore) {
    signalProtocolStore.on('keychange', async (uuid) => {
        const conversation = await window.ConversationController.getOrCreateAndWait(uuid.toString(), 'private');
        conversation.addKeyChange(uuid);
        const groups = await window.ConversationController.getAllGroupsInvolvingUuid(uuid);
        for (const group of groups) {
            group.addKeyChange(uuid);
        }
    });
}
exports.init = init;
