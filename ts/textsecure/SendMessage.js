"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const long_1 = __importDefault(require("long"));
const p_queue_1 = __importDefault(require("p-queue"));
const signal_client_1 = require("@signalapp/signal-client");
const assert_1 = require("../util/assert");
const parseIntOrThrow_1 = require("../util/parseIntOrThrow");
const Address_1 = require("../types/Address");
const QualifiedAddress_1 = require("../types/QualifiedAddress");
const LibSignalStores_1 = require("../LibSignalStores");
const MIME_1 = require("../types/MIME");
const TaskWithTimeout_1 = __importDefault(require("./TaskWithTimeout"));
const OutgoingMessage_1 = __importDefault(require("./OutgoingMessage"));
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const Errors_1 = require("./Errors");
const iterables_1 = require("../util/iterables");
const handleMessageSend_1 = require("../util/handleMessageSend");
const protobuf_1 = require("../protobuf");
const log = __importStar(require("../logging/log"));
function makeAttachmentSendReady(attachment) {
    const { data } = attachment;
    if (!data) {
        throw new Error('makeAttachmentSendReady: Missing data, returning undefined');
    }
    return Object.assign(Object.assign({}, attachment), { contentType: (0, MIME_1.MIMETypeToString)(attachment.contentType), data });
}
class Message {
    constructor(options) {
        this.attachmentPointers = [];
        this.attachments = options.attachments || [];
        this.body = options.body;
        this.expireTimer = options.expireTimer;
        this.flags = options.flags;
        this.group = options.group;
        this.groupV2 = options.groupV2;
        this.needsSync = options.needsSync;
        this.preview = options.preview;
        this.profileKey = options.profileKey;
        this.quote = options.quote;
        this.recipients = options.recipients;
        this.sticker = options.sticker;
        this.reaction = options.reaction;
        this.timestamp = options.timestamp;
        this.deletedForEveryoneTimestamp = options.deletedForEveryoneTimestamp;
        this.mentions = options.mentions;
        this.groupCallUpdate = options.groupCallUpdate;
        if (!(this.recipients instanceof Array)) {
            throw new Error('Invalid recipient list');
        }
        if (!this.group && !this.groupV2 && this.recipients.length !== 1) {
            throw new Error('Invalid recipient list for non-group');
        }
        if (typeof this.timestamp !== 'number') {
            throw new Error('Invalid timestamp');
        }
        if (this.expireTimer !== undefined && this.expireTimer !== null) {
            if (typeof this.expireTimer !== 'number' || !(this.expireTimer >= 0)) {
                throw new Error('Invalid expireTimer');
            }
        }
        if (this.attachments) {
            if (!(this.attachments instanceof Array)) {
                throw new Error('Invalid message attachments');
            }
        }
        if (this.flags !== undefined) {
            if (typeof this.flags !== 'number') {
                throw new Error('Invalid message flags');
            }
        }
        if (this.isEndSession()) {
            if (this.body !== null ||
                this.group !== null ||
                this.attachments.length !== 0) {
                throw new Error('Invalid end session message');
            }
        }
        else {
            if (typeof this.timestamp !== 'number' ||
                (this.body && typeof this.body !== 'string')) {
                throw new Error('Invalid message body');
            }
            if (this.group) {
                if (typeof this.group.id !== 'string' ||
                    typeof this.group.type !== 'number') {
                    throw new Error('Invalid group context');
                }
            }
        }
    }
    isEndSession() {
        return (this.flags || 0) & protobuf_1.SignalService.DataMessage.Flags.END_SESSION;
    }
    toProto() {
        if (this.dataMessage) {
            return this.dataMessage;
        }
        const proto = new protobuf_1.SignalService.DataMessage();
        proto.timestamp = this.timestamp;
        proto.attachments = this.attachmentPointers;
        if (this.body) {
            proto.body = this.body;
            const mentionCount = this.mentions ? this.mentions.length : 0;
            const placeholders = this.body.match(/\uFFFC/g);
            const placeholderCount = placeholders ? placeholders.length : 0;
            log.info(`Sending a message with ${mentionCount} mentions and ${placeholderCount} placeholders`);
        }
        if (this.flags) {
            proto.flags = this.flags;
        }
        if (this.groupV2) {
            proto.groupV2 = new protobuf_1.SignalService.GroupContextV2();
            proto.groupV2.masterKey = this.groupV2.masterKey;
            proto.groupV2.revision = this.groupV2.revision;
            proto.groupV2.groupChange = this.groupV2.groupChange || null;
        }
        else if (this.group) {
            proto.group = new protobuf_1.SignalService.GroupContext();
            proto.group.id = Bytes.fromString(this.group.id);
            proto.group.type = this.group.type;
        }
        if (this.sticker) {
            proto.sticker = new protobuf_1.SignalService.DataMessage.Sticker();
            proto.sticker.packId = Bytes.fromHex(this.sticker.packId);
            proto.sticker.packKey = Bytes.fromBase64(this.sticker.packKey);
            proto.sticker.stickerId = this.sticker.stickerId;
            proto.sticker.emoji = this.sticker.emoji;
            if (this.sticker.attachmentPointer) {
                proto.sticker.data = this.sticker.attachmentPointer;
            }
        }
        if (this.reaction) {
            proto.reaction = new protobuf_1.SignalService.DataMessage.Reaction();
            proto.reaction.emoji = this.reaction.emoji || null;
            proto.reaction.remove = this.reaction.remove || false;
            proto.reaction.targetAuthorUuid = this.reaction.targetAuthorUuid || null;
            proto.reaction.targetTimestamp = this.reaction.targetTimestamp || null;
        }
        if (Array.isArray(this.preview)) {
            proto.preview = this.preview.map(preview => {
                const item = new protobuf_1.SignalService.DataMessage.Preview();
                item.title = preview.title;
                item.url = preview.url;
                item.description = preview.description || null;
                item.date = preview.date || null;
                if (preview.attachmentPointer) {
                    item.image = preview.attachmentPointer;
                }
                return item;
            });
        }
        if (this.quote) {
            const { QuotedAttachment } = protobuf_1.SignalService.DataMessage.Quote;
            const { BodyRange, Quote } = protobuf_1.SignalService.DataMessage;
            proto.quote = new Quote();
            const { quote } = proto;
            quote.id = this.quote.id || null;
            quote.authorUuid = this.quote.authorUuid || null;
            quote.text = this.quote.text || null;
            quote.attachments = (this.quote.attachments || []).map((attachment) => {
                const quotedAttachment = new QuotedAttachment();
                quotedAttachment.contentType = attachment.contentType;
                if (attachment.fileName) {
                    quotedAttachment.fileName = attachment.fileName;
                }
                if (attachment.attachmentPointer) {
                    quotedAttachment.thumbnail = attachment.attachmentPointer;
                }
                return quotedAttachment;
            });
            const bodyRanges = this.quote.bodyRanges || [];
            quote.bodyRanges = bodyRanges.map(range => {
                const bodyRange = new BodyRange();
                bodyRange.start = range.start;
                bodyRange.length = range.length;
                if (range.mentionUuid !== undefined) {
                    bodyRange.mentionUuid = range.mentionUuid;
                }
                return bodyRange;
            });
            if (quote.bodyRanges.length &&
                (!proto.requiredProtocolVersion ||
                    proto.requiredProtocolVersion <
                        protobuf_1.SignalService.DataMessage.ProtocolVersion.MENTIONS)) {
                proto.requiredProtocolVersion =
                    protobuf_1.SignalService.DataMessage.ProtocolVersion.MENTIONS;
            }
        }
        if (this.expireTimer) {
            proto.expireTimer = this.expireTimer;
        }
        if (this.profileKey) {
            proto.profileKey = this.profileKey;
        }
        if (this.deletedForEveryoneTimestamp) {
            proto.delete = {
                targetSentTimestamp: this.deletedForEveryoneTimestamp,
            };
        }
        if (this.mentions) {
            proto.requiredProtocolVersion =
                protobuf_1.SignalService.DataMessage.ProtocolVersion.MENTIONS;
            proto.bodyRanges = this.mentions.map(({ start, length, mentionUuid }) => ({
                start,
                length,
                mentionUuid,
            }));
        }
        if (this.groupCallUpdate) {
            const { GroupCallUpdate } = protobuf_1.SignalService.DataMessage;
            const groupCallUpdate = new GroupCallUpdate();
            groupCallUpdate.eraId = this.groupCallUpdate.eraId;
            proto.groupCallUpdate = groupCallUpdate;
        }
        this.dataMessage = proto;
        return proto;
    }
    encode() {
        return protobuf_1.SignalService.DataMessage.encode(this.toProto()).finish();
    }
}
class MessageSender {
    constructor(server) {
        this.server = server;
        this.pendingMessages = {};
    }
    async queueJobForIdentifier(identifier, runJob) {
        const { id } = await window.ConversationController.getOrCreateAndWait(identifier, 'private');
        this.pendingMessages[id] =
            this.pendingMessages[id] || new p_queue_1.default({ concurrency: 1 });
        const queue = this.pendingMessages[id];
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(runJob, `queueJobForIdentifier ${identifier} ${id}`);
        return queue.add(taskWithTimeout);
    }
    // Attachment upload functions
    _getAttachmentSizeBucket(size) {
        return Math.max(541, Math.floor(1.05 ** Math.ceil(Math.log(size) / Math.log(1.05))));
    }
    getRandomPadding() {
        // Generate a random int from 1 and 512
        const buffer = (0, Crypto_1.getRandomBytes)(2);
        const paddingLength = (new Uint16Array(buffer)[0] & 0x1ff) + 1;
        // Generate a random padding buffer of the chosen size
        return (0, Crypto_1.getRandomBytes)(paddingLength);
    }
    getPaddedAttachment(data) {
        const size = data.byteLength;
        const paddedSize = this._getAttachmentSizeBucket(size);
        const padding = (0, Crypto_1.getZeroes)(paddedSize - size);
        return Bytes.concatenate([data, padding]);
    }
    async makeAttachmentPointer(attachment) {
        (0, assert_1.assert)(typeof attachment === 'object' && attachment !== null, 'Got null attachment in `makeAttachmentPointer`');
        const { data, size } = attachment;
        if (!(data instanceof Uint8Array)) {
            throw new Error(`makeAttachmentPointer: data was a '${typeof data}' instead of Uint8Array`);
        }
        if (data.byteLength !== size) {
            throw new Error(`makeAttachmentPointer: Size ${size} did not match data.byteLength ${data.byteLength}`);
        }
        const padded = this.getPaddedAttachment(data);
        const key = (0, Crypto_1.getRandomBytes)(64);
        const iv = (0, Crypto_1.getRandomBytes)(16);
        const result = (0, Crypto_1.encryptAttachment)(padded, key, iv);
        const id = await this.server.putAttachment(result.ciphertext);
        const proto = new protobuf_1.SignalService.AttachmentPointer();
        proto.cdnId = long_1.default.fromString(id);
        proto.contentType = attachment.contentType;
        proto.key = key;
        proto.size = attachment.size;
        proto.digest = result.digest;
        if (attachment.fileName) {
            proto.fileName = attachment.fileName;
        }
        if (attachment.flags) {
            proto.flags = attachment.flags;
        }
        if (attachment.width) {
            proto.width = attachment.width;
        }
        if (attachment.height) {
            proto.height = attachment.height;
        }
        if (attachment.caption) {
            proto.caption = attachment.caption;
        }
        if (attachment.blurHash) {
            proto.blurHash = attachment.blurHash;
        }
        return proto;
    }
    async uploadAttachments(message) {
        await Promise.all(message.attachments.map(attachment => this.makeAttachmentPointer(attachment)))
            .then(attachmentPointers => {
            // eslint-disable-next-line no-param-reassign
            message.attachmentPointers = attachmentPointers;
        })
            .catch(error => {
            if (error instanceof Errors_1.HTTPError) {
                throw new Errors_1.MessageError(message, error);
            }
            else {
                throw error;
            }
        });
    }
    async uploadLinkPreviews(message) {
        try {
            const preview = await Promise.all((message.preview || []).map(async (item) => {
                if (!item.image) {
                    return item;
                }
                const attachment = makeAttachmentSendReady(item.image);
                if (!attachment) {
                    return item;
                }
                return Object.assign(Object.assign({}, item), { attachmentPointer: await this.makeAttachmentPointer(attachment) });
            }));
            // eslint-disable-next-line no-param-reassign
            message.preview = preview;
        }
        catch (error) {
            if (error instanceof Errors_1.HTTPError) {
                throw new Errors_1.MessageError(message, error);
            }
            else {
                throw error;
            }
        }
    }
    async uploadSticker(message) {
        try {
            const { sticker } = message;
            if (!sticker) {
                return;
            }
            if (!sticker.data) {
                throw new Error('uploadSticker: No sticker data to upload!');
            }
            // eslint-disable-next-line no-param-reassign
            message.sticker = Object.assign(Object.assign({}, sticker), { attachmentPointer: await this.makeAttachmentPointer(sticker.data) });
        }
        catch (error) {
            if (error instanceof Errors_1.HTTPError) {
                throw new Errors_1.MessageError(message, error);
            }
            else {
                throw error;
            }
        }
    }
    async uploadThumbnails(message) {
        const makePointer = this.makeAttachmentPointer.bind(this);
        const { quote } = message;
        if (!quote || !quote.attachments || quote.attachments.length === 0) {
            return;
        }
        await Promise.all(quote.attachments.map((attachment) => {
            if (!attachment.thumbnail) {
                return null;
            }
            return makePointer(attachment.thumbnail).then(pointer => {
                // eslint-disable-next-line no-param-reassign
                attachment.attachmentPointer = pointer;
            });
        })).catch(error => {
            if (error instanceof Errors_1.HTTPError) {
                throw new Errors_1.MessageError(message, error);
            }
            else {
                throw error;
            }
        });
    }
    // Proto assembly
    async getDataMessage(options) {
        const message = await this.getHydratedMessage(options);
        return message.encode();
    }
    async getContentMessage(options) {
        const message = await this.getHydratedMessage(options);
        const dataMessage = message.toProto();
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.dataMessage = dataMessage;
        return contentMessage;
    }
    async getHydratedMessage(attributes) {
        const message = new Message(attributes);
        await Promise.all([
            this.uploadAttachments(message),
            this.uploadThumbnails(message),
            this.uploadLinkPreviews(message),
            this.uploadSticker(message),
        ]);
        return message;
    }
    getTypingContentMessage(options) {
        const ACTION_ENUM = protobuf_1.SignalService.TypingMessage.Action;
        const { recipientId, groupId, isTyping, timestamp } = options;
        if (!recipientId && !groupId) {
            throw new Error('getTypingContentMessage: Need to provide either recipientId or groupId!');
        }
        const finalTimestamp = timestamp || Date.now();
        const action = isTyping ? ACTION_ENUM.STARTED : ACTION_ENUM.STOPPED;
        const typingMessage = new protobuf_1.SignalService.TypingMessage();
        if (groupId) {
            typingMessage.groupId = groupId;
        }
        typingMessage.action = action;
        typingMessage.timestamp = finalTimestamp;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.typingMessage = typingMessage;
        return contentMessage;
    }
    getAttrsFromGroupOptions(options) {
        var _a;
        const { messageText, timestamp, attachments, quote, preview, sticker, reaction, expireTimer, profileKey, deletedForEveryoneTimestamp, groupV2, groupV1, mentions, groupCallUpdate, } = options;
        if (!groupV1 && !groupV2) {
            throw new Error('getAttrsFromGroupOptions: Neither group1 nor groupv2 information provided!');
        }
        const myE164 = window.textsecure.storage.user.getNumber();
        const myUuid = (_a = window.textsecure.storage.user.getUuid()) === null || _a === void 0 ? void 0 : _a.toString();
        const groupMembers = (groupV2 === null || groupV2 === void 0 ? void 0 : groupV2.members) || (groupV1 === null || groupV1 === void 0 ? void 0 : groupV1.members) || [];
        // We should always have a UUID but have this check just in case we don't.
        let isNotMe;
        if (myUuid) {
            isNotMe = r => r !== myE164 && r !== myUuid.toString();
        }
        else {
            isNotMe = r => r !== myE164;
        }
        const blockedIdentifiers = new Set((0, iterables_1.concat)(window.storage.blocked.getBlockedUuids(), window.storage.blocked.getBlockedNumbers()));
        const recipients = groupMembers.filter(recipient => isNotMe(recipient) && !blockedIdentifiers.has(recipient));
        return {
            attachments,
            body: messageText,
            deletedForEveryoneTimestamp,
            expireTimer,
            groupCallUpdate,
            groupV2,
            group: groupV1
                ? {
                    id: groupV1.id,
                    type: protobuf_1.SignalService.GroupContext.Type.DELIVER,
                }
                : undefined,
            mentions,
            preview,
            profileKey,
            quote,
            reaction,
            recipients,
            sticker,
            timestamp,
        };
    }
    createSyncMessage() {
        const syncMessage = new protobuf_1.SignalService.SyncMessage();
        syncMessage.padding = this.getRandomPadding();
        return syncMessage;
    }
    // Low-level sends
    async sendMessage({ messageOptions, contentHint, groupId, options, }) {
        const message = new Message(messageOptions);
        return Promise.all([
            this.uploadAttachments(message),
            this.uploadThumbnails(message),
            this.uploadLinkPreviews(message),
            this.uploadSticker(message),
        ]).then(async () => new Promise((resolve, reject) => {
            this.sendMessageProto({
                callback: (res) => {
                    res.dataMessage = message.encode();
                    if (res.errors && res.errors.length > 0) {
                        reject(new Errors_1.SendMessageProtoError(res));
                    }
                    else {
                        resolve(res);
                    }
                },
                contentHint,
                groupId,
                options,
                proto: message.toProto(),
                recipients: message.recipients || [],
                timestamp: message.timestamp,
            });
        }));
    }
    sendMessageProto({ callback, contentHint, groupId, options, proto, recipients, sendLogCallback, timestamp, }) {
        const rejections = window.textsecure.storage.get('signedKeyRotationRejected', 0);
        if (rejections > 5) {
            throw new Errors_1.SignedPreKeyRotationError();
        }
        const outgoing = new OutgoingMessage_1.default({
            callback,
            contentHint,
            groupId,
            identifiers: recipients,
            message: proto,
            options,
            sendLogCallback,
            server: this.server,
            timestamp,
        });
        recipients.forEach(identifier => {
            this.queueJobForIdentifier(identifier, async () => outgoing.sendToIdentifier(identifier));
        });
    }
    async sendMessageProtoAndWait({ timestamp, recipients, proto, contentHint, groupId, options, }) {
        return new Promise((resolve, reject) => {
            const callback = (result) => {
                if (result && result.errors && result.errors.length > 0) {
                    reject(new Errors_1.SendMessageProtoError(result));
                    return;
                }
                resolve(result);
            };
            this.sendMessageProto({
                callback,
                contentHint,
                groupId,
                options,
                proto,
                recipients,
                timestamp,
            });
        });
    }
    async sendIndividualProto({ identifier, proto, timestamp, contentHint, options, }) {
        (0, assert_1.assert)(identifier, "Identifier can't be undefined");
        return new Promise((resolve, reject) => {
            const callback = (res) => {
                if (res && res.errors && res.errors.length > 0) {
                    reject(new Errors_1.SendMessageProtoError(res));
                }
                else {
                    resolve(res);
                }
            };
            this.sendMessageProto({
                callback,
                contentHint,
                groupId: undefined,
                options,
                proto,
                recipients: [identifier],
                timestamp,
            });
        });
    }
    // You might wonder why this takes a groupId. models/messages.resend() can send a group
    //   message to just one person.
    async sendMessageToIdentifier({ identifier, messageText, attachments, quote, preview, sticker, reaction, deletedForEveryoneTimestamp, timestamp, expireTimer, contentHint, groupId, profileKey, options, }) {
        return this.sendMessage({
            messageOptions: {
                recipients: [identifier],
                body: messageText,
                timestamp,
                attachments,
                quote,
                preview,
                sticker,
                reaction,
                deletedForEveryoneTimestamp,
                expireTimer,
                profileKey,
            },
            contentHint,
            groupId,
            options,
        });
    }
    // Support for sync messages
    // Note: this is used for sending real messages to your other devices after sending a
    //   message to others.
    async sendSyncMessage({ encodedDataMessage, timestamp, destination, destinationUuid, expirationStartTimestamp, conversationIdsSentTo = [], conversationIdsWithSealedSender = new Set(), isUpdate, options, }) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const dataMessage = protobuf_1.SignalService.DataMessage.decode(encodedDataMessage);
        const sentMessage = new protobuf_1.SignalService.SyncMessage.Sent();
        sentMessage.timestamp = timestamp;
        sentMessage.message = dataMessage;
        if (destination) {
            sentMessage.destination = destination;
        }
        if (destinationUuid) {
            sentMessage.destinationUuid = destinationUuid;
        }
        if (expirationStartTimestamp) {
            sentMessage.expirationStartTimestamp = expirationStartTimestamp;
        }
        if (isUpdate) {
            sentMessage.isRecipientUpdate = true;
        }
        // Though this field has 'unidenified' in the name, it should have entries for each
        //   number we sent to.
        if (!(0, iterables_1.isEmpty)(conversationIdsSentTo)) {
            sentMessage.unidentifiedStatus = [
                ...(0, iterables_1.map)(conversationIdsSentTo, conversationId => {
                    const status = new protobuf_1.SignalService.SyncMessage.Sent.UnidentifiedDeliveryStatus();
                    const conv = window.ConversationController.get(conversationId);
                    if (conv) {
                        const e164 = conv.get('e164');
                        if (e164) {
                            status.destination = e164;
                        }
                        const uuid = conv.get('uuid');
                        if (uuid) {
                            status.destinationUuid = uuid;
                        }
                    }
                    status.unidentified =
                        conversationIdsWithSealedSender.has(conversationId);
                    return status;
                }),
            ];
        }
        const syncMessage = this.createSyncMessage();
        syncMessage.sent = sentMessage;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp,
            contentHint: ContentHint.RESENDABLE,
            options,
        });
    }
    async sendRequestBlockSyncMessage(options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const request = new protobuf_1.SignalService.SyncMessage.Request();
        request.type = protobuf_1.SignalService.SyncMessage.Request.Type.BLOCKED;
        const syncMessage = this.createSyncMessage();
        syncMessage.request = request;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.IMPLICIT,
            options,
        });
    }
    async sendRequestConfigurationSyncMessage(options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const request = new protobuf_1.SignalService.SyncMessage.Request();
        request.type = protobuf_1.SignalService.SyncMessage.Request.Type.CONFIGURATION;
        const syncMessage = this.createSyncMessage();
        syncMessage.request = request;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.IMPLICIT,
            options,
        });
    }
    async sendRequestGroupSyncMessage(options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const request = new protobuf_1.SignalService.SyncMessage.Request();
        request.type = protobuf_1.SignalService.SyncMessage.Request.Type.GROUPS;
        const syncMessage = this.createSyncMessage();
        syncMessage.request = request;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.IMPLICIT,
            options,
        });
    }
    async sendRequestContactSyncMessage(options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const request = new protobuf_1.SignalService.SyncMessage.Request();
        request.type = protobuf_1.SignalService.SyncMessage.Request.Type.CONTACTS;
        const syncMessage = this.createSyncMessage();
        syncMessage.request = request;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.IMPLICIT,
            options,
        });
    }
    async sendFetchManifestSyncMessage(options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const fetchLatest = new protobuf_1.SignalService.SyncMessage.FetchLatest();
        fetchLatest.type = protobuf_1.SignalService.SyncMessage.FetchLatest.Type.STORAGE_MANIFEST;
        const syncMessage = this.createSyncMessage();
        syncMessage.fetchLatest = fetchLatest;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.IMPLICIT,
            options,
        });
    }
    async sendFetchLocalProfileSyncMessage(options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const fetchLatest = new protobuf_1.SignalService.SyncMessage.FetchLatest();
        fetchLatest.type = protobuf_1.SignalService.SyncMessage.FetchLatest.Type.LOCAL_PROFILE;
        const syncMessage = this.createSyncMessage();
        syncMessage.fetchLatest = fetchLatest;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.IMPLICIT,
            options,
        });
    }
    async sendRequestKeySyncMessage(options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const request = new protobuf_1.SignalService.SyncMessage.Request();
        request.type = protobuf_1.SignalService.SyncMessage.Request.Type.KEYS;
        const syncMessage = this.createSyncMessage();
        syncMessage.request = request;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.IMPLICIT,
            options,
        });
    }
    async syncReadMessages(reads, options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const syncMessage = this.createSyncMessage();
        syncMessage.read = [];
        for (let i = 0; i < reads.length; i += 1) {
            const proto = new protobuf_1.SignalService.SyncMessage.Read(reads[i]);
            syncMessage.read.push(proto);
        }
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.RESENDABLE,
            options,
        });
    }
    async syncView(views, options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const syncMessage = this.createSyncMessage();
        syncMessage.viewed = views.map(view => new protobuf_1.SignalService.SyncMessage.Viewed(view));
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.RESENDABLE,
            options,
        });
    }
    async syncViewOnceOpen(sender, senderUuid, timestamp, options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const syncMessage = this.createSyncMessage();
        const viewOnceOpen = new protobuf_1.SignalService.SyncMessage.ViewOnceOpen();
        if (sender !== undefined) {
            viewOnceOpen.sender = sender;
        }
        viewOnceOpen.senderUuid = senderUuid;
        viewOnceOpen.timestamp = timestamp;
        syncMessage.viewOnceOpen = viewOnceOpen;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.RESENDABLE,
            options,
        });
    }
    async syncMessageRequestResponse(responseArgs, options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const syncMessage = this.createSyncMessage();
        const response = new protobuf_1.SignalService.SyncMessage.MessageRequestResponse();
        if (responseArgs.threadE164 !== undefined) {
            response.threadE164 = responseArgs.threadE164;
        }
        if (responseArgs.threadUuid !== undefined) {
            response.threadUuid = responseArgs.threadUuid;
        }
        if (responseArgs.groupId) {
            response.groupId = responseArgs.groupId;
        }
        response.type = responseArgs.type;
        syncMessage.messageRequestResponse = response;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.RESENDABLE,
            options,
        });
    }
    async sendStickerPackSync(operations, options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const ENUM = protobuf_1.SignalService.SyncMessage.StickerPackOperation.Type;
        const packOperations = operations.map(item => {
            const { packId, packKey, installed } = item;
            const operation = new protobuf_1.SignalService.SyncMessage.StickerPackOperation();
            operation.packId = Bytes.fromHex(packId);
            operation.packKey = Bytes.fromBase64(packKey);
            operation.type = installed ? ENUM.INSTALL : ENUM.REMOVE;
            return operation;
        });
        const syncMessage = this.createSyncMessage();
        syncMessage.stickerPackOperation = packOperations;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.IMPLICIT,
            options,
        });
    }
    async syncVerification(destinationE164, destinationUuid, state, identityKey, options) {
        const myUuid = window.textsecure.storage.user.getCheckedUuid();
        const now = Date.now();
        if (!destinationE164 && !destinationUuid) {
            throw new Error('syncVerification: Neither e164 nor UUID were provided');
        }
        // Get padding which we can share between null message and verified sync
        const padding = this.getRandomPadding();
        // First send a null message to mask the sync message.
        await (0, handleMessageSend_1.handleMessageSend)(this.sendNullMessage({ uuid: destinationUuid, e164: destinationE164, padding }, options), {
            messageIds: [],
            sendType: 'nullMessage',
        });
        const verified = new protobuf_1.SignalService.Verified();
        verified.state = state;
        if (destinationE164) {
            verified.destination = destinationE164;
        }
        if (destinationUuid) {
            verified.destinationUuid = destinationUuid;
        }
        verified.identityKey = identityKey;
        verified.nullMessage = padding;
        const syncMessage = this.createSyncMessage();
        syncMessage.verified = verified;
        const secondMessage = new protobuf_1.SignalService.Content();
        secondMessage.syncMessage = syncMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: myUuid.toString(),
            proto: secondMessage,
            timestamp: now,
            contentHint: ContentHint.RESENDABLE,
            options,
        });
    }
    // Sending messages to contacts
    async sendProfileKeyUpdate(profileKey, recipients, options, groupId) {
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendMessage({
            messageOptions: Object.assign({ recipients, timestamp: Date.now(), profileKey, flags: protobuf_1.SignalService.DataMessage.Flags.PROFILE_KEY_UPDATE }, (groupId
                ? {
                    group: {
                        id: groupId,
                        type: protobuf_1.SignalService.GroupContext.Type.DELIVER,
                    },
                }
                : {})),
            contentHint: ContentHint.IMPLICIT,
            groupId: undefined,
            options,
        });
    }
    async sendCallingMessage(recipientId, callingMessage, options) {
        const recipients = [recipientId];
        const finalTimestamp = Date.now();
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.callingMessage = callingMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendMessageProtoAndWait({
            timestamp: finalTimestamp,
            recipients,
            proto: contentMessage,
            contentHint: ContentHint.DEFAULT,
            groupId: undefined,
            options,
        });
    }
    async sendDeliveryReceipt(options) {
        return this.sendReceiptMessage(Object.assign(Object.assign({}, options), { type: protobuf_1.SignalService.ReceiptMessage.Type.DELIVERY }));
    }
    async sendReadReceipts(options) {
        return this.sendReceiptMessage(Object.assign(Object.assign({}, options), { type: protobuf_1.SignalService.ReceiptMessage.Type.READ }));
    }
    async sendViewedReceipts(options) {
        return this.sendReceiptMessage(Object.assign(Object.assign({}, options), { type: protobuf_1.SignalService.ReceiptMessage.Type.VIEWED }));
    }
    async sendReceiptMessage({ senderE164, senderUuid, timestamps, type, options, }) {
        if (!senderUuid && !senderE164) {
            throw new Error('sendReceiptMessage: Neither uuid nor e164 was provided!');
        }
        const receiptMessage = new protobuf_1.SignalService.ReceiptMessage();
        receiptMessage.type = type;
        receiptMessage.timestamp = timestamps;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.receiptMessage = receiptMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendIndividualProto({
            identifier: senderUuid || senderE164,
            proto: contentMessage,
            timestamp: Date.now(),
            contentHint: ContentHint.RESENDABLE,
            options,
        });
    }
    async sendNullMessage({ uuid, e164, padding, }, options) {
        const nullMessage = new protobuf_1.SignalService.NullMessage();
        const identifier = uuid || e164;
        if (!identifier) {
            throw new Error('sendNullMessage: Got neither uuid nor e164!');
        }
        nullMessage.padding = padding || this.getRandomPadding();
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.nullMessage = nullMessage;
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        // We want the NullMessage to look like a normal outgoing message
        const timestamp = Date.now();
        return this.sendIndividualProto({
            identifier,
            proto: contentMessage,
            timestamp,
            contentHint: ContentHint.IMPLICIT,
            options,
        });
    }
    async sendExpirationTimerUpdateToIdentifier(identifier, expireTimer, timestamp, profileKey, options) {
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendMessage({
            messageOptions: {
                recipients: [identifier],
                timestamp,
                expireTimer,
                profileKey,
                flags: protobuf_1.SignalService.DataMessage.Flags.EXPIRATION_TIMER_UPDATE,
            },
            contentHint: ContentHint.RESENDABLE,
            groupId: undefined,
            options,
        });
    }
    async sendRetryRequest({ groupId, options, plaintext, uuid, }) {
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        return this.sendMessageProtoAndWait({
            timestamp: Date.now(),
            recipients: [uuid],
            proto: plaintext,
            contentHint: ContentHint.DEFAULT,
            groupId,
            options,
        });
    }
    // Group sends
    // Used to ensure that when we send to a group the old way, we save to the send log as
    //   we send to each recipient. Then we don't have a long delay between the first send
    //   and the final save to the database with all recipients.
    makeSendLogCallback({ contentHint, messageId, proto, sendType, timestamp, }) {
        let initialSavePromise;
        return async ({ identifier, deviceIds, }) => {
            if (!(0, handleMessageSend_1.shouldSaveProto)(sendType)) {
                return;
            }
            const conversation = window.ConversationController.get(identifier);
            if (!conversation) {
                log.warn(`makeSendLogCallback: Unable to find conversation for identifier ${identifier}`);
                return;
            }
            const recipientUuid = conversation.get('uuid');
            if (!recipientUuid) {
                log.warn(`makeSendLogCallback: Conversation ${conversation.idForLogging()} had no UUID`);
                return;
            }
            if (!initialSavePromise) {
                initialSavePromise = window.Signal.Data.insertSentProto({
                    timestamp,
                    proto,
                    contentHint,
                }, {
                    recipients: { [recipientUuid]: deviceIds },
                    messageIds: messageId ? [messageId] : [],
                });
                await initialSavePromise;
            }
            else {
                const id = await initialSavePromise;
                await window.Signal.Data.insertProtoRecipients({
                    id,
                    recipientUuid,
                    deviceIds,
                });
            }
        };
    }
    // No functions should really call this; since most group sends are now via Sender Key
    async sendGroupProto({ contentHint, groupId, options, proto, recipients, sendLogCallback, timestamp = Date.now(), }) {
        var _a;
        const dataMessage = proto.dataMessage
            ? protobuf_1.SignalService.DataMessage.encode(proto.dataMessage).finish()
            : undefined;
        const myE164 = window.textsecure.storage.user.getNumber();
        const myUuid = (_a = window.textsecure.storage.user.getUuid()) === null || _a === void 0 ? void 0 : _a.toString();
        const identifiers = recipients.filter(id => id !== myE164 && id !== myUuid);
        if (identifiers.length === 0) {
            return Promise.resolve({
                dataMessage,
                errors: [],
                failoverIdentifiers: [],
                successfulIdentifiers: [],
                unidentifiedDeliveries: [],
            });
        }
        return new Promise((resolve, reject) => {
            const callback = (res) => {
                res.dataMessage = dataMessage;
                if (res.errors && res.errors.length > 0) {
                    reject(new Errors_1.SendMessageProtoError(res));
                }
                else {
                    resolve(res);
                }
            };
            this.sendMessageProto({
                callback,
                contentHint,
                groupId,
                options,
                proto,
                recipients: identifiers,
                sendLogCallback,
                timestamp,
            });
        });
    }
    async getSenderKeyDistributionMessage(distributionId) {
        const ourUuid = window.textsecure.storage.user.getCheckedUuid();
        const ourDeviceId = (0, parseIntOrThrow_1.parseIntOrThrow)(window.textsecure.storage.user.getDeviceId(), 'getSenderKeyDistributionMessage');
        const protocolAddress = signal_client_1.ProtocolAddress.new(ourUuid.toString(), ourDeviceId);
        const address = new QualifiedAddress_1.QualifiedAddress(ourUuid, new Address_1.Address(ourUuid, ourDeviceId));
        const senderKeyStore = new LibSignalStores_1.SenderKeys({ ourUuid });
        return window.textsecure.storage.protocol.enqueueSenderKeyJob(address, async () => signal_client_1.SenderKeyDistributionMessage.create(protocolAddress, distributionId, senderKeyStore));
    }
    // The one group send exception - a message that should never be sent via sender key
    async sendSenderKeyDistributionMessage({ contentHint, distributionId, groupId, identifiers, }, options) {
        const contentMessage = new protobuf_1.SignalService.Content();
        const timestamp = Date.now();
        log.info(`sendSenderKeyDistributionMessage: Sending ${distributionId} with timestamp ${timestamp}`);
        const senderKeyDistributionMessage = await this.getSenderKeyDistributionMessage(distributionId);
        contentMessage.senderKeyDistributionMessage =
            senderKeyDistributionMessage.serialize();
        const sendLogCallback = identifiers.length > 1
            ? this.makeSendLogCallback({
                contentHint,
                proto: Buffer.from(protobuf_1.SignalService.Content.encode(contentMessage).finish()),
                sendType: 'senderKeyDistributionMessage',
                timestamp,
            })
            : undefined;
        return this.sendGroupProto({
            contentHint,
            groupId,
            options,
            proto: contentMessage,
            recipients: identifiers,
            sendLogCallback,
            timestamp,
        });
    }
    // GroupV1-only functions; not to be used in the future
    async leaveGroup(groupId, groupIdentifiers, options) {
        const timestamp = Date.now();
        const proto = new protobuf_1.SignalService.Content({
            dataMessage: {
                group: {
                    id: Bytes.fromString(groupId),
                    type: protobuf_1.SignalService.GroupContext.Type.QUIT,
                },
            },
        });
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        const contentHint = ContentHint.RESENDABLE;
        const sendLogCallback = groupIdentifiers.length > 1
            ? this.makeSendLogCallback({
                contentHint,
                proto: Buffer.from(protobuf_1.SignalService.Content.encode(proto).finish()),
                sendType: 'legacyGroupChange',
                timestamp,
            })
            : undefined;
        return this.sendGroupProto({
            contentHint,
            groupId: undefined,
            options,
            proto,
            recipients: groupIdentifiers,
            sendLogCallback,
            timestamp,
        });
    }
    async sendExpirationTimerUpdateToGroup(groupId, groupIdentifiers, expireTimer, timestamp, profileKey, options) {
        var _a;
        const myNumber = window.textsecure.storage.user.getNumber();
        const myUuid = (_a = window.textsecure.storage.user.getUuid()) === null || _a === void 0 ? void 0 : _a.toString();
        const recipients = groupIdentifiers.filter(identifier => identifier !== myNumber && identifier !== myUuid);
        const messageOptions = {
            recipients,
            timestamp,
            expireTimer,
            profileKey,
            flags: protobuf_1.SignalService.DataMessage.Flags.EXPIRATION_TIMER_UPDATE,
            group: {
                id: groupId,
                type: protobuf_1.SignalService.GroupContext.Type.DELIVER,
            },
        };
        const proto = await this.getContentMessage(messageOptions);
        if (recipients.length === 0) {
            return Promise.resolve({
                successfulIdentifiers: [],
                failoverIdentifiers: [],
                errors: [],
                unidentifiedDeliveries: [],
                dataMessage: await this.getDataMessage(messageOptions),
            });
        }
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        const contentHint = ContentHint.RESENDABLE;
        const sendLogCallback = groupIdentifiers.length > 1
            ? this.makeSendLogCallback({
                contentHint,
                proto: Buffer.from(protobuf_1.SignalService.Content.encode(proto).finish()),
                sendType: 'expirationTimerUpdate',
                timestamp,
            })
            : undefined;
        return this.sendGroupProto({
            contentHint,
            groupId: undefined,
            options,
            proto,
            recipients,
            sendLogCallback,
            timestamp,
        });
    }
    // Simple pass-throughs
    async getProfile(number, options) {
        const { accessKey } = options;
        if (accessKey) {
            const unauthOptions = Object.assign(Object.assign({}, options), { accessKey });
            return this.server.getProfileUnauth(number, unauthOptions);
        }
        return this.server.getProfile(number, options);
    }
    async getProfileForUsername(username) {
        return this.server.getProfileForUsername(username);
    }
    async getUuidsForE164s(numbers) {
        return this.server.getUuidsForE164s(numbers);
    }
    async getUuidsForE164sV2(numbers) {
        return this.server.getUuidsForE164sV2(numbers);
    }
    async getAvatar(path) {
        return this.server.getAvatar(path);
    }
    async getSticker(packId, stickerId) {
        return this.server.getSticker(packId, stickerId);
    }
    async getStickerPackManifest(packId) {
        return this.server.getStickerPackManifest(packId);
    }
    async createGroup(group, options) {
        return this.server.createGroup(group, options);
    }
    async uploadGroupAvatar(avatar, options) {
        return this.server.uploadGroupAvatar(avatar, options);
    }
    async getGroup(options) {
        return this.server.getGroup(options);
    }
    async getGroupFromLink(groupInviteLink, auth) {
        return this.server.getGroupFromLink(groupInviteLink, auth);
    }
    async getGroupLog(startVersion, options) {
        return this.server.getGroupLog(startVersion, options);
    }
    async getGroupAvatar(key) {
        return this.server.getGroupAvatar(key);
    }
    async modifyGroup(changes, options, inviteLinkBase64) {
        return this.server.modifyGroup(changes, options, inviteLinkBase64);
    }
    async sendWithSenderKey(data, accessKeys, timestamp, online) {
        return this.server.sendWithSenderKey(data, accessKeys, timestamp, online);
    }
    async fetchLinkPreviewMetadata(href, abortSignal) {
        return this.server.fetchLinkPreviewMetadata(href, abortSignal);
    }
    async fetchLinkPreviewImage(href, abortSignal) {
        return this.server.fetchLinkPreviewImage(href, abortSignal);
    }
    async makeProxiedRequest(url, options) {
        return this.server.makeProxiedRequest(url, options);
    }
    async getStorageCredentials() {
        return this.server.getStorageCredentials();
    }
    async getStorageManifest(options) {
        return this.server.getStorageManifest(options);
    }
    async getStorageRecords(data, options) {
        return this.server.getStorageRecords(data, options);
    }
    async modifyStorageRecords(data, options) {
        return this.server.modifyStorageRecords(data, options);
    }
    async getGroupMembershipToken(options) {
        return this.server.getGroupExternalCredential(options);
    }
    async sendChallengeResponse(challengeResponse) {
        return this.server.sendChallengeResponse(challengeResponse);
    }
    async putProfile(jsonData) {
        return this.server.putProfile(jsonData);
    }
    async uploadAvatar(requestHeaders, avatarData) {
        return this.server.uploadAvatar(requestHeaders, avatarData);
    }
    async putUsername(username) {
        return this.server.putUsername(username);
    }
    async deleteUsername() {
        return this.server.deleteUsername();
    }
}
exports.default = MessageSender;
