"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.textsecure = void 0;
const EventTarget_1 = __importDefault(require("./EventTarget"));
const AccountManager_1 = __importDefault(require("./AccountManager"));
const MessageReceiver_1 = __importDefault(require("./MessageReceiver"));
const Helpers_1 = __importDefault(require("./Helpers"));
const ContactsParser_1 = require("./ContactsParser");
const SyncRequest_1 = __importDefault(require("./SyncRequest"));
const SendMessage_1 = __importDefault(require("./SendMessage"));
const Storage_1 = require("./Storage");
const WebAPI = __importStar(require("./WebAPI"));
const WebsocketResources_1 = __importDefault(require("./WebsocketResources"));
exports.textsecure = {
    utils: Helpers_1.default,
    storage: new Storage_1.Storage(),
    AccountManager: AccountManager_1.default,
    ContactBuffer: ContactsParser_1.ContactBuffer,
    EventTarget: EventTarget_1.default,
    GroupBuffer: ContactsParser_1.GroupBuffer,
    MessageReceiver: MessageReceiver_1.default,
    MessageSender: SendMessage_1.default,
    SyncRequest: SyncRequest_1.default,
    WebAPI,
    WebSocketResource: WebsocketResources_1.default,
};
exports.default = exports.textsecure;
