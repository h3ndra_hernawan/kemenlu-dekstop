"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.connect = void 0;
const websocket_1 = require("websocket");
const AbortableProcess_1 = require("../util/AbortableProcess");
const assert_1 = require("../util/assert");
const explodePromise_1 = require("../util/explodePromise");
const getUserAgent_1 = require("../util/getUserAgent");
const durations = __importStar(require("../util/durations"));
const log = __importStar(require("../logging/log"));
const Timers = __importStar(require("../Timers"));
const Errors_1 = require("./Errors");
const Utils_1 = require("./Utils");
const TEN_SECONDS = 10 * durations.SECOND;
function connect({ url, certificateAuthority, version, proxyAgent, timeout = TEN_SECONDS, createResource, }) {
    const fixedScheme = url
        .replace('https://', 'wss://')
        .replace('http://', 'ws://');
    const headers = {
        'User-Agent': (0, getUserAgent_1.getUserAgent)(version),
    };
    const client = new websocket_1.client({
        tlsOptions: {
            ca: certificateAuthority,
            agent: proxyAgent,
        },
        maxReceivedFrameSize: 0x210000,
    });
    client.connect(fixedScheme, undefined, undefined, headers);
    const { stack } = new Error();
    const { promise, resolve, reject } = (0, explodePromise_1.explodePromise)();
    const timer = Timers.setTimeout(() => {
        reject(new Errors_1.ConnectTimeoutError('Connection timed out'));
        client.abort();
    }, timeout);
    let resource;
    client.on('connect', socket => {
        Timers.clearTimeout(timer);
        resource = createResource(socket);
        resolve(resource);
    });
    client.on('httpResponse', async (response) => {
        Timers.clearTimeout(timer);
        const statusCode = response.statusCode || -1;
        await (0, Utils_1.handleStatusCode)(statusCode);
        const error = new Errors_1.HTTPError('connectResource: invalid websocket response', {
            code: statusCode || -1,
            headers: {},
            stack,
        });
        const translatedError = (0, Utils_1.translateError)(error);
        (0, assert_1.strictAssert)(translatedError, '`httpResponse` event cannot be emitted with 200 status code');
        reject(translatedError);
    });
    client.on('connectFailed', e => {
        Timers.clearTimeout(timer);
        reject(new Errors_1.HTTPError('connectResource: connectFailed', {
            code: -1,
            headers: {},
            response: e.toString(),
            stack,
        }));
    });
    return new AbortableProcess_1.AbortableProcess(`WebSocket.connect(${url})`, {
        abort() {
            if (resource) {
                log.warn(`WebSocket: closing socket ${url}`);
                resource.close(3000, 'aborted');
            }
            else {
                log.warn(`WebSocket: aborting connection ${url}`);
                Timers.clearTimeout(timer);
                client.abort();
            }
        },
    }, promise);
}
exports.connect = connect;
