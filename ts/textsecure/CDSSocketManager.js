"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CDSSocketManager = void 0;
const proxy_agent_1 = __importDefault(require("proxy-agent"));
const signal_client_1 = require("@signalapp/signal-client");
const Bytes = __importStar(require("../Bytes"));
const Curve_1 = require("../Curve");
const log = __importStar(require("../logging/log"));
const CDSSocket_1 = require("./CDSSocket");
const WebSocket_1 = require("./WebSocket");
class CDSSocketManager {
    constructor(options) {
        this.options = options;
        this.publicKey = signal_client_1.PublicKey.deserialize(Buffer.from((0, Curve_1.prefixPublicKey)(Bytes.fromHex(options.publicKey))));
        this.codeHash = Buffer.from(Bytes.fromHex(options.codeHash));
        if (options.proxyUrl) {
            this.proxyAgent = new proxy_agent_1.default(options.proxyUrl);
        }
    }
    async request(options) {
        log.info('CDSSocketManager: connecting socket');
        const socket = await this.connect().getResult();
        log.info('CDSSocketManager: connected socket');
        try {
            return await socket.request(options);
        }
        finally {
            log.info('CDSSocketManager: closing socket');
            socket.close(3000, 'Normal');
        }
    }
    connect() {
        const enclaveClient = signal_client_1.HsmEnclaveClient.new(this.publicKey, [this.codeHash]);
        const { publicKey: publicKeyHex, codeHash: codeHashHex, version, } = this.options;
        const url = `${this.options.url}/discovery/${publicKeyHex}/${codeHashHex}`;
        return (0, WebSocket_1.connect)({
            url,
            version,
            proxyAgent: this.proxyAgent,
            certificateAuthority: this.options.certificateAuthority,
            createResource: (socket) => {
                return new CDSSocket_1.CDSSocket(socket, enclaveClient);
            },
        });
    }
}
exports.CDSSocketManager = CDSSocketManager;
