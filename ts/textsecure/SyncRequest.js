"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const EventTarget_1 = __importDefault(require("./EventTarget"));
const MessageReceiver_1 = __importDefault(require("./MessageReceiver"));
const SendMessage_1 = __importDefault(require("./SendMessage"));
const assert_1 = require("../util/assert");
const getSendOptions_1 = require("../util/getSendOptions");
const handleMessageSend_1 = require("../util/handleMessageSend");
const log = __importStar(require("../logging/log"));
class SyncRequestInner extends EventTarget_1.default {
    constructor(sender, receiver, timeoutMillis) {
        super();
        this.sender = sender;
        this.receiver = receiver;
        this.started = false;
        if (!(sender instanceof SendMessage_1.default) ||
            !(receiver instanceof MessageReceiver_1.default)) {
            throw new Error('Tried to construct a SyncRequest without MessageSender and MessageReceiver');
        }
        this.oncontact = this.onContactSyncComplete.bind(this);
        receiver.addEventListener('contactSync', this.oncontact);
        this.ongroup = this.onGroupSyncComplete.bind(this);
        receiver.addEventListener('groupSync', this.ongroup);
        this.timeoutMillis = timeoutMillis || 60000;
    }
    async start() {
        if (this.started) {
            (0, assert_1.assert)(false, 'SyncRequestInner: started more than once. Doing nothing');
            return;
        }
        this.started = true;
        const { sender } = this;
        const ourConversation = window.ConversationController.getOurConversationOrThrow();
        const sendOptions = await (0, getSendOptions_1.getSendOptions)(ourConversation.attributes, {
            syncMessage: true,
        });
        if (window.ConversationController.areWePrimaryDevice()) {
            log.warn('SyncRequest.start: We are primary device; returning early');
            return;
        }
        log.info('SyncRequest created. Sending config sync request...');
        (0, handleMessageSend_1.handleMessageSend)(sender.sendRequestConfigurationSyncMessage(sendOptions), {
            messageIds: [],
            sendType: 'otherSync',
        });
        log.info('SyncRequest now sending block sync request...');
        (0, handleMessageSend_1.handleMessageSend)(sender.sendRequestBlockSyncMessage(sendOptions), {
            messageIds: [],
            sendType: 'otherSync',
        });
        log.info('SyncRequest now sending contact sync message...');
        (0, handleMessageSend_1.handleMessageSend)(sender.sendRequestContactSyncMessage(sendOptions), {
            messageIds: [],
            sendType: 'otherSync',
        })
            .then(() => {
            log.info('SyncRequest now sending group sync message...');
            return (0, handleMessageSend_1.handleMessageSend)(sender.sendRequestGroupSyncMessage(sendOptions), { messageIds: [], sendType: 'otherSync' });
        })
            .catch((error) => {
            log.error('SyncRequest error:', error && error.stack ? error.stack : error);
        });
        this.timeout = setTimeout(this.onTimeout.bind(this), this.timeoutMillis);
    }
    onContactSyncComplete() {
        this.contactSync = true;
        this.update();
    }
    onGroupSyncComplete() {
        this.groupSync = true;
        this.update();
    }
    update() {
        if (this.contactSync && this.groupSync) {
            this.dispatchEvent(new Event('success'));
            this.cleanup();
        }
    }
    onTimeout() {
        if (this.contactSync || this.groupSync) {
            this.dispatchEvent(new Event('success'));
        }
        else {
            this.dispatchEvent(new Event('timeout'));
        }
        this.cleanup();
    }
    cleanup() {
        clearTimeout(this.timeout);
        this.receiver.removeEventListener('contactsync', this.oncontact);
        this.receiver.removeEventListener('groupSync', this.ongroup);
        delete this.listeners;
    }
}
class SyncRequest {
    constructor(sender, receiver, timeoutMillis) {
        const inner = new SyncRequestInner(sender, receiver, timeoutMillis);
        this.inner = inner;
        this.addEventListener = inner.addEventListener.bind(inner);
        this.removeEventListener = inner.removeEventListener.bind(inner);
    }
    start() {
        this.inner.start();
    }
}
exports.default = SyncRequest;
