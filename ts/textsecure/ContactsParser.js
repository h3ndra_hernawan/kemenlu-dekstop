"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactBuffer = exports.GroupBuffer = void 0;
/* eslint-disable max-classes-per-file */
const protobufjs_1 = require("protobufjs");
const protobuf_1 = require("../protobuf");
const normalizeUuid_1 = require("../util/normalizeUuid");
const log = __importStar(require("../logging/log"));
class ParserBase {
    constructor(bytes, decoder) {
        this.decoder = decoder;
        this.reader = new protobufjs_1.Reader(bytes);
    }
    decodeDelimited() {
        var _a;
        if (this.reader.pos === this.reader.len) {
            return undefined; // eof
        }
        try {
            const proto = this.decoder.decodeDelimited(this.reader);
            if (!proto) {
                return undefined;
            }
            if (!proto.avatar) {
                return Object.assign(Object.assign({}, proto), { avatar: null });
            }
            const attachmentLen = (_a = proto.avatar.length) !== null && _a !== void 0 ? _a : 0;
            const avatarData = this.reader.buf.slice(this.reader.pos, this.reader.pos + attachmentLen);
            this.reader.skip(attachmentLen);
            return Object.assign(Object.assign({}, proto), { avatar: Object.assign(Object.assign({}, proto.avatar), { data: avatarData }) });
        }
        catch (error) {
            log.error('ProtoParser.next error:', error && error.stack ? error.stack : error);
            return undefined;
        }
    }
}
class GroupBuffer extends ParserBase {
    constructor(arrayBuffer) {
        super(arrayBuffer, protobuf_1.SignalService.GroupDetails);
    }
    next() {
        const proto = this.decodeDelimited();
        if (!proto) {
            return undefined;
        }
        if (!proto.members) {
            return proto;
        }
        return Object.assign(Object.assign({}, proto), { members: proto.members.map((member, i) => {
                if (!member.uuid) {
                    return member;
                }
                return Object.assign(Object.assign({}, member), { uuid: (0, normalizeUuid_1.normalizeUuid)(member.uuid, `GroupBuffer.member[${i}].uuid`) });
            }) });
    }
}
exports.GroupBuffer = GroupBuffer;
class ContactBuffer extends ParserBase {
    constructor(arrayBuffer) {
        super(arrayBuffer, protobuf_1.SignalService.ContactDetails);
    }
    next() {
        const proto = this.decodeDelimited();
        if (!proto) {
            return undefined;
        }
        if (!proto.uuid) {
            return proto;
        }
        const { verified } = proto;
        return Object.assign(Object.assign({}, proto), { verified: verified && verified.destinationUuid
                ? Object.assign(Object.assign({}, verified), { destinationUuid: (0, normalizeUuid_1.normalizeUuid)(verified.destinationUuid, 'ContactBuffer.verified.destinationUuid') }) : verified, uuid: (0, normalizeUuid_1.normalizeUuid)(proto.uuid, 'ContactBuffer.uuid') });
    }
}
exports.ContactBuffer = ContactBuffer;
