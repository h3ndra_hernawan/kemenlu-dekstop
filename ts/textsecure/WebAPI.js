"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialize = exports.multiRecipient410ResponseSchema = exports.multiRecipient409ResponseSchema = exports.multiRecipient200ResponseSchema = void 0;
/* eslint-disable no-param-reassign */
/* eslint-disable no-bitwise */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-nested-ternary */
/* eslint-disable @typescript-eslint/no-explicit-any */
const abort_controller_1 = __importDefault(require("abort-controller"));
const node_fetch_1 = __importDefault(require("node-fetch"));
const proxy_agent_1 = __importDefault(require("proxy-agent"));
const https_1 = require("https");
const p_props_1 = __importDefault(require("p-props"));
const lodash_1 = require("lodash");
const crypto_1 = require("crypto");
const node_forge_1 = require("node-forge");
const is_1 = __importDefault(require("@sindresorhus/is"));
const p_queue_1 = __importDefault(require("p-queue"));
const uuid_1 = require("uuid");
const zod_1 = require("zod");
const long_1 = __importDefault(require("long"));
const assert_1 = require("../util/assert");
const durations = __importStar(require("../util/durations"));
const getUserAgent_1 = require("../util/getUserAgent");
const getStreamWithTimeout_1 = require("../util/getStreamWithTimeout");
const userLanguages_1 = require("../util/userLanguages");
const webSafeBase64_1 = require("../util/webSafeBase64");
const errors_1 = require("../types/errors");
const Stickers_1 = require("../types/Stickers");
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const Curve_1 = require("../Curve");
const linkPreviewFetch = __importStar(require("../linkPreviews/linkPreviewFetch"));
const isBadgeImageFileUrlValid_1 = require("../badges/isBadgeImageFileUrlValid");
const SocketManager_1 = require("./SocketManager");
const CDSSocketManager_1 = require("./CDSSocketManager");
const protobuf_1 = require("../protobuf");
const Errors_1 = require("./Errors");
const Utils_1 = require("./Utils");
const log = __importStar(require("../logging/log"));
const url_1 = require("../util/url");
// Note: this will break some code that expects to be able to use err.response when a
//   web request fails, because it will force it to text. But it is very useful for
//   debugging failed requests.
const DEBUG = false;
let sgxConstantCache = null;
function makeLong(value) {
    return long_1.default.fromString(value);
}
function getSgxConstants() {
    if (sgxConstantCache) {
        return sgxConstantCache;
    }
    sgxConstantCache = {
        SGX_FLAGS_INITTED: makeLong('x0000000000000001L'),
        SGX_FLAGS_DEBUG: makeLong('x0000000000000002L'),
        SGX_FLAGS_MODE64BIT: makeLong('x0000000000000004L'),
        SGX_FLAGS_PROVISION_KEY: makeLong('x0000000000000004L'),
        SGX_FLAGS_EINITTOKEN_KEY: makeLong('x0000000000000004L'),
        SGX_FLAGS_RESERVED: makeLong('xFFFFFFFFFFFFFFC8L'),
        SGX_XFRM_LEGACY: makeLong('x0000000000000003L'),
        SGX_XFRM_AVX: makeLong('x0000000000000006L'),
        SGX_XFRM_RESERVED: makeLong('xFFFFFFFFFFFFFFF8L'),
    };
    return sgxConstantCache;
}
function _createRedactor(...toReplace) {
    // NOTE: It would be nice to remove this cast, but TypeScript doesn't support
    //   it. However, there is [an issue][0] that discusses this in more detail.
    // [0]: https://github.com/Microsoft/TypeScript/issues/16069
    const stringsToReplace = toReplace.filter(Boolean);
    return href => stringsToReplace.reduce((result, stringToReplace) => {
        const pattern = RegExp((0, lodash_1.escapeRegExp)(stringToReplace), 'g');
        const replacement = `[REDACTED]${stringToReplace.slice(-3)}`;
        return result.replace(pattern, replacement);
    }, href);
}
function _validateResponse(response, schema) {
    try {
        for (const i in schema) {
            switch (schema[i]) {
                case 'object':
                case 'string':
                case 'number':
                    if (typeof response[i] !== schema[i]) {
                        return false;
                    }
                    break;
                default:
            }
        }
    }
    catch (ex) {
        return false;
    }
    return true;
}
const FIVE_MINUTES = 5 * durations.MINUTE;
const GET_ATTACHMENT_CHUNK_TIMEOUT = 10 * durations.SECOND;
const agents = {};
function getContentType(response) {
    if (response.headers && response.headers.get) {
        return response.headers.get('content-type');
    }
    return null;
}
exports.multiRecipient200ResponseSchema = zod_1.z
    .object({
    uuids404: zod_1.z.array(zod_1.z.string()).optional(),
    needsSync: zod_1.z.boolean().optional(),
})
    .passthrough();
exports.multiRecipient409ResponseSchema = zod_1.z.array(zod_1.z
    .object({
    uuid: zod_1.z.string(),
    devices: zod_1.z
        .object({
        missingDevices: zod_1.z.array(zod_1.z.number()).optional(),
        extraDevices: zod_1.z.array(zod_1.z.number()).optional(),
    })
        .passthrough(),
})
    .passthrough());
exports.multiRecipient410ResponseSchema = zod_1.z.array(zod_1.z
    .object({
    uuid: zod_1.z.string(),
    devices: zod_1.z
        .object({
        staleDevices: zod_1.z.array(zod_1.z.number()).optional(),
    })
        .passthrough(),
})
    .passthrough());
function isSuccess(status) {
    return status >= 0 && status < 400;
}
function getHostname(url) {
    const urlObject = new URL(url);
    return urlObject.hostname;
}
async function _promiseAjax(providedUrl, options) {
    const { proxyUrl, socketManager } = options;
    const url = providedUrl || `${options.host}/${options.path}`;
    const logType = socketManager ? '(WS)' : '(REST)';
    const redactedURL = options.redactUrl ? options.redactUrl(url) : url;
    const unauthLabel = options.unauthenticated ? ' (unauth)' : '';
    log.info(`${options.type} ${logType} ${redactedURL}${unauthLabel}`);
    const timeout = typeof options.timeout === 'number' ? options.timeout : 10000;
    const agentType = options.unauthenticated ? 'unauth' : 'auth';
    const cacheKey = `${proxyUrl}-${agentType}`;
    const { timestamp } = agents[cacheKey] || { timestamp: null };
    if (!timestamp || timestamp + FIVE_MINUTES < Date.now()) {
        if (timestamp) {
            log.info(`Cycling agent for type ${cacheKey}`);
        }
        agents[cacheKey] = {
            agent: proxyUrl
                ? new proxy_agent_1.default(proxyUrl)
                : new https_1.Agent({ keepAlive: true }),
            timestamp: Date.now(),
        };
    }
    const { agent } = agents[cacheKey];
    const fetchOptions = {
        method: options.type,
        body: options.data,
        headers: Object.assign({ 'User-Agent': (0, getUserAgent_1.getUserAgent)(options.version), 'X-Signal-Agent': 'OWD' }, options.headers),
        redirect: options.redirect,
        agent,
        ca: options.certificateAuthority,
        timeout,
        abortSignal: options.abortSignal,
    };
    if (fetchOptions.body instanceof Uint8Array) {
        // node-fetch doesn't support Uint8Array, only node Buffer
        const contentLength = fetchOptions.body.byteLength;
        fetchOptions.body = Buffer.from(fetchOptions.body);
        // node-fetch doesn't set content-length like S3 requires
        fetchOptions.headers['Content-Length'] = contentLength.toString();
    }
    const { accessKey, basicAuth, unauthenticated } = options;
    if (basicAuth) {
        fetchOptions.headers.Authorization = `Basic ${basicAuth}`;
    }
    else if (unauthenticated) {
        if (!accessKey) {
            throw new Error('_promiseAjax: mode is unauthenticated, but accessKey was not provided');
        }
        // Access key is already a Base64 string
        fetchOptions.headers['Unidentified-Access-Key'] = accessKey;
    }
    else if (options.user && options.password) {
        const auth = Bytes.toBase64(Bytes.fromString(`${options.user}:${options.password}`));
        fetchOptions.headers.Authorization = `Basic ${auth}`;
    }
    if (options.contentType) {
        fetchOptions.headers['Content-Type'] = options.contentType;
    }
    let response;
    let result;
    try {
        response = socketManager
            ? await socketManager.fetch(url, fetchOptions)
            : await (0, node_fetch_1.default)(url, fetchOptions);
        if (options.serverUrl &&
            getHostname(options.serverUrl) === getHostname(url)) {
            await (0, Utils_1.handleStatusCode)(response.status);
            if (!unauthenticated && response.status === 401) {
                log.error('Got 401 from Signal Server. We might be unlinked.');
                window.Whisper.events.trigger('mightBeUnlinked');
            }
        }
        if (DEBUG && !isSuccess(response.status)) {
            result = await response.text();
        }
        else if ((options.responseType === 'json' ||
            options.responseType === 'jsonwithdetails') &&
            /^application\/json(;.*)?$/.test(response.headers.get('Content-Type') || '')) {
            result = await response.json();
        }
        else if (options.responseType === 'bytes' ||
            options.responseType === 'byteswithdetails') {
            result = await response.buffer();
        }
        else if (options.responseType === 'stream') {
            result = response.body;
        }
        else {
            result = await response.textConverted();
        }
    }
    catch (e) {
        log.error(options.type, logType, redactedURL, 0, 'Error');
        const stack = `${e.stack}\nInitial stack:\n${options.stack}`;
        throw makeHTTPError('promiseAjax catch', 0, {}, e.toString(), stack);
    }
    if (!isSuccess(response.status)) {
        log.error(options.type, logType, redactedURL, response.status, 'Error');
        throw makeHTTPError('promiseAjax: error response', response.status, response.headers.raw(), result, options.stack);
    }
    if (options.responseType === 'json' ||
        options.responseType === 'jsonwithdetails') {
        if (options.validateResponse) {
            if (!_validateResponse(result, options.validateResponse)) {
                log.error(options.type, logType, redactedURL, response.status, 'Error');
                throw makeHTTPError('promiseAjax: invalid response', response.status, response.headers.raw(), result, options.stack);
            }
        }
    }
    log.info(options.type, logType, redactedURL, response.status, 'Success');
    if (options.responseType === 'byteswithdetails') {
        (0, assert_1.assert)(result instanceof Uint8Array, 'Expected Uint8Array result');
        const fullResult = {
            data: result,
            contentType: getContentType(response),
            response,
        };
        return fullResult;
    }
    if (options.responseType === 'jsonwithdetails') {
        const fullResult = {
            data: result,
            contentType: getContentType(response),
            response,
        };
        return fullResult;
    }
    return result;
}
async function _retryAjax(url, options, providedLimit, providedCount) {
    const count = (providedCount || 0) + 1;
    const limit = providedLimit || 3;
    try {
        return await _promiseAjax(url, options);
    }
    catch (e) {
        if (e instanceof Errors_1.HTTPError && e.code === -1 && count < limit) {
            return new Promise(resolve => {
                setTimeout(() => {
                    resolve(_retryAjax(url, options, limit, count));
                }, 1000);
            });
        }
        throw e;
    }
}
async function _outerAjax(url, options) {
    options.stack = new Error().stack; // just in case, save stack here.
    return _retryAjax(url, options);
}
function makeHTTPError(message, providedCode, headers, response, stack) {
    return new Errors_1.HTTPError(message, {
        code: providedCode,
        headers,
        response,
        stack,
    });
}
const URL_CALLS = {
    accounts: 'v1/accounts',
    attachmentId: 'v2/attachments/form/upload',
    attestation: 'v1/attestation',
    challenge: 'v1/challenge',
    config: 'v1/config',
    deliveryCert: 'v1/certificate/delivery',
    devices: 'v1/devices',
    directoryAuth: 'v1/directory/auth',
    directoryAuthV2: 'v2/directory/auth',
    discovery: 'v1/discovery',
    getGroupAvatarUpload: 'v1/groups/avatar/form',
    getGroupCredentials: 'v1/certificate/group',
    getIceServers: 'v1/accounts/turn',
    getStickerPackUpload: 'v1/sticker/pack/form',
    groupLog: 'v1/groups/logs',
    groups: 'v1/groups',
    groupsViaLink: 'v1/groups/join',
    groupToken: 'v1/groups/token',
    keys: 'v2/keys',
    messages: 'v1/messages',
    multiRecipient: 'v1/messages/multi_recipient',
    profile: 'v1/profile',
    registerCapabilities: 'v1/devices/capabilities',
    reportMessage: 'v1/messages/report',
    signed: 'v2/keys/signed',
    storageManifest: 'v1/storage/manifest',
    storageModify: 'v1/storage/',
    storageRead: 'v1/storage/read',
    storageToken: 'v1/storage/auth',
    supportUnauthenticatedDelivery: 'v1/devices/unauthenticated_delivery',
    updateDeviceName: 'v1/accounts/name',
    username: 'v1/accounts/username',
    whoami: 'v1/accounts/whoami',
};
const WEBSOCKET_CALLS = new Set([
    // MessageController
    'messages',
    'multiRecipient',
    'reportMessage',
    // ProfileController
    'profile',
    // AttachmentControllerV2
    'attachmentId',
    // RemoteConfigController
    'config',
    // Certificate
    'deliveryCert',
    'getGroupCredentials',
    // Devices
    'devices',
    'registerCapabilities',
    'supportUnauthenticatedDelivery',
    // Directory
    'directoryAuth',
    'directoryAuthV2',
    // Storage
    'storageToken',
]);
const uploadAvatarHeadersZod = zod_1.z
    .object({
    acl: zod_1.z.string(),
    algorithm: zod_1.z.string(),
    credential: zod_1.z.string(),
    date: zod_1.z.string(),
    key: zod_1.z.string(),
    policy: zod_1.z.string(),
    signature: zod_1.z.string(),
})
    .passthrough();
// We first set up the data that won't change during this session of the app
function initialize({ url, storageUrl, updatesUrl, directoryEnclaveId, directoryTrustAnchor, directoryUrl, directoryV2Url, directoryV2PublicKey, directoryV2CodeHash, cdnUrlObject, certificateAuthority, contentProxyUrl, proxyUrl, version, }) {
    if (!is_1.default.string(url)) {
        throw new Error('WebAPI.initialize: Invalid server url');
    }
    if (!is_1.default.string(storageUrl)) {
        throw new Error('WebAPI.initialize: Invalid storageUrl');
    }
    if (!is_1.default.string(updatesUrl)) {
        throw new Error('WebAPI.initialize: Invalid updatesUrl');
    }
    if (!is_1.default.string(directoryEnclaveId)) {
        throw new Error('WebAPI.initialize: Invalid directory enclave id');
    }
    if (!is_1.default.string(directoryTrustAnchor)) {
        throw new Error('WebAPI.initialize: Invalid directory enclave id');
    }
    if (!is_1.default.string(directoryUrl)) {
        throw new Error('WebAPI.initialize: Invalid directory url');
    }
    if (!is_1.default.string(directoryV2Url)) {
        throw new Error('WebAPI.initialize: Invalid directory V2 url');
    }
    if (!is_1.default.string(directoryV2PublicKey)) {
        throw new Error('WebAPI.initialize: Invalid directory V2 public key');
    }
    if (!is_1.default.string(directoryV2CodeHash)) {
        throw new Error('WebAPI.initialize: Invalid directory V2 code hash');
    }
    if (!is_1.default.object(cdnUrlObject)) {
        throw new Error('WebAPI.initialize: Invalid cdnUrlObject');
    }
    if (!is_1.default.string(cdnUrlObject['0'])) {
        throw new Error('WebAPI.initialize: Missing CDN 0 configuration');
    }
    if (!is_1.default.string(cdnUrlObject['2'])) {
        throw new Error('WebAPI.initialize: Missing CDN 2 configuration');
    }
    if (!is_1.default.string(certificateAuthority)) {
        throw new Error('WebAPI.initialize: Invalid certificateAuthority');
    }
    if (!is_1.default.string(contentProxyUrl)) {
        throw new Error('WebAPI.initialize: Invalid contentProxyUrl');
    }
    if (proxyUrl && !is_1.default.string(proxyUrl)) {
        throw new Error('WebAPI.initialize: Invalid proxyUrl');
    }
    if (!is_1.default.string(version)) {
        throw new Error('WebAPI.initialize: Invalid version');
    }
    // Thanks to function-hoisting, we can put this return statement before all of the
    //   below function definitions.
    return {
        connect,
    };
    // Then we connect to the server with user-specific information. This is the only API
    //   exposed to the browser context, ensuring that it can't connect to arbitrary
    //   locations.
    function connect({ username: initialUsername, password: initialPassword, useWebSocket = true, }) {
        let username = initialUsername;
        let password = initialPassword;
        const PARSE_RANGE_HEADER = /\/(\d+)$/;
        const PARSE_GROUP_LOG_RANGE_HEADER = /$versions (\d{1,10})-(\d{1,10})\/(d{1,10})/;
        const socketManager = new SocketManager_1.SocketManager({
            url,
            certificateAuthority,
            version,
            proxyUrl,
        });
        socketManager.on('statusChange', () => {
            window.Whisper.events.trigger('socketStatusChange');
        });
        socketManager.on('authError', () => {
            window.Whisper.events.trigger('unlinkAndDisconnect');
        });
        if (useWebSocket) {
            socketManager.authenticate({ username, password });
        }
        const cdsSocketManager = new CDSSocketManager_1.CDSSocketManager({
            url: directoryV2Url,
            publicKey: directoryV2PublicKey,
            codeHash: directoryV2CodeHash,
            certificateAuthority,
            version,
            proxyUrl,
        });
        let fetchForLinkPreviews;
        if (proxyUrl) {
            const agent = new proxy_agent_1.default(proxyUrl);
            fetchForLinkPreviews = (href, init) => (0, node_fetch_1.default)(href, Object.assign(Object.assign({}, init), { agent }));
        }
        else {
            fetchForLinkPreviews = node_fetch_1.default;
        }
        // Thanks, function hoisting!
        return {
            getSocketStatus,
            checkSockets,
            onOnline,
            onOffline,
            registerRequestHandler,
            unregisterRequestHandler,
            authenticate,
            logout,
            confirmCode,
            createGroup,
            deleteUsername,
            fetchLinkPreviewImage,
            fetchLinkPreviewMetadata,
            getAttachment,
            getAvatar,
            getConfig,
            getDevices,
            getGroup,
            getGroupAvatar,
            getGroupCredentials,
            getGroupExternalCredential,
            getGroupFromLink,
            getGroupLog,
            getIceServers,
            getKeysForIdentifier,
            getKeysForIdentifierUnauth,
            getMyKeys,
            getProfile,
            getProfileForUsername,
            getProfileUnauth,
            getBadgeImageFile,
            getProvisioningResource,
            getSenderCertificate,
            getSticker,
            getStickerPackManifest,
            getStorageCredentials,
            getStorageManifest,
            getStorageRecords,
            getUuidsForE164s,
            getUuidsForE164sV2,
            makeProxiedRequest,
            makeSfuRequest,
            modifyGroup,
            modifyStorageRecords,
            putAttachment,
            putProfile,
            putStickers,
            putUsername,
            registerCapabilities,
            registerKeys,
            registerSupportForUnauthenticatedDelivery,
            reportMessage,
            requestVerificationSMS,
            requestVerificationVoice,
            sendMessages,
            sendMessagesUnauth,
            sendWithSenderKey,
            setSignedPreKey,
            updateDeviceName,
            uploadAvatar,
            uploadGroupAvatar,
            whoami,
            sendChallengeResponse,
        };
        async function _ajax(param) {
            if (!param.urlParameters) {
                param.urlParameters = '';
            }
            const useWebSocketForEndpoint = useWebSocket && WEBSOCKET_CALLS.has(param.call);
            const outerParams = {
                socketManager: useWebSocketForEndpoint ? socketManager : undefined,
                basicAuth: param.basicAuth,
                certificateAuthority,
                contentType: param.contentType || 'application/json; charset=utf-8',
                data: param.data ||
                    (param.jsonData ? JSON.stringify(param.jsonData) : undefined),
                headers: param.headers,
                host: param.host || url,
                password: param.password || password,
                path: URL_CALLS[param.call] + param.urlParameters,
                proxyUrl,
                responseType: param.responseType,
                timeout: param.timeout,
                type: param.httpType,
                user: param.username || username,
                redactUrl: param.redactUrl,
                serverUrl: url,
                validateResponse: param.validateResponse,
                version,
                unauthenticated: param.unauthenticated,
                accessKey: param.accessKey,
            };
            try {
                return await _outerAjax(null, outerParams);
            }
            catch (e) {
                if (!(e instanceof Errors_1.HTTPError)) {
                    throw e;
                }
                const translatedError = (0, Utils_1.translateError)(e);
                if (translatedError) {
                    throw translatedError;
                }
                throw e;
            }
        }
        async function whoami() {
            return (await _ajax({
                call: 'whoami',
                httpType: 'GET',
                responseType: 'json',
            }));
        }
        async function sendChallengeResponse(challengeResponse) {
            await _ajax({
                call: 'challenge',
                httpType: 'PUT',
                jsonData: challengeResponse,
            });
        }
        async function authenticate({ username: newUsername, password: newPassword, }) {
            username = newUsername;
            password = newPassword;
            if (useWebSocket) {
                await socketManager.authenticate({ username, password });
            }
        }
        async function logout() {
            username = '';
            password = '';
            if (useWebSocket) {
                await socketManager.logout();
            }
        }
        function getSocketStatus() {
            return socketManager.getStatus();
        }
        function checkSockets() {
            // Intentionally not awaiting
            socketManager.check();
        }
        async function onOnline() {
            await socketManager.onOnline();
        }
        async function onOffline() {
            await socketManager.onOffline();
        }
        function registerRequestHandler(handler) {
            socketManager.registerRequestHandler(handler);
        }
        function unregisterRequestHandler(handler) {
            socketManager.unregisterRequestHandler(handler);
        }
        async function getConfig() {
            const res = (await _ajax({
                call: 'config',
                httpType: 'GET',
                responseType: 'json',
            }));
            return res.config.filter(({ name }) => name.startsWith('desktop.') || name.startsWith('global.'));
        }
        async function getSenderCertificate(omitE164) {
            return (await _ajax(Object.assign({ call: 'deliveryCert', httpType: 'GET', responseType: 'json', validateResponse: { certificate: 'string' } }, (omitE164 ? { urlParameters: '?includeE164=false' } : {}))));
        }
        async function getStorageCredentials() {
            return (await _ajax({
                call: 'storageToken',
                httpType: 'GET',
                responseType: 'json',
                schema: { username: 'string', password: 'string' },
            }));
        }
        async function getStorageManifest(options = {}) {
            const { credentials, greaterThanVersion } = options;
            return _ajax(Object.assign({ call: 'storageManifest', contentType: 'application/x-protobuf', host: storageUrl, httpType: 'GET', responseType: 'bytes', urlParameters: greaterThanVersion
                    ? `/version/${greaterThanVersion}`
                    : '' }, credentials));
        }
        async function getStorageRecords(data, options = {}) {
            const { credentials } = options;
            return _ajax(Object.assign({ call: 'storageRead', contentType: 'application/x-protobuf', data, host: storageUrl, httpType: 'PUT', responseType: 'bytes' }, credentials));
        }
        async function modifyStorageRecords(data, options = {}) {
            const { credentials } = options;
            return _ajax(Object.assign({ call: 'storageModify', contentType: 'application/x-protobuf', data, host: storageUrl, httpType: 'PUT', 
                // If we run into a conflict, the current manifest is returned -
                //   it will will be an Uint8Array at the response key on the Error
                responseType: 'bytes' }, credentials));
        }
        async function registerSupportForUnauthenticatedDelivery() {
            await _ajax({
                call: 'supportUnauthenticatedDelivery',
                httpType: 'PUT',
                responseType: 'json',
            });
        }
        async function registerCapabilities(capabilities) {
            await _ajax({
                call: 'registerCapabilities',
                httpType: 'PUT',
                jsonData: capabilities,
            });
        }
        function getProfileUrl(identifier, profileKeyVersion, profileKeyCredentialRequest) {
            let profileUrl = `/${identifier}`;
            if (profileKeyVersion) {
                profileUrl += `/${profileKeyVersion}`;
            }
            if (profileKeyVersion && profileKeyCredentialRequest) {
                profileUrl += `/${profileKeyCredentialRequest}`;
            }
            return profileUrl;
        }
        async function getProfile(identifier, options) {
            const { profileKeyVersion, profileKeyCredentialRequest, userLanguages } = options;
            return (await _ajax({
                call: 'profile',
                httpType: 'GET',
                urlParameters: getProfileUrl(identifier, profileKeyVersion, profileKeyCredentialRequest),
                headers: {
                    'Accept-Language': (0, userLanguages_1.formatAcceptLanguageHeader)(userLanguages),
                },
                responseType: 'json',
                redactUrl: _createRedactor(identifier, profileKeyVersion, profileKeyCredentialRequest),
            }));
        }
        async function getProfileForUsername(usernameToFetch) {
            return getProfile(`username/${usernameToFetch}`, {
                userLanguages: [],
            });
        }
        async function putProfile(jsonData) {
            const res = await _ajax({
                call: 'profile',
                httpType: 'PUT',
                responseType: 'json',
                jsonData,
            });
            if (!res) {
                return;
            }
            return uploadAvatarHeadersZod.parse(res);
        }
        async function getProfileUnauth(identifier, options) {
            const { accessKey, profileKeyVersion, profileKeyCredentialRequest, userLanguages, } = options;
            return (await _ajax({
                call: 'profile',
                httpType: 'GET',
                urlParameters: getProfileUrl(identifier, profileKeyVersion, profileKeyCredentialRequest),
                headers: {
                    'Accept-Language': (0, userLanguages_1.formatAcceptLanguageHeader)(userLanguages),
                },
                responseType: 'json',
                unauthenticated: true,
                accessKey,
                redactUrl: _createRedactor(identifier, profileKeyVersion, profileKeyCredentialRequest),
            }));
        }
        async function getBadgeImageFile(imageFileUrl) {
            (0, assert_1.strictAssert)((0, isBadgeImageFileUrlValid_1.isBadgeImageFileUrlValid)(imageFileUrl, updatesUrl), 'getBadgeImageFile got an invalid URL. Was bad data saved?');
            return _outerAjax(imageFileUrl, {
                certificateAuthority,
                contentType: 'application/octet-stream',
                proxyUrl,
                responseType: 'bytes',
                timeout: 0,
                type: 'GET',
                redactUrl: (href) => {
                    const parsedUrl = (0, url_1.maybeParseUrl)(href);
                    if (!parsedUrl) {
                        return href;
                    }
                    const { pathname } = parsedUrl;
                    const pattern = RegExp((0, lodash_1.escapeRegExp)(pathname), 'g');
                    return href.replace(pattern, `[REDACTED]${pathname.slice(-3)}`);
                },
                version,
            });
        }
        async function getAvatar(path) {
            // Using _outerAJAX, since it's not hardcoded to the Signal Server. Unlike our
            //   attachment CDN, it uses our self-signed certificate, so we pass it in.
            return _outerAjax(`${cdnUrlObject['0']}/${path}`, {
                certificateAuthority,
                contentType: 'application/octet-stream',
                proxyUrl,
                responseType: 'bytes',
                timeout: 0,
                type: 'GET',
                redactUrl: (href) => {
                    const pattern = RegExp((0, lodash_1.escapeRegExp)(path), 'g');
                    return href.replace(pattern, `[REDACTED]${path.slice(-3)}`);
                },
                version,
            });
        }
        async function deleteUsername() {
            await _ajax({
                call: 'username',
                httpType: 'DELETE',
            });
        }
        async function putUsername(newUsername) {
            await _ajax({
                call: 'username',
                httpType: 'PUT',
                urlParameters: `/${newUsername}`,
            });
        }
        async function reportMessage(senderE164, serverGuid) {
            await _ajax({
                call: 'reportMessage',
                httpType: 'POST',
                urlParameters: `/${senderE164}/${serverGuid}`,
                responseType: 'bytes',
            });
        }
        async function requestVerificationSMS(number) {
            await _ajax({
                call: 'accounts',
                httpType: 'GET',
                urlParameters: `/sms/code/${number}`,
            });
        }
        async function requestVerificationVoice(number) {
            await _ajax({
                call: 'accounts',
                httpType: 'GET',
                urlParameters: `/voice/code/${number}`,
            });
        }
        async function confirmCode(number, code, newPassword, registrationId, deviceName, options = {}) {
            const capabilities = {
                announcementGroup: true,
                'gv2-3': true,
                'gv1-migration': true,
                senderKey: true,
                changeNumber: true,
            };
            const { accessKey, uuid } = options;
            const jsonData = {
                capabilities,
                fetchesMessages: true,
                name: deviceName || undefined,
                registrationId,
                supportsSms: false,
                unidentifiedAccessKey: accessKey
                    ? Bytes.toBase64(accessKey)
                    : undefined,
                unrestrictedUnidentifiedAccess: false,
            };
            const call = deviceName ? 'devices' : 'accounts';
            const urlPrefix = deviceName ? '/' : '/code/';
            // Reset old websocket credentials and disconnect.
            // AccountManager is our only caller and it will trigger
            // `registration_done` which will update credentials.
            await logout();
            // Update REST credentials, though. We need them for the call below
            username = number;
            password = newPassword;
            const response = (await _ajax({
                call,
                httpType: 'PUT',
                responseType: 'json',
                urlParameters: urlPrefix + code,
                jsonData,
            }));
            // Set final REST credentials to let `registerKeys` succeed.
            username = `${uuid || response.uuid || number}.${response.deviceId || 1}`;
            password = newPassword;
            return response;
        }
        async function updateDeviceName(deviceName) {
            await _ajax({
                call: 'updateDeviceName',
                httpType: 'PUT',
                jsonData: {
                    deviceName,
                },
            });
        }
        async function getIceServers() {
            return (await _ajax({
                call: 'getIceServers',
                httpType: 'GET',
                responseType: 'json',
            }));
        }
        async function getDevices() {
            return (await _ajax({
                call: 'devices',
                httpType: 'GET',
                responseType: 'json',
            }));
        }
        async function registerKeys(genKeys) {
            const preKeys = genKeys.preKeys.map(key => ({
                keyId: key.keyId,
                publicKey: Bytes.toBase64(key.publicKey),
            }));
            const keys = {
                identityKey: Bytes.toBase64(genKeys.identityKey),
                signedPreKey: {
                    keyId: genKeys.signedPreKey.keyId,
                    publicKey: Bytes.toBase64(genKeys.signedPreKey.publicKey),
                    signature: Bytes.toBase64(genKeys.signedPreKey.signature),
                },
                preKeys,
            };
            await _ajax({
                call: 'keys',
                httpType: 'PUT',
                jsonData: keys,
            });
        }
        async function setSignedPreKey(signedPreKey) {
            await _ajax({
                call: 'signed',
                httpType: 'PUT',
                jsonData: {
                    keyId: signedPreKey.keyId,
                    publicKey: Bytes.toBase64(signedPreKey.publicKey),
                    signature: Bytes.toBase64(signedPreKey.signature),
                },
            });
        }
        async function getMyKeys() {
            const result = (await _ajax({
                call: 'keys',
                httpType: 'GET',
                responseType: 'json',
                validateResponse: { count: 'number' },
            }));
            return result.count;
        }
        function handleKeys(res) {
            if (!Array.isArray(res.devices)) {
                throw new Error('Invalid response');
            }
            const devices = res.devices.map(device => {
                if (!_validateResponse(device, { signedPreKey: 'object' }) ||
                    !_validateResponse(device.signedPreKey, {
                        publicKey: 'string',
                        signature: 'string',
                    })) {
                    throw new Error('Invalid signedPreKey');
                }
                let preKey;
                if (device.preKey) {
                    if (!_validateResponse(device, { preKey: 'object' }) ||
                        !_validateResponse(device.preKey, { publicKey: 'string' })) {
                        throw new Error('Invalid preKey');
                    }
                    preKey = {
                        keyId: device.preKey.keyId,
                        publicKey: Bytes.fromBase64(device.preKey.publicKey),
                    };
                }
                return {
                    deviceId: device.deviceId,
                    registrationId: device.registrationId,
                    preKey,
                    signedPreKey: {
                        keyId: device.signedPreKey.keyId,
                        publicKey: Bytes.fromBase64(device.signedPreKey.publicKey),
                        signature: Bytes.fromBase64(device.signedPreKey.signature),
                    },
                };
            });
            return {
                devices,
                identityKey: Bytes.fromBase64(res.identityKey),
            };
        }
        async function getKeysForIdentifier(identifier, deviceId) {
            const keys = (await _ajax({
                call: 'keys',
                httpType: 'GET',
                urlParameters: `/${identifier}/${deviceId || '*'}`,
                responseType: 'json',
                validateResponse: { identityKey: 'string', devices: 'object' },
            }));
            return handleKeys(keys);
        }
        async function getKeysForIdentifierUnauth(identifier, deviceId, { accessKey } = {}) {
            const keys = (await _ajax({
                call: 'keys',
                httpType: 'GET',
                urlParameters: `/${identifier}/${deviceId || '*'}`,
                responseType: 'json',
                validateResponse: { identityKey: 'string', devices: 'object' },
                unauthenticated: true,
                accessKey,
            }));
            return handleKeys(keys);
        }
        async function sendMessagesUnauth(destination, messages, timestamp, online, { accessKey } = {}) {
            let jsonData;
            if (online) {
                jsonData = { messages, timestamp, online: true };
            }
            else {
                jsonData = { messages, timestamp };
            }
            await _ajax({
                call: 'messages',
                httpType: 'PUT',
                urlParameters: `/${destination}`,
                jsonData,
                responseType: 'json',
                unauthenticated: true,
                accessKey,
            });
        }
        async function sendMessages(destination, messages, timestamp, online) {
            let jsonData;
            if (online) {
                jsonData = { messages, timestamp, online: true };
            }
            else {
                jsonData = { messages, timestamp };
            }
            await _ajax({
                call: 'messages',
                httpType: 'PUT',
                urlParameters: `/${destination}`,
                jsonData,
                responseType: 'json',
            });
        }
        async function sendWithSenderKey(data, accessKeys, timestamp, online) {
            const response = await _ajax({
                call: 'multiRecipient',
                httpType: 'PUT',
                contentType: 'application/vnd.signal-messenger.mrm',
                data,
                urlParameters: `?ts=${timestamp}&online=${online ? 'true' : 'false'}`,
                responseType: 'json',
                unauthenticated: true,
                accessKey: Bytes.toBase64(accessKeys),
            });
            const parseResult = exports.multiRecipient200ResponseSchema.safeParse(response);
            if (parseResult.success) {
                return parseResult.data;
            }
            log.warn('WebAPI: invalid response from sendWithSenderKey', (0, errors_1.toLogFormat)(parseResult.error));
            return response;
        }
        function redactStickerUrl(stickerUrl) {
            return stickerUrl.replace(/(\/stickers\/)([^/]+)(\/)/, (_, begin, packId, end) => `${begin}${(0, Stickers_1.redactPackId)(packId)}${end}`);
        }
        async function getSticker(packId, stickerId) {
            if (!(0, Stickers_1.isPackIdValid)(packId)) {
                throw new Error('getSticker: pack ID was invalid');
            }
            return _outerAjax(`${cdnUrlObject['0']}/stickers/${packId}/full/${stickerId}`, {
                certificateAuthority,
                proxyUrl,
                responseType: 'bytes',
                type: 'GET',
                redactUrl: redactStickerUrl,
                version,
            });
        }
        async function getStickerPackManifest(packId) {
            if (!(0, Stickers_1.isPackIdValid)(packId)) {
                throw new Error('getStickerPackManifest: pack ID was invalid');
            }
            return _outerAjax(`${cdnUrlObject['0']}/stickers/${packId}/manifest.proto`, {
                certificateAuthority,
                proxyUrl,
                responseType: 'bytes',
                type: 'GET',
                redactUrl: redactStickerUrl,
                version,
            });
        }
        function makePutParams({ key, credential, acl, algorithm, date, policy, signature, }, encryptedBin) {
            // Note: when using the boundary string in the POST body, it needs to be prefixed by
            //   an extra --, and the final boundary string at the end gets a -- prefix and a --
            //   suffix.
            const boundaryString = `----------------${(0, uuid_1.v4)().replace(/-/g, '')}`;
            const CRLF = '\r\n';
            const getSection = (name, value) => [
                `--${boundaryString}`,
                `Content-Disposition: form-data; name="${name}"${CRLF}`,
                value,
            ].join(CRLF);
            const start = [
                getSection('key', key),
                getSection('x-amz-credential', credential),
                getSection('acl', acl),
                getSection('x-amz-algorithm', algorithm),
                getSection('x-amz-date', date),
                getSection('policy', policy),
                getSection('x-amz-signature', signature),
                getSection('Content-Type', 'application/octet-stream'),
                `--${boundaryString}`,
                'Content-Disposition: form-data; name="file"',
                `Content-Type: application/octet-stream${CRLF}${CRLF}`,
            ].join(CRLF);
            const end = `${CRLF}--${boundaryString}--${CRLF}`;
            const startBuffer = Buffer.from(start, 'utf8');
            const attachmentBuffer = Buffer.from(encryptedBin);
            const endBuffer = Buffer.from(end, 'utf8');
            const contentLength = startBuffer.length + attachmentBuffer.length + endBuffer.length;
            const data = Buffer.concat([startBuffer, attachmentBuffer, endBuffer], contentLength);
            return {
                data,
                contentType: `multipart/form-data; boundary=${boundaryString}`,
                headers: {
                    'Content-Length': contentLength.toString(),
                },
            };
        }
        async function putStickers(encryptedManifest, encryptedStickers, onProgress) {
            // Get manifest and sticker upload parameters
            const { packId, manifest, stickers } = (await _ajax({
                call: 'getStickerPackUpload',
                responseType: 'json',
                httpType: 'GET',
                urlParameters: `/${encryptedStickers.length}`,
            }));
            // Upload manifest
            const manifestParams = makePutParams(manifest, encryptedManifest);
            // This is going to the CDN, not the service, so we use _outerAjax
            await _outerAjax(`${cdnUrlObject['0']}/`, Object.assign(Object.assign({}, manifestParams), { certificateAuthority,
                proxyUrl, timeout: 0, type: 'POST', version }));
            // Upload stickers
            const queue = new p_queue_1.default({ concurrency: 3, timeout: 1000 * 60 * 2 });
            await Promise.all(stickers.map(async (sticker, index) => {
                const stickerParams = makePutParams(sticker, encryptedStickers[index]);
                await queue.add(async () => _outerAjax(`${cdnUrlObject['0']}/`, Object.assign(Object.assign({}, stickerParams), { certificateAuthority,
                    proxyUrl, timeout: 0, type: 'POST', version })));
                if (onProgress) {
                    onProgress();
                }
            }));
            // Done!
            return packId;
        }
        async function getAttachment(cdnKey, cdnNumber) {
            const abortController = new abort_controller_1.default();
            const cdnUrl = (0, lodash_1.isNumber)(cdnNumber)
                ? cdnUrlObject[cdnNumber] || cdnUrlObject['0']
                : cdnUrlObject['0'];
            // This is going to the CDN, not the service, so we use _outerAjax
            const stream = await _outerAjax(`${cdnUrl}/attachments/${cdnKey}`, {
                certificateAuthority,
                proxyUrl,
                responseType: 'stream',
                timeout: 0,
                type: 'GET',
                redactUrl: _createRedactor(cdnKey),
                version,
                abortSignal: abortController.signal,
            });
            return (0, getStreamWithTimeout_1.getStreamWithTimeout)(stream, {
                name: `getAttachment(${cdnKey})`,
                timeout: GET_ATTACHMENT_CHUNK_TIMEOUT,
                abortController,
            });
        }
        async function putAttachment(encryptedBin) {
            const response = (await _ajax({
                call: 'attachmentId',
                httpType: 'GET',
                responseType: 'json',
            }));
            const { attachmentIdString } = response;
            const params = makePutParams(response, encryptedBin);
            // This is going to the CDN, not the service, so we use _outerAjax
            await _outerAjax(`${cdnUrlObject['0']}/attachments/`, Object.assign(Object.assign({}, params), { certificateAuthority,
                proxyUrl, timeout: 0, type: 'POST', version }));
            return attachmentIdString;
        }
        function getHeaderPadding() {
            const max = (0, Crypto_1.getRandomValue)(1, 64);
            let characters = '';
            for (let i = 0; i < max; i += 1) {
                characters += String.fromCharCode((0, Crypto_1.getRandomValue)(65, 122));
            }
            return characters;
        }
        async function fetchLinkPreviewMetadata(href, abortSignal) {
            return linkPreviewFetch.fetchLinkPreviewMetadata(fetchForLinkPreviews, href, abortSignal);
        }
        async function fetchLinkPreviewImage(href, abortSignal) {
            return linkPreviewFetch.fetchLinkPreviewImage(fetchForLinkPreviews, href, abortSignal);
        }
        async function makeProxiedRequest(targetUrl, options = {}) {
            const { returnUint8Array, start, end } = options;
            const headers = {
                'X-SignalPadding': getHeaderPadding(),
            };
            if (is_1.default.number(start) && is_1.default.number(end)) {
                headers.Range = `bytes=${start}-${end}`;
            }
            const result = await _outerAjax(targetUrl, {
                responseType: returnUint8Array ? 'byteswithdetails' : undefined,
                proxyUrl: contentProxyUrl,
                type: 'GET',
                redirect: 'follow',
                redactUrl: () => '[REDACTED_URL]',
                headers,
                version,
            });
            if (!returnUint8Array) {
                return result;
            }
            const { response } = result;
            if (!response.headers || !response.headers.get) {
                throw new Error('makeProxiedRequest: Problem retrieving header value');
            }
            const range = response.headers.get('content-range');
            const match = PARSE_RANGE_HEADER.exec(range || '');
            if (!match || !match[1]) {
                throw new Error(`makeProxiedRequest: Unable to parse total size from ${range}`);
            }
            const totalSize = parseInt(match[1], 10);
            return {
                totalSize,
                result: result,
            };
        }
        async function makeSfuRequest(targetUrl, type, headers, body) {
            return _outerAjax(targetUrl, {
                certificateAuthority,
                data: body,
                headers,
                proxyUrl,
                responseType: 'byteswithdetails',
                timeout: 0,
                type,
                version,
            });
        }
        // Groups
        function generateGroupAuth(groupPublicParamsHex, authCredentialPresentationHex) {
            return Bytes.toBase64(Bytes.fromString(`${groupPublicParamsHex}:${authCredentialPresentationHex}`));
        }
        async function getGroupCredentials(startDay, endDay) {
            const response = (await _ajax({
                call: 'getGroupCredentials',
                urlParameters: `/${startDay}/${endDay}`,
                httpType: 'GET',
                responseType: 'json',
            }));
            return response.credentials;
        }
        async function getGroupExternalCredential(options) {
            const basicAuth = generateGroupAuth(options.groupPublicParamsHex, options.authCredentialPresentationHex);
            const response = await _ajax({
                basicAuth,
                call: 'groupToken',
                httpType: 'GET',
                contentType: 'application/x-protobuf',
                responseType: 'bytes',
                host: storageUrl,
            });
            return protobuf_1.SignalService.GroupExternalCredential.decode(response);
        }
        function verifyAttributes(attributes) {
            const { key, credential, acl, algorithm, date, policy, signature } = attributes;
            if (!key ||
                !credential ||
                !acl ||
                !algorithm ||
                !date ||
                !policy ||
                !signature) {
                throw new Error('verifyAttributes: Missing value from AvatarUploadAttributes');
            }
            return {
                key,
                credential,
                acl,
                algorithm,
                date,
                policy,
                signature,
            };
        }
        async function uploadAvatar(uploadAvatarRequestHeaders, avatarData) {
            const verified = verifyAttributes(uploadAvatarRequestHeaders);
            const { key } = verified;
            const manifestParams = makePutParams(verified, avatarData);
            await _outerAjax(`${cdnUrlObject['0']}/`, Object.assign(Object.assign({}, manifestParams), { certificateAuthority,
                proxyUrl, timeout: 0, type: 'POST', version }));
            return key;
        }
        async function uploadGroupAvatar(avatarData, options) {
            const basicAuth = generateGroupAuth(options.groupPublicParamsHex, options.authCredentialPresentationHex);
            const response = await _ajax({
                basicAuth,
                call: 'getGroupAvatarUpload',
                httpType: 'GET',
                responseType: 'bytes',
                host: storageUrl,
            });
            const attributes = protobuf_1.SignalService.AvatarUploadAttributes.decode(response);
            const verified = verifyAttributes(attributes);
            const { key } = verified;
            const manifestParams = makePutParams(verified, avatarData);
            await _outerAjax(`${cdnUrlObject['0']}/`, Object.assign(Object.assign({}, manifestParams), { certificateAuthority,
                proxyUrl, timeout: 0, type: 'POST', version }));
            return key;
        }
        async function getGroupAvatar(key) {
            return _outerAjax(`${cdnUrlObject['0']}/${key}`, {
                certificateAuthority,
                proxyUrl,
                responseType: 'bytes',
                timeout: 0,
                type: 'GET',
                version,
                redactUrl: _createRedactor(key),
            });
        }
        async function createGroup(group, options) {
            const basicAuth = generateGroupAuth(options.groupPublicParamsHex, options.authCredentialPresentationHex);
            const data = protobuf_1.SignalService.Group.encode(group).finish();
            await _ajax({
                basicAuth,
                call: 'groups',
                contentType: 'application/x-protobuf',
                data,
                host: storageUrl,
                httpType: 'PUT',
            });
        }
        async function getGroup(options) {
            const basicAuth = generateGroupAuth(options.groupPublicParamsHex, options.authCredentialPresentationHex);
            const response = await _ajax({
                basicAuth,
                call: 'groups',
                contentType: 'application/x-protobuf',
                host: storageUrl,
                httpType: 'GET',
                responseType: 'bytes',
            });
            return protobuf_1.SignalService.Group.decode(response);
        }
        async function getGroupFromLink(inviteLinkPassword, auth) {
            const basicAuth = generateGroupAuth(auth.groupPublicParamsHex, auth.authCredentialPresentationHex);
            const safeInviteLinkPassword = (0, webSafeBase64_1.toWebSafeBase64)(inviteLinkPassword);
            const response = await _ajax({
                basicAuth,
                call: 'groupsViaLink',
                contentType: 'application/x-protobuf',
                host: storageUrl,
                httpType: 'GET',
                responseType: 'bytes',
                urlParameters: `/${safeInviteLinkPassword}`,
                redactUrl: _createRedactor(safeInviteLinkPassword),
            });
            return protobuf_1.SignalService.GroupJoinInfo.decode(response);
        }
        async function modifyGroup(changes, options, inviteLinkBase64) {
            const basicAuth = generateGroupAuth(options.groupPublicParamsHex, options.authCredentialPresentationHex);
            const data = protobuf_1.SignalService.GroupChange.Actions.encode(changes).finish();
            const safeInviteLinkPassword = inviteLinkBase64
                ? (0, webSafeBase64_1.toWebSafeBase64)(inviteLinkBase64)
                : undefined;
            const response = await _ajax({
                basicAuth,
                call: 'groups',
                contentType: 'application/x-protobuf',
                data,
                host: storageUrl,
                httpType: 'PATCH',
                responseType: 'bytes',
                urlParameters: safeInviteLinkPassword
                    ? `?inviteLinkPassword=${safeInviteLinkPassword}`
                    : undefined,
                redactUrl: safeInviteLinkPassword
                    ? _createRedactor(safeInviteLinkPassword)
                    : undefined,
            });
            return protobuf_1.SignalService.GroupChange.decode(response);
        }
        async function getGroupLog(startVersion, options) {
            const basicAuth = generateGroupAuth(options.groupPublicParamsHex, options.authCredentialPresentationHex);
            const withDetails = await _ajax({
                basicAuth,
                call: 'groupLog',
                contentType: 'application/x-protobuf',
                host: storageUrl,
                httpType: 'GET',
                responseType: 'byteswithdetails',
                urlParameters: `/${startVersion}`,
            });
            const { data, response } = withDetails;
            const changes = protobuf_1.SignalService.GroupChanges.decode(data);
            if (response && response.status === 206) {
                const range = response.headers.get('Content-Range');
                const match = PARSE_GROUP_LOG_RANGE_HEADER.exec(range || '');
                const start = match ? parseInt(match[0], 10) : undefined;
                const end = match ? parseInt(match[1], 10) : undefined;
                const currentRevision = match ? parseInt(match[2], 10) : undefined;
                if (match &&
                    is_1.default.number(start) &&
                    is_1.default.number(end) &&
                    is_1.default.number(currentRevision)) {
                    return {
                        changes,
                        start,
                        end,
                        currentRevision,
                    };
                }
            }
            return {
                changes,
            };
        }
        function getProvisioningResource(handler) {
            return socketManager.getProvisioningResource(handler);
        }
        async function getDirectoryAuth() {
            return (await _ajax({
                call: 'directoryAuth',
                httpType: 'GET',
                responseType: 'json',
            }));
        }
        async function getDirectoryAuthV2() {
            return (await _ajax({
                call: 'directoryAuthV2',
                httpType: 'GET',
                responseType: 'json',
            }));
        }
        function validateAttestationQuote({ serverStaticPublic, quote: quoteBytes, }) {
            const SGX_CONSTANTS = getSgxConstants();
            const quote = Buffer.from(quoteBytes);
            const quoteVersion = quote.readInt16LE(0) & 0xffff;
            if (quoteVersion < 0 || quoteVersion > 2) {
                throw new Error(`Unknown version ${quoteVersion}`);
            }
            const miscSelect = quote.slice(64, 64 + 4);
            if (!miscSelect.every(byte => byte === 0)) {
                throw new Error('Quote miscSelect invalid!');
            }
            const reserved1 = quote.slice(68, 68 + 28);
            if (!reserved1.every(byte => byte === 0)) {
                throw new Error('Quote reserved1 invalid!');
            }
            const flags = long_1.default.fromBytesLE(Array.from(quote.slice(96, 96 + 8).values()));
            if (flags.and(SGX_CONSTANTS.SGX_FLAGS_RESERVED).notEquals(0) ||
                flags.and(SGX_CONSTANTS.SGX_FLAGS_INITTED).equals(0) ||
                flags.and(SGX_CONSTANTS.SGX_FLAGS_MODE64BIT).equals(0)) {
                throw new Error(`Quote flags invalid ${flags.toString()}`);
            }
            const xfrm = long_1.default.fromBytesLE(Array.from(quote.slice(104, 104 + 8).values()));
            if (xfrm.and(SGX_CONSTANTS.SGX_XFRM_RESERVED).notEquals(0)) {
                throw new Error(`Quote xfrm invalid ${xfrm}`);
            }
            const mrenclave = quote.slice(112, 112 + 32);
            const enclaveIdBytes = Bytes.fromHex(directoryEnclaveId);
            if (mrenclave.compare(enclaveIdBytes) !== 0) {
                throw new Error('Quote mrenclave invalid!');
            }
            const reserved2 = quote.slice(144, 144 + 32);
            if (!reserved2.every(byte => byte === 0)) {
                throw new Error('Quote reserved2 invalid!');
            }
            const reportData = quote.slice(368, 368 + 64);
            const serverStaticPublicBytes = serverStaticPublic;
            if (!reportData.every((byte, index) => {
                if (index >= 32) {
                    return byte === 0;
                }
                return byte === serverStaticPublicBytes[index];
            })) {
                throw new Error('Quote report_data invalid!');
            }
            const reserved3 = quote.slice(208, 208 + 96);
            if (!reserved3.every(byte => byte === 0)) {
                throw new Error('Quote reserved3 invalid!');
            }
            const reserved4 = quote.slice(308, 308 + 60);
            if (!reserved4.every(byte => byte === 0)) {
                throw new Error('Quote reserved4 invalid!');
            }
            const signatureLength = quote.readInt32LE(432) >>> 0;
            if (signatureLength !== quote.byteLength - 436) {
                throw new Error(`Bad signatureLength ${signatureLength}`);
            }
            // const signature = quote.slice(436, 436 + signatureLength);
        }
        function validateAttestationSignatureBody(signatureBody, encodedQuote) {
            // Parse timestamp as UTC
            const { timestamp } = signatureBody;
            const utcTimestamp = timestamp.endsWith('Z')
                ? timestamp
                : `${timestamp}Z`;
            const signatureTime = new Date(utcTimestamp).getTime();
            const now = Date.now();
            if (signatureBody.version !== 3) {
                throw new Error('Attestation signature invalid version!');
            }
            if (!encodedQuote.startsWith(signatureBody.isvEnclaveQuoteBody)) {
                throw new Error('Attestion signature mismatches quote!');
            }
            if (signatureBody.isvEnclaveQuoteStatus !== 'OK') {
                throw new Error('Attestation signature status not "OK"!');
            }
            if (signatureTime < now - 24 * 60 * 60 * 1000) {
                throw new Error('Attestation signature timestamp older than 24 hours!');
            }
        }
        async function validateAttestationSignature(signature, signatureBody, certificates) {
            const CERT_PREFIX = '-----BEGIN CERTIFICATE-----';
            const pem = (0, lodash_1.compact)(certificates.split(CERT_PREFIX).map(match => {
                if (!match) {
                    return null;
                }
                return `${CERT_PREFIX}${match}`;
            }));
            if (pem.length < 2) {
                throw new Error(`validateAttestationSignature: Expect two or more entries; got ${pem.length}`);
            }
            const verify = (0, crypto_1.createVerify)('RSA-SHA256');
            verify.update(Buffer.from(Bytes.fromString(signatureBody)));
            const isValid = verify.verify(pem[0], Buffer.from(signature));
            if (!isValid) {
                throw new Error('Validation of signature across signatureBody failed!');
            }
            const caStore = node_forge_1.pki.createCaStore([directoryTrustAnchor]);
            const chain = (0, lodash_1.compact)(pem.map(cert => node_forge_1.pki.certificateFromPem(cert)));
            const isChainValid = node_forge_1.pki.verifyCertificateChain(caStore, chain);
            if (!isChainValid) {
                throw new Error('Validation of certificate chain failed!');
            }
            const leafCert = chain[0];
            const fieldCN = leafCert.subject.getField('CN');
            if (!fieldCN ||
                fieldCN.value !== 'Intel SGX Attestation Report Signing') {
                throw new Error('Leaf cert CN field had unexpected value');
            }
            const fieldO = leafCert.subject.getField('O');
            if (!fieldO || fieldO.value !== 'Intel Corporation') {
                throw new Error('Leaf cert O field had unexpected value');
            }
            const fieldL = leafCert.subject.getField('L');
            if (!fieldL || fieldL.value !== 'Santa Clara') {
                throw new Error('Leaf cert L field had unexpected value');
            }
            const fieldST = leafCert.subject.getField('ST');
            if (!fieldST || fieldST.value !== 'CA') {
                throw new Error('Leaf cert ST field had unexpected value');
            }
            const fieldC = leafCert.subject.getField('C');
            if (!fieldC || fieldC.value !== 'US') {
                throw new Error('Leaf cert C field had unexpected value');
            }
        }
        async function putRemoteAttestation(auth) {
            const keyPair = (0, Curve_1.generateKeyPair)();
            const { privKey, pubKey } = keyPair;
            // Remove first "key type" byte from public key
            const slicedPubKey = pubKey.slice(1);
            const pubKeyBase64 = Bytes.toBase64(slicedPubKey);
            // Do request
            const data = JSON.stringify({ clientPublic: pubKeyBase64 });
            const result = (await _outerAjax(null, {
                certificateAuthority,
                type: 'PUT',
                contentType: 'application/json; charset=utf-8',
                host: directoryUrl,
                path: `${URL_CALLS.attestation}/${directoryEnclaveId}`,
                user: auth.username,
                password: auth.password,
                responseType: 'jsonwithdetails',
                data,
                timeout: 30000,
                version,
            }));
            const { data: responseBody, response } = result;
            const attestationsLength = Object.keys(responseBody.attestations).length;
            if (attestationsLength > 3) {
                throw new Error('Got more than three attestations from the Contact Discovery Service');
            }
            if (attestationsLength < 1) {
                throw new Error('Got no attestations from the Contact Discovery Service');
            }
            const cookie = response.headers.get('set-cookie');
            // Decode response
            return {
                cookie,
                attestations: await (0, p_props_1.default)(responseBody.attestations, async (attestation) => {
                    const decoded = Object.assign(Object.assign({}, attestation), { ciphertext: Bytes.fromBase64(attestation.ciphertext), iv: Bytes.fromBase64(attestation.iv), quote: Bytes.fromBase64(attestation.quote), serverEphemeralPublic: Bytes.fromBase64(attestation.serverEphemeralPublic), serverStaticPublic: Bytes.fromBase64(attestation.serverStaticPublic), signature: Bytes.fromBase64(attestation.signature), tag: Bytes.fromBase64(attestation.tag) });
                    // Validate response
                    validateAttestationQuote(decoded);
                    validateAttestationSignatureBody(JSON.parse(decoded.signatureBody), attestation.quote);
                    await validateAttestationSignature(decoded.signature, decoded.signatureBody, decoded.certificates);
                    // Derive key
                    const ephemeralToEphemeral = (0, Curve_1.calculateAgreement)(decoded.serverEphemeralPublic, privKey);
                    const ephemeralToStatic = (0, Curve_1.calculateAgreement)(decoded.serverStaticPublic, privKey);
                    const masterSecret = Bytes.concatenate([
                        ephemeralToEphemeral,
                        ephemeralToStatic,
                    ]);
                    const publicKeys = Bytes.concatenate([
                        slicedPubKey,
                        decoded.serverEphemeralPublic,
                        decoded.serverStaticPublic,
                    ]);
                    const [clientKey, serverKey] = (0, Crypto_1.deriveSecrets)(masterSecret, publicKeys, new Uint8Array(0));
                    // Decrypt ciphertext into requestId
                    const requestId = (0, Crypto_1.decryptAesGcm)(serverKey, decoded.iv, Bytes.concatenate([decoded.ciphertext, decoded.tag]));
                    return {
                        clientKey,
                        serverKey,
                        requestId,
                    };
                }),
            };
        }
        async function getUuidsForE164s(e164s) {
            const directoryAuth = await getDirectoryAuth();
            const attestationResult = await putRemoteAttestation(directoryAuth);
            // Encrypt data for discovery
            const data = await (0, Crypto_1.encryptCdsDiscoveryRequest)(attestationResult.attestations, e164s);
            const { cookie } = attestationResult;
            // Send discovery request
            const discoveryResponse = (await _outerAjax(null, {
                certificateAuthority,
                type: 'PUT',
                headers: cookie
                    ? {
                        cookie,
                    }
                    : undefined,
                contentType: 'application/json; charset=utf-8',
                host: directoryUrl,
                path: `${URL_CALLS.discovery}/${directoryEnclaveId}`,
                user: directoryAuth.username,
                password: directoryAuth.password,
                responseType: 'json',
                timeout: 30000,
                data: JSON.stringify(data),
                version,
            }));
            // Decode discovery request response
            const decodedDiscoveryResponse = (0, lodash_1.mapValues)(discoveryResponse, value => {
                return Bytes.fromBase64(value);
            });
            const returnedAttestation = Object.values(attestationResult.attestations).find(at => (0, Crypto_1.constantTimeEqual)(at.requestId, decodedDiscoveryResponse.requestId));
            if (!returnedAttestation) {
                throw new Error('No known attestations returned from CDS');
            }
            // Decrypt discovery response
            const decryptedDiscoveryData = (0, Crypto_1.decryptAesGcm)(returnedAttestation.serverKey, decodedDiscoveryResponse.iv, Bytes.concatenate([
                decodedDiscoveryResponse.data,
                decodedDiscoveryResponse.mac,
            ]));
            // Process and return result
            const uuids = (0, Crypto_1.splitUuids)(decryptedDiscoveryData);
            if (uuids.length !== e164s.length) {
                throw new Error('Returned set of UUIDs did not match returned set of e164s!');
            }
            return (0, lodash_1.zipObject)(e164s, uuids);
        }
        async function getUuidsForE164sV2(e164s) {
            const auth = await getDirectoryAuthV2();
            const uuids = await cdsSocketManager.request({
                auth,
                e164s,
            });
            return (0, lodash_1.zipObject)(e164s, uuids);
        }
    }
}
exports.initialize = initialize;
