"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-proto */
/* eslint-disable @typescript-eslint/no-explicit-any */
const arrayBuffer = new ArrayBuffer(0);
const uint8Array = new Uint8Array();
const StaticArrayBufferProto = arrayBuffer.__proto__;
const StaticUint8ArrayProto = uint8Array.__proto__;
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
function getString(thing) {
    if (thing === Object(thing)) {
        if (thing.__proto__ === StaticUint8ArrayProto) {
            return String.fromCharCode.apply(null, thing);
        }
        if (thing.__proto__ === StaticArrayBufferProto) {
            return getString(new Uint8Array(thing));
        }
    }
    return thing;
}
function getStringable(thing) {
    return (typeof thing === 'string' ||
        typeof thing === 'number' ||
        typeof thing === 'boolean' ||
        (thing === Object(thing) &&
            (thing.__proto__ === StaticArrayBufferProto ||
                thing.__proto__ === StaticUint8ArrayProto)));
}
function ensureStringed(thing) {
    if (getStringable(thing)) {
        return getString(thing);
    }
    if (thing instanceof Array) {
        const res = [];
        for (let i = 0; i < thing.length; i += 1) {
            res[i] = ensureStringed(thing[i]);
        }
        return res;
    }
    if (thing === Object(thing)) {
        const res = {};
        for (const key in thing) {
            res[key] = ensureStringed(thing[key]);
        }
        return res;
    }
    if (thing === null) {
        return null;
    }
    throw new Error(`unsure of how to jsonify object of type ${typeof thing}`);
}
// Number formatting utils
const utils = {
    getString,
    isNumberSane: (number) => number[0] === '+' && /^[0-9]+$/.test(number.substring(1)),
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    jsonThing: (thing) => JSON.stringify(ensureStringed(thing)),
    unencodeNumber: (number) => number.split('.'),
};
exports.default = utils;
