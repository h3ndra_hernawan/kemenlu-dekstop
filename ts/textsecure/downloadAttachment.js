"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.downloadAttachment = void 0;
const lodash_1 = require("lodash");
const assert_1 = require("../util/assert");
const dropNull_1 = require("../util/dropNull");
const MIME = __importStar(require("../types/MIME"));
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
async function downloadAttachment(server, attachment) {
    const cdnId = attachment.cdnId || attachment.cdnKey;
    const { cdnNumber } = attachment;
    if (!cdnId) {
        throw new Error('downloadAttachment: Attachment was missing cdnId!');
    }
    (0, assert_1.strictAssert)(cdnId, 'attachment without cdnId');
    const encrypted = await server.getAttachment(cdnId, (0, dropNull_1.dropNull)(cdnNumber));
    const { key, digest, size, contentType } = attachment;
    if (!digest) {
        throw new Error('Failure: Ask sender to update Signal and resend.');
    }
    (0, assert_1.strictAssert)(key, 'attachment has no key');
    (0, assert_1.strictAssert)(digest, 'attachment has no digest');
    const paddedData = (0, Crypto_1.decryptAttachment)(encrypted, Bytes.fromBase64(key), Bytes.fromBase64(digest));
    if (!(0, lodash_1.isNumber)(size)) {
        throw new Error(`downloadAttachment: Size was not provided, actual size was ${paddedData.byteLength}`);
    }
    const data = (0, Crypto_1.getFirstBytes)(paddedData, size);
    return Object.assign(Object.assign({}, (0, lodash_1.omit)(attachment, 'digest', 'key')), { size, contentType: contentType
            ? MIME.stringToMIMEType(contentType)
            : MIME.APPLICATION_OCTET_STREAM, data });
}
exports.downloadAttachment = downloadAttachment;
