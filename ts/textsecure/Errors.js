"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.WarnOnlyError = exports.ConnectTimeoutError = exports.UnregisteredUserError = exports.MessageError = exports.SignedPreKeyRotationError = exports.SendMessageProtoError = exports.SendMessageChallengeError = exports.SendMessageNetworkError = exports.OutgoingMessageError = exports.OutgoingIdentityKeyError = exports.ReplayableError = exports.HTTPError = void 0;
/* eslint-disable max-classes-per-file */
const parseRetryAfter_1 = require("../util/parseRetryAfter");
function appendStack(newError, originalError) {
    // eslint-disable-next-line no-param-reassign
    newError.stack += `\nOriginal stack:\n${originalError.stack}`;
}
class HTTPError extends Error {
    constructor(message, options) {
        super(`${message}; code: ${options.code}`);
        this.name = 'HTTPError';
        const { code: providedCode, headers, response, stack } = options;
        this.code = providedCode > 999 || providedCode < 100 ? -1 : providedCode;
        this.responseHeaders = headers;
        this.stack += `\nOriginal stack:\n${stack}`;
        this.response = response;
    }
}
exports.HTTPError = HTTPError;
class ReplayableError extends Error {
    constructor(options) {
        super(options.message);
        this.name = options.name || 'ReplayableError';
        this.message = options.message;
        // Maintains proper stack trace, where our error was thrown (only available on V8)
        //   via https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this);
        }
        this.functionCode = options.functionCode;
    }
}
exports.ReplayableError = ReplayableError;
class OutgoingIdentityKeyError extends ReplayableError {
    // Note: Data to resend message is no longer captured
    constructor(incomingIdentifier, _m, _t, identityKey) {
        const identifier = incomingIdentifier.split('.')[0];
        super({
            name: 'OutgoingIdentityKeyError',
            message: `The identity of ${identifier} has changed.`,
        });
        this.identifier = identifier;
        this.identityKey = identityKey;
    }
}
exports.OutgoingIdentityKeyError = OutgoingIdentityKeyError;
class OutgoingMessageError extends ReplayableError {
    // Note: Data to resend message is no longer captured
    constructor(incomingIdentifier, _m, _t, httpError) {
        const identifier = incomingIdentifier.split('.')[0];
        super({
            name: 'OutgoingMessageError',
            message: httpError ? httpError.message : 'no http error',
        });
        this.identifier = identifier;
        if (httpError) {
            this.httpError = httpError;
            appendStack(this, httpError);
        }
    }
    get code() {
        var _a;
        return (_a = this.httpError) === null || _a === void 0 ? void 0 : _a.code;
    }
}
exports.OutgoingMessageError = OutgoingMessageError;
class SendMessageNetworkError extends ReplayableError {
    constructor(identifier, _m, httpError) {
        super({
            name: 'SendMessageNetworkError',
            message: httpError.message,
        });
        [this.identifier] = identifier.split('.');
        this.httpError = httpError;
        appendStack(this, httpError);
    }
    get code() {
        return this.httpError.code;
    }
    get responseHeaders() {
        return this.httpError.responseHeaders;
    }
}
exports.SendMessageNetworkError = SendMessageNetworkError;
class SendMessageChallengeError extends ReplayableError {
    constructor(identifier, httpError) {
        super({
            name: 'SendMessageChallengeError',
            message: httpError.message,
        });
        [this.identifier] = identifier.split('.');
        this.httpError = httpError;
        this.data = httpError.response;
        const headers = httpError.responseHeaders || {};
        this.retryAfter = Date.now() + (0, parseRetryAfter_1.parseRetryAfter)(headers['retry-after']);
        appendStack(this, httpError);
    }
    get code() {
        return this.httpError.code;
    }
}
exports.SendMessageChallengeError = SendMessageChallengeError;
class SendMessageProtoError extends Error {
    constructor({ successfulIdentifiers, failoverIdentifiers, errors, unidentifiedDeliveries, dataMessage, contentHint, contentProto, timestamp, recipients, }) {
        super(`SendMessageProtoError: ${SendMessageProtoError.getMessage(errors)}`);
        this.successfulIdentifiers = successfulIdentifiers;
        this.failoverIdentifiers = failoverIdentifiers;
        this.errors = errors;
        this.unidentifiedDeliveries = unidentifiedDeliveries;
        this.dataMessage = dataMessage;
        this.contentHint = contentHint;
        this.contentProto = contentProto;
        this.timestamp = timestamp;
        this.recipients = recipients;
    }
    static getMessage(errors) {
        if (!errors) {
            return 'No errors';
        }
        return errors
            .map(error => (error.stackForLog ? error.stackForLog : error.toString()))
            .join(', ');
    }
}
exports.SendMessageProtoError = SendMessageProtoError;
class SignedPreKeyRotationError extends ReplayableError {
    constructor() {
        super({
            name: 'SignedPreKeyRotationError',
            message: 'Too many signed prekey rotation failures',
        });
    }
}
exports.SignedPreKeyRotationError = SignedPreKeyRotationError;
class MessageError extends ReplayableError {
    constructor(_m, httpError) {
        super({
            name: 'MessageError',
            message: httpError.message,
        });
        this.httpError = httpError;
        appendStack(this, httpError);
    }
    get code() {
        return this.httpError.code;
    }
}
exports.MessageError = MessageError;
class UnregisteredUserError extends Error {
    constructor(identifier, httpError) {
        const { message } = httpError;
        super(message);
        this.message = message;
        this.name = 'UnregisteredUserError';
        // Maintains proper stack trace, where our error was thrown (only available on V8)
        //   via https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this);
        }
        this.identifier = identifier;
        this.httpError = httpError;
        appendStack(this, httpError);
    }
    get code() {
        return this.httpError.code;
    }
}
exports.UnregisteredUserError = UnregisteredUserError;
class ConnectTimeoutError extends Error {
}
exports.ConnectTimeoutError = ConnectTimeoutError;
class WarnOnlyError extends Error {
}
exports.WarnOnlyError = WarnOnlyError;
