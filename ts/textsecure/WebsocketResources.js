"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CloseEvent = exports.IncomingWebSocketRequest = void 0;
const EventTarget_1 = __importDefault(require("./EventTarget"));
const durations = __importStar(require("../util/durations"));
const dropNull_1 = require("../util/dropNull");
const timestamp_1 = require("../util/timestamp");
const assert_1 = require("../util/assert");
const normalizeNumber_1 = require("../util/normalizeNumber");
const Errors = __importStar(require("../types/errors"));
const protobuf_1 = require("../protobuf");
const log = __importStar(require("../logging/log"));
const Timers = __importStar(require("../Timers"));
const THIRTY_SECONDS = 30 * durations.SECOND;
const MAX_MESSAGE_SIZE = 256 * 1024;
class IncomingWebSocketRequest {
    constructor(request, sendBytes) {
        this.sendBytes = sendBytes;
        (0, assert_1.strictAssert)(request.id, 'request without id');
        (0, assert_1.strictAssert)(request.verb, 'request without verb');
        (0, assert_1.strictAssert)(request.path, 'request without path');
        this.id = request.id;
        this.verb = request.verb;
        this.path = request.path;
        this.body = (0, dropNull_1.dropNull)(request.body);
        this.headers = request.headers || [];
    }
    respond(status, message) {
        const bytes = protobuf_1.SignalService.WebSocketMessage.encode({
            type: protobuf_1.SignalService.WebSocketMessage.Type.RESPONSE,
            response: { id: this.id, message, status },
        }).finish();
        this.sendBytes(Buffer.from(bytes));
    }
}
exports.IncomingWebSocketRequest = IncomingWebSocketRequest;
class CloseEvent extends Event {
    constructor(code, reason) {
        super('close');
        this.code = code;
        this.reason = reason;
    }
}
exports.CloseEvent = CloseEvent;
class WebSocketResource extends EventTarget_1.default {
    constructor(socket, options = {}) {
        super();
        this.socket = socket;
        this.options = options;
        this.outgoingId = 1;
        this.closed = false;
        this.outgoingMap = new Map();
        this.activeRequests = new Set();
        this.shuttingDown = false;
        this.boundOnMessage = this.onMessage.bind(this);
        socket.on('message', this.boundOnMessage);
        if (options.keepalive) {
            const keepalive = new KeepAlive(this, options.keepalive === true ? {} : options.keepalive);
            this.keepalive = keepalive;
            keepalive.reset();
            socket.on('message', () => keepalive.reset());
            socket.on('close', () => keepalive.stop());
            socket.on('error', (error) => {
                log.warn('WebSocketResource: WebSocket error', Errors.toLogFormat(error));
            });
        }
        socket.on('close', (code, reason) => {
            this.closed = true;
            log.warn('WebSocketResource: Socket closed');
            this.dispatchEvent(new CloseEvent(code, reason || 'normal'));
        });
        this.addEventListener('close', () => this.onClose());
    }
    addEventListener(name, handler) {
        return super.addEventListener(name, handler);
    }
    async sendRequest(options) {
        const id = this.outgoingId;
        (0, assert_1.strictAssert)(!this.outgoingMap.has(id), 'Duplicate outgoing request');
        // eslint-disable-next-line no-bitwise
        this.outgoingId = Math.max(1, (this.outgoingId + 1) & 0x7fffffff);
        const bytes = protobuf_1.SignalService.WebSocketMessage.encode({
            type: protobuf_1.SignalService.WebSocketMessage.Type.REQUEST,
            request: {
                verb: options.verb,
                path: options.path,
                body: options.body,
                headers: options.headers ? options.headers.slice() : undefined,
                id,
            },
        }).finish();
        (0, assert_1.strictAssert)(bytes.length <= MAX_MESSAGE_SIZE, 'WebSocket request byte size exceeded');
        (0, assert_1.strictAssert)(!this.shuttingDown, 'Cannot send request, shutting down');
        this.addActive(id);
        const promise = new Promise((resolve, reject) => {
            let timer = options.timeout
                ? Timers.setTimeout(() => {
                    this.removeActive(id);
                    reject(new Error('Request timed out'));
                }, options.timeout)
                : undefined;
            this.outgoingMap.set(id, result => {
                if (timer !== undefined) {
                    Timers.clearTimeout(timer);
                    timer = undefined;
                }
                this.removeActive(id);
                resolve(result);
            });
        });
        this.socket.sendBytes(Buffer.from(bytes));
        return promise;
    }
    forceKeepAlive() {
        if (!this.keepalive) {
            return;
        }
        this.keepalive.send();
    }
    close(code = 3000, reason) {
        if (this.closed) {
            return;
        }
        log.info('WebSocketResource.close()');
        if (this.keepalive) {
            this.keepalive.stop();
        }
        this.socket.close(code, reason);
        this.socket.removeListener('message', this.boundOnMessage);
        // On linux the socket can wait a long time to emit its close event if we've
        //   lost the internet connection. On the order of minutes. This speeds that
        //   process up.
        Timers.setTimeout(() => {
            if (this.closed) {
                return;
            }
            log.warn('WebSocketResource: Dispatching our own socket close event');
            this.dispatchEvent(new CloseEvent(code, reason || 'normal'));
        }, 5000);
    }
    shutdown() {
        if (this.closed) {
            return;
        }
        if (this.activeRequests.size === 0) {
            log.info('WebSocketResource: no active requests, closing');
            this.close(3000, 'Shutdown');
            return;
        }
        this.shuttingDown = true;
        log.info('WebSocketResource: shutting down');
        this.shutdownTimer = Timers.setTimeout(() => {
            if (this.closed) {
                return;
            }
            log.warn('WebSocketResource: Failed to shutdown gracefully');
            this.close(3000, 'Shutdown');
        }, THIRTY_SECONDS);
    }
    onMessage({ type, binaryData }) {
        var _a, _b, _c;
        if (type !== 'binary' || !binaryData) {
            throw new Error(`Unsupported websocket message type: ${type}`);
        }
        const message = protobuf_1.SignalService.WebSocketMessage.decode(binaryData);
        if (message.type === protobuf_1.SignalService.WebSocketMessage.Type.REQUEST &&
            message.request) {
            const handleRequest = this.options.handleRequest ||
                (request => request.respond(404, 'Not found'));
            const incomingRequest = new IncomingWebSocketRequest(message.request, (bytes) => {
                this.removeActive(incomingRequest);
                (0, assert_1.strictAssert)(bytes.length <= MAX_MESSAGE_SIZE, 'WebSocket response byte size exceeded');
                this.socket.sendBytes(bytes);
            });
            if (this.shuttingDown) {
                incomingRequest.respond(-1, 'Shutting down');
                return;
            }
            this.addActive(incomingRequest);
            handleRequest(incomingRequest);
        }
        else if (message.type === protobuf_1.SignalService.WebSocketMessage.Type.RESPONSE &&
            message.response) {
            const { response } = message;
            (0, assert_1.strictAssert)(response.id, 'response without id');
            const responseId = (0, normalizeNumber_1.normalizeNumber)(response.id);
            const resolve = this.outgoingMap.get(responseId);
            this.outgoingMap.delete(responseId);
            if (!resolve) {
                throw new Error(`Received response for unknown request ${responseId}`);
            }
            resolve({
                status: (_a = response.status) !== null && _a !== void 0 ? _a : -1,
                message: (_b = response.message) !== null && _b !== void 0 ? _b : '',
                response: (0, dropNull_1.dropNull)(response.body),
                headers: (_c = response.headers) !== null && _c !== void 0 ? _c : [],
            });
        }
    }
    onClose() {
        const outgoing = new Map(this.outgoingMap);
        this.outgoingMap.clear();
        for (const resolve of outgoing.values()) {
            resolve({
                status: -1,
                message: 'Connection closed',
                response: undefined,
                headers: [],
            });
        }
    }
    addActive(request) {
        this.activeRequests.add(request);
    }
    removeActive(request) {
        if (!this.activeRequests.has(request)) {
            log.warn('WebSocketResource: removing unknown request');
            return;
        }
        this.activeRequests.delete(request);
        if (this.activeRequests.size !== 0) {
            return;
        }
        if (!this.shuttingDown) {
            return;
        }
        if (this.shutdownTimer) {
            Timers.clearTimeout(this.shutdownTimer);
            this.shutdownTimer = undefined;
        }
        log.info('WebSocketResource: shutdown complete');
        this.close(3000, 'Shutdown');
    }
}
exports.default = WebSocketResource;
const KEEPALIVE_INTERVAL_MS = 55000; // 55 seconds + 5 seconds for closing the
// socket above.
const MAX_KEEPALIVE_INTERVAL_MS = 5 * durations.MINUTE;
class KeepAlive {
    constructor(websocketResource, opts = {}) {
        this.lastAliveAt = Date.now();
        if (websocketResource instanceof WebSocketResource) {
            this.path = opts.path !== undefined ? opts.path : '/';
            this.disconnect = opts.disconnect !== undefined ? opts.disconnect : true;
            this.wsr = websocketResource;
        }
        else {
            throw new TypeError('KeepAlive expected a WebSocketResource');
        }
    }
    stop() {
        this.clearTimers();
    }
    async send() {
        this.clearTimers();
        if ((0, timestamp_1.isOlderThan)(this.lastAliveAt, MAX_KEEPALIVE_INTERVAL_MS)) {
            log.info('WebSocketResources: disconnecting due to stale state');
            this.wsr.close(3001, `Last keepalive request was too far in the past: ${this.lastAliveAt}`);
            return;
        }
        if (this.disconnect) {
            // automatically disconnect if server doesn't ack
            this.disconnectTimer = Timers.setTimeout(() => {
                log.info('WebSocketResources: disconnecting due to no response');
                this.clearTimers();
                this.wsr.close(3001, 'No response to keepalive request');
            }, 10000);
        }
        else {
            this.reset();
        }
        log.info('WebSocketResources: Sending a keepalive message');
        const { status } = await this.wsr.sendRequest({
            verb: 'GET',
            path: this.path,
        });
        if (status >= 200 || status < 300) {
            this.reset();
        }
    }
    reset() {
        this.lastAliveAt = Date.now();
        this.clearTimers();
        this.keepAliveTimer = Timers.setTimeout(() => this.send(), KEEPALIVE_INTERVAL_MS);
    }
    clearTimers() {
        if (this.keepAliveTimer) {
            Timers.clearTimeout(this.keepAliveTimer);
            this.keepAliveTimer = undefined;
        }
        if (this.disconnectTimer) {
            Timers.clearTimeout(this.disconnectTimer);
            this.disconnectTimer = undefined;
        }
    }
}
