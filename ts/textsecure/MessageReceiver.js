"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-bitwise */
/* eslint-disable camelcase */
const lodash_1 = require("lodash");
const p_queue_1 = __importDefault(require("p-queue"));
const uuid_1 = require("uuid");
const signal_client_1 = require("@signalapp/signal-client");
const LibSignalStores_1 = require("../LibSignalStores");
const Curve_1 = require("../Curve");
const assert_1 = require("../util/assert");
const batcher_1 = require("../util/batcher");
const dropNull_1 = require("../util/dropNull");
const normalizeUuid_1 = require("../util/normalizeUuid");
const normalizeNumber_1 = require("../util/normalizeNumber");
const parseIntOrThrow_1 = require("../util/parseIntOrThrow");
const Zone_1 = require("../util/Zone");
const Crypto_1 = require("../Crypto");
const Address_1 = require("../types/Address");
const QualifiedAddress_1 = require("../types/QualifiedAddress");
const UUID_1 = require("../types/UUID");
const Errors = __importStar(require("../types/errors"));
const protobuf_1 = require("../protobuf");
const groups_1 = require("../groups");
const TaskWithTimeout_1 = __importDefault(require("./TaskWithTimeout"));
const processDataMessage_1 = require("./processDataMessage");
const processSyncMessage_1 = require("./processSyncMessage");
const EventTarget_1 = __importDefault(require("./EventTarget"));
const downloadAttachment_1 = require("./downloadAttachment");
const ContactsParser_1 = require("./ContactsParser");
const Errors_1 = require("./Errors");
const Bytes = __importStar(require("../Bytes"));
const messageReceiverEvents_1 = require("./messageReceiverEvents");
const log = __importStar(require("../logging/log"));
const GROUPV1_ID_LENGTH = 16;
const GROUPV2_ID_LENGTH = 32;
const RETRY_TIMEOUT = 2 * 60 * 1000;
var TaskType;
(function (TaskType) {
    TaskType["Encrypted"] = "Encrypted";
    TaskType["Decrypted"] = "Decrypted";
})(TaskType || (TaskType = {}));
class MessageReceiver extends EventTarget_1.default {
    constructor({ server, storage, serverTrustRoot }) {
        super();
        this.server = server;
        this.storage = storage;
        this.count = 0;
        this.processedCount = 0;
        if (!serverTrustRoot) {
            throw new Error('Server trust root is required!');
        }
        this.serverTrustRoot = Bytes.fromBase64(serverTrustRoot);
        this.incomingQueue = new p_queue_1.default({ concurrency: 1, timeout: 1000 * 60 * 2 });
        this.appQueue = new p_queue_1.default({ concurrency: 1, timeout: 1000 * 60 * 2 });
        // All envelopes start in encryptedQueue and progress to decryptedQueue
        this.encryptedQueue = new p_queue_1.default({
            concurrency: 1,
            timeout: 1000 * 60 * 2,
        });
        this.decryptedQueue = new p_queue_1.default({
            concurrency: 1,
            timeout: 1000 * 60 * 2,
        });
        this.decryptAndCacheBatcher = (0, batcher_1.createBatcher)({
            name: 'MessageReceiver.decryptAndCacheBatcher',
            wait: 75,
            maxSize: 30,
            processBatch: (items) => {
                // Not returning the promise here because we don't want to stall
                // the batch.
                this.decryptAndCacheBatch(items);
            },
        });
        this.cacheRemoveBatcher = (0, batcher_1.createBatcher)({
            name: 'MessageReceiver.cacheRemoveBatcher',
            wait: 75,
            maxSize: 30,
            processBatch: this.cacheRemoveBatch.bind(this),
        });
    }
    getProcessedCount() {
        return this.processedCount;
    }
    handleRequest(request) {
        // We do the message decryption here, instead of in the ordered pending queue,
        // to avoid exposing the time it took us to process messages through the time-to-ack.
        log.info('MessageReceiver: got request', request.verb, request.path);
        if (request.path !== '/api/v1/message') {
            request.respond(200, 'OK');
            if (request.verb === 'PUT' && request.path === '/api/v1/queue/empty') {
                this.incomingQueue.add(() => {
                    this.onEmpty();
                });
            }
            return;
        }
        const job = async () => {
            const headers = request.headers || [];
            if (!request.body) {
                throw new Error('MessageReceiver.handleRequest: request.body was falsey!');
            }
            const plaintext = request.body;
            try {
                const decoded = protobuf_1.SignalService.Envelope.decode(plaintext);
                const serverTimestamp = (0, normalizeNumber_1.normalizeNumber)(decoded.serverTimestamp);
                const ourUuid = this.storage.user.getCheckedUuid();
                const envelope = {
                    // Make non-private envelope IDs dashless so they don't get redacted
                    //   from logs
                    id: (0, uuid_1.v4)().replace(/-/g, ''),
                    receivedAtCounter: window.Signal.Util.incrementMessageCounter(),
                    receivedAtDate: Date.now(),
                    // Calculate the message age (time on server).
                    messageAgeSec: this.calculateMessageAge(headers, serverTimestamp),
                    // Proto.Envelope fields
                    type: decoded.type,
                    source: decoded.source,
                    sourceUuid: decoded.sourceUuid
                        ? (0, normalizeUuid_1.normalizeUuid)(decoded.sourceUuid, 'MessageReceiver.handleRequest.sourceUuid')
                        : undefined,
                    sourceDevice: decoded.sourceDevice,
                    destinationUuid: decoded.destinationUuid
                        ? new UUID_1.UUID((0, normalizeUuid_1.normalizeUuid)(decoded.destinationUuid, 'MessageReceiver.handleRequest.destinationUuid'))
                        : ourUuid,
                    timestamp: (0, normalizeNumber_1.normalizeNumber)(decoded.timestamp),
                    legacyMessage: (0, dropNull_1.dropNull)(decoded.legacyMessage),
                    content: (0, dropNull_1.dropNull)(decoded.content),
                    serverGuid: decoded.serverGuid,
                    serverTimestamp,
                };
                // After this point, decoding errors are not the server's
                //   fault, and we should handle them gracefully and tell the
                //   user they received an invalid message
                this.decryptAndCache(envelope, plaintext, request);
                this.processedCount += 1;
            }
            catch (e) {
                request.respond(500, 'Bad encrypted websocket message');
                log.error('Error handling incoming message:', Errors.toLogFormat(e));
                await this.dispatchAndWait(new messageReceiverEvents_1.ErrorEvent(e));
            }
        };
        this.incomingQueue.add(job);
    }
    reset() {
        // We always process our cache before processing a new websocket message
        this.incomingQueue.add(async () => this.queueAllCached());
        this.count = 0;
        this.isEmptied = false;
        this.stoppingProcessing = false;
    }
    stopProcessing() {
        this.stoppingProcessing = true;
    }
    hasEmptied() {
        return Boolean(this.isEmptied);
    }
    async drain() {
        const waitForEncryptedQueue = async () => this.addToQueue(async () => {
            log.info('drained');
        }, TaskType.Decrypted);
        const waitForIncomingQueue = async () => this.addToQueue(waitForEncryptedQueue, TaskType.Encrypted);
        return this.incomingQueue.add(waitForIncomingQueue);
    }
    addEventListener(name, handler) {
        return super.addEventListener(name, handler);
    }
    removeEventListener(name, handler) {
        return super.removeEventListener(name, handler);
    }
    //
    // Private
    //
    async dispatchAndWait(event) {
        this.appQueue.add(async () => Promise.all(this.dispatchEvent(event)));
    }
    calculateMessageAge(headers, serverTimestamp) {
        let messageAgeSec = 0; // Default to 0 in case of unreliable parameters.
        if (serverTimestamp) {
            // The 'X-Signal-Timestamp' is usually the last item, so start there.
            let it = headers.length;
            // eslint-disable-next-line no-plusplus
            while (--it >= 0) {
                const match = headers[it].match(/^X-Signal-Timestamp:\s*(\d+)\s*$/);
                if (match && match.length === 2) {
                    const timestamp = Number(match[1]);
                    // One final sanity check, the timestamp when a message is pulled from
                    // the server should be later than when it was pushed.
                    if (timestamp > serverTimestamp) {
                        messageAgeSec = Math.floor((timestamp - serverTimestamp) / 1000);
                    }
                    break;
                }
            }
        }
        return messageAgeSec;
    }
    async addToQueue(task, taskType) {
        if (taskType === TaskType.Encrypted) {
            this.count += 1;
        }
        const queue = taskType === TaskType.Encrypted
            ? this.encryptedQueue
            : this.decryptedQueue;
        try {
            return await queue.add(task);
        }
        finally {
            this.updateProgress(this.count);
        }
    }
    onEmpty() {
        const emitEmpty = async () => {
            await Promise.all([
                this.decryptAndCacheBatcher.flushAndWait(),
                this.cacheRemoveBatcher.flushAndWait(),
            ]);
            log.info("MessageReceiver: emitting 'empty' event");
            this.dispatchEvent(new messageReceiverEvents_1.EmptyEvent());
            this.isEmptied = true;
            this.maybeScheduleRetryTimeout();
        };
        const waitForDecryptedQueue = async () => {
            log.info("MessageReceiver: finished processing messages after 'empty', now waiting for application");
            // We don't await here because we don't want this to gate future message processing
            this.appQueue.add(emitEmpty);
        };
        const waitForEncryptedQueue = async () => {
            this.addToQueue(waitForDecryptedQueue, TaskType.Decrypted);
        };
        const waitForIncomingQueue = () => {
            this.addToQueue(waitForEncryptedQueue, TaskType.Encrypted);
            // Note: this.count is used in addToQueue
            // Resetting count so everything from the websocket after this starts at zero
            this.count = 0;
        };
        const waitForCacheAddBatcher = async () => {
            await this.decryptAndCacheBatcher.onIdle();
            this.incomingQueue.add(waitForIncomingQueue);
        };
        waitForCacheAddBatcher();
    }
    updateProgress(count) {
        // count by 10s
        if (count % 10 !== 0) {
            return;
        }
        this.dispatchEvent(new messageReceiverEvents_1.ProgressEvent({ count }));
    }
    async queueAllCached() {
        const items = await this.getAllFromCache();
        const max = items.length;
        for (let i = 0; i < max; i += 1) {
            // eslint-disable-next-line no-await-in-loop
            await this.queueCached(items[i]);
        }
    }
    async queueCached(item) {
        log.info('MessageReceiver.queueCached', item.id);
        try {
            let envelopePlaintext;
            if (item.envelope && item.version === 2) {
                envelopePlaintext = Bytes.fromBase64(item.envelope);
            }
            else if (item.envelope && typeof item.envelope === 'string') {
                envelopePlaintext = Bytes.fromBinary(item.envelope);
            }
            else {
                throw new Error('MessageReceiver.queueCached: item.envelope was malformed');
            }
            const decoded = protobuf_1.SignalService.Envelope.decode(envelopePlaintext);
            const ourUuid = this.storage.user.getCheckedUuid();
            const envelope = {
                id: item.id,
                receivedAtCounter: item.timestamp,
                receivedAtDate: Date.now(),
                messageAgeSec: item.messageAgeSec || 0,
                // Proto.Envelope fields
                type: decoded.type,
                source: decoded.source || item.source,
                sourceUuid: decoded.sourceUuid || item.sourceUuid,
                sourceDevice: decoded.sourceDevice || item.sourceDevice,
                destinationUuid: new UUID_1.UUID(decoded.destinationUuid || item.destinationUuid || ourUuid.toString()),
                timestamp: (0, normalizeNumber_1.normalizeNumber)(decoded.timestamp),
                legacyMessage: (0, dropNull_1.dropNull)(decoded.legacyMessage),
                content: (0, dropNull_1.dropNull)(decoded.content),
                serverGuid: decoded.serverGuid,
                serverTimestamp: (0, normalizeNumber_1.normalizeNumber)(item.serverTimestamp || decoded.serverTimestamp),
            };
            const { decrypted } = item;
            if (decrypted) {
                let payloadPlaintext;
                if (item.version === 2) {
                    payloadPlaintext = Bytes.fromBase64(decrypted);
                }
                else if (typeof decrypted === 'string') {
                    payloadPlaintext = Bytes.fromBinary(decrypted);
                }
                else {
                    throw new Error('Cached decrypted value was not a string!');
                }
                // Maintain invariant: encrypted queue => decrypted queue
                this.addToQueue(async () => {
                    this.queueDecryptedEnvelope(envelope, payloadPlaintext);
                }, TaskType.Encrypted);
            }
            else {
                this.queueCachedEnvelope(item, envelope);
            }
        }
        catch (error) {
            log.error('queueCached error handling item', item.id, 'removing it. Error:', Errors.toLogFormat(error));
            try {
                const { id } = item;
                await this.storage.protocol.removeUnprocessed(id);
            }
            catch (deleteError) {
                log.error('queueCached error deleting item', item.id, 'Error:', Errors.toLogFormat(deleteError));
            }
        }
    }
    getEnvelopeId(envelope) {
        const { timestamp } = envelope;
        let prefix = '';
        if (envelope.sourceUuid || envelope.source) {
            const sender = envelope.sourceUuid || envelope.source;
            prefix += `${sender}.${envelope.sourceDevice} `;
        }
        prefix += `> ${envelope.destinationUuid.toString()}`;
        return `${prefix}${timestamp} (${envelope.id})`;
    }
    clearRetryTimeout() {
        if (this.retryCachedTimeout) {
            clearInterval(this.retryCachedTimeout);
            this.retryCachedTimeout = undefined;
        }
    }
    maybeScheduleRetryTimeout() {
        if (this.isEmptied) {
            this.clearRetryTimeout();
            this.retryCachedTimeout = setTimeout(() => {
                this.incomingQueue.add(async () => this.queueAllCached());
            }, RETRY_TIMEOUT);
        }
    }
    async getAllFromCache() {
        log.info('getAllFromCache');
        const count = await this.storage.protocol.getUnprocessedCount();
        if (count > 1500) {
            await this.storage.protocol.removeAllUnprocessed();
            log.warn(`There were ${count} messages in cache. Deleted all instead of reprocessing`);
            return [];
        }
        const items = await this.storage.protocol.getAllUnprocessed();
        log.info('getAllFromCache loaded', items.length, 'saved envelopes');
        return items.map(item => {
            const { attempts = 0 } = item;
            return Object.assign(Object.assign({}, item), { attempts: attempts + 1 });
        });
    }
    async decryptAndCacheBatch(items) {
        log.info('MessageReceiver.decryptAndCacheBatch', items.length);
        const decrypted = [];
        const storageProtocol = this.storage.protocol;
        try {
            const zone = new Zone_1.Zone('decryptAndCacheBatch', {
                pendingSessions: true,
                pendingUnprocessed: true,
            });
            const storesMap = new Map();
            const failed = [];
            // Below we:
            //
            // 1. Enter zone
            // 2. Decrypt all batched envelopes
            // 3. Persist both decrypted envelopes and envelopes that we failed to
            //    decrypt (for future retries, see `attempts` field)
            // 4. Leave zone and commit all pending sessions and unprocesseds
            // 5. Acknowledge envelopes (can't fail)
            // 6. Finally process decrypted envelopes
            await storageProtocol.withZone(zone, 'MessageReceiver', async () => {
                await Promise.all(items.map(async ({ data, envelope }) => {
                    try {
                        const { destinationUuid } = envelope;
                        const uuidKind = this.storage.user.getOurUuidKind(destinationUuid);
                        if (uuidKind === UUID_1.UUIDKind.Unknown) {
                            log.warn('MessageReceiver.decryptAndCacheBatch: ' +
                                `Rejecting envelope ${this.getEnvelopeId(envelope)}, ` +
                                `unknown uuid: ${destinationUuid}`);
                            return;
                        }
                        let stores = storesMap.get(destinationUuid.toString());
                        if (!stores) {
                            stores = {
                                sessionStore: new LibSignalStores_1.Sessions({
                                    zone,
                                    ourUuid: destinationUuid,
                                }),
                                identityKeyStore: new LibSignalStores_1.IdentityKeys({
                                    zone,
                                    ourUuid: destinationUuid,
                                }),
                                zone,
                            };
                            storesMap.set(destinationUuid.toString(), stores);
                        }
                        const result = await this.queueEncryptedEnvelope(stores, envelope, uuidKind);
                        if (result.plaintext) {
                            decrypted.push({
                                plaintext: result.plaintext,
                                envelope: result.envelope,
                                data,
                            });
                        }
                    }
                    catch (error) {
                        failed.push(data);
                        log.error('MessageReceiver.decryptAndCacheBatch error when ' +
                            'processing the envelope', Errors.toLogFormat(error));
                    }
                }));
                log.info('MessageReceiver.decryptAndCacheBatch storing ' +
                    `${decrypted.length} decrypted envelopes, keeping ` +
                    `${failed.length} failed envelopes.`);
                // Store both decrypted and failed unprocessed envelopes
                const unprocesseds = decrypted.map(({ envelope, data, plaintext }) => {
                    return Object.assign(Object.assign({}, data), { source: envelope.source, sourceUuid: envelope.sourceUuid, sourceDevice: envelope.sourceDevice, destinationUuid: envelope.destinationUuid.toString(), serverGuid: envelope.serverGuid, serverTimestamp: envelope.serverTimestamp, decrypted: Bytes.toBase64(plaintext) });
                });
                await storageProtocol.addMultipleUnprocessed(unprocesseds.concat(failed), { zone });
            });
            log.info('MessageReceiver.decryptAndCacheBatch acknowledging receipt');
            // Acknowledge all envelopes
            for (const { request } of items) {
                try {
                    request.respond(200, 'OK');
                }
                catch (error) {
                    log.error('decryptAndCacheBatch: Failed to send 200 to server; still queuing envelope');
                }
            }
        }
        catch (error) {
            log.error('decryptAndCache error trying to add messages to cache:', Errors.toLogFormat(error));
            items.forEach(item => {
                item.request.respond(500, 'Failed to cache message');
            });
            return;
        }
        await Promise.all(decrypted.map(async ({ envelope, plaintext }) => {
            try {
                await this.queueDecryptedEnvelope(envelope, plaintext);
            }
            catch (error) {
                log.error('decryptAndCache error when processing decrypted envelope', Errors.toLogFormat(error));
            }
        }));
        log.info('MessageReceiver.decryptAndCacheBatch fully processed');
        this.maybeScheduleRetryTimeout();
    }
    decryptAndCache(envelope, plaintext, request) {
        const { id } = envelope;
        const data = {
            id,
            version: 2,
            envelope: Bytes.toBase64(plaintext),
            timestamp: envelope.receivedAtCounter,
            attempts: 1,
            messageAgeSec: envelope.messageAgeSec,
        };
        this.decryptAndCacheBatcher.add({
            request,
            envelope,
            data,
        });
    }
    async cacheRemoveBatch(items) {
        await this.storage.protocol.removeUnprocessed(items);
    }
    removeFromCache(envelope) {
        const { id } = envelope;
        this.cacheRemoveBatcher.add(id);
    }
    async queueDecryptedEnvelope(envelope, plaintext) {
        const id = this.getEnvelopeId(envelope);
        log.info('queueing decrypted envelope', id);
        const task = this.handleDecryptedEnvelope.bind(this, envelope, plaintext);
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(task, `queueDecryptedEnvelope ${id}`);
        try {
            await this.addToQueue(taskWithTimeout, TaskType.Decrypted);
        }
        catch (error) {
            log.error(`queueDecryptedEnvelope error handling envelope ${id}:`, Errors.toLogFormat(error));
        }
    }
    async queueEncryptedEnvelope(stores, envelope, uuidKind) {
        let logId = this.getEnvelopeId(envelope);
        log.info(`queueing ${uuidKind} envelope`, logId);
        const task = (0, TaskWithTimeout_1.default)(async () => {
            const unsealedEnvelope = await this.unsealEnvelope(stores, envelope, uuidKind);
            // Dropped early
            if (!unsealedEnvelope) {
                return { plaintext: undefined, envelope };
            }
            logId = this.getEnvelopeId(unsealedEnvelope);
            return this.decryptEnvelope(stores, unsealedEnvelope, uuidKind);
        }, `MessageReceiver: unseal and decrypt ${logId}`);
        try {
            return await this.addToQueue(task, TaskType.Encrypted);
        }
        catch (error) {
            const args = [
                'queueEncryptedEnvelope error handling envelope',
                logId,
                ':',
                Errors.toLogFormat(error),
            ];
            if (error instanceof Errors_1.WarnOnlyError) {
                log.warn(...args);
            }
            else {
                log.error(...args);
            }
            throw error;
        }
    }
    async queueCachedEnvelope(data, envelope) {
        this.decryptAndCacheBatcher.add({
            request: {
                respond(code, status) {
                    log.info('queueCachedEnvelope: fake response ' +
                        `with code ${code} and status ${status}`);
                },
            },
            envelope,
            data,
        });
    }
    // Called after `decryptEnvelope` decrypted the message.
    async handleDecryptedEnvelope(envelope, plaintext) {
        if (this.stoppingProcessing) {
            return;
        }
        if (envelope.content) {
            await this.innerHandleContentMessage(envelope, plaintext);
            return;
        }
        if (envelope.legacyMessage) {
            await this.innerHandleLegacyMessage(envelope, plaintext);
            return;
        }
        this.removeFromCache(envelope);
        throw new Error('Received message with no content and no legacyMessage');
    }
    async unsealEnvelope(stores, envelope, uuidKind) {
        var _a;
        const logId = this.getEnvelopeId(envelope);
        if (this.stoppingProcessing) {
            log.warn(`MessageReceiver.unsealEnvelope(${logId}): dropping`);
            throw new Error('Sealed envelope dropped due to stopping processing');
        }
        if (envelope.type !== protobuf_1.SignalService.Envelope.Type.UNIDENTIFIED_SENDER) {
            return envelope;
        }
        if (uuidKind === UUID_1.UUIDKind.PNI) {
            log.warn(`MessageReceiver.unsealEnvelope(${logId}): dropping for PNI`);
            return undefined;
        }
        (0, assert_1.strictAssert)(uuidKind === UUID_1.UUIDKind.ACI, 'Sealed non-ACI envelope');
        const ciphertext = envelope.content || envelope.legacyMessage;
        if (!ciphertext) {
            this.removeFromCache(envelope);
            throw new Error('Received message with no content and no legacyMessage');
        }
        log.info(`MessageReceiver.unsealEnvelope(${logId}): unidentified message`);
        const messageContent = await (0, signal_client_1.sealedSenderDecryptToUsmc)(Buffer.from(ciphertext), stores.identityKeyStore);
        // Here we take this sender information and attach it back to the envelope
        //   to make the rest of the app work properly.
        const certificate = messageContent.senderCertificate();
        const originalSource = envelope.source;
        const originalSourceUuid = envelope.sourceUuid;
        const newEnvelope = Object.assign(Object.assign({}, envelope), { 
            // Overwrite Envelope fields
            source: (0, dropNull_1.dropNull)(certificate.senderE164()), sourceUuid: (0, normalizeUuid_1.normalizeUuid)(certificate.senderUuid(), 'MessageReceiver.unsealEnvelope.UNIDENTIFIED_SENDER.sourceUuid'), sourceDevice: certificate.senderDeviceId(), 
            // UnsealedEnvelope-only fields
            unidentifiedDeliveryReceived: !(originalSource || originalSourceUuid), contentHint: messageContent.contentHint(), groupId: (_a = messageContent.groupId()) === null || _a === void 0 ? void 0 : _a.toString('base64'), usmc: messageContent, certificate, unsealedContent: messageContent });
        // This will throw if there's a problem
        this.validateUnsealedEnvelope(newEnvelope);
        return newEnvelope;
    }
    async decryptEnvelope(stores, envelope, uuidKind) {
        const logId = this.getEnvelopeId(envelope);
        if (this.stoppingProcessing) {
            log.warn(`MessageReceiver.decryptEnvelope(${logId}): dropping unsealed`);
            throw new Error('Unsealed envelope dropped due to stopping processing');
        }
        if (envelope.type === protobuf_1.SignalService.Envelope.Type.RECEIPT) {
            await this.onDeliveryReceipt(envelope);
            return { plaintext: undefined, envelope };
        }
        let ciphertext;
        let isLegacy = false;
        if (envelope.content) {
            ciphertext = envelope.content;
        }
        else if (envelope.legacyMessage) {
            ciphertext = envelope.legacyMessage;
            isLegacy = true;
        }
        else {
            this.removeFromCache(envelope);
            (0, assert_1.strictAssert)(false, 'Contentless envelope should be handled by unsealEnvelope');
        }
        log.info(`MessageReceiver.decryptEnvelope(${logId})${isLegacy ? ' (legacy)' : ''}`);
        const plaintext = await this.decrypt(stores, envelope, ciphertext, uuidKind);
        if (!plaintext) {
            log.warn('MessageReceiver.decryptEnvelope: plaintext was falsey');
            return { plaintext, envelope };
        }
        // Legacy envelopes do not carry senderKeyDistributionMessage
        if (isLegacy) {
            return { plaintext, envelope };
        }
        // Note: we need to process this as part of decryption, because we might need this
        //   sender key to decrypt the next message in the queue!
        try {
            const content = protobuf_1.SignalService.Content.decode(plaintext);
            if (content.senderKeyDistributionMessage &&
                Bytes.isNotEmpty(content.senderKeyDistributionMessage)) {
                await this.handleSenderKeyDistributionMessage(stores, envelope, content.senderKeyDistributionMessage);
            }
        }
        catch (error) {
            log.error('MessageReceiver.decryptEnvelope: Failed to process sender ' +
                `key distribution message: ${Errors.toLogFormat(error)}`);
        }
        if ((envelope.source && this.isBlocked(envelope.source)) ||
            (envelope.sourceUuid && this.isUuidBlocked(envelope.sourceUuid))) {
            log.info('MessageReceiver.decryptEnvelope: Dropping message from blocked sender');
            return { plaintext: undefined, envelope };
        }
        return { plaintext, envelope };
    }
    validateUnsealedEnvelope(envelope) {
        const { unsealedContent: messageContent, certificate } = envelope;
        (0, assert_1.strictAssert)(messageContent !== undefined, 'Missing message content for sealed sender message');
        (0, assert_1.strictAssert)(certificate !== undefined, 'Missing sender certificate for sealed sender message');
        if (!envelope.serverTimestamp) {
            throw new Error('MessageReceiver.decryptSealedSender: ' +
                'Sealed sender message was missing serverTimestamp');
        }
        const serverCertificate = certificate.serverCertificate();
        if (!(0, Curve_1.verifySignature)(this.serverTrustRoot, serverCertificate.certificateData(), serverCertificate.signature())) {
            throw new Error('MessageReceiver.validateUnsealedEnvelope: ' +
                'Server certificate trust root validation failed');
        }
        if (!(0, Curve_1.verifySignature)(serverCertificate.key().serialize(), certificate.certificate(), certificate.signature())) {
            throw new Error('MessageReceiver.validateUnsealedEnvelope: ' +
                'Server certificate server signature validation failed');
        }
        const logId = this.getEnvelopeId(envelope);
        if (envelope.serverTimestamp > certificate.expiration()) {
            throw new Error('MessageReceiver.validateUnsealedEnvelope: ' +
                `Sender certificate is expired for envelope ${logId}`);
        }
        return undefined;
    }
    async onDeliveryReceipt(envelope) {
        await this.dispatchAndWait(new messageReceiverEvents_1.DeliveryEvent({
            timestamp: envelope.timestamp,
            source: envelope.source,
            sourceUuid: envelope.sourceUuid,
            sourceDevice: envelope.sourceDevice,
        }, this.removeFromCache.bind(this, envelope)));
    }
    unpad(paddedPlaintext) {
        for (let i = paddedPlaintext.length - 1; i >= 0; i -= 1) {
            if (paddedPlaintext[i] === 0x80) {
                return new Uint8Array(paddedPlaintext.slice(0, i));
            }
            if (paddedPlaintext[i] !== 0x00) {
                throw new Error('Invalid padding');
            }
        }
        return paddedPlaintext;
    }
    async decryptSealedSender({ sessionStore, identityKeyStore, zone }, envelope, ciphertext) {
        const localE164 = this.storage.user.getNumber();
        const { destinationUuid } = envelope;
        const localDeviceId = (0, parseIntOrThrow_1.parseIntOrThrow)(this.storage.user.getDeviceId(), 'MessageReceiver.decryptSealedSender: localDeviceId');
        const logId = this.getEnvelopeId(envelope);
        const { unsealedContent: messageContent, certificate } = envelope;
        (0, assert_1.strictAssert)(messageContent !== undefined, 'Missing message content for sealed sender message');
        (0, assert_1.strictAssert)(certificate !== undefined, 'Missing sender certificate for sealed sender message');
        const unidentifiedSenderTypeEnum = protobuf_1.SignalService.UnidentifiedSenderMessage.Message.Type;
        if (messageContent.msgType() === unidentifiedSenderTypeEnum.PLAINTEXT_CONTENT) {
            log.info(`MessageReceiver.decryptSealedSender(${logId}): ` +
                'unidentified message/plaintext contents');
            const plaintextContent = signal_client_1.PlaintextContent.deserialize(messageContent.contents());
            return {
                plaintext: plaintextContent.body(),
            };
        }
        if (messageContent.msgType() === unidentifiedSenderTypeEnum.SENDERKEY_MESSAGE) {
            log.info(`MessageReceiver.decryptSealedSender(${logId}): ` +
                'unidentified message/sender key contents');
            const sealedSenderIdentifier = certificate.senderUuid();
            const sealedSenderSourceDevice = certificate.senderDeviceId();
            const senderKeyStore = new LibSignalStores_1.SenderKeys({ ourUuid: destinationUuid });
            const address = new QualifiedAddress_1.QualifiedAddress(destinationUuid, Address_1.Address.create(sealedSenderIdentifier, sealedSenderSourceDevice));
            const plaintext = await this.storage.protocol.enqueueSenderKeyJob(address, () => (0, signal_client_1.groupDecrypt)(signal_client_1.ProtocolAddress.new(sealedSenderIdentifier, sealedSenderSourceDevice), senderKeyStore, messageContent.contents()), zone);
            return { plaintext };
        }
        log.info(`MessageReceiver.decryptSealedSender(${logId}): ` +
            'unidentified message/passing to sealedSenderDecryptMessage');
        const preKeyStore = new LibSignalStores_1.PreKeys({ ourUuid: destinationUuid });
        const signedPreKeyStore = new LibSignalStores_1.SignedPreKeys({ ourUuid: destinationUuid });
        const sealedSenderIdentifier = envelope.sourceUuid;
        (0, assert_1.strictAssert)(sealedSenderIdentifier !== undefined, 'Empty sealed sender identifier');
        (0, assert_1.strictAssert)(envelope.sourceDevice !== undefined, 'Empty sealed sender device');
        const address = new QualifiedAddress_1.QualifiedAddress(destinationUuid, Address_1.Address.create(sealedSenderIdentifier, envelope.sourceDevice));
        const unsealedPlaintext = await this.storage.protocol.enqueueSessionJob(address, () => (0, signal_client_1.sealedSenderDecryptMessage)(Buffer.from(ciphertext), signal_client_1.PublicKey.deserialize(Buffer.from(this.serverTrustRoot)), envelope.serverTimestamp, localE164 || null, destinationUuid.toString(), localDeviceId, sessionStore, identityKeyStore, preKeyStore, signedPreKeyStore), zone);
        return { unsealedPlaintext };
    }
    async innerDecrypt(stores, envelope, ciphertext, uuidKind) {
        const { sessionStore, identityKeyStore, zone } = stores;
        const logId = this.getEnvelopeId(envelope);
        const envelopeTypeEnum = protobuf_1.SignalService.Envelope.Type;
        const identifier = envelope.sourceUuid;
        const { sourceDevice } = envelope;
        const { destinationUuid } = envelope;
        const preKeyStore = new LibSignalStores_1.PreKeys({ ourUuid: destinationUuid });
        const signedPreKeyStore = new LibSignalStores_1.SignedPreKeys({ ourUuid: destinationUuid });
        (0, assert_1.strictAssert)(identifier !== undefined, 'Empty identifier');
        (0, assert_1.strictAssert)(sourceDevice !== undefined, 'Empty source device');
        const address = new QualifiedAddress_1.QualifiedAddress(destinationUuid, Address_1.Address.create(identifier, sourceDevice));
        if (uuidKind === UUID_1.UUIDKind.PNI &&
            envelope.type !== envelopeTypeEnum.PREKEY_BUNDLE) {
            log.warn(`MessageReceiver.innerDecrypt(${logId}): ` +
                'non-PreKey envelope on PNI');
            return undefined;
        }
        (0, assert_1.strictAssert)(uuidKind === UUID_1.UUIDKind.PNI || uuidKind === UUID_1.UUIDKind.ACI, `Unsupported uuidKind: ${uuidKind}`);
        if (envelope.type === envelopeTypeEnum.PLAINTEXT_CONTENT) {
            log.info(`decrypt/${logId}: plaintext message`);
            const buffer = Buffer.from(ciphertext);
            const plaintextContent = signal_client_1.PlaintextContent.deserialize(buffer);
            return this.unpad(plaintextContent.body());
        }
        if (envelope.type === envelopeTypeEnum.CIPHERTEXT) {
            log.info(`decrypt/${logId}: ciphertext message`);
            if (!identifier) {
                throw new Error('MessageReceiver.innerDecrypt: No identifier for CIPHERTEXT message');
            }
            if (!sourceDevice) {
                throw new Error('MessageReceiver.innerDecrypt: No sourceDevice for CIPHERTEXT message');
            }
            const signalMessage = signal_client_1.SignalMessage.deserialize(Buffer.from(ciphertext));
            const plaintext = await this.storage.protocol.enqueueSessionJob(address, async () => this.unpad(await (0, signal_client_1.signalDecrypt)(signalMessage, signal_client_1.ProtocolAddress.new(identifier, sourceDevice), sessionStore, identityKeyStore)), zone);
            return plaintext;
        }
        if (envelope.type === envelopeTypeEnum.PREKEY_BUNDLE) {
            log.info(`decrypt/${logId}: prekey message`);
            if (!identifier) {
                throw new Error('MessageReceiver.innerDecrypt: No identifier for PREKEY_BUNDLE message');
            }
            if (!sourceDevice) {
                throw new Error('MessageReceiver.innerDecrypt: No sourceDevice for PREKEY_BUNDLE message');
            }
            const preKeySignalMessage = signal_client_1.PreKeySignalMessage.deserialize(Buffer.from(ciphertext));
            const plaintext = await this.storage.protocol.enqueueSessionJob(address, async () => this.unpad(await (0, signal_client_1.signalDecryptPreKey)(preKeySignalMessage, signal_client_1.ProtocolAddress.new(identifier, sourceDevice), sessionStore, identityKeyStore, preKeyStore, signedPreKeyStore)), zone);
            return plaintext;
        }
        if (envelope.type === envelopeTypeEnum.UNIDENTIFIED_SENDER) {
            log.info(`decrypt/${logId}: unidentified message`);
            const { plaintext, unsealedPlaintext } = await this.decryptSealedSender(stores, envelope, ciphertext);
            if (plaintext) {
                return this.unpad(plaintext);
            }
            if (unsealedPlaintext) {
                const content = unsealedPlaintext.message();
                if (!content) {
                    throw new Error('MessageReceiver.innerDecrypt: Content returned was falsey!');
                }
                // Return just the content because that matches the signature of the other
                //   decrypt methods used above.
                return this.unpad(content);
            }
            throw new Error('Unexpected lack of plaintext from unidentified sender');
        }
        throw new Error('Unknown message type');
    }
    async decrypt(stores, envelope, ciphertext, uuidKind) {
        var _a, _b;
        try {
            return await this.innerDecrypt(stores, envelope, ciphertext, uuidKind);
        }
        catch (error) {
            const uuid = envelope.sourceUuid;
            const deviceId = envelope.sourceDevice;
            // We don't do anything if it's just a duplicated message
            if (((_a = error === null || error === void 0 ? void 0 : error.message) === null || _a === void 0 ? void 0 : _a.includes) &&
                error.message.includes('message with old counter')) {
                this.removeFromCache(envelope);
                throw error;
            }
            // We don't do a light session reset if it's an error with the sealed sender
            //   wrapper, since we don't trust the sender information.
            if (((_b = error === null || error === void 0 ? void 0 : error.message) === null || _b === void 0 ? void 0 : _b.includes) &&
                error.message.includes('trust root validation failed')) {
                this.removeFromCache(envelope);
                throw error;
            }
            if ((envelope.source && this.isBlocked(envelope.source)) ||
                (envelope.sourceUuid && this.isUuidBlocked(envelope.sourceUuid))) {
                log.info('MessageReceiver.decrypt: Error from blocked sender; no further processing');
                this.removeFromCache(envelope);
                throw error;
            }
            if (uuid && deviceId) {
                const { usmc } = envelope;
                const event = new messageReceiverEvents_1.DecryptionErrorEvent({
                    cipherTextBytes: usmc ? usmc.contents() : undefined,
                    cipherTextType: usmc ? usmc.msgType() : undefined,
                    contentHint: envelope.contentHint,
                    groupId: envelope.groupId,
                    receivedAtCounter: envelope.receivedAtCounter,
                    receivedAtDate: envelope.receivedAtDate,
                    senderDevice: deviceId,
                    senderUuid: uuid,
                    timestamp: envelope.timestamp,
                }, () => this.removeFromCache(envelope));
                // Avoid deadlocks by scheduling processing on decrypted queue
                this.addToQueue(async () => this.dispatchEvent(event), TaskType.Decrypted);
            }
            else {
                const envelopeId = this.getEnvelopeId(envelope);
                this.removeFromCache(envelope);
                log.error(`MessageReceiver.decrypt: Envelope ${envelopeId} missing uuid or deviceId`);
            }
            throw error;
        }
    }
    async handleSentMessage(envelope, sentContainer) {
        log.info('MessageReceiver.handleSentMessage', this.getEnvelopeId(envelope));
        const { destination, destinationUuid, timestamp, message: msg, expirationStartTimestamp, unidentifiedStatus, isRecipientUpdate, } = sentContainer;
        if (!msg) {
            throw new Error('MessageReceiver.handleSentMessage: message was falsey!');
        }
        let p = Promise.resolve();
        // eslint-disable-next-line no-bitwise
        if (msg.flags && msg.flags & protobuf_1.SignalService.DataMessage.Flags.END_SESSION) {
            if (destinationUuid) {
                p = this.handleEndSession(new UUID_1.UUID(destinationUuid));
            }
            else if (destination) {
                const theirUuid = UUID_1.UUID.lookup(destination);
                if (theirUuid) {
                    p = this.handleEndSession(theirUuid);
                }
                else {
                    log.warn(`handleSentMessage: uuid not found for ${destination}`);
                    p = Promise.resolve();
                }
            }
            else {
                throw new Error('MessageReceiver.handleSentMessage: Cannot end session with falsey destination');
            }
        }
        await p;
        const message = await this.processDecrypted(envelope, msg);
        const groupId = this.getProcessedGroupId(message);
        const isBlocked = groupId ? this.isGroupBlocked(groupId) : false;
        const { source, sourceUuid } = envelope;
        const ourE164 = this.storage.user.getNumber();
        const ourUuid = this.storage.user.getCheckedUuid().toString();
        const isMe = (source && ourE164 && source === ourE164) ||
            (sourceUuid && ourUuid && sourceUuid === ourUuid);
        const isLeavingGroup = Boolean(!message.groupV2 &&
            message.group &&
            message.group.type === protobuf_1.SignalService.GroupContext.Type.QUIT);
        if (groupId && isBlocked && !(isMe && isLeavingGroup)) {
            log.warn(`Message ${this.getEnvelopeId(envelope)} ignored; destined for blocked group`);
            this.removeFromCache(envelope);
            return undefined;
        }
        const ev = new messageReceiverEvents_1.SentEvent({
            destination: (0, dropNull_1.dropNull)(destination),
            destinationUuid: (0, dropNull_1.dropNull)(destinationUuid),
            timestamp: timestamp ? (0, normalizeNumber_1.normalizeNumber)(timestamp) : undefined,
            serverTimestamp: envelope.serverTimestamp,
            device: envelope.sourceDevice,
            unidentifiedStatus,
            message,
            isRecipientUpdate: Boolean(isRecipientUpdate),
            receivedAtCounter: envelope.receivedAtCounter,
            receivedAtDate: envelope.receivedAtDate,
            expirationStartTimestamp: expirationStartTimestamp
                ? (0, normalizeNumber_1.normalizeNumber)(expirationStartTimestamp)
                : undefined,
        }, this.removeFromCache.bind(this, envelope));
        return this.dispatchAndWait(ev);
    }
    async handleDataMessage(envelope, msg) {
        const logId = this.getEnvelopeId(envelope);
        log.info('MessageReceiver.handleDataMessage', logId);
        if (msg.storyContext) {
            log.info(`MessageReceiver.handleDataMessage/${logId}: Dropping incoming dataMessage with storyContext field`);
            this.removeFromCache(envelope);
            return undefined;
        }
        let p = Promise.resolve();
        // eslint-disable-next-line no-bitwise
        const destination = envelope.sourceUuid;
        if (!destination) {
            throw new Error('MessageReceiver.handleDataMessage: source and sourceUuid were falsey');
        }
        if (this.isInvalidGroupData(msg, envelope)) {
            this.removeFromCache(envelope);
            return undefined;
        }
        await this.checkGroupV1Data(msg);
        if (msg.flags && msg.flags & protobuf_1.SignalService.DataMessage.Flags.END_SESSION) {
            p = this.handleEndSession(new UUID_1.UUID(destination));
        }
        if (msg.flags && msg.flags & protobuf_1.SignalService.DataMessage.Flags.PROFILE_KEY_UPDATE) {
            (0, assert_1.strictAssert)(msg.profileKey, 'PROFILE_KEY_UPDATE without profileKey');
            const ev = new messageReceiverEvents_1.ProfileKeyUpdateEvent({
                source: envelope.source,
                sourceUuid: envelope.sourceUuid,
                profileKey: Bytes.toBase64(msg.profileKey),
            }, this.removeFromCache.bind(this, envelope));
            return this.dispatchAndWait(ev);
        }
        await p;
        const message = await this.processDecrypted(envelope, msg);
        const groupId = this.getProcessedGroupId(message);
        const isBlocked = groupId ? this.isGroupBlocked(groupId) : false;
        const { source, sourceUuid } = envelope;
        const ourE164 = this.storage.user.getNumber();
        const ourUuid = this.storage.user.getCheckedUuid().toString();
        const isMe = (source && ourE164 && source === ourE164) ||
            (sourceUuid && ourUuid && sourceUuid === ourUuid);
        const isLeavingGroup = Boolean(!message.groupV2 &&
            message.group &&
            message.group.type === protobuf_1.SignalService.GroupContext.Type.QUIT);
        if (groupId && isBlocked && !(isMe && isLeavingGroup)) {
            log.warn(`Message ${this.getEnvelopeId(envelope)} ignored; destined for blocked group`);
            this.removeFromCache(envelope);
            return undefined;
        }
        const ev = new messageReceiverEvents_1.MessageEvent({
            source: envelope.source,
            sourceUuid: envelope.sourceUuid,
            sourceDevice: envelope.sourceDevice,
            timestamp: envelope.timestamp,
            serverGuid: envelope.serverGuid,
            serverTimestamp: envelope.serverTimestamp,
            unidentifiedDeliveryReceived: Boolean(envelope.unidentifiedDeliveryReceived),
            message,
            receivedAtCounter: envelope.receivedAtCounter,
            receivedAtDate: envelope.receivedAtDate,
        }, this.removeFromCache.bind(this, envelope));
        return this.dispatchAndWait(ev);
    }
    async innerHandleLegacyMessage(envelope, plaintext) {
        const message = protobuf_1.SignalService.DataMessage.decode(plaintext);
        return this.handleDataMessage(envelope, message);
    }
    async maybeUpdateTimestamp(envelope) {
        const { retryPlaceholders } = window.Signal.Services;
        if (!retryPlaceholders) {
            log.warn('maybeUpdateTimestamp: retry placeholders not available!');
            return envelope;
        }
        const { timestamp } = envelope;
        const identifier = envelope.groupId || envelope.sourceUuid;
        const conversation = window.ConversationController.get(identifier);
        try {
            if (!conversation) {
                log.info(`maybeUpdateTimestamp/${timestamp}: No conversation found for identifier ${identifier}`);
                return envelope;
            }
            const logId = `${conversation.idForLogging()}/${timestamp}`;
            const item = await retryPlaceholders.findByMessageAndRemove(conversation.id, timestamp);
            if (item && item.wasOpened) {
                log.info(`maybeUpdateTimestamp/${logId}: found retry placeholder, but conversation was opened. No updates made.`);
            }
            else if (item) {
                log.info(`maybeUpdateTimestamp/${logId}: found retry placeholder. Updating receivedAtCounter/receivedAtDate`);
                return Object.assign(Object.assign({}, envelope), { receivedAtCounter: item.receivedAtCounter, receivedAtDate: item.receivedAt });
            }
        }
        catch (error) {
            log.error(`maybeUpdateTimestamp/${timestamp}: Failed to process message: ${Errors.toLogFormat(error)}`);
        }
        return envelope;
    }
    async innerHandleContentMessage(incomingEnvelope, plaintext) {
        const content = protobuf_1.SignalService.Content.decode(plaintext);
        const envelope = await this.maybeUpdateTimestamp(incomingEnvelope);
        if (content.decryptionErrorMessage &&
            Bytes.isNotEmpty(content.decryptionErrorMessage)) {
            await this.handleDecryptionError(envelope, content.decryptionErrorMessage);
            return;
        }
        if (content.syncMessage) {
            await this.handleSyncMessage(envelope, (0, processSyncMessage_1.processSyncMessage)(content.syncMessage));
            return;
        }
        if (content.dataMessage) {
            await this.handleDataMessage(envelope, content.dataMessage);
            return;
        }
        if (content.nullMessage) {
            await this.handleNullMessage(envelope);
            return;
        }
        if (content.callingMessage) {
            await this.handleCallingMessage(envelope, content.callingMessage);
            return;
        }
        if (content.receiptMessage) {
            await this.handleReceiptMessage(envelope, content.receiptMessage);
            return;
        }
        if (content.typingMessage) {
            await this.handleTypingMessage(envelope, content.typingMessage);
            return;
        }
        if (content.storyMessage) {
            const logId = this.getEnvelopeId(envelope);
            log.info(`innerHandleContentMessage/${logId}: Dropping incoming message with storyMessage field`);
            this.removeFromCache(envelope);
            return;
        }
        this.removeFromCache(envelope);
        if (Bytes.isEmpty(content.senderKeyDistributionMessage)) {
            throw new Error('Unsupported content message');
        }
    }
    async handleDecryptionError(envelope, decryptionError) {
        const logId = this.getEnvelopeId(envelope);
        log.info(`handleDecryptionError: ${logId}`);
        const buffer = Buffer.from(decryptionError);
        const request = signal_client_1.DecryptionErrorMessage.deserialize(buffer);
        const { sourceUuid, sourceDevice } = envelope;
        if (!sourceUuid || !sourceDevice) {
            log.error(`handleDecryptionError/${logId}: Missing uuid or device!`);
            this.removeFromCache(envelope);
            return;
        }
        const event = new messageReceiverEvents_1.RetryRequestEvent({
            groupId: envelope.groupId,
            requesterDevice: sourceDevice,
            requesterUuid: sourceUuid,
            ratchetKey: request.ratchetKey(),
            senderDevice: request.deviceId(),
            sentAt: request.timestamp(),
        }, () => this.removeFromCache(envelope));
        await this.dispatchEvent(event);
    }
    async handleSenderKeyDistributionMessage(stores, envelope, distributionMessage) {
        const envelopeId = this.getEnvelopeId(envelope);
        log.info(`handleSenderKeyDistributionMessage/${envelopeId}`);
        // Note: we don't call removeFromCache here because this message can be combined
        //   with a dataMessage, for example. That processing will dictate cache removal.
        const identifier = envelope.sourceUuid;
        const { sourceDevice } = envelope;
        if (!identifier) {
            throw new Error(`handleSenderKeyDistributionMessage: No identifier for envelope ${envelopeId}`);
        }
        if (!(0, lodash_1.isNumber)(sourceDevice)) {
            throw new Error(`handleSenderKeyDistributionMessage: Missing sourceDevice for envelope ${envelopeId}`);
        }
        const sender = signal_client_1.ProtocolAddress.new(identifier, sourceDevice);
        const senderKeyDistributionMessage = signal_client_1.SenderKeyDistributionMessage.deserialize(Buffer.from(distributionMessage));
        const { destinationUuid } = envelope;
        const senderKeyStore = new LibSignalStores_1.SenderKeys({ ourUuid: destinationUuid });
        const address = new QualifiedAddress_1.QualifiedAddress(destinationUuid, Address_1.Address.create(identifier, sourceDevice));
        await this.storage.protocol.enqueueSenderKeyJob(address, () => (0, signal_client_1.processSenderKeyDistributionMessage)(sender, senderKeyDistributionMessage, senderKeyStore), stores.zone);
    }
    async handleCallingMessage(envelope, callingMessage) {
        this.removeFromCache(envelope);
        await window.Signal.Services.calling.handleCallingMessage(envelope, callingMessage);
    }
    async handleReceiptMessage(envelope, receiptMessage) {
        (0, assert_1.strictAssert)(receiptMessage.timestamp, 'Receipt message without timestamp');
        let EventClass;
        switch (receiptMessage.type) {
            case protobuf_1.SignalService.ReceiptMessage.Type.DELIVERY:
                EventClass = messageReceiverEvents_1.DeliveryEvent;
                break;
            case protobuf_1.SignalService.ReceiptMessage.Type.READ:
                EventClass = messageReceiverEvents_1.ReadEvent;
                break;
            case protobuf_1.SignalService.ReceiptMessage.Type.VIEWED:
                EventClass = messageReceiverEvents_1.ViewEvent;
                break;
            default:
                // This can happen if we get a receipt type we don't know about yet, which
                //   is totally fine.
                return;
        }
        await Promise.all(receiptMessage.timestamp.map(async (rawTimestamp) => {
            const ev = new EventClass({
                timestamp: (0, normalizeNumber_1.normalizeNumber)(rawTimestamp),
                envelopeTimestamp: envelope.timestamp,
                source: envelope.source,
                sourceUuid: envelope.sourceUuid,
                sourceDevice: envelope.sourceDevice,
            }, this.removeFromCache.bind(this, envelope));
            await this.dispatchAndWait(ev);
        }));
    }
    async handleTypingMessage(envelope, typingMessage) {
        this.removeFromCache(envelope);
        if (envelope.timestamp && typingMessage.timestamp) {
            const envelopeTimestamp = envelope.timestamp;
            const typingTimestamp = (0, normalizeNumber_1.normalizeNumber)(typingMessage.timestamp);
            if (typingTimestamp !== envelopeTimestamp) {
                log.warn(`Typing message envelope timestamp (${envelopeTimestamp}) did not match typing timestamp (${typingTimestamp})`);
                return;
            }
        }
        (0, assert_1.strictAssert)(envelope.sourceDevice !== undefined, 'TypingMessage requires sourceDevice in the envelope');
        const { groupId, timestamp, action } = typingMessage;
        let groupIdString;
        let groupV2IdString;
        if (groupId && groupId.byteLength > 0) {
            if (groupId.byteLength === GROUPV1_ID_LENGTH) {
                groupIdString = Bytes.toBinary(groupId);
                groupV2IdString = this.deriveGroupV2FromV1(groupId);
            }
            else if (groupId.byteLength === GROUPV2_ID_LENGTH) {
                groupV2IdString = Bytes.toBase64(groupId);
            }
            else {
                log.error('handleTypingMessage: Received invalid groupId value');
            }
        }
        await this.dispatchEvent(new messageReceiverEvents_1.TypingEvent({
            sender: envelope.source,
            senderUuid: envelope.sourceUuid,
            senderDevice: envelope.sourceDevice,
            typing: {
                typingMessage,
                timestamp: timestamp ? (0, normalizeNumber_1.normalizeNumber)(timestamp) : Date.now(),
                started: action === protobuf_1.SignalService.TypingMessage.Action.STARTED,
                stopped: action === protobuf_1.SignalService.TypingMessage.Action.STOPPED,
                groupId: groupIdString,
                groupV2Id: groupV2IdString,
            },
        }));
    }
    handleNullMessage(envelope) {
        log.info('MessageReceiver.handleNullMessage', this.getEnvelopeId(envelope));
        this.removeFromCache(envelope);
    }
    isInvalidGroupData(message, envelope) {
        const { group, groupV2 } = message;
        if (group) {
            const { id } = group;
            (0, assert_1.strictAssert)(id, 'Group data has no id');
            const isInvalid = id.byteLength !== GROUPV1_ID_LENGTH;
            if (isInvalid) {
                log.info('isInvalidGroupData: invalid GroupV1 message from', this.getEnvelopeId(envelope));
            }
            return isInvalid;
        }
        if (groupV2) {
            const { masterKey } = groupV2;
            (0, assert_1.strictAssert)(masterKey, 'Group v2 data has no masterKey');
            const isInvalid = masterKey.byteLength !== groups_1.MASTER_KEY_LENGTH;
            if (isInvalid) {
                log.info('isInvalidGroupData: invalid GroupV2 message from', this.getEnvelopeId(envelope));
            }
            return isInvalid;
        }
        return false;
    }
    deriveGroupV2FromV1(groupId) {
        if (groupId.byteLength !== GROUPV1_ID_LENGTH) {
            throw new Error(`deriveGroupV2FromV1: had id with wrong byteLength: ${groupId.byteLength}`);
        }
        const masterKey = (0, Crypto_1.deriveMasterKeyFromGroupV1)(groupId);
        const data = (0, groups_1.deriveGroupFields)(masterKey);
        return Bytes.toBase64(data.id);
    }
    async checkGroupV1Data(message) {
        const { group } = message;
        if (!group) {
            return;
        }
        if (!group.id) {
            throw new Error('deriveGroupV1Data: had falsey id');
        }
        const { id } = group;
        if (id.byteLength !== GROUPV1_ID_LENGTH) {
            throw new Error(`deriveGroupV1Data: had id with wrong byteLength: ${id.byteLength}`);
        }
    }
    getProcessedGroupId(message) {
        if (message.groupV2) {
            return message.groupV2.id;
        }
        if (message.group && message.group.id) {
            return message.group.id;
        }
        return undefined;
    }
    getGroupId(message) {
        if (message.groupV2) {
            (0, assert_1.strictAssert)(message.groupV2.masterKey, 'Missing groupV2.masterKey');
            const { id } = (0, groups_1.deriveGroupFields)(message.groupV2.masterKey);
            return Bytes.toBase64(id);
        }
        if (message.group && message.group.id) {
            return Bytes.toBinary(message.group.id);
        }
        return undefined;
    }
    getDestination(sentMessage) {
        if (sentMessage.message && sentMessage.message.groupV2) {
            return `groupv2(${this.getGroupId(sentMessage.message)})`;
        }
        if (sentMessage.message && sentMessage.message.group) {
            (0, assert_1.strictAssert)(sentMessage.message.group.id, 'group without id');
            return `group(${this.getGroupId(sentMessage.message)})`;
        }
        return sentMessage.destination || sentMessage.destinationUuid;
    }
    async handleSyncMessage(envelope, syncMessage) {
        const ourNumber = this.storage.user.getNumber();
        const ourUuid = this.storage.user.getCheckedUuid();
        const fromSelfSource = envelope.source && envelope.source === ourNumber;
        const fromSelfSourceUuid = envelope.sourceUuid && envelope.sourceUuid === ourUuid.toString();
        if (!fromSelfSource && !fromSelfSourceUuid) {
            throw new Error('Received sync message from another number');
        }
        const ourDeviceId = this.storage.user.getDeviceId();
        // eslint-disable-next-line eqeqeq
        if (envelope.sourceDevice == ourDeviceId) {
            throw new Error('Received sync message from our own device');
        }
        if (syncMessage.sent) {
            const sentMessage = syncMessage.sent;
            if (!sentMessage || !sentMessage.message) {
                throw new Error('MessageReceiver.handleSyncMessage: sync sent message was missing message');
            }
            if (this.isInvalidGroupData(sentMessage.message, envelope)) {
                this.removeFromCache(envelope);
                return undefined;
            }
            await this.checkGroupV1Data(sentMessage.message);
            (0, assert_1.strictAssert)(sentMessage.timestamp, 'sent message without timestamp');
            log.info('sent message to', this.getDestination(sentMessage), (0, normalizeNumber_1.normalizeNumber)(sentMessage.timestamp), 'from', this.getEnvelopeId(envelope));
            return this.handleSentMessage(envelope, sentMessage);
        }
        if (syncMessage.contacts) {
            this.handleContacts(envelope, syncMessage.contacts);
            return undefined;
        }
        if (syncMessage.groups) {
            this.handleGroups(envelope, syncMessage.groups);
            return undefined;
        }
        if (syncMessage.blocked) {
            return this.handleBlocked(envelope, syncMessage.blocked);
        }
        if (syncMessage.request) {
            log.info('Got SyncMessage Request');
            this.removeFromCache(envelope);
            return undefined;
        }
        if (syncMessage.read && syncMessage.read.length) {
            return this.handleRead(envelope, syncMessage.read);
        }
        if (syncMessage.verified) {
            return this.handleVerified(envelope, syncMessage.verified);
        }
        if (syncMessage.configuration) {
            return this.handleConfiguration(envelope, syncMessage.configuration);
        }
        if (syncMessage.stickerPackOperation &&
            syncMessage.stickerPackOperation.length > 0) {
            return this.handleStickerPackOperation(envelope, syncMessage.stickerPackOperation);
        }
        if (syncMessage.viewOnceOpen) {
            return this.handleViewOnceOpen(envelope, syncMessage.viewOnceOpen);
        }
        if (syncMessage.messageRequestResponse) {
            return this.handleMessageRequestResponse(envelope, syncMessage.messageRequestResponse);
        }
        if (syncMessage.fetchLatest) {
            return this.handleFetchLatest(envelope, syncMessage.fetchLatest);
        }
        if (syncMessage.keys) {
            return this.handleKeys(envelope, syncMessage.keys);
        }
        if (syncMessage.viewed && syncMessage.viewed.length) {
            return this.handleViewed(envelope, syncMessage.viewed);
        }
        this.removeFromCache(envelope);
        log.warn(`handleSyncMessage/${this.getEnvelopeId(envelope)}: Got empty SyncMessage`);
        return Promise.resolve();
    }
    async handleConfiguration(envelope, configuration) {
        log.info('got configuration sync message');
        const ev = new messageReceiverEvents_1.ConfigurationEvent(configuration, this.removeFromCache.bind(this, envelope));
        return this.dispatchAndWait(ev);
    }
    async handleViewOnceOpen(envelope, sync) {
        log.info('got view once open sync message');
        const ev = new messageReceiverEvents_1.ViewOnceOpenSyncEvent({
            source: (0, dropNull_1.dropNull)(sync.sender),
            sourceUuid: sync.senderUuid
                ? (0, normalizeUuid_1.normalizeUuid)(sync.senderUuid, 'handleViewOnceOpen.senderUuid')
                : undefined,
            timestamp: sync.timestamp ? (0, normalizeNumber_1.normalizeNumber)(sync.timestamp) : undefined,
        }, this.removeFromCache.bind(this, envelope));
        return this.dispatchAndWait(ev);
    }
    async handleMessageRequestResponse(envelope, sync) {
        log.info('got message request response sync message');
        const { groupId } = sync;
        let groupIdString;
        let groupV2IdString;
        if (groupId && groupId.byteLength > 0) {
            if (groupId.byteLength === GROUPV1_ID_LENGTH) {
                groupIdString = Bytes.toBinary(groupId);
                groupV2IdString = this.deriveGroupV2FromV1(groupId);
            }
            else if (groupId.byteLength === GROUPV2_ID_LENGTH) {
                groupV2IdString = Bytes.toBase64(groupId);
            }
            else {
                this.removeFromCache(envelope);
                log.error('Received message request with invalid groupId');
                return undefined;
            }
        }
        const ev = new messageReceiverEvents_1.MessageRequestResponseEvent({
            threadE164: (0, dropNull_1.dropNull)(sync.threadE164),
            threadUuid: sync.threadUuid
                ? (0, normalizeUuid_1.normalizeUuid)(sync.threadUuid, 'handleMessageRequestResponse.threadUuid')
                : undefined,
            messageRequestResponseType: sync.type,
            groupId: groupIdString,
            groupV2Id: groupV2IdString,
        }, this.removeFromCache.bind(this, envelope));
        return this.dispatchAndWait(ev);
    }
    async handleFetchLatest(envelope, sync) {
        log.info('got fetch latest sync message');
        const ev = new messageReceiverEvents_1.FetchLatestEvent(sync.type, this.removeFromCache.bind(this, envelope));
        return this.dispatchAndWait(ev);
    }
    async handleKeys(envelope, sync) {
        log.info('got keys sync message');
        if (!sync.storageService) {
            return undefined;
        }
        const ev = new messageReceiverEvents_1.KeysEvent(sync.storageService, this.removeFromCache.bind(this, envelope));
        return this.dispatchAndWait(ev);
    }
    async handleStickerPackOperation(envelope, operations) {
        const ENUM = protobuf_1.SignalService.SyncMessage.StickerPackOperation.Type;
        log.info('got sticker pack operation sync message');
        const stickerPacks = operations.map(operation => ({
            id: operation.packId ? Bytes.toHex(operation.packId) : undefined,
            key: operation.packKey ? Bytes.toBase64(operation.packKey) : undefined,
            isInstall: operation.type === ENUM.INSTALL,
            isRemove: operation.type === ENUM.REMOVE,
        }));
        const ev = new messageReceiverEvents_1.StickerPackEvent(stickerPacks, this.removeFromCache.bind(this, envelope));
        return this.dispatchAndWait(ev);
    }
    async handleVerified(envelope, verified) {
        const ev = new messageReceiverEvents_1.VerifiedEvent({
            state: verified.state,
            destination: (0, dropNull_1.dropNull)(verified.destination),
            destinationUuid: verified.destinationUuid
                ? (0, normalizeUuid_1.normalizeUuid)(verified.destinationUuid, 'handleVerified.destinationUuid')
                : undefined,
            identityKey: verified.identityKey ? verified.identityKey : undefined,
        }, this.removeFromCache.bind(this, envelope));
        return this.dispatchAndWait(ev);
    }
    async handleRead(envelope, read) {
        log.info('MessageReceiver.handleRead', this.getEnvelopeId(envelope));
        const results = [];
        for (const { timestamp, sender, senderUuid } of read) {
            const ev = new messageReceiverEvents_1.ReadSyncEvent({
                envelopeTimestamp: envelope.timestamp,
                timestamp: (0, normalizeNumber_1.normalizeNumber)((0, dropNull_1.dropNull)(timestamp)),
                sender: (0, dropNull_1.dropNull)(sender),
                senderUuid: senderUuid
                    ? (0, normalizeUuid_1.normalizeUuid)(senderUuid, 'handleRead.senderUuid')
                    : undefined,
            }, this.removeFromCache.bind(this, envelope));
            results.push(this.dispatchAndWait(ev));
        }
        await Promise.all(results);
    }
    async handleViewed(envelope, viewed) {
        log.info('MessageReceiver.handleViewed', this.getEnvelopeId(envelope));
        await Promise.all(viewed.map(async ({ timestamp, senderE164, senderUuid }) => {
            const ev = new messageReceiverEvents_1.ViewSyncEvent({
                envelopeTimestamp: envelope.timestamp,
                timestamp: (0, normalizeNumber_1.normalizeNumber)((0, dropNull_1.dropNull)(timestamp)),
                senderE164: (0, dropNull_1.dropNull)(senderE164),
                senderUuid: senderUuid
                    ? (0, normalizeUuid_1.normalizeUuid)(senderUuid, 'handleViewed.senderUuid')
                    : undefined,
            }, this.removeFromCache.bind(this, envelope));
            await this.dispatchAndWait(ev);
        }));
    }
    async handleContacts(envelope, contacts) {
        log.info('contact sync');
        const { blob } = contacts;
        if (!blob) {
            throw new Error('MessageReceiver.handleContacts: blob field was missing');
        }
        this.removeFromCache(envelope);
        // Note: we do not return here because we don't want to block the next message on
        //   this attachment download and a lot of processing of that attachment.
        const attachmentPointer = await this.handleAttachment(blob);
        const results = [];
        const contactBuffer = new ContactsParser_1.ContactBuffer(attachmentPointer.data);
        let contactDetails = contactBuffer.next();
        while (contactDetails !== undefined) {
            const contactEvent = new messageReceiverEvents_1.ContactEvent(contactDetails);
            results.push(this.dispatchAndWait(contactEvent));
            contactDetails = contactBuffer.next();
        }
        const finalEvent = new messageReceiverEvents_1.ContactSyncEvent();
        results.push(this.dispatchAndWait(finalEvent));
        await Promise.all(results);
        log.info('handleContacts: finished');
    }
    async handleGroups(envelope, groups) {
        log.info('group sync');
        const { blob } = groups;
        this.removeFromCache(envelope);
        if (!blob) {
            throw new Error('MessageReceiver.handleGroups: blob field was missing');
        }
        // Note: we do not return here because we don't want to block the next message on
        //   this attachment download and a lot of processing of that attachment.
        const attachmentPointer = await this.handleAttachment(blob);
        const groupBuffer = new ContactsParser_1.GroupBuffer(attachmentPointer.data);
        let groupDetails = groupBuffer.next();
        const promises = [];
        while (groupDetails) {
            const { id } = groupDetails;
            (0, assert_1.strictAssert)(id, 'Group details without id');
            if (id.byteLength !== 16) {
                log.error(`onGroupReceived: Id was ${id} bytes, expected 16 bytes. Dropping group.`);
                continue;
            }
            const ev = new messageReceiverEvents_1.GroupEvent(Object.assign(Object.assign({}, groupDetails), { id: Bytes.toBinary(id) }));
            const promise = this.dispatchAndWait(ev).catch(e => {
                log.error('error processing group', e);
            });
            groupDetails = groupBuffer.next();
            promises.push(promise);
        }
        await Promise.all(promises);
        const ev = new messageReceiverEvents_1.GroupSyncEvent();
        return this.dispatchAndWait(ev);
    }
    async handleBlocked(envelope, blocked) {
        log.info('Setting these numbers as blocked:', blocked.numbers);
        if (blocked.numbers) {
            await this.storage.put('blocked', blocked.numbers);
        }
        if (blocked.uuids) {
            const uuids = blocked.uuids.map((uuid, index) => {
                return (0, normalizeUuid_1.normalizeUuid)(uuid, `handleBlocked.uuids.${index}`);
            });
            log.info('Setting these uuids as blocked:', uuids);
            await this.storage.put('blocked-uuids', uuids);
        }
        const groupIds = (0, lodash_1.map)(blocked.groupIds, groupId => Bytes.toBinary(groupId));
        log.info('Setting these groups as blocked:', groupIds.map(groupId => `group(${groupId})`));
        await this.storage.put('blocked-groups', groupIds);
        this.removeFromCache(envelope);
    }
    isBlocked(number) {
        return this.storage.blocked.isBlocked(number);
    }
    isUuidBlocked(uuid) {
        return this.storage.blocked.isUuidBlocked(uuid);
    }
    isGroupBlocked(groupId) {
        return this.storage.blocked.isGroupBlocked(groupId);
    }
    async handleAttachment(attachment) {
        const cleaned = (0, processDataMessage_1.processAttachment)(attachment);
        return (0, downloadAttachment_1.downloadAttachment)(this.server, cleaned);
    }
    async handleEndSession(theirUuid) {
        log.info(`handleEndSession: closing sessions for ${theirUuid.toString()}`);
        await this.storage.protocol.archiveAllSessions(theirUuid);
    }
    async processDecrypted(envelope, decrypted) {
        return (0, processDataMessage_1.processDataMessage)(decrypted, envelope.timestamp);
    }
}
exports.default = MessageReceiver;
