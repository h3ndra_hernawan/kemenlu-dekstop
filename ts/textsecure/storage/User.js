"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const assert_1 = require("../../util/assert");
const UUID_1 = require("../../types/UUID");
const log = __importStar(require("../../logging/log"));
const Helpers_1 = __importDefault(require("../Helpers"));
class User {
    constructor(storage) {
        this.storage = storage;
    }
    async setUuidAndDeviceId(uuid, deviceId) {
        await this.storage.put('uuid_id', `${uuid}.${deviceId}`);
        log.info('storage.user: uuid and device id changed');
    }
    async setNumber(number) {
        if (this.getNumber() === number) {
            return;
        }
        const deviceId = this.getDeviceId();
        (0, assert_1.strictAssert)(deviceId !== undefined, 'Cannot update device number without knowing device id');
        log.info('storage.user: number changed');
        await Promise.all([
            this.storage.put('number_id', `${number}.${deviceId}`),
            this.storage.remove('senderCertificate'),
        ]);
        // Notify redux about phone number change
        window.Whisper.events.trigger('userChanged', true);
    }
    getNumber() {
        const numberId = this.storage.get('number_id');
        if (numberId === undefined)
            return undefined;
        return Helpers_1.default.unencodeNumber(numberId)[0];
    }
    getUuid() {
        const uuid = this.storage.get('uuid_id');
        if (uuid === undefined)
            return undefined;
        return new UUID_1.UUID(Helpers_1.default.unencodeNumber(uuid.toLowerCase())[0]);
    }
    getCheckedUuid() {
        const uuid = this.getUuid();
        (0, assert_1.strictAssert)(uuid !== undefined, 'Must have our own uuid');
        return uuid;
    }
    getPni() {
        const pni = this.storage.get('pni');
        if (pni === undefined)
            return undefined;
        return new UUID_1.UUID(pni);
    }
    getOurUuidKind(uuid) {
        const ourUuid = this.getUuid();
        if ((ourUuid === null || ourUuid === void 0 ? void 0 : ourUuid.toString()) === uuid.toString()) {
            return UUID_1.UUIDKind.ACI;
        }
        const pni = this.getPni();
        if ((pni === null || pni === void 0 ? void 0 : pni.toString()) === uuid.toString()) {
            return UUID_1.UUIDKind.PNI;
        }
        return UUID_1.UUIDKind.Unknown;
    }
    getDeviceId() {
        const value = this._getDeviceIdFromUuid() || this._getDeviceIdFromNumber();
        if (value === undefined) {
            return undefined;
        }
        return parseInt(value, 10);
    }
    getDeviceName() {
        return this.storage.get('device_name');
    }
    async setDeviceNameEncrypted() {
        return this.storage.put('deviceNameEncrypted', true);
    }
    getDeviceNameEncrypted() {
        return this.storage.get('deviceNameEncrypted');
    }
    async removeSignalingKey() {
        return this.storage.remove('signaling_key');
    }
    async setCredentials(credentials) {
        const { uuid, number, deviceId, deviceName, password } = credentials;
        await Promise.all([
            this.storage.put('number_id', `${number}.${deviceId}`),
            this.storage.put('uuid_id', `${uuid}.${deviceId}`),
            this.storage.put('password', password),
            deviceName
                ? this.storage.put('device_name', deviceName)
                : Promise.resolve(),
        ]);
    }
    async removeCredentials() {
        log.info('storage.user: removeCredentials');
        await Promise.all([
            this.storage.remove('number_id'),
            this.storage.remove('uuid_id'),
            this.storage.remove('password'),
            this.storage.remove('device_name'),
        ]);
    }
    getWebAPICredentials() {
        return {
            username: this.storage.get('uuid_id') || this.storage.get('number_id') || '',
            password: this.storage.get('password', ''),
        };
    }
    _getDeviceIdFromUuid() {
        const uuid = this.storage.get('uuid_id');
        if (uuid === undefined)
            return undefined;
        return Helpers_1.default.unencodeNumber(uuid)[1];
    }
    _getDeviceIdFromNumber() {
        const numberId = this.storage.get('number_id');
        if (numberId === undefined)
            return undefined;
        return Helpers_1.default.unencodeNumber(numberId)[1];
    }
}
exports.User = User;
