"use strict";
// Copyright 2016-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Blocked = void 0;
const lodash_1 = require("lodash");
const log = __importStar(require("../../logging/log"));
const BLOCKED_NUMBERS_ID = 'blocked';
const BLOCKED_UUIDS_ID = 'blocked-uuids';
const BLOCKED_GROUPS_ID = 'blocked-groups';
class Blocked {
    constructor(storage) {
        this.storage = storage;
    }
    getBlockedNumbers() {
        return this.storage.get(BLOCKED_NUMBERS_ID, new Array());
    }
    isBlocked(number) {
        return this.getBlockedNumbers().includes(number);
    }
    async addBlockedNumber(number) {
        const numbers = this.getBlockedNumbers();
        if (numbers.includes(number)) {
            return;
        }
        log.info('adding', number, 'to blocked list');
        await this.storage.put(BLOCKED_NUMBERS_ID, numbers.concat(number));
    }
    async removeBlockedNumber(number) {
        const numbers = this.getBlockedNumbers();
        if (!numbers.includes(number)) {
            return;
        }
        log.info('removing', number, 'from blocked list');
        await this.storage.put(BLOCKED_NUMBERS_ID, (0, lodash_1.without)(numbers, number));
    }
    getBlockedUuids() {
        return this.storage.get(BLOCKED_UUIDS_ID, new Array());
    }
    isUuidBlocked(uuid) {
        return this.getBlockedUuids().includes(uuid);
    }
    async addBlockedUuid(uuid) {
        const uuids = this.getBlockedUuids();
        if (uuids.includes(uuid)) {
            return;
        }
        log.info('adding', uuid, 'to blocked list');
        await this.storage.put(BLOCKED_UUIDS_ID, uuids.concat(uuid));
    }
    async removeBlockedUuid(uuid) {
        const numbers = this.getBlockedUuids();
        if (!numbers.includes(uuid)) {
            return;
        }
        log.info('removing', uuid, 'from blocked list');
        await this.storage.put(BLOCKED_UUIDS_ID, (0, lodash_1.without)(numbers, uuid));
    }
    getBlockedGroups() {
        return this.storage.get(BLOCKED_GROUPS_ID, new Array());
    }
    isGroupBlocked(groupId) {
        return this.getBlockedGroups().includes(groupId);
    }
    async addBlockedGroup(groupId) {
        const groupIds = this.getBlockedGroups();
        if (groupIds.includes(groupId)) {
            return;
        }
        log.info(`adding group(${groupId}) to blocked list`);
        await this.storage.put(BLOCKED_GROUPS_ID, groupIds.concat(groupId));
    }
    async removeBlockedGroup(groupId) {
        const groupIds = this.getBlockedGroups();
        if (!groupIds.includes(groupId)) {
            return;
        }
        log.info(`removing group(${groupId} from blocked list`);
        await this.storage.put(BLOCKED_GROUPS_ID, (0, lodash_1.without)(groupIds, groupId));
    }
}
exports.Blocked = Blocked;
