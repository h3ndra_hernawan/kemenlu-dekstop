"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.padMessage = exports.serializedCertificateSchema = void 0;
/* eslint-disable guard-for-in */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable more/no-then */
/* eslint-disable no-param-reassign */
const lodash_1 = require("lodash");
const zod_1 = require("zod");
const signal_client_1 = require("@signalapp/signal-client");
const Errors_1 = require("./Errors");
const PhoneNumber_1 = require("../types/PhoneNumber");
const Address_1 = require("../types/Address");
const QualifiedAddress_1 = require("../types/QualifiedAddress");
const UUID_1 = require("../types/UUID");
const LibSignalStores_1 = require("../LibSignalStores");
const updateConversationsWithUuidLookup_1 = require("../updateConversationsWithUuidLookup");
const getKeysForIdentifier_1 = require("./getKeysForIdentifier");
const protobuf_1 = require("../protobuf");
const log = __importStar(require("../logging/log"));
exports.serializedCertificateSchema = zod_1.z
    .object({
    expires: zod_1.z.number().optional(),
    serialized: zod_1.z.instanceof(Uint8Array),
})
    .nonstrict();
function ciphertextMessageTypeToEnvelopeType(type) {
    if (type === 3 /* PreKey */) {
        return protobuf_1.SignalService.Envelope.Type.PREKEY_BUNDLE;
    }
    if (type === 2 /* Whisper */) {
        return protobuf_1.SignalService.Envelope.Type.CIPHERTEXT;
    }
    if (type === 8 /* Plaintext */) {
        return protobuf_1.SignalService.Envelope.Type.PLAINTEXT_CONTENT;
    }
    throw new Error(`ciphertextMessageTypeToEnvelopeType: Unrecognized type ${type}`);
}
function getPaddedMessageLength(messageLength) {
    const messageLengthWithTerminator = messageLength + 1;
    let messagePartCount = Math.floor(messageLengthWithTerminator / 160);
    if (messageLengthWithTerminator % 160 !== 0) {
        messagePartCount += 1;
    }
    return messagePartCount * 160;
}
function padMessage(messageBuffer) {
    const plaintext = new Uint8Array(getPaddedMessageLength(messageBuffer.byteLength + 1) - 1);
    plaintext.set(messageBuffer);
    plaintext[messageBuffer.byteLength] = 0x80;
    return plaintext;
}
exports.padMessage = padMessage;
class OutgoingMessage {
    constructor({ callback, contentHint, groupId, identifiers, message, options, sendLogCallback, server, timestamp, }) {
        if (message instanceof protobuf_1.SignalService.DataMessage) {
            const content = new protobuf_1.SignalService.Content();
            content.dataMessage = message;
            // eslint-disable-next-line no-param-reassign
            this.message = content;
        }
        else {
            this.message = message;
        }
        this.server = server;
        this.timestamp = timestamp;
        this.identifiers = identifiers;
        this.contentHint = contentHint;
        this.groupId = groupId;
        this.callback = callback;
        this.identifiersCompleted = 0;
        this.errors = [];
        this.successfulIdentifiers = [];
        this.failoverIdentifiers = [];
        this.unidentifiedDeliveries = [];
        this.recipients = {};
        this.sendLogCallback = sendLogCallback;
        this.sendMetadata = options === null || options === void 0 ? void 0 : options.sendMetadata;
        this.online = options === null || options === void 0 ? void 0 : options.online;
    }
    numberCompleted() {
        this.identifiersCompleted += 1;
        if (this.identifiersCompleted >= this.identifiers.length) {
            const contentProto = this.getContentProtoBytes();
            const { timestamp, contentHint, recipients } = this;
            this.callback({
                successfulIdentifiers: this.successfulIdentifiers,
                failoverIdentifiers: this.failoverIdentifiers,
                errors: this.errors,
                unidentifiedDeliveries: this.unidentifiedDeliveries,
                contentHint,
                recipients,
                contentProto,
                timestamp,
            });
        }
    }
    registerError(identifier, reason, providedError) {
        let error = providedError;
        if (!error || (error instanceof Errors_1.HTTPError && error.code !== 404)) {
            if (error && error.code === 428) {
                error = new Errors_1.SendMessageChallengeError(identifier, error);
            }
            else {
                error = new Errors_1.OutgoingMessageError(identifier, null, null, error);
            }
        }
        error.reason = reason;
        error.stackForLog = providedError ? providedError.stack : undefined;
        this.errors[this.errors.length] = error;
        this.numberCompleted();
    }
    reloadDevicesAndSend(identifier, recurse) {
        return async () => {
            const ourUuid = window.textsecure.storage.user.getCheckedUuid();
            const deviceIds = await window.textsecure.storage.protocol.getDeviceIds({
                ourUuid,
                identifier,
            });
            if (deviceIds.length === 0) {
                this.registerError(identifier, 'reloadDevicesAndSend: Got empty device list when loading device keys', undefined);
                return undefined;
            }
            return this.doSendMessage(identifier, deviceIds, recurse);
        };
    }
    async getKeysForIdentifier(identifier, updateDevices) {
        var _a;
        const { sendMetadata } = this;
        const info = sendMetadata && sendMetadata[identifier]
            ? sendMetadata[identifier]
            : { accessKey: undefined };
        const { accessKey } = info;
        try {
            const { accessKeyFailed } = await (0, getKeysForIdentifier_1.getKeysForIdentifier)(identifier, this.server, updateDevices, accessKey);
            if (accessKeyFailed && !this.failoverIdentifiers.includes(identifier)) {
                this.failoverIdentifiers.push(identifier);
            }
        }
        catch (error) {
            if ((_a = error === null || error === void 0 ? void 0 : error.message) === null || _a === void 0 ? void 0 : _a.includes('untrusted identity for address')) {
                error.timestamp = this.timestamp;
            }
            throw error;
        }
    }
    async transmitMessage(identifier, jsonData, timestamp, { accessKey } = {}) {
        let promise;
        if (accessKey) {
            promise = this.server.sendMessagesUnauth(identifier, jsonData, timestamp, this.online, { accessKey });
        }
        else {
            promise = this.server.sendMessages(identifier, jsonData, timestamp, this.online);
        }
        return promise.catch(e => {
            if (e instanceof Errors_1.HTTPError && e.code !== 409 && e.code !== 410) {
                // 409 and 410 should bubble and be handled by doSendMessage
                // 404 should throw UnregisteredUserError
                // 428 should throw SendMessageChallengeError
                // all other network errors can be retried later.
                if (e.code === 404) {
                    throw new Errors_1.UnregisteredUserError(identifier, e);
                }
                if (e.code === 428) {
                    throw new Errors_1.SendMessageChallengeError(identifier, e);
                }
                throw new Errors_1.SendMessageNetworkError(identifier, jsonData, e);
            }
            throw e;
        });
    }
    getPlaintext() {
        if (!this.plaintext) {
            const { message } = this;
            if (message instanceof protobuf_1.SignalService.Content) {
                this.plaintext = padMessage(protobuf_1.SignalService.Content.encode(message).finish());
            }
            else {
                this.plaintext = message.serialize();
            }
        }
        return this.plaintext;
    }
    getContentProtoBytes() {
        if (this.message instanceof protobuf_1.SignalService.Content) {
            return new Uint8Array(protobuf_1.SignalService.Content.encode(this.message).finish());
        }
        return undefined;
    }
    async getCiphertextMessage({ identityKeyStore, protocolAddress, sessionStore, }) {
        const { message } = this;
        if (message instanceof protobuf_1.SignalService.Content) {
            return (0, signal_client_1.signalEncrypt)(Buffer.from(this.getPlaintext()), protocolAddress, sessionStore, identityKeyStore);
        }
        return message.asCiphertextMessage();
    }
    async doSendMessage(identifier, deviceIds, recurse) {
        const { sendMetadata } = this;
        const { accessKey, senderCertificate } = (sendMetadata === null || sendMetadata === void 0 ? void 0 : sendMetadata[identifier]) || {};
        if (accessKey && !senderCertificate) {
            log.warn('OutgoingMessage.doSendMessage: accessKey was provided, but senderCertificate was not');
        }
        const sealedSender = Boolean(accessKey && senderCertificate);
        // We don't send to ourselves unless sealedSender is enabled
        const ourNumber = window.textsecure.storage.user.getNumber();
        const ourUuid = window.textsecure.storage.user.getCheckedUuid();
        const ourDeviceId = window.textsecure.storage.user.getDeviceId();
        if ((identifier === ourNumber || identifier === ourUuid.toString()) &&
            !sealedSender) {
            deviceIds = (0, lodash_1.reject)(deviceIds, deviceId => 
            // because we store our own device ID as a string at least sometimes
            deviceId === ourDeviceId ||
                (typeof ourDeviceId === 'string' &&
                    deviceId === parseInt(ourDeviceId, 10)));
        }
        const sessionStore = new LibSignalStores_1.Sessions({ ourUuid });
        const identityKeyStore = new LibSignalStores_1.IdentityKeys({ ourUuid });
        return Promise.all(deviceIds.map(async (destinationDeviceId) => {
            const theirUuid = UUID_1.UUID.checkedLookup(identifier);
            const address = new QualifiedAddress_1.QualifiedAddress(ourUuid, new Address_1.Address(theirUuid, destinationDeviceId));
            return window.textsecure.storage.protocol.enqueueSessionJob(address, async () => {
                const protocolAddress = signal_client_1.ProtocolAddress.new(theirUuid.toString(), destinationDeviceId);
                const activeSession = await sessionStore.getSession(protocolAddress);
                if (!activeSession) {
                    throw new Error('OutgoingMessage.doSendMessage: No active sesssion!');
                }
                const destinationRegistrationId = activeSession.remoteRegistrationId();
                if (sealedSender && senderCertificate) {
                    const ciphertextMessage = await this.getCiphertextMessage({
                        identityKeyStore,
                        protocolAddress,
                        sessionStore,
                    });
                    const certificate = signal_client_1.SenderCertificate.deserialize(Buffer.from(senderCertificate.serialized));
                    const groupIdBuffer = this.groupId
                        ? Buffer.from(this.groupId, 'base64')
                        : null;
                    const content = signal_client_1.UnidentifiedSenderMessageContent.new(ciphertextMessage, certificate, this.contentHint, groupIdBuffer);
                    const buffer = await (0, signal_client_1.sealedSenderEncrypt)(content, protocolAddress, identityKeyStore);
                    return {
                        type: protobuf_1.SignalService.Envelope.Type.UNIDENTIFIED_SENDER,
                        destinationDeviceId,
                        destinationRegistrationId,
                        content: buffer.toString('base64'),
                    };
                }
                const ciphertextMessage = await this.getCiphertextMessage({
                    identityKeyStore,
                    protocolAddress,
                    sessionStore,
                });
                const type = ciphertextMessageTypeToEnvelopeType(ciphertextMessage.type());
                const content = ciphertextMessage.serialize().toString('base64');
                return {
                    type,
                    destinationDeviceId,
                    destinationRegistrationId,
                    content,
                };
            });
        }))
            .then(async (jsonData) => {
            if (sealedSender) {
                return this.transmitMessage(identifier, jsonData, this.timestamp, {
                    accessKey,
                }).then(() => {
                    this.recipients[identifier] = deviceIds;
                    this.unidentifiedDeliveries.push(identifier);
                    this.successfulIdentifiers.push(identifier);
                    this.numberCompleted();
                    if (this.sendLogCallback) {
                        this.sendLogCallback({
                            identifier,
                            deviceIds,
                        });
                    }
                    else if (this.successfulIdentifiers.length > 1) {
                        log.warn(`OutgoingMessage.doSendMessage: no sendLogCallback provided for message ${this.timestamp}, but multiple recipients`);
                    }
                }, async (error) => {
                    if (error instanceof Errors_1.HTTPError &&
                        (error.code === 401 || error.code === 403)) {
                        if (this.failoverIdentifiers.indexOf(identifier) === -1) {
                            this.failoverIdentifiers.push(identifier);
                        }
                        // This ensures that we don't hit this codepath the next time through
                        if (sendMetadata) {
                            delete sendMetadata[identifier];
                        }
                        return this.doSendMessage(identifier, deviceIds, recurse);
                    }
                    throw error;
                });
            }
            return this.transmitMessage(identifier, jsonData, this.timestamp).then(() => {
                this.successfulIdentifiers.push(identifier);
                this.recipients[identifier] = deviceIds;
                this.numberCompleted();
                if (this.sendLogCallback) {
                    this.sendLogCallback({
                        identifier,
                        deviceIds,
                    });
                }
                else if (this.successfulIdentifiers.length > 1) {
                    log.warn(`OutgoingMessage.doSendMessage: no sendLogCallback provided for message ${this.timestamp}, but multiple recipients`);
                }
            });
        })
            .catch(async (error) => {
            var _a;
            if (error instanceof Errors_1.HTTPError &&
                (error.code === 410 || error.code === 409)) {
                if (!recurse) {
                    this.registerError(identifier, 'Hit retry limit attempting to reload device list', error);
                    return undefined;
                }
                const response = error.response;
                let p = Promise.resolve();
                if (error.code === 409) {
                    p = this.removeDeviceIdsForIdentifier(identifier, response.extraDevices || []);
                }
                else {
                    p = Promise.all((response.staleDevices || []).map(async (deviceId) => {
                        await window.textsecure.storage.protocol.archiveSession(new QualifiedAddress_1.QualifiedAddress(ourUuid, new Address_1.Address(UUID_1.UUID.checkedLookup(identifier), deviceId)));
                    }));
                }
                return p.then(async () => {
                    const resetDevices = error.code === 410
                        ? response.staleDevices
                        : response.missingDevices;
                    return this.getKeysForIdentifier(identifier, resetDevices).then(
                    // We continue to retry as long as the error code was 409; the assumption is
                    //   that we'll request new device info and the next request will succeed.
                    this.reloadDevicesAndSend(identifier, error.code === 409));
                });
            }
            if ((_a = error === null || error === void 0 ? void 0 : error.message) === null || _a === void 0 ? void 0 : _a.includes('untrusted identity for address')) {
                // eslint-disable-next-line no-param-reassign
                error.timestamp = this.timestamp;
                log.error('Got "key changed" error from encrypt - no identityKey for application layer', identifier, deviceIds);
                log.info('closing all sessions for', identifier);
                window.textsecure.storage.protocol
                    .archiveAllSessions(UUID_1.UUID.checkedLookup(identifier))
                    .then(() => {
                    throw error;
                }, innerError => {
                    log.error(`doSendMessage: Error closing sessions: ${innerError.stack}`);
                    throw error;
                });
            }
            this.registerError(identifier, 'Failed to create or send message', error);
            return undefined;
        });
    }
    async removeDeviceIdsForIdentifier(identifier, deviceIdsToRemove) {
        const ourUuid = window.textsecure.storage.user.getCheckedUuid();
        const theirUuid = UUID_1.UUID.checkedLookup(identifier);
        await Promise.all(deviceIdsToRemove.map(async (deviceId) => {
            await window.textsecure.storage.protocol.archiveSession(new QualifiedAddress_1.QualifiedAddress(ourUuid, new Address_1.Address(theirUuid, deviceId)));
        }));
    }
    async sendToIdentifier(providedIdentifier) {
        var _a, _b;
        let identifier = providedIdentifier;
        try {
            if ((0, UUID_1.isValidUuid)(identifier)) {
                // We're good!
            }
            else if ((0, PhoneNumber_1.isValidNumber)(identifier)) {
                if (!window.textsecure.messaging) {
                    throw new Error('sendToIdentifier: window.textsecure.messaging is not available!');
                }
                try {
                    await (0, updateConversationsWithUuidLookup_1.updateConversationsWithUuidLookup)({
                        conversationController: window.ConversationController,
                        conversations: [
                            window.ConversationController.getOrCreate(identifier, 'private'),
                        ],
                        messaging: window.textsecure.messaging,
                    });
                    const uuid = (_a = window.ConversationController.get(identifier)) === null || _a === void 0 ? void 0 : _a.get('uuid');
                    if (!uuid) {
                        throw new Errors_1.UnregisteredUserError(identifier, new Errors_1.HTTPError('User is not registered', {
                            code: -1,
                            headers: {},
                        }));
                    }
                    identifier = uuid;
                }
                catch (error) {
                    log.error(`sendToIdentifier: Failed to fetch UUID for identifier ${identifier}`, error && error.stack ? error.stack : error);
                }
            }
            else {
                throw new Error(`sendToIdentifier: identifier ${identifier} was neither a UUID or E164`);
            }
            const ourUuid = window.textsecure.storage.user.getCheckedUuid();
            const deviceIds = await window.textsecure.storage.protocol.getDeviceIds({
                ourUuid,
                identifier,
            });
            if (deviceIds.length === 0) {
                await this.getKeysForIdentifier(identifier);
            }
            await this.reloadDevicesAndSend(identifier, true)();
        }
        catch (error) {
            if ((_b = error === null || error === void 0 ? void 0 : error.message) === null || _b === void 0 ? void 0 : _b.includes('untrusted identity for address')) {
                const newError = new Errors_1.OutgoingIdentityKeyError(identifier, error.originalMessage, error.timestamp, error.identityKey && new Uint8Array(error.identityKey));
                this.registerError(identifier, 'Untrusted identity', newError);
            }
            else {
                this.registerError(identifier, `Failed to retrieve new device keys for identifier ${identifier}`, error);
            }
        }
    }
}
exports.default = OutgoingMessage;
