"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.translateError = exports.handleStatusCode = void 0;
const log = __importStar(require("../logging/log"));
async function handleStatusCode(status) {
    if (status === 499) {
        log.error('Got 499 from Signal Server. Build is expired.');
        await window.storage.put('remoteBuildExpiration', Date.now());
        window.reduxActions.expiration.hydrateExpirationStatus(true);
    }
}
exports.handleStatusCode = handleStatusCode;
function translateError(error) {
    const { code } = error;
    if (code === 200) {
        // Happens sometimes when we get no response. Might be nice to get 204 instead.
        return undefined;
    }
    let message;
    switch (code) {
        case -1:
            message =
                'Failed to connect to the server, please check your network connection.';
            break;
        case 413:
            message = 'Rate limit exceeded, please try again later.';
            break;
        case 403:
            message = 'Invalid code, please try again.';
            break;
        case 417:
            message = 'Number already registered.';
            break;
        case 401:
            message =
                'Invalid authentication, most likely someone re-registered and invalidated our registration.';
            break;
        case 404:
            message = 'Number is not registered.';
            break;
        default:
            message = 'The server rejected our query, please file a bug report.';
    }
    // eslint-disable-next-line no-param-reassign
    error.message = `${message} (original: ${error.message})`;
    return error;
}
exports.translateError = translateError;
