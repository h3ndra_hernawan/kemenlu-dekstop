"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable more/no-then */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
const p_queue_1 = __importDefault(require("p-queue"));
const lodash_1 = require("lodash");
const EventTarget_1 = __importDefault(require("./EventTarget"));
const Errors_1 = require("./Errors");
const ProvisioningCipher_1 = __importDefault(require("./ProvisioningCipher"));
const TaskWithTimeout_1 = __importDefault(require("./TaskWithTimeout"));
const Bytes = __importStar(require("../Bytes"));
const RemoveAllConfiguration_1 = require("../types/RemoveAllConfiguration");
const senderCertificate_1 = require("../services/senderCertificate");
const Crypto_1 = require("../Crypto");
const Curve_1 = require("../Curve");
const UUID_1 = require("../types/UUID");
const timestamp_1 = require("../util/timestamp");
const ourProfileKey_1 = require("../services/ourProfileKey");
const assert_1 = require("../util/assert");
const getProvisioningUrl_1 = require("../util/getProvisioningUrl");
const protobuf_1 = require("../protobuf");
const log = __importStar(require("../logging/log"));
const DAY = 24 * 60 * 60 * 1000;
const MINIMUM_SIGNED_PREKEYS = 5;
const ARCHIVE_AGE = 30 * DAY;
const PREKEY_ROTATION_AGE = DAY * 1.5;
const PROFILE_KEY_LENGTH = 32;
const SIGNED_KEY_GEN_BATCH_SIZE = 100;
function getIdentifier(id) {
    if (!id || !id.length) {
        return id;
    }
    const parts = id.split('.');
    if (!parts.length) {
        return id;
    }
    return parts[0];
}
class AccountManager extends EventTarget_1.default {
    constructor(server) {
        super();
        this.server = server;
        this.pending = Promise.resolve();
    }
    async requestVoiceVerification(number) {
        return this.server.requestVerificationVoice(number);
    }
    async requestSMSVerification(number) {
        return this.server.requestVerificationSMS(number);
    }
    encryptDeviceName(name, identityKey) {
        if (!name) {
            return null;
        }
        const encrypted = (0, Crypto_1.encryptDeviceName)(name, identityKey.pubKey);
        const proto = new protobuf_1.SignalService.DeviceName();
        proto.ephemeralPublic = encrypted.ephemeralPublic;
        proto.syntheticIv = encrypted.syntheticIv;
        proto.ciphertext = encrypted.ciphertext;
        const bytes = protobuf_1.SignalService.DeviceName.encode(proto).finish();
        return Bytes.toBase64(bytes);
    }
    async decryptDeviceName(base64) {
        const ourUuid = window.textsecure.storage.user.getCheckedUuid();
        const identityKey = await window.textsecure.storage.protocol.getIdentityKeyPair(ourUuid);
        if (!identityKey) {
            throw new Error('decryptDeviceName: No identity key pair!');
        }
        const bytes = Bytes.fromBase64(base64);
        const proto = protobuf_1.SignalService.DeviceName.decode(bytes);
        (0, assert_1.assert)(proto.ephemeralPublic && proto.syntheticIv && proto.ciphertext, 'Missing required fields in DeviceName');
        const name = (0, Crypto_1.decryptDeviceName)(proto, identityKey.privKey);
        return name;
    }
    async maybeUpdateDeviceName() {
        const isNameEncrypted = window.textsecure.storage.user.getDeviceNameEncrypted();
        if (isNameEncrypted) {
            return;
        }
        const { storage } = window.textsecure;
        const deviceName = storage.user.getDeviceName();
        const identityKeyPair = await storage.protocol.getIdentityKeyPair(storage.user.getCheckedUuid());
        (0, assert_1.strictAssert)(identityKeyPair !== undefined, "Can't encrypt device name without identity key pair");
        const base64 = this.encryptDeviceName(deviceName || '', identityKeyPair);
        if (base64) {
            await this.server.updateDeviceName(base64);
        }
    }
    async deviceNameIsEncrypted() {
        await window.textsecure.storage.user.setDeviceNameEncrypted();
    }
    async registerSingleDevice(number, verificationCode) {
        return this.queueTask(async () => {
            const identityKeyPair = (0, Curve_1.generateKeyPair)();
            const profileKey = (0, Crypto_1.getRandomBytes)(PROFILE_KEY_LENGTH);
            const accessKey = (0, Crypto_1.deriveAccessKey)(profileKey);
            await this.createAccount(number, verificationCode, identityKeyPair, profileKey, null, null, null, { accessKey });
            await this.clearSessionsAndPreKeys();
            const keys = await this.generateKeys(SIGNED_KEY_GEN_BATCH_SIZE);
            await this.server.registerKeys(keys);
            await this.confirmKeys(keys);
            await this.registrationDone();
        });
    }
    async registerSecondDevice(setProvisioningUrl, confirmNumber, progressCallback) {
        const createAccount = this.createAccount.bind(this);
        const clearSessionsAndPreKeys = this.clearSessionsAndPreKeys.bind(this);
        const generateKeys = this.generateKeys.bind(this, SIGNED_KEY_GEN_BATCH_SIZE, progressCallback);
        const provisioningCipher = new ProvisioningCipher_1.default();
        const pubKey = await provisioningCipher.getPublicKey();
        let envelopeCallbacks;
        const envelopePromise = new Promise((resolve, reject) => {
            envelopeCallbacks = { resolve, reject };
        });
        const wsr = await this.server.getProvisioningResource({
            handleRequest(request) {
                var _a;
                if (request.path === '/v1/address' &&
                    request.verb === 'PUT' &&
                    request.body) {
                    const proto = protobuf_1.SignalService.ProvisioningUuid.decode(request.body);
                    const { uuid } = proto;
                    if (!uuid) {
                        throw new Error('registerSecondDevice: expected a UUID');
                    }
                    const url = (0, getProvisioningUrl_1.getProvisioningUrl)(uuid, pubKey);
                    (_a = window.CI) === null || _a === void 0 ? void 0 : _a.setProvisioningURL(url);
                    setProvisioningUrl(url);
                    request.respond(200, 'OK');
                }
                else if (request.path === '/v1/message' &&
                    request.verb === 'PUT' &&
                    request.body) {
                    const envelope = protobuf_1.SignalService.ProvisionEnvelope.decode(request.body);
                    request.respond(200, 'OK');
                    wsr.close();
                    envelopeCallbacks === null || envelopeCallbacks === void 0 ? void 0 : envelopeCallbacks.resolve(envelope);
                }
                else {
                    log.error('Unknown websocket message', request.path);
                }
            },
        });
        log.info('provisioning socket open');
        wsr.addEventListener('close', ({ code, reason }) => {
            log.info(`provisioning socket closed. Code: ${code} Reason: ${reason}`);
            // Note: if we have resolved the envelope already - this has no effect
            envelopeCallbacks === null || envelopeCallbacks === void 0 ? void 0 : envelopeCallbacks.reject(new Error('websocket closed'));
        });
        const envelope = await envelopePromise;
        const provisionMessage = await provisioningCipher.decrypt(envelope);
        await this.queueTask(async () => {
            const deviceName = await confirmNumber(provisionMessage.number);
            if (typeof deviceName !== 'string' || deviceName.length === 0) {
                throw new Error('AccountManager.registerSecondDevice: Invalid device name');
            }
            if (!provisionMessage.number ||
                !provisionMessage.provisioningCode ||
                !provisionMessage.identityKeyPair) {
                throw new Error('AccountManager.registerSecondDevice: Provision message was missing key data');
            }
            await createAccount(provisionMessage.number, provisionMessage.provisioningCode, provisionMessage.identityKeyPair, provisionMessage.profileKey, deviceName, provisionMessage.userAgent, provisionMessage.readReceipts, { uuid: provisionMessage.uuid });
            await clearSessionsAndPreKeys();
            const keys = await generateKeys();
            await this.server.registerKeys(keys);
            await this.confirmKeys(keys);
            await this.registrationDone();
        });
    }
    async refreshPreKeys() {
        const generateKeys = this.generateKeys.bind(this, SIGNED_KEY_GEN_BATCH_SIZE);
        const registerKeys = this.server.registerKeys.bind(this.server);
        return this.queueTask(async () => this.server.getMyKeys().then(async (preKeyCount) => {
            log.info(`prekey count ${preKeyCount}`);
            if (preKeyCount < 10) {
                return generateKeys().then(registerKeys);
            }
            return null;
        }));
    }
    async rotateSignedPreKey() {
        return this.queueTask(async () => {
            const ourUuid = window.textsecure.storage.user.getCheckedUuid();
            const signedKeyId = window.textsecure.storage.get('signedKeyId', 1);
            if (typeof signedKeyId !== 'number') {
                throw new Error('Invalid signedKeyId');
            }
            const store = window.textsecure.storage.protocol;
            const { server, cleanSignedPreKeys } = this;
            const existingKeys = await store.loadSignedPreKeys(ourUuid);
            existingKeys.sort((a, b) => (b.created_at || 0) - (a.created_at || 0));
            const confirmedKeys = existingKeys.filter(key => key.confirmed);
            const mostRecent = confirmedKeys[0];
            if ((0, timestamp_1.isMoreRecentThan)((mostRecent === null || mostRecent === void 0 ? void 0 : mostRecent.created_at) || 0, PREKEY_ROTATION_AGE)) {
                log.warn(`rotateSignedPreKey: ${confirmedKeys.length} confirmed keys, most recent was created ${mostRecent === null || mostRecent === void 0 ? void 0 : mostRecent.created_at}. Cancelling rotation.`);
                return;
            }
            return store
                .getIdentityKeyPair(ourUuid)
                .then(async (identityKey) => {
                if (!identityKey) {
                    throw new Error('rotateSignedPreKey: No identity key pair!');
                }
                return (0, Curve_1.generateSignedPreKey)(identityKey, signedKeyId);
            }, () => {
                // We swallow any error here, because we don't want to get into
                //   a loop of repeated retries.
                log.error('Failed to get identity key. Canceling key rotation.');
                return null;
            })
                .then(async (res) => {
                if (!res) {
                    return null;
                }
                log.info('Saving new signed prekey', res.keyId);
                return Promise.all([
                    window.textsecure.storage.put('signedKeyId', signedKeyId + 1),
                    store.storeSignedPreKey(ourUuid, res.keyId, res.keyPair),
                    server.setSignedPreKey({
                        keyId: res.keyId,
                        publicKey: res.keyPair.pubKey,
                        signature: res.signature,
                    }),
                ])
                    .then(async () => {
                    const confirmed = true;
                    log.info('Confirming new signed prekey', res.keyId);
                    return Promise.all([
                        window.textsecure.storage.remove('signedKeyRotationRejected'),
                        store.storeSignedPreKey(ourUuid, res.keyId, res.keyPair, confirmed),
                    ]);
                })
                    .then(cleanSignedPreKeys);
            })
                .catch(async (e) => {
                log.error('rotateSignedPrekey error:', e && e.stack ? e.stack : e);
                if (e instanceof Errors_1.HTTPError &&
                    e.code &&
                    e.code >= 400 &&
                    e.code <= 599) {
                    const rejections = 1 + window.textsecure.storage.get('signedKeyRotationRejected', 0);
                    await window.textsecure.storage.put('signedKeyRotationRejected', rejections);
                    log.error('Signed key rotation rejected count:', rejections);
                }
                else {
                    throw e;
                }
            });
        });
    }
    async queueTask(task) {
        this.pendingQueue = this.pendingQueue || new p_queue_1.default({ concurrency: 1 });
        const taskWithTimeout = (0, TaskWithTimeout_1.default)(task, 'AccountManager task');
        return this.pendingQueue.add(taskWithTimeout);
    }
    async cleanSignedPreKeys() {
        const ourUuid = window.textsecure.storage.user.getCheckedUuid();
        const store = window.textsecure.storage.protocol;
        const allKeys = await store.loadSignedPreKeys(ourUuid);
        allKeys.sort((a, b) => (b.created_at || 0) - (a.created_at || 0));
        const confirmed = allKeys.filter(key => key.confirmed);
        const unconfirmed = allKeys.filter(key => !key.confirmed);
        const recent = allKeys[0] ? allKeys[0].keyId : 'none';
        const recentConfirmed = confirmed[0] ? confirmed[0].keyId : 'none';
        const recentUnconfirmed = unconfirmed[0] ? unconfirmed[0].keyId : 'none';
        log.info(`cleanSignedPreKeys: Most recent signed key: ${recent}`);
        log.info(`cleanSignedPreKeys: Most recent confirmed signed key: ${recentConfirmed}`);
        log.info(`cleanSignedPreKeys: Most recent unconfirmed signed key: ${recentUnconfirmed}`);
        log.info('cleanSignedPreKeys: Total signed key count:', allKeys.length, '-', confirmed.length, 'confirmed');
        // Keep MINIMUM_SIGNED_PREKEYS keys, then drop if older than ARCHIVE_AGE
        await Promise.all(allKeys.map(async (key, index) => {
            if (index < MINIMUM_SIGNED_PREKEYS) {
                return;
            }
            const createdAt = key.created_at || 0;
            if ((0, timestamp_1.isOlderThan)(createdAt, ARCHIVE_AGE)) {
                const timestamp = new Date(createdAt).toJSON();
                const confirmedText = key.confirmed ? ' (confirmed)' : '';
                log.info(`Removing signed prekey: ${key.keyId} with timestamp ${timestamp}${confirmedText}`);
                await store.removeSignedPreKey(ourUuid, key.keyId);
            }
        }));
    }
    async createAccount(number, verificationCode, identityKeyPair, profileKey, deviceName, userAgent, readReceipts, options = {}) {
        var _a;
        const { storage } = window.textsecure;
        const { accessKey, uuid } = options;
        let password = Bytes.toBase64((0, Crypto_1.getRandomBytes)(16));
        password = password.substring(0, password.length - 2);
        const registrationId = (0, Crypto_1.generateRegistrationId)();
        const previousNumber = getIdentifier(storage.get('number_id'));
        const previousUuid = getIdentifier(storage.get('uuid_id'));
        let encryptedDeviceName;
        if (deviceName) {
            encryptedDeviceName = this.encryptDeviceName(deviceName, identityKeyPair);
            await this.deviceNameIsEncrypted();
        }
        log.info(`createAccount: Number is ${number}, password has length: ${password ? password.length : 'none'}`);
        const response = await this.server.confirmCode(number, verificationCode, password, registrationId, encryptedDeviceName, { accessKey, uuid });
        const ourUuid = uuid || response.uuid;
        (0, assert_1.strictAssert)(ourUuid !== undefined, 'Should have UUID after registration');
        const uuidChanged = previousUuid && ourUuid && previousUuid !== ourUuid;
        // We only consider the number changed if we didn't have a UUID before
        const numberChanged = !previousUuid && previousNumber && previousNumber !== number;
        if (uuidChanged || numberChanged) {
            if (uuidChanged) {
                log.warn('createAccount: New uuid is different from old uuid; deleting all previous data');
            }
            if (numberChanged) {
                log.warn('createAccount: New number is different from old number; deleting all previous data');
            }
            try {
                await storage.protocol.removeAllData();
                log.info('createAccount: Successfully deleted previous data');
            }
            catch (error) {
                log.error('Something went wrong deleting data from previous number', error && error.stack ? error.stack : error);
            }
        }
        else {
            log.info('createAccount: Erasing configuration (soft)');
            await storage.protocol.removeAllConfiguration(RemoveAllConfiguration_1.RemoveAllConfiguration.Soft);
        }
        await senderCertificate_1.senderCertificateService.clear();
        if (previousUuid) {
            await Promise.all([
                storage.put('identityKeyMap', (0, lodash_1.omit)(storage.get('identityKeyMap') || {}, previousUuid)),
                storage.put('registrationIdMap', (0, lodash_1.omit)(storage.get('registrationIdMap') || {}, previousUuid)),
            ]);
        }
        // `setCredentials` needs to be called
        // before `saveIdentifyWithAttributes` since `saveIdentityWithAttributes`
        // indirectly calls `ConversationController.getConverationId()` which
        // initializes the conversation for the given number (our number) which
        // calls out to the user storage API to get the stored UUID and number
        // information.
        await storage.user.setCredentials({
            uuid: ourUuid,
            number,
            deviceId: (_a = response.deviceId) !== null && _a !== void 0 ? _a : 1,
            deviceName: deviceName !== null && deviceName !== void 0 ? deviceName : undefined,
            password,
        });
        // This needs to be done very early, because it changes how things are saved in the
        //   database. Your identity, for example, in the saveIdentityWithAttributes call
        //   below.
        const conversationId = window.ConversationController.ensureContactIds({
            e164: number,
            uuid: ourUuid,
            highTrust: true,
        });
        if (!conversationId) {
            throw new Error('registrationDone: no conversationId!');
        }
        // update our own identity key, which may have changed
        // if we're relinking after a reinstall on the master device
        await storage.protocol.saveIdentityWithAttributes(new UUID_1.UUID(ourUuid), {
            publicKey: identityKeyPair.pubKey,
            firstUse: true,
            timestamp: Date.now(),
            verified: storage.protocol.VerifiedStatus.VERIFIED,
            nonblockingApproval: true,
        });
        const identityKeyMap = Object.assign(Object.assign({}, (storage.get('identityKeyMap') || {})), { [ourUuid]: {
                pubKey: Bytes.toBase64(identityKeyPair.pubKey),
                privKey: Bytes.toBase64(identityKeyPair.privKey),
            } });
        const registrationIdMap = Object.assign(Object.assign({}, (storage.get('registrationIdMap') || {})), { [ourUuid]: registrationId });
        await storage.put('identityKeyMap', identityKeyMap);
        await storage.put('registrationIdMap', registrationIdMap);
        if (profileKey) {
            await ourProfileKey_1.ourProfileKeyService.set(profileKey);
        }
        if (userAgent) {
            await storage.put('userAgent', userAgent);
        }
        await storage.put('read-receipt-setting', Boolean(readReceipts));
        const regionCode = window.libphonenumber.util.getRegionCodeForNumber(number);
        await storage.put('regionCode', regionCode);
        await storage.protocol.hydrateCaches();
    }
    async clearSessionsAndPreKeys() {
        const store = window.textsecure.storage.protocol;
        log.info('clearing all sessions, prekeys, and signed prekeys');
        await Promise.all([
            store.clearPreKeyStore(),
            store.clearSignedPreKeysStore(),
            store.clearSessionStore(),
        ]);
    }
    async getGroupCredentials(startDay, endDay) {
        return this.server.getGroupCredentials(startDay, endDay);
    }
    // Takes the same object returned by generateKeys
    async confirmKeys(keys) {
        const store = window.textsecure.storage.protocol;
        const key = keys.signedPreKey;
        const confirmed = true;
        if (!key) {
            throw new Error('confirmKeys: signedPreKey is null');
        }
        log.info('confirmKeys: confirming key', key.keyId);
        const ourUuid = window.textsecure.storage.user.getCheckedUuid();
        await store.storeSignedPreKey(ourUuid, key.keyId, key.keyPair, confirmed);
    }
    async generateKeys(count, providedProgressCallback) {
        const progressCallback = typeof providedProgressCallback === 'function'
            ? providedProgressCallback
            : null;
        const startId = window.textsecure.storage.get('maxPreKeyId', 1);
        const signedKeyId = window.textsecure.storage.get('signedKeyId', 1);
        const ourUuid = window.textsecure.storage.user.getCheckedUuid();
        if (typeof startId !== 'number') {
            throw new Error('Invalid maxPreKeyId');
        }
        if (typeof signedKeyId !== 'number') {
            throw new Error('Invalid signedKeyId');
        }
        const store = window.textsecure.storage.protocol;
        return store.getIdentityKeyPair(ourUuid).then(async (identityKey) => {
            if (!identityKey) {
                throw new Error('generateKeys: No identity key pair!');
            }
            const result = {
                preKeys: [],
                identityKey: identityKey.pubKey,
            };
            const promises = [];
            for (let keyId = startId; keyId < startId + count; keyId += 1) {
                promises.push(Promise.resolve((0, Curve_1.generatePreKey)(keyId)).then(async (res) => {
                    await store.storePreKey(ourUuid, res.keyId, res.keyPair);
                    result.preKeys.push({
                        keyId: res.keyId,
                        publicKey: res.keyPair.pubKey,
                    });
                    if (progressCallback) {
                        progressCallback();
                    }
                }));
            }
            promises.push(Promise.resolve((0, Curve_1.generateSignedPreKey)(identityKey, signedKeyId)).then(async (res) => {
                await store.storeSignedPreKey(ourUuid, res.keyId, res.keyPair);
                result.signedPreKey = {
                    keyId: res.keyId,
                    publicKey: res.keyPair.pubKey,
                    signature: res.signature,
                    // server.registerKeys doesn't use keyPair, confirmKeys does
                    keyPair: res.keyPair,
                };
            }));
            promises.push(window.textsecure.storage.put('maxPreKeyId', startId + count));
            promises.push(window.textsecure.storage.put('signedKeyId', signedKeyId + 1));
            return Promise.all(promises).then(async () => 
            // This is primarily for the signed prekey summary it logs out
            this.cleanSignedPreKeys().then(() => result));
        });
    }
    async registrationDone() {
        log.info('registration done');
        this.dispatchEvent(new Event('registration'));
    }
}
exports.default = AccountManager;
