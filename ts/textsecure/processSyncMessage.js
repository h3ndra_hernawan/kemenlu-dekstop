"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.processSyncMessage = void 0;
const normalizeUuid_1 = require("../util/normalizeUuid");
function processUnidentifiedDeliveryStatus(status) {
    const { destinationUuid } = status;
    return Object.assign(Object.assign({}, status), { destinationUuid: destinationUuid
            ? (0, normalizeUuid_1.normalizeUuid)(destinationUuid, 'syncMessage.sent.unidentifiedStatus.destinationUuid')
            : undefined });
}
function processSent(sent) {
    if (!sent) {
        return undefined;
    }
    const { destinationUuid, unidentifiedStatus } = sent;
    return Object.assign(Object.assign({}, sent), { destinationUuid: destinationUuid
            ? (0, normalizeUuid_1.normalizeUuid)(destinationUuid, 'syncMessage.sent.destinationUuid')
            : undefined, unidentifiedStatus: unidentifiedStatus
            ? unidentifiedStatus.map(processUnidentifiedDeliveryStatus)
            : undefined });
}
function processSyncMessage(syncMessage) {
    return Object.assign(Object.assign({}, syncMessage), { sent: processSent(syncMessage.sent) });
}
exports.processSyncMessage = processSyncMessage;
