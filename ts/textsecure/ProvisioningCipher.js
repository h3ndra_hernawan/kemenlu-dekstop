"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const Curve_1 = require("../Curve");
const protobuf_1 = require("../protobuf");
const assert_1 = require("../util/assert");
const normalizeUuid_1 = require("../util/normalizeUuid");
class ProvisioningCipherInner {
    async decrypt(provisionEnvelope) {
        (0, assert_1.strictAssert)(provisionEnvelope.publicKey && provisionEnvelope.body, 'Missing required fields in ProvisionEnvelope');
        const masterEphemeral = provisionEnvelope.publicKey;
        const message = provisionEnvelope.body;
        if (new Uint8Array(message)[0] !== 1) {
            throw new Error('Bad version number on ProvisioningMessage');
        }
        const iv = message.slice(1, 16 + 1);
        const mac = message.slice(message.byteLength - 32, message.byteLength);
        const ivAndCiphertext = message.slice(0, message.byteLength - 32);
        const ciphertext = message.slice(16 + 1, message.byteLength - 32);
        if (!this.keyPair) {
            throw new Error('ProvisioningCipher.decrypt: No keypair!');
        }
        const ecRes = (0, Curve_1.calculateAgreement)(masterEphemeral, this.keyPair.privKey);
        const keys = (0, Crypto_1.deriveSecrets)(ecRes, new Uint8Array(32), Bytes.fromString('TextSecure Provisioning Message'));
        (0, Crypto_1.verifyHmacSha256)(ivAndCiphertext, keys[1], mac, 32);
        const plaintext = (0, Crypto_1.decryptAes256CbcPkcsPadding)(keys[0], ciphertext, iv);
        const provisionMessage = protobuf_1.SignalService.ProvisionMessage.decode(plaintext);
        const privKey = provisionMessage.identityKeyPrivate;
        (0, assert_1.strictAssert)(privKey, 'Missing identityKeyPrivate in ProvisionMessage');
        const keyPair = (0, Curve_1.createKeyPair)(privKey);
        const { uuid } = provisionMessage;
        (0, assert_1.strictAssert)(uuid, 'Missing uuid in provisioning message');
        const ret = {
            identityKeyPair: keyPair,
            number: provisionMessage.number,
            uuid: (0, normalizeUuid_1.normalizeUuid)(uuid, 'ProvisionMessage.uuid'),
            provisioningCode: provisionMessage.provisioningCode,
            userAgent: provisionMessage.userAgent,
            readReceipts: provisionMessage.readReceipts,
        };
        if (provisionMessage.profileKey) {
            ret.profileKey = provisionMessage.profileKey;
        }
        return ret;
    }
    async getPublicKey() {
        if (!this.keyPair) {
            this.keyPair = (0, Curve_1.generateKeyPair)();
        }
        if (!this.keyPair) {
            throw new Error('ProvisioningCipher.decrypt: No keypair!');
        }
        return this.keyPair.pubKey;
    }
}
class ProvisioningCipher {
    constructor() {
        const inner = new ProvisioningCipherInner();
        this.decrypt = inner.decrypt.bind(inner);
        this.getPublicKey = inner.getPublicKey.bind(inner);
    }
}
exports.default = ProvisioningCipher;
