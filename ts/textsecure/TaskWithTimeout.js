"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resumeTasksWithTimeout = exports.suspendTasksWithTimeout = void 0;
const durations = __importStar(require("../util/durations"));
const explodePromise_1 = require("../util/explodePromise");
const errors_1 = require("../types/errors");
const log = __importStar(require("../logging/log"));
const tasks = new Set();
function suspendTasksWithTimeout() {
    log.info(`TaskWithTimeout: suspending ${tasks.size} tasks`);
    for (const task of tasks) {
        task.suspend();
    }
}
exports.suspendTasksWithTimeout = suspendTasksWithTimeout;
function resumeTasksWithTimeout() {
    log.info(`TaskWithTimeout: resuming ${tasks.size} tasks`);
    for (const task of tasks) {
        task.resume();
    }
}
exports.resumeTasksWithTimeout = resumeTasksWithTimeout;
function createTaskWithTimeout(task, id, options = {}) {
    const timeout = options.timeout || 2 * durations.MINUTE;
    const timeoutError = new Error(`${id || ''} task did not complete in time.`);
    return async (...args) => {
        let complete = false;
        let timer;
        const { promise: timerPromise, reject } = (0, explodePromise_1.explodePromise)();
        const startTimer = () => {
            stopTimer();
            if (complete) {
                return;
            }
            timer = setTimeout(() => {
                if (complete) {
                    return;
                }
                complete = true;
                tasks.delete(entry);
                log.error((0, errors_1.toLogFormat)(timeoutError));
                reject(timeoutError);
            }, timeout);
        };
        const stopTimer = () => {
            if (timer) {
                clearTimeout(timer);
                timer = undefined;
            }
        };
        const entry = {
            suspend: stopTimer,
            resume: startTimer,
        };
        tasks.add(entry);
        startTimer();
        let result;
        const run = async () => {
            result = await task(...args);
        };
        try {
            await Promise.race([run(), timerPromise]);
            return result;
        }
        finally {
            complete = true;
            tasks.delete(entry);
            stopTimer();
        }
    };
}
exports.default = createTaskWithTimeout;
