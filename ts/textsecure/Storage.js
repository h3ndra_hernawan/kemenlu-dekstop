"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Storage = void 0;
const User_1 = require("./storage/User");
const Blocked_1 = require("./storage/Blocked");
const assert_1 = require("../util/assert");
const Client_1 = __importDefault(require("../sql/Client"));
const log = __importStar(require("../logging/log"));
class Storage {
    constructor() {
        this.ready = false;
        this.readyCallbacks = [];
        this.items = Object.create(null);
        this.user = new User_1.User(this);
        this.blocked = new Blocked_1.Blocked(this);
        window.storage = this;
    }
    get protocol() {
        (0, assert_1.assert)(this.privProtocol !== undefined, 'SignalProtocolStore not initialized');
        return this.privProtocol;
    }
    set protocol(value) {
        this.privProtocol = value;
    }
    get(key, defaultValue) {
        if (!this.ready) {
            log.warn('Called storage.get before storage is ready. key:', key);
        }
        const item = this.items[key];
        if (item === undefined) {
            return defaultValue;
        }
        return item;
    }
    async put(key, value) {
        var _a;
        if (!this.ready) {
            log.warn('Called storage.put before storage is ready. key:', key);
        }
        this.items[key] = value;
        await window.Signal.Data.createOrUpdateItem({ id: key, value });
        (_a = window.reduxActions) === null || _a === void 0 ? void 0 : _a.items.putItemExternal(key, value);
    }
    async remove(key) {
        var _a;
        if (!this.ready) {
            log.warn('Called storage.remove before storage is ready. key:', key);
        }
        delete this.items[key];
        await Client_1.default.removeItemById(key);
        (_a = window.reduxActions) === null || _a === void 0 ? void 0 : _a.items.removeItemExternal(key);
    }
    // Regular methods
    onready(callback) {
        if (this.ready) {
            callback();
        }
        else {
            this.readyCallbacks.push(callback);
        }
    }
    async fetch() {
        this.reset();
        Object.assign(this.items, await Client_1.default.getAllItems());
        this.ready = true;
        this.callListeners();
    }
    reset() {
        this.ready = false;
        this.items = Object.create(null);
    }
    getItemsState() {
        const state = Object.create(null);
        // TypeScript isn't smart enough to figure out the types automatically.
        const { items } = this;
        const allKeys = Object.keys(items);
        for (const key of allKeys) {
            state[key] = items[key];
        }
        return state;
    }
    callListeners() {
        if (!this.ready) {
            return;
        }
        const callbacks = this.readyCallbacks;
        this.readyCallbacks = [];
        callbacks.forEach(callback => callback());
    }
}
exports.Storage = Storage;
