"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CDSSocket = void 0;
const events_1 = require("events");
const long_1 = __importDefault(require("long"));
const assert_1 = require("../util/assert");
const explodePromise_1 = require("../util/explodePromise");
const durations = __importStar(require("../util/durations"));
const Bytes = __importStar(require("../Bytes"));
const Timers = __importStar(require("../Timers"));
const Crypto_1 = require("../Crypto");
var State;
(function (State) {
    State[State["Handshake"] = 0] = "Handshake";
    State[State["Established"] = 1] = "Established";
    State[State["Closed"] = 2] = "Closed";
})(State || (State = {}));
const HANDSHAKE_TIMEOUT = 10 * durations.SECOND;
const REQUEST_TIMEOUT = 10 * durations.SECOND;
const VERSION = new Uint8Array([0x01]);
const USERNAME_LENGTH = 32;
const PASSWORD_LENGTH = 31;
class CDSSocket extends events_1.EventEmitter {
    constructor(socket, enclaveClient) {
        super();
        this.socket = socket;
        this.enclaveClient = enclaveClient;
        this.state = State.Handshake;
        this.requestQueue = new Array();
        const { promise: finishedHandshake, resolve, reject, } = (0, explodePromise_1.explodePromise)();
        this.finishedHandshake = finishedHandshake;
        const timer = Timers.setTimeout(() => {
            reject(new Error('CDS handshake timed out'));
        }, HANDSHAKE_TIMEOUT);
        socket.on('message', ({ type, binaryData }) => {
            (0, assert_1.strictAssert)(type === 'binary', 'Invalid CDS socket packet');
            (0, assert_1.strictAssert)(binaryData, 'Invalid CDS socket packet');
            if (this.state === State.Handshake) {
                this.enclaveClient.completeHandshake(binaryData);
                this.state = State.Established;
                Timers.clearTimeout(timer);
                resolve();
                return;
            }
            const requestHandler = this.requestQueue.shift();
            (0, assert_1.strictAssert)(requestHandler !== undefined, 'No handler for incoming CDS data');
            requestHandler(this.enclaveClient.establishedRecv(binaryData));
        });
        socket.on('close', (code, reason) => {
            this.state = State.Closed;
            this.emit('close', code, reason);
        });
        socket.on('error', (error) => this.emit('error', error));
        socket.sendBytes(this.enclaveClient.initialRequest());
    }
    close(code, reason) {
        this.socket.close(code, reason);
    }
    async request({ e164s, auth, timeout = REQUEST_TIMEOUT, }) {
        await this.finishedHandshake;
        (0, assert_1.strictAssert)(this.state === State.Established, 'Connection not established');
        const username = Bytes.fromString(auth.username);
        const password = Bytes.fromString(auth.password);
        (0, assert_1.strictAssert)(username.length === USERNAME_LENGTH, 'Invalid username length');
        (0, assert_1.strictAssert)(password.length === PASSWORD_LENGTH, 'Invalid password length');
        const request = Bytes.concatenate([
            VERSION,
            username,
            password,
            ...e164s.map(e164 => {
                // Long.fromString handles numbers with or without a leading '+'
                return new Uint8Array(long_1.default.fromString(e164).toBytesBE());
            }),
        ]);
        const { promise, resolve, reject } = (0, explodePromise_1.explodePromise)();
        const timer = Timers.setTimeout(() => {
            reject(new Error('CDS request timed out'));
        }, timeout);
        this.socket.sendBytes(this.enclaveClient.establishedSend(Buffer.from(request)));
        this.requestQueue.push(resolve);
        (0, assert_1.strictAssert)(this.requestQueue.length === 1, 'Concurrent use of CDS shold not happen');
        const uuids = await promise;
        Timers.clearTimeout(timer);
        return (0, Crypto_1.splitUuids)(uuids);
    }
    on(type, 
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    listener) {
        return super.on(type, listener);
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    emit(type, ...args) {
        return super.emit(type, ...args);
    }
}
exports.CDSSocket = CDSSocket;
