"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
/* eslint-disable max-classes-per-file */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ViewSyncEvent = exports.ReadSyncEvent = exports.VerifiedEvent = exports.StickerPackEvent = exports.KeysEvent = exports.FetchLatestEvent = exports.MessageRequestResponseEvent = exports.ViewOnceOpenSyncEvent = exports.ConfigurationEvent = exports.ViewEvent = exports.ReadEvent = exports.MessageEvent = exports.ProfileKeyUpdateEvent = exports.SentEvent = exports.RetryRequestEvent = exports.DecryptionErrorEvent = exports.DeliveryEvent = exports.ConfirmableEvent = exports.EnvelopeEvent = exports.GroupSyncEvent = exports.GroupEvent = exports.ContactSyncEvent = exports.ContactEvent = exports.ErrorEvent = exports.TypingEvent = exports.ProgressEvent = exports.EmptyEvent = exports.ReconnectEvent = void 0;
class ReconnectEvent extends Event {
    constructor() {
        super('reconnect');
    }
}
exports.ReconnectEvent = ReconnectEvent;
class EmptyEvent extends Event {
    constructor() {
        super('empty');
    }
}
exports.EmptyEvent = EmptyEvent;
class ProgressEvent extends Event {
    constructor({ count }) {
        super('progress');
        this.count = count;
    }
}
exports.ProgressEvent = ProgressEvent;
class TypingEvent extends Event {
    constructor({ sender, senderUuid, senderDevice, typing }) {
        super('typing');
        this.sender = sender;
        this.senderUuid = senderUuid;
        this.senderDevice = senderDevice;
        this.typing = typing;
    }
}
exports.TypingEvent = TypingEvent;
class ErrorEvent extends Event {
    constructor(error) {
        super('error');
        this.error = error;
    }
}
exports.ErrorEvent = ErrorEvent;
class ContactEvent extends Event {
    constructor(contactDetails) {
        super('contact');
        this.contactDetails = contactDetails;
    }
}
exports.ContactEvent = ContactEvent;
class ContactSyncEvent extends Event {
    constructor() {
        super('contactSync');
    }
}
exports.ContactSyncEvent = ContactSyncEvent;
class GroupEvent extends Event {
    constructor(groupDetails) {
        super('group');
        this.groupDetails = groupDetails;
    }
}
exports.GroupEvent = GroupEvent;
class GroupSyncEvent extends Event {
    constructor() {
        super('groupSync');
    }
}
exports.GroupSyncEvent = GroupSyncEvent;
class EnvelopeEvent extends Event {
    constructor(envelope) {
        super('envelope');
        this.envelope = envelope;
    }
}
exports.EnvelopeEvent = EnvelopeEvent;
class ConfirmableEvent extends Event {
    constructor(type, confirm) {
        super(type);
        this.confirm = confirm;
    }
}
exports.ConfirmableEvent = ConfirmableEvent;
class DeliveryEvent extends ConfirmableEvent {
    constructor(deliveryReceipt, confirm) {
        super('delivery', confirm);
        this.deliveryReceipt = deliveryReceipt;
    }
}
exports.DeliveryEvent = DeliveryEvent;
class DecryptionErrorEvent extends ConfirmableEvent {
    constructor(decryptionError, confirm) {
        super('decryption-error', confirm);
        this.decryptionError = decryptionError;
    }
}
exports.DecryptionErrorEvent = DecryptionErrorEvent;
class RetryRequestEvent extends ConfirmableEvent {
    constructor(retryRequest, confirm) {
        super('retry-request', confirm);
        this.retryRequest = retryRequest;
    }
}
exports.RetryRequestEvent = RetryRequestEvent;
class SentEvent extends ConfirmableEvent {
    constructor(data, confirm) {
        super('sent', confirm);
        this.data = data;
    }
}
exports.SentEvent = SentEvent;
class ProfileKeyUpdateEvent extends ConfirmableEvent {
    constructor(data, confirm) {
        super('profileKeyUpdate', confirm);
        this.data = data;
    }
}
exports.ProfileKeyUpdateEvent = ProfileKeyUpdateEvent;
class MessageEvent extends ConfirmableEvent {
    constructor(data, confirm) {
        super('message', confirm);
        this.data = data;
    }
}
exports.MessageEvent = MessageEvent;
class ReadEvent extends ConfirmableEvent {
    constructor(receipt, confirm) {
        super('read', confirm);
        this.receipt = receipt;
    }
}
exports.ReadEvent = ReadEvent;
class ViewEvent extends ConfirmableEvent {
    constructor(receipt, confirm) {
        super('view', confirm);
        this.receipt = receipt;
    }
}
exports.ViewEvent = ViewEvent;
class ConfigurationEvent extends ConfirmableEvent {
    constructor(configuration, confirm) {
        super('configuration', confirm);
        this.configuration = configuration;
    }
}
exports.ConfigurationEvent = ConfigurationEvent;
class ViewOnceOpenSyncEvent extends ConfirmableEvent {
    constructor({ source, sourceUuid, timestamp }, confirm) {
        super('viewOnceOpenSync', confirm);
        this.source = source;
        this.sourceUuid = sourceUuid;
        this.timestamp = timestamp;
    }
}
exports.ViewOnceOpenSyncEvent = ViewOnceOpenSyncEvent;
class MessageRequestResponseEvent extends ConfirmableEvent {
    constructor({ threadE164, threadUuid, messageRequestResponseType, groupId, groupV2Id, }, confirm) {
        super('messageRequestResponse', confirm);
        this.threadE164 = threadE164;
        this.threadUuid = threadUuid;
        this.messageRequestResponseType = messageRequestResponseType;
        this.groupId = groupId;
        this.groupV2Id = groupV2Id;
    }
}
exports.MessageRequestResponseEvent = MessageRequestResponseEvent;
class FetchLatestEvent extends ConfirmableEvent {
    constructor(eventType, confirm) {
        super('fetchLatest', confirm);
        this.eventType = eventType;
    }
}
exports.FetchLatestEvent = FetchLatestEvent;
class KeysEvent extends ConfirmableEvent {
    constructor(storageServiceKey, confirm) {
        super('keys', confirm);
        this.storageServiceKey = storageServiceKey;
    }
}
exports.KeysEvent = KeysEvent;
class StickerPackEvent extends ConfirmableEvent {
    constructor(stickerPacks, confirm) {
        super('sticker-pack', confirm);
        this.stickerPacks = stickerPacks;
    }
}
exports.StickerPackEvent = StickerPackEvent;
class VerifiedEvent extends ConfirmableEvent {
    constructor(verified, confirm) {
        super('verified', confirm);
        this.verified = verified;
    }
}
exports.VerifiedEvent = VerifiedEvent;
class ReadSyncEvent extends ConfirmableEvent {
    constructor(read, confirm) {
        super('readSync', confirm);
        this.read = read;
    }
}
exports.ReadSyncEvent = ReadSyncEvent;
class ViewSyncEvent extends ConfirmableEvent {
    constructor(view, confirm) {
        super('viewSync', confirm);
        this.view = view;
    }
}
exports.ViewSyncEvent = ViewSyncEvent;
