"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.processDataMessage = exports.processDelete = exports.processReaction = exports.processSticker = exports.processPreview = exports.processContact = exports.processQuote = exports.processGroupV2Context = exports.processAttachment = exports.ATTACHMENT_MAX = void 0;
const long_1 = __importDefault(require("long"));
const assert_1 = require("../util/assert");
const dropNull_1 = require("../util/dropNull");
const normalizeNumber_1 = require("../util/normalizeNumber");
const protobuf_1 = require("../protobuf");
const groups_1 = require("../groups");
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const Errors_1 = require("./Errors");
const FLAGS = protobuf_1.SignalService.DataMessage.Flags;
exports.ATTACHMENT_MAX = 32;
function processAttachment(attachment) {
    if (!attachment) {
        return undefined;
    }
    const { cdnId } = attachment;
    const hasCdnId = long_1.default.isLong(cdnId) ? !cdnId.isZero() : Boolean(cdnId);
    return Object.assign(Object.assign({}, (0, dropNull_1.shallowDropNull)(attachment)), { cdnId: hasCdnId ? String(cdnId) : undefined, key: attachment.key ? Bytes.toBase64(attachment.key) : undefined, digest: attachment.digest ? Bytes.toBase64(attachment.digest) : undefined });
}
exports.processAttachment = processAttachment;
function processGroupContext(group) {
    var _a;
    if (!group) {
        return undefined;
    }
    (0, assert_1.strictAssert)(group.id, 'group context without id');
    (0, assert_1.strictAssert)(group.type !== undefined && group.type !== null, 'group context without type');
    const masterKey = (0, Crypto_1.deriveMasterKeyFromGroupV1)(group.id);
    const data = (0, groups_1.deriveGroupFields)(masterKey);
    const derivedGroupV2Id = Bytes.toBase64(data.id);
    const result = {
        id: Bytes.toBinary(group.id),
        type: group.type,
        name: (0, dropNull_1.dropNull)(group.name),
        membersE164: (_a = group.membersE164) !== null && _a !== void 0 ? _a : [],
        avatar: processAttachment(group.avatar),
        derivedGroupV2Id,
    };
    if (result.type === protobuf_1.SignalService.GroupContext.Type.DELIVER) {
        result.name = undefined;
        result.membersE164 = [];
        result.avatar = undefined;
    }
    return result;
}
function processGroupV2Context(groupV2) {
    if (!groupV2) {
        return undefined;
    }
    (0, assert_1.strictAssert)(groupV2.masterKey, 'groupV2 context without masterKey');
    const data = (0, groups_1.deriveGroupFields)(groupV2.masterKey);
    return {
        masterKey: Bytes.toBase64(groupV2.masterKey),
        revision: (0, dropNull_1.dropNull)(groupV2.revision),
        groupChange: groupV2.groupChange
            ? Bytes.toBase64(groupV2.groupChange)
            : undefined,
        id: Bytes.toBase64(data.id),
        secretParams: Bytes.toBase64(data.secretParams),
        publicParams: Bytes.toBase64(data.publicParams),
    };
}
exports.processGroupV2Context = processGroupV2Context;
function processQuote(quote) {
    var _a, _b;
    if (!quote) {
        return undefined;
    }
    return {
        id: (0, normalizeNumber_1.normalizeNumber)((0, dropNull_1.dropNull)(quote.id)),
        authorUuid: (0, dropNull_1.dropNull)(quote.authorUuid),
        text: (0, dropNull_1.dropNull)(quote.text),
        attachments: ((_a = quote.attachments) !== null && _a !== void 0 ? _a : []).map(attachment => {
            return {
                contentType: (0, dropNull_1.dropNull)(attachment.contentType),
                fileName: (0, dropNull_1.dropNull)(attachment.fileName),
                thumbnail: processAttachment(attachment.thumbnail),
            };
        }),
        bodyRanges: (_b = quote.bodyRanges) !== null && _b !== void 0 ? _b : [],
    };
}
exports.processQuote = processQuote;
function processContact(contact) {
    if (!contact) {
        return undefined;
    }
    return contact.map(item => {
        return Object.assign(Object.assign({}, item), { avatar: item.avatar
                ? {
                    avatar: processAttachment(item.avatar.avatar),
                    isProfile: Boolean(item.avatar.isProfile),
                }
                : undefined });
    });
}
exports.processContact = processContact;
function isLinkPreviewDateValid(value) {
    return (typeof value === 'number' &&
        !Number.isNaN(value) &&
        Number.isFinite(value) &&
        value > 0);
}
function cleanLinkPreviewDate(value) {
    const result = (0, normalizeNumber_1.normalizeNumber)(value !== null && value !== void 0 ? value : undefined);
    return isLinkPreviewDateValid(result) ? result : undefined;
}
function processPreview(preview) {
    if (!preview) {
        return undefined;
    }
    return preview.map(item => {
        return {
            url: (0, dropNull_1.dropNull)(item.url),
            title: (0, dropNull_1.dropNull)(item.title),
            image: item.image ? processAttachment(item.image) : undefined,
            description: (0, dropNull_1.dropNull)(item.description),
            date: cleanLinkPreviewDate(item.date),
        };
    });
}
exports.processPreview = processPreview;
function processSticker(sticker) {
    if (!sticker) {
        return undefined;
    }
    return {
        packId: sticker.packId ? Bytes.toHex(sticker.packId) : undefined,
        packKey: sticker.packKey ? Bytes.toBase64(sticker.packKey) : undefined,
        stickerId: (0, normalizeNumber_1.normalizeNumber)((0, dropNull_1.dropNull)(sticker.stickerId)),
        data: processAttachment(sticker.data),
    };
}
exports.processSticker = processSticker;
function processReaction(reaction) {
    if (!reaction) {
        return undefined;
    }
    return {
        emoji: (0, dropNull_1.dropNull)(reaction.emoji),
        remove: Boolean(reaction.remove),
        targetAuthorUuid: (0, dropNull_1.dropNull)(reaction.targetAuthorUuid),
        targetTimestamp: (0, normalizeNumber_1.normalizeNumber)((0, dropNull_1.dropNull)(reaction.targetTimestamp)),
    };
}
exports.processReaction = processReaction;
function processDelete(del) {
    if (!del) {
        return undefined;
    }
    return {
        targetSentTimestamp: (0, normalizeNumber_1.normalizeNumber)((0, dropNull_1.dropNull)(del.targetSentTimestamp)),
    };
}
exports.processDelete = processDelete;
async function processDataMessage(message, envelopeTimestamp) {
    /* eslint-disable no-bitwise */
    var _a, _b, _c, _d;
    // Now that its decrypted, validate the message and clean it up for consumer
    //   processing
    // Note that messages may (generally) only perform one action and we ignore remaining
    //   fields after the first action.
    if (!message.timestamp) {
        throw new Error('Missing timestamp on dataMessage');
    }
    const timestamp = (0, normalizeNumber_1.normalizeNumber)(message.timestamp);
    if (envelopeTimestamp !== timestamp) {
        throw new Error(`Timestamp ${timestamp} in DataMessage did not ` +
            `match envelope timestamp ${envelopeTimestamp}`);
    }
    const result = {
        body: (0, dropNull_1.dropNull)(message.body),
        attachments: ((_a = message.attachments) !== null && _a !== void 0 ? _a : []).map((attachment) => processAttachment(attachment)),
        group: processGroupContext(message.group),
        groupV2: processGroupV2Context(message.groupV2),
        flags: (_b = message.flags) !== null && _b !== void 0 ? _b : 0,
        expireTimer: (_c = message.expireTimer) !== null && _c !== void 0 ? _c : 0,
        profileKey: message.profileKey
            ? Bytes.toBase64(message.profileKey)
            : undefined,
        timestamp,
        quote: processQuote(message.quote),
        contact: processContact(message.contact),
        preview: processPreview(message.preview),
        sticker: processSticker(message.sticker),
        requiredProtocolVersion: (0, normalizeNumber_1.normalizeNumber)((0, dropNull_1.dropNull)(message.requiredProtocolVersion)),
        isViewOnce: Boolean(message.isViewOnce),
        reaction: processReaction(message.reaction),
        delete: processDelete(message.delete),
        bodyRanges: (_d = message.bodyRanges) !== null && _d !== void 0 ? _d : [],
        groupCallUpdate: (0, dropNull_1.dropNull)(message.groupCallUpdate),
    };
    const isEndSession = Boolean(result.flags & FLAGS.END_SESSION);
    const isExpirationTimerUpdate = Boolean(result.flags & FLAGS.EXPIRATION_TIMER_UPDATE);
    const isProfileKeyUpdate = Boolean(result.flags & FLAGS.PROFILE_KEY_UPDATE);
    // The following assertion codifies an assumption: 0 or 1 flags are set, but never
    //   more. This assumption is fine as of this writing, but may not always be.
    const flagCount = [
        isEndSession,
        isExpirationTimerUpdate,
        isProfileKeyUpdate,
    ].filter(Boolean).length;
    (0, assert_1.assert)(flagCount <= 1, `Expected exactly <=1 flags to be set, but got ${flagCount}`);
    if (isEndSession) {
        result.body = undefined;
        result.attachments = [];
        result.group = undefined;
        return result;
    }
    if (isExpirationTimerUpdate) {
        result.body = undefined;
        result.attachments = [];
    }
    else if (isProfileKeyUpdate) {
        result.body = undefined;
        result.attachments = [];
    }
    else if (result.flags !== 0) {
        throw new Error(`Unknown flags in message: ${result.flags}`);
    }
    if (result.group) {
        switch (result.group.type) {
            case protobuf_1.SignalService.GroupContext.Type.UPDATE:
                result.body = undefined;
                result.attachments = [];
                break;
            case protobuf_1.SignalService.GroupContext.Type.QUIT:
                result.body = undefined;
                result.attachments = [];
                break;
            case protobuf_1.SignalService.GroupContext.Type.DELIVER:
                // Cleaned up in `processGroupContext`
                break;
            default: {
                throw new Errors_1.WarnOnlyError(`Unknown group message type: ${result.group.type}`);
            }
        }
    }
    const attachmentCount = result.attachments.length;
    if (attachmentCount > exports.ATTACHMENT_MAX) {
        throw new Error(`Too many attachments: ${attachmentCount} included in one message, ` +
            `max is ${exports.ATTACHMENT_MAX}`);
    }
    return result;
}
exports.processDataMessage = processDataMessage;
