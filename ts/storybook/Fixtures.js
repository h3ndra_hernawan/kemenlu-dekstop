"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.squareStickerUrl = exports.portraitTealUrl = exports.landscapePurpleUrl = exports.landscapeGreenUrl = exports.pngUrl = exports.gifUrl = void 0;
/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-ignore
const giphy_GVNvOUpeYmI7e_gif_1 = __importDefault(require("../../fixtures/giphy-GVNvOUpeYmI7e.gif"));
// @ts-ignore
const freepngs_2cd43b_bed7d1327e88454487397574d87b64dc_mv2_png_1 = __importDefault(require("../../fixtures/freepngs-2cd43b_bed7d1327e88454487397574d87b64dc_mv2.png"));
// @ts-ignore
const _1000x50_green_jpeg_1 = __importDefault(require("../../fixtures/1000x50-green.jpeg"));
// @ts-ignore
const _200x50_purple_png_1 = __importDefault(require("../../fixtures/200x50-purple.png"));
// @ts-ignore
const _50x1000_teal_jpeg_1 = __importDefault(require("../../fixtures/50x1000-teal.jpeg"));
// @ts-ignore
const _512x515_thumbs_up_lincoln_webp_1 = __importDefault(require("../../fixtures/512x515-thumbs-up-lincoln.webp"));
// 320x240
exports.gifUrl = `/${giphy_GVNvOUpeYmI7e_gif_1.default}`;
// 800×1200
exports.pngUrl = `/${freepngs_2cd43b_bed7d1327e88454487397574d87b64dc_mv2_png_1.default}`;
// 1000x50
exports.landscapeGreenUrl = `/${_1000x50_green_jpeg_1.default}`;
// 200x50
exports.landscapePurpleUrl = `/${_200x50_purple_png_1.default}`;
// 50x1000
exports.portraitTealUrl = `/${_50x1000_teal_jpeg_1.default}`;
exports.squareStickerUrl = `/${_512x515_thumbs_up_lincoln_webp_1.default}`;
