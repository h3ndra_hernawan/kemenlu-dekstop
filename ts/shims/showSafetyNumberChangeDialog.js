"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.showSafetyNumberChangeDialog = void 0;
// This file is here temporarily while we're switching off of Backbone into
// React. In the future, and in React-land, please just import and use
// the component directly. This is the thin API layer to bridge the gap
// while we convert things over. Please delete this file once all usages are
// ported over.
const react_1 = __importDefault(require("react"));
const react_dom_1 = require("react-dom");
const SafetyNumberChangeDialog_1 = require("../components/SafetyNumberChangeDialog");
let dialogContainerNode;
function removeDialog() {
    if (!dialogContainerNode) {
        return;
    }
    (0, react_dom_1.unmountComponentAtNode)(dialogContainerNode);
    document.body.removeChild(dialogContainerNode);
    dialogContainerNode = undefined;
}
function showSafetyNumberChangeDialog(options) {
    if (dialogContainerNode) {
        removeDialog();
    }
    dialogContainerNode = document.createElement('div');
    document.body.appendChild(dialogContainerNode);
    (0, react_dom_1.render)(react_1.default.createElement(SafetyNumberChangeDialog_1.SafetyNumberChangeDialog, { confirmText: options.confirmText, contacts: options.contacts.map(contact => contact.format()), i18n: window.i18n, onCancel: () => {
            options.reject();
            removeDialog();
        }, onConfirm: () => {
            options.resolve();
            removeDialog();
        }, renderSafetyNumber: props => {
            return window.Signal.State.Roots.createSafetyNumberViewer(window.reduxStore, props);
        } }), dialogContainerNode);
}
exports.showSafetyNumberChangeDialog = showSafetyNumberChangeDialog;
