"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.themeChanged = void 0;
function themeChanged() {
    if (window.reduxActions && window.reduxActions.user) {
        const theme = window.Events.getThemeSetting();
        window.reduxActions.user.userChanged({
            theme: theme === 'system' ? window.systemTheme : theme,
        });
    }
}
exports.themeChanged = themeChanged;
