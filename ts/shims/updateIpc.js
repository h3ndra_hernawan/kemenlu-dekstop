"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.startUpdate = void 0;
const electron_1 = require("electron");
function startUpdate() {
    electron_1.ipcRenderer.send('start-update');
}
exports.startUpdate = startUpdate;
