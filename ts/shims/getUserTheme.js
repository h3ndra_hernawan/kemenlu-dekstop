"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUserTheme = void 0;
const user_1 = require("../state/selectors/user");
function getUserTheme() {
    return (0, user_1.getTheme)(window.reduxStore.getState());
}
exports.getUserTheme = getUserTheme;
