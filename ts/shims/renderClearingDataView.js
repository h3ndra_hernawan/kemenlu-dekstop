"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.renderClearingDataView = void 0;
const react_1 = __importDefault(require("react"));
const react_dom_1 = require("react-dom");
const ClearingData_1 = require("../components/ClearingData");
const deleteAllData_1 = require("./deleteAllData");
function renderClearingDataView() {
    (0, react_dom_1.render)(react_1.default.createElement(ClearingData_1.ClearingData, { deleteAllData: deleteAllData_1.deleteAllData, i18n: window.i18n }), document.getElementById('app-container'));
}
exports.renderClearingDataView = renderClearingDataView;
