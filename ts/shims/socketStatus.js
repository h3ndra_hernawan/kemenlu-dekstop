"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSocketStatus = void 0;
function getSocketStatus() {
    const { getSocketStatus: getMessageReceiverStatus } = window;
    return getMessageReceiverStatus();
}
exports.getSocketStatus = getSocketStatus;
