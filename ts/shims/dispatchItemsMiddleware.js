"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.dispatchItemsMiddleware = void 0;
const electron_1 = require("electron");
const conversations_1 = require("../state/ducks/conversations");
const dispatchItemsMiddleware = ({ getState }) => next => action => {
    const result = next(action);
    if (action.type === 'items/PUT' ||
        action.type === 'items/PUT_EXTERNAL' ||
        action.type === 'items/REMOVE' ||
        action.type === 'items/REMOVE_EXTERNAL' ||
        action.type === 'items/RESET' ||
        action.type === conversations_1.COLOR_SELECTED ||
        action.type === conversations_1.COLORS_CHANGED) {
        electron_1.ipcRenderer.send('preferences-changed', getState().items);
    }
    return result;
};
exports.dispatchItemsMiddleware = dispatchItemsMiddleware;
