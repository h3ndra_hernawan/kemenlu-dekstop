"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.reloadSelectedConversation = void 0;
function reloadSelectedConversation() {
    const { conversations } = window.reduxStore.getState();
    const { selectedConversationId } = conversations;
    if (!selectedConversationId) {
        return;
    }
    const conversation = window.ConversationController.get(selectedConversationId);
    if (!conversation) {
        return;
    }
    conversation.cachedProps = undefined;
    window.reduxActions.conversations.conversationChanged(conversation.id, conversation.format());
}
exports.reloadSelectedConversation = reloadSelectedConversation;
