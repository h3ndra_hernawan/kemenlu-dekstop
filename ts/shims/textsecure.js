"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendStickerPackSync = void 0;
const handleMessageSend_1 = require("../util/handleMessageSend");
const getSendOptions_1 = require("../util/getSendOptions");
const log = __importStar(require("../logging/log"));
async function sendStickerPackSync(packId, packKey, installed) {
    const { ConversationController, textsecure } = window;
    const ourConversation = ConversationController.getOurConversationOrThrow();
    const sendOptions = await (0, getSendOptions_1.getSendOptions)(ourConversation.attributes, {
        syncMessage: true,
    });
    if (!textsecure.messaging) {
        log.error('shim: Cannot call sendStickerPackSync, textsecure.messaging is falsey');
        return;
    }
    if (window.ConversationController.areWePrimaryDevice()) {
        log.warn('shims/sendStickerPackSync: We are primary device; not sending sync');
        return;
    }
    (0, handleMessageSend_1.handleMessageSend)(textsecure.messaging.sendStickerPackSync([
        {
            packId,
            packKey,
            installed,
        },
    ], sendOptions), { messageIds: [], sendType: 'otherSync' }).catch(error => {
        log.error('shim: Error calling sendStickerPackSync:', error && error.stack ? error.stack : error);
    });
}
exports.sendStickerPackSync = sendStickerPackSync;
