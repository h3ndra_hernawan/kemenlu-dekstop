"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.trigger = void 0;
// Matching Whisper.events.trigger API
// eslint-disable-next-line max-len
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-explicit-any
function trigger(name, ...rest) {
    window.Whisper.events.trigger(name, ...rest);
}
exports.trigger = trigger;
