"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.reloadProfiles = exports.toggleVerification = void 0;
async function toggleVerification(id) {
    const contact = window.getConversations().get(id);
    if (contact) {
        await contact.toggleVerified();
    }
}
exports.toggleVerification = toggleVerification;
async function reloadProfiles(id) {
    const contact = window.getConversations().get(id);
    if (contact) {
        await contact.getProfiles();
    }
}
exports.reloadProfiles = reloadProfiles;
