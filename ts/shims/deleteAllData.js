"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAllData = void 0;
const log = __importStar(require("../logging/log"));
const deleteAllLogs_1 = require("../util/deleteAllLogs");
async function deleteAllData() {
    try {
        await (0, deleteAllLogs_1.deleteAllLogs)();
        log.info('deleteAllData: deleted all logs');
        await window.Signal.Data.removeAll();
        log.info('deleteAllData: emptied database');
        await window.Signal.Data.close();
        log.info('deleteAllData: closed database');
        await window.Signal.Data.removeDB();
        log.info('deleteAllData: removed database');
        await window.Signal.Data.removeOtherData();
        log.info('deleteAllData: removed all other data');
    }
    catch (error) {
        log.error('Something went wrong deleting all data:', error && error.stack ? error.stack : error);
    }
    window.restart();
}
exports.deleteAllData = deleteAllData;
