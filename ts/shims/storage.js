"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.remove = exports.put = void 0;
// Matching window.storage.put API
function put(key, value) {
    window.storage.put(key, value);
}
exports.put = put;
async function remove(key) {
    await window.storage.remove(key);
}
exports.remove = remove;
