"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.bounceAppIconStop = exports.bounceAppIconStart = void 0;
const electron_1 = require("electron");
function bounceAppIconStart(isCritical = false) {
    electron_1.ipcRenderer.send('bounce-app-icon-start', isCritical);
}
exports.bounceAppIconStart = bounceAppIconStart;
function bounceAppIconStop() {
    electron_1.ipcRenderer.send('bounce-app-icon-stop');
}
exports.bounceAppIconStop = bounceAppIconStop;
