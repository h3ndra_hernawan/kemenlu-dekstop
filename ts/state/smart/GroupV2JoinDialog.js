"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartGroupV2JoinDialog = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const GroupV2JoinDialog_1 = require("../../components/GroupV2JoinDialog");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const mapStateToProps = (state, props) => {
    const preJoinConversation = (0, conversations_1.getPreJoinConversation)(state);
    if (!preJoinConversation) {
        throw new Error('smart/GroupV2JoinDialog: No pre-join conversation!');
    }
    return Object.assign(Object.assign(Object.assign({}, props), preJoinConversation), { i18n: (0, user_1.getIntl)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartGroupV2JoinDialog = smart(GroupV2JoinDialog_1.GroupV2JoinDialog);
