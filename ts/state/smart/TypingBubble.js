"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartTypingBubble = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const TypingBubble_1 = require("../../components/conversation/TypingBubble");
const assert_1 = require("../../util/assert");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const badges_1 = require("../selectors/badges");
const mapStateToProps = (state, props) => {
    const { id } = props;
    const conversationSelector = (0, conversations_1.getConversationSelector)(state);
    const conversation = conversationSelector(id);
    if (!conversation) {
        throw new Error(`Did not find conversation ${id} in state!`);
    }
    (0, assert_1.strictAssert)(conversation.typingContactId, 'Missing typing contact ID');
    const typingContact = conversationSelector(conversation.typingContactId);
    return Object.assign(Object.assign({}, typingContact), { badge: (0, badges_1.getPreferredBadgeSelector)(state)(typingContact.badges), conversationType: conversation.type, i18n: (0, user_1.getIntl)(state), theme: (0, user_1.getTheme)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartTypingBubble = smart(TypingBubble_1.TypingBubble);
