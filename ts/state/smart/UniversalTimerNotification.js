"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartUniversalTimerNotification = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const UniversalTimerNotification_1 = require("../../components/conversation/UniversalTimerNotification");
const user_1 = require("../selectors/user");
const items_1 = require("../selectors/items");
const mapStateToProps = (state) => {
    return Object.assign(Object.assign({}, state.updates), { i18n: (0, user_1.getIntl)(state), expireTimer: (0, items_1.getUniversalExpireTimer)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartUniversalTimerNotification = smart(UniversalTimerNotification_1.UniversalTimerNotification);
