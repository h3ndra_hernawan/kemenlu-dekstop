"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.renderReactionPicker = void 0;
const react_1 = __importDefault(require("react"));
const ReactionPicker_1 = require("./ReactionPicker");
const renderReactionPicker = (props) => react_1.default.createElement(ReactionPicker_1.SmartReactionPicker, Object.assign({}, props));
exports.renderReactionPicker = renderReactionPicker;
