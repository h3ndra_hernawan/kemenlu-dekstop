"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartConversationView = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const ConversationView_1 = require("../../components/conversation/ConversationView");
const CompositionArea_1 = require("./CompositionArea");
const ConversationHeader_1 = require("./ConversationHeader");
const Timeline_1 = require("./Timeline");
const mapStateToProps = (_state, props) => {
    const { compositionAreaProps, conversationHeaderProps, timelineProps } = props;
    return {
        renderCompositionArea: () => (react_1.default.createElement(CompositionArea_1.SmartCompositionArea, Object.assign({}, compositionAreaProps))),
        renderConversationHeader: () => (react_1.default.createElement(ConversationHeader_1.SmartConversationHeader, Object.assign({}, conversationHeaderProps))),
        renderTimeline: () => react_1.default.createElement(Timeline_1.SmartTimeline, Object.assign({}, timelineProps)),
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartConversationView = smart(ConversationView_1.ConversationView);
