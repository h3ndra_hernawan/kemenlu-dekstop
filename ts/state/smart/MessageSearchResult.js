"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartMessageSearchResult = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const MessageSearchResult_1 = require("../../components/conversationList/MessageSearchResult");
const user_1 = require("../selectors/user");
const search_1 = require("../selectors/search");
function mapStateToProps(state, ourProps) {
    const { id, style } = ourProps;
    const props = (0, search_1.getMessageSearchResultSelector)(state)(id);
    if (!props) {
        throw new Error('SmartMessageSearchResult: no message was found');
    }
    return Object.assign(Object.assign({}, props), { i18n: (0, user_1.getIntl)(state), style });
}
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartMessageSearchResult = smart(MessageSearchResult_1.MessageSearchResult);
