"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartHeroRow = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const ConversationHero_1 = require("../../components/conversation/ConversationHero");
const badges_1 = require("../selectors/badges");
const user_1 = require("../selectors/user");
const mapStateToProps = (state, props) => {
    const { id } = props;
    const conversation = state.conversations.conversationLookup[id];
    if (!conversation) {
        throw new Error(`Did not find conversation ${id} in state!`);
    }
    return Object.assign(Object.assign({ i18n: (0, user_1.getIntl)(state) }, conversation), { conversationType: conversation.type, badge: (0, badges_1.getPreferredBadgeSelector)(state)(conversation.badges), theme: (0, user_1.getTheme)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartHeroRow = smart(ConversationHero_1.ConversationHero);
