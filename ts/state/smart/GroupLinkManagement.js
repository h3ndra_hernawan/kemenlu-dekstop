"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartGroupLinkManagement = void 0;
const react_redux_1 = require("react-redux");
const GroupLinkManagement_1 = require("../../components/conversation/conversation-details/GroupLinkManagement");
const conversations_1 = require("../selectors/conversations");
const user_1 = require("../selectors/user");
const mapStateToProps = (state, props) => {
    const conversation = (0, conversations_1.getConversationSelector)(state)(props.conversationId);
    const isAdmin = Boolean(conversation === null || conversation === void 0 ? void 0 : conversation.areWeAdmin);
    return Object.assign(Object.assign({}, props), { conversation, i18n: (0, user_1.getIntl)(state), isAdmin });
};
const smart = (0, react_redux_1.connect)(mapStateToProps);
exports.SmartGroupLinkManagement = smart(GroupLinkManagement_1.GroupLinkManagement);
