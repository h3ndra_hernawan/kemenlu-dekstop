"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartApp = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const App_1 = require("../../components/App");
const CallManager_1 = require("./CallManager");
const CustomizingPreferredReactionsModal_1 = require("./CustomizingPreferredReactionsModal");
const GlobalModalContainer_1 = require("./GlobalModalContainer");
const SafetyNumberViewer_1 = require("./SafetyNumberViewer");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const preferredReactions_1 = require("../selectors/preferredReactions");
const actions_1 = require("../actions");
const mapStateToProps = (state) => {
    return Object.assign(Object.assign({}, state.app), { conversationsStoppingMessageSendBecauseOfVerification: (0, conversations_1.getConversationsStoppingMessageSendBecauseOfVerification)(state), i18n: (0, user_1.getIntl)(state), isCustomizingPreferredReactions: (0, preferredReactions_1.getIsCustomizingPreferredReactions)(state), numberOfMessagesPendingBecauseOfVerification: (0, conversations_1.getNumberOfMessagesPendingBecauseOfVerification)(state), renderCallManager: () => react_1.default.createElement(CallManager_1.SmartCallManager, null), renderCustomizingPreferredReactionsModal: () => (react_1.default.createElement(CustomizingPreferredReactionsModal_1.SmartCustomizingPreferredReactionsModal, null)), renderGlobalModalContainer: () => react_1.default.createElement(GlobalModalContainer_1.SmartGlobalModalContainer, null), renderSafetyNumber: (props) => (react_1.default.createElement(SafetyNumberViewer_1.SmartSafetyNumberViewer, Object.assign({}, props))), theme: (0, user_1.getTheme)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartApp = smart(App_1.App);
