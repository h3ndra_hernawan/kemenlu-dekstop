"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartGroupV1MigrationDialog = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const GroupV1MigrationDialog_1 = require("../../components/GroupV1MigrationDialog");
const conversations_1 = require("../selectors/conversations");
const user_1 = require("../selectors/user");
const log = __importStar(require("../../logging/log"));
const mapStateToProps = (state, props) => {
    const getConversation = (0, conversations_1.getConversationSelector)(state);
    const { droppedMemberIds, invitedMemberIds } = props;
    const droppedMembers = droppedMemberIds
        .map(getConversation)
        .filter(Boolean);
    if (droppedMembers.length !== droppedMemberIds.length) {
        log.warn('smart/GroupV1MigrationDialog: droppedMembers length changed');
    }
    const invitedMembers = invitedMemberIds
        .map(getConversation)
        .filter(Boolean);
    if (invitedMembers.length !== invitedMemberIds.length) {
        log.warn('smart/GroupV1MigrationDialog: invitedMembers length changed');
    }
    return Object.assign(Object.assign({}, props), { droppedMembers,
        invitedMembers, i18n: (0, user_1.getIntl)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartGroupV1MigrationDialog = smart(GroupV1MigrationDialog_1.GroupV1MigrationDialog);
