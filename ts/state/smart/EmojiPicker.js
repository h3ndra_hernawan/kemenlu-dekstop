"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartEmojiPicker = void 0;
const React = __importStar(require("react"));
const react_redux_1 = require("react-redux");
const emojis_1 = require("../selectors/emojis");
const emojis_2 = require("../ducks/emojis");
const EmojiPicker_1 = require("../../components/emoji/EmojiPicker");
const user_1 = require("../selectors/user");
const items_1 = require("../selectors/items");
exports.SmartEmojiPicker = React.forwardRef(({ onClickSettings, onPickEmoji, onSetSkinTone, onClose, style }, ref) => {
    const i18n = (0, react_redux_1.useSelector)(user_1.getIntl);
    const skinTone = (0, react_redux_1.useSelector)(state => (0, items_1.getEmojiSkinTone)(state));
    const recentEmojis = (0, emojis_1.useRecentEmojis)();
    const { onUseEmoji } = (0, emojis_2.useActions)();
    const handlePickEmoji = React.useCallback(data => {
        onUseEmoji({ shortName: data.shortName });
        onPickEmoji(data);
    }, [onUseEmoji, onPickEmoji]);
    return (React.createElement(EmojiPicker_1.EmojiPicker, { ref: ref, i18n: i18n, skinTone: skinTone, onClickSettings: onClickSettings, onSetSkinTone: onSetSkinTone, onPickEmoji: handlePickEmoji, recentEmojis: recentEmojis, onClose: onClose, style: style }));
});
