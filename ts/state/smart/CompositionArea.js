"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartCompositionArea = void 0;
const react_redux_1 = require("react-redux");
const lodash_1 = require("lodash");
const actions_1 = require("../actions");
const CompositionArea_1 = require("../../components/CompositionArea");
const isConversationSMSOnly_1 = require("../../util/isConversationSMSOnly");
const dropNull_1 = require("../../util/dropNull");
const emojis_1 = require("../selectors/emojis");
const user_1 = require("../selectors/user");
const items_1 = require("../selectors/items");
const conversations_1 = require("../selectors/conversations");
const message_1 = require("../selectors/message");
const stickers_1 = require("../selectors/stickers");
const mapStateToProps = (state, props) => {
    const { id, handleClickQuotedMessage } = props;
    const conversationSelector = (0, conversations_1.getConversationSelector)(state);
    const conversation = conversationSelector(id);
    if (!conversation) {
        throw new Error(`Conversation id ${id} not found!`);
    }
    const { announcementsOnly, areWeAdmin, draftText, draftBodyRanges } = conversation;
    const receivedPacks = (0, stickers_1.getReceivedStickerPacks)(state);
    const installedPacks = (0, stickers_1.getInstalledStickerPacks)(state);
    const blessedPacks = (0, stickers_1.getBlessedStickerPacks)(state);
    const knownPacks = (0, stickers_1.getKnownStickerPacks)(state);
    const installedPack = (0, stickers_1.getRecentlyInstalledStickerPack)(state);
    const recentStickers = (0, stickers_1.getRecentStickers)(state);
    const showIntroduction = (0, lodash_1.get)(state.items, ['showStickersIntroduction'], false);
    const showPickerHint = Boolean((0, lodash_1.get)(state.items, ['showStickerPickerHint'], false) &&
        receivedPacks.length > 0);
    const { attachments: draftAttachments, linkPreviewLoading, linkPreviewResult, quotedMessage, shouldSendHighQualityAttachments, } = state.composer;
    const recentEmojis = (0, emojis_1.selectRecentEmojis)(state);
    return Object.assign(Object.assign({ 
        // Base
        conversationId: id, i18n: (0, user_1.getIntl)(state), theme: (0, user_1.getTheme)(state), 
        // AudioCapture
        errorDialogAudioRecorderType: state.audioRecorder.errorDialogAudioRecorderType, recordingState: state.audioRecorder.recordingState, 
        // AttachmentsList
        draftAttachments,
        // MediaQualitySelector
        shouldSendHighQualityAttachments,
        // StagedLinkPreview
        linkPreviewLoading,
        linkPreviewResult, 
        // Quote
        quotedMessageProps: quotedMessage
            ? (0, message_1.getPropsForQuote)(quotedMessage, {
                conversationSelector,
                ourConversationId: (0, user_1.getUserConversationId)(state),
            })
            : undefined, onClickQuotedMessage: () => {
            var _a;
            const messageId = (_a = quotedMessage === null || quotedMessage === void 0 ? void 0 : quotedMessage.quote) === null || _a === void 0 ? void 0 : _a.messageId;
            if (messageId) {
                handleClickQuotedMessage(messageId);
            }
        }, 
        // Emojis
        recentEmojis, skinTone: (0, items_1.getEmojiSkinTone)(state), 
        // Stickers
        receivedPacks,
        installedPack,
        blessedPacks,
        knownPacks,
        installedPacks,
        recentStickers,
        showIntroduction,
        showPickerHint }, conversation), { conversationType: conversation.type, isSMSOnly: Boolean((0, isConversationSMSOnly_1.isConversationSMSOnly)(conversation)), isFetchingUUID: conversation.isFetchingUUID, isMissingMandatoryProfileSharing: (0, conversations_1.isMissingRequiredProfileSharing)(conversation), 
        // Groups
        announcementsOnly,
        areWeAdmin, groupAdmins: (0, conversations_1.getGroupAdminsSelector)(state)(conversation.id), draftText: (0, dropNull_1.dropNull)(draftText), draftBodyRanges });
};
const dispatchPropsMap = Object.assign(Object.assign({}, actions_1.mapDispatchToProps), { onSetSkinTone: (tone) => actions_1.mapDispatchToProps.putItem('skinTone', tone), clearShowIntroduction: () => actions_1.mapDispatchToProps.removeItem('showStickersIntroduction'), clearShowPickerHint: () => actions_1.mapDispatchToProps.removeItem('showStickerPickerHint'), onPickEmoji: actions_1.mapDispatchToProps.onUseEmoji });
const smart = (0, react_redux_1.connect)(mapStateToProps, dispatchPropsMap);
exports.SmartCompositionArea = smart(CompositionArea_1.CompositionArea);
