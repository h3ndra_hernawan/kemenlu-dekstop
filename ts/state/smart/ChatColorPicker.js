"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartChatColorPicker = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const ChatColorPicker_1 = require("../../components/ChatColorPicker");
const conversations_1 = require("../selectors/conversations");
const items_1 = require("../selectors/items");
const user_1 = require("../selectors/user");
const mapStateToProps = (state, props) => {
    var _a, _b;
    const defaultConversationColor = (0, items_1.getDefaultConversationColor)(state);
    const colorValues = props.conversationId
        ? (0, conversations_1.getConversationSelector)(state)(props.conversationId)
        : {
            conversationColor: defaultConversationColor.color,
            customColorId: (_a = defaultConversationColor.customColorData) === null || _a === void 0 ? void 0 : _a.id,
            customColor: (_b = defaultConversationColor.customColorData) === null || _b === void 0 ? void 0 : _b.value,
        };
    const { customColors } = state.items;
    return Object.assign(Object.assign({}, props), { customColors: customColors ? customColors.colors : {}, getConversationsWithCustomColor: (colorId) => Promise.resolve((0, conversations_1.getConversationsWithCustomColorSelector)(state)(colorId)), i18n: (0, user_1.getIntl)(state), selectedColor: colorValues.conversationColor, selectedCustomColor: {
            id: colorValues.customColorId,
            value: colorValues.customColor,
        } });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartChatColorPicker = smart(ChatColorPicker_1.ChatColorPicker);
