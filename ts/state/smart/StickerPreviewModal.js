"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartStickerPreviewModal = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const StickerPreviewModal_1 = require("../../components/stickers/StickerPreviewModal");
const user_1 = require("../selectors/user");
const stickers_1 = require("../selectors/stickers");
const mapStateToProps = (state, props) => {
    const { packId } = props;
    const stickersPath = (0, user_1.getStickersPath)(state);
    const tempPath = (0, user_1.getTempPath)(state);
    const packs = (0, stickers_1.getPacks)(state);
    const blessedPacks = (0, stickers_1.getBlessedPacks)(state);
    const pack = packs[packId];
    return Object.assign(Object.assign({}, props), { pack: pack
            ? (0, stickers_1.translatePackFromDB)(pack, packs, blessedPacks, stickersPath, tempPath)
            : undefined, i18n: (0, user_1.getIntl)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartStickerPreviewModal = smart(StickerPreviewModal_1.StickerPreviewModal);
