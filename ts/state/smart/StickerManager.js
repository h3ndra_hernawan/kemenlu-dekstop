"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartStickerManager = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const StickerManager_1 = require("../../components/stickers/StickerManager");
const user_1 = require("../selectors/user");
const stickers_1 = require("../selectors/stickers");
const mapStateToProps = (state) => {
    const blessedPacks = (0, stickers_1.getBlessedStickerPacks)(state);
    const receivedPacks = (0, stickers_1.getReceivedStickerPacks)(state);
    const installedPacks = (0, stickers_1.getInstalledStickerPacks)(state);
    const knownPacks = (0, stickers_1.getKnownStickerPacks)(state);
    return {
        blessedPacks,
        receivedPacks,
        installedPacks,
        knownPacks,
        i18n: (0, user_1.getIntl)(state),
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartStickerManager = smart(StickerManager_1.StickerManager);
