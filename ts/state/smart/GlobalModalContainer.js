"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartGlobalModalContainer = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const GlobalModalContainer_1 = require("../../components/GlobalModalContainer");
const ProfileEditorModal_1 = require("./ProfileEditorModal");
const ContactModal_1 = require("./ContactModal");
const SafetyNumberModal_1 = require("./SafetyNumberModal");
const user_1 = require("../selectors/user");
function renderProfileEditor() {
    return react_1.default.createElement(ProfileEditorModal_1.SmartProfileEditorModal, null);
}
function renderContactModal() {
    return react_1.default.createElement(ContactModal_1.SmartContactModal, null);
}
const mapStateToProps = (state) => {
    const i18n = (0, user_1.getIntl)(state);
    return Object.assign(Object.assign({}, state.globalModals), { i18n,
        renderContactModal,
        renderProfileEditor, renderSafetyNumber: () => (react_1.default.createElement(SafetyNumberModal_1.SmartSafetyNumberModal, { contactID: String(state.globalModals.safetyNumberModalContactId) })) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartGlobalModalContainer = smart(GlobalModalContainer_1.GlobalModalContainer);
