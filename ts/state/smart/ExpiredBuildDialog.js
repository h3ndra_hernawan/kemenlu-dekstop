"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartExpiredBuildDialog = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const DialogExpiredBuild_1 = require("../../components/DialogExpiredBuild");
const user_1 = require("../selectors/user");
const mapStateToProps = (state, ownProps) => {
    return Object.assign({ hasExpired: state.expiration.hasExpired, i18n: (0, user_1.getIntl)(state) }, ownProps);
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartExpiredBuildDialog = smart(DialogExpiredBuild_1.DialogExpiredBuild);
