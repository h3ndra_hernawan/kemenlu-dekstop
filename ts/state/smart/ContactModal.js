"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartContactModal = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const ContactModal_1 = require("../../components/conversation/ContactModal");
const user_1 = require("../selectors/user");
const badges_1 = require("../selectors/badges");
const conversations_1 = require("../selectors/conversations");
const mapStateToProps = (state) => {
    const { contactId, conversationId } = state.globalModals.contactModalState || {};
    const currentConversation = (0, conversations_1.getConversationSelector)(state)(conversationId);
    const contact = (0, conversations_1.getConversationSelector)(state)(contactId);
    const areWeAdmin = currentConversation && currentConversation.areWeAdmin
        ? currentConversation.areWeAdmin
        : false;
    let isMember = false;
    let isAdmin = false;
    if (contact && currentConversation && currentConversation.memberships) {
        currentConversation.memberships.forEach(membership => {
            if (membership.uuid === contact.uuid) {
                isMember = true;
                isAdmin = membership.isAdmin;
            }
        });
    }
    return {
        areWeAdmin,
        badges: (0, badges_1.getBadgesSelector)(state)(contact.badges),
        contact,
        conversationId,
        i18n: (0, user_1.getIntl)(state),
        isAdmin,
        isMember,
        theme: (0, user_1.getTheme)(state),
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartContactModal = smart(ContactModal_1.ContactModal);
