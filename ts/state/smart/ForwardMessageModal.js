"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartForwardMessageModal = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const ForwardMessageModal_1 = require("../../components/ForwardMessageModal");
const conversations_1 = require("../selectors/conversations");
const linkPreviews_1 = require("../selectors/linkPreviews");
const user_1 = require("../selectors/user");
const items_1 = require("../selectors/items");
const emojis_1 = require("../selectors/emojis");
const mapStateToProps = (state, props) => {
    const { attachments, conversationId, doForwardMessage, isSticker, messageBody, onClose, onEditorStateChange, onTextTooLong, } = props;
    const candidateConversations = (0, conversations_1.getAllComposableConversations)(state);
    const recentEmojis = (0, emojis_1.selectRecentEmojis)(state);
    const skinTone = (0, items_1.getEmojiSkinTone)(state);
    const linkPreview = (0, linkPreviews_1.getLinkPreview)(state);
    return {
        attachments,
        candidateConversations,
        conversationId,
        doForwardMessage,
        i18n: (0, user_1.getIntl)(state),
        isSticker,
        linkPreview,
        messageBody,
        onClose,
        onEditorStateChange,
        recentEmojis,
        skinTone,
        onTextTooLong,
        theme: (0, user_1.getTheme)(state),
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, Object.assign(Object.assign({}, actions_1.mapDispatchToProps), { onPickEmoji: actions_1.mapDispatchToProps.onUseEmoji }));
exports.SmartForwardMessageModal = smart(ForwardMessageModal_1.ForwardMessageModal);
