"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartCallManager = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const lodash_1 = require("lodash");
const actions_1 = require("../actions");
const CallManager_1 = require("../../components/CallManager");
const calling_1 = require("../../services/calling");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const calling_2 = require("../ducks/calling");
const calling_3 = require("../selectors/calling");
const isGroupCallOutboundRingEnabled_1 = require("../../util/isGroupCallOutboundRingEnabled");
const Calling_1 = require("../../types/Calling");
const missingCaseError_1 = require("../../util/missingCaseError");
const CallingDeviceSelection_1 = require("./CallingDeviceSelection");
const SafetyNumberViewer_1 = require("./SafetyNumberViewer");
const callingTones_1 = require("../../util/callingTones");
const bounceAppIcon_1 = require("../../shims/bounceAppIcon");
const notifications_1 = require("../../services/notifications");
const log = __importStar(require("../../logging/log"));
function renderDeviceSelection() {
    return react_1.default.createElement(CallingDeviceSelection_1.SmartCallingDeviceSelection, null);
}
function renderSafetyNumberViewer(props) {
    return react_1.default.createElement(SafetyNumberViewer_1.SmartSafetyNumberViewer, Object.assign({}, props));
}
const getGroupCallVideoFrameSource = calling_1.calling.getGroupCallVideoFrameSource.bind(calling_1.calling);
async function notifyForCall(title, isVideoCall) {
    const shouldNotify = !window.isActive() && window.Events.getCallSystemNotification();
    if (!shouldNotify) {
        return;
    }
    let notificationTitle;
    const notificationSetting = notifications_1.notificationService.getNotificationSetting();
    switch (notificationSetting) {
        case notifications_1.NotificationSetting.Off:
        case notifications_1.NotificationSetting.NoNameOrMessage:
            notificationTitle = notifications_1.FALLBACK_NOTIFICATION_TITLE;
            break;
        case notifications_1.NotificationSetting.NameOnly:
        case notifications_1.NotificationSetting.NameAndMessage:
            notificationTitle = title;
            break;
        default:
            log.error((0, missingCaseError_1.missingCaseError)(notificationSetting));
            notificationTitle = notifications_1.FALLBACK_NOTIFICATION_TITLE;
            break;
    }
    notifications_1.notificationService.notify({
        title: notificationTitle,
        icon: isVideoCall
            ? 'images/icons/v2/video-solid-24.svg'
            : 'images/icons/v2/phone-right-solid-24.svg',
        message: window.i18n(isVideoCall ? 'incomingVideoCall' : 'incomingAudioCall'),
        onNotificationClick: () => {
            window.showWindow();
        },
        silent: false,
    });
}
const playRingtone = callingTones_1.callingTones.playRingtone.bind(callingTones_1.callingTones);
const stopRingtone = callingTones_1.callingTones.stopRingtone.bind(callingTones_1.callingTones);
const mapStateToActiveCallProp = (state) => {
    const { calling } = state;
    const { activeCallState } = calling;
    if (!activeCallState) {
        return undefined;
    }
    const call = (0, calling_2.getActiveCall)(calling);
    if (!call) {
        log.error('There was an active call state but no corresponding call');
        return undefined;
    }
    const conversationSelector = (0, conversations_1.getConversationSelector)(state);
    const conversation = conversationSelector(activeCallState.conversationId);
    if (!conversation) {
        log.error('The active call has no corresponding conversation');
        return undefined;
    }
    const conversationSelectorByUuid = (0, lodash_1.memoize)(uuid => {
        const conversationId = window.ConversationController.ensureContactIds({
            uuid,
        });
        return conversationId ? conversationSelector(conversationId) : undefined;
    });
    const baseResult = {
        conversation,
        hasLocalAudio: activeCallState.hasLocalAudio,
        hasLocalVideo: activeCallState.hasLocalVideo,
        isInSpeakerView: activeCallState.isInSpeakerView,
        joinedAt: activeCallState.joinedAt,
        outgoingRing: activeCallState.outgoingRing,
        pip: activeCallState.pip,
        presentingSource: activeCallState.presentingSource,
        presentingSourcesAvailable: activeCallState.presentingSourcesAvailable,
        settingsDialogOpen: activeCallState.settingsDialogOpen,
        showNeedsScreenRecordingPermissionsWarning: Boolean(activeCallState.showNeedsScreenRecordingPermissionsWarning),
        showParticipantsList: activeCallState.showParticipantsList,
    };
    switch (call.callMode) {
        case Calling_1.CallMode.Direct:
            if (call.isIncoming &&
                (call.callState === Calling_1.CallState.Prering ||
                    call.callState === Calling_1.CallState.Ringing)) {
                return;
            }
            return Object.assign(Object.assign({}, baseResult), { callEndedReason: call.callEndedReason, callMode: Calling_1.CallMode.Direct, callState: call.callState, peekedParticipants: [], remoteParticipants: [
                    {
                        hasRemoteVideo: Boolean(call.hasRemoteVideo),
                        presenting: Boolean(call.isSharingScreen),
                        title: conversation.title,
                        uuid: conversation.uuid,
                    },
                ] });
        case Calling_1.CallMode.Group: {
            const conversationsWithSafetyNumberChanges = [];
            const groupMembers = [];
            const remoteParticipants = [];
            const peekedParticipants = [];
            const { memberships = [] } = conversation;
            for (let i = 0; i < memberships.length; i += 1) {
                const { uuid } = memberships[i];
                const member = conversationSelector(uuid);
                if (!member) {
                    log.error('Group member has no corresponding conversation');
                    continue;
                }
                groupMembers.push(member);
            }
            for (let i = 0; i < call.remoteParticipants.length; i += 1) {
                const remoteParticipant = call.remoteParticipants[i];
                const remoteConversation = conversationSelectorByUuid(remoteParticipant.uuid);
                if (!remoteConversation) {
                    log.error('Remote participant has no corresponding conversation');
                    continue;
                }
                remoteParticipants.push(Object.assign(Object.assign({}, remoteConversation), { demuxId: remoteParticipant.demuxId, hasRemoteAudio: remoteParticipant.hasRemoteAudio, hasRemoteVideo: remoteParticipant.hasRemoteVideo, presenting: remoteParticipant.presenting, sharingScreen: remoteParticipant.sharingScreen, speakerTime: remoteParticipant.speakerTime, videoAspectRatio: remoteParticipant.videoAspectRatio }));
            }
            for (let i = 0; i < activeCallState.safetyNumberChangedUuids.length; i += 1) {
                const uuid = activeCallState.safetyNumberChangedUuids[i];
                const remoteConversation = conversationSelectorByUuid(uuid);
                if (!remoteConversation) {
                    log.error('Remote participant has no corresponding conversation');
                    continue;
                }
                conversationsWithSafetyNumberChanges.push(remoteConversation);
            }
            for (let i = 0; i < call.peekInfo.uuids.length; i += 1) {
                const peekedParticipantUuid = call.peekInfo.uuids[i];
                const peekedConversation = conversationSelectorByUuid(peekedParticipantUuid);
                if (!peekedConversation) {
                    log.error('Remote participant has no corresponding conversation');
                    continue;
                }
                peekedParticipants.push(peekedConversation);
            }
            return Object.assign(Object.assign({}, baseResult), { callMode: Calling_1.CallMode.Group, connectionState: call.connectionState, conversationsWithSafetyNumberChanges, deviceCount: call.peekInfo.deviceCount, groupMembers, joinState: call.joinState, maxDevices: call.peekInfo.maxDevices, peekedParticipants,
                remoteParticipants });
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(call);
    }
};
const mapStateToIncomingCallProp = (state) => {
    var _a;
    const call = (0, calling_3.getIncomingCall)(state);
    if (!call) {
        return undefined;
    }
    const conversation = (0, conversations_1.getConversationSelector)(state)(call.conversationId);
    if (!conversation) {
        log.error('The incoming call has no corresponding conversation');
        return undefined;
    }
    switch (call.callMode) {
        case Calling_1.CallMode.Direct:
            return {
                callMode: Calling_1.CallMode.Direct,
                conversation,
                isVideoCall: call.isVideoCall,
            };
        case Calling_1.CallMode.Group: {
            if (!call.ringerUuid) {
                log.error('The incoming group call has no ring state');
                return undefined;
            }
            const conversationSelector = (0, conversations_1.getConversationSelector)(state);
            const ringer = conversationSelector(call.ringerUuid);
            const otherMembersRung = ((_a = conversation.sortedGroupMembers) !== null && _a !== void 0 ? _a : []).filter(c => c.id !== ringer.id && !c.isMe);
            return {
                callMode: Calling_1.CallMode.Group,
                conversation,
                otherMembersRung,
                ringer,
            };
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(call);
    }
};
const mapStateToProps = (state) => ({
    activeCall: mapStateToActiveCallProp(state),
    bounceAppIconStart: bounceAppIcon_1.bounceAppIconStart,
    bounceAppIconStop: bounceAppIcon_1.bounceAppIconStop,
    availableCameras: state.calling.availableCameras,
    getGroupCallVideoFrameSource,
    i18n: (0, user_1.getIntl)(state),
    isGroupCallOutboundRingEnabled: (0, isGroupCallOutboundRingEnabled_1.isGroupCallOutboundRingEnabled)(),
    incomingCall: mapStateToIncomingCallProp(state),
    me: Object.assign(Object.assign({}, (0, conversations_1.getMe)(state)), { 
        // `getMe` returns a `ConversationType` which might not have a UUID, at least
        //   according to the type. This ensures one is set.
        uuid: (0, user_1.getUserUuid)(state) }),
    notifyForCall,
    playRingtone,
    stopRingtone,
    renderDeviceSelection,
    renderSafetyNumberViewer,
});
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartCallManager = smart(CallManager_1.CallManager);
