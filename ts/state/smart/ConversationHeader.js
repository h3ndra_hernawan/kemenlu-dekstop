"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartConversationHeader = void 0;
const react_redux_1 = require("react-redux");
const lodash_1 = require("lodash");
const ConversationHeader_1 = require("../../components/conversation/ConversationHeader");
const badges_1 = require("../selectors/badges");
const conversations_1 = require("../selectors/conversations");
const Calling_1 = require("../../types/Calling");
const conversations_2 = require("../ducks/conversations");
const calling_1 = require("../ducks/calling");
const user_1 = require("../selectors/user");
const getOwn_1 = require("../../util/getOwn");
const missingCaseError_1 = require("../../util/missingCaseError");
const isConversationSMSOnly_1 = require("../../util/isConversationSMSOnly");
const getOutgoingCallButtonStyle = (conversation, state) => {
    const { calling } = state;
    if ((0, calling_1.getActiveCall)(calling)) {
        return ConversationHeader_1.OutgoingCallButtonStyle.None;
    }
    const conversationCallMode = (0, conversations_2.getConversationCallMode)(conversation);
    switch (conversationCallMode) {
        case Calling_1.CallMode.None:
            return ConversationHeader_1.OutgoingCallButtonStyle.None;
        case Calling_1.CallMode.Direct:
            return ConversationHeader_1.OutgoingCallButtonStyle.Both;
        case Calling_1.CallMode.Group: {
            const call = (0, getOwn_1.getOwn)(calling.callsByConversation, conversation.id);
            if ((call === null || call === void 0 ? void 0 : call.callMode) === Calling_1.CallMode.Group &&
                (0, calling_1.isAnybodyElseInGroupCall)(call.peekInfo, (0, user_1.getUserUuid)(state))) {
                return ConversationHeader_1.OutgoingCallButtonStyle.Join;
            }
            return ConversationHeader_1.OutgoingCallButtonStyle.JustVideo;
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(conversationCallMode);
    }
};
const mapStateToProps = (state, ownProps) => {
    const { id } = ownProps;
    const conversation = (0, conversations_1.getConversationSelector)(state)(id);
    if (!conversation) {
        throw new Error('Could not find conversation');
    }
    return Object.assign(Object.assign({}, (0, lodash_1.pick)(conversation, [
        'acceptedMessageRequest',
        'announcementsOnly',
        'areWeAdmin',
        'avatarPath',
        'canChangeTimer',
        'color',
        'expireTimer',
        'groupVersion',
        'isArchived',
        'isMe',
        'isPinned',
        'isVerified',
        'left',
        'markedUnread',
        'muteExpiresAt',
        'name',
        'phoneNumber',
        'profileName',
        'sharedGroupNames',
        'title',
        'type',
        'unblurredAvatarPath',
    ])), { badge: (0, badges_1.getPreferredBadgeSelector)(state)(conversation.badges), conversationTitle: state.conversations.selectedConversationTitle, isMissingMandatoryProfileSharing: (0, conversations_1.isMissingRequiredProfileSharing)(conversation), isSMSOnly: (0, isConversationSMSOnly_1.isConversationSMSOnly)(conversation), i18n: (0, user_1.getIntl)(state), showBackButton: state.conversations.selectedConversationPanelDepth > 0, outgoingCallButtonStyle: getOutgoingCallButtonStyle(conversation, state), theme: (0, user_1.getTheme)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, {});
exports.SmartConversationHeader = smart(ConversationHeader_1.ConversationHeader);
