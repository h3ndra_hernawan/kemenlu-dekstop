"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartGlobalAudioProvider = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const GlobalAudioContext_1 = require("../../components/GlobalAudioContext");
const audioPlayer_1 = require("../selectors/audioPlayer");
const conversations_1 = require("../selectors/conversations");
const mapStateToProps = (state) => {
    return {
        conversationId: (0, conversations_1.getSelectedConversationId)(state),
        isPaused: (0, audioPlayer_1.isPaused)(state),
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartGlobalAudioProvider = smart(GlobalAudioContext_1.GlobalAudioProvider);
