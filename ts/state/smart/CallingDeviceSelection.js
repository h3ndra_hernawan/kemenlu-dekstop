"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartCallingDeviceSelection = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const CallingDeviceSelection_1 = require("../../components/CallingDeviceSelection");
const user_1 = require("../selectors/user");
const mapStateToProps = (state) => {
    const { availableMicrophones, availableSpeakers, selectedMicrophone, selectedSpeaker, availableCameras, selectedCamera, } = state.calling;
    return {
        availableCameras,
        availableMicrophones,
        availableSpeakers,
        i18n: (0, user_1.getIntl)(state),
        selectedCamera,
        selectedMicrophone,
        selectedSpeaker,
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartCallingDeviceSelection = smart(CallingDeviceSelection_1.CallingDeviceSelection);
