"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartConversationNotificationsSettings = void 0;
const react_redux_1 = require("react-redux");
const ConversationNotificationsSettings_1 = require("../../components/conversation/conversation-details/ConversationNotificationsSettings");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const assert_1 = require("../../util/assert");
const mapStateToProps = (state, props) => {
    const { conversationId, setDontNotifyForMentionsIfMuted, setMuteExpiration } = props;
    const conversationSelector = (0, conversations_1.getConversationByIdSelector)(state);
    const conversation = conversationSelector(conversationId);
    (0, assert_1.strictAssert)(conversation, 'Expected a conversation to be found');
    return {
        conversationType: conversation.type,
        dontNotifyForMentionsIfMuted: Boolean(conversation.dontNotifyForMentionsIfMuted),
        i18n: (0, user_1.getIntl)(state),
        muteExpiresAt: conversation.muteExpiresAt,
        setDontNotifyForMentionsIfMuted,
        setMuteExpiration,
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, {});
exports.SmartConversationNotificationsSettings = smart(ConversationNotificationsSettings_1.ConversationNotificationsSettings);
