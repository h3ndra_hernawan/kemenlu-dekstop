"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartTimeline = void 0;
const lodash_1 = require("lodash");
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const memoizee_1 = __importDefault(require("memoizee"));
const actions_1 = require("../actions");
const Timeline_1 = require("../../components/conversation/Timeline");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const TimelineItem_1 = require("./TimelineItem");
const TypingBubble_1 = require("./TypingBubble");
const LastSeenIndicator_1 = require("./LastSeenIndicator");
const HeroRow_1 = require("./HeroRow");
const TimelineLoadingRow_1 = require("./TimelineLoadingRow");
const renderAudioAttachment_1 = require("./renderAudioAttachment");
const renderEmojiPicker_1 = require("./renderEmojiPicker");
const renderReactionPicker_1 = require("./renderReactionPicker");
const getOwn_1 = require("../../util/getOwn");
const assert_1 = require("../../util/assert");
const missingCaseError_1 = require("../../util/missingCaseError");
const getGroupMemberships_1 = require("../../util/getGroupMemberships");
const groupMemberNameCollisions_1 = require("../../util/groupMemberNameCollisions");
const contactSpoofing_1 = require("../../util/contactSpoofing");
const createBoundOnHeightChange = (0, memoizee_1.default)((onHeightChange, messageId) => {
    return () => onHeightChange(messageId);
}, { max: 500 });
function renderItem({ actionProps, containerElementRef, containerWidthBreakpoint, conversationId, messageId, nextMessageId, onHeightChange, previousMessageId, }) {
    return (react_1.default.createElement(TimelineItem_1.SmartTimelineItem, Object.assign({}, actionProps, { containerElementRef: containerElementRef, containerWidthBreakpoint: containerWidthBreakpoint, conversationId: conversationId, messageId: messageId, previousMessageId: previousMessageId, nextMessageId: nextMessageId, onHeightChange: createBoundOnHeightChange(onHeightChange, messageId), renderEmojiPicker: renderEmojiPicker_1.renderEmojiPicker, renderReactionPicker: renderReactionPicker_1.renderReactionPicker, renderAudioAttachment: renderAudioAttachment_1.renderAudioAttachment })));
}
function renderLastSeenIndicator(id) {
    return react_1.default.createElement(LastSeenIndicator_1.SmartLastSeenIndicator, { id: id });
}
function renderHeroRow(id, onHeightChange, unblurAvatar, updateSharedGroups) {
    return (react_1.default.createElement(HeroRow_1.SmartHeroRow, { id: id, onHeightChange: onHeightChange, unblurAvatar: unblurAvatar, updateSharedGroups: updateSharedGroups }));
}
function renderLoadingRow(id) {
    return react_1.default.createElement(TimelineLoadingRow_1.SmartTimelineLoadingRow, { id: id });
}
function renderTypingBubble(id) {
    return react_1.default.createElement(TypingBubble_1.SmartTypingBubble, { id: id });
}
const getWarning = (conversation, state) => {
    switch (conversation.type) {
        case 'direct':
            if (!conversation.acceptedMessageRequest && !conversation.isBlocked) {
                const getConversationsWithTitle = (0, conversations_1.getConversationsByTitleSelector)(state);
                const conversationsWithSameTitle = getConversationsWithTitle(conversation.title);
                (0, assert_1.assert)(conversationsWithSameTitle.length, 'Expected at least 1 conversation with the same title (this one)');
                const safeConversation = conversationsWithSameTitle.find(otherConversation => otherConversation.acceptedMessageRequest &&
                    otherConversation.type === 'direct' &&
                    otherConversation.id !== conversation.id);
                if (safeConversation) {
                    return {
                        type: contactSpoofing_1.ContactSpoofingType.DirectConversationWithSameTitle,
                        safeConversation,
                    };
                }
            }
            return undefined;
        case 'group': {
            if (conversation.left || conversation.groupVersion !== 2) {
                return undefined;
            }
            const getConversationByUuid = (0, conversations_1.getConversationByUuidSelector)(state);
            const { memberships } = (0, getGroupMemberships_1.getGroupMemberships)(conversation, getConversationByUuid);
            const groupNameCollisions = (0, groupMemberNameCollisions_1.getCollisionsFromMemberships)(memberships);
            const hasGroupMembersWithSameName = !(0, lodash_1.isEmpty)(groupNameCollisions);
            if (hasGroupMembersWithSameName) {
                return {
                    type: contactSpoofing_1.ContactSpoofingType.MultipleGroupMembersWithSameTitle,
                    acknowledgedGroupNameCollisions: conversation.acknowledgedGroupNameCollisions || {},
                    groupNameCollisions: (0, groupMemberNameCollisions_1.dehydrateCollisionsWithConversations)(groupNameCollisions),
                };
            }
            return undefined;
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(conversation.type);
    }
};
const getContactSpoofingReview = (selectedConversationId, state) => {
    const { contactSpoofingReview } = state.conversations;
    if (!contactSpoofingReview) {
        return undefined;
    }
    const conversationSelector = (0, conversations_1.getConversationSelector)(state);
    const getConversationByUuid = (0, conversations_1.getConversationByUuidSelector)(state);
    const currentConversation = conversationSelector(selectedConversationId);
    switch (contactSpoofingReview.type) {
        case contactSpoofing_1.ContactSpoofingType.DirectConversationWithSameTitle:
            return {
                type: contactSpoofing_1.ContactSpoofingType.DirectConversationWithSameTitle,
                possiblyUnsafeConversation: currentConversation,
                safeConversation: conversationSelector(contactSpoofingReview.safeConversationId),
            };
        case contactSpoofing_1.ContactSpoofingType.MultipleGroupMembersWithSameTitle: {
            const { memberships } = (0, getGroupMemberships_1.getGroupMemberships)(currentConversation, getConversationByUuid);
            const groupNameCollisions = (0, groupMemberNameCollisions_1.getCollisionsFromMemberships)(memberships);
            const previouslyAcknowledgedTitlesById = (0, groupMemberNameCollisions_1.invertIdsByTitle)(currentConversation.acknowledgedGroupNameCollisions || {});
            const collisionInfoByTitle = (0, lodash_1.mapValues)(groupNameCollisions, conversations => conversations.map(conversation => ({
                conversation,
                oldName: (0, getOwn_1.getOwn)(previouslyAcknowledgedTitlesById, conversation.id),
            })));
            return {
                type: contactSpoofing_1.ContactSpoofingType.MultipleGroupMembersWithSameTitle,
                collisionInfoByTitle,
            };
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(contactSpoofingReview);
    }
};
const mapStateToProps = (state, props) => {
    const { id } = props, actions = __rest(props, ["id"]);
    const conversation = (0, conversations_1.getConversationSelector)(state)(id);
    const conversationMessages = (0, conversations_1.getConversationMessagesSelector)(state)(id);
    const selectedMessage = (0, conversations_1.getSelectedMessage)(state);
    return Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({ id }, (0, lodash_1.pick)(conversation, [
        'areWeAdmin',
        'unreadCount',
        'typingContactId',
        'isGroupV1AndDisabled',
    ])), { isIncomingMessageRequest: Boolean(conversation.messageRequestsEnabled &&
            !conversation.acceptedMessageRequest) }), conversationMessages), { invitedContactsForNewlyCreatedGroup: (0, conversations_1.getInvitedContactsForNewlyCreatedGroup)(state), selectedMessageId: selectedMessage ? selectedMessage.id : undefined, warning: getWarning(conversation, state), contactSpoofingReview: getContactSpoofingReview(id, state), i18n: (0, user_1.getIntl)(state), renderItem,
        renderLastSeenIndicator,
        renderHeroRow,
        renderLoadingRow,
        renderTypingBubble }), actions);
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartTimeline = smart(Timeline_1.Timeline);
