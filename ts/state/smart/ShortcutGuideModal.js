"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartShortcutGuideModal = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const ShortcutGuideModal_1 = require("../../components/ShortcutGuideModal");
const lib_1 = require("../../components/stickers/lib");
const user_1 = require("../selectors/user");
const stickers_1 = require("../selectors/stickers");
const mapStateToProps = (state, props) => {
    const { close } = props;
    const blessedPacks = (0, stickers_1.getBlessedStickerPacks)(state);
    const installedPacks = (0, stickers_1.getInstalledStickerPacks)(state);
    const knownPacks = (0, stickers_1.getKnownStickerPacks)(state);
    const receivedPacks = (0, stickers_1.getReceivedStickerPacks)(state);
    const hasInstalledStickers = (0, lib_1.countStickers)({
        knownPacks,
        blessedPacks,
        installedPacks,
        receivedPacks,
    }) > 0;
    const platform = (0, user_1.getPlatform)(state);
    return {
        close,
        hasInstalledStickers,
        platform,
        i18n: (0, user_1.getIntl)(state),
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartShortcutGuideModal = smart(ShortcutGuideModal_1.ShortcutGuideModal);
