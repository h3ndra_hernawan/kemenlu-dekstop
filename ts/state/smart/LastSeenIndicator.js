"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartLastSeenIndicator = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const LastSeenIndicator_1 = require("../../components/conversation/LastSeenIndicator");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const mapStateToProps = (state, props) => {
    const { id } = props;
    const conversation = (0, conversations_1.getConversationMessagesSelector)(state)(id);
    if (!conversation) {
        throw new Error(`Did not find conversation ${id} in state!`);
    }
    const { totalUnread } = conversation;
    return {
        count: totalUnread,
        i18n: (0, user_1.getIntl)(state),
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartLastSeenIndicator = smart(LastSeenIndicator_1.LastSeenIndicator);
