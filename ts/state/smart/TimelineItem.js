"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartTimelineItem = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const TimelineItem_1 = require("../../components/conversation/TimelineItem");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const ContactName_1 = require("./ContactName");
const UniversalTimerNotification_1 = require("./UniversalTimerNotification");
function renderContact(conversationId) {
    return react_1.default.createElement(ContactName_1.SmartContactName, { conversationId: conversationId });
}
function renderUniversalTimerNotification() {
    return react_1.default.createElement(UniversalTimerNotification_1.SmartUniversalTimerNotification, null);
}
const mapStateToProps = (state, props) => {
    const { containerElementRef, conversationId, messageId, nextMessageId, previousMessageId, } = props;
    const messageSelector = (0, conversations_1.getMessageSelector)(state);
    const item = messageSelector(messageId);
    const previousItem = previousMessageId
        ? messageSelector(previousMessageId)
        : undefined;
    const nextItem = nextMessageId ? messageSelector(nextMessageId) : undefined;
    const selectedMessage = (0, conversations_1.getSelectedMessage)(state);
    const isSelected = Boolean(selectedMessage && messageId === selectedMessage.id);
    const conversation = (0, conversations_1.getConversationSelector)(state)(conversationId);
    return {
        item,
        previousItem,
        nextItem,
        id: messageId,
        containerElementRef,
        conversationId,
        conversationColor: conversation === null || conversation === void 0 ? void 0 : conversation.conversationColor,
        customColor: conversation === null || conversation === void 0 ? void 0 : conversation.customColor,
        isSelected,
        renderContact,
        renderUniversalTimerNotification,
        i18n: (0, user_1.getIntl)(state),
        interactionMode: (0, user_1.getInteractionMode)(state),
        theme: (0, user_1.getTheme)(state),
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartTimelineItem = smart(TimelineItem_1.TimelineItem);
