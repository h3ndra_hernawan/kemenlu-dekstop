"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartReactionPicker = void 0;
const React = __importStar(require("react"));
const react_redux_1 = require("react-redux");
const preferredReactions_1 = require("../ducks/preferredReactions");
const items_1 = require("../ducks/items");
const user_1 = require("../selectors/user");
const items_2 = require("../selectors/items");
const ReactionPicker_1 = require("../../components/conversation/ReactionPicker");
exports.SmartReactionPicker = React.forwardRef((props, ref) => {
    const { openCustomizePreferredReactionsModal } = (0, preferredReactions_1.useActions)();
    const { onSetSkinTone } = (0, items_1.useActions)();
    const i18n = (0, react_redux_1.useSelector)(user_1.getIntl);
    const preferredReactionEmoji = (0, react_redux_1.useSelector)(items_2.getPreferredReactionEmoji);
    return (React.createElement(ReactionPicker_1.ReactionPicker, Object.assign({ i18n: i18n, onSetSkinTone: onSetSkinTone, openCustomizePreferredReactionsModal: openCustomizePreferredReactionsModal, preferredReactionEmoji: preferredReactionEmoji, ref: ref }, props)));
});
