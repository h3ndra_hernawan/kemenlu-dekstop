"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartUpdateDialog = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const DialogUpdate_1 = require("../../components/DialogUpdate");
const user_1 = require("../selectors/user");
const network_1 = require("../selectors/network");
const mapStateToProps = (state, ownProps) => {
    return Object.assign(Object.assign(Object.assign({}, state.updates), { hasNetworkDialog: (0, network_1.hasNetworkDialog)(state), i18n: (0, user_1.getIntl)(state), currentVersion: window.getVersion() }), ownProps);
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartUpdateDialog = smart(DialogUpdate_1.DialogUpdate);
