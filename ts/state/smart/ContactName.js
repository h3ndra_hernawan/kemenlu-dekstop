"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartContactName = void 0;
const React = __importStar(require("react"));
const react_redux_1 = require("react-redux");
const ContactName_1 = require("../../components/conversation/ContactName");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const SmartContactName = props => {
    const { conversationId } = props;
    const i18n = (0, react_redux_1.useSelector)(user_1.getIntl);
    const getConversation = (0, react_redux_1.useSelector)(conversations_1.getConversationSelector);
    const conversation = getConversation(conversationId) || {
        title: i18n('unknownContact'),
    };
    return (React.createElement(ContactName_1.ContactName, { firstName: conversation.firstName, title: conversation.title }));
};
exports.SmartContactName = SmartContactName;
