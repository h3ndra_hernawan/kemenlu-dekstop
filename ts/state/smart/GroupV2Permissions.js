"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartGroupV2Permissions = void 0;
const react_redux_1 = require("react-redux");
const GroupV2Permissions_1 = require("../../components/conversation/conversation-details/GroupV2Permissions");
const conversations_1 = require("../selectors/conversations");
const user_1 = require("../selectors/user");
const mapStateToProps = (state, props) => {
    const conversation = (0, conversations_1.getConversationSelector)(state)(props.conversationId);
    return Object.assign(Object.assign({}, props), { conversation, i18n: (0, user_1.getIntl)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps);
exports.SmartGroupV2Permissions = smart(GroupV2Permissions_1.GroupV2Permissions);
