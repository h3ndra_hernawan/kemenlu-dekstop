"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.renderAudioAttachment = void 0;
const react_1 = __importDefault(require("react"));
const GlobalAudioContext_1 = require("../../components/GlobalAudioContext");
const MessageAudio_1 = require("./MessageAudio");
function renderAudioAttachment(props) {
    return (react_1.default.createElement(GlobalAudioContext_1.GlobalAudioContext.Consumer, null, globalAudioProps => {
        return (globalAudioProps && (react_1.default.createElement(MessageAudio_1.SmartMessageAudio, Object.assign({}, props, globalAudioProps))));
    }));
}
exports.renderAudioAttachment = renderAudioAttachment;
