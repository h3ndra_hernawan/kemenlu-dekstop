"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartLeftPane = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const lodash_1 = require("lodash");
const actions_1 = require("../actions");
const LeftPane_1 = require("../../components/LeftPane");
const missingCaseError_1 = require("../../util/missingCaseError");
const conversationsEnums_1 = require("../ducks/conversationsEnums");
const search_1 = require("../selectors/search");
const user_1 = require("../selectors/user");
const badges_1 = require("../selectors/badges");
const items_1 = require("../selectors/items");
const conversations_1 = require("../selectors/conversations");
const ExpiredBuildDialog_1 = require("./ExpiredBuildDialog");
const MainHeader_1 = require("./MainHeader");
const MessageSearchResult_1 = require("./MessageSearchResult");
const NetworkStatus_1 = require("./NetworkStatus");
const RelinkDialog_1 = require("./RelinkDialog");
const UpdateDialog_1 = require("./UpdateDialog");
const CaptchaDialog_1 = require("./CaptchaDialog");
function renderExpiredBuildDialog(props) {
    return react_1.default.createElement(ExpiredBuildDialog_1.SmartExpiredBuildDialog, Object.assign({}, props));
}
function renderMainHeader() {
    return react_1.default.createElement(MainHeader_1.SmartMainHeader, null);
}
function renderMessageSearchResult(id) {
    return react_1.default.createElement(MessageSearchResult_1.SmartMessageSearchResult, { id: id });
}
function renderNetworkStatus(props) {
    return react_1.default.createElement(NetworkStatus_1.SmartNetworkStatus, Object.assign({}, props));
}
function renderRelinkDialog(props) {
    return react_1.default.createElement(RelinkDialog_1.SmartRelinkDialog, Object.assign({}, props));
}
function renderUpdateDialog(props) {
    return react_1.default.createElement(UpdateDialog_1.SmartUpdateDialog, Object.assign({}, props));
}
function renderCaptchaDialog({ onSkip }) {
    return react_1.default.createElement(CaptchaDialog_1.SmartCaptchaDialog, { onSkip: onSkip });
}
const getModeSpecificProps = (state) => {
    const composerStep = (0, conversations_1.getComposerStep)(state);
    switch (composerStep) {
        case undefined:
            if ((0, conversations_1.getShowArchived)(state)) {
                const { archivedConversations } = (0, conversations_1.getLeftPaneLists)(state);
                const searchConversation = (0, search_1.getSearchConversation)(state);
                const searchTerm = (0, search_1.getQuery)(state);
                return Object.assign({ mode: LeftPane_1.LeftPaneMode.Archive, archivedConversations,
                    searchConversation,
                    searchTerm }, (searchConversation && searchTerm ? (0, search_1.getSearchResults)(state) : {}));
            }
            if ((0, search_1.isSearching)(state)) {
                const primarySendsSms = Boolean((0, lodash_1.get)(state.items, ['primarySendsSms'], false));
                return Object.assign({ mode: LeftPane_1.LeftPaneMode.Search, primarySendsSms }, (0, search_1.getSearchResults)(state));
            }
            return Object.assign({ mode: LeftPane_1.LeftPaneMode.Inbox, isAboutToSearchInAConversation: (0, search_1.getIsSearchingInAConversation)(state), startSearchCounter: (0, search_1.getStartSearchCounter)(state) }, (0, conversations_1.getLeftPaneLists)(state));
        case conversationsEnums_1.ComposerStep.StartDirectConversation:
            return {
                mode: LeftPane_1.LeftPaneMode.Compose,
                composeContacts: (0, conversations_1.getFilteredComposeContacts)(state),
                composeGroups: (0, conversations_1.getFilteredComposeGroups)(state),
                regionCode: (0, user_1.getRegionCode)(state),
                searchTerm: (0, conversations_1.getComposerConversationSearchTerm)(state),
                isUsernamesEnabled: (0, items_1.getUsernamesEnabled)(state),
                isFetchingUsername: (0, conversations_1.getIsFetchingUsername)(state),
            };
        case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
            return {
                mode: LeftPane_1.LeftPaneMode.ChooseGroupMembers,
                candidateContacts: (0, conversations_1.getFilteredCandidateContactsForNewGroup)(state),
                cantAddContactForModal: (0, conversations_1.getCantAddContactForModal)(state),
                isShowingRecommendedGroupSizeModal: (0, conversations_1.getRecommendedGroupSizeModalState)(state) ===
                    conversationsEnums_1.OneTimeModalState.Showing,
                isShowingMaximumGroupSizeModal: (0, conversations_1.getMaximumGroupSizeModalState)(state) === conversationsEnums_1.OneTimeModalState.Showing,
                searchTerm: (0, conversations_1.getComposerConversationSearchTerm)(state),
                selectedContacts: (0, conversations_1.getComposeSelectedContacts)(state),
            };
        case conversationsEnums_1.ComposerStep.SetGroupMetadata:
            return {
                mode: LeftPane_1.LeftPaneMode.SetGroupMetadata,
                groupAvatar: (0, conversations_1.getComposeGroupAvatar)(state),
                groupName: (0, conversations_1.getComposeGroupName)(state),
                groupExpireTimer: (0, conversations_1.getComposeGroupExpireTimer)(state),
                hasError: (0, conversations_1.hasGroupCreationError)(state),
                isCreating: (0, conversations_1.isCreatingGroup)(state),
                isEditingAvatar: (0, conversations_1.isEditingAvatar)(state),
                selectedContacts: (0, conversations_1.getComposeSelectedContacts)(state),
                userAvatarData: (0, conversations_1.getComposeAvatarData)(state),
            };
        default:
            throw (0, missingCaseError_1.missingCaseError)(composerStep);
    }
};
const mapStateToProps = (state) => {
    var _a;
    return {
        modeSpecificProps: getModeSpecificProps(state),
        badgesById: (0, badges_1.getBadgesById)(state),
        canResizeLeftPane: window.Signal.RemoteConfig.isEnabled('desktop.internalUser'),
        preferredWidthFromStorage: (0, items_1.getPreferredLeftPaneWidth)(state),
        selectedConversationId: (0, conversations_1.getSelectedConversationId)(state),
        selectedMessageId: (_a = (0, conversations_1.getSelectedMessage)(state)) === null || _a === void 0 ? void 0 : _a.id,
        showArchived: (0, conversations_1.getShowArchived)(state),
        i18n: (0, user_1.getIntl)(state),
        regionCode: (0, user_1.getRegionCode)(state),
        challengeStatus: state.network.challengeStatus,
        renderExpiredBuildDialog,
        renderMainHeader,
        renderMessageSearchResult,
        renderNetworkStatus,
        renderRelinkDialog,
        renderUpdateDialog,
        renderCaptchaDialog,
        theme: (0, user_1.getTheme)(state),
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartLeftPane = smart(LeftPane_1.LeftPane);
