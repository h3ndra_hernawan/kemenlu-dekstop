"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartProfileEditorModal = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const ProfileEditorModal_1 = require("../../components/ProfileEditorModal");
const user_1 = require("../selectors/user");
const items_1 = require("../selectors/items");
const conversations_1 = require("../selectors/conversations");
const emojis_1 = require("../selectors/emojis");
function mapStateToProps(state) {
    const { avatarPath, avatars: userAvatarData = [], aboutText, aboutEmoji, color, firstName, familyName, id: conversationId, username, } = (0, conversations_1.getMe)(state);
    const recentEmojis = (0, emojis_1.selectRecentEmojis)(state);
    const skinTone = (0, items_1.getEmojiSkinTone)(state);
    const isUsernameFlagEnabled = (0, items_1.getUsernamesEnabled)(state);
    return {
        aboutEmoji,
        aboutText,
        avatarPath,
        color,
        conversationId,
        familyName,
        firstName: String(firstName),
        hasError: state.globalModals.profileEditorHasError,
        i18n: (0, user_1.getIntl)(state),
        isUsernameFlagEnabled,
        recentEmojis,
        skinTone,
        userAvatarData,
        username,
        usernameSaveState: (0, conversations_1.getUsernameSaveState)(state),
    };
}
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartProfileEditorModal = smart(ProfileEditorModal_1.ProfileEditorModal);
