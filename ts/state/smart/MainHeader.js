"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartMainHeader = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const MainHeader_1 = require("../../components/MainHeader");
const badges_1 = require("../selectors/badges");
const search_1 = require("../selectors/search");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const mapStateToProps = (state) => {
    const me = (0, conversations_1.getMe)(state);
    return Object.assign(Object.assign({ disabled: state.network.challengeStatus !== 'idle', hasPendingUpdate: Boolean(state.updates.didSnooze), searchTerm: (0, search_1.getQuery)(state), searchConversation: (0, search_1.getSearchConversation)(state), selectedConversation: (0, conversations_1.getSelectedConversation)(state), startSearchCounter: (0, search_1.getStartSearchCounter)(state), regionCode: (0, user_1.getRegionCode)(state), ourConversationId: (0, user_1.getUserConversationId)(state), ourNumber: (0, user_1.getUserNumber)(state), ourUuid: (0, user_1.getUserUuid)(state) }, me), { badge: (0, badges_1.getPreferredBadgeSelector)(state)(me.badges), theme: (0, user_1.getTheme)(state), i18n: (0, user_1.getIntl)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartMainHeader = smart(MainHeader_1.MainHeader);
