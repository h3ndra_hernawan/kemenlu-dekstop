"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartCaptchaDialog = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const CaptchaDialog_1 = require("../../components/CaptchaDialog");
const user_1 = require("../selectors/user");
const network_1 = require("../selectors/network");
const challenge_1 = require("../../challenge");
const mapStateToProps = (state) => {
    return Object.assign(Object.assign({}, state.updates), { isPending: (0, network_1.isChallengePending)(state), i18n: (0, user_1.getIntl)(state), onContinue() {
            document.location.href = (0, challenge_1.getChallengeURL)();
        } });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartCaptchaDialog = smart(CaptchaDialog_1.CaptchaDialog);
