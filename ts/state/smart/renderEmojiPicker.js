"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.renderEmojiPicker = void 0;
const react_1 = __importDefault(require("react"));
const EmojiPicker_1 = require("./EmojiPicker");
function renderEmojiPicker({ ref, onClickSettings, onPickEmoji, onSetSkinTone, onClose, style, }) {
    return (react_1.default.createElement(EmojiPicker_1.SmartEmojiPicker, { ref: ref, onClickSettings: onClickSettings, onPickEmoji: onPickEmoji, onSetSkinTone: onSetSkinTone, onClose: onClose, style: style }));
}
exports.renderEmojiPicker = renderEmojiPicker;
