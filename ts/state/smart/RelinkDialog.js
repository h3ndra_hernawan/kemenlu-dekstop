"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartRelinkDialog = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const DialogRelink_1 = require("../../components/DialogRelink");
const user_1 = require("../selectors/user");
const registration_1 = require("../../util/registration");
const mapStateToProps = (state, ownProps) => {
    return Object.assign({ i18n: (0, user_1.getIntl)(state), isRegistrationDone: (0, registration_1.isDone)() }, ownProps);
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartRelinkDialog = smart(DialogRelink_1.DialogRelink);
