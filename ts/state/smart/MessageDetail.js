"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartMessageDetail = void 0;
const react_redux_1 = require("react-redux");
const MessageDetail_1 = require("../../components/conversation/MessageDetail");
const actions_1 = require("../actions");
const badges_1 = require("../selectors/badges");
const user_1 = require("../selectors/user");
const renderAudioAttachment_1 = require("./renderAudioAttachment");
const renderEmojiPicker_1 = require("./renderEmojiPicker");
const renderReactionPicker_1 = require("./renderReactionPicker");
const conversations_1 = require("../selectors/conversations");
const mapStateToProps = (state, props) => {
    const { contacts, errors, message, receivedAt, sentAt, showSafetyNumber, displayTapToViewMessage, kickOffAttachmentDownload, markAttachmentAsCorrupted, markViewed, openConversation, openLink, reactToMessage, replyToMessage, retrySend, showContactDetail, showContactModal, showExpiredIncomingTapToViewToast, showExpiredOutgoingTapToViewToast, showForwardMessageModal, showVisualAttachment, } = props;
    const contactNameColor = message.conversationType === 'group'
        ? (0, conversations_1.getContactNameColorSelector)(state)(message.conversationId, message.author.id)
        : undefined;
    const getPreferredBadge = (0, badges_1.getPreferredBadgeSelector)(state);
    return {
        contacts,
        contactNameColor,
        errors,
        message,
        receivedAt,
        sentAt,
        getPreferredBadge,
        i18n: (0, user_1.getIntl)(state),
        interactionMode: (0, user_1.getInteractionMode)(state),
        theme: (0, user_1.getTheme)(state),
        showSafetyNumber,
        displayTapToViewMessage,
        kickOffAttachmentDownload,
        markAttachmentAsCorrupted,
        markViewed,
        openConversation,
        openLink,
        reactToMessage,
        renderAudioAttachment: renderAudioAttachment_1.renderAudioAttachment,
        renderEmojiPicker: renderEmojiPicker_1.renderEmojiPicker,
        renderReactionPicker: renderReactionPicker_1.renderReactionPicker,
        replyToMessage,
        retrySend,
        showContactDetail,
        showContactModal,
        showExpiredIncomingTapToViewToast,
        showExpiredOutgoingTapToViewToast,
        showForwardMessageModal,
        showVisualAttachment,
    };
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartMessageDetail = smart(MessageDetail_1.MessageDetail);
