"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartPendingInvites = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const PendingInvites_1 = require("../../components/conversation/conversation-details/PendingInvites");
const user_1 = require("../selectors/user");
const conversations_1 = require("../selectors/conversations");
const getGroupMemberships_1 = require("../../util/getGroupMemberships");
const assert_1 = require("../../util/assert");
const mapStateToProps = (state, props) => {
    const conversationSelector = (0, conversations_1.getConversationByIdSelector)(state);
    const conversationByUuidSelector = (0, conversations_1.getConversationByUuidSelector)(state);
    const conversation = conversationSelector(props.conversationId);
    (0, assert_1.assert)(conversation, '<SmartPendingInvites> expected a conversation to be found');
    return Object.assign(Object.assign(Object.assign({}, props), (0, getGroupMemberships_1.getGroupMemberships)(conversation, conversationByUuidSelector)), { conversation, i18n: (0, user_1.getIntl)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartPendingInvites = smart(PendingInvites_1.PendingInvites);
