"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartConversationDetails = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const ConversationDetails_1 = require("../../components/conversation/conversation-details/ConversationDetails");
const conversations_1 = require("../selectors/conversations");
const getGroupMemberships_1 = require("../../util/getGroupMemberships");
const user_1 = require("../selectors/user");
const badges_1 = require("../selectors/badges");
const assert_1 = require("../../util/assert");
const protobuf_1 = require("../../protobuf");
const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
const mapStateToProps = (state, props) => {
    const conversationSelector = (0, conversations_1.getConversationByIdSelector)(state);
    const conversation = conversationSelector(props.conversationId);
    (0, assert_1.assert)(conversation, '<SmartConversationDetails> expected a conversation to be found');
    const canEditGroupInfo = Boolean(conversation.canEditGroupInfo);
    const isAdmin = Boolean(conversation.areWeAdmin);
    const candidateContactsToAdd = (0, conversations_1.getCandidateContactsForNewGroup)(state);
    const hasGroupLink = Boolean(conversation.groupLink) &&
        conversation.accessControlAddFromInviteLink !== ACCESS_ENUM.UNSATISFIABLE;
    const conversationByUuidSelector = (0, conversations_1.getConversationByUuidSelector)(state);
    const groupMemberships = (0, getGroupMemberships_1.getGroupMemberships)(conversation, conversationByUuidSelector);
    const badges = (0, badges_1.getBadgesSelector)(state)(conversation.badges);
    const preferredBadgeByConversation = {};
    const getPreferredBadge = (0, badges_1.getPreferredBadgeSelector)(state);
    groupMemberships.memberships.forEach(({ member }) => {
        const preferredBadge = getPreferredBadge(member.badges);
        if (preferredBadge) {
            preferredBadgeByConversation[member.id] = preferredBadge;
        }
    });
    return Object.assign(Object.assign(Object.assign(Object.assign({}, props), { badges,
        canEditGroupInfo,
        candidateContactsToAdd,
        conversation, i18n: (0, user_1.getIntl)(state), isAdmin,
        preferredBadgeByConversation }), groupMemberships), { userAvatarData: conversation.avatars || [], hasGroupLink, isGroup: conversation.type === 'group', theme: (0, user_1.getTheme)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartConversationDetails = smart(ConversationDetails_1.ConversationDetails);
