"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmartSafetyNumberModal = void 0;
const react_redux_1 = require("react-redux");
const actions_1 = require("../actions");
const SafetyNumberModal_1 = require("../../components/SafetyNumberModal");
const safetyNumber_1 = require("../selectors/safetyNumber");
const conversations_1 = require("../selectors/conversations");
const user_1 = require("../selectors/user");
const mapStateToProps = (state, props) => {
    return Object.assign(Object.assign(Object.assign({}, props), (0, safetyNumber_1.getContactSafetyNumber)(state, props)), { contact: (0, conversations_1.getConversationSelector)(state)(props.contactID), i18n: (0, user_1.getIntl)(state) });
};
const smart = (0, react_redux_1.connect)(mapStateToProps, actions_1.mapDispatchToProps);
exports.SmartSafetyNumberModal = smart(SafetyNumberModal_1.SafetyNumberModal);
