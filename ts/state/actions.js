"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapDispatchToProps = exports.actionCreators = void 0;
const accounts_1 = require("./ducks/accounts");
const app_1 = require("./ducks/app");
const audioPlayer_1 = require("./ducks/audioPlayer");
const audioRecorder_1 = require("./ducks/audioRecorder");
const badges_1 = require("./ducks/badges");
const calling_1 = require("./ducks/calling");
const composer_1 = require("./ducks/composer");
const conversations_1 = require("./ducks/conversations");
const emojis_1 = require("./ducks/emojis");
const expiration_1 = require("./ducks/expiration");
const globalModals_1 = require("./ducks/globalModals");
const items_1 = require("./ducks/items");
const linkPreviews_1 = require("./ducks/linkPreviews");
const network_1 = require("./ducks/network");
const safetyNumber_1 = require("./ducks/safetyNumber");
const search_1 = require("./ducks/search");
const stickers_1 = require("./ducks/stickers");
const updates_1 = require("./ducks/updates");
const user_1 = require("./ducks/user");
exports.actionCreators = {
    accounts: accounts_1.actions,
    app: app_1.actions,
    audioPlayer: audioPlayer_1.actions,
    audioRecorder: audioRecorder_1.actions,
    badges: badges_1.actions,
    calling: calling_1.actions,
    composer: composer_1.actions,
    conversations: conversations_1.actions,
    emojis: emojis_1.actions,
    expiration: expiration_1.actions,
    globalModals: globalModals_1.actions,
    items: items_1.actions,
    linkPreviews: linkPreviews_1.actions,
    network: network_1.actions,
    safetyNumber: safetyNumber_1.actions,
    search: search_1.actions,
    stickers: stickers_1.actions,
    updates: updates_1.actions,
    user: user_1.actions,
};
exports.mapDispatchToProps = Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, accounts_1.actions), app_1.actions), audioPlayer_1.actions), audioRecorder_1.actions), badges_1.actions), calling_1.actions), composer_1.actions), conversations_1.actions), emojis_1.actions), expiration_1.actions), globalModals_1.actions), items_1.actions), linkPreviews_1.actions), network_1.actions), safetyNumber_1.actions), search_1.actions), stickers_1.actions), updates_1.actions), user_1.actions);
