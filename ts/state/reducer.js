"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = void 0;
const redux_1 = require("redux");
const accounts_1 = require("./ducks/accounts");
const app_1 = require("./ducks/app");
const audioPlayer_1 = require("./ducks/audioPlayer");
const audioRecorder_1 = require("./ducks/audioRecorder");
const badges_1 = require("./ducks/badges");
const calling_1 = require("./ducks/calling");
const composer_1 = require("./ducks/composer");
const conversations_1 = require("./ducks/conversations");
const emojis_1 = require("./ducks/emojis");
const expiration_1 = require("./ducks/expiration");
const globalModals_1 = require("./ducks/globalModals");
const items_1 = require("./ducks/items");
const linkPreviews_1 = require("./ducks/linkPreviews");
const network_1 = require("./ducks/network");
const preferredReactions_1 = require("./ducks/preferredReactions");
const safetyNumber_1 = require("./ducks/safetyNumber");
const search_1 = require("./ducks/search");
const stickers_1 = require("./ducks/stickers");
const updates_1 = require("./ducks/updates");
const user_1 = require("./ducks/user");
exports.reducer = (0, redux_1.combineReducers)({
    accounts: accounts_1.reducer,
    app: app_1.reducer,
    audioPlayer: audioPlayer_1.reducer,
    audioRecorder: audioRecorder_1.reducer,
    badges: badges_1.reducer,
    calling: calling_1.reducer,
    composer: composer_1.reducer,
    conversations: conversations_1.reducer,
    emojis: emojis_1.reducer,
    expiration: expiration_1.reducer,
    globalModals: globalModals_1.reducer,
    items: items_1.reducer,
    linkPreviews: linkPreviews_1.reducer,
    network: network_1.reducer,
    preferredReactions: preferredReactions_1.reducer,
    safetyNumber: safetyNumber_1.reducer,
    search: search_1.reducer,
    stickers: stickers_1.reducer,
    updates: updates_1.reducer,
    user: user_1.reducer,
});
