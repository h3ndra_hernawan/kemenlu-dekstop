"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createGroupV2Permissions = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const GroupV2Permissions_1 = require("../smart/GroupV2Permissions");
const createGroupV2Permissions = (store, props) => (react_1.default.createElement(react_redux_1.Provider, { store: store },
    react_1.default.createElement(GroupV2Permissions_1.SmartGroupV2Permissions, Object.assign({}, props))));
exports.createGroupV2Permissions = createGroupV2Permissions;
