"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStickerManager = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const StickerManager_1 = require("../smart/StickerManager");
const createStickerManager = (store) => (react_1.default.createElement(react_redux_1.Provider, { store: store },
    react_1.default.createElement(StickerManager_1.SmartStickerManager, null)));
exports.createStickerManager = createStickerManager;
