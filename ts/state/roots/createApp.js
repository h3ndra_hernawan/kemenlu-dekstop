"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createApp = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const App_1 = require("../smart/App");
const GlobalAudioProvider_1 = require("../smart/GlobalAudioProvider");
const createApp = (store) => (react_1.default.createElement(react_redux_1.Provider, { store: store },
    react_1.default.createElement(GlobalAudioProvider_1.SmartGlobalAudioProvider, null,
        react_1.default.createElement(App_1.SmartApp, null))));
exports.createApp = createApp;
