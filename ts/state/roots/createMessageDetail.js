"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createMessageDetail = void 0;
const react_1 = __importDefault(require("react"));
const react_redux_1 = require("react-redux");
const MessageDetail_1 = require("../smart/MessageDetail");
const createMessageDetail = (store, props) => (react_1.default.createElement(react_redux_1.Provider, { store: store },
    react_1.default.createElement(MessageDetail_1.SmartMessageDetail, Object.assign({}, props))));
exports.createMessageDetail = createMessageDetail;
