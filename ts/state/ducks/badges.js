"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getInitialState = exports.actions = void 0;
const lodash_1 = require("lodash");
const getOwn_1 = require("../../util/getOwn");
const badgeImageFileDownloader_1 = require("../../badges/badgeImageFileDownloader");
// Actions
const IMAGE_FILE_DOWNLOADED = 'badges/IMAGE_FILE_DOWNLOADED';
const UPDATE_OR_CREATE = 'badges/UPDATE_OR_CREATE';
// Action creators
exports.actions = {
    badgeImageFileDownloaded,
    updateOrCreate,
};
function badgeImageFileDownloaded(url, localPath) {
    return {
        type: IMAGE_FILE_DOWNLOADED,
        payload: { url, localPath },
    };
}
function updateOrCreate(badges) {
    return async (dispatch) => {
        // There is a race condition here: if we save the badges but we fail to kick off a
        //   check (e.g., due to a crash), we won't download its image files. In the unlikely
        //   event that this happens, we'll repair it the next time we check for undownloaded
        //   image files.
        await window.Signal.Data.updateOrCreateBadges(badges);
        dispatch({
            type: UPDATE_OR_CREATE,
            payload: badges,
        });
        badgeImageFileDownloader_1.badgeImageFileDownloader.checkForFilesToDownload();
    };
}
// Reducer
function getInitialState() {
    return { byId: {} };
}
exports.getInitialState = getInitialState;
function reducer(state = getInitialState(), action) {
    switch (action.type) {
        // This should match the database logic.
        case IMAGE_FILE_DOWNLOADED: {
            const { url, localPath } = action.payload;
            return Object.assign(Object.assign({}, state), { byId: (0, lodash_1.mapValues)(state.byId, badge => (Object.assign(Object.assign({}, badge), { images: badge.images.map(image => (0, lodash_1.mapValues)(image, imageFile => imageFile.url === url
                        ? Object.assign(Object.assign({}, imageFile), { localPath }) : imageFile)) }))) });
        }
        // This should match the database logic.
        case UPDATE_OR_CREATE: {
            const newById = Object.assign({}, state.byId);
            action.payload.forEach(badge => {
                const existingBadge = (0, getOwn_1.getOwn)(newById, badge.id);
                const oldLocalPaths = new Map();
                existingBadge === null || existingBadge === void 0 ? void 0 : existingBadge.images.forEach(image => {
                    Object.values(image).forEach(({ localPath, url }) => {
                        if (localPath) {
                            oldLocalPaths.set(url, localPath);
                        }
                    });
                });
                const images = badge.images.map(image => (0, lodash_1.mapValues)(image, imageFile => (Object.assign(Object.assign({}, imageFile), { localPath: imageFile.localPath || oldLocalPaths.get(imageFile.url) }))));
                if (existingBadge) {
                    newById[badge.id] = Object.assign(Object.assign({}, existingBadge), { category: badge.category, name: badge.name, descriptionTemplate: badge.descriptionTemplate, images });
                }
                else {
                    newById[badge.id] = Object.assign(Object.assign({}, badge), { images });
                }
            });
            return Object.assign(Object.assign({}, state), { byId: newById });
        }
        default:
            return state;
    }
}
exports.reducer = reducer;
