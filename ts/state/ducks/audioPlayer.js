"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.useActions = exports.actions = void 0;
const useBoundActions_1 = require("../../hooks/useBoundActions");
// Action Creators
exports.actions = {
    setActiveAudioID,
};
const useActions = () => (0, useBoundActions_1.useBoundActions)(exports.actions);
exports.useActions = useActions;
function setActiveAudioID(id, context) {
    return {
        type: 'audioPlayer/SET_ACTIVE_AUDIO_ID',
        payload: { id, context },
    };
}
// Reducer
function getEmptyState() {
    return {
        activeAudioID: undefined,
        activeAudioContext: undefined,
    };
}
function reducer(state = getEmptyState(), action) {
    if (action.type === 'audioPlayer/SET_ACTIVE_AUDIO_ID') {
        const { payload } = action;
        return Object.assign(Object.assign({}, state), { activeAudioID: payload.id, activeAudioContext: payload.context });
    }
    // Reset activeAudioID on conversation change.
    if (action.type === 'SWITCH_TO_ASSOCIATED_VIEW') {
        return Object.assign(Object.assign({}, state), { activeAudioID: undefined });
    }
    // Reset activeAudioID on when played message is deleted on expiration.
    if (action.type === 'MESSAGE_DELETED') {
        const { id } = action.payload;
        if (state.activeAudioID !== id) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeAudioID: undefined });
    }
    // Reset activeAudioID on when played message is deleted for everyone.
    if (action.type === 'MESSAGE_CHANGED') {
        const { id, data } = action.payload;
        if (state.activeAudioID !== id) {
            return state;
        }
        if (!data.deletedForEveryone) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeAudioID: undefined });
    }
    return state;
}
exports.reducer = reducer;
