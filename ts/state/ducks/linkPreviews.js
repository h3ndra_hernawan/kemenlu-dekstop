"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getEmptyState = exports.actions = exports.REMOVE_PREVIEW = void 0;
const assignWithNoUnnecessaryAllocation_1 = require("../../util/assignWithNoUnnecessaryAllocation");
// Actions
const ADD_PREVIEW = 'linkPreviews/ADD_PREVIEW';
exports.REMOVE_PREVIEW = 'linkPreviews/REMOVE_PREVIEW';
// Action Creators
exports.actions = {
    addLinkPreview,
    removeLinkPreview,
};
function addLinkPreview(payload) {
    return {
        type: ADD_PREVIEW,
        payload,
    };
}
function removeLinkPreview() {
    return {
        type: exports.REMOVE_PREVIEW,
    };
}
// Reducer
function getEmptyState() {
    return {
        linkPreview: undefined,
    };
}
exports.getEmptyState = getEmptyState;
function reducer(state = getEmptyState(), action) {
    if (action.type === ADD_PREVIEW) {
        const { payload } = action;
        return {
            linkPreview: payload,
        };
    }
    if (action.type === exports.REMOVE_PREVIEW) {
        return (0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(state, {
            linkPreview: undefined,
        });
    }
    return state;
}
exports.reducer = reducer;
