"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getEmptyState = exports.actions = void 0;
const events_1 = require("../../shims/events");
const Util_1 = require("../../types/Util");
// Action Creators
exports.actions = {
    userChanged,
    manualReconnect,
};
function userChanged(attributes) {
    return {
        type: 'USER_CHANGED',
        payload: attributes,
    };
}
function manualReconnect() {
    (0, events_1.trigger)('manualConnect');
    return {
        type: 'NOOP',
        payload: null,
    };
}
// Reducer
function getEmptyState() {
    return {
        attachmentsPath: 'missing',
        stickersPath: 'missing',
        tempPath: 'missing',
        ourConversationId: 'missing',
        ourDeviceId: 0,
        ourUuid: '00000000-0000-4000-8000-000000000000',
        ourNumber: 'missing',
        regionCode: 'missing',
        platform: 'missing',
        interactionMode: 'mouse',
        theme: Util_1.ThemeType.light,
        i18n: Object.assign(() => {
            throw new Error('i18n not yet set up');
        }, {
            getLocale() {
                throw new Error('i18n not yet set up');
            },
        }),
    };
}
exports.getEmptyState = getEmptyState;
function reducer(state = getEmptyState(), action) {
    if (!state) {
        return getEmptyState();
    }
    if (action.type === 'USER_CHANGED') {
        const { payload } = action;
        return Object.assign(Object.assign({}, state), payload);
    }
    return state;
}
exports.reducer = reducer;
