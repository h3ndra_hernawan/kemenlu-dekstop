"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getEmptyState = exports.actions = void 0;
const lodash_1 = require("lodash");
const cleanSearchTerm_1 = require("../../util/cleanSearchTerm");
const Client_1 = __importDefault(require("../../sql/Client"));
const makeLookup_1 = require("../../util/makeLookup");
const search_1 = require("../selectors/search");
const user_1 = require("../selectors/user");
const { searchConversations: dataSearchConversations, searchMessages: dataSearchMessages, searchMessagesInConversation, } = Client_1.default;
// Action Creators
exports.actions = {
    startSearch,
    clearSearch,
    clearConversationSearch,
    searchInConversation,
    updateSearchTerm,
};
function startSearch() {
    return {
        type: 'SEARCH_START',
        payload: null,
    };
}
function clearSearch() {
    return {
        type: 'SEARCH_CLEAR',
        payload: null,
    };
}
function clearConversationSearch() {
    return {
        type: 'CLEAR_CONVERSATION_SEARCH',
        payload: null,
    };
}
function searchInConversation(searchConversationId) {
    return {
        type: 'SEARCH_IN_CONVERSATION',
        payload: { searchConversationId },
    };
}
function updateSearchTerm(query) {
    return (dispatch, getState) => {
        var _a;
        dispatch({
            type: 'SEARCH_UPDATE',
            payload: { query },
        });
        const state = getState();
        doSearch({
            dispatch,
            noteToSelf: (0, user_1.getIntl)(state)('noteToSelf').toLowerCase(),
            ourConversationId: (0, user_1.getUserConversationId)(state),
            query: (0, search_1.getQuery)(state),
            searchConversationId: (_a = (0, search_1.getSearchConversation)(state)) === null || _a === void 0 ? void 0 : _a.id,
        });
    };
}
const doSearch = (0, lodash_1.debounce)(({ dispatch, noteToSelf, ourConversationId, query, searchConversationId, }) => {
    if (!query) {
        return;
    }
    (async () => {
        dispatch({
            type: 'SEARCH_MESSAGES_RESULTS_FULFILLED',
            payload: {
                messages: await queryMessages(query, searchConversationId),
                query,
            },
        });
    })();
    if (!searchConversationId) {
        (async () => {
            const { conversationIds, contactIds } = await queryConversationsAndContacts(query, {
                ourConversationId,
                noteToSelf,
            });
            dispatch({
                type: 'SEARCH_DISCUSSIONS_RESULTS_FULFILLED',
                payload: {
                    conversationIds,
                    contactIds,
                    query,
                },
            });
        })();
    }
}, 200);
async function queryMessages(query, searchConversationId) {
    try {
        const normalized = (0, cleanSearchTerm_1.cleanSearchTerm)(query);
        if (normalized.length === 0) {
            return [];
        }
        if (searchConversationId) {
            return searchMessagesInConversation(normalized, searchConversationId);
        }
        return dataSearchMessages(normalized);
    }
    catch (e) {
        return [];
    }
}
async function queryConversationsAndContacts(providedQuery, options) {
    const { ourConversationId, noteToSelf } = options;
    const query = providedQuery.replace(/[+.()]*/g, '');
    const searchResults = await dataSearchConversations(query);
    // Split into two groups - active conversations and items just from address book
    let conversationIds = [];
    let contactIds = [];
    const max = searchResults.length;
    for (let i = 0; i < max; i += 1) {
        const conversation = searchResults[i];
        if (conversation.type === 'private' && !conversation.lastMessage) {
            contactIds.push(conversation.id);
        }
        else {
            conversationIds.push(conversation.id);
        }
    }
    // // @ts-ignore
    // console._log(
    //   '%cqueryConversationsAndContacts',
    //   'background:black;color:red;',
    //   { searchResults, conversations, ourNumber, ourUuid }
    // );
    // Inject synthetic Note to Self entry if query matches localized 'Note to Self'
    if (noteToSelf.indexOf(providedQuery.toLowerCase()) !== -1) {
        // ensure that we don't have duplicates in our results
        contactIds = contactIds.filter(id => id !== ourConversationId);
        conversationIds = conversationIds.filter(id => id !== ourConversationId);
        contactIds.unshift(ourConversationId);
    }
    return { conversationIds, contactIds };
}
// Reducer
function getEmptyState() {
    return {
        startSearchCounter: 0,
        query: '',
        messageIds: [],
        messageLookup: {},
        conversationIds: [],
        contactIds: [],
        discussionsLoading: false,
        messagesLoading: false,
    };
}
exports.getEmptyState = getEmptyState;
function reducer(state = getEmptyState(), action) {
    if (action.type === 'SHOW_ARCHIVED_CONVERSATIONS') {
        return getEmptyState();
    }
    if (action.type === 'SEARCH_START') {
        return Object.assign(Object.assign({}, state), { searchConversationId: undefined, startSearchCounter: state.startSearchCounter + 1 });
    }
    if (action.type === 'SEARCH_CLEAR') {
        return getEmptyState();
    }
    if (action.type === 'SEARCH_UPDATE') {
        const { payload } = action;
        const { query } = payload;
        const hasQuery = Boolean(query);
        const isWithinConversation = Boolean(state.searchConversationId);
        return Object.assign(Object.assign(Object.assign({}, state), { query, messagesLoading: hasQuery }), (hasQuery
            ? {
                messageIds: [],
                messageLookup: {},
                discussionsLoading: !isWithinConversation,
                contactIds: [],
                conversationIds: [],
            }
            : {}));
    }
    if (action.type === 'SEARCH_IN_CONVERSATION') {
        const { payload } = action;
        const { searchConversationId } = payload;
        if (searchConversationId === state.searchConversationId) {
            return Object.assign(Object.assign({}, state), { startSearchCounter: state.startSearchCounter + 1 });
        }
        return Object.assign(Object.assign({}, getEmptyState()), { searchConversationId, startSearchCounter: state.startSearchCounter + 1 });
    }
    if (action.type === 'CLEAR_CONVERSATION_SEARCH') {
        const { searchConversationId } = state;
        return Object.assign(Object.assign({}, getEmptyState()), { searchConversationId });
    }
    if (action.type === 'SEARCH_MESSAGES_RESULTS_FULFILLED') {
        const { payload } = action;
        const { messages, query } = payload;
        // Reject if the associated query is not the most recent user-provided query
        if (state.query !== query) {
            return state;
        }
        const messageIds = messages.map(message => message.id);
        return Object.assign(Object.assign({}, state), { query,
            messageIds, messageLookup: (0, makeLookup_1.makeLookup)(messages, 'id'), messagesLoading: false });
    }
    if (action.type === 'SEARCH_DISCUSSIONS_RESULTS_FULFILLED') {
        const { payload } = action;
        const { contactIds, conversationIds, query } = payload;
        // Reject if the associated query is not the most recent user-provided query
        if (state.query !== query) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { contactIds,
            conversationIds, discussionsLoading: false });
    }
    if (action.type === 'CONVERSATIONS_REMOVE_ALL') {
        return getEmptyState();
    }
    if (action.type === 'SELECTED_CONVERSATION_CHANGED') {
        const { payload } = action;
        const { id, messageId } = payload;
        const { searchConversationId } = state;
        if (searchConversationId && searchConversationId !== id) {
            return getEmptyState();
        }
        return Object.assign(Object.assign({}, state), { selectedMessage: messageId });
    }
    if (action.type === 'CONVERSATION_UNLOADED') {
        const { payload } = action;
        const { id } = payload;
        const { searchConversationId } = state;
        if (searchConversationId && searchConversationId === id) {
            return getEmptyState();
        }
        return state;
    }
    if (action.type === 'MESSAGE_DELETED') {
        const { messageIds, messageLookup } = state;
        if (!messageIds || messageIds.length < 1) {
            return state;
        }
        const { payload } = action;
        const { id } = payload;
        return Object.assign(Object.assign({}, state), { messageIds: (0, lodash_1.reject)(messageIds, messageId => id === messageId), messageLookup: (0, lodash_1.omit)(messageLookup, id) });
    }
    return state;
}
exports.reducer = reducer;
