"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.useActions = exports.actions = exports.RecordingState = exports.ErrorDialogAudioRecorderType = void 0;
const log = __importStar(require("../../logging/log"));
const protobuf_1 = require("../../protobuf");
const fileToBytes_1 = require("../../util/fileToBytes");
const audioRecorder_1 = require("../../services/audioRecorder");
const MIME_1 = require("../../types/MIME");
const useBoundActions_1 = require("../../hooks/useBoundActions");
var ErrorDialogAudioRecorderType;
(function (ErrorDialogAudioRecorderType) {
    ErrorDialogAudioRecorderType[ErrorDialogAudioRecorderType["Blur"] = 0] = "Blur";
    ErrorDialogAudioRecorderType[ErrorDialogAudioRecorderType["ErrorRecording"] = 1] = "ErrorRecording";
    ErrorDialogAudioRecorderType[ErrorDialogAudioRecorderType["Timeout"] = 2] = "Timeout";
})(ErrorDialogAudioRecorderType = exports.ErrorDialogAudioRecorderType || (exports.ErrorDialogAudioRecorderType = {}));
// State
var RecordingState;
(function (RecordingState) {
    RecordingState["Recording"] = "recording";
    RecordingState["Initializing"] = "initializing";
    RecordingState["Idle"] = "idle";
})(RecordingState = exports.RecordingState || (exports.RecordingState = {}));
// Actions
const CANCEL_RECORDING = 'audioRecorder/CANCEL_RECORDING';
const COMPLETE_RECORDING = 'audioRecorder/COMPLETE_RECORDING';
const ERROR_RECORDING = 'audioRecorder/ERROR_RECORDING';
const NOW_RECORDING = 'audioRecorder/NOW_RECORDING';
const START_RECORDING = 'audioRecorder/START_RECORDING';
// Action Creators
exports.actions = {
    cancelRecording,
    completeRecording,
    errorRecording,
    startRecording,
};
const useActions = () => (0, useBoundActions_1.useBoundActions)(exports.actions);
exports.useActions = useActions;
function startRecording() {
    return async (dispatch, getState) => {
        if (getState().composer.attachments.length) {
            return;
        }
        if (getState().audioRecorder.recordingState !== RecordingState.Idle) {
            return;
        }
        dispatch({
            type: START_RECORDING,
            payload: undefined,
        });
        try {
            const started = await audioRecorder_1.recorder.start();
            if (started) {
                dispatch({
                    type: NOW_RECORDING,
                    payload: undefined,
                });
            }
            else {
                dispatch({
                    type: ERROR_RECORDING,
                    payload: ErrorDialogAudioRecorderType.ErrorRecording,
                });
            }
        }
        catch (err) {
            dispatch({
                type: ERROR_RECORDING,
                payload: ErrorDialogAudioRecorderType.ErrorRecording,
            });
        }
    };
}
function completeRecordingAction() {
    return {
        type: COMPLETE_RECORDING,
        payload: undefined,
    };
}
function completeRecording(conversationId, onSendAudioRecording) {
    return async (dispatch, getState) => {
        const state = getState();
        const isSelectedConversation = state.conversations.selectedConversationId === conversationId;
        if (!isSelectedConversation) {
            log.warn('completeRecording: Recording started in one conversation and completed in another');
            dispatch(cancelRecording());
            return;
        }
        const blob = await audioRecorder_1.recorder.stop();
        try {
            if (!blob) {
                throw new Error('completeRecording: no blob returned');
            }
            const data = await (0, fileToBytes_1.fileToBytes)(blob);
            const voiceNoteAttachment = {
                pending: false,
                contentType: (0, MIME_1.stringToMIMEType)(blob.type),
                data,
                size: data.byteLength,
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.VOICE_MESSAGE,
            };
            if (onSendAudioRecording) {
                onSendAudioRecording(voiceNoteAttachment);
            }
        }
        finally {
            dispatch(completeRecordingAction());
        }
    };
}
function cancelRecording() {
    return async (dispatch) => {
        await audioRecorder_1.recorder.stop();
        audioRecorder_1.recorder.clear();
        dispatch({
            type: CANCEL_RECORDING,
            payload: undefined,
        });
    };
}
function errorRecording(errorDialogAudioRecorderType) {
    audioRecorder_1.recorder.stop();
    return {
        type: ERROR_RECORDING,
        payload: errorDialogAudioRecorderType,
    };
}
// Reducer
function getEmptyState() {
    return {
        recordingState: RecordingState.Idle,
    };
}
function reducer(state = getEmptyState(), action) {
    if (action.type === START_RECORDING) {
        return Object.assign(Object.assign({}, state), { errorDialogAudioRecorderType: undefined, recordingState: RecordingState.Initializing });
    }
    if (action.type === NOW_RECORDING) {
        return Object.assign(Object.assign({}, state), { errorDialogAudioRecorderType: undefined, recordingState: RecordingState.Recording });
    }
    if (action.type === CANCEL_RECORDING || action.type === COMPLETE_RECORDING) {
        return Object.assign(Object.assign({}, state), { errorDialogAudioRecorderType: undefined, recordingState: RecordingState.Idle });
    }
    if (action.type === ERROR_RECORDING) {
        return Object.assign(Object.assign({}, state), { errorDialogAudioRecorderType: action.payload });
    }
    return state;
}
exports.reducer = reducer;
