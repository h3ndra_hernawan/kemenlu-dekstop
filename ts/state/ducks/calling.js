"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getEmptyState = exports.actions = exports.isAnybodyElseInGroupCall = exports.getIncomingCall = exports.getActiveCall = void 0;
const electron_1 = require("electron");
const ringrtc_1 = require("ringrtc");
const mac_screen_capture_permissions_1 = require("mac-screen-capture-permissions");
const lodash_1 = require("lodash");
const getOwn_1 = require("../../util/getOwn");
const Errors = __importStar(require("../../types/errors"));
const user_1 = require("../selectors/user");
const isConversationTooBigToRing_1 = require("../../conversations/isConversationTooBigToRing");
const missingCaseError_1 = require("../../util/missingCaseError");
const calling_1 = require("../../services/calling");
const Calling_1 = require("../../types/Calling");
const callingTones_1 = require("../../util/callingTones");
const callingPermissions_1 = require("../../util/callingPermissions");
const isGroupCallOutboundRingEnabled_1 = require("../../util/isGroupCallOutboundRingEnabled");
const sleep_1 = require("../../util/sleep");
const LatestQueue_1 = require("../../util/LatestQueue");
const log = __importStar(require("../../logging/log"));
// Helpers
const getActiveCall = ({ activeCallState, callsByConversation, }) => activeCallState &&
    (0, getOwn_1.getOwn)(callsByConversation, activeCallState.conversationId);
exports.getActiveCall = getActiveCall;
// In theory, there could be multiple incoming calls, or an incoming call while there's
//   an active call. In practice, the UI is not ready for this, and RingRTC doesn't
//   support it for direct calls.
const getIncomingCall = (callsByConversation, ourUuid) => Object.values(callsByConversation).find(call => {
    switch (call.callMode) {
        case Calling_1.CallMode.Direct:
            return call.isIncoming && call.callState === Calling_1.CallState.Ringing;
        case Calling_1.CallMode.Group:
            return (call.ringerUuid &&
                call.connectionState === Calling_1.GroupCallConnectionState.NotConnected &&
                (0, exports.isAnybodyElseInGroupCall)(call.peekInfo, ourUuid));
        default:
            throw (0, missingCaseError_1.missingCaseError)(call);
    }
});
exports.getIncomingCall = getIncomingCall;
const isAnybodyElseInGroupCall = ({ uuids }, ourUuid) => uuids.some(id => id !== ourUuid);
exports.isAnybodyElseInGroupCall = isAnybodyElseInGroupCall;
const getGroupCallRingState = (call) => (call === null || call === void 0 ? void 0 : call.ringId) === undefined
    ? {}
    : { ringId: call.ringId, ringerUuid: call.ringerUuid };
// Actions
const ACCEPT_CALL_PENDING = 'calling/ACCEPT_CALL_PENDING';
const CANCEL_CALL = 'calling/CANCEL_CALL';
const CANCEL_INCOMING_GROUP_CALL_RING = 'calling/CANCEL_INCOMING_GROUP_CALL_RING';
const SHOW_CALL_LOBBY = 'calling/SHOW_CALL_LOBBY';
const CALL_STATE_CHANGE_FULFILLED = 'calling/CALL_STATE_CHANGE_FULFILLED';
const CHANGE_IO_DEVICE_FULFILLED = 'calling/CHANGE_IO_DEVICE_FULFILLED';
const CLOSE_NEED_PERMISSION_SCREEN = 'calling/CLOSE_NEED_PERMISSION_SCREEN';
const DECLINE_DIRECT_CALL = 'calling/DECLINE_DIRECT_CALL';
const GROUP_CALL_STATE_CHANGE = 'calling/GROUP_CALL_STATE_CHANGE';
const HANG_UP = 'calling/HANG_UP';
const INCOMING_DIRECT_CALL = 'calling/INCOMING_DIRECT_CALL';
const INCOMING_GROUP_CALL = 'calling/INCOMING_GROUP_CALL';
const MARK_CALL_TRUSTED = 'calling/MARK_CALL_TRUSTED';
const MARK_CALL_UNTRUSTED = 'calling/MARK_CALL_UNTRUSTED';
const OUTGOING_CALL = 'calling/OUTGOING_CALL';
const PEEK_NOT_CONNECTED_GROUP_CALL_FULFILLED = 'calling/PEEK_NOT_CONNECTED_GROUP_CALL_FULFILLED';
const REFRESH_IO_DEVICES = 'calling/REFRESH_IO_DEVICES';
const REMOTE_SHARING_SCREEN_CHANGE = 'calling/REMOTE_SHARING_SCREEN_CHANGE';
const REMOTE_VIDEO_CHANGE = 'calling/REMOTE_VIDEO_CHANGE';
const RETURN_TO_ACTIVE_CALL = 'calling/RETURN_TO_ACTIVE_CALL';
const SET_LOCAL_AUDIO_FULFILLED = 'calling/SET_LOCAL_AUDIO_FULFILLED';
const SET_LOCAL_VIDEO_FULFILLED = 'calling/SET_LOCAL_VIDEO_FULFILLED';
const SET_OUTGOING_RING = 'calling/SET_OUTGOING_RING';
const SET_PRESENTING = 'calling/SET_PRESENTING';
const SET_PRESENTING_SOURCES = 'calling/SET_PRESENTING_SOURCES';
const TOGGLE_NEEDS_SCREEN_RECORDING_PERMISSIONS = 'calling/TOGGLE_NEEDS_SCREEN_RECORDING_PERMISSIONS';
const START_DIRECT_CALL = 'calling/START_DIRECT_CALL';
const TOGGLE_PARTICIPANTS = 'calling/TOGGLE_PARTICIPANTS';
const TOGGLE_PIP = 'calling/TOGGLE_PIP';
const TOGGLE_SETTINGS = 'calling/TOGGLE_SETTINGS';
const TOGGLE_SPEAKER_VIEW = 'calling/TOGGLE_SPEAKER_VIEW';
// Action Creators
function acceptCall(payload) {
    return async (dispatch, getState) => {
        const { conversationId, asVideoCall } = payload;
        const call = (0, getOwn_1.getOwn)(getState().calling.callsByConversation, conversationId);
        if (!call) {
            log.error('Trying to accept a non-existent call');
            return;
        }
        switch (call.callMode) {
            case Calling_1.CallMode.Direct:
                await calling_1.calling.acceptDirectCall(conversationId, asVideoCall);
                break;
            case Calling_1.CallMode.Group:
                await calling_1.calling.joinGroupCall(conversationId, true, asVideoCall, false);
                break;
            default:
                throw (0, missingCaseError_1.missingCaseError)(call);
        }
        dispatch({
            type: ACCEPT_CALL_PENDING,
            payload,
        });
    };
}
function callStateChange(payload) {
    return async (dispatch) => {
        const { callState } = payload;
        if (callState === Calling_1.CallState.Ended) {
            await callingTones_1.callingTones.playEndCall();
            electron_1.ipcRenderer.send('close-screen-share-controller');
        }
        dispatch({
            type: CALL_STATE_CHANGE_FULFILLED,
            payload,
        });
    };
}
function changeIODevice(payload) {
    return async (dispatch) => {
        // Only `setPreferredCamera` returns a Promise.
        if (payload.type === Calling_1.CallingDeviceType.CAMERA) {
            await calling_1.calling.setPreferredCamera(payload.selectedDevice);
        }
        else if (payload.type === Calling_1.CallingDeviceType.MICROPHONE) {
            calling_1.calling.setPreferredMicrophone(payload.selectedDevice);
        }
        else if (payload.type === Calling_1.CallingDeviceType.SPEAKER) {
            calling_1.calling.setPreferredSpeaker(payload.selectedDevice);
        }
        dispatch({
            type: CHANGE_IO_DEVICE_FULFILLED,
            payload,
        });
    };
}
function closeNeedPermissionScreen() {
    return {
        type: CLOSE_NEED_PERMISSION_SCREEN,
        payload: null,
    };
}
function cancelCall(payload) {
    calling_1.calling.stopCallingLobby(payload.conversationId);
    return {
        type: CANCEL_CALL,
    };
}
function cancelIncomingGroupCallRing(payload) {
    return {
        type: CANCEL_INCOMING_GROUP_CALL_RING,
        payload,
    };
}
function declineCall(payload) {
    return (dispatch, getState) => {
        const { conversationId } = payload;
        const call = (0, getOwn_1.getOwn)(getState().calling.callsByConversation, conversationId);
        if (!call) {
            log.error('Trying to decline a non-existent call');
            return;
        }
        switch (call.callMode) {
            case Calling_1.CallMode.Direct:
                calling_1.calling.declineDirectCall(conversationId);
                dispatch({
                    type: DECLINE_DIRECT_CALL,
                    payload,
                });
                break;
            case Calling_1.CallMode.Group: {
                const { ringId } = call;
                if (ringId === undefined) {
                    log.error('Trying to decline a group call without a ring ID');
                }
                else {
                    calling_1.calling.declineGroupCall(conversationId, ringId);
                    dispatch({
                        type: CANCEL_INCOMING_GROUP_CALL_RING,
                        payload: { conversationId, ringId },
                    });
                }
                break;
            }
            default:
                throw (0, missingCaseError_1.missingCaseError)(call);
        }
    };
}
function getPresentingSources() {
    return async (dispatch, getState) => {
        // We check if the user has permissions first before calling desktopCapturer
        // Next we call getPresentingSources so that one gets the prompt for permissions,
        // if necessary.
        // Finally, we have the if statement which shows the modal, if needed.
        // It is in this exact order so that during first-time-use one will be
        // prompted for permissions and if they so happen to deny we can still
        // capture that state correctly.
        const platform = (0, user_1.getPlatform)(getState());
        const needsPermission = platform === 'darwin' && !(0, mac_screen_capture_permissions_1.hasScreenCapturePermission)();
        const sources = await calling_1.calling.getPresentingSources();
        if (needsPermission) {
            dispatch({
                type: TOGGLE_NEEDS_SCREEN_RECORDING_PERMISSIONS,
            });
            return;
        }
        dispatch({
            type: SET_PRESENTING_SOURCES,
            payload: sources,
        });
    };
}
function groupCallStateChange(payload) {
    return async (dispatch, getState) => {
        let didSomeoneStartPresenting;
        const activeCall = (0, exports.getActiveCall)(getState().calling);
        if ((activeCall === null || activeCall === void 0 ? void 0 : activeCall.callMode) === Calling_1.CallMode.Group) {
            const wasSomeonePresenting = activeCall.remoteParticipants.some(participant => participant.presenting);
            const isSomeonePresenting = payload.remoteParticipants.some(participant => participant.presenting);
            didSomeoneStartPresenting = !wasSomeonePresenting && isSomeonePresenting;
        }
        else {
            didSomeoneStartPresenting = false;
        }
        dispatch({
            type: GROUP_CALL_STATE_CHANGE,
            payload: Object.assign(Object.assign({}, payload), { ourUuid: getState().user.ourUuid }),
        });
        if (didSomeoneStartPresenting) {
            callingTones_1.callingTones.someonePresenting();
        }
        if (payload.connectionState === Calling_1.GroupCallConnectionState.NotConnected) {
            electron_1.ipcRenderer.send('close-screen-share-controller');
        }
    };
}
function hangUp(payload) {
    calling_1.calling.hangup(payload.conversationId);
    return {
        type: HANG_UP,
        payload,
    };
}
function keyChanged(payload) {
    return (dispatch, getState) => {
        const state = getState();
        const { activeCallState } = state.calling;
        const activeCall = (0, exports.getActiveCall)(state.calling);
        if (!activeCall || !activeCallState) {
            return;
        }
        if (activeCall.callMode === Calling_1.CallMode.Group) {
            const uuidsChanged = new Set(activeCallState.safetyNumberChangedUuids);
            // Iterate over each participant to ensure that the uuid passed in
            // matches one of the participants in the group call.
            activeCall.remoteParticipants.forEach(participant => {
                if (participant.uuid === payload.uuid) {
                    uuidsChanged.add(participant.uuid);
                }
            });
            const safetyNumberChangedUuids = Array.from(uuidsChanged);
            if (safetyNumberChangedUuids.length) {
                dispatch({
                    type: MARK_CALL_UNTRUSTED,
                    payload: {
                        safetyNumberChangedUuids,
                    },
                });
            }
        }
    };
}
function keyChangeOk(payload) {
    return dispatch => {
        calling_1.calling.resendGroupCallMediaKeys(payload.conversationId);
        dispatch({
            type: MARK_CALL_TRUSTED,
            payload: null,
        });
    };
}
function receiveIncomingDirectCall(payload) {
    return {
        type: INCOMING_DIRECT_CALL,
        payload,
    };
}
function receiveIncomingGroupCall(payload) {
    return {
        type: INCOMING_GROUP_CALL,
        payload,
    };
}
function openSystemPreferencesAction() {
    return () => {
        (0, mac_screen_capture_permissions_1.openSystemPreferences)();
    };
}
function outgoingCall(payload) {
    return {
        type: OUTGOING_CALL,
        payload,
    };
}
// We might call this function many times in rapid succession (for example, if lots of
//   people are joining and leaving at once). We want to make sure to update eventually
//   (if people join and leave for an hour, we don't want you to have to wait an hour to
//   get an update), and we also don't want to update too often. That's why we use a
//   "latest queue".
const peekQueueByConversation = new Map();
function peekNotConnectedGroupCall(payload) {
    return (dispatch, getState) => {
        const { conversationId } = payload;
        let queue = peekQueueByConversation.get(conversationId);
        if (!queue) {
            queue = new LatestQueue_1.LatestQueue();
            queue.onceEmpty(() => {
                peekQueueByConversation.delete(conversationId);
            });
            peekQueueByConversation.set(conversationId, queue);
        }
        queue.add(async () => {
            const state = getState();
            // We make sure we're not trying to peek at a connected (or connecting, or
            //   reconnecting) call. Because this is asynchronous, it's possible that the call
            //   will connect by the time we dispatch, so we also need to do a similar check in
            //   the reducer.
            const existingCall = (0, getOwn_1.getOwn)(state.calling.callsByConversation, conversationId);
            if ((existingCall === null || existingCall === void 0 ? void 0 : existingCall.callMode) === Calling_1.CallMode.Group &&
                existingCall.connectionState !== Calling_1.GroupCallConnectionState.NotConnected) {
                return;
            }
            // If we peek right after receiving the message, we may get outdated information.
            //   This is most noticeable when someone leaves. We add a delay and then make sure
            //   to only be peeking once.
            await (0, sleep_1.sleep)(1000);
            let peekInfo;
            try {
                peekInfo = await calling_1.calling.peekGroupCall(conversationId);
            }
            catch (err) {
                log.error('Group call peeking failed', Errors.toLogFormat(err));
                return;
            }
            if (!peekInfo) {
                return;
            }
            const { ourUuid } = state.user;
            await calling_1.calling.updateCallHistoryForGroupCall(conversationId, peekInfo);
            const formattedPeekInfo = calling_1.calling.formatGroupCallPeekInfoForRedux(peekInfo);
            dispatch({
                type: PEEK_NOT_CONNECTED_GROUP_CALL_FULFILLED,
                payload: {
                    conversationId,
                    peekInfo: formattedPeekInfo,
                    ourUuid,
                },
            });
        });
    };
}
function refreshIODevices(payload) {
    return {
        type: REFRESH_IO_DEVICES,
        payload,
    };
}
function remoteSharingScreenChange(payload) {
    return {
        type: REMOTE_SHARING_SCREEN_CHANGE,
        payload,
    };
}
function remoteVideoChange(payload) {
    return {
        type: REMOTE_VIDEO_CHANGE,
        payload,
    };
}
function returnToActiveCall() {
    return {
        type: RETURN_TO_ACTIVE_CALL,
    };
}
function setIsCallActive(isCallActive) {
    return () => {
        window.SignalContext.setIsCallActive(isCallActive);
    };
}
function setLocalPreview(payload) {
    return () => {
        calling_1.calling.videoCapturer.setLocalPreview(payload.element);
    };
}
function setRendererCanvas(payload) {
    return () => {
        calling_1.calling.videoRenderer.setCanvas(payload.element);
    };
}
function setLocalAudio(payload) {
    return (dispatch, getState) => {
        const activeCall = (0, exports.getActiveCall)(getState().calling);
        if (!activeCall) {
            log.warn('Trying to set local audio when no call is active');
            return;
        }
        calling_1.calling.setOutgoingAudio(activeCall.conversationId, payload.enabled);
        dispatch({
            type: SET_LOCAL_AUDIO_FULFILLED,
            payload,
        });
    };
}
function setLocalVideo(payload) {
    return async (dispatch, getState) => {
        const activeCall = (0, exports.getActiveCall)(getState().calling);
        if (!activeCall) {
            log.warn('Trying to set local video when no call is active');
            return;
        }
        let enabled;
        if (await (0, callingPermissions_1.requestCameraPermissions)()) {
            if (activeCall.callMode === Calling_1.CallMode.Group ||
                (activeCall.callMode === Calling_1.CallMode.Direct && activeCall.callState)) {
                calling_1.calling.setOutgoingVideo(activeCall.conversationId, payload.enabled);
            }
            else if (payload.enabled) {
                calling_1.calling.enableLocalCamera();
            }
            else {
                calling_1.calling.disableLocalVideo();
            }
            ({ enabled } = payload);
        }
        else {
            enabled = false;
        }
        dispatch({
            type: SET_LOCAL_VIDEO_FULFILLED,
            payload: Object.assign(Object.assign({}, payload), { enabled }),
        });
    };
}
function setGroupCallVideoRequest(payload) {
    return () => {
        calling_1.calling.setGroupCallVideoRequest(payload.conversationId, payload.resolutions.map(resolution => (Object.assign(Object.assign({}, resolution), { 
            // The `framerate` property in RingRTC has to be set, even if it's set to
            //   `undefined`.
            framerate: undefined }))));
    };
}
function setPresenting(sourceToPresent) {
    return async (dispatch, getState) => {
        const callingState = getState().calling;
        const { activeCallState } = callingState;
        const activeCall = (0, exports.getActiveCall)(callingState);
        if (!activeCall || !activeCallState) {
            log.warn('Trying to present when no call is active');
            return;
        }
        calling_1.calling.setPresenting(activeCall.conversationId, activeCallState.hasLocalVideo, sourceToPresent);
        dispatch({
            type: SET_PRESENTING,
            payload: sourceToPresent,
        });
        if (sourceToPresent) {
            await callingTones_1.callingTones.someonePresenting();
        }
    };
}
function setOutgoingRing(payload) {
    return {
        type: SET_OUTGOING_RING,
        payload,
    };
}
function startCallingLobby(payload) {
    return () => {
        calling_1.calling.startCallingLobby(payload.conversationId, payload.isVideoCall);
    };
}
// TODO: This action should be replaced with an action dispatched in the
//   `startCallingLobby` thunk.
function showCallLobby(payload) {
    return {
        type: SHOW_CALL_LOBBY,
        payload,
    };
}
function startCall(payload) {
    return async (dispatch, getState) => {
        switch (payload.callMode) {
            case Calling_1.CallMode.Direct:
                calling_1.calling.startOutgoingDirectCall(payload.conversationId, payload.hasLocalAudio, payload.hasLocalVideo);
                dispatch({
                    type: START_DIRECT_CALL,
                    payload,
                });
                break;
            case Calling_1.CallMode.Group: {
                let outgoingRing;
                const state = getState();
                const { activeCallState } = state.calling;
                if ((0, isGroupCallOutboundRingEnabled_1.isGroupCallOutboundRingEnabled)() && (activeCallState === null || activeCallState === void 0 ? void 0 : activeCallState.outgoingRing)) {
                    const conversation = (0, getOwn_1.getOwn)(state.conversations.conversationLookup, activeCallState.conversationId);
                    outgoingRing = Boolean(conversation && !(0, isConversationTooBigToRing_1.isConversationTooBigToRing)(conversation));
                }
                else {
                    outgoingRing = false;
                }
                await calling_1.calling.joinGroupCall(payload.conversationId, payload.hasLocalAudio, payload.hasLocalVideo, outgoingRing);
                // The calling service should already be wired up to Redux so we don't need to
                //   dispatch anything here.
                break;
            }
            default:
                throw (0, missingCaseError_1.missingCaseError)(payload.callMode);
        }
    };
}
function toggleParticipants() {
    return {
        type: TOGGLE_PARTICIPANTS,
    };
}
function togglePip() {
    return {
        type: TOGGLE_PIP,
    };
}
function toggleScreenRecordingPermissionsDialog() {
    return {
        type: TOGGLE_NEEDS_SCREEN_RECORDING_PERMISSIONS,
    };
}
function toggleSettings() {
    return {
        type: TOGGLE_SETTINGS,
    };
}
function toggleSpeakerView() {
    return {
        type: TOGGLE_SPEAKER_VIEW,
    };
}
exports.actions = {
    acceptCall,
    callStateChange,
    cancelCall,
    cancelIncomingGroupCallRing,
    changeIODevice,
    closeNeedPermissionScreen,
    declineCall,
    getPresentingSources,
    groupCallStateChange,
    hangUp,
    keyChangeOk,
    keyChanged,
    openSystemPreferencesAction,
    outgoingCall,
    peekNotConnectedGroupCall,
    receiveIncomingDirectCall,
    receiveIncomingGroupCall,
    refreshIODevices,
    remoteSharingScreenChange,
    remoteVideoChange,
    returnToActiveCall,
    setGroupCallVideoRequest,
    setIsCallActive,
    setLocalAudio,
    setLocalPreview,
    setLocalVideo,
    setPresenting,
    setRendererCanvas,
    setOutgoingRing,
    showCallLobby,
    startCall,
    startCallingLobby,
    toggleParticipants,
    togglePip,
    toggleScreenRecordingPermissionsDialog,
    toggleSettings,
    toggleSpeakerView,
};
// Reducer
function getEmptyState() {
    return {
        availableCameras: [],
        availableMicrophones: [],
        availableSpeakers: [],
        selectedCamera: undefined,
        selectedMicrophone: undefined,
        selectedSpeaker: undefined,
        callsByConversation: {},
        activeCallState: undefined,
    };
}
exports.getEmptyState = getEmptyState;
function getGroupCall(conversationId, state) {
    const call = (0, getOwn_1.getOwn)(state.callsByConversation, conversationId);
    return (call === null || call === void 0 ? void 0 : call.callMode) === Calling_1.CallMode.Group ? call : undefined;
}
function removeConversationFromState(state, conversationId) {
    var _a;
    return Object.assign(Object.assign({}, (conversationId === ((_a = state.activeCallState) === null || _a === void 0 ? void 0 : _a.conversationId)
        ? (0, lodash_1.omit)(state, 'activeCallState')
        : state)), { callsByConversation: (0, lodash_1.omit)(state.callsByConversation, conversationId) });
}
function reducer(state = getEmptyState(), action) {
    var _a, _b, _c;
    const { callsByConversation } = state;
    if (action.type === SHOW_CALL_LOBBY) {
        const { conversationId } = action.payload;
        let call;
        let outgoingRing;
        switch (action.payload.callMode) {
            case Calling_1.CallMode.Direct:
                call = {
                    callMode: Calling_1.CallMode.Direct,
                    conversationId,
                    isIncoming: false,
                    isVideoCall: action.payload.hasLocalVideo,
                };
                outgoingRing = true;
                break;
            case Calling_1.CallMode.Group: {
                // We expect to be in this state briefly. The Calling service should update the
                //   call state shortly.
                const existingCall = getGroupCall(conversationId, state);
                const ringState = getGroupCallRingState(existingCall);
                call = Object.assign({ callMode: Calling_1.CallMode.Group, conversationId, connectionState: action.payload.connectionState, joinState: action.payload.joinState, peekInfo: action.payload.peekInfo ||
                        (existingCall === null || existingCall === void 0 ? void 0 : existingCall.peekInfo) || {
                        uuids: action.payload.remoteParticipants.map(({ uuid }) => uuid),
                        maxDevices: Infinity,
                        deviceCount: action.payload.remoteParticipants.length,
                    }, remoteParticipants: action.payload.remoteParticipants }, ringState);
                outgoingRing =
                    (0, isGroupCallOutboundRingEnabled_1.isGroupCallOutboundRingEnabled)() &&
                        !ringState.ringId &&
                        !call.peekInfo.uuids.length &&
                        !call.remoteParticipants.length &&
                        !action.payload.isConversationTooBigToRing;
                break;
            }
            default:
                throw (0, missingCaseError_1.missingCaseError)(action.payload);
        }
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [action.payload.conversationId]: call }), activeCallState: {
                conversationId: action.payload.conversationId,
                hasLocalAudio: action.payload.hasLocalAudio,
                hasLocalVideo: action.payload.hasLocalVideo,
                isInSpeakerView: false,
                pip: false,
                safetyNumberChangedUuids: [],
                settingsDialogOpen: false,
                showParticipantsList: false,
                outgoingRing,
            } });
    }
    if (action.type === START_DIRECT_CALL) {
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [action.payload.conversationId]: {
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: action.payload.conversationId,
                    callState: Calling_1.CallState.Prering,
                    isIncoming: false,
                    isVideoCall: action.payload.hasLocalVideo,
                } }), activeCallState: {
                conversationId: action.payload.conversationId,
                hasLocalAudio: action.payload.hasLocalAudio,
                hasLocalVideo: action.payload.hasLocalVideo,
                isInSpeakerView: false,
                pip: false,
                safetyNumberChangedUuids: [],
                settingsDialogOpen: false,
                showParticipantsList: false,
                outgoingRing: true,
            } });
    }
    if (action.type === ACCEPT_CALL_PENDING) {
        if (!(0, lodash_1.has)(state.callsByConversation, action.payload.conversationId)) {
            log.warn('Unable to accept a non-existent call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: {
                conversationId: action.payload.conversationId,
                hasLocalAudio: true,
                hasLocalVideo: action.payload.asVideoCall,
                isInSpeakerView: false,
                pip: false,
                safetyNumberChangedUuids: [],
                settingsDialogOpen: false,
                showParticipantsList: false,
                outgoingRing: false,
            } });
    }
    if (action.type === CANCEL_CALL ||
        action.type === HANG_UP ||
        action.type === CLOSE_NEED_PERMISSION_SCREEN) {
        const activeCall = (0, exports.getActiveCall)(state);
        if (!activeCall) {
            log.warn('No active call to remove');
            return state;
        }
        switch (activeCall.callMode) {
            case Calling_1.CallMode.Direct:
                return removeConversationFromState(state, activeCall.conversationId);
            case Calling_1.CallMode.Group:
                return (0, lodash_1.omit)(state, 'activeCallState');
            default:
                throw (0, missingCaseError_1.missingCaseError)(activeCall);
        }
    }
    if (action.type === CANCEL_INCOMING_GROUP_CALL_RING) {
        const { conversationId, ringId } = action.payload;
        const groupCall = getGroupCall(conversationId, state);
        if (!groupCall || groupCall.ringId !== ringId) {
            return state;
        }
        if (groupCall.connectionState === Calling_1.GroupCallConnectionState.NotConnected) {
            return removeConversationFromState(state, conversationId);
        }
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [conversationId]: (0, lodash_1.omit)(groupCall, ['ringId', 'ringerUuid']) }) });
    }
    if (action.type === 'CONVERSATION_CHANGED') {
        const activeCall = (0, exports.getActiveCall)(state);
        const { activeCallState } = state;
        if (!(activeCallState === null || activeCallState === void 0 ? void 0 : activeCallState.outgoingRing) ||
            activeCallState.conversationId !== action.payload.id ||
            (activeCall === null || activeCall === void 0 ? void 0 : activeCall.callMode) !== Calling_1.CallMode.Group ||
            activeCall.joinState !== Calling_1.GroupCallJoinState.NotJoined ||
            !(0, isConversationTooBigToRing_1.isConversationTooBigToRing)(action.payload.data)) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { outgoingRing: false }) });
    }
    if (action.type === DECLINE_DIRECT_CALL) {
        return removeConversationFromState(state, action.payload.conversationId);
    }
    if (action.type === INCOMING_DIRECT_CALL) {
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [action.payload.conversationId]: {
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: action.payload.conversationId,
                    callState: Calling_1.CallState.Prering,
                    isIncoming: true,
                    isVideoCall: action.payload.isVideoCall,
                } }) });
    }
    if (action.type === INCOMING_GROUP_CALL) {
        const { conversationId, ringId, ringerUuid } = action.payload;
        let groupCall;
        const existingGroupCall = getGroupCall(conversationId, state);
        if (existingGroupCall) {
            if (existingGroupCall.ringerUuid) {
                log.info('Group call was already ringing');
                return state;
            }
            if (existingGroupCall.joinState !== Calling_1.GroupCallJoinState.NotJoined) {
                log.info("Got a ring for a call we're already in");
                return state;
            }
            groupCall = Object.assign(Object.assign({}, existingGroupCall), { ringId,
                ringerUuid });
        }
        else {
            groupCall = {
                callMode: Calling_1.CallMode.Group,
                conversationId,
                connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                joinState: Calling_1.GroupCallJoinState.NotJoined,
                peekInfo: {
                    uuids: [],
                    maxDevices: Infinity,
                    deviceCount: 0,
                },
                remoteParticipants: [],
                ringId,
                ringerUuid,
            };
        }
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [conversationId]: groupCall }) });
    }
    if (action.type === OUTGOING_CALL) {
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [action.payload.conversationId]: {
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: action.payload.conversationId,
                    callState: Calling_1.CallState.Prering,
                    isIncoming: false,
                    isVideoCall: action.payload.hasLocalVideo,
                } }), activeCallState: {
                conversationId: action.payload.conversationId,
                hasLocalAudio: action.payload.hasLocalAudio,
                hasLocalVideo: action.payload.hasLocalVideo,
                isInSpeakerView: false,
                pip: false,
                safetyNumberChangedUuids: [],
                settingsDialogOpen: false,
                showParticipantsList: false,
                outgoingRing: true,
            } });
    }
    if (action.type === CALL_STATE_CHANGE_FULFILLED) {
        // We want to keep the state around for ended calls if they resulted in a message
        //   request so we can show the "needs permission" screen.
        if (action.payload.callState === Calling_1.CallState.Ended &&
            action.payload.callEndedReason !==
                ringrtc_1.CallEndedReason.RemoteHangupNeedPermission) {
            return removeConversationFromState(state, action.payload.conversationId);
        }
        const call = (0, getOwn_1.getOwn)(state.callsByConversation, action.payload.conversationId);
        if ((call === null || call === void 0 ? void 0 : call.callMode) !== Calling_1.CallMode.Direct) {
            log.warn('Cannot update state for a non-direct call');
            return state;
        }
        let activeCallState;
        if (((_a = state.activeCallState) === null || _a === void 0 ? void 0 : _a.conversationId) === action.payload.conversationId) {
            activeCallState = Object.assign(Object.assign({}, state.activeCallState), { joinedAt: action.payload.acceptedTime });
        }
        else {
            ({ activeCallState } = state);
        }
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [action.payload.conversationId]: Object.assign(Object.assign({}, call), { callState: action.payload.callState, callEndedReason: action.payload.callEndedReason }) }), activeCallState });
    }
    if (action.type === GROUP_CALL_STATE_CHANGE) {
        const { connectionState, conversationId, hasLocalAudio, hasLocalVideo, joinState, ourUuid, peekInfo, remoteParticipants, } = action.payload;
        const existingCall = getGroupCall(conversationId, state);
        const existingRingState = getGroupCallRingState(existingCall);
        const newPeekInfo = peekInfo ||
            (existingCall === null || existingCall === void 0 ? void 0 : existingCall.peekInfo) || {
            uuids: remoteParticipants.map(({ uuid }) => uuid),
            maxDevices: Infinity,
            deviceCount: remoteParticipants.length,
        };
        let newActiveCallState;
        if (connectionState === Calling_1.GroupCallConnectionState.NotConnected) {
            newActiveCallState =
                ((_b = state.activeCallState) === null || _b === void 0 ? void 0 : _b.conversationId) === conversationId
                    ? undefined
                    : state.activeCallState;
            if (!(0, exports.isAnybodyElseInGroupCall)(newPeekInfo, ourUuid) &&
                (!existingCall || !existingCall.ringerUuid)) {
                return Object.assign(Object.assign({}, state), { callsByConversation: (0, lodash_1.omit)(callsByConversation, conversationId), activeCallState: newActiveCallState });
            }
        }
        else {
            newActiveCallState =
                ((_c = state.activeCallState) === null || _c === void 0 ? void 0 : _c.conversationId) === conversationId
                    ? Object.assign(Object.assign({}, state.activeCallState), { hasLocalAudio,
                        hasLocalVideo }) : state.activeCallState;
        }
        if (newActiveCallState &&
            newActiveCallState.outgoingRing &&
            newActiveCallState.conversationId === conversationId &&
            (0, exports.isAnybodyElseInGroupCall)(newPeekInfo, ourUuid)) {
            newActiveCallState = Object.assign(Object.assign({}, newActiveCallState), { outgoingRing: false });
        }
        let newRingState;
        if (joinState === Calling_1.GroupCallJoinState.NotJoined) {
            newRingState = existingRingState;
        }
        else {
            newRingState = {};
        }
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [conversationId]: Object.assign({ callMode: Calling_1.CallMode.Group, conversationId,
                    connectionState,
                    joinState, peekInfo: newPeekInfo, remoteParticipants }, newRingState) }), activeCallState: newActiveCallState });
    }
    if (action.type === PEEK_NOT_CONNECTED_GROUP_CALL_FULFILLED) {
        const { conversationId, peekInfo, ourUuid } = action.payload;
        const existingCall = getGroupCall(conversationId, state) || {
            callMode: Calling_1.CallMode.Group,
            conversationId,
            connectionState: Calling_1.GroupCallConnectionState.NotConnected,
            joinState: Calling_1.GroupCallJoinState.NotJoined,
            peekInfo: {
                uuids: [],
                maxDevices: Infinity,
                deviceCount: 0,
            },
            remoteParticipants: [],
        };
        // This action should only update non-connected group calls. It's not necessarily a
        //   mistake if this action is dispatched "over" a connected call. Here's a valid
        //   sequence of events:
        //
        // 1. We ask RingRTC to peek, kicking off an asynchronous operation.
        // 2. The associated group call is joined.
        // 3. The peek promise from step 1 resolves.
        if (existingCall.connectionState !== Calling_1.GroupCallConnectionState.NotConnected) {
            return state;
        }
        if (!(0, exports.isAnybodyElseInGroupCall)(peekInfo, ourUuid) &&
            !existingCall.ringerUuid) {
            return removeConversationFromState(state, conversationId);
        }
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [conversationId]: Object.assign(Object.assign({}, existingCall), { peekInfo }) }) });
    }
    if (action.type === REMOTE_SHARING_SCREEN_CHANGE) {
        const { conversationId, isSharingScreen } = action.payload;
        const call = (0, getOwn_1.getOwn)(state.callsByConversation, conversationId);
        if ((call === null || call === void 0 ? void 0 : call.callMode) !== Calling_1.CallMode.Direct) {
            log.warn('Cannot update remote video for a non-direct call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [conversationId]: Object.assign(Object.assign({}, call), { isSharingScreen }) }) });
    }
    if (action.type === REMOTE_VIDEO_CHANGE) {
        const { conversationId, hasVideo } = action.payload;
        const call = (0, getOwn_1.getOwn)(state.callsByConversation, conversationId);
        if ((call === null || call === void 0 ? void 0 : call.callMode) !== Calling_1.CallMode.Direct) {
            log.warn('Cannot update remote video for a non-direct call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { callsByConversation: Object.assign(Object.assign({}, callsByConversation), { [conversationId]: Object.assign(Object.assign({}, call), { hasRemoteVideo: hasVideo }) }) });
    }
    if (action.type === RETURN_TO_ACTIVE_CALL) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot return to active call if there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { pip: false }) });
    }
    if (action.type === SET_LOCAL_AUDIO_FULFILLED) {
        if (!state.activeCallState) {
            log.warn('Cannot set local audio with no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, state.activeCallState), { hasLocalAudio: action.payload.enabled }) });
    }
    if (action.type === SET_LOCAL_VIDEO_FULFILLED) {
        if (!state.activeCallState) {
            log.warn('Cannot set local video with no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, state.activeCallState), { hasLocalVideo: action.payload.enabled }) });
    }
    if (action.type === CHANGE_IO_DEVICE_FULFILLED) {
        const { selectedDevice } = action.payload;
        const nextState = Object.create(null);
        if (action.payload.type === Calling_1.CallingDeviceType.CAMERA) {
            nextState.selectedCamera = selectedDevice;
        }
        else if (action.payload.type === Calling_1.CallingDeviceType.MICROPHONE) {
            nextState.selectedMicrophone = selectedDevice;
        }
        else if (action.payload.type === Calling_1.CallingDeviceType.SPEAKER) {
            nextState.selectedSpeaker = selectedDevice;
        }
        return Object.assign(Object.assign({}, state), nextState);
    }
    if (action.type === REFRESH_IO_DEVICES) {
        const { availableMicrophones, selectedMicrophone, availableSpeakers, selectedSpeaker, availableCameras, selectedCamera, } = action.payload;
        return Object.assign(Object.assign({}, state), { availableMicrophones,
            selectedMicrophone,
            availableSpeakers,
            selectedSpeaker,
            availableCameras,
            selectedCamera });
    }
    if (action.type === TOGGLE_SETTINGS) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot toggle settings when there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { settingsDialogOpen: !activeCallState.settingsDialogOpen }) });
    }
    if (action.type === TOGGLE_PARTICIPANTS) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot toggle participants list when there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { showParticipantsList: !activeCallState.showParticipantsList }) });
    }
    if (action.type === TOGGLE_PIP) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot toggle PiP when there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { pip: !activeCallState.pip }) });
    }
    if (action.type === SET_PRESENTING) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot toggle presenting when there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { presentingSource: action.payload, presentingSourcesAvailable: undefined }) });
    }
    if (action.type === SET_PRESENTING_SOURCES) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot set presenting sources when there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { presentingSourcesAvailable: action.payload }) });
    }
    if (action.type === SET_OUTGOING_RING) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot set outgoing ring when there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { outgoingRing: action.payload }) });
    }
    if (action.type === TOGGLE_NEEDS_SCREEN_RECORDING_PERMISSIONS) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot set presenting sources when there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { showNeedsScreenRecordingPermissionsWarning: !activeCallState.showNeedsScreenRecordingPermissionsWarning }) });
    }
    if (action.type === TOGGLE_SPEAKER_VIEW) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot toggle speaker view when there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { isInSpeakerView: !activeCallState.isInSpeakerView }) });
    }
    if (action.type === MARK_CALL_UNTRUSTED) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot mark call as untrusted when there is no active call');
            return state;
        }
        const { safetyNumberChangedUuids } = action.payload;
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { pip: false, safetyNumberChangedUuids, settingsDialogOpen: false, showParticipantsList: false }) });
    }
    if (action.type === MARK_CALL_TRUSTED) {
        const { activeCallState } = state;
        if (!activeCallState) {
            log.warn('Cannot mark call as trusted when there is no active call');
            return state;
        }
        return Object.assign(Object.assign({}, state), { activeCallState: Object.assign(Object.assign({}, activeCallState), { safetyNumberChangedUuids: [] }) });
    }
    return state;
}
exports.reducer = reducer;
