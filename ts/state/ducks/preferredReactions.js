"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getInitialState = exports.useActions = exports.actions = void 0;
const lodash_1 = require("lodash");
const log = __importStar(require("../../logging/log"));
const Errors = __importStar(require("../../types/errors"));
const replaceIndex_1 = require("../../util/replaceIndex");
const useBoundActions_1 = require("../../hooks/useBoundActions");
const constants_1 = require("../../reactions/constants");
const preferredReactionEmoji_1 = require("../../reactions/preferredReactionEmoji");
const items_1 = require("../selectors/items");
const lib_1 = require("../../components/emoji/lib");
// Actions
const CANCEL_CUSTOMIZE_PREFERRED_REACTIONS_MODAL = 'preferredReactions/CANCEL_CUSTOMIZE_PREFERRED_REACTIONS_MODAL';
const DESELECT_DRAFT_EMOJI = 'preferredReactions/DESELECT_DRAFT_EMOJI';
const OPEN_CUSTOMIZE_PREFERRED_REACTIONS_MODAL = 'preferredReactions/OPEN_CUSTOMIZE_PREFERRED_REACTIONS_MODAL';
const REPLACE_SELECTED_DRAFT_EMOJI = 'preferredReactions/REPLACE_SELECTED_DRAFT_EMOJI';
const RESET_DRAFT_EMOJI = 'preferredReactions/RESET_DRAFT_EMOJI';
const SAVE_PREFERRED_REACTIONS_FULFILLED = 'preferredReactions/SAVE_PREFERRED_REACTIONS_FULFILLED';
const SAVE_PREFERRED_REACTIONS_PENDING = 'preferredReactions/SAVE_PREFERRED_REACTIONS_PENDING';
const SAVE_PREFERRED_REACTIONS_REJECTED = 'preferredReactions/SAVE_PREFERRED_REACTIONS_REJECTED';
const SELECT_DRAFT_EMOJI_TO_BE_REPLACED = 'preferredReactions/SELECT_DRAFT_EMOJI_TO_BE_REPLACED';
// Action creators
exports.actions = {
    cancelCustomizePreferredReactionsModal,
    deselectDraftEmoji,
    openCustomizePreferredReactionsModal,
    replaceSelectedDraftEmoji,
    resetDraftEmoji,
    savePreferredReactions,
    selectDraftEmojiToBeReplaced,
};
const useActions = () => (0, useBoundActions_1.useBoundActions)(exports.actions);
exports.useActions = useActions;
function cancelCustomizePreferredReactionsModal() {
    return { type: CANCEL_CUSTOMIZE_PREFERRED_REACTIONS_MODAL };
}
function deselectDraftEmoji() {
    return { type: DESELECT_DRAFT_EMOJI };
}
function openCustomizePreferredReactionsModal() {
    return (dispatch, getState) => {
        const state = getState();
        const originalPreferredReactions = (0, preferredReactionEmoji_1.getPreferredReactionEmoji)(getState().items.preferredReactionEmoji, (0, items_1.getEmojiSkinTone)(state));
        dispatch({
            type: OPEN_CUSTOMIZE_PREFERRED_REACTIONS_MODAL,
            payload: { originalPreferredReactions },
        });
    };
}
function replaceSelectedDraftEmoji(newEmoji) {
    return {
        type: REPLACE_SELECTED_DRAFT_EMOJI,
        payload: newEmoji,
    };
}
function resetDraftEmoji() {
    return (dispatch, getState) => {
        const skinTone = (0, items_1.getEmojiSkinTone)(getState());
        dispatch({ type: RESET_DRAFT_EMOJI, payload: { skinTone } });
    };
}
function savePreferredReactions() {
    return async (dispatch, getState) => {
        const { draftPreferredReactions } = getState().preferredReactions.customizePreferredReactionsModal || {};
        if (!draftPreferredReactions) {
            log.error("savePreferredReactions won't work because the modal is not open");
            return;
        }
        let succeeded = false;
        dispatch({ type: SAVE_PREFERRED_REACTIONS_PENDING });
        try {
            await window.storage.put('preferredReactionEmoji', draftPreferredReactions);
            succeeded = true;
        }
        catch (err) {
            log.warn(Errors.toLogFormat(err));
        }
        if (succeeded) {
            dispatch({ type: SAVE_PREFERRED_REACTIONS_FULFILLED });
            window.ConversationController.getOurConversationOrThrow().captureChange('preferredReactionEmoji');
        }
        else {
            dispatch({ type: SAVE_PREFERRED_REACTIONS_REJECTED });
        }
    };
}
function selectDraftEmojiToBeReplaced(index) {
    return {
        type: SELECT_DRAFT_EMOJI_TO_BE_REPLACED,
        payload: index,
    };
}
// Reducer
function getInitialState() {
    return {};
}
exports.getInitialState = getInitialState;
function reducer(state = getInitialState(), action) {
    switch (action.type) {
        case CANCEL_CUSTOMIZE_PREFERRED_REACTIONS_MODAL:
        case SAVE_PREFERRED_REACTIONS_FULFILLED:
            return (0, lodash_1.omit)(state, ['customizePreferredReactionsModal']);
        case DESELECT_DRAFT_EMOJI:
            if (!state.customizePreferredReactionsModal) {
                return state;
            }
            return Object.assign(Object.assign({}, state), { customizePreferredReactionsModal: Object.assign(Object.assign({}, state.customizePreferredReactionsModal), { selectedDraftEmojiIndex: undefined }) });
        case OPEN_CUSTOMIZE_PREFERRED_REACTIONS_MODAL: {
            const { originalPreferredReactions } = action.payload;
            return Object.assign(Object.assign({}, state), { customizePreferredReactionsModal: {
                    draftPreferredReactions: originalPreferredReactions,
                    originalPreferredReactions,
                    selectedDraftEmojiIndex: undefined,
                    isSaving: false,
                    hadSaveError: false,
                } });
        }
        case REPLACE_SELECTED_DRAFT_EMOJI: {
            const newEmoji = action.payload;
            const { customizePreferredReactionsModal } = state;
            if (!customizePreferredReactionsModal) {
                return state;
            }
            const { draftPreferredReactions, selectedDraftEmojiIndex } = customizePreferredReactionsModal;
            if (selectedDraftEmojiIndex === undefined) {
                return state;
            }
            return Object.assign(Object.assign({}, state), { customizePreferredReactionsModal: Object.assign(Object.assign({}, customizePreferredReactionsModal), { draftPreferredReactions: (0, replaceIndex_1.replaceIndex)(draftPreferredReactions, selectedDraftEmojiIndex, newEmoji), selectedDraftEmojiIndex: undefined }) });
        }
        case RESET_DRAFT_EMOJI: {
            const { skinTone } = action.payload;
            if (!state.customizePreferredReactionsModal) {
                return state;
            }
            return Object.assign(Object.assign({}, state), { customizePreferredReactionsModal: Object.assign(Object.assign({}, state.customizePreferredReactionsModal), { draftPreferredReactions: constants_1.DEFAULT_PREFERRED_REACTION_EMOJI_SHORT_NAMES.map(shortName => (0, lib_1.convertShortName)(shortName, skinTone)), selectedDraftEmojiIndex: undefined }) });
        }
        case SAVE_PREFERRED_REACTIONS_PENDING:
            if (!state.customizePreferredReactionsModal) {
                return state;
            }
            return Object.assign(Object.assign({}, state), { customizePreferredReactionsModal: Object.assign(Object.assign({}, state.customizePreferredReactionsModal), { selectedDraftEmojiIndex: undefined, isSaving: true, hadSaveError: false }) });
        case SAVE_PREFERRED_REACTIONS_REJECTED:
            if (!state.customizePreferredReactionsModal) {
                return state;
            }
            return Object.assign(Object.assign({}, state), { customizePreferredReactionsModal: Object.assign(Object.assign({}, state.customizePreferredReactionsModal), { isSaving: false, hadSaveError: true }) });
        case SELECT_DRAFT_EMOJI_TO_BE_REPLACED: {
            const index = action.payload;
            if (!state.customizePreferredReactionsModal ||
                !(index in
                    state.customizePreferredReactionsModal.draftPreferredReactions)) {
                return state;
            }
            return Object.assign(Object.assign({}, state), { customizePreferredReactionsModal: Object.assign(Object.assign({}, state.customizePreferredReactionsModal), { selectedDraftEmojiIndex: index }) });
        }
        default:
            return state;
    }
}
exports.reducer = reducer;
