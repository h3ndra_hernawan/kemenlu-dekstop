"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.useActions = exports.actions = void 0;
const lodash_1 = require("lodash");
const uuid_1 = require("uuid");
const storageShim = __importStar(require("../../shims/storage"));
const useBoundActions_1 = require("../../hooks/useBoundActions");
const Colors_1 = require("../../types/Colors");
const reloadSelectedConversation_1 = require("../../shims/reloadSelectedConversation");
const conversations_1 = require("./conversations");
// Action Creators
exports.actions = {
    addCustomColor,
    editCustomColor,
    removeCustomColor,
    resetDefaultChatColor,
    savePreferredLeftPaneWidth,
    setGlobalDefaultConversationColor,
    onSetSkinTone,
    putItem,
    putItemExternal,
    removeItem,
    removeItemExternal,
    resetItems,
};
const useActions = () => (0, useBoundActions_1.useBoundActions)(exports.actions);
exports.useActions = useActions;
function putItem(key, value) {
    storageShim.put(key, value);
    return {
        type: 'items/PUT',
        payload: null,
    };
}
function onSetSkinTone(tone) {
    return putItem('skinTone', tone);
}
function putItemExternal(key, value) {
    return {
        type: 'items/PUT_EXTERNAL',
        payload: {
            key,
            value,
        },
    };
}
function removeItem(key) {
    storageShim.remove(key);
    return {
        type: 'items/REMOVE',
        payload: null,
    };
}
function removeItemExternal(key) {
    return {
        type: 'items/REMOVE_EXTERNAL',
        payload: key,
    };
}
function resetItems() {
    return { type: 'items/RESET' };
}
function getDefaultCustomColorData() {
    return {
        colors: {},
        version: 1,
    };
}
function addCustomColor(customColor, conversationId) {
    return (dispatch, getState) => {
        const { customColors = getDefaultCustomColorData() } = getState().items;
        let uuid = (0, uuid_1.v4)();
        while (customColors.colors[uuid]) {
            uuid = (0, uuid_1.v4)();
        }
        const nextCustomColors = Object.assign(Object.assign({}, customColors), { colors: Object.assign(Object.assign({}, customColors.colors), { [uuid]: customColor }) });
        dispatch(putItem('customColors', nextCustomColors));
        const customColorData = {
            id: uuid,
            value: customColor,
        };
        if (conversationId) {
            conversations_1.actions.colorSelected({
                conversationId,
                conversationColor: 'custom',
                customColorData,
            })(dispatch, getState, null);
        }
        else {
            setGlobalDefaultConversationColor('custom', customColorData)(dispatch, getState, null);
        }
    };
}
function editCustomColor(colorId, color) {
    return (dispatch, getState) => {
        const { customColors = getDefaultCustomColorData() } = getState().items;
        if (!customColors.colors[colorId]) {
            return;
        }
        const nextCustomColors = Object.assign(Object.assign({}, customColors), { colors: Object.assign(Object.assign({}, customColors.colors), { [colorId]: color }) });
        dispatch(putItem('customColors', nextCustomColors));
    };
}
function removeCustomColor(payload) {
    return (dispatch, getState) => {
        const { customColors = getDefaultCustomColorData() } = getState().items;
        const nextCustomColors = Object.assign(Object.assign({}, customColors), { colors: (0, lodash_1.omit)(customColors.colors, payload) });
        dispatch(putItem('customColors', nextCustomColors));
        resetDefaultChatColor()(dispatch, getState, null);
    };
}
function resetDefaultChatColor() {
    return dispatch => {
        dispatch(putItem('defaultConversationColor', {
            color: Colors_1.ConversationColors[0],
        }));
        (0, reloadSelectedConversation_1.reloadSelectedConversation)();
    };
}
function setGlobalDefaultConversationColor(color, customColorData) {
    return dispatch => {
        dispatch(putItem('defaultConversationColor', {
            color,
            customColorData,
        }));
        (0, reloadSelectedConversation_1.reloadSelectedConversation)();
    };
}
function savePreferredLeftPaneWidth(preferredWidth) {
    return dispatch => {
        dispatch(putItem('preferredLeftPaneWidth', preferredWidth));
    };
}
// Reducer
function getEmptyState() {
    return {
        defaultConversationColor: {
            color: Colors_1.ConversationColors[0],
        },
    };
}
function reducer(state = getEmptyState(), action) {
    if (action.type === 'items/PUT_EXTERNAL') {
        const { payload } = action;
        return Object.assign(Object.assign({}, state), { [payload.key]: payload.value });
    }
    if (action.type === 'items/REMOVE_EXTERNAL') {
        const { payload } = action;
        return (0, lodash_1.omit)(state, payload);
    }
    if (action.type === 'items/RESET') {
        return getEmptyState();
    }
    return state;
}
exports.reducer = reducer;
