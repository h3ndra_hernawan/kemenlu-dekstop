"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getEmptyState = exports.actions = exports.AppViewType = void 0;
const log = __importStar(require("../../logging/log"));
// State
var AppViewType;
(function (AppViewType) {
    AppViewType["Blank"] = "Blank";
    AppViewType["Inbox"] = "Inbox";
    AppViewType["Installer"] = "Installer";
    AppViewType["Standalone"] = "Standalone";
})(AppViewType = exports.AppViewType || (exports.AppViewType = {}));
// Actions
const INITIAL_LOAD_COMPLETE = 'app/INITIAL_LOAD_COMPLETE';
const OPEN_INBOX = 'app/OPEN_INBOX';
const OPEN_INSTALLER = 'app/OPEN_INSTALLER';
const OPEN_STANDALONE = 'app/OPEN_STANDALONE';
exports.actions = {
    initialLoadComplete,
    openInbox,
    openInstaller,
    openStandalone,
};
function initialLoadComplete() {
    return {
        type: INITIAL_LOAD_COMPLETE,
    };
}
function openInbox() {
    return async (dispatch) => {
        log.info('open inbox');
        await window.ConversationController.load();
        dispatch({
            type: OPEN_INBOX,
        });
    };
}
function openInstaller() {
    return dispatch => {
        window.addSetupMenuItems();
        dispatch({
            type: OPEN_INSTALLER,
        });
    };
}
function openStandalone() {
    return dispatch => {
        if (window.getEnvironment() === 'production') {
            return;
        }
        window.addSetupMenuItems();
        dispatch({
            type: OPEN_STANDALONE,
        });
    };
}
// Reducer
function getEmptyState() {
    return {
        appView: AppViewType.Blank,
        hasInitialLoadCompleted: false,
    };
}
exports.getEmptyState = getEmptyState;
function reducer(state = getEmptyState(), action) {
    if (action.type === OPEN_INBOX) {
        return Object.assign(Object.assign({}, state), { appView: AppViewType.Inbox });
    }
    if (action.type === INITIAL_LOAD_COMPLETE) {
        return Object.assign(Object.assign({}, state), { hasInitialLoadCompleted: true });
    }
    if (action.type === OPEN_INSTALLER) {
        return Object.assign(Object.assign({}, state), { appView: AppViewType.Installer });
    }
    if (action.type === OPEN_STANDALONE) {
        return Object.assign(Object.assign({}, state), { appView: AppViewType.Standalone });
    }
    return state;
}
exports.reducer = reducer;
