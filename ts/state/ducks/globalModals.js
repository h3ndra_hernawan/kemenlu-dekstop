"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getEmptyState = exports.actions = exports.TOGGLE_PROFILE_EDITOR_ERROR = void 0;
// Actions
const HIDE_CONTACT_MODAL = 'globalModals/HIDE_CONTACT_MODAL';
const SHOW_CONTACT_MODAL = 'globalModals/SHOW_CONTACT_MODAL';
const SHOW_WHATS_NEW_MODAL = 'globalModals/SHOW_WHATS_NEW_MODAL_MODAL';
const SHOW_USERNAME_NOT_FOUND_MODAL = 'globalModals/SHOW_USERNAME_NOT_FOUND_MODAL';
const HIDE_USERNAME_NOT_FOUND_MODAL = 'globalModals/HIDE_USERNAME_NOT_FOUND_MODAL';
const HIDE_WHATS_NEW_MODAL = 'globalModals/HIDE_WHATS_NEW_MODAL_MODAL';
const TOGGLE_PROFILE_EDITOR = 'globalModals/TOGGLE_PROFILE_EDITOR';
exports.TOGGLE_PROFILE_EDITOR_ERROR = 'globalModals/TOGGLE_PROFILE_EDITOR_ERROR';
const TOGGLE_SAFETY_NUMBER_MODAL = 'globalModals/TOGGLE_SAFETY_NUMBER_MODAL';
// Action Creators
exports.actions = {
    hideContactModal,
    showContactModal,
    hideWhatsNewModal,
    showWhatsNewModal,
    hideUsernameNotFoundModal,
    showUsernameNotFoundModal,
    toggleProfileEditor,
    toggleProfileEditorHasError,
    toggleSafetyNumberModal,
};
function hideContactModal() {
    return {
        type: HIDE_CONTACT_MODAL,
    };
}
function showContactModal(contactId, conversationId) {
    return {
        type: SHOW_CONTACT_MODAL,
        payload: {
            contactId,
            conversationId,
        },
    };
}
function hideWhatsNewModal() {
    return {
        type: HIDE_WHATS_NEW_MODAL,
    };
}
function showWhatsNewModal() {
    return {
        type: SHOW_WHATS_NEW_MODAL,
    };
}
function hideUsernameNotFoundModal() {
    return {
        type: HIDE_USERNAME_NOT_FOUND_MODAL,
    };
}
function showUsernameNotFoundModal(username) {
    return {
        type: SHOW_USERNAME_NOT_FOUND_MODAL,
        payload: {
            username,
        },
    };
}
function toggleProfileEditor() {
    return { type: TOGGLE_PROFILE_EDITOR };
}
function toggleProfileEditorHasError() {
    return { type: exports.TOGGLE_PROFILE_EDITOR_ERROR };
}
function toggleSafetyNumberModal(safetyNumberModalContactId) {
    return {
        type: TOGGLE_SAFETY_NUMBER_MODAL,
        payload: safetyNumberModalContactId,
    };
}
// Reducer
function getEmptyState() {
    return {
        isProfileEditorVisible: false,
        profileEditorHasError: false,
        isWhatsNewVisible: false,
    };
}
exports.getEmptyState = getEmptyState;
function reducer(state = getEmptyState(), action) {
    if (action.type === TOGGLE_PROFILE_EDITOR) {
        return Object.assign(Object.assign({}, state), { isProfileEditorVisible: !state.isProfileEditorVisible });
    }
    if (action.type === exports.TOGGLE_PROFILE_EDITOR_ERROR) {
        return Object.assign(Object.assign({}, state), { profileEditorHasError: !state.profileEditorHasError });
    }
    if (action.type === SHOW_WHATS_NEW_MODAL) {
        return Object.assign(Object.assign({}, state), { isWhatsNewVisible: true });
    }
    if (action.type === HIDE_WHATS_NEW_MODAL) {
        return Object.assign(Object.assign({}, state), { isWhatsNewVisible: false });
    }
    if (action.type === HIDE_USERNAME_NOT_FOUND_MODAL) {
        return Object.assign(Object.assign({}, state), { usernameNotFoundModalState: undefined });
    }
    if (action.type === SHOW_USERNAME_NOT_FOUND_MODAL) {
        const { username } = action.payload;
        return Object.assign(Object.assign({}, state), { usernameNotFoundModalState: {
                username,
            } });
    }
    if (action.type === SHOW_CONTACT_MODAL) {
        return Object.assign(Object.assign({}, state), { contactModalState: action.payload });
    }
    if (action.type === HIDE_CONTACT_MODAL) {
        return Object.assign(Object.assign({}, state), { contactModalState: undefined });
    }
    if (action.type === TOGGLE_SAFETY_NUMBER_MODAL) {
        return Object.assign(Object.assign({}, state), { safetyNumberModalContactId: action.payload });
    }
    return state;
}
exports.reducer = reducer;
