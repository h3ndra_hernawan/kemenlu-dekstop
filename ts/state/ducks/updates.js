"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.actions = void 0;
const updateIpc = __importStar(require("../../shims/updateIpc"));
const Dialogs_1 = require("../../types/Dialogs");
const durations_1 = require("../../util/durations");
// Actions
const DISMISS_DIALOG = 'updates/DISMISS_DIALOG';
const SHOW_UPDATE_DIALOG = 'updates/SHOW_UPDATE_DIALOG';
const SNOOZE_UPDATE = 'updates/SNOOZE_UPDATE';
const START_UPDATE = 'updates/START_UPDATE';
const UNSNOOZE_UPDATE = 'updates/UNSNOOZE_UPDATE';
// Action Creators
function dismissDialog() {
    return {
        type: DISMISS_DIALOG,
    };
}
function showUpdateDialog(dialogType, updateDialogOptions = {}) {
    return {
        type: SHOW_UPDATE_DIALOG,
        payload: {
            dialogType,
            otherState: updateDialogOptions,
        },
    };
}
function snoozeUpdate() {
    return (dispatch, getState) => {
        const { dialogType } = getState().updates;
        setTimeout(() => {
            dispatch({
                type: UNSNOOZE_UPDATE,
                payload: dialogType,
            });
        }, durations_1.DAY);
        dispatch({
            type: SNOOZE_UPDATE,
        });
    };
}
function startUpdate() {
    updateIpc.startUpdate();
    return {
        type: START_UPDATE,
    };
}
exports.actions = {
    dismissDialog,
    showUpdateDialog,
    snoozeUpdate,
    startUpdate,
};
// Reducer
function getEmptyState() {
    return {
        dialogType: Dialogs_1.DialogType.None,
        didSnooze: false,
        showEventsCount: 0,
    };
}
function reducer(state = getEmptyState(), action) {
    if (action.type === SHOW_UPDATE_DIALOG) {
        const { dialogType, otherState } = action.payload;
        return Object.assign(Object.assign(Object.assign({}, state), otherState), { dialogType, showEventsCount: state.showEventsCount + 1 });
    }
    if (action.type === SNOOZE_UPDATE) {
        return Object.assign(Object.assign({}, state), { dialogType: Dialogs_1.DialogType.None, didSnooze: true });
    }
    if (action.type === START_UPDATE) {
        return Object.assign(Object.assign({}, state), { dialogType: Dialogs_1.DialogType.None });
    }
    if (action.type === DISMISS_DIALOG &&
        state.dialogType === Dialogs_1.DialogType.MacOS_Read_Only) {
        return Object.assign(Object.assign({}, state), { dialogType: Dialogs_1.DialogType.None });
    }
    if (action.type === UNSNOOZE_UPDATE) {
        return Object.assign(Object.assign({}, state), { dialogType: action.payload, didSnooze: false });
    }
    return state;
}
exports.reducer = reducer;
