"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getEmptyState = exports.actions = void 0;
const log = __importStar(require("../../logging/log"));
const assignWithNoUnnecessaryAllocation_1 = require("../../util/assignWithNoUnnecessaryAllocation");
const linkPreviews_1 = require("./linkPreviews");
const writeDraftAttachment_1 = require("../../util/writeDraftAttachment");
const deleteDraftAttachment_1 = require("../../util/deleteDraftAttachment");
const replaceIndex_1 = require("../../util/replaceIndex");
const resolveDraftAttachmentOnDisk_1 = require("../../util/resolveDraftAttachmentOnDisk");
const handleAttachmentsProcessing_1 = require("../../util/handleAttachmentsProcessing");
// Actions
const ADD_PENDING_ATTACHMENT = 'composer/ADD_PENDING_ATTACHMENT';
const REPLACE_ATTACHMENTS = 'composer/REPLACE_ATTACHMENTS';
const RESET_COMPOSER = 'composer/RESET_COMPOSER';
const SET_HIGH_QUALITY_SETTING = 'composer/SET_HIGH_QUALITY_SETTING';
const SET_LINK_PREVIEW_RESULT = 'composer/SET_LINK_PREVIEW_RESULT';
const SET_QUOTED_MESSAGE = 'composer/SET_QUOTED_MESSAGE';
// Action Creators
exports.actions = {
    addAttachment,
    addPendingAttachment,
    processAttachments,
    removeAttachment,
    replaceAttachments,
    resetComposer,
    setLinkPreviewResult,
    setMediaQualitySetting,
    setQuotedMessage,
};
// Not cool that we have to pull from ConversationModel here
// but if the current selected conversation isn't the one that we're operating
// on then we won't be able to grab attachments from state so we resort to the
// next in-memory store.
function getAttachmentsFromConversationModel(conversationId) {
    const conversation = window.ConversationController.get(conversationId);
    return (conversation === null || conversation === void 0 ? void 0 : conversation.get('draftAttachments')) || [];
}
function addAttachment(conversationId, attachment) {
    return async (dispatch, getState) => {
        // We do async operations first so multiple in-process addAttachments don't stomp on
        //   each other.
        const onDisk = await (0, writeDraftAttachment_1.writeDraftAttachment)(attachment);
        const isSelectedConversation = getState().conversations.selectedConversationId === conversationId;
        const draftAttachments = isSelectedConversation
            ? getState().composer.attachments
            : getAttachmentsFromConversationModel(conversationId);
        const hasDraftAttachmentPending = draftAttachments.some(draftAttachment => draftAttachment.pending && draftAttachment.path === attachment.path);
        // User has canceled the draft so we don't need to continue processing
        if (!hasDraftAttachmentPending) {
            await (0, deleteDraftAttachment_1.deleteDraftAttachment)(onDisk);
            return;
        }
        // Remove any pending attachments that were transcoding
        const index = draftAttachments.findIndex(draftAttachment => draftAttachment.path === attachment.path);
        let nextAttachments = draftAttachments;
        if (index < 0) {
            log.warn(`addAttachment: Failed to find pending attachment with path ${attachment.path}`);
            nextAttachments = [...draftAttachments, onDisk];
        }
        else {
            nextAttachments = (0, replaceIndex_1.replaceIndex)(draftAttachments, index, onDisk);
        }
        replaceAttachments(conversationId, nextAttachments)(dispatch, getState, null);
        const conversation = window.ConversationController.get(conversationId);
        if (conversation) {
            conversation.attributes.draftAttachments = nextAttachments;
            window.Signal.Data.updateConversation(conversation.attributes);
        }
    };
}
function addPendingAttachment(conversationId, pendingAttachment) {
    return (dispatch, getState) => {
        const isSelectedConversation = getState().conversations.selectedConversationId === conversationId;
        const draftAttachments = isSelectedConversation
            ? getState().composer.attachments
            : getAttachmentsFromConversationModel(conversationId);
        const nextAttachments = [...draftAttachments, pendingAttachment];
        dispatch({
            type: REPLACE_ATTACHMENTS,
            payload: nextAttachments,
        });
        const conversation = window.ConversationController.get(conversationId);
        if (conversation) {
            conversation.attributes.draftAttachments = nextAttachments;
            window.Signal.Data.updateConversation(conversation.attributes);
        }
    };
}
function processAttachments(options) {
    return async (dispatch) => {
        await (0, handleAttachmentsProcessing_1.handleAttachmentsProcessing)(options);
        dispatch({
            type: 'NOOP',
            payload: null,
        });
    };
}
function removeAttachment(conversationId, filePath) {
    return async (dispatch, getState) => {
        const { attachments } = getState().composer;
        const [targetAttachment] = attachments.filter(attachment => attachment.path === filePath);
        if (!targetAttachment) {
            return;
        }
        const nextAttachments = attachments.filter(attachment => attachment.path !== filePath);
        const conversation = window.ConversationController.get(conversationId);
        if (conversation) {
            conversation.attributes.draftAttachments = nextAttachments;
            conversation.attributes.draftChanged = true;
            window.Signal.Data.updateConversation(conversation.attributes);
        }
        replaceAttachments(conversationId, nextAttachments)(dispatch, getState, null);
        if (targetAttachment.path &&
            targetAttachment.fileName !== targetAttachment.path) {
            await (0, deleteDraftAttachment_1.deleteDraftAttachment)(targetAttachment);
        }
    };
}
function replaceAttachments(conversationId, attachments) {
    return (dispatch, getState) => {
        // If the call came from a conversation we are no longer in we do not
        // update the state.
        if (getState().conversations.selectedConversationId !== conversationId) {
            return;
        }
        dispatch({
            type: REPLACE_ATTACHMENTS,
            payload: attachments.map(resolveDraftAttachmentOnDisk_1.resolveDraftAttachmentOnDisk),
        });
    };
}
function resetComposer() {
    return {
        type: RESET_COMPOSER,
    };
}
function setLinkPreviewResult(isLoading, linkPreview) {
    return {
        type: SET_LINK_PREVIEW_RESULT,
        payload: {
            isLoading,
            linkPreview,
        },
    };
}
function setMediaQualitySetting(payload) {
    return {
        type: SET_HIGH_QUALITY_SETTING,
        payload,
    };
}
function setQuotedMessage(payload) {
    return {
        type: SET_QUOTED_MESSAGE,
        payload,
    };
}
// Reducer
function getEmptyState() {
    return {
        attachments: [],
        linkPreviewLoading: false,
        shouldSendHighQualityAttachments: false,
    };
}
exports.getEmptyState = getEmptyState;
function reducer(state = getEmptyState(), action) {
    if (action.type === RESET_COMPOSER) {
        return getEmptyState();
    }
    if (action.type === REPLACE_ATTACHMENTS) {
        const { payload: attachments } = action;
        return Object.assign(Object.assign(Object.assign({}, state), { attachments }), (attachments.length
            ? {}
            : { shouldSendHighQualityAttachments: false }));
    }
    if (action.type === SET_HIGH_QUALITY_SETTING) {
        return Object.assign(Object.assign({}, state), { shouldSendHighQualityAttachments: action.payload });
    }
    if (action.type === SET_QUOTED_MESSAGE) {
        return Object.assign(Object.assign({}, state), { quotedMessage: action.payload });
    }
    if (action.type === SET_LINK_PREVIEW_RESULT) {
        return Object.assign(Object.assign({}, state), { linkPreviewLoading: action.payload.isLoading, linkPreviewResult: action.payload.linkPreview });
    }
    if (action.type === linkPreviews_1.REMOVE_PREVIEW) {
        return (0, assignWithNoUnnecessaryAllocation_1.assignWithNoUnnecessaryAllocation)(state, {
            linkPreviewLoading: false,
            linkPreviewResult: undefined,
        });
    }
    if (action.type === ADD_PENDING_ATTACHMENT) {
        return Object.assign(Object.assign({}, state), { attachments: [...state.attachments, action.payload] });
    }
    return state;
}
exports.reducer = reducer;
