"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.getEmptyState = exports.actions = void 0;
const userLanguages_1 = require("../../util/userLanguages");
// Action Creators
exports.actions = {
    checkForAccount,
};
function checkForAccount(identifier) {
    return async (dispatch) => {
        if (!window.textsecure.messaging) {
            dispatch({
                type: 'NOOP',
                payload: null,
            });
            return;
        }
        let hasAccount = false;
        try {
            await window.textsecure.messaging.getProfile(identifier, {
                userLanguages: (0, userLanguages_1.getUserLanguages)(navigator.languages, window.getLocale()),
            });
            hasAccount = true;
        }
        catch (_error) {
            // Doing nothing with this failed fetch
        }
        dispatch({
            type: 'accounts/UPDATE',
            payload: {
                identifier,
                hasAccount,
            },
        });
    };
}
// Reducer
function getEmptyState() {
    return {
        accounts: {},
    };
}
exports.getEmptyState = getEmptyState;
function reducer(state = getEmptyState(), action) {
    if (!state) {
        return getEmptyState();
    }
    if (action.type === 'accounts/UPDATE') {
        const { payload } = action;
        const { identifier, hasAccount } = payload;
        return Object.assign(Object.assign({}, state), { accounts: Object.assign(Object.assign({}, state.accounts), { [identifier]: hasAccount }) });
    }
    return state;
}
exports.reducer = reducer;
