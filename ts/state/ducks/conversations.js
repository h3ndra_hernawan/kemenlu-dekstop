"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.updateConversationLookups = exports.getEmptyState = exports.actions = exports.COLOR_SELECTED = exports.COLORS_CHANGED = exports.getConversationCallMode = exports.ConversationTypes = exports.InteractionModes = void 0;
const lodash_1 = require("lodash");
const groups = __importStar(require("../../groups"));
const log = __importStar(require("../../logging/log"));
const calling_1 = require("../../services/calling");
const getOwn_1 = require("../../util/getOwn");
const assert_1 = require("../../util/assert");
const universalExpireTimer = __importStar(require("../../util/universalExpireTimer"));
const events_1 = require("../../shims/events");
const globalModals_1 = require("./globalModals");
const isRecord_1 = require("../../util/isRecord");
const Calling_1 = require("../../types/Calling");
const UUID_1 = require("../../types/UUID");
const limits_1 = require("../../groups/limits");
const getMessagesById_1 = require("../../messages/getMessagesById");
const isMessageUnread_1 = require("../../util/isMessageUnread");
const toggleSelectedContactForGroupAddition_1 = require("../../groups/toggleSelectedContactForGroupAddition");
const contactSpoofing_1 = require("../../util/contactSpoofing");
const writeProfile_1 = require("../../services/writeProfile");
const writeUsername_1 = require("../../services/writeUsername");
const conversations_1 = require("../selectors/conversations");
const Avatar_1 = require("../../types/Avatar");
const getAvatarData_1 = require("../../util/getAvatarData");
const isSameAvatarData_1 = require("../../util/isSameAvatarData");
const longRunningTaskWrapper_1 = require("../../util/longRunningTaskWrapper");
const conversationsEnums_1 = require("./conversationsEnums");
const showToast_1 = require("../../util/showToast");
const ToastFailedToDeleteUsername_1 = require("../../components/ToastFailedToDeleteUsername");
const ToastFailedToFetchUsername_1 = require("../../components/ToastFailedToFetchUsername");
const Username_1 = require("../../types/Username");
exports.InteractionModes = ['mouse', 'keyboard'];
exports.ConversationTypes = ['direct', 'group'];
// Helpers
const getConversationCallMode = (conversation) => {
    if (conversation.left ||
        conversation.isBlocked ||
        conversation.isMe ||
        !conversation.acceptedMessageRequest) {
        return Calling_1.CallMode.None;
    }
    if (conversation.type === 'direct') {
        return Calling_1.CallMode.Direct;
    }
    if (conversation.type === 'group' && conversation.groupVersion === 2) {
        return Calling_1.CallMode.Group;
    }
    return Calling_1.CallMode.None;
};
exports.getConversationCallMode = getConversationCallMode;
// Actions
exports.COLORS_CHANGED = 'conversations/COLORS_CHANGED';
exports.COLOR_SELECTED = 'conversations/COLOR_SELECTED';
const CANCEL_MESSAGES_PENDING_CONVERSATION_VERIFICATION = 'conversations/CANCEL_MESSAGES_PENDING_CONVERSATION_VERIFICATION';
const COMPOSE_TOGGLE_EDITING_AVATAR = 'conversations/compose/COMPOSE_TOGGLE_EDITING_AVATAR';
const COMPOSE_ADD_AVATAR = 'conversations/compose/ADD_AVATAR';
const COMPOSE_REMOVE_AVATAR = 'conversations/compose/REMOVE_AVATAR';
const COMPOSE_REPLACE_AVATAR = 'conversations/compose/REPLACE_AVATAR';
const CUSTOM_COLOR_REMOVED = 'conversations/CUSTOM_COLOR_REMOVED';
const MESSAGE_STOPPED_BY_MISSING_VERIFICATION = 'conversations/MESSAGE_STOPPED_BY_MISSING_VERIFICATION';
const REPLACE_AVATARS = 'conversations/REPLACE_AVATARS';
const UPDATE_USERNAME_SAVE_STATE = 'conversations/UPDATE_USERNAME_SAVE_STATE';
// Action Creators
exports.actions = {
    cancelMessagesPendingConversationVerification,
    cantAddContactToGroup,
    clearChangedMessages,
    clearGroupCreationError,
    clearInvitedUuidsForNewlyCreatedGroup,
    clearSelectedMessage,
    clearUnreadMetrics,
    clearUsernameSave,
    closeCantAddContactToGroupModal,
    closeContactSpoofingReview,
    closeMaximumGroupSizeModal,
    closeRecommendedGroupSizeModal,
    colorSelected,
    composeDeleteAvatarFromDisk,
    composeReplaceAvatar,
    composeSaveAvatarToDisk,
    conversationAdded,
    conversationChanged,
    conversationRemoved,
    conversationUnloaded,
    createGroup,
    deleteAvatarFromDisk,
    doubleCheckMissingQuoteReference,
    messageStoppedByMissingVerification,
    messageChanged,
    messageDeleted,
    messageExpanded,
    messageSizeChanged,
    messagesAdded,
    messagesReset,
    myProfileChanged,
    openConversationExternal,
    openConversationInternal,
    removeAllConversations,
    removeCustomColorOnConversations,
    removeMemberFromGroup,
    repairNewestMessage,
    repairOldestMessage,
    replaceAvatar,
    resetAllChatColors,
    reviewGroupMemberNameCollision,
    reviewMessageRequestNameCollision,
    saveAvatarToDisk,
    saveUsername,
    scrollToBottom,
    scrollToMessage,
    selectMessage,
    setComposeGroupAvatar,
    setComposeGroupExpireTimer,
    setComposeGroupName,
    setComposeSearchTerm,
    setIsNearBottom,
    setLoadCountdownStart,
    setMessagesLoading,
    setPreJoinConversation,
    setRecentMediaItems,
    setSelectedConversationHeaderTitle,
    setSelectedConversationPanelDepth,
    showArchivedConversations,
    showChooseGroupMembers,
    showInbox,
    startComposing,
    startNewConversationFromPhoneNumber,
    startNewConversationFromUsername,
    startSettingGroupMetadata,
    toggleAdmin,
    toggleConversationInChooseMembers,
    toggleComposeEditingAvatar,
    updateConversationModelSharedGroups,
    verifyConversationsStoppingMessageSend,
};
function filterAvatarData(avatars, data) {
    return avatars.filter(avatarData => !(0, isSameAvatarData_1.isSameAvatarData)(data, avatarData));
}
function getNextAvatarId(avatars) {
    return Math.max(...avatars.map(x => Number(x.id))) + 1;
}
async function getAvatarsAndUpdateConversation(conversations, conversationId, getNextAvatarsData) {
    const conversation = window.ConversationController.get(conversationId);
    if (!conversation) {
        throw new Error('No conversation found');
    }
    const { conversationLookup } = conversations;
    const conversationAttrs = conversationLookup[conversationId];
    const avatars = conversationAttrs.avatars || (0, getAvatarData_1.getAvatarData)(conversation.attributes);
    const nextAvatarId = getNextAvatarId(avatars);
    const nextAvatars = getNextAvatarsData(avatars, nextAvatarId);
    // We don't save buffers to the db, but we definitely want it in-memory so
    // we don't have to re-generate them.
    //
    // Mutating here because we don't want to trigger a model change
    // because we're updating redux here manually ourselves. Au revoir Backbone!
    conversation.attributes.avatars = nextAvatars.map(avatarData => (0, lodash_1.omit)(avatarData, ['buffer']));
    await window.Signal.Data.updateConversation(conversation.attributes);
    return nextAvatars;
}
function deleteAvatarFromDisk(avatarData, conversationId) {
    return async (dispatch, getState) => {
        if (avatarData.imagePath) {
            await window.Signal.Migrations.deleteAvatar(avatarData.imagePath);
        }
        else {
            log.info('No imagePath for avatarData. Removing from userAvatarData, but not disk');
        }
        (0, assert_1.strictAssert)(conversationId, 'conversationId not provided');
        const avatars = await getAvatarsAndUpdateConversation(getState().conversations, conversationId, prevAvatarsData => filterAvatarData(prevAvatarsData, avatarData));
        dispatch({
            type: REPLACE_AVATARS,
            payload: {
                conversationId,
                avatars,
            },
        });
    };
}
function replaceAvatar(curr, prev, conversationId) {
    return async (dispatch, getState) => {
        (0, assert_1.strictAssert)(conversationId, 'conversationId not provided');
        const avatars = await getAvatarsAndUpdateConversation(getState().conversations, conversationId, (prevAvatarsData, nextId) => {
            var _a;
            const newAvatarData = Object.assign(Object.assign({}, curr), { id: (_a = prev === null || prev === void 0 ? void 0 : prev.id) !== null && _a !== void 0 ? _a : nextId });
            const existingAvatarsData = prev
                ? filterAvatarData(prevAvatarsData, prev)
                : prevAvatarsData;
            return [newAvatarData, ...existingAvatarsData];
        });
        dispatch({
            type: REPLACE_AVATARS,
            payload: {
                conversationId,
                avatars,
            },
        });
    };
}
function saveAvatarToDisk(avatarData, conversationId) {
    return async (dispatch, getState) => {
        if (!avatarData.buffer) {
            throw new Error('No avatar Uint8Array provided');
        }
        (0, assert_1.strictAssert)(conversationId, 'conversationId not provided');
        const imagePath = await window.Signal.Migrations.writeNewAvatarData(avatarData.buffer);
        const avatars = await getAvatarsAndUpdateConversation(getState().conversations, conversationId, (prevAvatarsData, id) => {
            const newAvatarData = Object.assign(Object.assign({}, avatarData), { imagePath,
                id });
            return [newAvatarData, ...prevAvatarsData];
        });
        dispatch({
            type: REPLACE_AVATARS,
            payload: {
                conversationId,
                avatars,
            },
        });
    };
}
function makeUsernameSaveType(newSaveState) {
    return {
        type: UPDATE_USERNAME_SAVE_STATE,
        payload: {
            newSaveState,
        },
    };
}
function clearUsernameSave() {
    return makeUsernameSaveType(conversationsEnums_1.UsernameSaveState.None);
}
function saveUsername({ username, previousUsername, }) {
    return async (dispatch, getState) => {
        const state = getState();
        const previousState = (0, conversations_1.getUsernameSaveState)(state);
        if (previousState !== conversationsEnums_1.UsernameSaveState.None) {
            log.error(`saveUsername: Save requested, but previous state was ${previousState}`);
            dispatch(makeUsernameSaveType(conversationsEnums_1.UsernameSaveState.GeneralError));
            return;
        }
        try {
            dispatch(makeUsernameSaveType(conversationsEnums_1.UsernameSaveState.Saving));
            await (0, writeUsername_1.writeUsername)({ username, previousUsername });
            // writeUsername above updates the backbone model which in turn updates
            // redux through it's on:change event listener. Once we lose Backbone
            // we'll need to manually sync these new changes.
            dispatch(makeUsernameSaveType(conversationsEnums_1.UsernameSaveState.Success));
        }
        catch (error) {
            // Check to see if we were deleting
            if (!username) {
                dispatch(makeUsernameSaveType(conversationsEnums_1.UsernameSaveState.DeleteFailed));
                (0, showToast_1.showToast)(ToastFailedToDeleteUsername_1.ToastFailedToDeleteUsername);
                return;
            }
            if (!(0, isRecord_1.isRecord)(error)) {
                dispatch(makeUsernameSaveType(conversationsEnums_1.UsernameSaveState.GeneralError));
                return;
            }
            if (error.code === 409) {
                dispatch(makeUsernameSaveType(conversationsEnums_1.UsernameSaveState.UsernameTakenError));
                return;
            }
            if (error.code === 400) {
                dispatch(makeUsernameSaveType(conversationsEnums_1.UsernameSaveState.UsernameMalformedError));
                return;
            }
            dispatch(makeUsernameSaveType(conversationsEnums_1.UsernameSaveState.GeneralError));
        }
    };
}
function myProfileChanged(profileData, avatarBuffer) {
    return async (dispatch, getState) => {
        const conversation = (0, conversations_1.getMe)(getState());
        try {
            await (0, writeProfile_1.writeProfile)(Object.assign(Object.assign({}, conversation), profileData), avatarBuffer);
            // writeProfile above updates the backbone model which in turn updates
            // redux through it's on:change event listener. Once we lose Backbone
            // we'll need to manually sync these new changes.
            dispatch({
                type: 'NOOP',
                payload: null,
            });
        }
        catch (err) {
            log.error('myProfileChanged', err && err.stack ? err.stack : err);
            dispatch({ type: globalModals_1.TOGGLE_PROFILE_EDITOR_ERROR });
        }
    };
}
function removeCustomColorOnConversations(colorId) {
    return async (dispatch) => {
        const conversationsToUpdate = [];
        // We don't want to trigger a model change because we're updating redux
        // here manually ourselves. Au revoir Backbone!
        window.getConversations().forEach(conversation => {
            if (conversation.get('customColorId') === colorId) {
                // eslint-disable-next-line no-param-reassign
                delete conversation.attributes.conversationColor;
                // eslint-disable-next-line no-param-reassign
                delete conversation.attributes.customColor;
                // eslint-disable-next-line no-param-reassign
                delete conversation.attributes.customColorId;
                conversationsToUpdate.push(conversation.attributes);
            }
        });
        if (conversationsToUpdate.length) {
            await window.Signal.Data.updateConversations(conversationsToUpdate);
        }
        dispatch({
            type: CUSTOM_COLOR_REMOVED,
            payload: {
                colorId,
            },
        });
    };
}
function resetAllChatColors() {
    return async (dispatch) => {
        // Calling this with no args unsets all the colors in the db
        await window.Signal.Data.updateAllConversationColors();
        // We don't want to trigger a model change because we're updating redux
        // here manually ourselves. Au revoir Backbone!
        window.getConversations().forEach(conversation => {
            // eslint-disable-next-line no-param-reassign
            delete conversation.attributes.conversationColor;
            // eslint-disable-next-line no-param-reassign
            delete conversation.attributes.customColor;
            // eslint-disable-next-line no-param-reassign
            delete conversation.attributes.customColorId;
        });
        dispatch({
            type: exports.COLORS_CHANGED,
            payload: {
                conversationColor: undefined,
                customColorData: undefined,
            },
        });
    };
}
function colorSelected({ conversationId, conversationColor, customColorData, }) {
    return async (dispatch) => {
        // We don't want to trigger a model change because we're updating redux
        // here manually ourselves. Au revoir Backbone!
        const conversation = window.ConversationController.get(conversationId);
        if (conversation) {
            if (conversationColor) {
                conversation.attributes.conversationColor = conversationColor;
                if (customColorData) {
                    conversation.attributes.customColor = customColorData.value;
                    conversation.attributes.customColorId = customColorData.id;
                }
                else {
                    delete conversation.attributes.customColor;
                    delete conversation.attributes.customColorId;
                }
            }
            else {
                delete conversation.attributes.conversationColor;
                delete conversation.attributes.customColor;
                delete conversation.attributes.customColorId;
            }
            await window.Signal.Data.updateConversation(conversation.attributes);
        }
        dispatch({
            type: exports.COLOR_SELECTED,
            payload: {
                conversationId,
                conversationColor,
                customColorData,
            },
        });
    };
}
function toggleComposeEditingAvatar() {
    return {
        type: COMPOSE_TOGGLE_EDITING_AVATAR,
    };
}
function verifyConversationsStoppingMessageSend() {
    return async (_dispatch, getState) => {
        const conversationIds = Object.keys(getState().conversations.outboundMessagesPendingConversationVerification);
        await Promise.all(conversationIds.map(async (conversationId) => {
            const conversation = window.ConversationController.get(conversationId);
            await (conversation === null || conversation === void 0 ? void 0 : conversation.setVerifiedDefault());
        }));
    };
}
function composeSaveAvatarToDisk(avatarData) {
    return async (dispatch) => {
        if (!avatarData.buffer) {
            throw new Error('No avatar Uint8Array provided');
        }
        const imagePath = await window.Signal.Migrations.writeNewAvatarData(avatarData.buffer);
        dispatch({
            type: COMPOSE_ADD_AVATAR,
            payload: Object.assign(Object.assign({}, avatarData), { imagePath }),
        });
    };
}
function composeDeleteAvatarFromDisk(avatarData) {
    return async (dispatch) => {
        if (avatarData.imagePath) {
            await window.Signal.Migrations.deleteAvatar(avatarData.imagePath);
        }
        else {
            log.info('No imagePath for avatarData. Removing from userAvatarData, but not disk');
        }
        dispatch({
            type: COMPOSE_REMOVE_AVATAR,
            payload: avatarData,
        });
    };
}
function composeReplaceAvatar(curr, prev) {
    return {
        type: COMPOSE_REPLACE_AVATAR,
        payload: {
            curr,
            prev,
        },
    };
}
function cancelMessagesPendingConversationVerification() {
    return async (dispatch, getState) => {
        const messageIdsPending = (0, conversations_1.getMessageIdsPendingBecauseOfVerification)(getState());
        const messagesStopped = await (0, getMessagesById_1.getMessagesById)([...messageIdsPending]);
        messagesStopped.forEach(message => {
            message.markFailed();
        });
        dispatch({
            type: CANCEL_MESSAGES_PENDING_CONVERSATION_VERIFICATION,
        });
        await window.Signal.Data.saveMessages(messagesStopped.map(message => message.attributes));
    };
}
function cantAddContactToGroup(conversationId) {
    return {
        type: 'CANT_ADD_CONTACT_TO_GROUP',
        payload: { conversationId },
    };
}
function setPreJoinConversation(data) {
    return {
        type: 'SET_PRE_JOIN_CONVERSATION',
        payload: {
            data,
        },
    };
}
function conversationAdded(id, data) {
    return {
        type: 'CONVERSATION_ADDED',
        payload: {
            id,
            data,
        },
    };
}
function conversationChanged(id, data) {
    return async (dispatch, getState) => {
        var _a;
        calling_1.calling.groupMembersChanged(id);
        if (!data.isUntrusted) {
            const messageIdsPending = (_a = (0, getOwn_1.getOwn)(getState().conversations
                .outboundMessagesPendingConversationVerification, id)) !== null && _a !== void 0 ? _a : [];
            if (messageIdsPending.length) {
                const messagesPending = await (0, getMessagesById_1.getMessagesById)(messageIdsPending);
                messagesPending.forEach(message => {
                    message.retrySend();
                });
            }
        }
        dispatch({
            type: 'CONVERSATION_CHANGED',
            payload: {
                id,
                data,
            },
        });
    };
}
function conversationRemoved(id) {
    return {
        type: 'CONVERSATION_REMOVED',
        payload: {
            id,
        },
    };
}
function conversationUnloaded(id) {
    return {
        type: 'CONVERSATION_UNLOADED',
        payload: {
            id,
        },
    };
}
function createGroup() {
    return async (dispatch, getState, ...args) => {
        const { composer } = getState().conversations;
        if ((composer === null || composer === void 0 ? void 0 : composer.step) !== conversationsEnums_1.ComposerStep.SetGroupMetadata ||
            composer.isCreating) {
            (0, assert_1.assert)(false, 'Cannot create group in this stage; doing nothing');
            return;
        }
        dispatch({ type: 'CREATE_GROUP_PENDING' });
        try {
            const conversation = await groups.createGroupV2({
                name: composer.groupName.trim(),
                avatar: composer.groupAvatar,
                avatars: composer.userAvatarData.map(avatarData => (0, lodash_1.omit)(avatarData, ['buffer'])),
                expireTimer: composer.groupExpireTimer,
                conversationIds: composer.selectedConversationIds,
            });
            dispatch({
                type: 'CREATE_GROUP_FULFILLED',
                payload: {
                    invitedUuids: (conversation.get('pendingMembersV2') || []).map(member => member.uuid),
                },
            });
            openConversationInternal({
                conversationId: conversation.id,
                switchToAssociatedView: true,
            })(dispatch, getState, ...args);
        }
        catch (err) {
            log.error('Failed to create group', err && err.stack ? err.stack : err);
            dispatch({ type: 'CREATE_GROUP_REJECTED' });
        }
    };
}
function removeAllConversations() {
    return {
        type: 'CONVERSATIONS_REMOVE_ALL',
        payload: null,
    };
}
function selectMessage(messageId, conversationId) {
    return {
        type: 'MESSAGE_SELECTED',
        payload: {
            messageId,
            conversationId,
        },
    };
}
function messageStoppedByMissingVerification(messageId, untrustedConversationIds) {
    return {
        type: MESSAGE_STOPPED_BY_MISSING_VERIFICATION,
        payload: {
            messageId,
            untrustedConversationIds,
        },
    };
}
function messageChanged(id, conversationId, data) {
    return {
        type: 'MESSAGE_CHANGED',
        payload: {
            id,
            conversationId,
            data,
        },
    };
}
function messageDeleted(id, conversationId) {
    return {
        type: 'MESSAGE_DELETED',
        payload: {
            id,
            conversationId,
        },
    };
}
function messageExpanded(id, displayLimit) {
    return {
        type: 'MESSAGE_EXPANDED',
        payload: {
            id,
            displayLimit,
        },
    };
}
function messageSizeChanged(id, conversationId) {
    return {
        type: 'MESSAGE_SIZE_CHANGED',
        payload: {
            id,
            conversationId,
        },
    };
}
function messagesAdded(conversationId, messages, isNewMessage, isActive) {
    return {
        type: 'MESSAGES_ADDED',
        payload: {
            conversationId,
            messages,
            isNewMessage,
            isActive,
        },
    };
}
function repairNewestMessage(conversationId) {
    return {
        type: 'REPAIR_NEWEST_MESSAGE',
        payload: {
            conversationId,
        },
    };
}
function repairOldestMessage(conversationId) {
    return {
        type: 'REPAIR_OLDEST_MESSAGE',
        payload: {
            conversationId,
        },
    };
}
function reviewGroupMemberNameCollision(groupConversationId) {
    return {
        type: 'REVIEW_GROUP_MEMBER_NAME_COLLISION',
        payload: { groupConversationId },
    };
}
function reviewMessageRequestNameCollision(payload) {
    return { type: 'REVIEW_MESSAGE_REQUEST_NAME_COLLISION', payload };
}
function messagesReset(conversationId, messages, metrics, scrollToMessageId, unboundedFetch) {
    return {
        type: 'MESSAGES_RESET',
        payload: {
            unboundedFetch: Boolean(unboundedFetch),
            conversationId,
            messages,
            metrics,
            scrollToMessageId,
        },
    };
}
function setMessagesLoading(conversationId, isLoadingMessages) {
    return {
        type: 'SET_MESSAGES_LOADING',
        payload: {
            conversationId,
            isLoadingMessages,
        },
    };
}
function setLoadCountdownStart(conversationId, loadCountdownStart) {
    return {
        type: 'SET_LOAD_COUNTDOWN_START',
        payload: {
            conversationId,
            loadCountdownStart,
        },
    };
}
function setIsNearBottom(conversationId, isNearBottom) {
    return {
        type: 'SET_NEAR_BOTTOM',
        payload: {
            conversationId,
            isNearBottom,
        },
    };
}
function setSelectedConversationHeaderTitle(title) {
    return {
        type: 'SET_CONVERSATION_HEADER_TITLE',
        payload: { title },
    };
}
function setSelectedConversationPanelDepth(panelDepth) {
    return {
        type: 'SET_SELECTED_CONVERSATION_PANEL_DEPTH',
        payload: { panelDepth },
    };
}
function setRecentMediaItems(id, recentMediaItems) {
    return {
        type: 'SET_RECENT_MEDIA_ITEMS',
        payload: { id, recentMediaItems },
    };
}
function clearChangedMessages(conversationId) {
    return {
        type: 'CLEAR_CHANGED_MESSAGES',
        payload: {
            conversationId,
        },
    };
}
function clearInvitedUuidsForNewlyCreatedGroup() {
    return { type: 'CLEAR_INVITED_UUIDS_FOR_NEWLY_CREATED_GROUP' };
}
function clearGroupCreationError() {
    return { type: 'CLEAR_GROUP_CREATION_ERROR' };
}
function clearSelectedMessage() {
    return {
        type: 'CLEAR_SELECTED_MESSAGE',
        payload: null,
    };
}
function clearUnreadMetrics(conversationId) {
    return {
        type: 'CLEAR_UNREAD_METRICS',
        payload: {
            conversationId,
        },
    };
}
function closeCantAddContactToGroupModal() {
    return { type: 'CLOSE_CANT_ADD_CONTACT_TO_GROUP_MODAL' };
}
function closeContactSpoofingReview() {
    return { type: 'CLOSE_CONTACT_SPOOFING_REVIEW' };
}
function closeMaximumGroupSizeModal() {
    return { type: 'CLOSE_MAXIMUM_GROUP_SIZE_MODAL' };
}
function closeRecommendedGroupSizeModal() {
    return { type: 'CLOSE_RECOMMENDED_GROUP_SIZE_MODAL' };
}
function scrollToBottom(conversationId) {
    return {
        type: 'SCROLL_TO_BOTTOM',
        payload: {
            conversationId,
        },
    };
}
function scrollToMessage(conversationId, messageId) {
    return {
        type: 'SCROLL_TO_MESSAGE',
        payload: {
            conversationId,
            messageId,
        },
    };
}
function setComposeGroupAvatar(groupAvatar) {
    return {
        type: 'SET_COMPOSE_GROUP_AVATAR',
        payload: { groupAvatar },
    };
}
function setComposeGroupName(groupName) {
    return {
        type: 'SET_COMPOSE_GROUP_NAME',
        payload: { groupName },
    };
}
function setComposeGroupExpireTimer(groupExpireTimer) {
    return {
        type: 'SET_COMPOSE_GROUP_EXPIRE_TIMER',
        payload: { groupExpireTimer },
    };
}
function setComposeSearchTerm(searchTerm) {
    return {
        type: 'SET_COMPOSE_SEARCH_TERM',
        payload: { searchTerm },
    };
}
function startComposing() {
    return { type: 'START_COMPOSING' };
}
function showChooseGroupMembers() {
    return { type: 'SHOW_CHOOSE_GROUP_MEMBERS' };
}
function startNewConversationFromPhoneNumber(e164) {
    return dispatch => {
        (0, events_1.trigger)('showConversation', e164);
        dispatch(showInbox());
    };
}
async function checkForUsername(username) {
    if (!(0, Username_1.isValidUsername)(username)) {
        return undefined;
    }
    try {
        const profile = await window.textsecure.messaging.getProfileForUsername(username);
        if (!profile.username || profile.username !== username) {
            log.error("checkForUsername: Returned username didn't match searched");
            return;
        }
        if (!profile.uuid) {
            log.error("checkForUsername: Returned profile didn't include a uuid");
            return;
        }
        return {
            uuid: UUID_1.UUID.cast(profile.uuid),
            username: profile.username,
        };
    }
    catch (error) {
        if (!(0, isRecord_1.isRecord)(error)) {
            throw error;
        }
        if (error.code === 404) {
            return undefined;
        }
        throw error;
    }
}
function startNewConversationFromUsername(username) {
    return async (dispatch, getState) => {
        const state = getState();
        const byUsername = (0, conversations_1.getConversationsByUsername)(state);
        const knownConversation = (0, getOwn_1.getOwn)(byUsername, username);
        if (knownConversation && knownConversation.uuid) {
            (0, events_1.trigger)('showConversation', knownConversation.uuid, username);
            dispatch(showInbox());
            return;
        }
        dispatch({
            type: 'SET_IS_FETCHING_USERNAME',
            payload: {
                isFetchingUsername: true,
            },
        });
        try {
            const foundUsername = await checkForUsername(username);
            dispatch({
                type: 'SET_IS_FETCHING_USERNAME',
                payload: {
                    isFetchingUsername: false,
                },
            });
            if (!foundUsername) {
                dispatch(globalModals_1.actions.showUsernameNotFoundModal(username));
                return;
            }
            (0, events_1.trigger)('showConversation', foundUsername.uuid, undefined, foundUsername.username);
            dispatch(showInbox());
        }
        catch (error) {
            log.error('startNewConversationFromUsername: Something went wrong fetching username:', error.stack);
            dispatch({
                type: 'SET_IS_FETCHING_USERNAME',
                payload: {
                    isFetchingUsername: false,
                },
            });
            (0, showToast_1.showToast)(ToastFailedToFetchUsername_1.ToastFailedToFetchUsername);
        }
    };
}
function startSettingGroupMetadata() {
    return { type: 'START_SETTING_GROUP_METADATA' };
}
function toggleConversationInChooseMembers(conversationId) {
    return dispatch => {
        const maxRecommendedGroupSize = (0, limits_1.getGroupSizeRecommendedLimit)(151);
        const maxGroupSize = Math.max((0, limits_1.getGroupSizeHardLimit)(1001), maxRecommendedGroupSize + 1);
        (0, assert_1.assert)(maxGroupSize > maxRecommendedGroupSize, 'Expected the hard max group size to be larger than the recommended maximum');
        dispatch({
            type: 'TOGGLE_CONVERSATION_IN_CHOOSE_MEMBERS',
            payload: { conversationId, maxGroupSize, maxRecommendedGroupSize },
        });
    };
}
// Note: we need two actions here to simplify. Operations outside of the left pane can
//   trigger an 'openConversation' so we go through Whisper.events for all
//   conversation selection. Internal just triggers the Whisper.event, and External
//   makes the changes to the store.
function openConversationInternal({ conversationId, messageId, switchToAssociatedView, }) {
    return dispatch => {
        (0, events_1.trigger)('showConversation', conversationId, messageId);
        if (switchToAssociatedView) {
            dispatch({
                type: 'SWITCH_TO_ASSOCIATED_VIEW',
                payload: { conversationId },
            });
        }
    };
}
function openConversationExternal(id, messageId) {
    return {
        type: 'SELECTED_CONVERSATION_CHANGED',
        payload: {
            id,
            messageId,
        },
    };
}
function removeMemberFromGroup(conversationId, contactId) {
    return dispatch => {
        const conversationModel = window.ConversationController.get(conversationId);
        if (conversationModel) {
            const idForLogging = conversationModel.idForLogging();
            (0, longRunningTaskWrapper_1.longRunningTaskWrapper)({
                name: 'removeMemberFromGroup',
                idForLogging,
                task: () => conversationModel.removeFromGroupV2(contactId),
            });
        }
        dispatch({
            type: 'NOOP',
            payload: null,
        });
    };
}
function toggleAdmin(conversationId, contactId) {
    return dispatch => {
        const conversationModel = window.ConversationController.get(conversationId);
        if (conversationModel) {
            conversationModel.toggleAdmin(contactId);
        }
        dispatch({
            type: 'NOOP',
            payload: null,
        });
    };
}
function updateConversationModelSharedGroups(conversationId) {
    return dispatch => {
        const conversation = window.ConversationController.get(conversationId);
        if (conversation && conversation.throttledUpdateSharedGroups) {
            conversation.throttledUpdateSharedGroups();
        }
        dispatch({
            type: 'NOOP',
            payload: null,
        });
    };
}
function showInbox() {
    return {
        type: 'SHOW_INBOX',
        payload: null,
    };
}
function showArchivedConversations() {
    return {
        type: 'SHOW_ARCHIVED_CONVERSATIONS',
        payload: null,
    };
}
function doubleCheckMissingQuoteReference(messageId) {
    const message = window.MessageController.getById(messageId);
    if (message) {
        message.doubleCheckMissingQuoteReference();
    }
    return {
        type: 'NOOP',
        payload: null,
    };
}
// Reducer
function getEmptyState() {
    return {
        conversationLookup: {},
        conversationsByE164: {},
        conversationsByUuid: {},
        conversationsByGroupId: {},
        conversationsByUsername: {},
        outboundMessagesPendingConversationVerification: {},
        messagesByConversation: {},
        messagesLookup: {},
        selectedMessageCounter: 0,
        showArchived: false,
        selectedConversationTitle: '',
        selectedConversationPanelDepth: 0,
        usernameSaveState: conversationsEnums_1.UsernameSaveState.None,
    };
}
exports.getEmptyState = getEmptyState;
function hasMessageHeightChanged(message, previous) {
    const messageAttachments = message.attachments || [];
    const previousAttachments = previous.attachments || [];
    const errorStatusChanged = (!message.errors && previous.errors) ||
        (message.errors && !previous.errors) ||
        (message.errors &&
            previous.errors &&
            message.errors.length !== previous.errors.length);
    if (errorStatusChanged) {
        return true;
    }
    const groupUpdateChanged = message.group_update !== previous.group_update;
    if (groupUpdateChanged) {
        return true;
    }
    const stickerPendingChanged = message.sticker &&
        message.sticker.data &&
        previous.sticker &&
        previous.sticker.data &&
        !previous.sticker.data.blurHash &&
        previous.sticker.data.pending !== message.sticker.data.pending;
    if (stickerPendingChanged) {
        return true;
    }
    const longMessageAttachmentLoaded = previous.bodyPending && !message.bodyPending;
    if (longMessageAttachmentLoaded) {
        return true;
    }
    const firstAttachmentNoLongerPending = previousAttachments[0] &&
        previousAttachments[0].pending &&
        messageAttachments[0] &&
        !messageAttachments[0].pending;
    if (firstAttachmentNoLongerPending) {
        return true;
    }
    const currentReactions = message.reactions || [];
    const lastReactions = previous.reactions || [];
    const reactionsChanged = (currentReactions.length === 0) !== (lastReactions.length === 0);
    if (reactionsChanged) {
        return true;
    }
    const isDeletedForEveryone = message.deletedForEveryone;
    const wasDeletedForEveryone = previous.deletedForEveryone;
    if (isDeletedForEveryone !== wasDeletedForEveryone) {
        return true;
    }
    return false;
}
function updateConversationLookups(added, removed, state) {
    const result = {
        conversationsByE164: state.conversationsByE164,
        conversationsByUuid: state.conversationsByUuid,
        conversationsByGroupId: state.conversationsByGroupId,
        conversationsByUsername: state.conversationsByUsername,
    };
    if (removed && removed.e164) {
        result.conversationsByE164 = (0, lodash_1.omit)(result.conversationsByE164, removed.e164);
    }
    if (removed && removed.uuid) {
        result.conversationsByUuid = (0, lodash_1.omit)(result.conversationsByUuid, removed.uuid);
    }
    if (removed && removed.groupId) {
        result.conversationsByGroupId = (0, lodash_1.omit)(result.conversationsByGroupId, removed.groupId);
    }
    if (removed && removed.username) {
        result.conversationsByUsername = (0, lodash_1.omit)(result.conversationsByUsername, removed.username);
    }
    if (added && added.e164) {
        result.conversationsByE164 = Object.assign(Object.assign({}, result.conversationsByE164), { [added.e164]: added });
    }
    if (added && added.uuid) {
        result.conversationsByUuid = Object.assign(Object.assign({}, result.conversationsByUuid), { [added.uuid]: added });
    }
    if (added && added.groupId) {
        result.conversationsByGroupId = Object.assign(Object.assign({}, result.conversationsByGroupId), { [added.groupId]: added });
    }
    if (added && added.username) {
        result.conversationsByUsername = Object.assign(Object.assign({}, result.conversationsByUsername), { [added.username]: added });
    }
    return result;
}
exports.updateConversationLookups = updateConversationLookups;
function closeComposerModal(state, modalToClose) {
    const { composer } = state;
    if ((composer === null || composer === void 0 ? void 0 : composer.step) !== conversationsEnums_1.ComposerStep.ChooseGroupMembers) {
        (0, assert_1.assert)(false, "Can't close the modal in this composer step. Doing nothing");
        return state;
    }
    if (composer[modalToClose] !== conversationsEnums_1.OneTimeModalState.Showing) {
        return state;
    }
    return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { [modalToClose]: conversationsEnums_1.OneTimeModalState.Shown }) });
}
function reducer(state = getEmptyState(), action) {
    var _a, _b, _c;
    if (action.type === CANCEL_MESSAGES_PENDING_CONVERSATION_VERIFICATION) {
        return Object.assign(Object.assign({}, state), { outboundMessagesPendingConversationVerification: {} });
    }
    if (action.type === 'CANT_ADD_CONTACT_TO_GROUP') {
        const { composer } = state;
        if ((composer === null || composer === void 0 ? void 0 : composer.step) !== conversationsEnums_1.ComposerStep.ChooseGroupMembers) {
            (0, assert_1.assert)(false, "Can't update modal in this composer step. Doing nothing");
            return state;
        }
        return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { cantAddContactIdForModal: action.payload.conversationId }) });
    }
    if (action.type === 'CLEAR_INVITED_UUIDS_FOR_NEWLY_CREATED_GROUP') {
        return (0, lodash_1.omit)(state, 'invitedUuidsForNewlyCreatedGroup');
    }
    if (action.type === 'CLEAR_GROUP_CREATION_ERROR') {
        const { composer } = state;
        if ((composer === null || composer === void 0 ? void 0 : composer.step) !== conversationsEnums_1.ComposerStep.SetGroupMetadata) {
            (0, assert_1.assert)(false, "Can't clear group creation error in this composer state. Doing nothing");
            return state;
        }
        return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { hasError: false }) });
    }
    if (action.type === 'CLOSE_CANT_ADD_CONTACT_TO_GROUP_MODAL') {
        const { composer } = state;
        if ((composer === null || composer === void 0 ? void 0 : composer.step) !== conversationsEnums_1.ComposerStep.ChooseGroupMembers) {
            (0, assert_1.assert)(false, "Can't close the modal in this composer step. Doing nothing");
            return state;
        }
        return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { cantAddContactIdForModal: undefined }) });
    }
    if (action.type === 'CLOSE_CONTACT_SPOOFING_REVIEW') {
        return (0, lodash_1.omit)(state, 'contactSpoofingReview');
    }
    if (action.type === 'CLOSE_MAXIMUM_GROUP_SIZE_MODAL') {
        return closeComposerModal(state, 'maximumGroupSizeModalState');
    }
    if (action.type === 'CLOSE_RECOMMENDED_GROUP_SIZE_MODAL') {
        return closeComposerModal(state, 'recommendedGroupSizeModalState');
    }
    if (action.type === 'SET_PRE_JOIN_CONVERSATION') {
        const { payload } = action;
        const { data } = payload;
        return Object.assign(Object.assign({}, state), { preJoinConversation: data });
    }
    if (action.type === 'CONVERSATION_ADDED') {
        const { payload } = action;
        const { id, data } = payload;
        const { conversationLookup } = state;
        return Object.assign(Object.assign(Object.assign(Object.assign({}, state), { conversationLookup: Object.assign(Object.assign({}, conversationLookup), { [id]: data }) }), updateConversationLookups(data, undefined, state)), { outboundMessagesPendingConversationVerification: data.isUntrusted
                ? state.outboundMessagesPendingConversationVerification
                : (0, lodash_1.omit)(state.outboundMessagesPendingConversationVerification, id) });
    }
    if (action.type === 'CONVERSATION_CHANGED') {
        const { payload } = action;
        const { id, data } = payload;
        const { conversationLookup } = state;
        const { selectedConversationId } = state;
        let { showArchived } = state;
        const existing = conversationLookup[id];
        // We only modify the lookup if we already had that conversation and the conversation
        //   changed.
        if (!existing || data === existing) {
            return state;
        }
        const keysToOmit = [];
        if (selectedConversationId === id) {
            // Archived -> Inbox: we go back to the normal inbox view
            if (existing.isArchived && !data.isArchived) {
                showArchived = false;
            }
            // Inbox -> Archived: no conversation is selected
            // Note: With today's stacked converastions architecture, this can result in weird
            //   behavior - no selected conversation in the left pane, but a conversation show
            //   in the right pane.
            if (!existing.isArchived && data.isArchived) {
                keysToOmit.push('selectedConversationId');
            }
            if (!existing.isBlocked && data.isBlocked) {
                keysToOmit.push('contactSpoofingReview');
            }
        }
        return Object.assign(Object.assign(Object.assign(Object.assign({}, (0, lodash_1.omit)(state, keysToOmit)), { selectedConversationId,
            showArchived, conversationLookup: Object.assign(Object.assign({}, conversationLookup), { [id]: data }) }), updateConversationLookups(data, existing, state)), { outboundMessagesPendingConversationVerification: data.isUntrusted
                ? state.outboundMessagesPendingConversationVerification
                : (0, lodash_1.omit)(state.outboundMessagesPendingConversationVerification, id) });
    }
    if (action.type === 'CONVERSATION_REMOVED') {
        const { payload } = action;
        const { id } = payload;
        const { conversationLookup } = state;
        const existing = (0, getOwn_1.getOwn)(conversationLookup, id);
        // No need to make a change if we didn't have a record of this conversation!
        if (!existing) {
            return state;
        }
        return Object.assign(Object.assign(Object.assign({}, state), { conversationLookup: (0, lodash_1.omit)(conversationLookup, [id]) }), updateConversationLookups(undefined, existing, state));
    }
    if (action.type === 'CONVERSATION_UNLOADED') {
        const { payload } = action;
        const { id } = payload;
        const existingConversation = state.messagesByConversation[id];
        if (!existingConversation) {
            return state;
        }
        const { messageIds } = existingConversation;
        const selectedConversationId = state.selectedConversationId !== id
            ? state.selectedConversationId
            : undefined;
        return Object.assign(Object.assign({}, (0, lodash_1.omit)(state, 'contactSpoofingReview')), { selectedConversationId, selectedConversationPanelDepth: 0, messagesLookup: (0, lodash_1.omit)(state.messagesLookup, messageIds), messagesByConversation: (0, lodash_1.omit)(state.messagesByConversation, [id]) });
    }
    if (action.type === 'CONVERSATIONS_REMOVE_ALL') {
        return getEmptyState();
    }
    if (action.type === 'CREATE_GROUP_PENDING') {
        const { composer } = state;
        if ((composer === null || composer === void 0 ? void 0 : composer.step) !== conversationsEnums_1.ComposerStep.SetGroupMetadata) {
            // This should be unlikely, but it can happen if someone closes the composer while
            //   a group is being created.
            return state;
        }
        return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { hasError: false, isCreating: true }) });
    }
    if (action.type === 'CREATE_GROUP_FULFILLED') {
        // We don't do much here and instead rely on `openConversationInternal` to do most of
        //   the work.
        return Object.assign(Object.assign({}, state), { invitedUuidsForNewlyCreatedGroup: action.payload.invitedUuids });
    }
    if (action.type === 'CREATE_GROUP_REJECTED') {
        const { composer } = state;
        if ((composer === null || composer === void 0 ? void 0 : composer.step) !== conversationsEnums_1.ComposerStep.SetGroupMetadata) {
            // This should be unlikely, but it can happen if someone closes the composer while
            //   a group is being created.
            return state;
        }
        return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { hasError: true, isCreating: false }) });
    }
    if (action.type === 'SET_SELECTED_CONVERSATION_PANEL_DEPTH') {
        return Object.assign(Object.assign({}, state), { selectedConversationPanelDepth: action.payload.panelDepth });
    }
    if (action.type === 'MESSAGE_SELECTED') {
        const { messageId, conversationId } = action.payload;
        if (state.selectedConversationId !== conversationId) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { selectedMessage: messageId, selectedMessageCounter: state.selectedMessageCounter + 1 });
    }
    if (action.type === MESSAGE_STOPPED_BY_MISSING_VERIFICATION) {
        const { messageId, untrustedConversationIds } = action.payload;
        const newOutboundMessagesPendingConversationVerification = Object.assign({}, state.outboundMessagesPendingConversationVerification);
        untrustedConversationIds.forEach(conversationId => {
            var _a;
            const existingPendingMessageIds = (_a = (0, getOwn_1.getOwn)(newOutboundMessagesPendingConversationVerification, conversationId)) !== null && _a !== void 0 ? _a : [];
            if (!existingPendingMessageIds.includes(messageId)) {
                newOutboundMessagesPendingConversationVerification[conversationId] = [
                    ...existingPendingMessageIds,
                    messageId,
                ];
            }
        });
        return Object.assign(Object.assign({}, state), { outboundMessagesPendingConversationVerification: newOutboundMessagesPendingConversationVerification });
    }
    if (action.type === 'MESSAGE_CHANGED') {
        const { id, conversationId, data } = action.payload;
        const existingConversation = state.messagesByConversation[conversationId];
        // We don't keep track of messages unless their conversation is loaded...
        if (!existingConversation) {
            return state;
        }
        // ...and we've already loaded that message once
        const existingMessage = (0, getOwn_1.getOwn)(state.messagesLookup, id);
        if (!existingMessage) {
            return state;
        }
        // Check for changes which could affect height - that's why we need this
        //   heightChangeMessageIds field. It tells Timeline to recalculate all of its heights
        const hasHeightChanged = hasMessageHeightChanged(data, existingMessage);
        const { heightChangeMessageIds } = existingConversation;
        const updatedChanges = hasHeightChanged
            ? (0, lodash_1.uniq)([...heightChangeMessageIds, id])
            : heightChangeMessageIds;
        return Object.assign(Object.assign({}, state), { messagesLookup: Object.assign(Object.assign({}, state.messagesLookup), { [id]: Object.assign(Object.assign({}, data), { displayLimit: existingMessage.displayLimit }) }), messagesByConversation: Object.assign(Object.assign({}, state.messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { heightChangeMessageIds: updatedChanges }) }) });
    }
    if (action.type === 'MESSAGE_EXPANDED') {
        const { id, displayLimit } = action.payload;
        const existingMessage = state.messagesLookup[id];
        if (!existingMessage) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { messagesLookup: Object.assign(Object.assign({}, state.messagesLookup), { [id]: Object.assign(Object.assign({}, existingMessage), { displayLimit }) }) });
    }
    if (action.type === 'MESSAGE_SIZE_CHANGED') {
        const { id, conversationId } = action.payload;
        const existingConversation = (0, getOwn_1.getOwn)(state.messagesByConversation, conversationId);
        if (!existingConversation) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { messagesByConversation: Object.assign(Object.assign({}, state.messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { heightChangeMessageIds: (0, lodash_1.uniq)([
                        ...existingConversation.heightChangeMessageIds,
                        id,
                    ]) }) }) });
    }
    if (action.type === 'MESSAGES_RESET') {
        const { conversationId, messages, metrics, scrollToMessageId, unboundedFetch, } = action.payload;
        const { messagesByConversation, messagesLookup } = state;
        const existingConversation = messagesByConversation[conversationId];
        const resetCounter = existingConversation
            ? existingConversation.resetCounter + 1
            : 0;
        const lookup = (0, lodash_1.fromPairs)(messages.map(message => [message.id, message]));
        const sorted = (0, lodash_1.orderBy)((0, lodash_1.values)(lookup), ['received_at', 'sent_at'], ['ASC', 'ASC']);
        let { newest, oldest } = metrics;
        // If our metrics are a little out of date, we'll fix them up
        if (sorted.length > 0) {
            const first = sorted[0];
            if (first && (!oldest || first.received_at <= oldest.received_at)) {
                oldest = (0, lodash_1.pick)(first, ['id', 'received_at', 'sent_at']);
            }
            const last = sorted[sorted.length - 1];
            if (last &&
                (!newest || unboundedFetch || last.received_at >= newest.received_at)) {
                newest = (0, lodash_1.pick)(last, ['id', 'received_at', 'sent_at']);
            }
        }
        const messageIds = sorted.map(message => message.id);
        return Object.assign(Object.assign({}, state), { selectedMessage: scrollToMessageId, selectedMessageCounter: state.selectedMessageCounter + 1, messagesLookup: Object.assign(Object.assign({}, messagesLookup), lookup), messagesByConversation: Object.assign(Object.assign({}, messagesByConversation), { [conversationId]: {
                    isLoadingMessages: false,
                    scrollToMessageId,
                    scrollToMessageCounter: existingConversation
                        ? existingConversation.scrollToMessageCounter + 1
                        : 0,
                    scrollToBottomCounter: existingConversation
                        ? existingConversation.scrollToBottomCounter
                        : 0,
                    messageIds,
                    metrics: Object.assign(Object.assign({}, metrics), { newest,
                        oldest }),
                    resetCounter,
                    heightChangeMessageIds: [],
                } }) });
    }
    if (action.type === 'SET_MESSAGES_LOADING') {
        const { payload } = action;
        const { conversationId, isLoadingMessages } = payload;
        const { messagesByConversation } = state;
        const existingConversation = messagesByConversation[conversationId];
        if (!existingConversation) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { messagesByConversation: Object.assign(Object.assign({}, messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { loadCountdownStart: undefined, isLoadingMessages }) }) });
    }
    if (action.type === 'SET_LOAD_COUNTDOWN_START') {
        const { payload } = action;
        const { conversationId, loadCountdownStart } = payload;
        const { messagesByConversation } = state;
        const existingConversation = messagesByConversation[conversationId];
        if (!existingConversation) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { messagesByConversation: Object.assign(Object.assign({}, messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { loadCountdownStart }) }) });
    }
    if (action.type === 'SET_NEAR_BOTTOM') {
        const { payload } = action;
        const { conversationId, isNearBottom } = payload;
        const { messagesByConversation } = state;
        const existingConversation = messagesByConversation[conversationId];
        if (!existingConversation) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { messagesByConversation: Object.assign(Object.assign({}, messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { isNearBottom }) }) });
    }
    if (action.type === 'SCROLL_TO_BOTTOM') {
        const { payload } = action;
        const { conversationId } = payload;
        const { messagesByConversation } = state;
        const existingConversation = messagesByConversation[conversationId];
        if (!existingConversation) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { messagesByConversation: Object.assign(Object.assign({}, messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { scrollToBottomCounter: existingConversation.scrollToBottomCounter + 1 }) }) });
    }
    if (action.type === 'SCROLL_TO_MESSAGE') {
        const { payload } = action;
        const { conversationId, messageId } = payload;
        const { messagesByConversation, messagesLookup } = state;
        const existingConversation = messagesByConversation[conversationId];
        if (!existingConversation) {
            return state;
        }
        if (!messagesLookup[messageId]) {
            return state;
        }
        if (!existingConversation.messageIds.includes(messageId)) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { selectedMessage: messageId, selectedMessageCounter: state.selectedMessageCounter + 1, messagesByConversation: Object.assign(Object.assign({}, messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { isLoadingMessages: false, scrollToMessageId: messageId, scrollToMessageCounter: existingConversation.scrollToMessageCounter + 1 }) }) });
    }
    if (action.type === 'MESSAGE_DELETED') {
        const { id, conversationId } = action.payload;
        const { messagesByConversation, messagesLookup } = state;
        const existingConversation = messagesByConversation[conversationId];
        if (!existingConversation) {
            return state;
        }
        // Assuming that we always have contiguous groups of messages in memory, the removal
        //   of one message at one end of our message set be replaced with the message right
        //   next to it.
        const oldIds = existingConversation.messageIds;
        let { newest, oldest } = existingConversation.metrics;
        if (oldIds.length > 1) {
            const firstId = oldIds[0];
            const lastId = oldIds[oldIds.length - 1];
            if (oldest && oldest.id === firstId && firstId === id) {
                const second = messagesLookup[oldIds[1]];
                oldest = second
                    ? (0, lodash_1.pick)(second, ['id', 'received_at', 'sent_at'])
                    : undefined;
            }
            if (newest && newest.id === lastId && lastId === id) {
                const penultimate = messagesLookup[oldIds[oldIds.length - 2]];
                newest = penultimate
                    ? (0, lodash_1.pick)(penultimate, ['id', 'received_at', 'sent_at'])
                    : undefined;
            }
        }
        // Removing it from our caches
        const messageIds = (0, lodash_1.without)(existingConversation.messageIds, id);
        const heightChangeMessageIds = (0, lodash_1.without)(existingConversation.heightChangeMessageIds, id);
        let metrics;
        if (messageIds.length === 0) {
            metrics = {
                totalUnread: 0,
            };
        }
        else {
            metrics = Object.assign(Object.assign({}, existingConversation.metrics), { oldest,
                newest });
        }
        return Object.assign(Object.assign({}, state), { messagesLookup: (0, lodash_1.omit)(messagesLookup, id), messagesByConversation: {
                [conversationId]: Object.assign(Object.assign({}, existingConversation), { messageIds,
                    heightChangeMessageIds,
                    metrics }),
            } });
    }
    if (action.type === 'REPAIR_NEWEST_MESSAGE') {
        const { conversationId } = action.payload;
        const { messagesByConversation, messagesLookup } = state;
        const existingConversation = (0, getOwn_1.getOwn)(messagesByConversation, conversationId);
        if (!existingConversation) {
            return state;
        }
        const { messageIds } = existingConversation;
        const lastId = messageIds && messageIds.length
            ? messageIds[messageIds.length - 1]
            : undefined;
        const last = lastId ? (0, getOwn_1.getOwn)(messagesLookup, lastId) : undefined;
        const newest = last
            ? (0, lodash_1.pick)(last, ['id', 'received_at', 'sent_at'])
            : undefined;
        return Object.assign(Object.assign({}, state), { messagesByConversation: Object.assign(Object.assign({}, messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { metrics: Object.assign(Object.assign({}, existingConversation.metrics), { newest }) }) }) });
    }
    if (action.type === 'REPAIR_OLDEST_MESSAGE') {
        const { conversationId } = action.payload;
        const { messagesByConversation, messagesLookup } = state;
        const existingConversation = (0, getOwn_1.getOwn)(messagesByConversation, conversationId);
        if (!existingConversation) {
            return state;
        }
        const { messageIds } = existingConversation;
        const firstId = messageIds && messageIds.length ? messageIds[0] : undefined;
        const first = firstId ? (0, getOwn_1.getOwn)(messagesLookup, firstId) : undefined;
        const oldest = first
            ? (0, lodash_1.pick)(first, ['id', 'received_at', 'sent_at'])
            : undefined;
        return Object.assign(Object.assign({}, state), { messagesByConversation: Object.assign(Object.assign({}, messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { metrics: Object.assign(Object.assign({}, existingConversation.metrics), { oldest }) }) }) });
    }
    if (action.type === 'REVIEW_GROUP_MEMBER_NAME_COLLISION') {
        return Object.assign(Object.assign({}, state), { contactSpoofingReview: Object.assign({ type: contactSpoofing_1.ContactSpoofingType.MultipleGroupMembersWithSameTitle }, action.payload) });
    }
    if (action.type === 'REVIEW_MESSAGE_REQUEST_NAME_COLLISION') {
        return Object.assign(Object.assign({}, state), { contactSpoofingReview: Object.assign({ type: contactSpoofing_1.ContactSpoofingType.DirectConversationWithSameTitle }, action.payload) });
    }
    if (action.type === 'MESSAGES_ADDED') {
        const { conversationId, isActive, isNewMessage, messages } = action.payload;
        const { messagesByConversation, messagesLookup } = state;
        const existingConversation = messagesByConversation[conversationId];
        if (!existingConversation) {
            return state;
        }
        let { newest, oldest, oldestUnread, totalUnread } = existingConversation.metrics;
        if (messages.length < 1) {
            return state;
        }
        const lookup = (0, lodash_1.fromPairs)(existingConversation.messageIds.map(id => [id, messagesLookup[id]]));
        messages.forEach(message => {
            lookup[message.id] = message;
        });
        const sorted = (0, lodash_1.orderBy)((0, lodash_1.values)(lookup), ['received_at', 'sent_at'], ['ASC', 'ASC']);
        const messageIds = sorted.map(message => message.id);
        const first = sorted[0];
        const last = sorted[sorted.length - 1];
        if (!newest) {
            newest = (0, lodash_1.pick)(first, ['id', 'received_at', 'sent_at']);
        }
        if (!oldest) {
            oldest = (0, lodash_1.pick)(last, ['id', 'received_at', 'sent_at']);
        }
        const existingTotal = existingConversation.messageIds.length;
        if (isNewMessage && existingTotal > 0) {
            const lastMessageId = existingConversation.messageIds[existingTotal - 1];
            // If our messages in memory don't include the most recent messages, then we
            //   won't add new messages to our message list.
            const haveLatest = newest && newest.id === lastMessageId;
            if (!haveLatest) {
                return state;
            }
        }
        // Update oldest and newest if we receive older/newer
        // messages (or duplicated timestamps!)
        if (first && oldest && first.received_at <= oldest.received_at) {
            oldest = (0, lodash_1.pick)(first, ['id', 'received_at', 'sent_at']);
        }
        if (last && newest && last.received_at >= newest.received_at) {
            newest = (0, lodash_1.pick)(last, ['id', 'received_at', 'sent_at']);
        }
        const newIds = messages.map(message => message.id);
        const newMessageIds = (0, lodash_1.difference)(newIds, existingConversation.messageIds);
        const { isNearBottom } = existingConversation;
        if ((!isNearBottom || !isActive) && !oldestUnread) {
            const oldestId = newMessageIds.find(messageId => {
                const message = lookup[messageId];
                return message && (0, isMessageUnread_1.isMessageUnread)(message);
            });
            if (oldestId) {
                oldestUnread = (0, lodash_1.pick)(lookup[oldestId], [
                    'id',
                    'received_at',
                    'sent_at',
                ]);
            }
        }
        if (oldestUnread) {
            const newUnread = newMessageIds.reduce((sum, messageId) => {
                const message = lookup[messageId];
                return sum + (message && (0, isMessageUnread_1.isMessageUnread)(message) ? 1 : 0);
            }, 0);
            totalUnread = (totalUnread || 0) + newUnread;
        }
        const changedIds = (0, lodash_1.intersection)(newIds, existingConversation.messageIds);
        const heightChangeMessageIds = (0, lodash_1.uniq)([
            ...changedIds,
            ...existingConversation.heightChangeMessageIds,
        ]);
        return Object.assign(Object.assign({}, state), { messagesLookup: Object.assign(Object.assign({}, messagesLookup), lookup), messagesByConversation: Object.assign(Object.assign({}, messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { isLoadingMessages: false, messageIds,
                    heightChangeMessageIds, scrollToMessageId: undefined, metrics: Object.assign(Object.assign({}, existingConversation.metrics), { newest,
                        oldest,
                        totalUnread,
                        oldestUnread }) }) }) });
    }
    if (action.type === 'CLEAR_SELECTED_MESSAGE') {
        return Object.assign(Object.assign({}, state), { selectedMessage: undefined });
    }
    if (action.type === 'CLEAR_CHANGED_MESSAGES') {
        const { payload } = action;
        const { conversationId } = payload;
        const existingConversation = state.messagesByConversation[conversationId];
        if (!existingConversation) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { messagesByConversation: Object.assign(Object.assign({}, state.messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { heightChangeMessageIds: [] }) }) });
    }
    if (action.type === 'CLEAR_UNREAD_METRICS') {
        const { payload } = action;
        const { conversationId } = payload;
        const existingConversation = state.messagesByConversation[conversationId];
        if (!existingConversation) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { messagesByConversation: Object.assign(Object.assign({}, state.messagesByConversation), { [conversationId]: Object.assign(Object.assign({}, existingConversation), { metrics: Object.assign(Object.assign({}, existingConversation.metrics), { oldestUnread: undefined, totalUnread: 0 }) }) }) });
    }
    if (action.type === 'SELECTED_CONVERSATION_CHANGED') {
        const { payload } = action;
        const { id } = payload;
        return Object.assign(Object.assign({}, (0, lodash_1.omit)(state, 'contactSpoofingReview')), { selectedConversationId: id });
    }
    if (action.type === 'SHOW_INBOX') {
        return Object.assign(Object.assign({}, (0, lodash_1.omit)(state, 'composer')), { showArchived: false });
    }
    if (action.type === 'SHOW_ARCHIVED_CONVERSATIONS') {
        return Object.assign(Object.assign({}, (0, lodash_1.omit)(state, 'composer')), { showArchived: true });
    }
    if (action.type === 'SET_CONVERSATION_HEADER_TITLE') {
        return Object.assign(Object.assign({}, state), { selectedConversationTitle: action.payload.title });
    }
    if (action.type === 'SET_RECENT_MEDIA_ITEMS') {
        const { id, recentMediaItems } = action.payload;
        const { conversationLookup } = state;
        const conversationData = conversationLookup[id];
        if (!conversationData) {
            return state;
        }
        const data = Object.assign(Object.assign({}, conversationData), { recentMediaItems });
        return Object.assign(Object.assign(Object.assign({}, state), { conversationLookup: Object.assign(Object.assign({}, conversationLookup), { [id]: data }) }), updateConversationLookups(data, undefined, state));
    }
    if (action.type === 'START_COMPOSING') {
        if (((_a = state.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.StartDirectConversation) {
            return state;
        }
        return Object.assign(Object.assign({}, state), { showArchived: false, composer: {
                step: conversationsEnums_1.ComposerStep.StartDirectConversation,
                searchTerm: '',
                isFetchingUsername: false,
            } });
    }
    if (action.type === 'SHOW_CHOOSE_GROUP_MEMBERS') {
        let selectedConversationIds;
        let recommendedGroupSizeModalState;
        let maximumGroupSizeModalState;
        let groupName;
        let groupAvatar;
        let groupExpireTimer;
        let userAvatarData = (0, Avatar_1.getDefaultAvatars)(true);
        switch ((_b = state.composer) === null || _b === void 0 ? void 0 : _b.step) {
            case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
                return state;
            case conversationsEnums_1.ComposerStep.SetGroupMetadata:
                ({
                    selectedConversationIds,
                    recommendedGroupSizeModalState,
                    maximumGroupSizeModalState,
                    groupName,
                    groupAvatar,
                    groupExpireTimer,
                    userAvatarData,
                } = state.composer);
                break;
            default:
                selectedConversationIds = [];
                recommendedGroupSizeModalState = conversationsEnums_1.OneTimeModalState.NeverShown;
                maximumGroupSizeModalState = conversationsEnums_1.OneTimeModalState.NeverShown;
                groupName = '';
                groupExpireTimer = universalExpireTimer.get();
                break;
        }
        return Object.assign(Object.assign({}, state), { showArchived: false, composer: {
                step: conversationsEnums_1.ComposerStep.ChooseGroupMembers,
                searchTerm: '',
                selectedConversationIds,
                cantAddContactIdForModal: undefined,
                recommendedGroupSizeModalState,
                maximumGroupSizeModalState,
                groupName,
                groupAvatar,
                groupExpireTimer,
                userAvatarData,
            } });
    }
    if (action.type === 'START_SETTING_GROUP_METADATA') {
        const { composer } = state;
        switch (composer === null || composer === void 0 ? void 0 : composer.step) {
            case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
                return Object.assign(Object.assign({}, state), { showArchived: false, composer: Object.assign({ step: conversationsEnums_1.ComposerStep.SetGroupMetadata, isEditingAvatar: false, isCreating: false, hasError: false }, (0, lodash_1.pick)(composer, [
                        'groupAvatar',
                        'groupName',
                        'groupExpireTimer',
                        'maximumGroupSizeModalState',
                        'recommendedGroupSizeModalState',
                        'selectedConversationIds',
                        'userAvatarData',
                    ])) });
            case conversationsEnums_1.ComposerStep.SetGroupMetadata:
                return state;
            default:
                (0, assert_1.assert)(false, 'Cannot transition to setting group metadata from this state');
                return state;
        }
    }
    if (action.type === 'SET_COMPOSE_GROUP_AVATAR') {
        const { composer } = state;
        switch (composer === null || composer === void 0 ? void 0 : composer.step) {
            case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
            case conversationsEnums_1.ComposerStep.SetGroupMetadata:
                return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { groupAvatar: action.payload.groupAvatar }) });
            default:
                (0, assert_1.assert)(false, 'Setting compose group avatar at this step is a no-op');
                return state;
        }
    }
    if (action.type === 'SET_COMPOSE_GROUP_NAME') {
        const { composer } = state;
        switch (composer === null || composer === void 0 ? void 0 : composer.step) {
            case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
            case conversationsEnums_1.ComposerStep.SetGroupMetadata:
                return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { groupName: action.payload.groupName }) });
            default:
                (0, assert_1.assert)(false, 'Setting compose group name at this step is a no-op');
                return state;
        }
    }
    if (action.type === 'SET_COMPOSE_GROUP_EXPIRE_TIMER') {
        const { composer } = state;
        switch (composer === null || composer === void 0 ? void 0 : composer.step) {
            case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
            case conversationsEnums_1.ComposerStep.SetGroupMetadata:
                return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { groupExpireTimer: action.payload.groupExpireTimer }) });
            default:
                (0, assert_1.assert)(false, 'Setting compose group name at this step is a no-op');
                return state;
        }
    }
    if (action.type === 'SET_COMPOSE_SEARCH_TERM') {
        const { composer } = state;
        if (!composer) {
            (0, assert_1.assert)(false, 'Setting compose search term with the composer closed is a no-op');
            return state;
        }
        if (composer.step !== conversationsEnums_1.ComposerStep.StartDirectConversation &&
            composer.step !== conversationsEnums_1.ComposerStep.ChooseGroupMembers) {
            (0, assert_1.assert)(false, `Setting compose search term at step ${composer.step} is a no-op`);
            return state;
        }
        return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { searchTerm: action.payload.searchTerm }) });
    }
    if (action.type === 'SET_IS_FETCHING_USERNAME') {
        const { composer } = state;
        if (!composer) {
            (0, assert_1.assert)(false, 'Setting compose username with the composer closed is a no-op');
            return state;
        }
        if (composer.step !== conversationsEnums_1.ComposerStep.StartDirectConversation) {
            (0, assert_1.assert)(false, 'Setting compose username at this step is a no-op');
            return state;
        }
        const { isFetchingUsername } = action.payload;
        return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { isFetchingUsername }) });
    }
    if (action.type === COMPOSE_TOGGLE_EDITING_AVATAR) {
        const { composer } = state;
        switch (composer === null || composer === void 0 ? void 0 : composer.step) {
            case conversationsEnums_1.ComposerStep.SetGroupMetadata:
                return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { isEditingAvatar: !composer.isEditingAvatar }) });
            default:
                (0, assert_1.assert)(false, 'Setting editing avatar at this step is a no-op');
                return state;
        }
    }
    if (action.type === COMPOSE_ADD_AVATAR) {
        const { payload } = action;
        const { composer } = state;
        switch (composer === null || composer === void 0 ? void 0 : composer.step) {
            case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
            case conversationsEnums_1.ComposerStep.SetGroupMetadata:
                return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { userAvatarData: [
                            Object.assign(Object.assign({}, payload), { id: getNextAvatarId(composer.userAvatarData) }),
                            ...composer.userAvatarData,
                        ] }) });
            default:
                (0, assert_1.assert)(false, 'Adding an avatar at this step is a no-op');
                return state;
        }
    }
    if (action.type === COMPOSE_REMOVE_AVATAR) {
        const { payload } = action;
        const { composer } = state;
        switch (composer === null || composer === void 0 ? void 0 : composer.step) {
            case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
            case conversationsEnums_1.ComposerStep.SetGroupMetadata:
                return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { userAvatarData: filterAvatarData(composer.userAvatarData, payload) }) });
            default:
                (0, assert_1.assert)(false, 'Removing an avatar at this step is a no-op');
                return state;
        }
    }
    if (action.type === COMPOSE_REPLACE_AVATAR) {
        const { curr, prev } = action.payload;
        const { composer } = state;
        switch (composer === null || composer === void 0 ? void 0 : composer.step) {
            case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
            case conversationsEnums_1.ComposerStep.SetGroupMetadata:
                return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), { userAvatarData: [
                            Object.assign(Object.assign({}, curr), { id: (_c = prev === null || prev === void 0 ? void 0 : prev.id) !== null && _c !== void 0 ? _c : getNextAvatarId(composer.userAvatarData) }),
                            ...(prev
                                ? filterAvatarData(composer.userAvatarData, prev)
                                : composer.userAvatarData),
                        ] }) });
            default:
                (0, assert_1.assert)(false, 'Replacing an avatar at this step is a no-op');
                return state;
        }
    }
    if (action.type === 'SWITCH_TO_ASSOCIATED_VIEW') {
        const conversation = (0, getOwn_1.getOwn)(state.conversationLookup, action.payload.conversationId);
        if (!conversation) {
            return state;
        }
        return Object.assign(Object.assign({}, (0, lodash_1.omit)(state, 'composer')), { showArchived: Boolean(conversation.isArchived) });
    }
    if (action.type === 'TOGGLE_CONVERSATION_IN_CHOOSE_MEMBERS') {
        const { composer } = state;
        if ((composer === null || composer === void 0 ? void 0 : composer.step) !== conversationsEnums_1.ComposerStep.ChooseGroupMembers) {
            (0, assert_1.assert)(false, 'Toggling conversation members is a no-op in this composer step');
            return state;
        }
        return Object.assign(Object.assign({}, state), { composer: Object.assign(Object.assign({}, composer), (0, toggleSelectedContactForGroupAddition_1.toggleSelectedContactForGroupAddition)(action.payload.conversationId, {
                maxGroupSize: action.payload.maxGroupSize,
                maxRecommendedGroupSize: action.payload.maxRecommendedGroupSize,
                maximumGroupSizeModalState: composer.maximumGroupSizeModalState,
                // We say you're already in the group, even though it hasn't been created yet.
                numberOfContactsAlreadyInGroup: 1,
                recommendedGroupSizeModalState: composer.recommendedGroupSizeModalState,
                selectedConversationIds: composer.selectedConversationIds,
            })) });
    }
    if (action.type === exports.COLORS_CHANGED) {
        const { conversationLookup } = state;
        const { conversationColor, customColorData } = action.payload;
        const nextState = Object.assign({}, state);
        Object.keys(conversationLookup).forEach(id => {
            const existing = conversationLookup[id];
            const added = Object.assign(Object.assign({}, existing), { conversationColor, customColor: customColorData === null || customColorData === void 0 ? void 0 : customColorData.value, customColorId: customColorData === null || customColorData === void 0 ? void 0 : customColorData.id });
            Object.assign(nextState, updateConversationLookups(added, existing, nextState), {
                conversationLookup: Object.assign(Object.assign({}, nextState.conversationLookup), { [id]: added }),
            });
        });
        return nextState;
    }
    if (action.type === exports.COLOR_SELECTED) {
        const { conversationLookup } = state;
        const { conversationId, conversationColor, customColorData } = action.payload;
        const existing = conversationLookup[conversationId];
        if (!existing) {
            return state;
        }
        const changed = Object.assign(Object.assign({}, existing), { conversationColor, customColor: customColorData === null || customColorData === void 0 ? void 0 : customColorData.value, customColorId: customColorData === null || customColorData === void 0 ? void 0 : customColorData.id });
        return Object.assign(Object.assign(Object.assign({}, state), { conversationLookup: Object.assign(Object.assign({}, conversationLookup), { [conversationId]: changed }) }), updateConversationLookups(changed, existing, state));
    }
    if (action.type === CUSTOM_COLOR_REMOVED) {
        const { conversationLookup } = state;
        const { colorId } = action.payload;
        const nextState = Object.assign({}, state);
        Object.keys(conversationLookup).forEach(id => {
            const existing = conversationLookup[id];
            if (existing.customColorId !== colorId) {
                return;
            }
            const changed = Object.assign(Object.assign({}, existing), { conversationColor: undefined, customColor: undefined, customColorId: undefined });
            Object.assign(nextState, updateConversationLookups(changed, existing, nextState), {
                conversationLookup: Object.assign(Object.assign({}, nextState.conversationLookup), { [id]: changed }),
            });
        });
        return nextState;
    }
    if (action.type === REPLACE_AVATARS) {
        const { conversationLookup } = state;
        const { conversationId, avatars } = action.payload;
        const conversation = conversationLookup[conversationId];
        if (!conversation) {
            return state;
        }
        const changed = Object.assign(Object.assign({}, conversation), { avatars });
        return Object.assign(Object.assign(Object.assign({}, state), { conversationLookup: Object.assign(Object.assign({}, conversationLookup), { [conversationId]: changed }) }), updateConversationLookups(changed, conversation, state));
    }
    if (action.type === UPDATE_USERNAME_SAVE_STATE) {
        const { newSaveState } = action.payload;
        return Object.assign(Object.assign({}, state), { usernameSaveState: newSaveState });
    }
    return state;
}
exports.reducer = reducer;
