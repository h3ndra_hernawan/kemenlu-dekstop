"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.OneTimeModalState = exports.ComposerStep = exports.UsernameSaveState = void 0;
// We prevent circular loops between ducks and selectors/components with `import type`.
//   For example, Selectors are used in action creators using thunk/getState, but those
//   Selectors need types from the ducks. Selectors shouldn't use code from ducks.
//
// But enums can be used as types but also as code. So we keep them out of the ducks.
var UsernameSaveState;
(function (UsernameSaveState) {
    UsernameSaveState["None"] = "None";
    UsernameSaveState["Saving"] = "Saving";
    UsernameSaveState["UsernameTakenError"] = "UsernameTakenError";
    UsernameSaveState["UsernameMalformedError"] = "UsernameMalformedError";
    UsernameSaveState["GeneralError"] = "GeneralError";
    UsernameSaveState["DeleteFailed"] = "DeleteFailed";
    UsernameSaveState["Success"] = "Success";
})(UsernameSaveState = exports.UsernameSaveState || (exports.UsernameSaveState = {}));
var ComposerStep;
(function (ComposerStep) {
    ComposerStep["StartDirectConversation"] = "StartDirectConversation";
    ComposerStep["ChooseGroupMembers"] = "ChooseGroupMembers";
    ComposerStep["SetGroupMetadata"] = "SetGroupMetadata";
})(ComposerStep = exports.ComposerStep || (exports.ComposerStep = {}));
var OneTimeModalState;
(function (OneTimeModalState) {
    OneTimeModalState[OneTimeModalState["NeverShown"] = 0] = "NeverShown";
    OneTimeModalState[OneTimeModalState["Showing"] = 1] = "Showing";
    OneTimeModalState[OneTimeModalState["Shown"] = 2] = "Shown";
})(OneTimeModalState = exports.OneTimeModalState || (exports.OneTimeModalState = {}));
