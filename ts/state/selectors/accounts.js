"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAccountSelector = exports.getAccounts = void 0;
const reselect_1 = require("reselect");
const getAccounts = (state) => state.accounts;
exports.getAccounts = getAccounts;
exports.getAccountSelector = (0, reselect_1.createSelector)(exports.getAccounts, (accounts) => {
    return (identifier) => {
        if (!identifier) {
            return false;
        }
        return accounts.accounts[identifier] || false;
    };
});
