"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLastChallengeError = exports.canDownload = exports.canDeleteForEveryone = exports.canReact = exports.canReply = exports.getPropsForAttachment = exports.getPropsForEmbeddedContact = exports.getMessagePropStatus = exports.isTapToView = exports.isDeliveryIssue = exports.isChatSessionRefreshed = exports.isChangeNumberNotification = exports.isUniversalTimerNotification = exports.isProfileChange = exports.getPropsForCallHistory = exports.isCallHistory = exports.isEndSession = exports.isGroupUpdate = exports.isVerifiedChange = exports.isKeyChange = exports.isExpirationTimerUpdate = exports.isMessageHistoryUnsynced = exports.isGroupV1Migration = exports.isGroupV2Change = exports.isUnsupportedMessage = exports.getPropsForBubble = exports.getBubblePropsForMessage = exports.getPropsForMessage = exports.getPropsForQuote = exports.getReactionsForMessage = exports.getPreviewsForMessage = exports.processBodyRanges = exports.getAttachmentsForMessage = exports.getConversation = exports.getContact = exports.getSourceUuid = exports.getSourceDevice = exports.getSource = exports.hasErrors = exports.isOutgoing = exports.isIncoming = void 0;
const lodash_1 = require("lodash");
const reselect_1 = require("reselect");
const filesize_1 = __importDefault(require("filesize"));
const LinkPreview_1 = require("../../types/LinkPreview");
const EmbeddedContact_1 = require("../../types/EmbeddedContact");
const Calling_1 = require("../../types/Calling");
const protobuf_1 = require("../../protobuf");
const Attachment_1 = require("../../types/Attachment");
const MessageReadStatus_1 = require("../../messages/MessageReadStatus");
const memoizeByRoot_1 = require("../../util/memoizeByRoot");
const missingCaseError_1 = require("../../util/missingCaseError");
const isNotNil_1 = require("../../util/isNotNil");
const timestamp_1 = require("../../util/timestamp");
const iterables = __importStar(require("../../util/iterables"));
const assert_1 = require("../../util/assert");
const conversations_1 = require("./conversations");
const MessageSendState_1 = require("../../messages/MessageSendState");
const log = __importStar(require("../../logging/log"));
const THREE_HOURS = 3 * 60 * 60 * 1000;
function isIncoming(message) {
    return message.type === 'incoming';
}
exports.isIncoming = isIncoming;
function isOutgoing(message) {
    return message.type === 'outgoing';
}
exports.isOutgoing = isOutgoing;
function hasErrors(message) {
    return message.errors ? message.errors.length > 0 : false;
}
exports.hasErrors = hasErrors;
function getSource(message, ourNumber) {
    if (isIncoming(message)) {
        return message.source;
    }
    if (!isOutgoing(message)) {
        log.warn('message.getSource: Called for non-incoming/non-outoing message');
    }
    return ourNumber;
}
exports.getSource = getSource;
function getSourceDevice(message, ourDeviceId) {
    const { sourceDevice } = message;
    if (isIncoming(message)) {
        return sourceDevice;
    }
    if (!isOutgoing(message)) {
        log.warn('message.getSourceDevice: Called for non-incoming/non-outoing message');
    }
    return sourceDevice || ourDeviceId;
}
exports.getSourceDevice = getSourceDevice;
function getSourceUuid(message, ourUuid) {
    if (isIncoming(message)) {
        return message.sourceUuid;
    }
    if (!isOutgoing(message)) {
        log.warn('message.getSourceUuid: Called for non-incoming/non-outoing message');
    }
    return ourUuid;
}
exports.getSourceUuid = getSourceUuid;
function getContactId(message, { conversationSelector, ourConversationId, ourNumber, ourUuid, }) {
    const source = getSource(message, ourNumber);
    const sourceUuid = getSourceUuid(message, ourUuid);
    if (!source && !sourceUuid) {
        return ourConversationId;
    }
    const conversation = conversationSelector(sourceUuid || source);
    return conversation.id;
}
// TODO: DESKTOP-2145
function getContact(message, { conversationSelector, ourConversationId, ourNumber, ourUuid, }) {
    const source = getSource(message, ourNumber);
    const sourceUuid = getSourceUuid(message, ourUuid);
    if (!source && !sourceUuid) {
        return conversationSelector(ourConversationId);
    }
    return conversationSelector(sourceUuid || source);
}
exports.getContact = getContact;
function getConversation(message, conversationSelector) {
    return conversationSelector(message.conversationId);
}
exports.getConversation = getConversation;
// Message
exports.getAttachmentsForMessage = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot)(
// `memoizeByRoot` requirement
lodash_1.identity, ({ sticker }) => sticker, ({ attachments }) => attachments, (_, sticker, attachments = []) => {
    if (sticker && sticker.data) {
        const { data } = sticker;
        // We don't show anything if we don't have the sticker or the blurhash...
        if (!data.blurHash && (data.pending || !data.path)) {
            return [];
        }
        return [
            Object.assign(Object.assign({}, data), { 
                // We want to show the blurhash for stickers, not the spinner
                pending: false, url: data.path
                    ? window.Signal.Migrations.getAbsoluteAttachmentPath(data.path)
                    : undefined }),
        ];
    }
    return attachments
        .filter(attachment => !attachment.error)
        .map(attachment => getPropsForAttachment(attachment))
        .filter(isNotNil_1.isNotNil);
});
exports.processBodyRanges = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot, lodash_1.isEqual)(
// `memoizeByRoot` requirement
lodash_1.identity, ({ bodyRanges }, { conversationSelector }) => {
    if (!bodyRanges) {
        return undefined;
    }
    return bodyRanges
        .filter(range => range.mentionUuid)
        .map(range => {
        const conversation = conversationSelector(range.mentionUuid);
        return Object.assign(Object.assign({}, range), { conversationID: conversation.id, replacementText: conversation.title });
    })
        .sort((a, b) => b.start - a.start);
}, (_, ranges) => ranges);
const getAuthorForMessage = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot)(
// `memoizeByRoot` requirement
lodash_1.identity, getContact, (_, convo) => {
    const { acceptedMessageRequest, avatarPath, badges, color, id, isMe, name, phoneNumber, profileName, sharedGroupNames, title, unblurredAvatarPath, } = convo;
    const unsafe = {
        acceptedMessageRequest,
        avatarPath,
        badges,
        color,
        id,
        isMe,
        name,
        phoneNumber,
        profileName,
        sharedGroupNames,
        title,
        unblurredAvatarPath,
    };
    const safe = unsafe;
    return safe;
});
const getCachedAuthorForMessage = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot, lodash_1.isEqual)(
// `memoizeByRoot` requirement
lodash_1.identity, getAuthorForMessage, (_, author) => author);
const getAuthorBadgeForMessage = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot, lodash_1.isEqual)(
// `memoizeByRoot` requirement
lodash_1.identity, (_, { preferredBadgeSelector }) => preferredBadgeSelector, getAuthorForMessage, (_, preferredBadgeSelector, author) => preferredBadgeSelector(author.badges));
exports.getPreviewsForMessage = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot)(
// `memoizeByRoot` requirement
lodash_1.identity, ({ preview }) => preview, (_, previews = []) => {
    return previews.map(preview => (Object.assign(Object.assign({}, preview), { isStickerPack: (0, LinkPreview_1.isStickerPack)(preview.url), domain: (0, LinkPreview_1.getDomain)(preview.url), image: preview.image ? getPropsForAttachment(preview.image) : null })));
});
exports.getReactionsForMessage = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot, lodash_1.isEqual)(
// `memoizeByRoot` requirement
lodash_1.identity, ({ reactions = [] }, { conversationSelector }) => {
    const reactionBySender = new Map();
    for (const reaction of reactions) {
        const existingReaction = reactionBySender.get(reaction.fromId);
        if (!existingReaction ||
            reaction.timestamp > existingReaction.timestamp) {
            reactionBySender.set(reaction.fromId, reaction);
        }
    }
    const reactionsWithEmpties = reactionBySender.values();
    const reactionsWithEmoji = iterables.filter(reactionsWithEmpties, re => re.emoji);
    const formattedReactions = iterables.map(reactionsWithEmoji, re => {
        const c = conversationSelector(re.fromId);
        const unsafe = (0, lodash_1.pick)(c, [
            'acceptedMessageRequest',
            'avatarPath',
            'color',
            'id',
            'isMe',
            'name',
            'phoneNumber',
            'profileName',
            'sharedGroupNames',
            'title',
        ]);
        const from = unsafe;
        (0, assert_1.strictAssert)(re.emoji, 'Expected all reactions to have an emoji');
        return {
            emoji: re.emoji,
            timestamp: re.timestamp,
            from,
        };
    });
    return [...formattedReactions];
}, (_, reactions) => reactions);
exports.getPropsForQuote = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot, lodash_1.isEqual)(
// `memoizeByRoot` requirement
lodash_1.identity, (message, { conversationSelector, ourConversationId, }) => {
    var _a;
    const { quote } = message;
    if (!quote) {
        return undefined;
    }
    const { author, authorUuid, id: sentAt, isViewOnce, referencedMessageNotFound, text, } = quote;
    const contact = conversationSelector(authorUuid || author);
    const authorId = contact.id;
    const authorName = contact.name;
    const authorPhoneNumber = contact.phoneNumber;
    const authorProfileName = contact.profileName;
    const authorTitle = contact.title;
    const isFromMe = authorId === ourConversationId;
    const firstAttachment = quote.attachments && quote.attachments[0];
    const conversation = getConversation(message, conversationSelector);
    const defaultConversationColor = window.Events.getDefaultConversationColor();
    return {
        authorId,
        authorName,
        authorPhoneNumber,
        authorProfileName,
        authorTitle,
        bodyRanges: (0, exports.processBodyRanges)(quote, { conversationSelector }),
        conversationColor: conversation.conversationColor || defaultConversationColor.color,
        customColor: conversation.customColor ||
            ((_a = defaultConversationColor.customColorData) === null || _a === void 0 ? void 0 : _a.value),
        isFromMe,
        rawAttachment: firstAttachment
            ? processQuoteAttachment(firstAttachment)
            : undefined,
        isViewOnce,
        referencedMessageNotFound,
        sentAt: Number(sentAt),
        text: createNonBreakingLastSeparator(text),
    };
}, (_, quote) => quote);
const getShallowPropsForMessage = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot, lodash_1.isEqual)(
// `memoizeByRoot` requirement
lodash_1.identity, (message, { accountSelector, conversationSelector, ourConversationId, ourNumber, ourUuid, regionCode, selectedMessageId, selectedMessageCounter, contactNameColorSelector, }) => {
    var _a, _b, _c;
    const { expireTimer, expirationStartTimestamp, conversationId } = message;
    const expirationLength = expireTimer ? expireTimer * 1000 : undefined;
    const expirationTimestamp = expirationStartTimestamp && expirationLength
        ? expirationStartTimestamp + expirationLength
        : undefined;
    const conversation = getConversation(message, conversationSelector);
    const isGroup = conversation.type === 'group';
    const { sticker } = message;
    const isMessageTapToView = isTapToView(message);
    const isSelected = message.id === selectedMessageId;
    const selectedReaction = ((message.reactions || []).find(re => re.fromId === ourConversationId) ||
        {}).emoji;
    const authorId = getContactId(message, {
        conversationSelector,
        ourConversationId,
        ourNumber,
        ourUuid,
    });
    const contactNameColor = contactNameColorSelector(conversationId, authorId);
    const defaultConversationColor = window.Events.getDefaultConversationColor();
    return {
        canDeleteForEveryone: canDeleteForEveryone(message),
        canDownload: canDownload(message, conversationSelector),
        canReply: canReply(message, ourConversationId, conversationSelector),
        contact: getPropsForEmbeddedContact(message, regionCode, accountSelector),
        contactNameColor,
        conversationColor: conversation.conversationColor || defaultConversationColor.color,
        conversationId,
        conversationType: isGroup ? 'group' : 'direct',
        customColor: conversation.customColor ||
            ((_a = defaultConversationColor.customColorData) === null || _a === void 0 ? void 0 : _a.value),
        deletedForEveryone: message.deletedForEveryone || false,
        direction: isIncoming(message) ? 'incoming' : 'outgoing',
        displayLimit: message.displayLimit,
        expirationLength,
        expirationTimestamp,
        id: message.id,
        isBlocked: conversation.isBlocked || false,
        isMessageRequestAccepted: (_b = conversation === null || conversation === void 0 ? void 0 : conversation.acceptedMessageRequest) !== null && _b !== void 0 ? _b : true,
        isSelected,
        isSelectedCounter: isSelected ? selectedMessageCounter : undefined,
        isSticker: Boolean(sticker),
        isTapToView: isMessageTapToView,
        isTapToViewError: isMessageTapToView && isIncoming(message) && message.isTapToViewInvalid,
        isTapToViewExpired: isMessageTapToView && message.isErased,
        readStatus: (_c = message.readStatus) !== null && _c !== void 0 ? _c : MessageReadStatus_1.ReadStatus.Read,
        selectedReaction,
        status: getMessagePropStatus(message, ourConversationId),
        text: createNonBreakingLastSeparator(message.body),
        textPending: message.bodyPending,
        timestamp: message.sent_at,
    };
}, (_, props) => props);
exports.getPropsForMessage = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot)(
// `memoizeByRoot` requirement
lodash_1.identity, exports.getAttachmentsForMessage, exports.processBodyRanges, getCachedAuthorForMessage, getAuthorBadgeForMessage, exports.getPreviewsForMessage, exports.getReactionsForMessage, exports.getPropsForQuote, getShallowPropsForMessage, (_, attachments, bodyRanges, author, authorBadge, previews, reactions, quote, shallowProps) => {
    return Object.assign({ attachments,
        author,
        authorBadge,
        bodyRanges,
        previews,
        quote,
        reactions }, shallowProps);
});
exports.getBubblePropsForMessage = (0, reselect_1.createSelectorCreator)(memoizeByRoot_1.memoizeByRoot)(
// `memoizeByRoot` requirement
lodash_1.identity, exports.getPropsForMessage, (_, data) => ({
    type: 'message',
    data,
}));
// Top-level prop generation for the message bubble
function getPropsForBubble(message, options) {
    if (isUnsupportedMessage(message)) {
        return {
            type: 'unsupportedMessage',
            data: getPropsForUnsupportedMessage(message, options),
        };
    }
    if (isGroupV2Change(message)) {
        return {
            type: 'groupV2Change',
            data: getPropsForGroupV2Change(message, options),
        };
    }
    if (isGroupV1Migration(message)) {
        return {
            type: 'groupV1Migration',
            data: getPropsForGroupV1Migration(message, options),
        };
    }
    if (isMessageHistoryUnsynced(message)) {
        return {
            type: 'linkNotification',
            data: null,
        };
    }
    if (isExpirationTimerUpdate(message)) {
        return {
            type: 'timerNotification',
            data: getPropsForTimerNotification(message, options),
        };
    }
    if (isKeyChange(message)) {
        return {
            type: 'safetyNumberNotification',
            data: getPropsForSafetyNumberNotification(message, options),
        };
    }
    if (isVerifiedChange(message)) {
        return {
            type: 'verificationNotification',
            data: getPropsForVerificationNotification(message, options),
        };
    }
    if (isGroupUpdate(message)) {
        return {
            type: 'groupNotification',
            data: getPropsForGroupNotification(message, options),
        };
    }
    if (isEndSession(message)) {
        return {
            type: 'resetSessionNotification',
            data: null,
        };
    }
    if (isCallHistory(message)) {
        return {
            type: 'callHistory',
            data: getPropsForCallHistory(message, options),
        };
    }
    if (isProfileChange(message)) {
        return {
            type: 'profileChange',
            data: getPropsForProfileChange(message, options),
        };
    }
    if (isUniversalTimerNotification(message)) {
        return {
            type: 'universalTimerNotification',
            data: null,
        };
    }
    if (isChangeNumberNotification(message)) {
        return {
            type: 'changeNumberNotification',
            data: getPropsForChangeNumberNotification(message, options),
        };
    }
    if (isChatSessionRefreshed(message)) {
        return {
            type: 'chatSessionRefreshed',
            data: null,
        };
    }
    if (isDeliveryIssue(message)) {
        return {
            type: 'deliveryIssue',
            data: getPropsForDeliveryIssue(message, options),
        };
    }
    return (0, exports.getBubblePropsForMessage)(message, options);
}
exports.getPropsForBubble = getPropsForBubble;
// Unsupported Message
function isUnsupportedMessage(message) {
    const versionAtReceive = message.supportedVersionAtReceive;
    const requiredVersion = message.requiredProtocolVersion;
    return ((0, lodash_1.isNumber)(versionAtReceive) &&
        (0, lodash_1.isNumber)(requiredVersion) &&
        versionAtReceive < requiredVersion);
}
exports.isUnsupportedMessage = isUnsupportedMessage;
function getPropsForUnsupportedMessage(message, options) {
    const CURRENT_PROTOCOL_VERSION = protobuf_1.SignalService.DataMessage.ProtocolVersion.CURRENT;
    const requiredVersion = message.requiredProtocolVersion;
    const canProcessNow = Boolean(CURRENT_PROTOCOL_VERSION &&
        requiredVersion &&
        CURRENT_PROTOCOL_VERSION >= requiredVersion);
    return {
        canProcessNow,
        contact: getContact(message, options),
    };
}
// GroupV2 Change
function isGroupV2Change(message) {
    return Boolean(message.groupV2Change);
}
exports.isGroupV2Change = isGroupV2Change;
function getPropsForGroupV2Change(message, { conversationSelector, ourUuid }) {
    const change = message.groupV2Change;
    if (!change) {
        throw new Error('getPropsForGroupV2Change: Change is missing!');
    }
    const conversation = getConversation(message, conversationSelector);
    return {
        groupName: (conversation === null || conversation === void 0 ? void 0 : conversation.type) === 'group' ? conversation === null || conversation === void 0 ? void 0 : conversation.name : undefined,
        ourUuid,
        change,
    };
}
// GroupV1 Migration
function isGroupV1Migration(message) {
    return message.type === 'group-v1-migration';
}
exports.isGroupV1Migration = isGroupV1Migration;
function getPropsForGroupV1Migration(message, { conversationSelector }) {
    const migration = message.groupMigration;
    if (!migration) {
        // Backwards-compatibility with data schema in early betas
        const invitedGV2Members = message.invitedGV2Members || [];
        const droppedGV2MemberIds = message.droppedGV2MemberIds || [];
        const invitedMembers = invitedGV2Members.map(item => conversationSelector(item.uuid));
        const droppedMembers = droppedGV2MemberIds.map(conversationId => conversationSelector(conversationId));
        return {
            areWeInvited: false,
            droppedMembers,
            invitedMembers,
        };
    }
    const { areWeInvited, droppedMemberIds, invitedMembers: rawInvitedMembers, } = migration;
    const invitedMembers = rawInvitedMembers.map(item => conversationSelector(item.uuid));
    const droppedMembers = droppedMemberIds.map(conversationId => conversationSelector(conversationId));
    return {
        areWeInvited,
        droppedMembers,
        invitedMembers,
    };
}
// Message History Unsynced
function isMessageHistoryUnsynced(message) {
    return message.type === 'message-history-unsynced';
}
exports.isMessageHistoryUnsynced = isMessageHistoryUnsynced;
// Note: props are null!
// Expiration Timer Update
function isExpirationTimerUpdate(message) {
    const flag = protobuf_1.SignalService.DataMessage.Flags.EXPIRATION_TIMER_UPDATE;
    // eslint-disable-next-line no-bitwise
    return Boolean(message.flags && message.flags & flag);
}
exports.isExpirationTimerUpdate = isExpirationTimerUpdate;
function getPropsForTimerNotification(message, { ourConversationId, conversationSelector }) {
    const timerUpdate = message.expirationTimerUpdate;
    if (!timerUpdate) {
        throw new Error('getPropsForTimerNotification: missing expirationTimerUpdate!');
    }
    const { expireTimer, fromSync, source, sourceUuid } = timerUpdate;
    const disabled = !expireTimer;
    const sourceId = sourceUuid || source;
    const formattedContact = conversationSelector(sourceId);
    const basicProps = Object.assign(Object.assign({}, formattedContact), { disabled,
        expireTimer, type: 'fromOther' });
    if (fromSync) {
        return Object.assign(Object.assign({}, basicProps), { type: 'fromSync' });
    }
    if (formattedContact.id === ourConversationId) {
        return Object.assign(Object.assign({}, basicProps), { type: 'fromMe' });
    }
    if (!sourceId) {
        return Object.assign(Object.assign({}, basicProps), { type: 'fromMember' });
    }
    return basicProps;
}
// Key Change
function isKeyChange(message) {
    return message.type === 'keychange';
}
exports.isKeyChange = isKeyChange;
function getPropsForSafetyNumberNotification(message, { conversationSelector }) {
    const conversation = getConversation(message, conversationSelector);
    const isGroup = (conversation === null || conversation === void 0 ? void 0 : conversation.type) === 'group';
    const identifier = message.key_changed;
    const contact = conversationSelector(identifier);
    return {
        isGroup,
        contact,
    };
}
// Verified Change
function isVerifiedChange(message) {
    return message.type === 'verified-change';
}
exports.isVerifiedChange = isVerifiedChange;
function getPropsForVerificationNotification(message, { conversationSelector }) {
    const type = message.verified ? 'markVerified' : 'markNotVerified';
    const isLocal = message.local || false;
    const identifier = message.verifiedChanged;
    return {
        type,
        isLocal,
        contact: conversationSelector(identifier),
    };
}
// Group Update (V1)
function isGroupUpdate(message) {
    return Boolean(message.group_update);
}
exports.isGroupUpdate = isGroupUpdate;
function getPropsForGroupNotification(message, options) {
    var _a;
    const groupUpdate = message.group_update;
    if (!groupUpdate) {
        throw new Error('getPropsForGroupNotification: Message missing group_update');
    }
    const { conversationSelector } = options;
    const changes = [];
    if (!groupUpdate.avatarUpdated &&
        !groupUpdate.left &&
        !groupUpdate.joined &&
        !groupUpdate.name) {
        changes.push({
            type: 'general',
        });
    }
    if ((_a = groupUpdate.joined) === null || _a === void 0 ? void 0 : _a.length) {
        changes.push({
            type: 'add',
            contacts: (0, lodash_1.map)(Array.isArray(groupUpdate.joined)
                ? groupUpdate.joined
                : [groupUpdate.joined], identifier => conversationSelector(identifier)),
        });
    }
    if (groupUpdate.left === 'You') {
        changes.push({
            type: 'remove',
        });
    }
    else if (groupUpdate.left) {
        changes.push({
            type: 'remove',
            contacts: (0, lodash_1.map)(Array.isArray(groupUpdate.left) ? groupUpdate.left : [groupUpdate.left], identifier => conversationSelector(identifier)),
        });
    }
    if (groupUpdate.name) {
        changes.push({
            type: 'name',
            newName: groupUpdate.name,
        });
    }
    if (groupUpdate.avatarUpdated) {
        changes.push({
            type: 'avatar',
        });
    }
    const from = getContact(message, options);
    return {
        from,
        changes,
    };
}
// End Session
function isEndSession(message) {
    const flag = protobuf_1.SignalService.DataMessage.Flags.END_SESSION;
    // eslint-disable-next-line no-bitwise
    return Boolean(message.flags && message.flags & flag);
}
exports.isEndSession = isEndSession;
// Call History
function isCallHistory(message) {
    return message.type === 'call-history';
}
exports.isCallHistory = isCallHistory;
function getPropsForCallHistory(message, { conversationSelector, callSelector, activeCall, }) {
    var _a, _b;
    const { callHistoryDetails } = message;
    if (!callHistoryDetails) {
        throw new Error('getPropsForCallHistory: Missing callHistoryDetails');
    }
    switch (callHistoryDetails.callMode) {
        // Old messages weren't saved with a call mode.
        case undefined:
        case Calling_1.CallMode.Direct:
            return Object.assign(Object.assign({}, callHistoryDetails), { callMode: Calling_1.CallMode.Direct });
        case Calling_1.CallMode.Group: {
            const { conversationId } = message;
            if (!conversationId) {
                throw new Error('getPropsForCallHistory: missing conversation ID');
            }
            const creator = conversationSelector(callHistoryDetails.creatorUuid);
            let call = callSelector(conversationId);
            if (call && call.callMode !== Calling_1.CallMode.Group) {
                log.error('getPropsForCallHistory: there is an unexpected non-group call; pretending it does not exist');
                call = undefined;
            }
            return {
                activeCallConversationId: activeCall === null || activeCall === void 0 ? void 0 : activeCall.conversationId,
                callMode: Calling_1.CallMode.Group,
                conversationId,
                creator,
                deviceCount: (_a = call === null || call === void 0 ? void 0 : call.peekInfo.deviceCount) !== null && _a !== void 0 ? _a : 0,
                ended: callHistoryDetails.eraId !== (call === null || call === void 0 ? void 0 : call.peekInfo.eraId),
                maxDevices: (_b = call === null || call === void 0 ? void 0 : call.peekInfo.maxDevices) !== null && _b !== void 0 ? _b : Infinity,
                startedTime: callHistoryDetails.startedTime,
            };
        }
        default:
            throw new Error(`getPropsForCallHistory: missing case ${(0, missingCaseError_1.missingCaseError)(callHistoryDetails)}`);
    }
}
exports.getPropsForCallHistory = getPropsForCallHistory;
// Profile Change
function isProfileChange(message) {
    return message.type === 'profile-change';
}
exports.isProfileChange = isProfileChange;
function getPropsForProfileChange(message, { conversationSelector }) {
    const change = message.profileChange;
    const { changedId } = message;
    const changedContact = conversationSelector(changedId);
    if (!change) {
        throw new Error('getPropsForProfileChange: profileChange is undefined');
    }
    return {
        changedContact,
        change,
    };
}
// Universal Timer Notification
// Note: smart, so props not generated here
function isUniversalTimerNotification(message) {
    return message.type === 'universal-timer-notification';
}
exports.isUniversalTimerNotification = isUniversalTimerNotification;
// Change Number Notification
function isChangeNumberNotification(message) {
    return message.type === 'change-number-notification';
}
exports.isChangeNumberNotification = isChangeNumberNotification;
function getPropsForChangeNumberNotification(message, { conversationSelector }) {
    return {
        sender: conversationSelector(message.sourceUuid),
        timestamp: message.sent_at,
    };
}
// Chat Session Refreshed
function isChatSessionRefreshed(message) {
    return message.type === 'chat-session-refreshed';
}
exports.isChatSessionRefreshed = isChatSessionRefreshed;
// Note: props are null
// Delivery Issue
function isDeliveryIssue(message) {
    return message.type === 'delivery-issue';
}
exports.isDeliveryIssue = isDeliveryIssue;
function getPropsForDeliveryIssue(message, { conversationSelector }) {
    const sender = conversationSelector(message.sourceUuid);
    const conversation = conversationSelector(message.conversationId);
    return {
        sender,
        inGroup: conversation.type === 'group',
    };
}
// Other utility functions
function isTapToView(message) {
    // If a message is deleted for everyone, that overrides all other styling
    if (message.deletedForEveryone) {
        return false;
    }
    return Boolean(message.isViewOnce || message.messageTimer);
}
exports.isTapToView = isTapToView;
function createNonBreakingLastSeparator(text) {
    if (!text) {
        return '';
    }
    const nbsp = '\xa0';
    const regex = /(\S)( +)(\S+\s*)$/;
    return text.replace(regex, (_match, start, spaces, end) => {
        const newSpaces = end.length < 12
            ? (0, lodash_1.reduce)(spaces, accumulator => accumulator + nbsp, '')
            : spaces;
        return `${start}${newSpaces}${end}`;
    });
}
function getMessagePropStatus(message, ourConversationId) {
    var _a, _b;
    if (!isOutgoing(message)) {
        return undefined;
    }
    if (getLastChallengeError(message)) {
        return 'paused';
    }
    const { sendStateByConversationId = {} } = message;
    if ((0, MessageSendState_1.isMessageJustForMe)(sendStateByConversationId, ourConversationId)) {
        const status = (_b = (_a = sendStateByConversationId[ourConversationId]) === null || _a === void 0 ? void 0 : _a.status) !== null && _b !== void 0 ? _b : MessageSendState_1.SendStatus.Pending;
        const sent = (0, MessageSendState_1.isSent)(status);
        if (hasErrors(message) ||
            (0, MessageSendState_1.someSendStatus)(sendStateByConversationId, MessageSendState_1.isFailed)) {
            return sent ? 'partial-sent' : 'error';
        }
        return sent ? 'viewed' : 'sending';
    }
    const sendStates = Object.values((0, lodash_1.omit)(sendStateByConversationId, ourConversationId));
    const highestSuccessfulStatus = sendStates.reduce((result, { status }) => (0, MessageSendState_1.maxStatus)(result, status), MessageSendState_1.SendStatus.Pending);
    if (hasErrors(message) ||
        (0, MessageSendState_1.someSendStatus)(sendStateByConversationId, MessageSendState_1.isFailed)) {
        return (0, MessageSendState_1.isSent)(highestSuccessfulStatus) ? 'partial-sent' : 'error';
    }
    if ((0, MessageSendState_1.isViewed)(highestSuccessfulStatus)) {
        return 'viewed';
    }
    if ((0, MessageSendState_1.isRead)(highestSuccessfulStatus)) {
        return 'read';
    }
    if ((0, MessageSendState_1.isDelivered)(highestSuccessfulStatus)) {
        return 'delivered';
    }
    if ((0, MessageSendState_1.isSent)(highestSuccessfulStatus)) {
        return 'sent';
    }
    return 'sending';
}
exports.getMessagePropStatus = getMessagePropStatus;
function getPropsForEmbeddedContact(message, regionCode, accountSelector) {
    const contacts = message.contact;
    if (!contacts || !contacts.length) {
        return undefined;
    }
    const firstContact = contacts[0];
    const numbers = firstContact === null || firstContact === void 0 ? void 0 : firstContact.number;
    const firstNumber = numbers && numbers[0] ? numbers[0].value : undefined;
    return (0, EmbeddedContact_1.embeddedContactSelector)(firstContact, {
        regionCode,
        getAbsoluteAttachmentPath: window.Signal.Migrations.getAbsoluteAttachmentPath,
        firstNumber,
        isNumberOnSignal: accountSelector(firstNumber),
    });
}
exports.getPropsForEmbeddedContact = getPropsForEmbeddedContact;
function getPropsForAttachment(attachment) {
    if (!attachment) {
        return null;
    }
    const { path, pending, size, screenshot, thumbnail } = attachment;
    return Object.assign(Object.assign({}, attachment), { fileSize: size ? (0, filesize_1.default)(size) : undefined, isVoiceMessage: (0, Attachment_1.isVoiceMessage)(attachment), pending, url: path
            ? window.Signal.Migrations.getAbsoluteAttachmentPath(path)
            : undefined, screenshot: screenshot
            ? Object.assign(Object.assign({}, screenshot), { url: window.Signal.Migrations.getAbsoluteAttachmentPath(screenshot.path) }) : undefined, thumbnail: thumbnail
            ? Object.assign(Object.assign({}, thumbnail), { url: window.Signal.Migrations.getAbsoluteAttachmentPath(thumbnail.path) }) : undefined });
}
exports.getPropsForAttachment = getPropsForAttachment;
function processQuoteAttachment(attachment) {
    const { thumbnail } = attachment;
    const path = thumbnail &&
        thumbnail.path &&
        window.Signal.Migrations.getAbsoluteAttachmentPath(thumbnail.path);
    const objectUrl = thumbnail && thumbnail.objectUrl;
    const thumbnailWithObjectUrl = (!path && !objectUrl) || !thumbnail
        ? undefined
        : Object.assign(Object.assign({}, thumbnail), { objectUrl: path || objectUrl });
    return Object.assign(Object.assign({}, attachment), { isVoiceMessage: (0, Attachment_1.isVoiceMessage)(attachment), thumbnail: thumbnailWithObjectUrl });
}
function canReplyOrReact(message, ourConversationId, conversation) {
    const { deletedForEveryone, sendStateByConversationId } = message;
    if (!conversation) {
        return false;
    }
    if (conversation.isGroupV1AndDisabled) {
        return false;
    }
    if ((0, conversations_1.isMissingRequiredProfileSharing)(conversation)) {
        return false;
    }
    if (!conversation.acceptedMessageRequest) {
        return false;
    }
    if (deletedForEveryone) {
        return false;
    }
    if (isOutgoing(message)) {
        return ((0, MessageSendState_1.isMessageJustForMe)(sendStateByConversationId, ourConversationId) ||
            (0, MessageSendState_1.someSendStatus)((0, lodash_1.omit)(sendStateByConversationId, ourConversationId), MessageSendState_1.isSent));
    }
    if (isIncoming(message)) {
        return true;
    }
    // Fail safe.
    return false;
}
function canReply(message, ourConversationId, conversationSelector) {
    const conversation = getConversation(message, conversationSelector);
    if (!conversation ||
        (conversation.announcementsOnly && !conversation.areWeAdmin)) {
        return false;
    }
    return canReplyOrReact(message, ourConversationId, conversation);
}
exports.canReply = canReply;
function canReact(message, ourConversationId, conversationSelector) {
    const conversation = getConversation(message, conversationSelector);
    return canReplyOrReact(message, ourConversationId, conversation);
}
exports.canReact = canReact;
function canDeleteForEveryone(message) {
    return (
    // Is this a message I sent?
    isOutgoing(message) &&
        // Has the message already been deleted?
        !message.deletedForEveryone &&
        // Is it too old to delete?
        (0, timestamp_1.isMoreRecentThan)(message.sent_at, THREE_HOURS) &&
        // Is it pending/sent to anyone?
        (0, MessageSendState_1.someSendStatus)(message.sendStateByConversationId, sendStatus => sendStatus !== MessageSendState_1.SendStatus.Failed));
}
exports.canDeleteForEveryone = canDeleteForEveryone;
function canDownload(message, conversationSelector) {
    if (isOutgoing(message)) {
        return true;
    }
    const conversation = getConversation(message, conversationSelector);
    const isAccepted = Boolean(conversation && conversation.acceptedMessageRequest);
    if (!isAccepted) {
        return false;
    }
    // Ensure that all attachments are downloadable
    const { attachments } = message;
    if (attachments && attachments.length) {
        return attachments.every(attachment => Boolean(attachment.path));
    }
    return true;
}
exports.canDownload = canDownload;
function getLastChallengeError(message) {
    const { errors } = message;
    if (!errors) {
        return undefined;
    }
    const challengeErrors = errors
        .filter((error) => {
        return (error.name === 'SendMessageChallengeError' &&
            (0, lodash_1.isNumber)(error.retryAfter) &&
            (0, lodash_1.isObject)(error.data));
    })
        .sort((a, b) => a.retryAfter - b.retryAfter);
    return challengeErrors.pop();
}
exports.getLastChallengeError = getLastChallengeError;
