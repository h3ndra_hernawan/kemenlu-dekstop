"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getIsCustomizingPreferredReactions = exports.getCustomizeModalState = void 0;
const reselect_1 = require("reselect");
const getPreferredReactionsState = (state) => state.preferredReactions;
exports.getCustomizeModalState = (0, reselect_1.createSelector)(getPreferredReactionsState, (state) => state.customizePreferredReactionsModal);
exports.getIsCustomizingPreferredReactions = (0, reselect_1.createSelector)(exports.getCustomizeModalState, (customizeModal) => Boolean(customizeModal));
