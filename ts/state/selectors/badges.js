"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPreferredBadgeSelector = exports.getBadgesSelector = exports.getBadgesById = void 0;
const reselect_1 = require("reselect");
const lodash_1 = require("lodash");
const log = __importStar(require("../../logging/log"));
const getOwn_1 = require("../../util/getOwn");
const getBadgeState = (state) => state.badges;
exports.getBadgesById = (0, reselect_1.createSelector)(getBadgeState, state => (0, lodash_1.mapValues)(state.byId, badge => (Object.assign(Object.assign({}, badge), { images: badge.images.map(image => (0, lodash_1.mapValues)(image, imageFile => imageFile.localPath
        ? Object.assign(Object.assign({}, imageFile), { localPath: window.Signal.Migrations.getAbsoluteBadgeImageFilePath(imageFile.localPath) }) : imageFile)) }))));
exports.getBadgesSelector = (0, reselect_1.createSelector)(exports.getBadgesById, badgesById => (conversationBadges) => {
    const result = [];
    for (const { id } of conversationBadges) {
        const badge = (0, getOwn_1.getOwn)(badgesById, id);
        if (!badge) {
            log.error('getBadgesSelector: conversation badge was not found');
            continue;
        }
        result.push(badge);
    }
    return result;
});
exports.getPreferredBadgeSelector = (0, reselect_1.createSelector)(exports.getBadgesById, (badgesById) => conversationBadges => {
    var _a;
    const firstId = (_a = conversationBadges[0]) === null || _a === void 0 ? void 0 : _a.id;
    if (!firstId) {
        return undefined;
    }
    const badge = (0, getOwn_1.getOwn)(badgesById, firstId);
    if (!badge) {
        log.error('getPreferredBadgeSelector: conversation badge was not found');
        return undefined;
    }
    return badge;
});
