"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLinkPreview = void 0;
const reselect_1 = require("reselect");
const assert_1 = require("../../util/assert");
const LinkPreview_1 = require("../../types/LinkPreview");
exports.getLinkPreview = (0, reselect_1.createSelector)(({ linkPreviews }) => linkPreviews.linkPreview, linkPreview => {
    if (linkPreview) {
        const domain = (0, LinkPreview_1.getDomain)(linkPreview.url);
        (0, assert_1.assert)(domain !== undefined, "Domain of linkPreview can't be undefined");
        return Object.assign(Object.assign({}, linkPreview), { domain, isLoaded: true });
    }
    return undefined;
});
