"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMessageSelector = exports.getContactNameColorSelector = exports.getCachedSelectorForMessage = exports.getConversationByUuidSelector = exports.getConversationByIdSelector = exports.getConversationSelector = exports.getCachedSelectorForConversation = exports._conversationSelector = exports.getComposeSelectedContacts = exports.getComposeGroupExpireTimer = exports.getComposeGroupName = exports.getComposeGroupAvatar = exports.getCantAddContactForModal = exports.getFilteredCandidateContactsForNewGroup = exports.getFilteredComposeGroups = exports.getFilteredComposeContacts = exports.getComposableGroups = exports.getCandidateContactsForNewGroup = exports.getComposableContacts = exports.getAllComposableConversations = exports.getIsFetchingUsername = exports.getComposerConversationSearchTerm = exports.getMe = exports.getRecommendedGroupSizeModalState = exports.getMaximumGroupSizeModalState = exports.getLeftPaneLists = exports._getLeftPaneLists = exports.getConversationComparator = exports._getConversationComparator = exports.getMessagesByConversation = exports.getMessages = exports.getComposeAvatarData = exports.isEditingAvatar = exports.isCreatingGroup = exports.hasGroupCreationError = exports.getComposerStep = exports.getShowArchived = exports.getUsernameSaveState = exports.getSelectedMessage = exports.getSelectedConversation = exports.getSelectedConversationId = exports.getConversationsByTitleSelector = exports.getConversationsByUsername = exports.getConversationsByGroupId = exports.getConversationsByE164 = exports.getConversationsByUuid = exports.getConversationLookup = exports.getPreJoinConversation = exports.getConversations = exports.getPlaceholderContact = void 0;
exports.getNumberOfMessagesPendingBecauseOfVerification = exports.getMessageIdsPendingBecauseOfVerification = exports.getConversationsStoppingMessageSendBecauseOfVerification = exports.getGroupAdminsSelector = exports.isMissingRequiredProfileSharing = exports.getConversationsWithCustomColorSelector = exports.getInvitedContactsForNewlyCreatedGroup = exports.getConversationMessagesSelector = exports.getCachedSelectorForConversationMessages = exports._conversationMessagesSelector = void 0;
const memoizee_1 = __importDefault(require("memoizee"));
const lodash_1 = require("lodash");
const reselect_1 = require("reselect");
const conversationsEnums_1 = require("../ducks/conversationsEnums");
const getOwn_1 = require("../../util/getOwn");
const isNotNil_1 = require("../../util/isNotNil");
const deconstructLookup_1 = require("../../util/deconstructLookup");
const assert_1 = require("../../util/assert");
const isConversationUnregistered_1 = require("../../util/isConversationUnregistered");
const filterAndSortConversations_1 = require("../../util/filterAndSortConversations");
const Colors_1 = require("../../types/Colors");
const isInSystemContacts_1 = require("../../util/isInSystemContacts");
const sortByTitle_1 = require("../../util/sortByTitle");
const whatTypeOfConversation_1 = require("../../util/whatTypeOfConversation");
const user_1 = require("./user");
const items_1 = require("./items");
const message_1 = require("./message");
const calling_1 = require("./calling");
const badges_1 = require("./badges");
const accounts_1 = require("./accounts");
const log = __importStar(require("../../logging/log"));
let placeholderContact;
const getPlaceholderContact = () => {
    if (placeholderContact) {
        return placeholderContact;
    }
    placeholderContact = {
        acceptedMessageRequest: false,
        badges: [],
        id: 'placeholder-contact',
        type: 'direct',
        title: window.i18n('unknownContact'),
        isMe: false,
        sharedGroupNames: [],
    };
    return placeholderContact;
};
exports.getPlaceholderContact = getPlaceholderContact;
const getConversations = (state) => state.conversations;
exports.getConversations = getConversations;
exports.getPreJoinConversation = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.preJoinConversation;
});
exports.getConversationLookup = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.conversationLookup;
});
exports.getConversationsByUuid = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.conversationsByUuid;
});
exports.getConversationsByE164 = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.conversationsByE164;
});
exports.getConversationsByGroupId = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.conversationsByGroupId;
});
exports.getConversationsByUsername = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.conversationsByUsername;
});
const getAllConversations = (0, reselect_1.createSelector)(exports.getConversationLookup, (lookup) => Object.values(lookup));
exports.getConversationsByTitleSelector = (0, reselect_1.createSelector)(getAllConversations, (conversations) => (title) => conversations.filter(conversation => conversation.title === title));
exports.getSelectedConversationId = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.selectedConversationId;
});
exports.getSelectedConversation = (0, reselect_1.createSelector)(exports.getSelectedConversationId, exports.getConversationLookup, (selectedConversationId, conversationLookup) => {
    if (!selectedConversationId) {
        return undefined;
    }
    const conversation = (0, getOwn_1.getOwn)(conversationLookup, selectedConversationId);
    (0, assert_1.assert)(conversation, 'getSelectedConversation: could not find selected conversation in lookup; returning undefined');
    return conversation;
});
exports.getSelectedMessage = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    if (!state.selectedMessage) {
        return undefined;
    }
    return {
        id: state.selectedMessage,
        counter: state.selectedMessageCounter,
    };
});
exports.getUsernameSaveState = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.usernameSaveState;
});
exports.getShowArchived = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return Boolean(state.showArchived);
});
const getComposerState = (0, reselect_1.createSelector)(exports.getConversations, (state) => state.composer);
exports.getComposerStep = (0, reselect_1.createSelector)(getComposerState, (composerState) => composerState === null || composerState === void 0 ? void 0 : composerState.step);
exports.hasGroupCreationError = (0, reselect_1.createSelector)(getComposerState, (composerState) => {
    if ((composerState === null || composerState === void 0 ? void 0 : composerState.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata) {
        return composerState.hasError;
    }
    return false;
});
exports.isCreatingGroup = (0, reselect_1.createSelector)(getComposerState, (composerState) => (composerState === null || composerState === void 0 ? void 0 : composerState.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata &&
    composerState.isCreating);
exports.isEditingAvatar = (0, reselect_1.createSelector)(getComposerState, (composerState) => (composerState === null || composerState === void 0 ? void 0 : composerState.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata &&
    composerState.isEditingAvatar);
exports.getComposeAvatarData = (0, reselect_1.createSelector)(getComposerState, (composerState) => (composerState === null || composerState === void 0 ? void 0 : composerState.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata
    ? composerState.userAvatarData
    : []);
exports.getMessages = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.messagesLookup;
});
exports.getMessagesByConversation = (0, reselect_1.createSelector)(exports.getConversations, (state) => {
    return state.messagesByConversation;
});
const collator = new Intl.Collator();
// Note: we will probably want to put i18n and regionCode back when we are formatting
//   phone numbers and contacts from scratch here again.
const _getConversationComparator = () => {
    return (left, right) => {
        const leftTimestamp = left.timestamp;
        const rightTimestamp = right.timestamp;
        if (leftTimestamp && !rightTimestamp) {
            return -1;
        }
        if (rightTimestamp && !leftTimestamp) {
            return 1;
        }
        if (leftTimestamp && rightTimestamp && leftTimestamp !== rightTimestamp) {
            return rightTimestamp - leftTimestamp;
        }
        if (typeof left.inboxPosition === 'number' &&
            typeof right.inboxPosition === 'number') {
            return right.inboxPosition > left.inboxPosition ? -1 : 1;
        }
        if (typeof left.inboxPosition === 'number' && right.inboxPosition == null) {
            return -1;
        }
        if (typeof right.inboxPosition === 'number' && left.inboxPosition == null) {
            return 1;
        }
        return collator.compare(left.title, right.title);
    };
};
exports._getConversationComparator = _getConversationComparator;
exports.getConversationComparator = (0, reselect_1.createSelector)(user_1.getIntl, user_1.getRegionCode, exports._getConversationComparator);
const _getLeftPaneLists = (lookup, comparator, selectedConversation, pinnedConversationIds) => {
    const conversations = [];
    const archivedConversations = [];
    const pinnedConversations = [];
    const values = Object.values(lookup);
    const max = values.length;
    for (let i = 0; i < max; i += 1) {
        let conversation = values[i];
        if (selectedConversation === conversation.id) {
            conversation = Object.assign(Object.assign({}, conversation), { isSelected: true });
        }
        // We always show pinned conversations
        if (conversation.isPinned) {
            pinnedConversations.push(conversation);
            continue;
        }
        if (conversation.activeAt) {
            if (conversation.isArchived) {
                archivedConversations.push(conversation);
            }
            else {
                conversations.push(conversation);
            }
        }
    }
    conversations.sort(comparator);
    archivedConversations.sort(comparator);
    pinnedConversations.sort((a, b) => (pinnedConversationIds || []).indexOf(a.id) -
        (pinnedConversationIds || []).indexOf(b.id));
    return { conversations, archivedConversations, pinnedConversations };
};
exports._getLeftPaneLists = _getLeftPaneLists;
exports.getLeftPaneLists = (0, reselect_1.createSelector)(exports.getConversationLookup, exports.getConversationComparator, exports.getSelectedConversationId, items_1.getPinnedConversationIds, exports._getLeftPaneLists);
exports.getMaximumGroupSizeModalState = (0, reselect_1.createSelector)(getComposerState, (composerState) => {
    switch (composerState === null || composerState === void 0 ? void 0 : composerState.step) {
        case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
        case conversationsEnums_1.ComposerStep.SetGroupMetadata:
            return composerState.maximumGroupSizeModalState;
        default:
            (0, assert_1.assert)(false, 'Can\'t get the maximum group size modal state in this composer state; returning "never shown"');
            return conversationsEnums_1.OneTimeModalState.NeverShown;
    }
});
exports.getRecommendedGroupSizeModalState = (0, reselect_1.createSelector)(getComposerState, (composerState) => {
    switch (composerState === null || composerState === void 0 ? void 0 : composerState.step) {
        case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
        case conversationsEnums_1.ComposerStep.SetGroupMetadata:
            return composerState.recommendedGroupSizeModalState;
        default:
            (0, assert_1.assert)(false, 'Can\'t get the recommended group size modal state in this composer state; returning "never shown"');
            return conversationsEnums_1.OneTimeModalState.NeverShown;
    }
});
exports.getMe = (0, reselect_1.createSelector)([exports.getConversationLookup, user_1.getUserConversationId], (lookup, ourConversationId) => {
    return lookup[ourConversationId];
});
exports.getComposerConversationSearchTerm = (0, reselect_1.createSelector)(getComposerState, (composer) => {
    if (!composer) {
        (0, assert_1.assert)(false, 'getComposerConversationSearchTerm: composer is not open');
        return '';
    }
    if (composer.step === conversationsEnums_1.ComposerStep.SetGroupMetadata) {
        (0, assert_1.assert)(false, 'getComposerConversationSearchTerm: composer does not have a search term');
        return '';
    }
    return composer.searchTerm;
});
exports.getIsFetchingUsername = (0, reselect_1.createSelector)(getComposerState, (composer) => {
    if (!composer) {
        (0, assert_1.assert)(false, 'getIsFetchingUsername: composer is not open');
        return false;
    }
    if (composer.step !== conversationsEnums_1.ComposerStep.StartDirectConversation) {
        (0, assert_1.assert)(false, `getIsFetchingUsername: step ${composer.step} has no isFetchingUsername key`);
        return false;
    }
    return composer.isFetchingUsername;
});
function isTrusted(conversation) {
    if (conversation.type === 'group') {
        return true;
    }
    return Boolean((0, isInSystemContacts_1.isInSystemContacts)(conversation) ||
        conversation.profileSharing ||
        conversation.isMe);
}
function hasDisplayInfo(conversation) {
    if (conversation.type === 'group') {
        return Boolean(conversation.name);
    }
    return Boolean(conversation.name ||
        conversation.profileName ||
        conversation.phoneNumber ||
        conversation.isMe);
}
function canComposeConversation(conversation) {
    return Boolean(!conversation.isBlocked &&
        !(0, isConversationUnregistered_1.isConversationUnregistered)(conversation) &&
        hasDisplayInfo(conversation) &&
        isTrusted(conversation));
}
exports.getAllComposableConversations = (0, reselect_1.createSelector)(exports.getConversationLookup, (conversationLookup) => Object.values(conversationLookup).filter(conversation => !conversation.isBlocked &&
    !conversation.isGroupV1AndDisabled &&
    !(0, isConversationUnregistered_1.isConversationUnregistered)(conversation) &&
    // All conversation should have a title except in weird cases where
    // they don't, in that case we don't want to show these for Forwarding.
    conversation.title &&
    hasDisplayInfo(conversation)));
/**
 * getComposableContacts/getCandidateContactsForNewGroup both return contacts for the
 * composer and group members, a different list from your primary system contacts.
 * This list may include false positives, which is better than missing contacts.
 *
 * Note: the key difference between them:
 *   getComposableContacts includes Note to Self
 *   getCandidateContactsForNewGroup does not include Note to Self
 *
 * Because they filter unregistered contacts and that's (partially) determined by the
 * current time, it's possible for them to return stale contacts that have unregistered
 * if no other conversations change. This should be a rare false positive.
 */
exports.getComposableContacts = (0, reselect_1.createSelector)(exports.getConversationLookup, (conversationLookup) => Object.values(conversationLookup).filter(conversation => conversation.type === 'direct' && canComposeConversation(conversation)));
exports.getCandidateContactsForNewGroup = (0, reselect_1.createSelector)(exports.getConversationLookup, (conversationLookup) => Object.values(conversationLookup).filter(conversation => conversation.type === 'direct' &&
    !conversation.isMe &&
    canComposeConversation(conversation)));
exports.getComposableGroups = (0, reselect_1.createSelector)(exports.getConversationLookup, (conversationLookup) => Object.values(conversationLookup).filter(conversation => conversation.type === 'group' && canComposeConversation(conversation)));
const getNormalizedComposerConversationSearchTerm = (0, reselect_1.createSelector)(exports.getComposerConversationSearchTerm, (searchTerm) => searchTerm.trim());
exports.getFilteredComposeContacts = (0, reselect_1.createSelector)(getNormalizedComposerConversationSearchTerm, exports.getComposableContacts, (searchTerm, contacts) => {
    return (0, filterAndSortConversations_1.filterAndSortConversationsByTitle)(contacts, searchTerm);
});
exports.getFilteredComposeGroups = (0, reselect_1.createSelector)(getNormalizedComposerConversationSearchTerm, exports.getComposableGroups, (searchTerm, groups) => {
    return (0, filterAndSortConversations_1.filterAndSortConversationsByTitle)(groups, searchTerm);
});
exports.getFilteredCandidateContactsForNewGroup = (0, reselect_1.createSelector)(exports.getCandidateContactsForNewGroup, getNormalizedComposerConversationSearchTerm, filterAndSortConversations_1.filterAndSortConversationsByTitle);
exports.getCantAddContactForModal = (0, reselect_1.createSelector)(exports.getConversationLookup, getComposerState, (conversationLookup, composerState) => {
    if ((composerState === null || composerState === void 0 ? void 0 : composerState.step) !== conversationsEnums_1.ComposerStep.ChooseGroupMembers) {
        return undefined;
    }
    const conversationId = composerState.cantAddContactIdForModal;
    if (!conversationId) {
        return undefined;
    }
    const result = (0, getOwn_1.getOwn)(conversationLookup, conversationId);
    (0, assert_1.assert)(result, 'getCantAddContactForModal: failed to look up conversation by ID; returning undefined');
    return result;
});
const getGroupCreationComposerState = (0, reselect_1.createSelector)(getComposerState, (composerState) => {
    switch (composerState === null || composerState === void 0 ? void 0 : composerState.step) {
        case conversationsEnums_1.ComposerStep.ChooseGroupMembers:
        case conversationsEnums_1.ComposerStep.SetGroupMetadata:
            return composerState;
        default:
            (0, assert_1.assert)(false, 'getSetGroupMetadataComposerState: expected step to be SetGroupMetadata');
            return {
                groupName: '',
                groupAvatar: undefined,
                groupExpireTimer: 0,
                selectedConversationIds: [],
            };
    }
});
exports.getComposeGroupAvatar = (0, reselect_1.createSelector)(getGroupCreationComposerState, (composerState) => composerState.groupAvatar);
exports.getComposeGroupName = (0, reselect_1.createSelector)(getGroupCreationComposerState, (composerState) => composerState.groupName);
exports.getComposeGroupExpireTimer = (0, reselect_1.createSelector)(getGroupCreationComposerState, (composerState) => composerState.groupExpireTimer);
exports.getComposeSelectedContacts = (0, reselect_1.createSelector)(exports.getConversationLookup, getGroupCreationComposerState, (conversationLookup, composerState) => (0, deconstructLookup_1.deconstructLookup)(conversationLookup, composerState.selectedConversationIds));
// This is where we will put Conversation selector logic, replicating what
// is currently in models/conversation.getProps()
// What needs to happen to pull that selector logic here?
//   1) contactTypingTimers - that UI-only state needs to be moved to redux
//   2) all of the message selectors need to be reselect-based; today those
//      Backbone-based prop-generation functions expect to get Conversation information
//      directly via ConversationController
function _conversationSelector(conversation
// regionCode: string,
// userNumber: string
) {
    if (conversation) {
        return conversation;
    }
    return (0, exports.getPlaceholderContact)();
}
exports._conversationSelector = _conversationSelector;
exports.getCachedSelectorForConversation = (0, reselect_1.createSelector)(user_1.getRegionCode, user_1.getUserNumber, () => {
    // Note: memoizee will check all parameters provided, and only run our selector
    //   if any of them have changed.
    return (0, memoizee_1.default)(_conversationSelector, { max: 2000 });
});
exports.getConversationSelector = (0, reselect_1.createSelector)(exports.getCachedSelectorForConversation, exports.getConversationLookup, exports.getConversationsByUuid, exports.getConversationsByE164, exports.getConversationsByGroupId, (selector, byId, byUuid, byE164, byGroupId) => {
    return (id) => {
        if (!id) {
            log.warn(`getConversationSelector: Called with a falsey id ${id}`);
            // This will return a placeholder contact
            return selector(undefined);
        }
        const onUuid = (0, getOwn_1.getOwn)(byUuid, id.toLowerCase ? id.toLowerCase() : id);
        if (onUuid) {
            return selector(onUuid);
        }
        const onE164 = (0, getOwn_1.getOwn)(byE164, id);
        if (onE164) {
            return selector(onE164);
        }
        const onGroupId = (0, getOwn_1.getOwn)(byGroupId, id);
        if (onGroupId) {
            return selector(onGroupId);
        }
        const onId = (0, getOwn_1.getOwn)(byId, id);
        if (onId) {
            return selector(onId);
        }
        log.warn(`getConversationSelector: No conversation found for id ${id}`);
        // This will return a placeholder contact
        return selector(undefined);
    };
});
exports.getConversationByIdSelector = (0, reselect_1.createSelector)(exports.getConversationLookup, conversationLookup => (id) => (0, getOwn_1.getOwn)(conversationLookup, id));
exports.getConversationByUuidSelector = (0, reselect_1.createSelector)(exports.getConversationsByUuid, conversationsByUuid => (uuid) => (0, getOwn_1.getOwn)(conversationsByUuid, uuid));
// A little optimization to reset our selector cache whenever high-level application data
//   changes: regionCode and userNumber.
exports.getCachedSelectorForMessage = (0, reselect_1.createSelector)(user_1.getRegionCode, user_1.getUserNumber, () => {
    // Note: memoizee will check all parameters provided, and only run our selector
    //   if any of them have changed.
    return (0, memoizee_1.default)(message_1.getPropsForBubble, { max: 2000 });
});
const getCachedConversationMemberColorsSelector = (0, reselect_1.createSelector)(exports.getConversationSelector, user_1.getUserConversationId, (conversationSelector, ourConversationId) => {
    return (0, memoizee_1.default)((conversationId) => {
        const contactNameColors = new Map();
        const { sortedGroupMembers = [], type, id: theirId, } = conversationSelector(conversationId);
        if (type === 'direct') {
            contactNameColors.set(ourConversationId, Colors_1.ContactNameColors[0]);
            contactNameColors.set(theirId, Colors_1.ContactNameColors[0]);
            return contactNameColors;
        }
        [...sortedGroupMembers]
            .sort((left, right) => String(left.uuid) > String(right.uuid) ? 1 : -1)
            .forEach((member, i) => {
            contactNameColors.set(member.id, Colors_1.ContactNameColors[i % Colors_1.ContactNameColors.length]);
        });
        return contactNameColors;
    }, { max: 100 });
});
exports.getContactNameColorSelector = (0, reselect_1.createSelector)(getCachedConversationMemberColorsSelector, conversationMemberColorsSelector => {
    return (conversationId, contactId) => {
        const contactNameColors = conversationMemberColorsSelector(conversationId);
        const color = contactNameColors.get(contactId);
        if (!color) {
            log.warn(`No color generated for contact ${contactId}`);
            return Colors_1.ContactNameColors[0];
        }
        return color;
    };
});
exports.getMessageSelector = (0, reselect_1.createSelector)(exports.getCachedSelectorForMessage, exports.getMessages, exports.getSelectedMessage, exports.getConversationSelector, badges_1.getPreferredBadgeSelector, user_1.getRegionCode, user_1.getUserNumber, user_1.getUserUuid, user_1.getUserConversationId, calling_1.getCallSelector, calling_1.getActiveCall, accounts_1.getAccountSelector, exports.getContactNameColorSelector, (messageSelector, messageLookup, selectedMessage, conversationSelector, preferredBadgeSelector, regionCode, ourNumber, ourUuid, ourConversationId, callSelector, activeCall, accountSelector, contactNameColorSelector) => {
    return (id) => {
        const message = messageLookup[id];
        if (!message) {
            return undefined;
        }
        return messageSelector(message, {
            conversationSelector,
            ourConversationId,
            ourNumber,
            ourUuid,
            preferredBadgeSelector,
            regionCode,
            selectedMessageId: selectedMessage === null || selectedMessage === void 0 ? void 0 : selectedMessage.id,
            selectedMessageCounter: selectedMessage === null || selectedMessage === void 0 ? void 0 : selectedMessage.counter,
            contactNameColorSelector,
            callSelector,
            activeCall,
            accountSelector,
        });
    };
});
function _conversationMessagesSelector(conversation) {
    const { heightChangeMessageIds, isLoadingMessages, isNearBottom, loadCountdownStart, messageIds, metrics, resetCounter, scrollToBottomCounter, scrollToMessageId, scrollToMessageCounter, } = conversation;
    const firstId = messageIds[0];
    const lastId = messageIds.length === 0 ? undefined : messageIds[messageIds.length - 1];
    const { oldestUnread } = metrics;
    const haveNewest = !metrics.newest || !lastId || lastId === metrics.newest.id;
    const haveOldest = !metrics.oldest || !firstId || firstId === metrics.oldest.id;
    const items = messageIds;
    const messageHeightChangeLookup = heightChangeMessageIds && heightChangeMessageIds.length
        ? (0, lodash_1.fromPairs)(heightChangeMessageIds.map(id => [id, true]))
        : null;
    const messageHeightChangeIndex = messageHeightChangeLookup
        ? messageIds.findIndex(id => messageHeightChangeLookup[id])
        : undefined;
    const oldestUnreadIndex = oldestUnread
        ? messageIds.findIndex(id => id === oldestUnread.id)
        : undefined;
    const scrollToIndex = scrollToMessageId
        ? messageIds.findIndex(id => id === scrollToMessageId)
        : undefined;
    const { totalUnread } = metrics;
    return {
        haveNewest,
        haveOldest,
        isLoadingMessages,
        loadCountdownStart,
        items,
        isNearBottom: isNearBottom || false,
        messageHeightChangeIndex: (0, lodash_1.isNumber)(messageHeightChangeIndex) && messageHeightChangeIndex >= 0
            ? messageHeightChangeIndex
            : undefined,
        oldestUnreadIndex: (0, lodash_1.isNumber)(oldestUnreadIndex) && oldestUnreadIndex >= 0
            ? oldestUnreadIndex
            : undefined,
        resetCounter,
        scrollToBottomCounter,
        scrollToIndex: (0, lodash_1.isNumber)(scrollToIndex) && scrollToIndex >= 0 ? scrollToIndex : undefined,
        scrollToIndexCounter: scrollToMessageCounter,
        totalUnread,
    };
}
exports._conversationMessagesSelector = _conversationMessagesSelector;
exports.getCachedSelectorForConversationMessages = (0, reselect_1.createSelector)(user_1.getRegionCode, user_1.getUserNumber, () => {
    // Note: memoizee will check all parameters provided, and only run our selector
    //   if any of them have changed.
    return (0, memoizee_1.default)(_conversationMessagesSelector, { max: 50 });
});
exports.getConversationMessagesSelector = (0, reselect_1.createSelector)(exports.getCachedSelectorForConversationMessages, exports.getMessagesByConversation, (conversationMessagesSelector, messagesByConversation) => {
    return (id) => {
        const conversation = messagesByConversation[id];
        if (!conversation) {
            // TODO: DESKTOP-2340
            return {
                haveNewest: false,
                haveOldest: false,
                isLoadingMessages: false,
                isNearBottom: false,
                items: [],
                loadCountdownStart: undefined,
                messageHeightChangeIndex: undefined,
                oldestUnreadIndex: undefined,
                resetCounter: 0,
                scrollToBottomCounter: 0,
                scrollToIndex: undefined,
                scrollToIndexCounter: 0,
                totalUnread: 0,
            };
        }
        return conversationMessagesSelector(conversation);
    };
});
exports.getInvitedContactsForNewlyCreatedGroup = (0, reselect_1.createSelector)(exports.getConversationsByUuid, exports.getConversations, (conversationLookup, { invitedUuidsForNewlyCreatedGroup = [] }) => (0, deconstructLookup_1.deconstructLookup)(conversationLookup, invitedUuidsForNewlyCreatedGroup));
exports.getConversationsWithCustomColorSelector = (0, reselect_1.createSelector)(getAllConversations, conversations => {
    return (colorId) => {
        return conversations.filter(conversation => conversation.customColorId === colorId);
    };
});
function isMissingRequiredProfileSharing(conversation) {
    const doesConversationRequireIt = !conversation.left &&
        ((0, whatTypeOfConversation_1.isGroupV1)(conversation) || (0, whatTypeOfConversation_1.isDirectConversation)(conversation));
    return Boolean(doesConversationRequireIt &&
        !conversation.profileSharing &&
        window.Signal.RemoteConfig.isEnabled('desktop.mandatoryProfileSharing') &&
        conversation.messageCount &&
        conversation.messageCount > 0);
}
exports.isMissingRequiredProfileSharing = isMissingRequiredProfileSharing;
exports.getGroupAdminsSelector = (0, reselect_1.createSelector)(exports.getConversationSelector, (conversationSelector) => {
    return (conversationId) => {
        const { groupId, groupVersion, memberships = [], } = conversationSelector(conversationId);
        if (!(0, whatTypeOfConversation_1.isGroupV2)({
            groupId,
            groupVersion,
        })) {
            return [];
        }
        const admins = [];
        memberships.forEach(membership => {
            if (membership.isAdmin) {
                const admin = conversationSelector(membership.uuid);
                admins.push(admin);
            }
        });
        return admins;
    };
});
const getOutboundMessagesPendingConversationVerification = (0, reselect_1.createSelector)(exports.getConversations, (conversations) => conversations.outboundMessagesPendingConversationVerification);
const getConversationIdsStoppingMessageSendBecauseOfVerification = (0, reselect_1.createSelector)(getOutboundMessagesPendingConversationVerification, (outboundMessagesPendingConversationVerification) => Object.keys(outboundMessagesPendingConversationVerification));
exports.getConversationsStoppingMessageSendBecauseOfVerification = (0, reselect_1.createSelector)(exports.getConversationByIdSelector, getConversationIdsStoppingMessageSendBecauseOfVerification, (conversationSelector, conversationIds) => {
    const conversations = conversationIds
        .map(conversationId => conversationSelector(conversationId))
        .filter(isNotNil_1.isNotNil);
    return (0, sortByTitle_1.sortByTitle)(conversations);
});
exports.getMessageIdsPendingBecauseOfVerification = (0, reselect_1.createSelector)(getOutboundMessagesPendingConversationVerification, (outboundMessagesPendingConversationVerification) => {
    const result = new Set();
    Object.values(outboundMessagesPendingConversationVerification).forEach(messageGroup => {
        messageGroup.forEach(messageId => {
            result.add(messageId);
        });
    });
    return result;
});
exports.getNumberOfMessagesPendingBecauseOfVerification = (0, reselect_1.createSelector)(exports.getMessageIdsPendingBecauseOfVerification, (messageIds) => messageIds.size);
