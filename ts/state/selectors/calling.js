"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getIncomingCall = exports.isInCall = exports.getActiveCall = exports.getCallSelector = exports.getCallsByConversation = exports.getActiveCallState = void 0;
const reselect_1 = require("reselect");
const calling_1 = require("../ducks/calling");
const user_1 = require("./user");
const getOwn_1 = require("../../util/getOwn");
const getCalling = (state) => state.calling;
exports.getActiveCallState = (0, reselect_1.createSelector)(getCalling, (state) => state.activeCallState);
exports.getCallsByConversation = (0, reselect_1.createSelector)(getCalling, (state) => state.callsByConversation);
exports.getCallSelector = (0, reselect_1.createSelector)(exports.getCallsByConversation, (callsByConversation) => (conversationId) => (0, getOwn_1.getOwn)(callsByConversation, conversationId));
exports.getActiveCall = (0, reselect_1.createSelector)(exports.getActiveCallState, exports.getCallSelector, (activeCallState, callSelector) => {
    if (activeCallState && activeCallState.conversationId) {
        return callSelector(activeCallState.conversationId);
    }
    return undefined;
});
exports.isInCall = (0, reselect_1.createSelector)(exports.getActiveCall, (call) => Boolean(call));
exports.getIncomingCall = (0, reselect_1.createSelector)(exports.getCallsByConversation, user_1.getUserUuid, (callsByConversation, ourUuid) => (0, calling_1.getIncomingCall)(callsByConversation, ourUuid));
