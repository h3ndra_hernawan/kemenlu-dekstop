"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.useRecentEmojis = exports.selectRecentEmojis = void 0;
const reselect_1 = require("reselect");
const react_redux_1 = require("react-redux");
const lib_1 = require("../../components/emoji/lib");
exports.selectRecentEmojis = (0, reselect_1.createSelector)(({ emojis }) => emojis.recents, recents => recents.filter(lib_1.isShortName));
const useRecentEmojis = () => (0, react_redux_1.useSelector)(exports.selectRecentEmojis);
exports.useRecentEmojis = useRecentEmojis;
