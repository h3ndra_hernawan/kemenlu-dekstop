"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getContactSafetyNumber = void 0;
const reselect_1 = require("reselect");
const getSafetyNumber = (state) => state.safetyNumber;
const getContactID = (_, props) => props.contactID;
exports.getContactSafetyNumber = (0, reselect_1.createSelector)([getSafetyNumber, getContactID], ({ contacts }, contactID) => contacts[contactID]);
