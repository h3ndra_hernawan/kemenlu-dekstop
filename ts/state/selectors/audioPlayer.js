"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isPaused = void 0;
const isPaused = (state) => {
    return state.audioPlayer.activeAudioID === undefined;
};
exports.isPaused = isPaused;
