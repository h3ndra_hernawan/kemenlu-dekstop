"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPreferredReactionEmoji = exports.getPreferredLeftPaneWidth = exports.getEmojiSkinTone = exports.getCustomColors = exports.getDefaultConversationColor = exports.getUsernamesEnabled = exports.getUniversalExpireTimer = exports.getPinnedConversationIds = exports.getUserAgent = exports.getItems = void 0;
const reselect_1 = require("reselect");
const lodash_1 = require("lodash");
const universalExpireTimer_1 = require("../../util/universalExpireTimer");
const Colors_1 = require("../../types/Colors");
const preferredReactionEmoji_1 = require("../../reactions/preferredReactionEmoji");
const DEFAULT_PREFERRED_LEFT_PANE_WIDTH = 320;
const getItems = (state) => state.items;
exports.getItems = getItems;
exports.getUserAgent = (0, reselect_1.createSelector)(exports.getItems, (state) => state.userAgent);
exports.getPinnedConversationIds = (0, reselect_1.createSelector)(exports.getItems, (state) => (state.pinnedConversationIds || []));
exports.getUniversalExpireTimer = (0, reselect_1.createSelector)(exports.getItems, (state) => state[universalExpireTimer_1.ITEM_NAME] || 0);
const getRemoteConfig = (0, reselect_1.createSelector)(exports.getItems, (state) => state.remoteConfig);
exports.getUsernamesEnabled = (0, reselect_1.createSelector)(getRemoteConfig, (remoteConfig) => { var _a; return Boolean((_a = remoteConfig === null || remoteConfig === void 0 ? void 0 : remoteConfig['desktop.usernames']) === null || _a === void 0 ? void 0 : _a.enabled); });
exports.getDefaultConversationColor = (0, reselect_1.createSelector)(exports.getItems, (state) => { var _a; return (_a = state.defaultConversationColor) !== null && _a !== void 0 ? _a : Colors_1.DEFAULT_CONVERSATION_COLOR; });
exports.getCustomColors = (0, reselect_1.createSelector)(exports.getItems, (state) => { var _a; return (_a = state.customColors) === null || _a === void 0 ? void 0 : _a.colors; });
exports.getEmojiSkinTone = (0, reselect_1.createSelector)(exports.getItems, ({ skinTone }) => typeof skinTone === 'number' &&
    (0, lodash_1.isInteger)(skinTone) &&
    skinTone >= 0 &&
    skinTone <= 5
    ? skinTone
    : 0);
exports.getPreferredLeftPaneWidth = (0, reselect_1.createSelector)(exports.getItems, ({ preferredLeftPaneWidth }) => typeof preferredLeftPaneWidth === 'number' &&
    Number.isInteger(preferredLeftPaneWidth)
    ? preferredLeftPaneWidth
    : DEFAULT_PREFERRED_LEFT_PANE_WIDTH);
exports.getPreferredReactionEmoji = (0, reselect_1.createSelector)(exports.getItems, exports.getEmojiSkinTone, (state, skinTone) => (0, preferredReactionEmoji_1.getPreferredReactionEmoji)(state.preferredReactionEmoji, skinTone));
