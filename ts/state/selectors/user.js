"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTheme = exports.getTempPath = exports.getPlatform = exports.getStickersPath = exports.getAttachmentsPath = exports.getInteractionMode = exports.getIntl = exports.getUserUuid = exports.getUserConversationId = exports.getRegionCode = exports.getUserDeviceId = exports.getUserNumber = exports.getUser = void 0;
const reselect_1 = require("reselect");
const getUser = (state) => state.user;
exports.getUser = getUser;
exports.getUserNumber = (0, reselect_1.createSelector)(exports.getUser, (state) => state.ourNumber);
exports.getUserDeviceId = (0, reselect_1.createSelector)(exports.getUser, (state) => state.ourDeviceId);
exports.getRegionCode = (0, reselect_1.createSelector)(exports.getUser, (state) => state.regionCode);
exports.getUserConversationId = (0, reselect_1.createSelector)(exports.getUser, (state) => state.ourConversationId);
exports.getUserUuid = (0, reselect_1.createSelector)(exports.getUser, (state) => state.ourUuid);
exports.getIntl = (0, reselect_1.createSelector)(exports.getUser, (state) => state.i18n);
exports.getInteractionMode = (0, reselect_1.createSelector)(exports.getUser, (state) => state.interactionMode);
exports.getAttachmentsPath = (0, reselect_1.createSelector)(exports.getUser, (state) => state.attachmentsPath);
exports.getStickersPath = (0, reselect_1.createSelector)(exports.getUser, (state) => state.stickersPath);
exports.getPlatform = (0, reselect_1.createSelector)(exports.getUser, (state) => state.platform);
exports.getTempPath = (0, reselect_1.createSelector)(exports.getUser, (state) => state.tempPath);
exports.getTheme = (0, reselect_1.createSelector)(exports.getUser, (state) => state.theme);
