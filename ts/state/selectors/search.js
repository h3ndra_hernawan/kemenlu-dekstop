"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMessageSearchResultSelector = exports.getCachedSelectorForMessageSearchResult = exports.getSearchResults = exports.getMessageSearchResultLookup = exports.isSearching = exports.getStartSearchCounter = exports.getSearchConversationName = exports.getSearchConversation = exports.getIsSearchingInAConversation = exports.getSelectedMessage = exports.getQuery = exports.getSearch = void 0;
const memoizee_1 = __importDefault(require("memoizee"));
const reselect_1 = require("reselect");
const deconstructLookup_1 = require("../../util/deconstructLookup");
const user_1 = require("./user");
const conversations_1 = require("./conversations");
const log = __importStar(require("../../logging/log"));
const getOwn_1 = require("../../util/getOwn");
const getSearch = (state) => state.search;
exports.getSearch = getSearch;
exports.getQuery = (0, reselect_1.createSelector)(exports.getSearch, (state) => state.query);
exports.getSelectedMessage = (0, reselect_1.createSelector)(exports.getSearch, (state) => state.selectedMessage);
const getSearchConversationId = (0, reselect_1.createSelector)(exports.getSearch, (state) => state.searchConversationId);
exports.getIsSearchingInAConversation = (0, reselect_1.createSelector)(getSearchConversationId, Boolean);
exports.getSearchConversation = (0, reselect_1.createSelector)(getSearchConversationId, conversations_1.getConversationLookup, (searchConversationId, conversationLookup) => searchConversationId
    ? (0, getOwn_1.getOwn)(conversationLookup, searchConversationId)
    : undefined);
exports.getSearchConversationName = (0, reselect_1.createSelector)(exports.getSearchConversation, user_1.getIntl, (conversation, i18n) => {
    if (!conversation) {
        return undefined;
    }
    return conversation.isMe ? i18n('noteToSelf') : conversation.title;
});
exports.getStartSearchCounter = (0, reselect_1.createSelector)(exports.getSearch, (state) => state.startSearchCounter);
exports.isSearching = (0, reselect_1.createSelector)(exports.getQuery, (query) => query.trim().length > 0);
exports.getMessageSearchResultLookup = (0, reselect_1.createSelector)(exports.getSearch, (state) => state.messageLookup);
exports.getSearchResults = (0, reselect_1.createSelector)([exports.getSearch, exports.getSearchConversationName, conversations_1.getConversationLookup], (state, searchConversationName, conversationLookup) => {
    const { contactIds, conversationIds, discussionsLoading, messageIds, messageLookup, messagesLoading, } = state;
    return {
        conversationResults: discussionsLoading
            ? { isLoading: true }
            : {
                isLoading: false,
                results: (0, deconstructLookup_1.deconstructLookup)(conversationLookup, conversationIds),
            },
        contactResults: discussionsLoading
            ? { isLoading: true }
            : {
                isLoading: false,
                results: (0, deconstructLookup_1.deconstructLookup)(conversationLookup, contactIds),
            },
        messageResults: messagesLoading
            ? { isLoading: true }
            : {
                isLoading: false,
                results: (0, deconstructLookup_1.deconstructLookup)(messageLookup, messageIds),
            },
        searchConversationName,
        searchTerm: state.query,
    };
});
exports.getCachedSelectorForMessageSearchResult = (0, reselect_1.createSelector)(user_1.getUserConversationId, conversations_1.getConversationSelector, (_, conversationSelector) => {
    // Note: memoizee will check all parameters provided, and only run our selector
    //   if any of them have changed.
    return (0, memoizee_1.default)((message, from, to, searchConversationId, selectedMessageId) => {
        const bodyRanges = message.bodyRanges || [];
        return {
            from,
            to,
            id: message.id,
            conversationId: message.conversationId,
            sentAt: message.sent_at,
            snippet: message.snippet || '',
            bodyRanges: bodyRanges.map((bodyRange) => {
                const conversation = conversationSelector(bodyRange.mentionUuid);
                return Object.assign(Object.assign({}, bodyRange), { replacementText: conversation.title });
            }),
            body: message.body || '',
            isSelected: Boolean(selectedMessageId && message.id === selectedMessageId),
            isSearchingInConversation: Boolean(searchConversationId),
        };
    }, { max: 500 });
});
exports.getMessageSearchResultSelector = (0, reselect_1.createSelector)(exports.getCachedSelectorForMessageSearchResult, exports.getMessageSearchResultLookup, exports.getSelectedMessage, conversations_1.getConversationSelector, getSearchConversationId, user_1.getUserConversationId, (messageSearchResultSelector, messageSearchResultLookup, selectedMessageId, conversationSelector, searchConversationId, ourConversationId) => {
    return (id) => {
        const message = messageSearchResultLookup[id];
        if (!message) {
            log.warn(`getMessageSearchResultSelector: messageSearchResultLookup was missing id ${id}`);
            return undefined;
        }
        const { conversationId, source, sourceUuid, type } = message;
        let from;
        let to;
        if (type === 'incoming') {
            from = conversationSelector(sourceUuid || source);
            to = conversationSelector(conversationId);
            if (from === to) {
                to = conversationSelector(ourConversationId);
            }
        }
        else if (type === 'outgoing') {
            from = conversationSelector(ourConversationId);
            to = conversationSelector(conversationId);
        }
        else {
            log.warn(`getMessageSearchResultSelector: Got unexpected type ${type}`);
            return undefined;
        }
        return messageSearchResultSelector(message, from, to, searchConversationId, selectedMessageId);
    };
});
