"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isChallengePending = exports.hasNetworkDialog = void 0;
const reselect_1 = require("reselect");
const registration_1 = require("../../util/registration");
const SocketStatus_1 = require("../../types/SocketStatus");
const getNetwork = (state) => state.network;
exports.hasNetworkDialog = (0, reselect_1.createSelector)(getNetwork, registration_1.isDone, ({ isOnline, socketStatus, withinConnectingGracePeriod }, isRegistrationDone) => isRegistrationDone &&
    (!isOnline ||
        (socketStatus === SocketStatus_1.SocketStatus.CONNECTING &&
            !withinConnectingGracePeriod) ||
        socketStatus === SocketStatus_1.SocketStatus.CLOSED ||
        socketStatus === SocketStatus_1.SocketStatus.CLOSING));
exports.isChallengePending = (0, reselect_1.createSelector)(getNetwork, ({ challengeStatus }) => challengeStatus === 'pending');
