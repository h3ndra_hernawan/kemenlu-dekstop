"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isPathInside_1 = require("../../util/isPathInside");
describe('isPathInside', () => {
    it('returns false if the child path is not inside the parent path', () => {
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('x', 'a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('a/b', '/a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/a/b', 'a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/x', '/a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/x/y', '/a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/a/x', '/a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/a/bad', '/a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/a/x', '/a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/a/b', '/a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/a/b/c/..', '/a/b'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/', '/'));
        chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('/x/..', '/'));
        if (process.platform === 'win32') {
            chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('C:\\a\\x\\y', 'C:\\a\\b'));
            chai_1.assert.isFalse((0, isPathInside_1.isPathInside)('D:\\a\\b\\c', 'C:\\a\\b'));
        }
    });
    it('returns true if the child path is inside the parent path', () => {
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('a/b/c', 'a/b'));
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('a/b/c/d', 'a/b'));
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('/a/b/c', '/a/b'));
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('/a/b/c', '/a/b/'));
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('/a/b/c/', '/a/b'));
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('/a/b/c/', '/a/b/'));
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('/a/b/c/d', '/a/b'));
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('/a/b/c/d/..', '/a/b'));
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('/x/y/z/z/y', '/'));
        chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('x/y/z/z/y', '/'));
        if (process.platform === 'win32') {
            chai_1.assert.isTrue((0, isPathInside_1.isPathInside)('C:\\a\\b\\c', 'C:\\a\\b'));
        }
    });
});
