"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const chai_1 = require("chai");
const MIME_1 = require("../../types/MIME");
const sniffImageMimeType_1 = require("../../util/sniffImageMimeType");
describe('sniffImageMimeType', () => {
    const fixture = (filename) => {
        const fixturePath = path.join(__dirname, '..', '..', '..', 'fixtures', filename);
        return fs.promises.readFile(fixturePath);
    };
    it('returns undefined for empty buffers', () => {
        chai_1.assert.isUndefined((0, sniffImageMimeType_1.sniffImageMimeType)(new Uint8Array()));
    });
    it('returns undefined for non-image files', async () => {
        await Promise.all(['pixabay-Soap-Bubble-7141.mp4', 'lorem-ipsum.txt'].map(async (filename) => {
            chai_1.assert.isUndefined((0, sniffImageMimeType_1.sniffImageMimeType)(await fixture(filename)));
        }));
    });
    it('sniffs ICO files', async () => {
        chai_1.assert.strictEqual((0, sniffImageMimeType_1.sniffImageMimeType)(await fixture('kitten-1-64-64.ico')), MIME_1.IMAGE_ICO);
    });
    it('sniffs BMP files', async () => {
        chai_1.assert.strictEqual((0, sniffImageMimeType_1.sniffImageMimeType)(await fixture('2x2.bmp')), MIME_1.IMAGE_BMP);
    });
    it('sniffs GIF files', async () => {
        chai_1.assert.strictEqual((0, sniffImageMimeType_1.sniffImageMimeType)(await fixture('giphy-GVNvOUpeYmI7e.gif')), MIME_1.IMAGE_GIF);
    });
    it('sniffs WEBP files', async () => {
        chai_1.assert.strictEqual((0, sniffImageMimeType_1.sniffImageMimeType)(await fixture('512x515-thumbs-up-lincoln.webp')), MIME_1.IMAGE_WEBP);
    });
    it('sniffs PNG files', async () => {
        await Promise.all([
            'freepngs-2cd43b_bed7d1327e88454487397574d87b64dc_mv2.png',
            'Animated_PNG_example_bouncing_beach_ball.png',
        ].map(async (filename) => {
            chai_1.assert.strictEqual((0, sniffImageMimeType_1.sniffImageMimeType)(await fixture(filename)), MIME_1.IMAGE_PNG);
        }));
    });
    it('sniffs JPEG files', async () => {
        chai_1.assert.strictEqual((0, sniffImageMimeType_1.sniffImageMimeType)(await fixture('kitten-1-64-64.jpg')), MIME_1.IMAGE_JPEG);
    });
});
