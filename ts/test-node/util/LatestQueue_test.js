"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const LatestQueue_1 = require("../../util/LatestQueue");
describe('LatestQueue', () => {
    it('if the queue is empty, new tasks are started immediately', done => {
        new LatestQueue_1.LatestQueue().add(async () => {
            done();
        });
    });
    it('only enqueues the latest operation', done => {
        const queue = new LatestQueue_1.LatestQueue();
        const spy = sinon.spy();
        let openFirstTaskGate;
        const firstTaskGate = new Promise(resolve => {
            openFirstTaskGate = resolve;
        });
        if (!openFirstTaskGate) {
            throw new Error('Test is misconfigured; cannot grab inner resolve');
        }
        queue.add(async () => {
            await firstTaskGate;
            spy('first');
        });
        queue.add(async () => {
            spy('second');
        });
        queue.add(async () => {
            spy('third');
        });
        sinon.assert.notCalled(spy);
        openFirstTaskGate();
        queue.onceEmpty(() => {
            sinon.assert.calledTwice(spy);
            sinon.assert.calledWith(spy, 'first');
            sinon.assert.calledWith(spy, 'third');
            done();
        });
    });
});
