"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const path = __importStar(require("path"));
const os = __importStar(require("os"));
const fs = __importStar(require("fs"));
const fse = __importStar(require("fs-extra"));
const Sinon = __importStar(require("sinon"));
const helpers_1 = require("../helpers");
const windowsZoneIdentifier_1 = require("../../util/windowsZoneIdentifier");
describe('writeWindowsZoneIdentifier', () => {
    before(function thisNeeded() {
        if (process.platform !== 'win32') {
            this.skip();
        }
    });
    beforeEach(async function thisNeeded() {
        this.sandbox = Sinon.createSandbox();
        this.tmpdir = await fs.promises.mkdtemp(path.join(os.tmpdir(), 'signal-test-'));
    });
    afterEach(async function thisNeeded() {
        this.sandbox.restore();
        await fse.remove(this.tmpdir);
    });
    it('writes zone transfer ID 3 (internet) to the Zone.Identifier file', async function thisNeeded() {
        const file = path.join(this.tmpdir, 'file.txt');
        await fse.outputFile(file, 'hello');
        await (0, windowsZoneIdentifier_1.writeWindowsZoneIdentifier)(file);
        chai_1.assert.strictEqual(await fs.promises.readFile(`${file}:Zone.Identifier`, 'utf8'), '[ZoneTransfer]\r\nZoneId=3');
    });
    it('fails if there is an existing Zone.Identifier file', async function thisNeeded() {
        const file = path.join(this.tmpdir, 'file.txt');
        await fse.outputFile(file, 'hello');
        await fs.promises.writeFile(`${file}:Zone.Identifier`, '# already here');
        await (0, helpers_1.assertRejects)(() => (0, windowsZoneIdentifier_1.writeWindowsZoneIdentifier)(file));
    });
    it('fails if the original file does not exist', async function thisNeeded() {
        const file = path.join(this.tmpdir, 'file-never-created.txt');
        await (0, helpers_1.assertRejects)(() => (0, windowsZoneIdentifier_1.writeWindowsZoneIdentifier)(file));
    });
    it('fails if not on Windows', async function thisNeeded() {
        this.sandbox.stub(process, 'platform').get(() => 'darwin');
        const file = path.join(this.tmpdir, 'file.txt');
        await fse.outputFile(file, 'hello');
        await (0, helpers_1.assertRejects)(() => (0, windowsZoneIdentifier_1.writeWindowsZoneIdentifier)(file));
    });
});
