"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const toggleMaximizedBrowserWindow_1 = require("../../util/toggleMaximizedBrowserWindow");
describe('toggleMaximizedBrowserWindow', () => {
    const createFakeWindow = () => ({
        isMaximized: sinon.stub(),
        unmaximize: sinon.spy(),
        maximize: sinon.spy(),
    });
    it('maximizes an unmaximized window', () => {
        const browserWindow = createFakeWindow();
        browserWindow.isMaximized.returns(false);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (0, toggleMaximizedBrowserWindow_1.toggleMaximizedBrowserWindow)(browserWindow);
        sinon.assert.calledOnce(browserWindow.maximize);
        sinon.assert.notCalled(browserWindow.unmaximize);
    });
    it('unmaximizes a maximized window', () => {
        const browserWindow = createFakeWindow();
        browserWindow.isMaximized.returns(true);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (0, toggleMaximizedBrowserWindow_1.toggleMaximizedBrowserWindow)(browserWindow);
        sinon.assert.notCalled(browserWindow.maximize);
        sinon.assert.calledOnce(browserWindow.unmaximize);
    });
});
