"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const ringrtc_1 = require("ringrtc");
const protobuf_1 = require("../../protobuf");
const callingMessageToProto_1 = require("../../util/callingMessageToProto");
describe('callingMessageToProto', () => {
    // NOTE: These tests are incomplete.
    describe('hangup field', () => {
        it('leaves the field unset if `hangup` is not provided', () => {
            const result = (0, callingMessageToProto_1.callingMessageToProto)(new ringrtc_1.CallingMessage());
            chai_1.assert.isUndefined(result.hangup);
        });
        it('attaches the type if provided', () => {
            var _a;
            const callingMessage = new ringrtc_1.CallingMessage();
            callingMessage.hangup = new ringrtc_1.HangupMessage();
            callingMessage.hangup.type = ringrtc_1.HangupType.Busy;
            const result = (0, callingMessageToProto_1.callingMessageToProto)(callingMessage);
            chai_1.assert.strictEqual((_a = result.hangup) === null || _a === void 0 ? void 0 : _a.type, 3);
        });
    });
    describe('opaque field', () => {
        it('leaves the field unset if neither `opaque` nor urgency are provided', () => {
            const result = (0, callingMessageToProto_1.callingMessageToProto)(new ringrtc_1.CallingMessage());
            chai_1.assert.isUndefined(result.opaque);
        });
        it('attaches opaque data', () => {
            var _a;
            const callingMessage = new ringrtc_1.CallingMessage();
            callingMessage.opaque = new ringrtc_1.OpaqueMessage();
            callingMessage.opaque.data = Buffer.from([1, 2, 3]);
            const result = (0, callingMessageToProto_1.callingMessageToProto)(callingMessage);
            chai_1.assert.deepEqual((_a = result.opaque) === null || _a === void 0 ? void 0 : _a.data, new Uint8Array([1, 2, 3]));
        });
        it('attaches urgency if provided', () => {
            var _a, _b;
            const droppableResult = (0, callingMessageToProto_1.callingMessageToProto)(new ringrtc_1.CallingMessage(), ringrtc_1.CallMessageUrgency.Droppable);
            chai_1.assert.deepEqual((_a = droppableResult.opaque) === null || _a === void 0 ? void 0 : _a.urgency, protobuf_1.SignalService.CallingMessage.Opaque.Urgency.DROPPABLE);
            const urgentResult = (0, callingMessageToProto_1.callingMessageToProto)(new ringrtc_1.CallingMessage(), ringrtc_1.CallMessageUrgency.HandleImmediately);
            chai_1.assert.deepEqual((_b = urgentResult.opaque) === null || _b === void 0 ? void 0 : _b.urgency, protobuf_1.SignalService.CallingMessage.Opaque.Urgency.HANDLE_IMMEDIATELY);
        });
        it('attaches urgency and opaque data if both are provided', () => {
            var _a, _b;
            const callingMessage = new ringrtc_1.CallingMessage();
            callingMessage.opaque = new ringrtc_1.OpaqueMessage();
            callingMessage.opaque.data = Buffer.from([1, 2, 3]);
            const result = (0, callingMessageToProto_1.callingMessageToProto)(callingMessage, ringrtc_1.CallMessageUrgency.HandleImmediately);
            chai_1.assert.deepEqual((_a = result.opaque) === null || _a === void 0 ? void 0 : _a.data, new Uint8Array([1, 2, 3]));
            chai_1.assert.deepEqual((_b = result.opaque) === null || _b === void 0 ? void 0 : _b.urgency, protobuf_1.SignalService.CallingMessage.Opaque.Urgency.HANDLE_IMMEDIATELY);
        });
    });
});
