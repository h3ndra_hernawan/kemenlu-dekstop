"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon_1 = require("sinon");
const sleep_1 = require("../../util/sleep");
describe('sleep', () => {
    beforeEach(function beforeEach() {
        // This isn't a hook.
        // eslint-disable-next-line react-hooks/rules-of-hooks
        this.clock = (0, sinon_1.useFakeTimers)();
    });
    afterEach(function afterEach() {
        this.clock.restore();
    });
    it('returns a promise that resolves after the specified number of milliseconds', async function test() {
        let isDone = false;
        (async () => {
            await (0, sleep_1.sleep)(123);
            isDone = true;
        })();
        chai_1.assert.isFalse(isDone);
        await this.clock.tickAsync(100);
        chai_1.assert.isFalse(isDone);
        await this.clock.tickAsync(25);
        chai_1.assert.isTrue(isDone);
    });
});
