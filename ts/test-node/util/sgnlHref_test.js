"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon_1 = __importDefault(require("sinon"));
const sgnlHref_1 = require("../../util/sgnlHref");
function shouldNeverBeCalled() {
    chai_1.assert.fail('This should never be called');
}
const explodingLogger = {
    fatal: shouldNeverBeCalled,
    error: shouldNeverBeCalled,
    warn: shouldNeverBeCalled,
    info: shouldNeverBeCalled,
    debug: shouldNeverBeCalled,
    trace: shouldNeverBeCalled,
};
describe('sgnlHref', () => {
    [
        { protocol: 'sgnl', check: sgnlHref_1.isSgnlHref, name: 'isSgnlHref' },
        { protocol: 'signalcaptcha', check: sgnlHref_1.isCaptchaHref, name: 'isCaptchaHref' },
    ].forEach(({ protocol, check, name }) => {
        describe(name, () => {
            it('returns false for non-strings', () => {
                const logger = Object.assign(Object.assign({}, explodingLogger), { warn: sinon_1.default.spy() });
                const castToString = (value) => value;
                chai_1.assert.isFalse(check(castToString(undefined), logger));
                chai_1.assert.isFalse(check(castToString(null), logger));
                chai_1.assert.isFalse(check(castToString(123), logger));
                sinon_1.default.assert.calledThrice(logger.warn);
            });
            it('returns false for invalid URLs', () => {
                chai_1.assert.isFalse(check('', explodingLogger));
                chai_1.assert.isFalse(check(protocol, explodingLogger));
                chai_1.assert.isFalse(check(`${protocol}://::`, explodingLogger));
            });
            it(`returns false if the protocol is not "${protocol}:"`, () => {
                chai_1.assert.isFalse(check('https://example', explodingLogger));
                chai_1.assert.isFalse(check('https://signal.art/addstickers/?pack_id=abc', explodingLogger));
                chai_1.assert.isFalse(check('signal://example', explodingLogger));
            });
            it(`returns true if the protocol is "${protocol}:"`, () => {
                chai_1.assert.isTrue(check(`${protocol}://`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}://example`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}://example.com`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol.toUpperCase()}://example`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}://example?foo=bar`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}://example/`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}://example#`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}:foo`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}://user:pass@example`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}://example.com:1234`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}://example.com/extra/path/data`, explodingLogger));
                chai_1.assert.isTrue(check(`${protocol}://example/?foo=bar#hash`, explodingLogger));
            });
            it('accepts URL objects', () => {
                const invalid = new URL('https://example.com');
                chai_1.assert.isFalse(check(invalid, explodingLogger));
                const valid = new URL(`${protocol}://example`);
                chai_1.assert.isTrue(check(valid, explodingLogger));
            });
        });
    });
    describe('isSignalHttpsLink', () => {
        it('returns false for non-strings', () => {
            const logger = Object.assign(Object.assign({}, explodingLogger), { warn: sinon_1.default.spy() });
            const castToString = (value) => value;
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)(castToString(undefined), logger));
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)(castToString(null), logger));
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)(castToString(123), logger));
            sinon_1.default.assert.calledThrice(logger.warn);
        });
        it('returns false for invalid URLs', () => {
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('', explodingLogger));
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('https', explodingLogger));
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('https://::', explodingLogger));
        });
        it('returns false if the protocol is not "https:"', () => {
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('sgnl://signal.art', explodingLogger));
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('sgnl://signal.art/addstickers/?pack_id=abc', explodingLogger));
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('signal://signal.group', explodingLogger));
        });
        it('returns false if the URL is not a valid Signal URL', () => {
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('https://signal.org', explodingLogger));
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('https://example.com', explodingLogger));
        });
        it('returns true if the protocol is "https:"', () => {
            chai_1.assert.isTrue((0, sgnlHref_1.isSignalHttpsLink)('https://signal.group', explodingLogger));
            chai_1.assert.isTrue((0, sgnlHref_1.isSignalHttpsLink)('https://signal.art', explodingLogger));
            chai_1.assert.isTrue((0, sgnlHref_1.isSignalHttpsLink)('HTTPS://signal.art', explodingLogger));
            chai_1.assert.isTrue((0, sgnlHref_1.isSignalHttpsLink)('https://signal.me', explodingLogger));
        });
        it('returns false if username or password are set', () => {
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('https://user:password@signal.group', explodingLogger));
        });
        it('returns false if port is set', () => {
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)('https://signal.group:1234', explodingLogger));
        });
        it('accepts URL objects', () => {
            const invalid = new URL('sgnl://example.com');
            chai_1.assert.isFalse((0, sgnlHref_1.isSignalHttpsLink)(invalid, explodingLogger));
            const valid = new URL('https://signal.art');
            chai_1.assert.isTrue((0, sgnlHref_1.isSignalHttpsLink)(valid, explodingLogger));
        });
    });
    describe('parseSgnlHref', () => {
        it('returns a null command for invalid URLs', () => {
            ['', 'sgnl', 'https://example/?foo=bar'].forEach(href => {
                chai_1.assert.deepEqual((0, sgnlHref_1.parseSgnlHref)(href, explodingLogger), {
                    command: null,
                    args: new Map(),
                    hash: undefined,
                });
            });
        });
        it('parses the command for URLs with no arguments', () => {
            [
                'sgnl://foo',
                'sgnl://foo/',
                'sgnl://foo?',
                'SGNL://foo?',
                'sgnl://user:pass@foo',
                'sgnl://foo/path/data',
            ].forEach(href => {
                chai_1.assert.deepEqual((0, sgnlHref_1.parseSgnlHref)(href, explodingLogger), {
                    command: 'foo',
                    args: new Map(),
                    hash: undefined,
                });
            });
        });
        it("parses a command's arguments", () => {
            chai_1.assert.deepEqual((0, sgnlHref_1.parseSgnlHref)('sgnl://Foo?bar=baz&qux=Quux&num=123&empty=&encoded=hello%20world', explodingLogger), {
                command: 'Foo',
                args: new Map([
                    ['bar', 'baz'],
                    ['qux', 'Quux'],
                    ['num', '123'],
                    ['empty', ''],
                    ['encoded', 'hello world'],
                ]),
                hash: undefined,
            });
        });
        it('treats the port as part of the command', () => {
            chai_1.assert.propertyVal((0, sgnlHref_1.parseSgnlHref)('sgnl://foo:1234', explodingLogger), 'command', 'foo:1234');
        });
        it('ignores duplicate query parameters', () => {
            chai_1.assert.deepPropertyVal((0, sgnlHref_1.parseSgnlHref)('sgnl://x?foo=bar&foo=totally-ignored', explodingLogger), 'args', new Map([['foo', 'bar']]));
        });
        it('includes hash', () => {
            [
                'sgnl://foo?bar=baz#somehash',
                'sgnl://user:pass@foo?bar=baz#somehash',
            ].forEach(href => {
                chai_1.assert.deepEqual((0, sgnlHref_1.parseSgnlHref)(href, explodingLogger), {
                    command: 'foo',
                    args: new Map([['bar', 'baz']]),
                    hash: 'somehash',
                });
            });
        });
        it('ignores other parts of the URL', () => {
            [
                'sgnl://foo?bar=baz',
                'sgnl://foo/?bar=baz',
                'sgnl://foo/lots/of/path?bar=baz',
                'sgnl://user:pass@foo?bar=baz',
            ].forEach(href => {
                chai_1.assert.deepEqual((0, sgnlHref_1.parseSgnlHref)(href, explodingLogger), {
                    command: 'foo',
                    args: new Map([['bar', 'baz']]),
                    hash: undefined,
                });
            });
        });
        it("doesn't do anything fancy with arrays or objects in the query string", () => {
            // The `qs` module does things like this, which we don't want.
            chai_1.assert.deepPropertyVal((0, sgnlHref_1.parseSgnlHref)('sgnl://x?foo[]=bar&foo[]=baz', explodingLogger), 'args', new Map([['foo[]', 'bar']]));
            chai_1.assert.deepPropertyVal((0, sgnlHref_1.parseSgnlHref)('sgnl://x?foo[bar][baz]=foobarbaz', explodingLogger), 'args', new Map([['foo[bar][baz]', 'foobarbaz']]));
        });
    });
    describe('parseCaptchaHref', () => {
        it('throws on invalid URLs', () => {
            ['', 'sgnl', 'https://example/?foo=bar'].forEach(href => {
                chai_1.assert.throws(() => (0, sgnlHref_1.parseCaptchaHref)(href, explodingLogger), 'Not a captcha href');
            });
        });
        it('parses the command for URLs with no arguments', () => {
            [
                'signalcaptcha://foo',
                'signalcaptcha://foo?x=y',
                'signalcaptcha://a:b@foo?x=y',
                'signalcaptcha://foo#hash',
                'signalcaptcha://foo/',
            ].forEach(href => {
                chai_1.assert.deepEqual((0, sgnlHref_1.parseCaptchaHref)(href, explodingLogger), {
                    captcha: 'foo',
                });
            });
        });
    });
    describe('parseE164FromSignalDotMeHash', () => {
        it('returns undefined for invalid inputs', () => {
            [
                '',
                ' p/+18885551234',
                'p/+18885551234 ',
                'x/+18885551234',
                'p/+notanumber',
                'p/7c7e87a0-3b74-4efd-9a00-6eb8b1dd5be8',
                'p/+08885551234',
                'p/18885551234',
            ].forEach(hash => {
                chai_1.assert.isUndefined((0, sgnlHref_1.parseE164FromSignalDotMeHash)(hash));
            });
        });
        it('returns the E164 for valid inputs', () => {
            chai_1.assert.strictEqual((0, sgnlHref_1.parseE164FromSignalDotMeHash)('p/+18885551234'), '+18885551234');
            chai_1.assert.strictEqual((0, sgnlHref_1.parseE164FromSignalDotMeHash)('p/+441632960104'), '+441632960104');
        });
    });
    describe('parseSignalHttpsLink', () => {
        it('returns a null command for invalid URLs', () => {
            ['', 'https', 'https://example/?foo=bar'].forEach(href => {
                chai_1.assert.deepEqual((0, sgnlHref_1.parseSignalHttpsLink)(href, explodingLogger), {
                    command: null,
                    args: new Map(),
                    hash: undefined,
                });
            });
        });
        it('handles signal.art links', () => {
            chai_1.assert.deepEqual((0, sgnlHref_1.parseSignalHttpsLink)('https://signal.art/addstickers/#pack_id=baz&pack_key=Quux&num=123&empty=&encoded=hello%20world', explodingLogger), {
                command: 'addstickers',
                args: new Map([
                    ['pack_id', 'baz'],
                    ['pack_key', 'Quux'],
                    ['num', '123'],
                    ['empty', ''],
                    ['encoded', 'hello world'],
                ]),
                hash: 'pack_id=baz&pack_key=Quux&num=123&empty=&encoded=hello%20world',
            });
        });
        it('handles signal.group links', () => {
            chai_1.assert.deepEqual((0, sgnlHref_1.parseSignalHttpsLink)('https://signal.group/#data', explodingLogger), {
                command: 'signal.group',
                args: new Map(),
                hash: 'data',
            });
        });
        it('handles signal.me links', () => {
            chai_1.assert.deepEqual((0, sgnlHref_1.parseSignalHttpsLink)('https://signal.me/#p/+18885551234', explodingLogger), {
                command: 'signal.me',
                args: new Map(),
                hash: 'p/+18885551234',
            });
        });
    });
});
