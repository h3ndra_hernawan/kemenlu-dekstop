"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sortByTitle_1 = require("../../util/sortByTitle");
describe('sortByTitle', () => {
    it("does nothing to arrays that don't need to be sorted", () => {
        chai_1.assert.deepEqual((0, sortByTitle_1.sortByTitle)([]), []);
        chai_1.assert.deepEqual((0, sortByTitle_1.sortByTitle)([{ title: 'foo' }]), [{ title: 'foo' }]);
    });
    it('sorts the array by title', () => {
        // Because the function relies on locale-aware comparisons, we don't have very
        //   thorough tests here, as it can change based on platform.
        chai_1.assert.deepEqual((0, sortByTitle_1.sortByTitle)([{ title: 'foo' }, { title: 'bar' }]), [
            { title: 'bar' },
            { title: 'foo' },
        ]);
    });
    it("doesn't mutate its argument", () => {
        const arr = [{ title: 'foo' }, { title: 'bar' }];
        (0, sortByTitle_1.sortByTitle)(arr);
        chai_1.assert.deepEqual(arr, [{ title: 'foo' }, { title: 'bar' }]);
    });
});
