"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const chai_1 = require("chai");
const getAnimatedPngDataIfExists_1 = require("../../util/getAnimatedPngDataIfExists");
describe('getAnimatedPngDataIfExists', () => {
    const fixture = (filename) => {
        const fixturePath = path.join(__dirname, '..', '..', '..', 'fixtures', filename);
        return fs.promises.readFile(fixturePath);
    };
    it('returns null for empty buffers', () => {
        chai_1.assert.isNull((0, getAnimatedPngDataIfExists_1.getAnimatedPngDataIfExists)(Buffer.alloc(0)));
    });
    it('returns null for non-PNG files', async () => {
        await Promise.all([
            'kitten-1-64-64.jpg',
            '512x515-thumbs-up-lincoln.webp',
            'giphy-GVNvOUpeYmI7e.gif',
            'pixabay-Soap-Bubble-7141.mp4',
            'lorem-ipsum.txt',
        ].map(async (filename) => {
            chai_1.assert.isNull((0, getAnimatedPngDataIfExists_1.getAnimatedPngDataIfExists)(await fixture(filename)));
        }));
    });
    it('returns null for non-animated PNG files', async () => {
        chai_1.assert.isNull((0, getAnimatedPngDataIfExists_1.getAnimatedPngDataIfExists)(await fixture('20x200-yellow.png')));
    });
    it('returns data for animated PNG files', async () => {
        chai_1.assert.deepEqual((0, getAnimatedPngDataIfExists_1.getAnimatedPngDataIfExists)(await fixture('Animated_PNG_example_bouncing_beach_ball.png')), { numPlays: Infinity });
        chai_1.assert.deepEqual((0, getAnimatedPngDataIfExists_1.getAnimatedPngDataIfExists)(await fixture('apng_with_2_plays.png')), { numPlays: 2 });
    });
});
