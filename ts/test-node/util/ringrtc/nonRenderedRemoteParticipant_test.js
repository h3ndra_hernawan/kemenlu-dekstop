"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const nonRenderedRemoteParticipant_1 = require("../../../util/ringrtc/nonRenderedRemoteParticipant");
describe('nonRenderedRemoteParticipant', () => {
    it('returns a video request object a width and height of 0', () => {
        chai_1.assert.deepEqual((0, nonRenderedRemoteParticipant_1.nonRenderedRemoteParticipant)({ demuxId: 123 }), {
            demuxId: 123,
            width: 0,
            height: 0,
        });
    });
});
