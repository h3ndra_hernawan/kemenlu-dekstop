"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const normalizeGroupCallTimestamp_1 = require("../../../util/ringrtc/normalizeGroupCallTimestamp");
describe('normalizeGroupCallTimestamp', () => {
    it('returns undefined if passed NaN', () => {
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(NaN));
    });
    it('returns undefined if passed 0', () => {
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(0));
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(-0));
    });
    it('returns undefined if passed a negative number', () => {
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(-1));
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(-123));
    });
    it('returns undefined if passed a string that cannot be parsed as a number', () => {
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(''));
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)('uhhh'));
    });
    it('returns undefined if passed a BigInt of 0', () => {
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(BigInt(0)));
    });
    it('returns undefined if passed a negative BigInt', () => {
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(BigInt(-1)));
        chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(BigInt(-123)));
    });
    it('returns undefined if passed a non-parseable type', () => {
        [
            undefined,
            null,
            {},
            [],
            [123],
            Symbol('123'),
            { [Symbol.toPrimitive]: () => 123 },
            // eslint-disable-next-line no-new-wrappers
            new Number(123),
        ].forEach(value => {
            chai_1.assert.isUndefined((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(value));
        });
    });
    it('returns positive numbers passed in', () => {
        chai_1.assert.strictEqual((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(1), 1);
        chai_1.assert.strictEqual((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(123), 123);
    });
    it('parses strings as numbers', () => {
        chai_1.assert.strictEqual((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)('1'), 1);
        chai_1.assert.strictEqual((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)('123'), 123);
    });
    it('only parses the first 15 characters of a string', () => {
        chai_1.assert.strictEqual((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)('12345678901234567890123456789'), 123456789012345);
    });
    it('converts positive BigInts to numbers', () => {
        chai_1.assert.strictEqual((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(BigInt(1)), 1);
        chai_1.assert.strictEqual((0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(BigInt(123)), 123);
    });
});
