"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const getUserAgent_1 = require("../../util/getUserAgent");
describe('getUserAgent', () => {
    beforeEach(function beforeEach() {
        this.sandbox = sinon.createSandbox();
    });
    afterEach(function afterEach() {
        this.sandbox.restore();
    });
    it('returns the right User-Agent on Windows', function test() {
        this.sandbox.stub(process, 'platform').get(() => 'win32');
        chai_1.assert.strictEqual((0, getUserAgent_1.getUserAgent)('1.2.3'), 'Signal-Desktop/1.2.3 Windows');
    });
    it('returns the right User-Agent on macOS', function test() {
        this.sandbox.stub(process, 'platform').get(() => 'darwin');
        chai_1.assert.strictEqual((0, getUserAgent_1.getUserAgent)('1.2.3'), 'Signal-Desktop/1.2.3 macOS');
    });
    it('returns the right User-Agent on Linux', function test() {
        this.sandbox.stub(process, 'platform').get(() => 'linux');
        chai_1.assert.strictEqual((0, getUserAgent_1.getUserAgent)('1.2.3'), 'Signal-Desktop/1.2.3 Linux');
    });
    it('omits the platform on unsupported platforms', function test() {
        this.sandbox.stub(process, 'platform').get(() => 'freebsd');
        chai_1.assert.strictEqual((0, getUserAgent_1.getUserAgent)('1.2.3'), 'Signal-Desktop/1.2.3');
    });
});
