"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isFileDangerous_1 = require("../../util/isFileDangerous");
describe('isFileDangerous', () => {
    it('returns false for images', () => {
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('dog.gif'), false);
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('cat.jpg'), false);
    });
    it('returns false for documents', () => {
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('resume.docx'), false);
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('price_list.pdf'), false);
    });
    it('returns true for executable files', () => {
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('run.exe'), true);
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('install.pif'), true);
    });
    it('returns true for Microsoft settings files', () => {
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('downl.SettingContent-ms'), true);
    });
    it('returns false for non-dangerous files that end in ".", which can happen on Windows', () => {
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('dog.png.'), false);
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('resume.docx.'), false);
    });
    it('returns true for dangerous files that end in ".", which can happen on Windows', () => {
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('run.exe.'), true);
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('install.pif.'), true);
    });
    it('returns false for empty filename', () => {
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)(''), false);
    });
    it('returns false for exe at various parts of filename', () => {
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('.exemanifesto.txt'), false);
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('runexe'), false);
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('run_exe'), false);
    });
    it('returns true for upper-case EXE', () => {
        chai_1.assert.strictEqual((0, isFileDangerous_1.isFileDangerous)('run.EXE'), true);
    });
});
