"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const combineNames_1 = require("../../util/combineNames");
describe('combineNames', () => {
    it('returns undefined if no names provided', () => {
        chai_1.assert.strictEqual((0, combineNames_1.combineNames)('', ''), undefined);
    });
    it('returns first name only if family name not provided', () => {
        chai_1.assert.strictEqual((0, combineNames_1.combineNames)('Alice'), 'Alice');
    });
    it('returns returns combined names', () => {
        chai_1.assert.strictEqual((0, combineNames_1.combineNames)('Alice', 'Jones'), 'Alice Jones');
    });
    it('returns given name first if names in Chinese', () => {
        chai_1.assert.strictEqual((0, combineNames_1.combineNames)('振宁', '杨'), '杨振宁');
    });
    it('returns given name first if names in Japanese', () => {
        chai_1.assert.strictEqual((0, combineNames_1.combineNames)('泰夫', '木田'), '木田泰夫');
    });
    it('returns given name first if names in Korean', () => {
        chai_1.assert.strictEqual((0, combineNames_1.combineNames)('채원', '도윤'), '도윤채원');
    });
});
