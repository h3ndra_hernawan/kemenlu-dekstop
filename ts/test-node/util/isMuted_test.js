"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isMuted_1 = require("../../util/isMuted");
describe('isMuted', () => {
    it('returns false if passed undefined', () => {
        chai_1.assert.isFalse((0, isMuted_1.isMuted)(undefined));
    });
    it('returns false if passed a date in the past', () => {
        chai_1.assert.isFalse((0, isMuted_1.isMuted)(0));
        chai_1.assert.isFalse((0, isMuted_1.isMuted)(Date.now() - 123));
    });
    it('returns false if passed a date in the future', () => {
        chai_1.assert.isTrue((0, isMuted_1.isMuted)(Date.now() + 123));
        chai_1.assert.isTrue((0, isMuted_1.isMuted)(Date.now() + 123456));
    });
});
