"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const getOwn_1 = require("../../util/getOwn");
describe('getOwn', () => {
    class Person {
        constructor(birthYear) {
            this.birthYear = birthYear;
        }
        getAge() {
            return new Date().getFullYear() - this.birthYear;
        }
    }
    it('returns undefined when asking for a non-existent property', () => {
        const obj = { bar: 123 };
        chai_1.assert.isUndefined((0, getOwn_1.getOwn)(obj, 'foo'));
    });
    it('returns undefined when asking for a non-own property', () => {
        const obj = { bar: 123 };
        chai_1.assert.isUndefined((0, getOwn_1.getOwn)(obj, 'hasOwnProperty'));
        const person = new Person(1880);
        chai_1.assert.isUndefined((0, getOwn_1.getOwn)(person, 'getAge'));
    });
    it('returns own properties', () => {
        const obj = { foo: 123 };
        chai_1.assert.strictEqual((0, getOwn_1.getOwn)(obj, 'foo'), 123);
        const person = new Person(1880);
        chai_1.assert.strictEqual((0, getOwn_1.getOwn)(person, 'birthYear'), 1880);
    });
    it('works even if `hasOwnProperty` has been overridden for the object', () => {
        const obj = {
            foo: 123,
            hasOwnProperty: () => true,
        };
        chai_1.assert.strictEqual((0, getOwn_1.getOwn)(obj, 'foo'), 123);
        chai_1.assert.isUndefined((0, getOwn_1.getOwn)(obj, 'bar'));
    });
});
