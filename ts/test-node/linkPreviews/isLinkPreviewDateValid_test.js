"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const isLinkPreviewDateValid_1 = require("../../linkPreviews/isLinkPreviewDateValid");
describe('isLinkPreviewDateValid', () => {
    it('returns false for non-numbers', () => {
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(null));
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(undefined));
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(Date.now().toString()));
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(new Date()));
    });
    it('returns false for zero', () => {
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(0));
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(-0));
    });
    it('returns false for NaN', () => {
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(0 / 0));
    });
    it('returns false for any infinite value', () => {
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(Infinity));
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(-Infinity));
    });
    it('returns false for timestamps more than a day from now', () => {
        const twoDays = 2 * 24 * 60 * 60 * 1000;
        chai_1.assert.isFalse((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(Date.now() + twoDays));
    });
    it('returns true for timestamps before tomorrow', () => {
        chai_1.assert.isTrue((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(Date.now()));
        chai_1.assert.isTrue((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(Date.now() + 123));
        chai_1.assert.isTrue((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(Date.now() - 123));
        chai_1.assert.isTrue((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(new Date(1995, 3, 20).valueOf()));
        chai_1.assert.isTrue((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(new Date(1970, 3, 20).valueOf()));
        chai_1.assert.isTrue((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(new Date(1969, 3, 20).valueOf()));
        chai_1.assert.isTrue((0, isLinkPreviewDateValid_1.isLinkPreviewDateValid)(1));
    });
});
