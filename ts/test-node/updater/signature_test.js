"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const chai_1 = require("chai");
const fs_extra_1 = require("fs-extra");
const signature_1 = require("../../updater/signature");
const common_1 = require("../../updater/common");
const curve_1 = require("../../updater/curve");
describe('updater/signatures', () => {
    it('_getFileHash returns correct hash', async () => {
        const filePath = (0, path_1.join)(__dirname, '../../../fixtures/ghost-kitty.mp4');
        const expected = '7bc77f27d92d00b4a1d57c480ca86dacc43d57bc318339c92119d1fbf6b557a5';
        const hash = await (0, signature_1._getFileHash)(filePath);
        chai_1.assert.strictEqual(expected, Buffer.from(hash).toString('hex'));
    });
    it('roundtrips binary file writes', async () => {
        let tempDir;
        try {
            tempDir = await (0, common_1.createTempDir)();
            const path = (0, path_1.join)(tempDir, 'something.bin');
            const { publicKey } = (0, curve_1.keyPair)();
            await (0, signature_1.writeHexToPath)(path, publicKey);
            const fromDisk = await (0, signature_1.loadHexFromPath)(path);
            chai_1.assert.strictEqual(Buffer.from(fromDisk).compare(Buffer.from(publicKey)), 0);
        }
        finally {
            if (tempDir) {
                await (0, common_1.deleteTempDir)(tempDir);
            }
        }
    });
    it('roundtrips signature', async () => {
        let tempDir;
        try {
            tempDir = await (0, common_1.createTempDir)();
            const version = 'v1.23.2';
            const sourcePath = (0, path_1.join)(__dirname, '../../../fixtures/ghost-kitty.mp4');
            const updatePath = (0, path_1.join)(tempDir, 'ghost-kitty.mp4');
            await (0, fs_extra_1.copy)(sourcePath, updatePath);
            const privateKeyPath = (0, path_1.join)(tempDir, 'private.key');
            const { publicKey, privateKey } = (0, curve_1.keyPair)();
            await (0, signature_1.writeHexToPath)(privateKeyPath, privateKey);
            await (0, signature_1.writeSignature)(updatePath, version, privateKeyPath);
            const signaturePath = (0, signature_1.getSignaturePath)(updatePath);
            chai_1.assert.strictEqual((0, fs_1.existsSync)(signaturePath), true);
            const verified = await (0, signature_1.verifySignature)(updatePath, version, publicKey);
            chai_1.assert.strictEqual(verified, true);
        }
        finally {
            if (tempDir) {
                await (0, common_1.deleteTempDir)(tempDir);
            }
        }
    });
    it('fails signature verification if version changes', async () => {
        let tempDir;
        try {
            tempDir = await (0, common_1.createTempDir)();
            const version = 'v1.23.2';
            const brokenVersion = 'v1.23.3';
            const sourcePath = (0, path_1.join)(__dirname, '../../../fixtures/ghost-kitty.mp4');
            const updatePath = (0, path_1.join)(tempDir, 'ghost-kitty.mp4');
            await (0, fs_extra_1.copy)(sourcePath, updatePath);
            const privateKeyPath = (0, path_1.join)(tempDir, 'private.key');
            const { publicKey, privateKey } = (0, curve_1.keyPair)();
            await (0, signature_1.writeHexToPath)(privateKeyPath, privateKey);
            await (0, signature_1.writeSignature)(updatePath, version, privateKeyPath);
            const verified = await (0, signature_1.verifySignature)(updatePath, brokenVersion, publicKey);
            chai_1.assert.strictEqual(verified, false);
        }
        finally {
            if (tempDir) {
                await (0, common_1.deleteTempDir)(tempDir);
            }
        }
    });
    it('fails signature verification if signature tampered with', async () => {
        let tempDir;
        try {
            tempDir = await (0, common_1.createTempDir)();
            const version = 'v1.23.2';
            const sourcePath = (0, path_1.join)(__dirname, '../../../fixtures/ghost-kitty.mp4');
            const updatePath = (0, path_1.join)(tempDir, 'ghost-kitty.mp4');
            await (0, fs_extra_1.copy)(sourcePath, updatePath);
            const privateKeyPath = (0, path_1.join)(tempDir, 'private.key');
            const { publicKey, privateKey } = (0, curve_1.keyPair)();
            await (0, signature_1.writeHexToPath)(privateKeyPath, privateKey);
            await (0, signature_1.writeSignature)(updatePath, version, privateKeyPath);
            const signaturePath = (0, signature_1.getSignaturePath)(updatePath);
            const signature = Buffer.from(await (0, signature_1.loadHexFromPath)(signaturePath));
            signature[4] += 3;
            await (0, signature_1.writeHexToPath)(signaturePath, signature);
            const verified = await (0, signature_1.verifySignature)(updatePath, version, publicKey);
            chai_1.assert.strictEqual(verified, false);
        }
        finally {
            if (tempDir) {
                await (0, common_1.deleteTempDir)(tempDir);
            }
        }
    });
    it('fails signature verification if binary file tampered with', async () => {
        let tempDir;
        try {
            tempDir = await (0, common_1.createTempDir)();
            const version = 'v1.23.2';
            const sourcePath = (0, path_1.join)(__dirname, '../../../fixtures/ghost-kitty.mp4');
            const updatePath = (0, path_1.join)(tempDir, 'ghost-kitty.mp4');
            await (0, fs_extra_1.copy)(sourcePath, updatePath);
            const privateKeyPath = (0, path_1.join)(tempDir, 'private.key');
            const { publicKey, privateKey } = (0, curve_1.keyPair)();
            await (0, signature_1.writeHexToPath)(privateKeyPath, privateKey);
            await (0, signature_1.writeSignature)(updatePath, version, privateKeyPath);
            const brokenSourcePath = (0, path_1.join)(__dirname, '../../../fixtures/pixabay-Soap-Bubble-7141.mp4');
            await (0, fs_extra_1.copy)(brokenSourcePath, updatePath);
            const verified = await (0, signature_1.verifySignature)(updatePath, version, publicKey);
            chai_1.assert.strictEqual(verified, false);
        }
        finally {
            if (tempDir) {
                await (0, common_1.deleteTempDir)(tempDir);
            }
        }
    });
    it('fails signature verification if signed by different key', async () => {
        let tempDir;
        try {
            tempDir = await (0, common_1.createTempDir)();
            const version = 'v1.23.2';
            const sourcePath = (0, path_1.join)(__dirname, '../../../fixtures/ghost-kitty.mp4');
            const updatePath = (0, path_1.join)(tempDir, 'ghost-kitty.mp4');
            await (0, fs_extra_1.copy)(sourcePath, updatePath);
            const privateKeyPath = (0, path_1.join)(tempDir, 'private.key');
            const { publicKey } = (0, curve_1.keyPair)();
            const { privateKey } = (0, curve_1.keyPair)();
            await (0, signature_1.writeHexToPath)(privateKeyPath, privateKey);
            await (0, signature_1.writeSignature)(updatePath, version, privateKeyPath);
            const verified = await (0, signature_1.verifySignature)(updatePath, version, publicKey);
            chai_1.assert.strictEqual(verified, false);
        }
        finally {
            if (tempDir) {
                await (0, common_1.deleteTempDir)(tempDir);
            }
        }
    });
});
