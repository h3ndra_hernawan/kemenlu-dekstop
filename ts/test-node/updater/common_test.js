"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const common_1 = require("../../updater/common");
describe('updater/signatures', () => {
    const windows = (0, common_1.parseYaml)(`version: 1.23.2
files:
  - url: signal-desktop-win-1.23.2.exe
    sha512: hhK+cVAb+QOK/Ln0RBcq8Rb1iPcUC0KZeT4NwLB25PMGoPmakY27XE1bXq4QlkASJN1EkYTbKf3oUJtcllziyQ==
    size: 92020776
path: signal-desktop-win-1.23.2.exe
sha512: hhK+cVAb+QOK/Ln0RBcq8Rb1iPcUC0KZeT4NwLB25PMGoPmakY27XE1bXq4QlkASJN1EkYTbKf3oUJtcllziyQ==
releaseDate: '2019-03-29T16:58:08.210Z'
`);
    const mac = (0, common_1.parseYaml)(`version: 1.23.2
files:
  - url: signal-desktop-mac-1.23.2.zip
    sha512: f4pPo3WulTVi9zBWGsJPNIlvPOTCxPibPPDmRFDoXMmFm6lqJpXZQ9DSWMJumfc4BRp4y/NTQLGYI6b4WuJwhg==
    size: 105179791
    blockMapSize: 111109
path: signal-desktop-mac-1.23.2.zip
sha512: f4pPo3WulTVi9zBWGsJPNIlvPOTCxPibPPDmRFDoXMmFm6lqJpXZQ9DSWMJumfc4BRp4y/NTQLGYI6b4WuJwhg==
releaseDate: '2019-03-29T16:57:16.997Z'
`);
    const windowsBeta = (0, common_1.parseYaml)(`version: 1.23.2-beta.1
files:
  - url: signal-desktop-beta-win-1.23.2-beta.1.exe
    sha512: ZHM1F3y/Y6ulP5NhbFuh7t2ZCpY4lD9BeBhPV+g2B/0p/66kp0MJDeVxTgjR49OakwpMAafA1d6y2QBail4hSQ==
    size: 92028656
path: signal-desktop-beta-win-1.23.2-beta.1.exe
sha512: ZHM1F3y/Y6ulP5NhbFuh7t2ZCpY4lD9BeBhPV+g2B/0p/66kp0MJDeVxTgjR49OakwpMAafA1d6y2QBail4hSQ==
releaseDate: '2019-03-29T01:56:00.544Z'
`);
    const macBeta = (0, common_1.parseYaml)(`version: 1.23.2-beta.1
files:
  - url: signal-desktop-beta-mac-1.23.2-beta.1.zip
    sha512: h/01N0DD5Jw2Q6M1n4uLGLTCrMFxcn8QOPtLR3HpABsf3w9b2jFtKb56/2cbuJXP8ol8TkTDWKnRV6mnqnLBDw==
    size: 105182398
    blockMapSize: 110894
path: signal-desktop-beta-mac-1.23.2-beta.1.zip
sha512: h/01N0DD5Jw2Q6M1n4uLGLTCrMFxcn8QOPtLR3HpABsf3w9b2jFtKb56/2cbuJXP8ol8TkTDWKnRV6mnqnLBDw==
releaseDate: '2019-03-29T01:53:23.881Z'
`);
    describe('#getVersion', () => {
        it('successfully gets version', () => {
            const expected = '1.23.2';
            chai_1.assert.strictEqual((0, common_1.getVersion)(windows), expected);
            chai_1.assert.strictEqual((0, common_1.getVersion)(mac), expected);
            const expectedBeta = '1.23.2-beta.1';
            chai_1.assert.strictEqual((0, common_1.getVersion)(windowsBeta), expectedBeta);
            chai_1.assert.strictEqual((0, common_1.getVersion)(macBeta), expectedBeta);
        });
    });
    describe('#getUpdateFileName', () => {
        it('successfully gets version', () => {
            chai_1.assert.strictEqual((0, common_1.getUpdateFileName)(windows), 'signal-desktop-win-1.23.2.exe');
            chai_1.assert.strictEqual((0, common_1.getUpdateFileName)(mac), 'signal-desktop-mac-1.23.2.zip');
            chai_1.assert.strictEqual((0, common_1.getUpdateFileName)(windowsBeta), 'signal-desktop-beta-win-1.23.2-beta.1.exe');
            chai_1.assert.strictEqual((0, common_1.getUpdateFileName)(macBeta), 'signal-desktop-beta-mac-1.23.2-beta.1.zip');
        });
    });
    describe('#isUpdateFileNameValid', () => {
        it('returns true for normal filenames', () => {
            chai_1.assert.strictEqual((0, common_1.isUpdateFileNameValid)('signal-desktop-win-1.23.2.exe'), true);
            chai_1.assert.strictEqual((0, common_1.isUpdateFileNameValid)('signal-desktop-mac-1.23.2-beta.1.zip'), true);
        });
        it('returns false for problematic names', () => {
            chai_1.assert.strictEqual((0, common_1.isUpdateFileNameValid)('../signal-desktop-win-1.23.2.exe'), false);
            chai_1.assert.strictEqual((0, common_1.isUpdateFileNameValid)('%signal-desktop-mac-1.23.2-beta.1.zip'), false);
            chai_1.assert.strictEqual((0, common_1.isUpdateFileNameValid)('@signal-desktop-mac-1.23.2-beta.1.zip'), false);
        });
    });
    describe('#validatePath', () => {
        it('succeeds for simple children', async () => {
            const base = await (0, common_1.createTempDir)();
            (0, common_1.validatePath)(base, `${base}/child`);
            (0, common_1.validatePath)(base, `${base}/child/grandchild`);
        });
        it('returns false for problematic names', async () => {
            const base = await (0, common_1.createTempDir)();
            chai_1.assert.throws(() => {
                (0, common_1.validatePath)(base, `${base}/../child`);
            });
            chai_1.assert.throws(() => {
                (0, common_1.validatePath)(base, '/root');
            });
        });
    });
});
