"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const uuid_1 = require("uuid");
const ConversationList_1 = require("../../../components/ConversationList");
const LeftPaneHelper_1 = require("../../../components/leftPane/LeftPaneHelper");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const LeftPaneSearchHelper_1 = require("../../../components/leftPane/LeftPaneSearchHelper");
const LeftPaneArchiveHelper_1 = require("../../../components/leftPane/LeftPaneArchiveHelper");
describe('LeftPaneArchiveHelper', () => {
    let sandbox;
    const defaults = {
        archivedConversations: [],
        searchConversation: undefined,
        searchTerm: '',
    };
    const searchingDefaults = Object.assign(Object.assign({}, defaults), { searchConversation: (0, getDefaultConversation_1.getDefaultConversation)(), conversationResults: { isLoading: false, results: [] }, contactResults: { isLoading: false, results: [] }, messageResults: { isLoading: false, results: [] }, searchTerm: 'foo', primarySendsSms: false });
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('getBackAction', () => {
        it('returns the "show inbox" action', () => {
            const showInbox = sinon.fake();
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(defaults);
            chai_1.assert.strictEqual(helper.getBackAction({ showInbox }), showInbox);
        });
    });
    describe('getRowCount', () => {
        it('returns the number of archived conversations', () => {
            chai_1.assert.strictEqual(new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(defaults).getRowCount(), 0);
            chai_1.assert.strictEqual(new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(Object.assign(Object.assign({}, defaults), { archivedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] })).getRowCount(), 2);
        });
        it('defers to the search helper if searching', () => {
            sandbox.stub(LeftPaneSearchHelper_1.LeftPaneSearchHelper.prototype, 'getRowCount').returns(123);
            chai_1.assert.strictEqual(new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(searchingDefaults).getRowCount(), 123);
        });
    });
    describe('getRowIndexToScrollTo', () => {
        it('returns undefined if no conversation is selected', () => {
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(Object.assign(Object.assign({}, defaults), { archivedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] }));
            chai_1.assert.isUndefined(helper.getRowIndexToScrollTo(undefined));
        });
        it('returns undefined if the selected conversation is not pinned or non-pinned', () => {
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(Object.assign(Object.assign({}, defaults), { archivedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] }));
            chai_1.assert.isUndefined(helper.getRowIndexToScrollTo((0, uuid_1.v4)()));
        });
        it("returns the archived conversation's index", () => {
            const archivedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(Object.assign(Object.assign({}, defaults), { archivedConversations }));
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(archivedConversations[0].id), 0);
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(archivedConversations[1].id), 1);
        });
        it('defers to the search helper if searching', () => {
            sandbox
                .stub(LeftPaneSearchHelper_1.LeftPaneSearchHelper.prototype, 'getRowIndexToScrollTo')
                .returns(123);
            const archivedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(searchingDefaults);
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(archivedConversations[0].id), 123);
        });
    });
    describe('getRow', () => {
        it('returns each conversation as a row', () => {
            const archivedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(Object.assign(Object.assign({}, defaults), { archivedConversations }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Conversation,
                conversation: archivedConversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: archivedConversations[1],
            });
        });
        it('defers to the search helper if searching', () => {
            sandbox
                .stub(LeftPaneSearchHelper_1.LeftPaneSearchHelper.prototype, 'getRow')
                .returns({ type: ConversationList_1.RowType.SearchResultsLoadingFakeHeader });
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(searchingDefaults);
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.SearchResultsLoadingFakeHeader,
            });
        });
    });
    describe('getConversationAndMessageAtIndex', () => {
        it('returns the conversation at the given index when it exists', () => {
            var _a, _b;
            const archivedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(Object.assign(Object.assign({}, defaults), { archivedConversations }));
            chai_1.assert.strictEqual((_a = helper.getConversationAndMessageAtIndex(0)) === null || _a === void 0 ? void 0 : _a.conversationId, archivedConversations[0].id);
            chai_1.assert.strictEqual((_b = helper.getConversationAndMessageAtIndex(1)) === null || _b === void 0 ? void 0 : _b.conversationId, archivedConversations[1].id);
        });
        it('when requesting an index out of bounds, returns the last conversation', () => {
            var _a, _b, _c;
            const archivedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(Object.assign(Object.assign({}, defaults), { archivedConversations }));
            chai_1.assert.strictEqual((_a = helper.getConversationAndMessageAtIndex(2)) === null || _a === void 0 ? void 0 : _a.conversationId, archivedConversations[1].id);
            chai_1.assert.strictEqual((_b = helper.getConversationAndMessageAtIndex(99)) === null || _b === void 0 ? void 0 : _b.conversationId, archivedConversations[1].id);
            // This is mostly a resilience measure in case we're ever called with an invalid
            //   index.
            chai_1.assert.strictEqual((_c = helper.getConversationAndMessageAtIndex(-1)) === null || _c === void 0 ? void 0 : _c.conversationId, archivedConversations[1].id);
        });
        it('returns undefined if there are no archived conversations', () => {
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(defaults);
            chai_1.assert.isUndefined(helper.getConversationAndMessageAtIndex(0));
            chai_1.assert.isUndefined(helper.getConversationAndMessageAtIndex(1));
            chai_1.assert.isUndefined(helper.getConversationAndMessageAtIndex(-1));
        });
        it('defers to the search helper if searching', () => {
            sandbox
                .stub(LeftPaneSearchHelper_1.LeftPaneSearchHelper.prototype, 'getConversationAndMessageAtIndex')
                .returns({ conversationId: 'abc123' });
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(searchingDefaults);
            chai_1.assert.deepEqual(helper.getConversationAndMessageAtIndex(999), {
                conversationId: 'abc123',
            });
        });
    });
    describe('getConversationAndMessageInDirection', () => {
        it('returns the next conversation when searching downward', () => {
            const archivedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(Object.assign(Object.assign({}, defaults), { archivedConversations }));
            chai_1.assert.deepEqual(helper.getConversationAndMessageInDirection({ direction: LeftPaneHelper_1.FindDirection.Down, unreadOnly: false }, archivedConversations[0].id, undefined), { conversationId: archivedConversations[1].id });
        });
        // Additional tests are found with `getConversationInDirection`.
        it('defers to the search helper if searching', () => {
            sandbox
                .stub(LeftPaneSearchHelper_1.LeftPaneSearchHelper.prototype, 'getConversationAndMessageInDirection')
                .returns({ conversationId: 'abc123' });
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(searchingDefaults);
            chai_1.assert.deepEqual(helper.getConversationAndMessageInDirection({
                direction: LeftPaneHelper_1.FindDirection.Down,
                unreadOnly: false,
            }, (0, getDefaultConversation_1.getDefaultConversation)().id, undefined), {
                conversationId: 'abc123',
            });
        });
    });
    describe('shouldRecomputeRowHeights', () => {
        it('returns false when not searching because row heights are constant', () => {
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(Object.assign(Object.assign({}, defaults), { archivedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] }));
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights(Object.assign(Object.assign({}, defaults), { archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] })));
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights(Object.assign(Object.assign({}, defaults), { archivedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] })));
        });
        it('returns true when going from searching → not searching', () => {
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(defaults);
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights(searchingDefaults));
        });
        it('returns true when going from not searching → searching', () => {
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(searchingDefaults);
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights(defaults));
        });
        it('defers to the search helper if searching', () => {
            sandbox
                .stub(LeftPaneSearchHelper_1.LeftPaneSearchHelper.prototype, 'shouldRecomputeRowHeights')
                .returns(true);
            const helper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(searchingDefaults);
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights(searchingDefaults));
        });
    });
});
