"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const LeftPaneHelper_1 = require("../../../components/leftPane/LeftPaneHelper");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const getConversationInDirection_1 = require("../../../components/leftPane/getConversationInDirection");
describe('getConversationInDirection', () => {
    const fakeConversation = (markedUnread = false) => (0, getDefaultConversation_1.getDefaultConversation)({ markedUnread });
    const fakeConversations = [
        fakeConversation(),
        fakeConversation(true),
        fakeConversation(true),
        fakeConversation(),
    ];
    describe('searching for any conversation', () => {
        const up = {
            direction: LeftPaneHelper_1.FindDirection.Up,
            unreadOnly: false,
        };
        const down = {
            direction: LeftPaneHelper_1.FindDirection.Down,
            unreadOnly: false,
        };
        it('returns undefined if there are no conversations', () => {
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)([], up, undefined));
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)([], down, undefined));
        });
        it('if no conversation is selected, returns the last conversation when going up', () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, up, undefined), { conversationId: fakeConversations[3].id });
        });
        it('if no conversation is selected, returns the first conversation when going down', () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, down, undefined), { conversationId: fakeConversations[0].id });
        });
        it('if the first conversation is selected, returns the last conversation when going up', () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, up, fakeConversations[0].id), { conversationId: fakeConversations[3].id });
        });
        it('if the last conversation is selected, returns the first conversation when going down', () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, down, fakeConversations[3].id), { conversationId: fakeConversations[0].id });
        });
        it('goes up one conversation in normal cases', () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, up, fakeConversations[2].id), { conversationId: fakeConversations[1].id });
        });
        it('goes down one conversation in normal cases', () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, down, fakeConversations[0].id), { conversationId: fakeConversations[1].id });
        });
    });
    describe('searching for unread conversations', () => {
        const up = {
            direction: LeftPaneHelper_1.FindDirection.Up,
            unreadOnly: true,
        };
        const down = {
            direction: LeftPaneHelper_1.FindDirection.Down,
            unreadOnly: true,
        };
        const noUnreads = [
            fakeConversation(),
            fakeConversation(),
            fakeConversation(),
        ];
        it('returns undefined if there are no conversations', () => {
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)([], up, undefined));
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)([], down, undefined));
        });
        it('if no conversation is selected, finds the last unread conversation (if it exists) when searching up', () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, up, undefined), { conversationId: fakeConversations[2].id });
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)(noUnreads, up, undefined));
        });
        it('if no conversation is selected, finds the first unread conversation (if it exists) when searching down', () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, down, undefined), { conversationId: fakeConversations[1].id });
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)(noUnreads, down, undefined));
        });
        it("searches up for unread conversations, returning undefined if no conversation exists (doesn't wrap around)", () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, up, fakeConversations[3].id), { conversationId: fakeConversations[2].id });
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, up, fakeConversations[2].id), { conversationId: fakeConversations[1].id });
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, up, fakeConversations[1].id));
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)(noUnreads, up, noUnreads[2].id));
        });
        it("searches down for unread conversations, returning undefined if no conversation exists (doesn't wrap around)", () => {
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, down, fakeConversations[0].id), { conversationId: fakeConversations[1].id });
            chai_1.assert.deepEqual((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, down, fakeConversations[1].id), { conversationId: fakeConversations[2].id });
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)(fakeConversations, down, fakeConversations[2].id));
            chai_1.assert.isUndefined((0, getConversationInDirection_1.getConversationInDirection)(noUnreads, down, noUnreads[1].id));
        });
    });
});
