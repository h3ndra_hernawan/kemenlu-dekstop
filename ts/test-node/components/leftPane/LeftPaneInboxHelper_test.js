"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const ConversationList_1 = require("../../../components/ConversationList");
const LeftPaneHelper_1 = require("../../../components/leftPane/LeftPaneHelper");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const LeftPaneInboxHelper_1 = require("../../../components/leftPane/LeftPaneInboxHelper");
describe('LeftPaneInboxHelper', () => {
    const defaultProps = {
        conversations: [],
        pinnedConversations: [],
        archivedConversations: [],
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    };
    describe('getBackAction', () => {
        it("returns undefined; you can't go back from the main inbox", () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(defaultProps);
            chai_1.assert.isUndefined(helper.getBackAction({
                showChooseGroupMembers: sinon.fake(),
                showInbox: sinon.fake(),
                startComposing: sinon.fake(),
            }));
        });
    });
    describe('getRowCount', () => {
        it('returns 0 if there are no conversations', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(defaultProps);
            chai_1.assert.strictEqual(helper.getRowCount(), 0);
        });
        it('returns 1 if there are only archived conversations', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.strictEqual(helper.getRowCount(), 1);
        });
        it("returns the number of non-pinned conversations if that's all there is", () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] }));
            chai_1.assert.strictEqual(helper.getRowCount(), 3);
        });
        it("returns the number of pinned conversations if that's all there is", () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] }));
            chai_1.assert.strictEqual(helper.getRowCount(), 3);
        });
        it('adds 2 rows for each header if there are pinned and non-pinned conversations,', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], pinnedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.strictEqual(helper.getRowCount(), 6);
        });
        it('adds 1 row for the archive button if there are any archived conversations', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.strictEqual(helper.getRowCount(), 4);
        });
    });
    describe('getRowIndexToScrollTo', () => {
        it('returns undefined if no conversation is selected', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()], pinnedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.isUndefined(helper.getRowIndexToScrollTo(undefined));
        });
        it('returns undefined if the selected conversation is not pinned or non-pinned', () => {
            const archivedConversations = [(0, getDefaultConversation_1.getDefaultConversation)()];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()], pinnedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()], archivedConversations }));
            chai_1.assert.isUndefined(helper.getRowIndexToScrollTo(archivedConversations[0].id));
        });
        it("returns the pinned conversation's index if there are only pinned conversations", () => {
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { pinnedConversations }));
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(pinnedConversations[0].id), 0);
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(pinnedConversations[1].id), 1);
        });
        it("returns the conversation's index if there are only non-pinned conversations", () => {
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations }));
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(conversations[0].id), 0);
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(conversations[1].id), 1);
        });
        it("returns the pinned conversation's index + 1 (for the header) if there are both pinned and non-pinned conversations", () => {
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [(0, getDefaultConversation_1.getDefaultConversation)()], pinnedConversations }));
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(pinnedConversations[0].id), 1);
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(pinnedConversations[1].id), 2);
        });
        it("returns the non-pinned conversation's index + pinnedConversations.length + 2 (for the headers) if there are both pinned and non-pinned conversations", () => {
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations, pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] }));
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(conversations[0].id), 5);
            chai_1.assert.strictEqual(helper.getRowIndexToScrollTo(conversations[1].id), 6);
        });
    });
    describe('getRow', () => {
        it('returns the archive button if there are only archived conversations', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { archivedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.ArchiveButton,
                archivedConversationsCount: 2,
            });
            chai_1.assert.isUndefined(helper.getRow(1));
        });
        it("returns pinned conversations if that's all there are", () => {
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { pinnedConversations }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Conversation,
                conversation: pinnedConversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: pinnedConversations[1],
            });
            chai_1.assert.isUndefined(helper.getRow(2));
        });
        it('returns pinned conversations and an archive button if there are no non-pinned conversations', () => {
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { pinnedConversations, archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Conversation,
                conversation: pinnedConversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: pinnedConversations[1],
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.ArchiveButton,
                archivedConversationsCount: 1,
            });
            chai_1.assert.isUndefined(helper.getRow(3));
        });
        it("returns non-pinned conversations if that's all there are", () => {
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[1],
            });
            chai_1.assert.isUndefined(helper.getRow(2));
        });
        it('returns non-pinned conversations and an archive button if there are no pinned conversations', () => {
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations, archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[1],
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.ArchiveButton,
                archivedConversationsCount: 1,
            });
            chai_1.assert.isUndefined(helper.getRow(3));
        });
        it('returns headers if there are both pinned and non-pinned conversations', () => {
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations,
                pinnedConversations }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'LeftPane--pinned',
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: pinnedConversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Conversation,
                conversation: pinnedConversations[1],
            });
            chai_1.assert.deepEqual(helper.getRow(3), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'LeftPane--chats',
            });
            chai_1.assert.deepEqual(helper.getRow(4), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(5), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[1],
            });
            chai_1.assert.deepEqual(helper.getRow(6), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[2],
            });
            chai_1.assert.isUndefined(helper.getRow(7));
        });
        it('returns headers if there are both pinned and non-pinned conversations, and an archive button', () => {
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations,
                pinnedConversations, archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'LeftPane--pinned',
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: pinnedConversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Conversation,
                conversation: pinnedConversations[1],
            });
            chai_1.assert.deepEqual(helper.getRow(3), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'LeftPane--chats',
            });
            chai_1.assert.deepEqual(helper.getRow(4), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(5), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[1],
            });
            chai_1.assert.deepEqual(helper.getRow(6), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[2],
            });
            chai_1.assert.deepEqual(helper.getRow(7), {
                type: ConversationList_1.RowType.ArchiveButton,
                archivedConversationsCount: 1,
            });
            chai_1.assert.isUndefined(helper.getRow(8));
        });
    });
    describe('getConversationAndMessageAtIndex', () => {
        it('returns pinned converastions, then non-pinned conversations', () => {
            var _a, _b, _c, _d, _e;
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations,
                pinnedConversations }));
            chai_1.assert.strictEqual((_a = helper.getConversationAndMessageAtIndex(0)) === null || _a === void 0 ? void 0 : _a.conversationId, pinnedConversations[0].id);
            chai_1.assert.strictEqual((_b = helper.getConversationAndMessageAtIndex(1)) === null || _b === void 0 ? void 0 : _b.conversationId, pinnedConversations[1].id);
            chai_1.assert.strictEqual((_c = helper.getConversationAndMessageAtIndex(2)) === null || _c === void 0 ? void 0 : _c.conversationId, conversations[0].id);
            chai_1.assert.strictEqual((_d = helper.getConversationAndMessageAtIndex(3)) === null || _d === void 0 ? void 0 : _d.conversationId, conversations[1].id);
            chai_1.assert.strictEqual((_e = helper.getConversationAndMessageAtIndex(4)) === null || _e === void 0 ? void 0 : _e.conversationId, conversations[2].id);
        });
        it("when requesting an index out of bounds, returns the last pinned conversation when that's all there is", () => {
            var _a, _b, _c;
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { pinnedConversations }));
            chai_1.assert.strictEqual((_a = helper.getConversationAndMessageAtIndex(2)) === null || _a === void 0 ? void 0 : _a.conversationId, pinnedConversations[1].id);
            chai_1.assert.strictEqual((_b = helper.getConversationAndMessageAtIndex(99)) === null || _b === void 0 ? void 0 : _b.conversationId, pinnedConversations[1].id);
            // This is mostly a resilience measure in case we're ever called with an invalid
            //   index.
            chai_1.assert.strictEqual((_c = helper.getConversationAndMessageAtIndex(-1)) === null || _c === void 0 ? void 0 : _c.conversationId, pinnedConversations[1].id);
        });
        it("when requesting an index out of bounds, returns the last non-pinned conversation when that's all there is", () => {
            var _a, _b, _c;
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations }));
            chai_1.assert.strictEqual((_a = helper.getConversationAndMessageAtIndex(2)) === null || _a === void 0 ? void 0 : _a.conversationId, conversations[1].id);
            chai_1.assert.strictEqual((_b = helper.getConversationAndMessageAtIndex(99)) === null || _b === void 0 ? void 0 : _b.conversationId, conversations[1].id);
            // This is mostly a resilience measure in case we're ever called with an invalid
            //   index.
            chai_1.assert.strictEqual((_c = helper.getConversationAndMessageAtIndex(-1)) === null || _c === void 0 ? void 0 : _c.conversationId, conversations[1].id);
        });
        it('when requesting an index out of bounds, returns the last non-pinned conversation when there are both pinned and non-pinned conversations', () => {
            var _a, _b, _c;
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations,
                pinnedConversations }));
            chai_1.assert.strictEqual((_a = helper.getConversationAndMessageAtIndex(4)) === null || _a === void 0 ? void 0 : _a.conversationId, conversations[1].id);
            chai_1.assert.strictEqual((_b = helper.getConversationAndMessageAtIndex(99)) === null || _b === void 0 ? void 0 : _b.conversationId, conversations[1].id);
            // This is mostly a resilience measure in case we're ever called with an invalid
            //   index.
            chai_1.assert.strictEqual((_c = helper.getConversationAndMessageAtIndex(-1)) === null || _c === void 0 ? void 0 : _c.conversationId, conversations[1].id);
        });
        it('returns undefined if there are no conversations', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.isUndefined(helper.getConversationAndMessageAtIndex(0));
            chai_1.assert.isUndefined(helper.getConversationAndMessageAtIndex(1));
            chai_1.assert.isUndefined(helper.getConversationAndMessageAtIndex(-1));
        });
    });
    describe('getConversationAndMessageInDirection', () => {
        it('returns the next conversation when searching downward', () => {
            const pinnedConversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const conversations = [(0, getDefaultConversation_1.getDefaultConversation)()];
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations,
                pinnedConversations }));
            chai_1.assert.deepEqual(helper.getConversationAndMessageInDirection({ direction: LeftPaneHelper_1.FindDirection.Down, unreadOnly: false }, pinnedConversations[1].id, undefined), { conversationId: conversations[0].id });
        });
        // Additional tests are found with `getConversationInDirection`.
    });
    describe('requiresFullWidth', () => {
        it("returns false if we're not about to search in a conversation and there's at least one", () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.isFalse(helper.requiresFullWidth());
        });
        it('returns true if there are no conversations', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(defaultProps);
            chai_1.assert.isTrue(helper.requiresFullWidth());
        });
        it("returns true if we're about to search", () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { startSearchCounter: 1 }));
            chai_1.assert.isTrue(helper.requiresFullWidth());
        });
        it("returns true if we're about to search in a conversation", () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { isAboutToSearchInAConversation: true }));
            chai_1.assert.isTrue(helper.requiresFullWidth());
        });
    });
    describe('shouldRecomputeRowHeights', () => {
        it("returns false if the number of conversations in each section doesn't change", () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights(Object.assign(Object.assign({}, defaultProps), { conversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], archivedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] })));
        });
        it('returns false if the only thing changed is whether conversations are archived', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights(Object.assign(Object.assign({}, defaultProps), { conversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] })));
        });
        it('returns false if the only thing changed is the number of non-pinned conversations', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights(Object.assign(Object.assign({}, defaultProps), { conversations: [(0, getDefaultConversation_1.getDefaultConversation)()], pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], archivedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] })));
        });
        it('returns true if the number of pinned conversations changes', () => {
            const helper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(Object.assign(Object.assign({}, defaultProps), { conversations: [(0, getDefaultConversation_1.getDefaultConversation)()], pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] }));
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights(Object.assign(Object.assign({}, defaultProps), { conversations: [(0, getDefaultConversation_1.getDefaultConversation)()], pinnedConversations: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] })));
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights(Object.assign(Object.assign({}, defaultProps), { conversations: [(0, getDefaultConversation_1.getDefaultConversation)()], pinnedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()], archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] })));
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights(Object.assign(Object.assign({}, defaultProps), { conversations: [(0, getDefaultConversation_1.getDefaultConversation)()], pinnedConversations: [], archivedConversations: [(0, getDefaultConversation_1.getDefaultConversation)()] })));
        });
    });
});
