"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const uuid_1 = require("uuid");
const ConversationList_1 = require("../../../components/ConversationList");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const LeftPaneSearchHelper_1 = require("../../../components/leftPane/LeftPaneSearchHelper");
describe('LeftPaneSearchHelper', () => {
    const fakeMessage = () => ({
        id: (0, uuid_1.v4)(),
        conversationId: (0, uuid_1.v4)(),
    });
    describe('getBackAction', () => {
        it('returns undefined; going back is handled elsewhere in the app', () => {
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: { isLoading: false, results: [] },
                contactResults: { isLoading: false, results: [] },
                messageResults: { isLoading: false, results: [] },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.isUndefined(helper.getBackAction({
                showChooseGroupMembers: sinon.fake(),
                showInbox: sinon.fake(),
                startComposing: sinon.fake(),
            }));
        });
    });
    describe('getRowCount', () => {
        it('returns 100 if any results are loading', () => {
            chai_1.assert.strictEqual(new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: { isLoading: true },
                contactResults: { isLoading: true },
                messageResults: { isLoading: true },
                searchTerm: 'foo',
                primarySendsSms: false,
            }).getRowCount(), 100);
            chai_1.assert.strictEqual(new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: {
                    isLoading: false,
                    results: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                },
                contactResults: { isLoading: true },
                messageResults: { isLoading: true },
                searchTerm: 'foo',
                primarySendsSms: false,
            }).getRowCount(), 100);
            chai_1.assert.strictEqual(new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: { isLoading: true },
                contactResults: { isLoading: true },
                messageResults: { isLoading: false, results: [fakeMessage()] },
                searchTerm: 'foo',
                primarySendsSms: false,
            }).getRowCount(), 100);
        });
        it('returns 0 when there are no search results', () => {
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: { isLoading: false, results: [] },
                contactResults: { isLoading: false, results: [] },
                messageResults: { isLoading: false, results: [] },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.strictEqual(helper.getRowCount(), 0);
        });
        it('returns 1 + the number of results, dropping empty sections', () => {
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: {
                    isLoading: false,
                    results: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                },
                contactResults: { isLoading: false, results: [] },
                messageResults: { isLoading: false, results: [fakeMessage()] },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.strictEqual(helper.getRowCount(), 5);
        });
    });
    describe('getRow', () => {
        it('returns a "loading search results" row if any results are loading', () => {
            const helpers = [
                new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                    conversationResults: { isLoading: true },
                    contactResults: { isLoading: true },
                    messageResults: { isLoading: true },
                    searchTerm: 'foo',
                    primarySendsSms: false,
                }),
                new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                    conversationResults: {
                        isLoading: false,
                        results: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                    },
                    contactResults: { isLoading: true },
                    messageResults: { isLoading: true },
                    searchTerm: 'foo',
                    primarySendsSms: false,
                }),
                new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                    conversationResults: { isLoading: true },
                    contactResults: { isLoading: true },
                    messageResults: { isLoading: false, results: [fakeMessage()] },
                    searchTerm: 'foo',
                    primarySendsSms: false,
                }),
            ];
            helpers.forEach(helper => {
                chai_1.assert.deepEqual(helper.getRow(0), {
                    type: ConversationList_1.RowType.SearchResultsLoadingFakeHeader,
                });
                for (let i = 1; i < 99; i += 1) {
                    chai_1.assert.deepEqual(helper.getRow(i), {
                        type: ConversationList_1.RowType.SearchResultsLoadingFakeRow,
                    });
                }
                chai_1.assert.isUndefined(helper.getRow(100));
            });
        });
        it('returns header + results when all sections have loaded with results', () => {
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const contacts = [(0, getDefaultConversation_1.getDefaultConversation)()];
            const messages = [fakeMessage(), fakeMessage()];
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: {
                    isLoading: false,
                    results: conversations,
                },
                contactResults: { isLoading: false, results: contacts },
                messageResults: { isLoading: false, results: messages },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'conversationsHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[1],
            });
            chai_1.assert.deepEqual(helper.getRow(3), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'contactsHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(4), {
                type: ConversationList_1.RowType.Conversation,
                conversation: contacts[0],
            });
            chai_1.assert.deepEqual(helper.getRow(5), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'messagesHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(6), {
                type: ConversationList_1.RowType.MessageSearchResult,
                messageId: messages[0].id,
            });
            chai_1.assert.deepEqual(helper.getRow(7), {
                type: ConversationList_1.RowType.MessageSearchResult,
                messageId: messages[1].id,
            });
        });
        it('omits conversations when there are no conversation results', () => {
            const contacts = [(0, getDefaultConversation_1.getDefaultConversation)()];
            const messages = [fakeMessage(), fakeMessage()];
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: {
                    isLoading: false,
                    results: [],
                },
                contactResults: { isLoading: false, results: contacts },
                messageResults: { isLoading: false, results: messages },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'contactsHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: contacts[0],
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'messagesHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(3), {
                type: ConversationList_1.RowType.MessageSearchResult,
                messageId: messages[0].id,
            });
            chai_1.assert.deepEqual(helper.getRow(4), {
                type: ConversationList_1.RowType.MessageSearchResult,
                messageId: messages[1].id,
            });
        });
        it('omits contacts when there are no contact results', () => {
            const conversations = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const messages = [fakeMessage(), fakeMessage()];
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: {
                    isLoading: false,
                    results: conversations,
                },
                contactResults: { isLoading: false, results: [] },
                messageResults: { isLoading: false, results: messages },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'conversationsHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[0],
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Conversation,
                conversation: conversations[1],
            });
            chai_1.assert.deepEqual(helper.getRow(3), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'messagesHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(4), {
                type: ConversationList_1.RowType.MessageSearchResult,
                messageId: messages[0].id,
            });
            chai_1.assert.deepEqual(helper.getRow(5), {
                type: ConversationList_1.RowType.MessageSearchResult,
                messageId: messages[1].id,
            });
        });
    });
    it('omits messages when there are no message results', () => {
        const conversations = [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()];
        const contacts = [(0, getDefaultConversation_1.getDefaultConversation)()];
        const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
            conversationResults: {
                isLoading: false,
                results: conversations,
            },
            contactResults: { isLoading: false, results: contacts },
            messageResults: { isLoading: false, results: [] },
            searchTerm: 'foo',
            primarySendsSms: false,
        });
        chai_1.assert.deepEqual(helper.getRow(0), {
            type: ConversationList_1.RowType.Header,
            i18nKey: 'conversationsHeader',
        });
        chai_1.assert.deepEqual(helper.getRow(1), {
            type: ConversationList_1.RowType.Conversation,
            conversation: conversations[0],
        });
        chai_1.assert.deepEqual(helper.getRow(2), {
            type: ConversationList_1.RowType.Conversation,
            conversation: conversations[1],
        });
        chai_1.assert.deepEqual(helper.getRow(3), {
            type: ConversationList_1.RowType.Header,
            i18nKey: 'contactsHeader',
        });
        chai_1.assert.deepEqual(helper.getRow(4), {
            type: ConversationList_1.RowType.Conversation,
            conversation: contacts[0],
        });
        chai_1.assert.isUndefined(helper.getRow(5));
    });
    describe('isScrollable', () => {
        it('returns false if any results are loading', () => {
            const helpers = [
                new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                    conversationResults: { isLoading: true },
                    contactResults: { isLoading: true },
                    messageResults: { isLoading: true },
                    searchTerm: 'foo',
                    primarySendsSms: false,
                }),
                new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                    conversationResults: {
                        isLoading: false,
                        results: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                    },
                    contactResults: { isLoading: true },
                    messageResults: { isLoading: true },
                    searchTerm: 'foo',
                    primarySendsSms: false,
                }),
                new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                    conversationResults: { isLoading: true },
                    contactResults: { isLoading: true },
                    messageResults: { isLoading: false, results: [fakeMessage()] },
                    searchTerm: 'foo',
                    primarySendsSms: false,
                }),
            ];
            helpers.forEach(helper => {
                chai_1.assert.isFalse(helper.isScrollable());
            });
        });
        it('returns true if all results have loaded', () => {
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: {
                    isLoading: false,
                    results: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                },
                contactResults: { isLoading: false, results: [] },
                messageResults: {
                    isLoading: false,
                    results: [fakeMessage(), fakeMessage(), fakeMessage()],
                },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.isTrue(helper.isScrollable());
        });
    });
    describe('shouldRecomputeRowHeights', () => {
        it("returns false if the number of results doesn't change", () => {
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: {
                    isLoading: false,
                    results: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                },
                contactResults: { isLoading: false, results: [] },
                messageResults: {
                    isLoading: false,
                    results: [fakeMessage(), fakeMessage(), fakeMessage()],
                },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights({
                conversationResults: {
                    isLoading: false,
                    results: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                },
                contactResults: { isLoading: false, results: [] },
                messageResults: {
                    isLoading: false,
                    results: [fakeMessage(), fakeMessage(), fakeMessage()],
                },
                searchTerm: 'bar',
                primarySendsSms: false,
            }));
        });
        it('returns false when a section completes loading, but not all sections are done (because the pane is still loading overall)', () => {
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: { isLoading: true },
                contactResults: { isLoading: true },
                messageResults: { isLoading: true },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights({
                conversationResults: {
                    isLoading: false,
                    results: [(0, getDefaultConversation_1.getDefaultConversation)()],
                },
                contactResults: { isLoading: true },
                messageResults: { isLoading: true },
                searchTerm: 'bar',
                primarySendsSms: false,
            }));
        });
        it('returns true when all sections finish loading', () => {
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: { isLoading: true },
                contactResults: { isLoading: true },
                messageResults: { isLoading: false, results: [fakeMessage()] },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights({
                conversationResults: {
                    isLoading: false,
                    results: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                },
                contactResults: { isLoading: false, results: [] },
                messageResults: { isLoading: false, results: [fakeMessage()] },
                searchTerm: 'foo',
                primarySendsSms: false,
            }));
        });
        it('returns true if the number of results in a section changes', () => {
            const helper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper({
                conversationResults: {
                    isLoading: false,
                    results: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                },
                contactResults: { isLoading: false, results: [] },
                messageResults: { isLoading: false, results: [] },
                searchTerm: 'foo',
                primarySendsSms: false,
            });
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights({
                conversationResults: {
                    isLoading: false,
                    results: [(0, getDefaultConversation_1.getDefaultConversation)()],
                },
                contactResults: { isLoading: true },
                messageResults: { isLoading: true },
                searchTerm: 'bar',
                primarySendsSms: false,
            }));
        });
    });
});
