"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const lodash_1 = require("lodash");
const ConversationList_1 = require("../../../components/ConversationList");
const remoteConfig = __importStar(require("../../../RemoteConfig"));
const ContactCheckbox_1 = require("../../../components/conversationList/ContactCheckbox");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const LeftPaneChooseGroupMembersHelper_1 = require("../../../components/leftPane/LeftPaneChooseGroupMembersHelper");
describe('LeftPaneChooseGroupMembersHelper', () => {
    const defaults = {
        candidateContacts: [],
        cantAddContactForModal: undefined,
        isShowingRecommendedGroupSizeModal: false,
        isShowingMaximumGroupSizeModal: false,
        searchTerm: '',
        selectedContacts: [],
    };
    let sinonSandbox;
    beforeEach(() => {
        sinonSandbox = sinon.createSandbox();
        sinonSandbox
            .stub(remoteConfig, 'getValue')
            .withArgs('global.groupsv2.maxGroupSize')
            .returns('22')
            .withArgs('global.groupsv2.groupSizeHardLimit')
            .returns('33');
    });
    afterEach(() => {
        sinonSandbox.restore();
    });
    describe('getBackAction', () => {
        it('returns the "show composer" action', () => {
            const startComposing = sinon.fake();
            const helper = new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(defaults);
            chai_1.assert.strictEqual(helper.getBackAction({ startComposing }), startComposing);
        });
    });
    describe('getRowCount', () => {
        it('returns 0 if there are no contacts', () => {
            chai_1.assert.strictEqual(new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(Object.assign(Object.assign({}, defaults), { candidateContacts: [], searchTerm: '', selectedContacts: [(0, getDefaultConversation_1.getDefaultConversation)()] })).getRowCount(), 0);
            chai_1.assert.strictEqual(new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(Object.assign(Object.assign({}, defaults), { candidateContacts: [], searchTerm: 'foo bar', selectedContacts: [(0, getDefaultConversation_1.getDefaultConversation)()] })).getRowCount(), 0);
        });
        it('returns the number of candidate contacts + 2 if there are any', () => {
            chai_1.assert.strictEqual(new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(Object.assign(Object.assign({}, defaults), { candidateContacts: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ], searchTerm: '', selectedContacts: [(0, getDefaultConversation_1.getDefaultConversation)()] })).getRowCount(), 4);
        });
    });
    describe('getRow', () => {
        it('returns undefined if there are no contacts', () => {
            chai_1.assert.isUndefined(new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(Object.assign(Object.assign({}, defaults), { candidateContacts: [], searchTerm: '', selectedContacts: [(0, getDefaultConversation_1.getDefaultConversation)()] })).getRow(0));
            chai_1.assert.isUndefined(new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(Object.assign(Object.assign({}, defaults), { candidateContacts: [], searchTerm: '', selectedContacts: [(0, getDefaultConversation_1.getDefaultConversation)()] })).getRow(99));
            chai_1.assert.isUndefined(new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(Object.assign(Object.assign({}, defaults), { candidateContacts: [], searchTerm: 'foo bar', selectedContacts: [(0, getDefaultConversation_1.getDefaultConversation)()] })).getRow(0));
        });
        it('returns a header, then the contacts, then a blank space if there are contacts', () => {
            const candidateContacts = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(Object.assign(Object.assign({}, defaults), { candidateContacts, searchTerm: 'foo bar', selectedContacts: [candidateContacts[1]] }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'contactsHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.ContactCheckbox,
                contact: candidateContacts[0],
                isChecked: false,
                disabledReason: undefined,
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.ContactCheckbox,
                contact: candidateContacts[1],
                isChecked: true,
                disabledReason: undefined,
            });
            chai_1.assert.deepEqual(helper.getRow(3), { type: ConversationList_1.RowType.Blank });
        });
        it("disables non-selected contact checkboxes if you've selected the maximum number of contacts", () => {
            const candidateContacts = (0, lodash_1.times)(50, () => (0, getDefaultConversation_1.getDefaultConversation)());
            const helper = new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(Object.assign(Object.assign({}, defaults), { candidateContacts, searchTerm: 'foo bar', selectedContacts: candidateContacts.slice(1, 33) }));
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.ContactCheckbox,
                contact: candidateContacts[0],
                isChecked: false,
                disabledReason: ContactCheckbox_1.ContactCheckboxDisabledReason.MaximumContactsSelected,
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.ContactCheckbox,
                contact: candidateContacts[1],
                isChecked: true,
                disabledReason: undefined,
            });
        });
        it("disables contacts that aren't GV2-capable, unless they are already selected somehow", () => {
            const candidateContacts = [
                Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)()), { isGroupV2Capable: false }),
                Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)()), { isGroupV2Capable: undefined }),
                Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)()), { isGroupV2Capable: false }),
            ];
            const helper = new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(Object.assign(Object.assign({}, defaults), { candidateContacts, searchTerm: 'foo bar', selectedContacts: [candidateContacts[2]] }));
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.ContactCheckbox,
                contact: candidateContacts[0],
                isChecked: false,
                disabledReason: ContactCheckbox_1.ContactCheckboxDisabledReason.NotCapable,
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.ContactCheckbox,
                contact: candidateContacts[1],
                isChecked: false,
                disabledReason: ContactCheckbox_1.ContactCheckboxDisabledReason.NotCapable,
            });
            chai_1.assert.deepEqual(helper.getRow(3), {
                type: ConversationList_1.RowType.ContactCheckbox,
                contact: candidateContacts[2],
                isChecked: true,
                disabledReason: undefined,
            });
        });
    });
});
