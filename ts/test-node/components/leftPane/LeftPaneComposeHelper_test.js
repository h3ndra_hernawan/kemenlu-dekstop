"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const ConversationList_1 = require("../../../components/ConversationList");
const LeftPaneHelper_1 = require("../../../components/leftPane/LeftPaneHelper");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const LeftPaneComposeHelper_1 = require("../../../components/leftPane/LeftPaneComposeHelper");
describe('LeftPaneComposeHelper', () => {
    let sinonSandbox;
    beforeEach(() => {
        sinonSandbox = sinon.createSandbox();
    });
    afterEach(() => {
        sinonSandbox.restore();
    });
    describe('getBackAction', () => {
        it('returns the "show inbox" action', () => {
            const showInbox = sinon.fake();
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.strictEqual(helper.getBackAction({ showInbox }), showInbox);
        });
    });
    describe('getRowCount', () => {
        it('returns 1 (for the "new group" button) if not searching and there are no contacts', () => {
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 1);
        });
        it('returns the number of contacts + 2 (for the "new group" button and header) if not searching', () => {
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 4);
        });
        it('returns the number of contacts + number of groups + 3 (for the "new group" button and the headers) if not searching', () => {
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 7);
        });
        it('returns the number of contacts, number groups + 4 (for headers and username)', () => {
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                regionCode: 'US',
                searchTerm: 'someone',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 8);
        });
        it('if usernames are disabled, two less rows are shown', () => {
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                regionCode: 'US',
                searchTerm: 'someone',
                isUsernamesEnabled: false,
                isFetchingUsername: false,
            }).getRowCount(), 6);
        });
        it('returns the number of conversations + the headers, but not for a phone number', () => {
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 2);
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 5);
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [(0, getDefaultConversation_1.getDefaultConversation)()],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 7);
        });
        it('returns 1 (for the "Start new conversation" button) if searching for a phone number with no contacts', () => {
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '+16505551234',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 1);
        });
        it('returns 2 if just username in results', () => {
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'someone',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 2);
        });
        it('returns the number of contacts + 4 (for the "Start new conversation" button and header) if searching for a phone number', () => {
            chai_1.assert.strictEqual(new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '+16505551234',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }).getRowCount(), 4);
        });
    });
    describe('getRow', () => {
        it('returns a "new group" button if not searching and there are no contacts', () => {
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.CreateNewGroup,
            });
            chai_1.assert.isUndefined(helper.getRow(1));
        });
        it('returns a "new group" button, a header, and contacts if not searching', () => {
            const composeContacts = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts,
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.CreateNewGroup,
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'contactsHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Contact,
                contact: composeContacts[0],
            });
            chai_1.assert.deepEqual(helper.getRow(3), {
                type: ConversationList_1.RowType.Contact,
                contact: composeContacts[1],
            });
        });
        it('returns a "new group" button, a header, contacts, groups header, and groups -- if not searching', () => {
            const composeContacts = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const composeGroups = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts,
                composeGroups,
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.CreateNewGroup,
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'contactsHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Contact,
                contact: composeContacts[0],
            });
            chai_1.assert.deepEqual(helper.getRow(3), {
                type: ConversationList_1.RowType.Contact,
                contact: composeContacts[1],
            });
            chai_1.assert.deepEqual(helper.getRow(4), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'groupsHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(5), {
                type: ConversationList_1.RowType.Conversation,
                conversation: composeGroups[0],
            });
            chai_1.assert.deepEqual(helper.getRow(6), {
                type: ConversationList_1.RowType.Conversation,
                conversation: composeGroups[1],
            });
        });
        it('returns no rows if searching, no results, and usernames are disabled', () => {
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: false,
                isFetchingUsername: false,
            });
            chai_1.assert.isUndefined(helper.getRow(0));
            chai_1.assert.isUndefined(helper.getRow(1));
        });
        it('returns one row per contact if searching', () => {
            const composeContacts = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts,
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Contact,
                contact: composeContacts[0],
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Contact,
                contact: composeContacts[1],
            });
        });
        it('returns a "start new conversation" row if searching for a phone number and there are no results', () => {
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '+16505551234',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.StartNewConversation,
                phoneNumber: '+16505551234',
            });
            chai_1.assert.isUndefined(helper.getRow(1));
        });
        it('returns just a "find by username" header if no results', () => {
            const username = 'someone';
            const isFetchingUsername = true;
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: username,
                isUsernamesEnabled: true,
                isFetchingUsername,
            });
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'findByUsernameHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.UsernameSearchResult,
                username,
                isFetchingUsername,
            });
            chai_1.assert.isUndefined(helper.getRow(2));
        });
        it('returns a "start new conversation" row, a header, and contacts if searching for a phone number', () => {
            const composeContacts = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts,
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '+16505551234',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.StartNewConversation,
                phoneNumber: '+16505551234',
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'contactsHeader',
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Contact,
                contact: composeContacts[0],
            });
            chai_1.assert.deepEqual(helper.getRow(3), {
                type: ConversationList_1.RowType.Contact,
                contact: composeContacts[1],
            });
        });
    });
    describe('getConversationAndMessageAtIndex', () => {
        it('returns undefined because keyboard shortcuts are not supported', () => {
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.isUndefined(helper.getConversationAndMessageAtIndex(0));
        });
    });
    describe('getConversationAndMessageInDirection', () => {
        it('returns undefined because keyboard shortcuts are not supported', () => {
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.isUndefined(helper.getConversationAndMessageInDirection({ direction: LeftPaneHelper_1.FindDirection.Down, unreadOnly: false }, undefined, undefined));
        });
    });
    describe('shouldRecomputeRowHeights', () => {
        it('returns false if just search changes, so "Find by username" header is in same position', () => {
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'different search',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights({
                composeContacts: [],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'last search',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
        });
        it('returns true if "Find by usernames" header changes location or goes away', () => {
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
            chai_1.assert.isFalse(helper.shouldRecomputeRowHeights({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '+16505559876',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
        });
        it('returns true if search changes or becomes an e164', () => {
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '+16505551234',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
        });
        it('returns true if going from no search to some search (showing "Find by username" section)', () => {
            const helper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: '',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.isTrue(helper.shouldRecomputeRowHeights({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
        });
        it('should be true if going from contact to group or vice versa', () => {
            const helperContacts = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.isTrue(helperContacts.shouldRecomputeRowHeights({
                composeContacts: [],
                composeGroups: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
            const helperGroups = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [],
                composeGroups: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.isTrue(helperGroups.shouldRecomputeRowHeights({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [],
                regionCode: 'US',
                searchTerm: 'foo bar',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
        });
        it('should be true if the headers are in different row indices as before', () => {
            const helperContacts = new LeftPaneComposeHelper_1.LeftPaneComposeHelper({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [(0, getDefaultConversation_1.getDefaultConversation)()],
                regionCode: 'US',
                searchTerm: 'soup',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            });
            chai_1.assert.isTrue(helperContacts.shouldRecomputeRowHeights({
                composeContacts: [(0, getDefaultConversation_1.getDefaultConversation)()],
                composeGroups: [(0, getDefaultConversation_1.getDefaultConversation)(), (0, getDefaultConversation_1.getDefaultConversation)()],
                regionCode: 'US',
                searchTerm: 'soup',
                isUsernamesEnabled: true,
                isFetchingUsername: false,
            }));
        });
    });
});
