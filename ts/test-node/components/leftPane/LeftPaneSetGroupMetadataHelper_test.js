"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const ConversationList_1 = require("../../../components/ConversationList");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const LeftPaneSetGroupMetadataHelper_1 = require("../../../components/leftPane/LeftPaneSetGroupMetadataHelper");
function getComposeState() {
    return {
        groupAvatar: undefined,
        groupExpireTimer: 0,
        groupName: '',
        hasError: false,
        isCreating: false,
        isEditingAvatar: false,
        selectedContacts: [],
        userAvatarData: [],
    };
}
describe('LeftPaneSetGroupMetadataHelper', () => {
    describe('getBackAction', () => {
        it('returns the "show composer" action if a request is not active', () => {
            const showChooseGroupMembers = sinon.fake();
            const helper = new LeftPaneSetGroupMetadataHelper_1.LeftPaneSetGroupMetadataHelper(Object.assign({}, getComposeState()));
            chai_1.assert.strictEqual(helper.getBackAction({ showChooseGroupMembers }), showChooseGroupMembers);
        });
        it("returns undefined (i.e., you can't go back) if a request is active", () => {
            const helper = new LeftPaneSetGroupMetadataHelper_1.LeftPaneSetGroupMetadataHelper(Object.assign(Object.assign({}, getComposeState()), { groupName: 'Foo Bar', isCreating: true }));
            chai_1.assert.isUndefined(helper.getBackAction({ showChooseGroupMembers: sinon.fake() }));
        });
    });
    describe('getRowCount', () => {
        it('returns 0 if there are no contacts', () => {
            chai_1.assert.strictEqual(new LeftPaneSetGroupMetadataHelper_1.LeftPaneSetGroupMetadataHelper(Object.assign({}, getComposeState())).getRowCount(), 0);
        });
        it('returns the number of candidate contacts + 2 if there are any', () => {
            chai_1.assert.strictEqual(new LeftPaneSetGroupMetadataHelper_1.LeftPaneSetGroupMetadataHelper(Object.assign(Object.assign({}, getComposeState()), { selectedContacts: [
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                    (0, getDefaultConversation_1.getDefaultConversation)(),
                ] })).getRowCount(), 4);
        });
    });
    describe('getRow', () => {
        it('returns undefined if there are no contacts', () => {
            chai_1.assert.isUndefined(new LeftPaneSetGroupMetadataHelper_1.LeftPaneSetGroupMetadataHelper(Object.assign({}, getComposeState())).getRow(0));
        });
        it('returns a header, then the contacts, then a blank space if there are contacts', () => {
            const selectedContacts = [
                (0, getDefaultConversation_1.getDefaultConversation)(),
                (0, getDefaultConversation_1.getDefaultConversation)(),
            ];
            const helper = new LeftPaneSetGroupMetadataHelper_1.LeftPaneSetGroupMetadataHelper(Object.assign(Object.assign({}, getComposeState()), { selectedContacts }));
            chai_1.assert.deepEqual(helper.getRow(0), {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'setGroupMetadata__members-header',
            });
            chai_1.assert.deepEqual(helper.getRow(1), {
                type: ConversationList_1.RowType.Contact,
                contact: selectedContacts[0],
                isClickable: false,
            });
            chai_1.assert.deepEqual(helper.getRow(2), {
                type: ConversationList_1.RowType.Contact,
                contact: selectedContacts[1],
                isClickable: false,
            });
            chai_1.assert.deepEqual(helper.getRow(3), { type: ConversationList_1.RowType.Blank });
        });
    });
});
