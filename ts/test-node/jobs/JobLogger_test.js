"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const JobLogger_1 = require("../../jobs/JobLogger");
describe('JobLogger', () => {
    const LEVELS = ['fatal', 'error', 'warn', 'info', 'debug', 'trace'];
    const createFakeLogger = () => ({
        fatal: sinon.fake(),
        error: sinon.fake(),
        warn: sinon.fake(),
        info: sinon.fake(),
        debug: sinon.fake(),
        trace: sinon.fake(),
    });
    LEVELS.forEach(level => {
        describe(level, () => {
            it('logs its arguments with a prefix', () => {
                const fakeLogger = createFakeLogger();
                const logger = new JobLogger_1.JobLogger({ id: 'abc', queueType: 'test queue' }, fakeLogger);
                logger.attempt = 123;
                logger[level]('foo', 456);
                sinon.assert.calledOnce(fakeLogger[level]);
                sinon.assert.calledWith(fakeLogger[level], sinon.match((arg) => typeof arg === 'string' &&
                    arg.includes('test queue') &&
                    arg.includes('abc') &&
                    arg.includes('123')), 'foo', 456);
                LEVELS.filter(l => l !== level).forEach(otherLevel => {
                    sinon.assert.notCalled(fakeLogger[otherLevel]);
                });
            });
        });
    });
});
