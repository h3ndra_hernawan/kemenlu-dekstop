"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable max-classes-per-file */
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const events_1 = __importStar(require("events"));
const zod_1 = require("zod");
const lodash_1 = require("lodash");
const uuid_1 = require("uuid");
const p_queue_1 = __importDefault(require("p-queue"));
const JobError_1 = require("../../jobs/JobError");
const TestJobQueueStore_1 = require("./TestJobQueueStore");
const missingCaseError_1 = require("../../util/missingCaseError");
const helpers_1 = require("../helpers");
const JobQueue_1 = require("../../jobs/JobQueue");
describe('JobQueue', () => {
    describe('end-to-end tests', () => {
        it('writes jobs to the database, processes them, and then deletes them', async () => {
            const testJobSchema = zod_1.z.object({
                a: zod_1.z.number(),
                b: zod_1.z.number(),
            });
            const results = new Set();
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            class Queue extends JobQueue_1.JobQueue {
                parseData(data) {
                    return testJobSchema.parse(data);
                }
                async run({ data }) {
                    results.add(data.a + data.b);
                }
            }
            const addQueue = new Queue({
                store,
                queueType: 'test add queue',
                maxAttempts: 1,
            });
            chai_1.assert.deepEqual(results, new Set());
            chai_1.assert.isEmpty(store.storedJobs);
            addQueue.streamJobs();
            store.pauseStream('test add queue');
            const job1 = await addQueue.add({ a: 1, b: 2 });
            const job2 = await addQueue.add({ a: 3, b: 4 });
            chai_1.assert.deepEqual(results, new Set());
            chai_1.assert.lengthOf(store.storedJobs, 2);
            store.resumeStream('test add queue');
            await job1.completion;
            await job2.completion;
            chai_1.assert.deepEqual(results, new Set([3, 7]));
            chai_1.assert.isEmpty(store.storedJobs);
        });
        it('by default, kicks off multiple jobs in parallel', async () => {
            let activeJobCount = 0;
            const eventBus = new events_1.default();
            const updateActiveJobCount = (incrementBy) => {
                activeJobCount += incrementBy;
                eventBus.emit('updated');
            };
            class Queue extends JobQueue_1.JobQueue {
                parseData(data) {
                    return zod_1.z.number().parse(data);
                }
                async run() {
                    try {
                        updateActiveJobCount(1);
                        await new Promise(resolve => {
                            eventBus.on('updated', () => {
                                if (activeJobCount === 4) {
                                    eventBus.emit('got to 4');
                                    resolve();
                                }
                            });
                        });
                    }
                    finally {
                        updateActiveJobCount(-1);
                    }
                }
            }
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            const queue = new Queue({
                store,
                queueType: 'test queue',
                maxAttempts: 100,
            });
            queue.streamJobs();
            queue.add(1);
            queue.add(2);
            queue.add(3);
            queue.add(4);
            await (0, events_1.once)(eventBus, 'got to 4');
        });
        it('can override the in-memory queue', async () => {
            let jobsAdded = 0;
            const testQueue = new p_queue_1.default();
            testQueue.on('add', () => {
                jobsAdded += 1;
            });
            class Queue extends JobQueue_1.JobQueue {
                parseData(data) {
                    return zod_1.z.number().parse(data);
                }
                getInMemoryQueue(parsedJob) {
                    (0, chai_1.assert)(new Set([1, 2, 3, 4]).has(parsedJob.data), 'Bad data passed to `getInMemoryQueue`');
                    return testQueue;
                }
                run() {
                    return Promise.resolve();
                }
            }
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            const queue = new Queue({
                store,
                queueType: 'test queue',
                maxAttempts: 100,
            });
            queue.streamJobs();
            const jobs = await Promise.all([
                queue.add(1),
                queue.add(2),
                queue.add(3),
                queue.add(4),
            ]);
            await Promise.all(jobs.map(job => job.completion));
            chai_1.assert.strictEqual(jobsAdded, 4);
        });
        it('writes jobs to the database correctly', async () => {
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    return zod_1.z.string().parse(data);
                }
                async run() {
                    return Promise.resolve();
                }
            }
            const queue1 = new TestQueue({
                store,
                queueType: 'test 1',
                maxAttempts: 1,
            });
            const queue2 = new TestQueue({
                store,
                queueType: 'test 2',
                maxAttempts: 1,
            });
            store.pauseStream('test 1');
            store.pauseStream('test 2');
            queue1.streamJobs();
            queue2.streamJobs();
            await queue1.add('one');
            await queue2.add('A');
            await queue1.add('two');
            await queue2.add('B');
            await queue1.add('three');
            chai_1.assert.lengthOf(store.storedJobs, 5);
            const ids = store.storedJobs.map(job => job.id);
            chai_1.assert.lengthOf(store.storedJobs, new Set(ids).size, 'Expected every job to have a unique ID');
            const timestamps = store.storedJobs.map(job => job.timestamp);
            timestamps.forEach(timestamp => {
                chai_1.assert.approximately(timestamp, Date.now(), 3000, 'Expected the timestamp to be ~now');
            });
            const datas = store.storedJobs.map(job => job.data);
            chai_1.assert.sameMembers(datas, ['three', 'two', 'one', 'A', 'B'], "Expected every job's data to be stored");
            const queueTypes = (0, lodash_1.groupBy)(store.storedJobs, 'queueType');
            chai_1.assert.hasAllKeys(queueTypes, ['test 1', 'test 2']);
            chai_1.assert.lengthOf(queueTypes['test 1'], 3);
            chai_1.assert.lengthOf(queueTypes['test 2'], 2);
        });
        it('can override the insertion logic, skipping storage persistence', async () => {
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    return zod_1.z.string().parse(data);
                }
                async run() {
                    return Promise.resolve();
                }
            }
            const queue = new TestQueue({
                store,
                queueType: 'test queue',
                maxAttempts: 1,
            });
            queue.streamJobs();
            const insert = sinon.stub().resolves();
            await queue.add('foo bar', insert);
            chai_1.assert.lengthOf(store.storedJobs, 0);
            sinon.assert.calledOnce(insert);
            sinon.assert.calledWith(insert, sinon.match({
                id: sinon.match.string,
                timestamp: sinon.match.number,
                queueType: 'test queue',
                data: 'foo bar',
            }));
        });
        it('retries jobs, running them up to maxAttempts times', async () => {
            let fooAttempts = 0;
            let barAttempts = 0;
            let fooSucceeded = false;
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            class RetryQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    if (data !== 'foo' && data !== 'bar') {
                        throw new Error('Invalid data');
                    }
                    return data;
                }
                async run({ data }) {
                    switch (data) {
                        case 'foo':
                            fooAttempts += 1;
                            if (fooAttempts < 3) {
                                throw new Error('foo job should fail the first and second time');
                            }
                            fooSucceeded = true;
                            break;
                        case 'bar':
                            barAttempts += 1;
                            throw new Error('bar job always fails in this test');
                            break;
                        default:
                            throw (0, missingCaseError_1.missingCaseError)(data);
                    }
                }
            }
            const retryQueue = new RetryQueue({
                store,
                queueType: 'test retry queue',
                maxAttempts: 5,
            });
            retryQueue.streamJobs();
            await (await retryQueue.add('foo')).completion;
            let booErr;
            try {
                await (await retryQueue.add('bar')).completion;
            }
            catch (err) {
                booErr = err;
            }
            chai_1.assert.strictEqual(fooAttempts, 3);
            chai_1.assert.isTrue(fooSucceeded);
            chai_1.assert.strictEqual(barAttempts, 5);
            // Chai's `assert.instanceOf` doesn't tell TypeScript anything, so we do it here.
            if (!(booErr instanceof JobError_1.JobError)) {
                chai_1.assert.fail('Expected error to be a JobError');
                return;
            }
            chai_1.assert.include(booErr.message, 'bar job always fails in this test');
            chai_1.assert.isEmpty(store.storedJobs);
        });
        it('passes the attempt number to the run function', async () => {
            const attempts = [];
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    return zod_1.z.string().parse(data);
                }
                async run(_, { attempt }) {
                    attempts.push(attempt);
                    throw new Error('this job always fails');
                }
            }
            const queue = new TestQueue({
                store,
                queueType: 'test',
                maxAttempts: 6,
            });
            queue.streamJobs();
            try {
                await (await queue.add('foo')).completion;
            }
            catch (err) {
                // We expect this to fail.
            }
            chai_1.assert.deepStrictEqual(attempts, [1, 2, 3, 4, 5, 6]);
        });
        it('passes a logger to the run function', async () => {
            const uniqueString = (0, uuid_1.v4)();
            const fakeLogger = {
                fatal: sinon.fake(),
                error: sinon.fake(),
                warn: sinon.fake(),
                info: sinon.fake(),
                debug: sinon.fake(),
                trace: sinon.fake(),
            };
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    return zod_1.z.number().parse(data);
                }
                async run(_, { log }) {
                    log.info(uniqueString);
                    log.warn(uniqueString);
                    log.error(uniqueString);
                }
            }
            const queue = new TestQueue({
                store: new TestJobQueueStore_1.TestJobQueueStore(),
                queueType: 'test queue 123',
                maxAttempts: 6,
                logger: fakeLogger,
            });
            queue.streamJobs();
            const job = await queue.add(1);
            await job.completion;
            [fakeLogger.info, fakeLogger.warn, fakeLogger.error].forEach(logFn => {
                sinon.assert.calledWith(logFn, sinon.match((arg) => typeof arg === 'string' &&
                    arg.includes(job.id) &&
                    arg.includes('test queue 123')), sinon.match((arg) => typeof arg === 'string' && arg.includes(uniqueString)));
            });
        });
        it('makes job.completion reject if parseData throws', async () => {
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    if (data === 'valid') {
                        return data;
                    }
                    throw new Error('uh oh');
                }
                async run() {
                    return Promise.resolve();
                }
            }
            const queue = new TestQueue({
                store: new TestJobQueueStore_1.TestJobQueueStore(),
                queueType: 'test queue',
                maxAttempts: 999,
            });
            queue.streamJobs();
            const job = await queue.add('this will fail to parse');
            let jobError;
            try {
                await job.completion;
            }
            catch (err) {
                jobError = err;
            }
            // Chai's `assert.instanceOf` doesn't tell TypeScript anything, so we do it here.
            if (!(jobError instanceof JobError_1.JobError)) {
                chai_1.assert.fail('Expected error to be a JobError');
                return;
            }
            chai_1.assert.include(jobError.message, 'Failed to parse job data. Was unexpected data loaded from the database?');
        });
        it("doesn't run the job if parseData throws", async () => {
            const run = sinon.stub().resolves();
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    if (data === 'valid') {
                        return data;
                    }
                    throw new Error('invalid data!');
                }
                run(job) {
                    return run(job);
                }
            }
            const queue = new TestQueue({
                store: new TestJobQueueStore_1.TestJobQueueStore(),
                queueType: 'test queue',
                maxAttempts: 999,
            });
            queue.streamJobs();
            (await queue.add('invalid')).completion.catch(lodash_1.noop);
            (await queue.add('invalid')).completion.catch(lodash_1.noop);
            await queue.add('valid');
            (await queue.add('invalid')).completion.catch(lodash_1.noop);
            (await queue.add('invalid')).completion.catch(lodash_1.noop);
            sinon.assert.calledOnce(run);
            sinon.assert.calledWithMatch(run, { data: 'valid' });
        });
        it('keeps jobs in the storage if parseData throws', async () => {
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    if (data === 'valid') {
                        return data;
                    }
                    throw new Error('invalid data!');
                }
                async run() {
                    return Promise.resolve();
                }
            }
            const queue = new TestQueue({
                store,
                queueType: 'test queue',
                maxAttempts: 999,
            });
            queue.streamJobs();
            await (await queue.add('invalid 1')).completion.catch(lodash_1.noop);
            await (await queue.add('invalid 2')).completion.catch(lodash_1.noop);
            const datas = store.storedJobs.map(job => job.data);
            chai_1.assert.sameMembers(datas, ['invalid 1', 'invalid 2']);
        });
        it('adding the job resolves AFTER inserting the job into the database', async () => {
            let inserted = false;
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            store.events.on('insert', () => {
                inserted = true;
            });
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(_) {
                    return undefined;
                }
                async run() {
                    return Promise.resolve();
                }
            }
            const queue = new TestQueue({
                store,
                queueType: 'test queue',
                maxAttempts: 999,
            });
            queue.streamJobs();
            const addPromise = queue.add(undefined);
            chai_1.assert.isFalse(inserted);
            await addPromise;
            chai_1.assert.isTrue(inserted);
        });
        it('starts the job AFTER inserting the job into the database', async () => {
            const events = [];
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            store.events.on('insert', () => {
                events.push('insert');
            });
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    events.push('parsing data');
                    return data;
                }
                async run() {
                    events.push('running');
                }
            }
            const queue = new TestQueue({
                store,
                queueType: 'test queue',
                maxAttempts: 999,
            });
            queue.streamJobs();
            await (await queue.add(123)).completion;
            chai_1.assert.deepEqual(events, ['insert', 'parsing data', 'running']);
        });
        it('resolves job.completion AFTER deleting the job from the database', async () => {
            const events = [];
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            store.events.on('delete', () => {
                events.push('delete');
            });
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(_) {
                    return undefined;
                }
                async run() {
                    return Promise.resolve();
                }
            }
            const queue = new TestQueue({
                store,
                queueType: 'test queue',
                maxAttempts: 999,
            });
            queue.streamJobs();
            store.pauseStream('test queue');
            const job = await queue.add(undefined);
            // eslint-disable-next-line more/no-then
            const jobCompletionPromise = job.completion.then(() => {
                events.push('resolved');
            });
            chai_1.assert.lengthOf(store.storedJobs, 1);
            store.resumeStream('test queue');
            await jobCompletionPromise;
            chai_1.assert.deepEqual(events, ['delete', 'resolved']);
        });
        it('if the job fails after every attempt, rejects job.completion AFTER deleting the job from the database', async () => {
            const events = [];
            const store = new TestJobQueueStore_1.TestJobQueueStore();
            store.events.on('delete', () => {
                events.push('delete');
            });
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(_) {
                    return undefined;
                }
                async run() {
                    events.push('running');
                    throw new Error('uh oh');
                }
            }
            const queue = new TestQueue({
                store,
                queueType: 'test queue',
                maxAttempts: 5,
            });
            queue.streamJobs();
            store.pauseStream('test queue');
            const job = await queue.add(undefined);
            const jobCompletionPromise = job.completion.catch(() => {
                events.push('rejected');
            });
            chai_1.assert.lengthOf(store.storedJobs, 1);
            store.resumeStream('test queue');
            await jobCompletionPromise;
            chai_1.assert.deepEqual(events, [
                'running',
                'running',
                'running',
                'running',
                'running',
                'delete',
                'rejected',
            ]);
        });
    });
    describe('streamJobs', () => {
        const storedJobSchema = zod_1.z.object({
            id: zod_1.z.string(),
            timestamp: zod_1.z.number(),
            queueType: zod_1.z.string(),
            data: zod_1.z.unknown(),
        });
        class FakeStream {
            constructor() {
                this.eventEmitter = new events_1.default();
            }
            [Symbol.asyncIterator]() {
                return __asyncGenerator(this, arguments, function* _a() {
                    while (true) {
                        // eslint-disable-next-line no-await-in-loop
                        const [job] = yield __await((0, events_1.once)(this.eventEmitter, 'drip'));
                        yield yield __await(storedJobSchema.parse(job));
                    }
                });
            }
            drip(job) {
                this.eventEmitter.emit('drip', job);
            }
        }
        let fakeStream;
        let fakeStore;
        beforeEach(() => {
            fakeStream = new FakeStream();
            fakeStore = {
                insert: sinon.stub().resolves(),
                delete: sinon.stub().resolves(),
                stream: sinon.stub().returns(fakeStream),
            };
        });
        it('starts streaming jobs from the store', async () => {
            const eventEmitter = new events_1.default();
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    return zod_1.z.number().parse(data);
                }
                async run({ data }) {
                    eventEmitter.emit('run', data);
                }
            }
            const noopQueue = new TestQueue({
                store: fakeStore,
                queueType: 'test noop queue',
                maxAttempts: 99,
            });
            sinon.assert.notCalled(fakeStore.stream);
            noopQueue.streamJobs();
            sinon.assert.calledOnce(fakeStore.stream);
            fakeStream.drip({
                id: (0, uuid_1.v4)(),
                timestamp: Date.now(),
                queueType: 'test noop queue',
                data: 123,
            });
            const [firstRunData] = await (0, events_1.once)(eventEmitter, 'run');
            fakeStream.drip({
                id: (0, uuid_1.v4)(),
                timestamp: Date.now(),
                queueType: 'test noop queue',
                data: 456,
            });
            const [secondRunData] = await (0, events_1.once)(eventEmitter, 'run');
            chai_1.assert.strictEqual(firstRunData, 123);
            chai_1.assert.strictEqual(secondRunData, 456);
        });
        it('rejects when called more than once', async () => {
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(data) {
                    return data;
                }
                async run() {
                    return Promise.resolve();
                }
            }
            const noopQueue = new TestQueue({
                store: fakeStore,
                queueType: 'test noop queue',
                maxAttempts: 99,
            });
            noopQueue.streamJobs();
            await (0, helpers_1.assertRejects)(() => noopQueue.streamJobs());
            await (0, helpers_1.assertRejects)(() => noopQueue.streamJobs());
            sinon.assert.calledOnce(fakeStore.stream);
        });
    });
    describe('add', () => {
        it('rejects if the job queue has not started streaming', async () => {
            const fakeStore = {
                insert: sinon.stub().resolves(),
                delete: sinon.stub().resolves(),
                stream: sinon.stub(),
            };
            class TestQueue extends JobQueue_1.JobQueue {
                parseData(_) {
                    return undefined;
                }
                async run() {
                    return Promise.resolve();
                }
            }
            const noopQueue = new TestQueue({
                store: fakeStore,
                queueType: 'test noop queue',
                maxAttempts: 99,
            });
            await (0, helpers_1.assertRejects)(() => noopQueue.add(undefined));
            sinon.assert.notCalled(fakeStore.stream);
        });
    });
});
