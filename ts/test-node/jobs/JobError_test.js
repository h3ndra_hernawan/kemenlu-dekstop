"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const JobError_1 = require("../../jobs/JobError");
describe('JobError', () => {
    it('stores the provided argument as a property', () => {
        const fakeError = new Error('uh oh');
        const jobError1 = new JobError_1.JobError(fakeError);
        chai_1.assert.strictEqual(jobError1.lastErrorThrownByJob, fakeError);
        const jobError2 = new JobError_1.JobError(123);
        chai_1.assert.strictEqual(jobError2.lastErrorThrownByJob, 123);
    });
    it('if passed an Error, augments its `message`', () => {
        const fakeError = new Error('uh oh');
        const jobError = new JobError_1.JobError(fakeError);
        chai_1.assert.strictEqual(jobError.message, 'Job failed. Last error: uh oh');
    });
    it('if passed a non-Error, stringifies it', () => {
        const jobError = new JobError_1.JobError({ foo: 'bar' });
        chai_1.assert.strictEqual(jobError.message, 'Job failed. Last error: {"foo":"bar"}');
    });
});
