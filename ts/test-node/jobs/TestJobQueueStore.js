"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestJobQueueStore = void 0;
/* eslint-disable max-classes-per-file */
/* eslint-disable no-await-in-loop */
const events_1 = __importStar(require("events"));
const sleep_1 = require("../../util/sleep");
class TestJobQueueStore {
    constructor(jobs = []) {
        this.events = new events_1.default();
        this.openStreams = new Set();
        this.pipes = new Map();
        this.storedJobs = [];
        jobs.forEach(job => {
            this.insert(job);
        });
    }
    async insert(job, { shouldPersist = true } = {}) {
        await fakeDelay();
        this.storedJobs.forEach(storedJob => {
            if (job.id === storedJob.id) {
                throw new Error('Cannot store two jobs with the same ID');
            }
        });
        if (shouldPersist) {
            this.storedJobs.push(job);
        }
        this.getPipe(job.queueType).add(job);
        this.events.emit('insert');
    }
    async delete(id) {
        await fakeDelay();
        this.storedJobs = this.storedJobs.filter(job => job.id !== id);
        this.events.emit('delete');
    }
    stream(queueType) {
        if (this.openStreams.has(queueType)) {
            throw new Error('Cannot stream the same queueType more than once');
        }
        this.openStreams.add(queueType);
        return this.getPipe(queueType);
    }
    pauseStream(queueType) {
        return this.getPipe(queueType).pause();
    }
    resumeStream(queueType) {
        return this.getPipe(queueType).resume();
    }
    getPipe(queueType) {
        const existingPipe = this.pipes.get(queueType);
        if (existingPipe) {
            return existingPipe;
        }
        const result = new Pipe();
        this.pipes.set(queueType, result);
        return result;
    }
}
exports.TestJobQueueStore = TestJobQueueStore;
class Pipe {
    constructor() {
        this.queue = [];
        this.eventEmitter = new events_1.default();
        this.isLocked = false;
        this.isPaused = false;
    }
    add(value) {
        this.queue.push(value);
        this.eventEmitter.emit('add');
    }
    [Symbol.asyncIterator]() {
        return __asyncGenerator(this, arguments, function* _a() {
            if (this.isLocked) {
                throw new Error('Cannot iterate over a pipe more than once');
            }
            this.isLocked = true;
            while (true) {
                for (const value of this.queue) {
                    yield __await(this.waitForUnpaused());
                    yield yield __await(value);
                }
                this.queue = [];
                // We do this because we want to yield values in series.
                yield __await((0, events_1.once)(this.eventEmitter, 'add'));
            }
        });
    }
    pause() {
        this.isPaused = true;
    }
    resume() {
        this.isPaused = false;
        this.eventEmitter.emit('resume');
    }
    async waitForUnpaused() {
        if (this.isPaused) {
            await (0, events_1.once)(this.eventEmitter, 'resume');
        }
    }
}
function fakeDelay() {
    return (0, sleep_1.sleep)(0);
}
