"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Job_1 = require("../../jobs/Job");
describe('Job', () => {
    it('stores its arguments', () => {
        const id = 'abc123';
        const timestamp = Date.now();
        const queueType = 'test queue';
        const data = { foo: 'bar' };
        const completion = Promise.resolve();
        const job = new Job_1.Job(id, timestamp, queueType, data, completion);
        chai_1.assert.strictEqual(job.id, id);
        chai_1.assert.strictEqual(job.timestamp, timestamp);
        chai_1.assert.strictEqual(job.queueType, queueType);
        chai_1.assert.strictEqual(job.data, data);
        chai_1.assert.strictEqual(job.completion, completion);
    });
});
