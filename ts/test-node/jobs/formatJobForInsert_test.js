"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const formatJobForInsert_1 = require("../../jobs/formatJobForInsert");
describe('formatJobForInsert', () => {
    it('removes non-essential properties', () => {
        const input = {
            id: 'abc123',
            timestamp: 1234,
            queueType: 'test queue',
            data: { foo: 'bar' },
            extra: 'ignored',
            alsoIgnored: true,
        };
        const output = (0, formatJobForInsert_1.formatJobForInsert)(input);
        chai_1.assert.deepEqual(output, {
            id: 'abc123',
            timestamp: 1234,
            queueType: 'test queue',
            data: { foo: 'bar' },
        });
    });
});
