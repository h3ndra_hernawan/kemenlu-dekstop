"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const lodash_1 = require("lodash");
const JobQueueDatabaseStore_1 = require("../../jobs/JobQueueDatabaseStore");
describe('JobQueueDatabaseStore', () => {
    let fakeDatabase;
    beforeEach(() => {
        fakeDatabase = {
            getJobsInQueue: sinon.stub().resolves([]),
            insertJob: sinon.stub(),
            deleteJob: sinon.stub(),
        };
    });
    describe('insert', () => {
        it("fails if streaming hasn't started yet", async () => {
            const store = new JobQueueDatabaseStore_1.JobQueueDatabaseStore(fakeDatabase);
            let error;
            try {
                await store.insert({
                    id: 'abc',
                    timestamp: 1234,
                    queueType: 'test queue',
                    data: { hi: 5 },
                });
            }
            catch (err) {
                error = err;
            }
            chai_1.assert.instanceOf(error, Error);
        });
        it('adds jobs to the database', async () => {
            const store = new JobQueueDatabaseStore_1.JobQueueDatabaseStore(fakeDatabase);
            store.stream('test queue');
            await store.insert({
                id: 'abc',
                timestamp: 1234,
                queueType: 'test queue',
                data: { hi: 5 },
            });
            sinon.assert.calledOnce(fakeDatabase.insertJob);
            sinon.assert.calledWithMatch(fakeDatabase.insertJob, {
                id: 'abc',
                timestamp: 1234,
                queueType: 'test queue',
                data: { hi: 5 },
            });
        });
        it('enqueues jobs after putting them in the database', async () => {
            const events = [];
            fakeDatabase.insertJob.callsFake(() => {
                events.push('insert');
            });
            const store = new JobQueueDatabaseStore_1.JobQueueDatabaseStore(fakeDatabase);
            const streamPromise = (async () => {
                var e_1, _a;
                try {
                    // We don't actually care about using the variable from the async iterable.
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    for (var _b = __asyncValues(store.stream('test queue')), _c; _c = await _b.next(), !_c.done;) {
                        const _job = _c.value;
                        events.push('yielded job');
                        break;
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) await _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            })();
            await store.insert({
                id: 'abc',
                timestamp: 1234,
                queueType: 'test queue',
                data: { hi: 5 },
            });
            await streamPromise;
            chai_1.assert.deepEqual(events, ['insert', 'yielded job']);
        });
        it('can skip the database', async () => {
            const store = new JobQueueDatabaseStore_1.JobQueueDatabaseStore(fakeDatabase);
            const streamPromise = (async () => {
                var e_2, _a;
                try {
                    // We don't actually care about using the variable from the async iterable.
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    for (var _b = __asyncValues(store.stream('test queue')), _c; _c = await _b.next(), !_c.done;) {
                        const _job = _c.value;
                        break;
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) await _a.call(_b);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            })();
            await store.insert({
                id: 'abc',
                timestamp: 1234,
                queueType: 'test queue',
                data: { hi: 5 },
            }, { shouldPersist: false });
            await streamPromise;
            sinon.assert.notCalled(fakeDatabase.insertJob);
        });
        it("doesn't insert jobs until the initial fetch has completed", async () => {
            const events = [];
            let resolveGetJobsInQueue = lodash_1.noop;
            const getJobsInQueuePromise = new Promise(resolve => {
                resolveGetJobsInQueue = resolve;
            });
            fakeDatabase.getJobsInQueue.callsFake(() => {
                events.push('loaded jobs');
                return getJobsInQueuePromise;
            });
            fakeDatabase.insertJob.callsFake(() => {
                events.push('insert');
            });
            const store = new JobQueueDatabaseStore_1.JobQueueDatabaseStore(fakeDatabase);
            store.stream('test queue');
            const insertPromise = store.insert({
                id: 'abc',
                timestamp: 1234,
                queueType: 'test queue',
                data: { hi: 5 },
            });
            sinon.assert.notCalled(fakeDatabase.insertJob);
            resolveGetJobsInQueue([]);
            await insertPromise;
            sinon.assert.calledOnce(fakeDatabase.insertJob);
            chai_1.assert.deepEqual(events, ['loaded jobs', 'insert']);
        });
    });
    describe('delete', () => {
        it('deletes jobs from the database', async () => {
            const store = new JobQueueDatabaseStore_1.JobQueueDatabaseStore(fakeDatabase);
            await store.delete('xyz');
            sinon.assert.calledOnce(fakeDatabase.deleteJob);
            sinon.assert.calledWith(fakeDatabase.deleteJob, 'xyz');
        });
    });
    describe('stream', () => {
        it('yields all values in the database, then all values inserted', async () => {
            const makeJob = (id, queueType) => ({
                id,
                timestamp: Date.now(),
                queueType,
                data: { hi: 5 },
            });
            const ids = async (stream, amount) => {
                var e_3, _a;
                const result = [];
                try {
                    for (var stream_1 = __asyncValues(stream), stream_1_1; stream_1_1 = await stream_1.next(), !stream_1_1.done;) {
                        const job = stream_1_1.value;
                        result.push(job.id);
                        if (result.length >= amount) {
                            break;
                        }
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (stream_1_1 && !stream_1_1.done && (_a = stream_1.return)) await _a.call(stream_1);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
                return result;
            };
            fakeDatabase.getJobsInQueue
                .withArgs('queue A')
                .resolves([
                makeJob('A.1', 'queue A'),
                makeJob('A.2', 'queue A'),
                makeJob('A.3', 'queue A'),
            ]);
            fakeDatabase.getJobsInQueue.withArgs('queue B').resolves([]);
            fakeDatabase.getJobsInQueue
                .withArgs('queue C')
                .resolves([makeJob('C.1', 'queue C'), makeJob('C.2', 'queue C')]);
            const store = new JobQueueDatabaseStore_1.JobQueueDatabaseStore(fakeDatabase);
            const streamA = store.stream('queue A');
            const streamB = store.stream('queue B');
            const streamC = store.stream('queue C');
            await store.insert(makeJob('A.4', 'queue A'));
            await store.insert(makeJob('C.3', 'queue C'));
            await store.insert(makeJob('B.1', 'queue B'));
            await store.insert(makeJob('A.5', 'queue A'));
            const streamAIds = await ids(streamA, 5);
            const streamBIds = await ids(streamB, 1);
            const streamCIds = await ids(streamC, 3);
            chai_1.assert.deepEqual(streamAIds, ['A.1', 'A.2', 'A.3', 'A.4', 'A.5']);
            chai_1.assert.deepEqual(streamBIds, ['B.1']);
            chai_1.assert.deepEqual(streamCIds, ['C.1', 'C.2', 'C.3']);
            sinon.assert.calledThrice(fakeDatabase.getJobsInQueue);
        });
    });
});
