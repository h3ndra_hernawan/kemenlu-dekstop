"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const Errors_1 = require("../../../textsecure/Errors");
const durations = __importStar(require("../../../util/durations"));
const sleepFor413RetryAfterTime_1 = require("../../../jobs/helpers/sleepFor413RetryAfterTime");
describe('sleepFor413RetryAfterTimeIfApplicable', () => {
    const createLogger = () => ({ info: sinon.spy() });
    let sandbox;
    let clock;
    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sandbox.useFakeTimers();
    });
    afterEach(() => {
        sandbox.restore();
    });
    it('does nothing if no time remains', async () => {
        const log = createLogger();
        await Promise.all([-1, 0].map(timeRemaining => (0, sleepFor413RetryAfterTime_1.sleepFor413RetryAfterTime)({
            err: {},
            log,
            timeRemaining,
        })));
        sinon.assert.notCalled(log.info);
    });
    it('waits for 1 second if the error lacks Retry-After info', async () => {
        let done = false;
        (async () => {
            await (0, sleepFor413RetryAfterTime_1.sleepFor413RetryAfterTime)({
                err: {},
                log: createLogger(),
                timeRemaining: 1234,
            });
            done = true;
        })();
        await clock.tickAsync(999);
        chai_1.assert.isFalse(done);
        await clock.tickAsync(2);
        chai_1.assert.isTrue(done);
    });
    it('finds the Retry-After header on an HTTPError', async () => {
        const err = new Errors_1.HTTPError('Slow down', {
            code: 413,
            headers: { 'retry-after': '200' },
            response: {},
        });
        let done = false;
        (async () => {
            await (0, sleepFor413RetryAfterTime_1.sleepFor413RetryAfterTime)({
                err,
                log: createLogger(),
                timeRemaining: 123456789,
            });
            done = true;
        })();
        await clock.tickAsync(199 * durations.SECOND);
        chai_1.assert.isFalse(done);
        await clock.tickAsync(2 * durations.SECOND);
        chai_1.assert.isTrue(done);
    });
    it('finds the Retry-After on an HTTPError nested under a wrapper error', async () => {
        const httpError = new Errors_1.HTTPError('Slow down', {
            code: 413,
            headers: { 'retry-after': '200' },
            response: {},
        });
        let done = false;
        (async () => {
            await (0, sleepFor413RetryAfterTime_1.sleepFor413RetryAfterTime)({
                err: { httpError },
                log: createLogger(),
                timeRemaining: 123456789,
            });
            done = true;
        })();
        await clock.tickAsync(199 * durations.SECOND);
        chai_1.assert.isFalse(done);
        await clock.tickAsync(2 * durations.SECOND);
        chai_1.assert.isTrue(done);
    });
    it("won't wait longer than the remaining time", async () => {
        const err = new Errors_1.HTTPError('Slow down', {
            code: 413,
            headers: { 'retry-after': '99999' },
            response: {},
        });
        let done = false;
        (async () => {
            await (0, sleepFor413RetryAfterTime_1.sleepFor413RetryAfterTime)({
                err,
                log: createLogger(),
                timeRemaining: 3 * durations.SECOND,
            });
            done = true;
        })();
        await clock.tickAsync(4 * durations.SECOND);
        chai_1.assert.isTrue(done);
    });
    it('logs how long it will wait', async () => {
        const log = createLogger();
        const err = new Errors_1.HTTPError('Slow down', {
            code: 413,
            headers: { 'retry-after': '123' },
            response: {},
        });
        (0, sleepFor413RetryAfterTime_1.sleepFor413RetryAfterTime)({ err, log, timeRemaining: 9999999 });
        await clock.nextAsync();
        sinon.assert.calledOnce(log.info);
        sinon.assert.calledWith(log.info, sinon.match(/123000 millisecond\(s\)/));
    });
});
