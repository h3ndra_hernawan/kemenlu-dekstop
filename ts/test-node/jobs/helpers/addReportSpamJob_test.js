"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const Job_1 = require("../../../jobs/Job");
const addReportSpamJob_1 = require("../../../jobs/helpers/addReportSpamJob");
describe('addReportSpamJob', () => {
    let getMessageServerGuidsForSpam;
    let jobQueue;
    beforeEach(() => {
        getMessageServerGuidsForSpam = sinon.stub().resolves(['abc', 'xyz']);
        jobQueue = {
            add: sinon
                .stub()
                .callsFake(async (data) => new Job_1.Job('fake-job-id', Date.now(), 'fake job queue type', data, Promise.resolve())),
        };
    });
    it('does nothing if the conversation lacks an E164', async () => {
        await (0, addReportSpamJob_1.addReportSpamJob)({
            conversation: (0, getDefaultConversation_1.getDefaultConversation)({ e164: undefined }),
            getMessageServerGuidsForSpam,
            jobQueue,
        });
        sinon.assert.notCalled(getMessageServerGuidsForSpam);
        sinon.assert.notCalled(jobQueue.add);
    });
    it("doesn't enqueue a job if there are no messages with server GUIDs", async () => {
        getMessageServerGuidsForSpam.resolves([]);
        await (0, addReportSpamJob_1.addReportSpamJob)({
            conversation: (0, getDefaultConversation_1.getDefaultConversation)(),
            getMessageServerGuidsForSpam,
            jobQueue,
        });
        sinon.assert.notCalled(jobQueue.add);
    });
    it('enqueues a job', async () => {
        const conversation = (0, getDefaultConversation_1.getDefaultConversation)();
        await (0, addReportSpamJob_1.addReportSpamJob)({
            conversation,
            getMessageServerGuidsForSpam,
            jobQueue,
        });
        sinon.assert.calledOnce(getMessageServerGuidsForSpam);
        sinon.assert.calledWith(getMessageServerGuidsForSpam, conversation.id);
        sinon.assert.calledOnce(jobQueue.add);
        sinon.assert.calledWith(jobQueue.add, {
            e164: conversation.e164,
            serverGuids: ['abc', 'xyz'],
        });
    });
});
