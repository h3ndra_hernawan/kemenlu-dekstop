"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const InMemoryQueues_1 = require("../../../jobs/helpers/InMemoryQueues");
describe('InMemoryQueues', () => {
    describe('get', () => {
        it('returns a new PQueue for each key', () => {
            const queues = new InMemoryQueues_1.InMemoryQueues();
            chai_1.assert.strictEqual(queues.get('a'), queues.get('a'));
            chai_1.assert.notStrictEqual(queues.get('a'), queues.get('b'));
            chai_1.assert.notStrictEqual(queues.get('b'), queues.get('c'));
        });
        it('returns a queue that only executes one thing at a time', () => {
            const queue = new InMemoryQueues_1.InMemoryQueues().get('foo');
            chai_1.assert.strictEqual(queue.concurrency, 1);
        });
        it('cleans up the queues when all tasks have run', async () => {
            const queues = new InMemoryQueues_1.InMemoryQueues();
            const originalQueue = queues.get('foo');
            originalQueue.pause();
            const tasksPromise = originalQueue.addAll([
                async () => {
                    chai_1.assert.strictEqual(queues.get('foo'), originalQueue);
                },
                async () => {
                    chai_1.assert.strictEqual(queues.get('foo'), originalQueue);
                },
            ]);
            originalQueue.start();
            await tasksPromise;
            chai_1.assert.notStrictEqual(queues.get('foo'), originalQueue);
        });
    });
});
