"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const getHttpErrorCode_1 = require("../../../jobs/helpers/getHttpErrorCode");
describe('getHttpErrorCode', () => {
    it('returns -1 if not passed an object', () => {
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)(undefined), -1);
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)(null), -1);
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)(404), -1);
    });
    it('returns -1 if passed an object lacking a valid code', () => {
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)({}), -1);
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)({ code: 'garbage' }), -1);
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)({ httpError: { code: 'garbage' } }), -1);
    });
    it('returns the top-level error code if it exists', () => {
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)({ code: 404 }), 404);
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)({ code: '404' }), 404);
    });
    it('returns a nested error code if available', () => {
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)({ httpError: { code: 404 } }), 404);
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)({ httpError: { code: '404' } }), 404);
    });
    it('"prefers" the first valid error code it finds if there is ambiguity', () => {
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)({ code: '404', httpError: { code: 999 } }), 404);
        chai_1.assert.strictEqual((0, getHttpErrorCode_1.getHttpErrorCode)({ code: 'garbage', httpError: { code: 404 } }), 404);
    });
});
