"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const MIME = __importStar(require("../../types/MIME"));
describe('MIME', () => {
    describe('isGif', () => {
        it('returns true for GIFs', () => {
            chai_1.assert.isTrue(MIME.isGif('image/gif'));
        });
        it('returns false for non-GIFs', () => {
            chai_1.assert.isFalse(MIME.isGif('image/jpeg'));
            chai_1.assert.isFalse(MIME.isGif('text/plain'));
        });
    });
    describe('isLongMessage', () => {
        it('returns true for long messages', () => {
            chai_1.assert.isTrue(MIME.isLongMessage('text/x-signal-plain'));
        });
        it('returns true for other content types', () => {
            chai_1.assert.isFalse(MIME.isLongMessage('text/plain'));
            chai_1.assert.isFalse(MIME.isLongMessage('image/gif'));
        });
    });
});
