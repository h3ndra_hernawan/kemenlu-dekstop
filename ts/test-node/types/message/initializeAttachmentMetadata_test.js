"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Message = __importStar(require("../../../types/message/initializeAttachmentMetadata"));
const protobuf_1 = require("../../../protobuf");
const MIME = __importStar(require("../../../types/MIME"));
const Bytes = __importStar(require("../../../Bytes"));
describe('Message', () => {
    describe('initializeAttachmentMetadata', () => {
        it('should classify visual media attachments', async () => {
            const input = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [
                    {
                        contentType: MIME.IMAGE_JPEG,
                        data: Bytes.fromString('foo'),
                        fileName: 'foo.jpg',
                        size: 1111,
                    },
                ],
            };
            const expected = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [
                    {
                        contentType: MIME.IMAGE_JPEG,
                        data: Bytes.fromString('foo'),
                        fileName: 'foo.jpg',
                        size: 1111,
                    },
                ],
                hasAttachments: 1,
                hasVisualMediaAttachments: 1,
                hasFileAttachments: undefined,
            };
            const actual = await Message.initializeAttachmentMetadata(input);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('should classify file attachments', async () => {
            const input = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [
                    {
                        contentType: MIME.APPLICATION_OCTET_STREAM,
                        data: Bytes.fromString('foo'),
                        fileName: 'foo.bin',
                        size: 1111,
                    },
                ],
            };
            const expected = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [
                    {
                        contentType: MIME.APPLICATION_OCTET_STREAM,
                        data: Bytes.fromString('foo'),
                        fileName: 'foo.bin',
                        size: 1111,
                    },
                ],
                hasAttachments: 1,
                hasVisualMediaAttachments: undefined,
                hasFileAttachments: 1,
            };
            const actual = await Message.initializeAttachmentMetadata(input);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('should classify voice message attachments', async () => {
            const input = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [
                    {
                        contentType: MIME.AUDIO_AAC,
                        flags: protobuf_1.SignalService.AttachmentPointer.Flags.VOICE_MESSAGE,
                        data: Bytes.fromString('foo'),
                        fileName: 'Voice Message.aac',
                        size: 1111,
                    },
                ],
            };
            const expected = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [
                    {
                        contentType: MIME.AUDIO_AAC,
                        flags: protobuf_1.SignalService.AttachmentPointer.Flags.VOICE_MESSAGE,
                        data: Bytes.fromString('foo'),
                        fileName: 'Voice Message.aac',
                        size: 1111,
                    },
                ],
                hasAttachments: 1,
                hasVisualMediaAttachments: undefined,
                hasFileAttachments: undefined,
            };
            const actual = await Message.initializeAttachmentMetadata(input);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('does not include long message attachments', async () => {
            const input = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [
                    {
                        contentType: MIME.LONG_MESSAGE,
                        data: Bytes.fromString('foo'),
                        fileName: 'message.txt',
                        size: 1111,
                    },
                ],
            };
            const expected = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [
                    {
                        contentType: MIME.LONG_MESSAGE,
                        data: Bytes.fromString('foo'),
                        fileName: 'message.txt',
                        size: 1111,
                    },
                ],
                hasAttachments: 0,
                hasVisualMediaAttachments: undefined,
                hasFileAttachments: undefined,
            };
            const actual = await Message.initializeAttachmentMetadata(input);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('handles not attachments', async () => {
            const input = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [],
            };
            const expected = {
                type: 'incoming',
                conversationId: 'foo',
                id: '11111111-1111-1111-1111-111111111111',
                timestamp: 1523317140899,
                received_at: 1523317140899,
                sent_at: 1523317140800,
                attachments: [],
                hasAttachments: 0,
                hasVisualMediaAttachments: undefined,
                hasFileAttachments: undefined,
            };
            const actual = await Message.initializeAttachmentMetadata(input);
            chai_1.assert.deepEqual(actual, expected);
        });
    });
});
