"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Attachment = __importStar(require("../../types/Attachment"));
const MIME = __importStar(require("../../types/MIME"));
const protobuf_1 = require("../../protobuf");
const Bytes = __importStar(require("../../Bytes"));
const logger = __importStar(require("../../logging/log"));
const fakeAttachment_1 = require("../../test-both/helpers/fakeAttachment");
describe('Attachment', () => {
    describe('getUploadSizeLimitKb', () => {
        const { getUploadSizeLimitKb } = Attachment;
        it('returns 6000 kilobytes for supported non-GIF images', () => {
            chai_1.assert.strictEqual(getUploadSizeLimitKb(MIME.IMAGE_JPEG), 6000);
            chai_1.assert.strictEqual(getUploadSizeLimitKb(MIME.IMAGE_PNG), 6000);
            chai_1.assert.strictEqual(getUploadSizeLimitKb(MIME.IMAGE_WEBP), 6000);
        });
        it('returns 25000 kilobytes for GIFs', () => {
            chai_1.assert.strictEqual(getUploadSizeLimitKb(MIME.IMAGE_GIF), 25000);
        });
        it('returns 100000 for other file types', () => {
            chai_1.assert.strictEqual(getUploadSizeLimitKb(MIME.APPLICATION_JSON), 100000);
            chai_1.assert.strictEqual(getUploadSizeLimitKb(MIME.AUDIO_AAC), 100000);
            chai_1.assert.strictEqual(getUploadSizeLimitKb(MIME.AUDIO_MP3), 100000);
            chai_1.assert.strictEqual(getUploadSizeLimitKb(MIME.VIDEO_MP4), 100000);
            chai_1.assert.strictEqual(getUploadSizeLimitKb('image/vnd.adobe.photoshop'), 100000);
        });
    });
    describe('getFileExtension', () => {
        it('should return file extension from content type', () => {
            const input = (0, fakeAttachment_1.fakeAttachment)({
                data: Bytes.fromString('foo'),
                contentType: MIME.IMAGE_GIF,
            });
            chai_1.assert.strictEqual(Attachment.getFileExtension(input), 'gif');
        });
        it('should return file extension for QuickTime videos', () => {
            const input = (0, fakeAttachment_1.fakeAttachment)({
                data: Bytes.fromString('foo'),
                contentType: MIME.VIDEO_QUICKTIME,
            });
            chai_1.assert.strictEqual(Attachment.getFileExtension(input), 'mov');
        });
    });
    describe('getSuggestedFilename', () => {
        context('for attachment with filename', () => {
            it('should return existing filename if present', () => {
                const attachment = (0, fakeAttachment_1.fakeAttachment)({
                    fileName: 'funny-cat.mov',
                    data: Bytes.fromString('foo'),
                    contentType: MIME.VIDEO_QUICKTIME,
                });
                const actual = Attachment.getSuggestedFilename({ attachment });
                const expected = 'funny-cat.mov';
                chai_1.assert.strictEqual(actual, expected);
            });
        });
        context('for attachment without filename', () => {
            it('should generate a filename based on timestamp', () => {
                const attachment = (0, fakeAttachment_1.fakeAttachment)({
                    data: Bytes.fromString('foo'),
                    contentType: MIME.VIDEO_QUICKTIME,
                });
                const timestamp = new Date(new Date(0).getTimezoneOffset() * 60 * 1000);
                const actual = Attachment.getSuggestedFilename({
                    attachment,
                    timestamp,
                });
                const expected = 'signal-1970-01-01-000000.mov';
                chai_1.assert.strictEqual(actual, expected);
            });
        });
        context('for attachment with index', () => {
            it('should generate a filename based on timestamp', () => {
                const attachment = (0, fakeAttachment_1.fakeAttachment)({
                    data: Bytes.fromString('foo'),
                    contentType: MIME.VIDEO_QUICKTIME,
                });
                const timestamp = new Date(new Date(0).getTimezoneOffset() * 60 * 1000);
                const actual = Attachment.getSuggestedFilename({
                    attachment,
                    timestamp,
                    index: 3,
                });
                const expected = 'signal-1970-01-01-000000_003.mov';
                chai_1.assert.strictEqual(actual, expected);
            });
        });
    });
    describe('isVisualMedia', () => {
        it('should return true for images', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'meme.gif',
                data: Bytes.fromString('gif'),
                contentType: MIME.IMAGE_GIF,
            });
            chai_1.assert.isTrue(Attachment.isVisualMedia(attachment));
        });
        it('should return true for videos', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'meme.mp4',
                data: Bytes.fromString('mp4'),
                contentType: MIME.VIDEO_MP4,
            });
            chai_1.assert.isTrue(Attachment.isVisualMedia(attachment));
        });
        it('should return false for voice message attachment', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'Voice Message.aac',
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.VOICE_MESSAGE,
                data: Bytes.fromString('voice message'),
                contentType: MIME.AUDIO_AAC,
            });
            chai_1.assert.isFalse(Attachment.isVisualMedia(attachment));
        });
        it('should return false for other attachments', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'foo.json',
                data: Bytes.fromString('{"foo": "bar"}'),
                contentType: MIME.APPLICATION_JSON,
            });
            chai_1.assert.isFalse(Attachment.isVisualMedia(attachment));
        });
    });
    describe('isFile', () => {
        it('should return true for JSON', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'foo.json',
                data: Bytes.fromString('{"foo": "bar"}'),
                contentType: MIME.APPLICATION_JSON,
            });
            chai_1.assert.isTrue(Attachment.isFile(attachment));
        });
        it('should return false for images', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'meme.gif',
                data: Bytes.fromString('gif'),
                contentType: MIME.IMAGE_GIF,
            });
            chai_1.assert.isFalse(Attachment.isFile(attachment));
        });
        it('should return false for videos', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'meme.mp4',
                data: Bytes.fromString('mp4'),
                contentType: MIME.VIDEO_MP4,
            });
            chai_1.assert.isFalse(Attachment.isFile(attachment));
        });
        it('should return false for voice message attachment', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'Voice Message.aac',
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.VOICE_MESSAGE,
                data: Bytes.fromString('voice message'),
                contentType: MIME.AUDIO_AAC,
            });
            chai_1.assert.isFalse(Attachment.isFile(attachment));
        });
    });
    describe('isVoiceMessage', () => {
        it('should return true for voice message attachment', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'Voice Message.aac',
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.VOICE_MESSAGE,
                data: Bytes.fromString('voice message'),
                contentType: MIME.AUDIO_AAC,
            });
            chai_1.assert.isTrue(Attachment.isVoiceMessage(attachment));
        });
        it('should return true for legacy Android voice message attachment', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                data: Bytes.fromString('voice message'),
                contentType: MIME.AUDIO_MP3,
            });
            chai_1.assert.isTrue(Attachment.isVoiceMessage(attachment));
        });
        it('should return false for other attachments', () => {
            const attachment = (0, fakeAttachment_1.fakeAttachment)({
                fileName: 'foo.gif',
                data: Bytes.fromString('foo'),
                contentType: MIME.IMAGE_GIF,
            });
            chai_1.assert.isFalse(Attachment.isVoiceMessage(attachment));
        });
    });
    describe('replaceUnicodeOrderOverrides', () => {
        it('should sanitize left-to-right order override character', async () => {
            const input = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'test\u202Dfig.exe',
                size: 1111,
            };
            const expected = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'test\uFFFDfig.exe',
                size: 1111,
            };
            const actual = await Attachment.replaceUnicodeOrderOverrides(input);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('should sanitize right-to-left order override character', async () => {
            const input = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'test\u202Efig.exe',
                size: 1111,
            };
            const expected = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'test\uFFFDfig.exe',
                size: 1111,
            };
            const actual = await Attachment.replaceUnicodeOrderOverrides(input);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('should sanitize multiple override characters', async () => {
            const input = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'test\u202e\u202dlol\u202efig.exe',
                size: 1111,
            };
            const expected = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'test\uFFFD\uFFFDlol\uFFFDfig.exe',
                size: 1111,
            };
            const actual = await Attachment.replaceUnicodeOrderOverrides(input);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('should ignore non-order-override characters', () => {
            const input = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'abc',
                size: 1111,
            };
            const actual = Attachment._replaceUnicodeOrderOverridesSync(input);
            chai_1.assert.deepEqual(actual, input);
        });
        it('should replace order-override characters', () => {
            const input = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'abc\u202D\u202E',
                size: 1111,
            };
            const actual = Attachment._replaceUnicodeOrderOverridesSync(input);
            chai_1.assert.deepEqual(actual, {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'abc\uFFFD\uFFFD',
                size: 1111,
            });
        });
    });
    describe('replaceUnicodeV2', () => {
        it('should remove all bad characters', async () => {
            const input = {
                size: 1111,
                contentType: MIME.IMAGE_JPEG,
                fileName: 'file\u202A\u202B\u202C\u202D\u202E\u2066\u2067\u2068\u2069\u200E\u200F\u061C.jpeg',
            };
            const expected = {
                fileName: 'file\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD.jpeg',
                contentType: MIME.IMAGE_JPEG,
                size: 1111,
            };
            const actual = await Attachment.replaceUnicodeV2(input);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('should should leave normal filename alone', async () => {
            const input = {
                fileName: 'normal.jpeg',
                contentType: MIME.IMAGE_JPEG,
                size: 1111,
            };
            const expected = {
                fileName: 'normal.jpeg',
                contentType: MIME.IMAGE_JPEG,
                size: 1111,
            };
            const actual = await Attachment.replaceUnicodeV2(input);
            chai_1.assert.deepEqual(actual, expected);
        });
        it('should handle missing fileName', async () => {
            const input = {
                size: 1111,
                contentType: MIME.IMAGE_JPEG,
            };
            const expected = {
                size: 1111,
                contentType: MIME.IMAGE_JPEG,
            };
            const actual = await Attachment.replaceUnicodeV2(input);
            chai_1.assert.deepEqual(actual, expected);
        });
    });
    describe('removeSchemaVersion', () => {
        it('should remove existing schema version', () => {
            const input = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'foo.jpg',
                size: 1111,
                schemaVersion: 1,
            };
            const expected = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'foo.jpg',
                size: 1111,
            };
            const actual = Attachment.removeSchemaVersion({
                attachment: input,
                logger,
            });
            chai_1.assert.deepEqual(actual, expected);
        });
    });
    describe('migrateDataToFileSystem', () => {
        it('should write data to disk and store relative path to it', async () => {
            const input = {
                contentType: MIME.IMAGE_JPEG,
                data: Bytes.fromString('Above us only sky'),
                fileName: 'foo.jpg',
                size: 1111,
            };
            const expected = {
                contentType: MIME.IMAGE_JPEG,
                path: 'abc/abcdefgh123456789',
                fileName: 'foo.jpg',
                size: 1111,
            };
            const expectedAttachmentData = Bytes.fromString('Above us only sky');
            const writeNewAttachmentData = async (attachmentData) => {
                chai_1.assert.deepEqual(attachmentData, expectedAttachmentData);
                return 'abc/abcdefgh123456789';
            };
            const actual = await Attachment.migrateDataToFileSystem(input, {
                writeNewAttachmentData,
            });
            chai_1.assert.deepEqual(actual, expected);
        });
        it('should skip over (invalid) attachments without data', async () => {
            const input = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'foo.jpg',
                size: 1111,
            };
            const expected = {
                contentType: MIME.IMAGE_JPEG,
                fileName: 'foo.jpg',
                size: 1111,
            };
            const writeNewAttachmentData = async () => 'abc/abcdefgh123456789';
            const actual = await Attachment.migrateDataToFileSystem(input, {
                writeNewAttachmentData,
            });
            chai_1.assert.deepEqual(actual, expected);
        });
        it('should throw error if data is not valid', async () => {
            const input = {
                contentType: MIME.IMAGE_JPEG,
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                data: 123,
                fileName: 'foo.jpg',
                size: 1111,
            };
            const writeNewAttachmentData = async () => 'abc/abcdefgh123456789';
            await chai_1.assert.isRejected(Attachment.migrateDataToFileSystem(input, {
                writeNewAttachmentData,
            }), 'Expected `attachment.data` to be a typed array; got: number');
        });
    });
});
