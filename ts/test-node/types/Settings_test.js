"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const os_1 = __importDefault(require("os"));
const sinon_1 = __importDefault(require("sinon"));
const chai_1 = require("chai");
const Settings = __importStar(require("../../types/Settings"));
describe('Settings', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon_1.default.createSandbox();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('getAudioNotificationSupport', () => {
        it('returns native support on macOS', () => {
            sandbox.stub(process, 'platform').value('darwin');
            chai_1.assert.strictEqual(Settings.getAudioNotificationSupport(), Settings.AudioNotificationSupport.Native);
        });
        it('returns no support on Windows 7', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('7.0.0');
            chai_1.assert.strictEqual(Settings.getAudioNotificationSupport(), Settings.AudioNotificationSupport.None);
        });
        it('returns native support on Windows 8', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('8.0.0');
            chai_1.assert.strictEqual(Settings.getAudioNotificationSupport(), Settings.AudioNotificationSupport.Native);
        });
        it('returns custom support on Linux', () => {
            sandbox.stub(process, 'platform').value('linux');
            chai_1.assert.strictEqual(Settings.getAudioNotificationSupport(), Settings.AudioNotificationSupport.Custom);
        });
    });
    describe('isAudioNotificationSupported', () => {
        it('returns true on macOS', () => {
            sandbox.stub(process, 'platform').value('darwin');
            chai_1.assert.isTrue(Settings.isAudioNotificationSupported());
        });
        it('returns false on Windows 7', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('7.0.0');
            chai_1.assert.isFalse(Settings.isAudioNotificationSupported());
        });
        it('returns true on Windows 8', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('8.0.0');
            chai_1.assert.isTrue(Settings.isAudioNotificationSupported());
        });
        it('returns true on Linux', () => {
            sandbox.stub(process, 'platform').value('linux');
            chai_1.assert.isTrue(Settings.isAudioNotificationSupported());
        });
    });
    describe('isNotificationGroupingSupported', () => {
        it('returns true on macOS', () => {
            sandbox.stub(process, 'platform').value('darwin');
            chai_1.assert.isTrue(Settings.isNotificationGroupingSupported());
        });
        it('returns true on Windows 7', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('7.0.0');
            chai_1.assert.isFalse(Settings.isNotificationGroupingSupported());
        });
        it('returns true on Windows 8', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('8.0.0');
            chai_1.assert.isTrue(Settings.isNotificationGroupingSupported());
        });
        it('returns true on Linux', () => {
            sandbox.stub(process, 'platform').value('linux');
            chai_1.assert.isTrue(Settings.isNotificationGroupingSupported());
        });
    });
    describe('isAutoLaunchSupported', () => {
        it('returns true on Windows', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('8.0.0');
            chai_1.assert.isTrue(Settings.isAutoLaunchSupported());
        });
        it('returns true on macOS', () => {
            sandbox.stub(process, 'platform').value('darwin');
            chai_1.assert.isTrue(Settings.isAutoLaunchSupported());
        });
        it('returns false on Linux', () => {
            sandbox.stub(process, 'platform').value('linux');
            chai_1.assert.isFalse(Settings.isAutoLaunchSupported());
        });
    });
    describe('isHideMenuBarSupported', () => {
        it('returns false on macOS', () => {
            sandbox.stub(process, 'platform').value('darwin');
            chai_1.assert.isFalse(Settings.isHideMenuBarSupported());
        });
        it('returns true on Windows 7', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('7.0.0');
            chai_1.assert.isTrue(Settings.isHideMenuBarSupported());
        });
        it('returns true on Windows 8', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('8.0.0');
            chai_1.assert.isTrue(Settings.isHideMenuBarSupported());
        });
        it('returns true on Linux', () => {
            sandbox.stub(process, 'platform').value('linux');
            chai_1.assert.isTrue(Settings.isHideMenuBarSupported());
        });
    });
    describe('isDrawAttentionSupported', () => {
        it('returns false on macOS', () => {
            sandbox.stub(process, 'platform').value('darwin');
            chai_1.assert.isFalse(Settings.isDrawAttentionSupported());
        });
        it('returns true on Windows 7', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('7.0.0');
            chai_1.assert.isTrue(Settings.isDrawAttentionSupported());
        });
        it('returns true on Windows 8', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('8.0.0');
            chai_1.assert.isTrue(Settings.isDrawAttentionSupported());
        });
        it('returns true on Linux', () => {
            sandbox.stub(process, 'platform').value('linux');
            chai_1.assert.isTrue(Settings.isDrawAttentionSupported());
        });
    });
    describe('isSystemTraySupported', () => {
        it('returns false on macOS', () => {
            sandbox.stub(process, 'platform').value('darwin');
            chai_1.assert.isFalse(Settings.isSystemTraySupported('1.2.3'));
        });
        it('returns true on Windows 8', () => {
            sandbox.stub(process, 'platform').value('win32');
            sandbox.stub(os_1.default, 'release').returns('8.0.0');
            chai_1.assert.isTrue(Settings.isSystemTraySupported('1.2.3'));
        });
        it('returns false on Linux production', () => {
            sandbox.stub(process, 'platform').value('linux');
            chai_1.assert.isFalse(Settings.isSystemTraySupported('1.2.3'));
        });
        it('returns true on Linux beta', () => {
            sandbox.stub(process, 'platform').value('linux');
            chai_1.assert.isTrue(Settings.isSystemTraySupported('1.2.3-beta.4'));
        });
    });
});
