"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Username = __importStar(require("../../types/Username"));
describe('Username', () => {
    describe('getUsernameFromSearch', () => {
        const { getUsernameFromSearch } = Username;
        it('matches invalid username searches', () => {
            chai_1.assert.strictEqual(getUsernameFromSearch('username!'), 'username!');
            chai_1.assert.strictEqual(getUsernameFromSearch('1username'), '1username');
            chai_1.assert.strictEqual(getUsernameFromSearch('use'), 'use');
            chai_1.assert.strictEqual(getUsernameFromSearch('username9012345678901234567'), 'username9012345678901234567');
        });
        it('matches valid username searches', () => {
            chai_1.assert.strictEqual(getUsernameFromSearch('username_34'), 'username_34');
            chai_1.assert.strictEqual(getUsernameFromSearch('u5ername'), 'u5ername');
            chai_1.assert.strictEqual(getUsernameFromSearch('user'), 'user');
            chai_1.assert.strictEqual(getUsernameFromSearch('username901234567890123456'), 'username901234567890123456');
        });
        it('matches valid and invalid usernames with @ prefix', () => {
            chai_1.assert.strictEqual(getUsernameFromSearch('@username!'), 'username!');
            chai_1.assert.strictEqual(getUsernameFromSearch('@1username'), '1username');
            chai_1.assert.strictEqual(getUsernameFromSearch('@username_34'), 'username_34');
            chai_1.assert.strictEqual(getUsernameFromSearch('@u5ername'), 'u5ername');
        });
        it('matches valid and invalid usernames with @ suffix', () => {
            chai_1.assert.strictEqual(getUsernameFromSearch('username!@'), 'username!');
            chai_1.assert.strictEqual(getUsernameFromSearch('1username@'), '1username');
            chai_1.assert.strictEqual(getUsernameFromSearch('username_34@'), 'username_34');
            chai_1.assert.strictEqual(getUsernameFromSearch('u5ername@'), 'u5ername');
        });
        it('does not match something that looks like a phone number', () => {
            chai_1.assert.isUndefined(getUsernameFromSearch('+'));
            chai_1.assert.isUndefined(getUsernameFromSearch('2223'));
            chai_1.assert.isUndefined(getUsernameFromSearch('+3'));
            chai_1.assert.isUndefined(getUsernameFromSearch('+234234234233'));
        });
    });
    describe('isValidUsername', () => {
        const { isValidUsername } = Username;
        it('does not match invalid username searches', () => {
            chai_1.assert.isFalse(isValidUsername('username!'));
            chai_1.assert.isFalse(isValidUsername('1username'));
            chai_1.assert.isFalse(isValidUsername('use'));
            chai_1.assert.isFalse(isValidUsername('username9012345678901234567'));
        });
        it('matches valid usernames', () => {
            chai_1.assert.isTrue(isValidUsername('username_34'));
            chai_1.assert.isTrue(isValidUsername('u5ername'));
            chai_1.assert.isTrue(isValidUsername('_username'));
            chai_1.assert.isTrue(isValidUsername('user'));
            chai_1.assert.isTrue(isValidUsername('username901234567890123456'));
        });
        it('does not match valid and invalid usernames with @ prefix or suffix', () => {
            chai_1.assert.isFalse(isValidUsername('@username_34'));
            chai_1.assert.isFalse(isValidUsername('@1username'));
            chai_1.assert.isFalse(isValidUsername('username_34@'));
            chai_1.assert.isFalse(isValidUsername('1username@'));
        });
    });
});
