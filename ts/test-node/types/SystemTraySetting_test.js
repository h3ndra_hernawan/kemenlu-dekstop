"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const SystemTraySetting_1 = require("../../types/SystemTraySetting");
describe('system tray setting utilities', () => {
    describe('shouldMinimizeToSystemTray', () => {
        it('returns false if the system tray is disabled', () => {
            chai_1.assert.isFalse((0, SystemTraySetting_1.shouldMinimizeToSystemTray)(SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray));
        });
        it('returns true if the system tray is enabled', () => {
            chai_1.assert.isTrue((0, SystemTraySetting_1.shouldMinimizeToSystemTray)(SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray));
            chai_1.assert.isTrue((0, SystemTraySetting_1.shouldMinimizeToSystemTray)(SystemTraySetting_1.SystemTraySetting.MinimizeToAndStartInSystemTray));
        });
    });
    describe('parseSystemTraySetting', () => {
        it('parses valid strings into their enum values', () => {
            chai_1.assert.strictEqual((0, SystemTraySetting_1.parseSystemTraySetting)('DoNotUseSystemTray'), SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray);
            chai_1.assert.strictEqual((0, SystemTraySetting_1.parseSystemTraySetting)('MinimizeToSystemTray'), SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray);
            chai_1.assert.strictEqual((0, SystemTraySetting_1.parseSystemTraySetting)('MinimizeToAndStartInSystemTray'), SystemTraySetting_1.SystemTraySetting.MinimizeToAndStartInSystemTray);
        });
        it('parses invalid strings to DoNotUseSystemTray', () => {
            chai_1.assert.strictEqual((0, SystemTraySetting_1.parseSystemTraySetting)('garbage'), SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray);
        });
    });
});
