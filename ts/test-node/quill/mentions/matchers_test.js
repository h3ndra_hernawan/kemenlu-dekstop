"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const quill_delta_1 = __importDefault(require("quill-delta"));
const matchers_1 = require("../../../quill/mentions/matchers");
const memberRepository_1 = require("../../../quill/memberRepository");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
class FakeTokenList extends Array {
    constructor(elements) {
        super();
        elements.forEach(element => this.push(element));
    }
    contains(searchElement) {
        return this.includes(searchElement);
    }
}
const createMockElement = (className, dataset) => ({
    classList: new FakeTokenList([className]),
    dataset,
});
const createMockAtMentionElement = (dataset) => createMockElement('MessageBody__at-mention', dataset);
const createMockMentionBlotElement = (dataset) => createMockElement('mention-blot', dataset);
const memberMahershala = (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
    id: '555444',
    title: 'Mahershala Ali',
    firstName: 'Mahershala',
    profileName: 'Mahershala A.',
    type: 'direct',
    lastUpdated: Date.now(),
    markedUnread: false,
    areWeAdmin: false,
});
const memberShia = (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
    id: '333222',
    title: 'Shia LaBeouf',
    firstName: 'Shia',
    profileName: 'Shia L.',
    type: 'direct',
    lastUpdated: Date.now(),
    markedUnread: false,
    areWeAdmin: false,
});
const members = [memberMahershala, memberShia];
const memberRepositoryRef = {
    current: new memberRepository_1.MemberRepository(members),
};
const matcher = (0, matchers_1.matchMention)(memberRepositoryRef);
const isMention = (insert) => {
    if (insert) {
        if (Object.getOwnPropertyNames(insert).includes('mention'))
            return true;
    }
    return false;
};
const EMPTY_DELTA = new quill_delta_1.default();
describe('matchMention', () => {
    it('handles an AtMentionify from clipboard', () => {
        const result = matcher(createMockAtMentionElement({
            id: memberMahershala.id,
            title: memberMahershala.title,
        }), EMPTY_DELTA);
        const { ops } = result;
        chai_1.assert.isNotEmpty(ops);
        const [op] = ops;
        const { insert } = op;
        if (isMention(insert)) {
            const { title, uuid } = insert.mention;
            chai_1.assert.equal(title, memberMahershala.title);
            chai_1.assert.equal(uuid, memberMahershala.uuid);
        }
        else {
            chai_1.assert.fail('insert is invalid');
        }
    });
    it('handles an MentionBlot from clipboard', () => {
        const result = matcher(createMockMentionBlotElement({
            uuid: memberMahershala.uuid || '',
            title: memberMahershala.title,
        }), EMPTY_DELTA);
        const { ops } = result;
        chai_1.assert.isNotEmpty(ops);
        const [op] = ops;
        const { insert } = op;
        if (isMention(insert)) {
            const { title, uuid } = insert.mention;
            chai_1.assert.equal(title, memberMahershala.title);
            chai_1.assert.equal(uuid, memberMahershala.uuid);
        }
        else {
            chai_1.assert.fail('insert is invalid');
        }
    });
    it('converts a missing AtMentionify to string', () => {
        const result = matcher(createMockAtMentionElement({
            id: 'florp',
            title: 'Nonexistent',
        }), EMPTY_DELTA);
        const { ops } = result;
        chai_1.assert.isNotEmpty(ops);
        const [op] = ops;
        const { insert } = op;
        if (isMention(insert)) {
            chai_1.assert.fail('insert is invalid');
        }
        else {
            chai_1.assert.equal(insert, '@Nonexistent');
        }
    });
    it('converts a missing MentionBlot to string', () => {
        const result = matcher(createMockMentionBlotElement({
            uuid: 'florp',
            title: 'Nonexistent',
        }), EMPTY_DELTA);
        const { ops } = result;
        chai_1.assert.isNotEmpty(ops);
        const [op] = ops;
        const { insert } = op;
        if (isMention(insert)) {
            chai_1.assert.fail('insert is invalid');
        }
        else {
            chai_1.assert.equal(insert, '@Nonexistent');
        }
    });
    it('passes other clipboard elements through', () => {
        const result = matcher(createMockElement('ignore', {}), EMPTY_DELTA);
        chai_1.assert.equal(result, EMPTY_DELTA);
    });
});
