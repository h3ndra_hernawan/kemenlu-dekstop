"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const os_1 = require("os");
const fs_1 = require("fs");
const uuid_1 = require("uuid");
const chai_1 = require("chai");
const base_config_1 = require("../../../app/base_config");
describe('base_config', () => {
    let targetFile;
    function getNewPath() {
        return `${(0, os_1.tmpdir)()}/${(0, uuid_1.v4)()}.txt`;
    }
    afterEach(() => {
        if (targetFile) {
            (0, fs_1.unlinkSync)(targetFile);
        }
    });
    it('does not throw if file is missing', () => {
        const missingFile = getNewPath();
        const { _getCachedValue } = (0, base_config_1.start)('test', missingFile);
        chai_1.assert.deepEqual(_getCachedValue(), Object.create(null));
    });
    it('successfully loads config file', () => {
        targetFile = getNewPath();
        const config = { a: 1, b: 2 };
        (0, fs_1.writeFileSync)(targetFile, JSON.stringify(config));
        const { _getCachedValue } = (0, base_config_1.start)('test', targetFile);
        chai_1.assert.deepEqual(_getCachedValue(), config);
    });
    it('throws if file is malformed', () => {
        targetFile = getNewPath();
        (0, fs_1.writeFileSync)(targetFile, '{{ malformed JSON');
        const fileForClosure = targetFile;
        chai_1.assert.throws(() => (0, base_config_1.start)('test', fileForClosure));
    });
    it('does not throw if file is empty', () => {
        targetFile = getNewPath();
        (0, fs_1.writeFileSync)(targetFile, '');
        const { _getCachedValue } = (0, base_config_1.start)('test', targetFile);
        chai_1.assert.deepEqual(_getCachedValue(), Object.create(null));
    });
    it('does not throw if file is malformed, with allowMalformedOnStartup', () => {
        targetFile = getNewPath();
        (0, fs_1.writeFileSync)(targetFile, '{{ malformed JSON');
        const { _getCachedValue } = (0, base_config_1.start)('test', targetFile, {
            allowMalformedOnStartup: true,
        });
        chai_1.assert.deepEqual(_getCachedValue(), Object.create(null));
    });
});
