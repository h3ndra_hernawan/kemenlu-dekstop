"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const electron_1 = require("electron");
const path = __importStar(require("path"));
const SystemTrayService_1 = require("../../../app/SystemTrayService");
describe('SystemTrayService', () => {
    let sandbox;
    /**
     * Instantiating an Electron `Tray` has side-effects that we need to clean up. Make sure
     * to use `newService` instead of `new SystemTrayService` in these tests to ensure that
     * the tray is cleaned up.
     *
     * This only affects these tests, not the "real" code.
     */
    function newService() {
        const result = new SystemTrayService_1.SystemTrayService({
            messages: {
                hide: { message: 'Hide' },
                quit: { message: 'Quit' },
                show: { message: 'Show' },
                signalDesktop: { message: 'Signal' },
            },
        });
        servicesCreated.add(result);
        return result;
    }
    const servicesCreated = new Set();
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });
    afterEach(() => {
        sandbox.restore();
        servicesCreated.forEach(service => {
            var _a;
            (_a = service._getTray()) === null || _a === void 0 ? void 0 : _a.destroy();
        });
        servicesCreated.clear();
    });
    it("doesn't render a tray icon unless (1) we're enabled (2) there's a browser window", () => {
        const service = newService();
        chai_1.assert.isUndefined(service._getTray());
        service.setEnabled(true);
        chai_1.assert.isUndefined(service._getTray());
        service.setMainWindow(new electron_1.BrowserWindow({ show: false }));
        chai_1.assert.instanceOf(service._getTray(), electron_1.Tray);
        service.setEnabled(false);
        chai_1.assert.isUndefined(service._getTray());
    });
    it('renders a "Hide" button when the window is shown and a "Show" button when the window is hidden', () => {
        var _a, _b, _c, _d, _e;
        // We don't actually want to show a browser window. It's disruptive when you're
        //   running tests and can introduce test-only flakiness. We jump through some hoops
        //   to fake the behavior.
        let fakeIsVisible = false;
        const browserWindow = new electron_1.BrowserWindow({ show: fakeIsVisible });
        sinon.stub(browserWindow, 'isVisible').callsFake(() => fakeIsVisible);
        sinon.stub(browserWindow, 'show').callsFake(() => {
            fakeIsVisible = true;
            browserWindow.emit('show');
        });
        sinon.stub(browserWindow, 'hide').callsFake(() => {
            fakeIsVisible = false;
            browserWindow.emit('hide');
        });
        const service = newService();
        service.setEnabled(true);
        service.setMainWindow(browserWindow);
        const tray = service._getTray();
        if (!tray) {
            throw new Error('Test setup failed: expected a tray');
        }
        // Ideally, there'd be something like `tray.getContextMenu`, but that doesn't exist.
        //   We also can't spy on `Tray.prototype.setContextMenu` because it's not defined
        //   that way. So we spy on the specific instance, just to get the context menu.
        const setContextMenuSpy = sandbox.spy(tray, 'setContextMenu');
        const getToggleMenuItem = () => {
            var _a, _b;
            return (_b = (_a = setContextMenuSpy.lastCall) === null || _a === void 0 ? void 0 : _a.firstArg) === null || _b === void 0 ? void 0 : _b.getMenuItemById('toggleWindowVisibility');
        };
        browserWindow.show();
        chai_1.assert.strictEqual((_a = getToggleMenuItem()) === null || _a === void 0 ? void 0 : _a.label, 'Hide');
        (_b = getToggleMenuItem()) === null || _b === void 0 ? void 0 : _b.click();
        chai_1.assert.strictEqual((_c = getToggleMenuItem()) === null || _c === void 0 ? void 0 : _c.label, 'Show');
        (_d = getToggleMenuItem()) === null || _d === void 0 ? void 0 : _d.click();
        chai_1.assert.strictEqual((_e = getToggleMenuItem()) === null || _e === void 0 ? void 0 : _e.label, 'Hide');
    });
    it('destroys the tray when disabling', () => {
        const service = newService();
        service.setEnabled(true);
        service.setMainWindow(new electron_1.BrowserWindow({ show: false }));
        const tray = service._getTray();
        if (!tray) {
            throw new Error('Test setup failed: expected a tray');
        }
        chai_1.assert.isFalse(tray.isDestroyed());
        service.setEnabled(false);
        chai_1.assert.isTrue(tray.isDestroyed());
    });
    it('maintains the same Tray instance when switching browser window instances', () => {
        const service = newService();
        service.setEnabled(true);
        service.setMainWindow(new electron_1.BrowserWindow({ show: false }));
        const originalTray = service._getTray();
        service.setMainWindow(new electron_1.BrowserWindow({ show: false }));
        chai_1.assert.strictEqual(service._getTray(), originalTray);
    });
    it('removes browser window event listeners when changing browser window instances', () => {
        const firstBrowserWindow = new electron_1.BrowserWindow({ show: false });
        const showListenersAtStart = firstBrowserWindow.listenerCount('show');
        const hideListenersAtStart = firstBrowserWindow.listenerCount('hide');
        const service = newService();
        service.setEnabled(true);
        service.setMainWindow(firstBrowserWindow);
        chai_1.assert.strictEqual(firstBrowserWindow.listenerCount('show'), showListenersAtStart + 1);
        chai_1.assert.strictEqual(firstBrowserWindow.listenerCount('hide'), hideListenersAtStart + 1);
        service.setMainWindow(new electron_1.BrowserWindow({ show: false }));
        chai_1.assert.strictEqual(firstBrowserWindow.listenerCount('show'), showListenersAtStart);
        chai_1.assert.strictEqual(firstBrowserWindow.listenerCount('hide'), hideListenersAtStart);
    });
    it('removes browser window event listeners when removing browser window instances', () => {
        const browserWindow = new electron_1.BrowserWindow({ show: false });
        const showListenersAtStart = browserWindow.listenerCount('show');
        const hideListenersAtStart = browserWindow.listenerCount('hide');
        const service = newService();
        service.setEnabled(true);
        service.setMainWindow(browserWindow);
        chai_1.assert.strictEqual(browserWindow.listenerCount('show'), showListenersAtStart + 1);
        chai_1.assert.strictEqual(browserWindow.listenerCount('hide'), hideListenersAtStart + 1);
        service.setMainWindow(undefined);
        chai_1.assert.strictEqual(browserWindow.listenerCount('show'), showListenersAtStart);
        chai_1.assert.strictEqual(browserWindow.listenerCount('hide'), hideListenersAtStart);
    });
    it('updates the icon when the unread count changes', () => {
        const service = newService();
        service.setEnabled(true);
        service.setMainWindow(new electron_1.BrowserWindow({ show: false }));
        const tray = service._getTray();
        if (!tray) {
            throw new Error('Test setup failed: expected a tray');
        }
        // Ideally, there'd be something like `tray.getImage`, but that doesn't exist. We also
        //   can't spy on `Tray.prototype.setImage` because it's not defined that way. So we
        //   spy on the specific instance, just to get the image.
        const setImageSpy = sandbox.spy(tray, 'setImage');
        const getImagePath = () => {
            var _a;
            const result = (_a = setImageSpy.lastCall) === null || _a === void 0 ? void 0 : _a.firstArg;
            if (!result) {
                throw new Error('Expected tray.setImage to be called at least once');
            }
            return result;
        };
        for (let i = 9; i >= 1; i -= 1) {
            service.setUnreadCount(i);
            chai_1.assert.strictEqual(path.parse(getImagePath()).base, `${i}.png`);
        }
        for (let i = 10; i < 13; i += 1) {
            service.setUnreadCount(i);
            chai_1.assert.strictEqual(path.parse(getImagePath()).base, '10.png');
        }
        service.setUnreadCount(0);
        chai_1.assert.match(path.parse(getImagePath()).base, /^icon_\d+\.png$/);
    });
    it('uses a fallback image if the icon file cannot be found', () => {
        const service = newService();
        service.setEnabled(true);
        service.setMainWindow(new electron_1.BrowserWindow({ show: false }));
        const tray = service._getTray();
        if (!tray) {
            throw new Error('Test setup failed: expected a tray');
        }
        const setImageStub = sandbox.stub(tray, 'setImage');
        setImageStub.withArgs(sinon.match.string).throws('Failed to load');
        service.setUnreadCount(4);
        // Electron doesn't export this class, so we have to wrestle it out.
        const NativeImage = electron_1.nativeImage.createEmpty().constructor;
        sinon.assert.calledTwice(setImageStub);
        sinon.assert.calledWith(setImageStub, sinon.match.string);
        sinon.assert.calledWith(setImageStub, sinon.match.instanceOf(NativeImage));
    });
});
