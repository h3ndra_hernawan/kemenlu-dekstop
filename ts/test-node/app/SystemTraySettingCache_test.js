"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const SystemTraySetting_1 = require("../../types/SystemTraySetting");
const SystemTraySettingCache_1 = require("../../../app/SystemTraySettingCache");
describe('SystemTraySettingCache', () => {
    let sandbox;
    let sqlCallStub;
    let configGetStub;
    let configSetStub;
    let sql;
    let config;
    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sqlCallStub = sandbox.stub().resolves();
        sql = { sqlCall: sqlCallStub };
        configGetStub = sandbox.stub().returns(undefined);
        configSetStub = sandbox.stub().returns(undefined);
        config = { get: configGetStub, set: configSetStub };
    });
    afterEach(() => {
        sandbox.restore();
    });
    it('returns MinimizeToAndStartInSystemTray if passed the --start-in-tray argument', async () => {
        const justOneArg = new SystemTraySettingCache_1.SystemTraySettingCache(sql, config, ['--start-in-tray'], '1.2.3');
        chai_1.assert.strictEqual(await justOneArg.get(), SystemTraySetting_1.SystemTraySetting.MinimizeToAndStartInSystemTray);
        const bothArgs = new SystemTraySettingCache_1.SystemTraySettingCache(sql, config, ['--start-in-tray', '--use-tray-icon'], '1.2.3');
        chai_1.assert.strictEqual(await bothArgs.get(), SystemTraySetting_1.SystemTraySetting.MinimizeToAndStartInSystemTray);
        sinon.assert.notCalled(sqlCallStub);
        sinon.assert.notCalled(configGetStub);
        sinon.assert.notCalled(configSetStub);
    });
    it('returns MinimizeToSystemTray if passed the --use-tray-icon argument', async () => {
        const cache = new SystemTraySettingCache_1.SystemTraySettingCache(sql, config, ['--use-tray-icon'], '1.2.3');
        chai_1.assert.strictEqual(await cache.get(), SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray);
        sinon.assert.notCalled(sqlCallStub);
        sinon.assert.notCalled(configGetStub);
        sinon.assert.notCalled(configSetStub);
    });
    it('returns DoNotUseSystemTray if system tray is supported but no preference is stored', async () => {
        sandbox.stub(process, 'platform').value('win32');
        const cache = new SystemTraySettingCache_1.SystemTraySettingCache(sql, config, [], '1.2.3');
        chai_1.assert.strictEqual(await cache.get(), SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray);
        (0, chai_1.assert)(configGetStub.calledOnceWith('system-tray-setting'));
        (0, chai_1.assert)(configSetStub.calledOnceWith('system-tray-setting', SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray));
    });
    it('returns DoNotUseSystemTray if system tray is supported but the stored preference is invalid', async () => {
        sandbox.stub(process, 'platform').value('win32');
        sqlCallStub.resolves({ value: 'garbage' });
        const cache = new SystemTraySettingCache_1.SystemTraySettingCache(sql, config, [], '1.2.3');
        chai_1.assert.strictEqual(await cache.get(), SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray);
        (0, chai_1.assert)(configGetStub.calledOnceWith('system-tray-setting'));
        (0, chai_1.assert)(configSetStub.calledOnceWith('system-tray-setting', SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray));
    });
    it('returns the stored preference if system tray is supported and something is stored', async () => {
        sandbox.stub(process, 'platform').value('win32');
        sqlCallStub.resolves({ value: 'MinimizeToSystemTray' });
        const cache = new SystemTraySettingCache_1.SystemTraySettingCache(sql, config, [], '1.2.3');
        chai_1.assert.strictEqual(await cache.get(), SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray);
        (0, chai_1.assert)(configGetStub.calledOnceWith('system-tray-setting'));
        (0, chai_1.assert)(configSetStub.calledOnceWith('system-tray-setting', SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray));
    });
    it('returns the cached preference if system tray is supported and something is stored', async () => {
        sandbox.stub(process, 'platform').value('win32');
        configGetStub.returns('MinimizeToSystemTray');
        const cache = new SystemTraySettingCache_1.SystemTraySettingCache(sql, config, [], '1.2.3');
        chai_1.assert.strictEqual(await cache.get(), SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray);
        (0, chai_1.assert)(configGetStub.calledOnceWith('system-tray-setting'));
        sinon.assert.notCalled(sqlCallStub);
    });
    it('only kicks off one request to the database if multiple sources ask at once', async () => {
        sandbox.stub(process, 'platform').value('win32');
        const cache = new SystemTraySettingCache_1.SystemTraySettingCache(sql, config, [], '1.2.3');
        await Promise.all([cache.get(), cache.get(), cache.get()]);
        (0, chai_1.assert)(configGetStub.calledOnceWith('system-tray-setting'));
        sinon.assert.calledOnce(sqlCallStub);
    });
    it('returns DoNotUseSystemTray if system tray is unsupported and there are no CLI flags', async () => {
        sandbox.stub(process, 'platform').value('darwin');
        const cache = new SystemTraySettingCache_1.SystemTraySettingCache(sql, config, [], '1.2.3');
        chai_1.assert.strictEqual(await cache.get(), SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray);
        sinon.assert.notCalled(configGetStub);
        sinon.assert.notCalled(configSetStub);
        sinon.assert.notCalled(sqlCallStub);
    });
});
