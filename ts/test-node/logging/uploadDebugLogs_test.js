"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const got_1 = __importDefault(require("got"));
const form_data_1 = __importDefault(require("form-data"));
const util = __importStar(require("util"));
const zlib = __importStar(require("zlib"));
const debuglogs_1 = require("../../logging/debuglogs");
const gzip = util.promisify(zlib.gzip);
describe('upload', () => {
    beforeEach(function beforeEach() {
        this.sandbox = sinon.createSandbox();
        this.sandbox.stub(process, 'platform').get(() => 'linux');
        this.fakeGet = this.sandbox.stub(got_1.default, 'get');
        this.fakePost = this.sandbox.stub(got_1.default, 'post');
        this.fakeGet.resolves({
            body: {
                fields: {
                    foo: 'bar',
                    key: 'abc123',
                },
                url: 'https://example.com/fake-upload',
            },
        });
        this.fakePost.resolves({ statusCode: 204 });
    });
    afterEach(function afterEach() {
        this.sandbox.restore();
    });
    it('makes a request to get the S3 bucket, then uploads it there', async function test() {
        chai_1.assert.strictEqual(await (0, debuglogs_1.upload)('hello world', '1.2.3'), 'https://debuglogs.org/abc123.gz');
        sinon.assert.calledOnce(this.fakeGet);
        sinon.assert.calledWith(this.fakeGet, 'https://debuglogs.org', {
            responseType: 'json',
            headers: { 'User-Agent': 'Signal-Desktop/1.2.3 Linux' },
        });
        const compressedContent = await gzip('hello world');
        sinon.assert.calledOnce(this.fakePost);
        sinon.assert.calledWith(this.fakePost, 'https://example.com/fake-upload', {
            headers: { 'User-Agent': 'Signal-Desktop/1.2.3 Linux' },
            body: sinon.match((value) => {
                if (!(value instanceof form_data_1.default)) {
                    return false;
                }
                // `FormData` doesn't offer high-level APIs for fetching data, so we do this.
                const buffer = value.getBuffer();
                (0, chai_1.assert)(buffer.includes(compressedContent), 'gzipped content was not in body');
                return true;
            }, 'FormData'),
        });
    });
    it("rejects if we can't get a token", async function test() {
        this.fakeGet.rejects(new Error('HTTP request failure'));
        let err;
        try {
            await (0, debuglogs_1.upload)('hello world', '1.2.3');
        }
        catch (e) {
            err = e;
        }
        chai_1.assert.instanceOf(err, Error);
    });
    it('rejects with an invalid token body', async function test() {
        const bodies = [
            null,
            {},
            { fields: {} },
            { fields: { nokey: 'ok' } },
            { fields: { key: '123' } },
            { fields: { key: '123' }, url: { not: 'a string' } },
            { fields: { key: '123' }, url: 'http://notsecure.example.com' },
            { fields: { key: '123' }, url: 'not a valid URL' },
        ];
        for (const body of bodies) {
            this.fakeGet.resolves({ body });
            let err;
            try {
                // Again, these should be run serially.
                // eslint-disable-next-line no-await-in-loop
                await (0, debuglogs_1.upload)('hello world', '1.2.3');
            }
            catch (e) {
                err = e;
            }
            chai_1.assert.instanceOf(err, Error);
        }
    });
    it("rejects if the upload doesn't return a 204", async function test() {
        this.fakePost.resolves({ statusCode: 400 });
        let err;
        try {
            await (0, debuglogs_1.upload)('hello world', '1.2.3');
        }
        catch (e) {
            err = e;
        }
        chai_1.assert.instanceOf(err, Error);
    });
});
