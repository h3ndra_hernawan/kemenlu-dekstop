"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.assertRejects = void 0;
const chai_1 = require("chai");
async function assertRejects(fn) {
    let err;
    try {
        await fn();
    }
    catch (e) {
        err = e;
    }
    (0, chai_1.assert)(err instanceof Error, 'Expected promise to reject with an Error, but it resolved');
}
exports.assertRejects = assertRejects;
