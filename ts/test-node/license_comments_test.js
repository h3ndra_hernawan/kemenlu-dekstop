"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
// This file is meant to be run frequently, so it doesn't check the license year. See the
// imported `license_comments` file for a job that does this, to be run on CI.
const chai_1 = require("chai");
const license_comments_1 = require("../util/lint/license_comments");
describe('license comments', () => {
    it('includes a license comment at the top of every relevant file', async function test() {
        // This usually executes quickly but can be slow in some cases, such as Windows CI.
        this.timeout(10000);
        await (0, license_comments_1.forEachRelevantFile)(async (file) => {
            let firstLine;
            let secondLine;
            if ((0, license_comments_1.getExtension)(file) === '.sh') {
                const firstThreeLines = await (0, license_comments_1.readFirstLines)(file, 3);
                [, firstLine, secondLine] = firstThreeLines;
            }
            else {
                [firstLine, secondLine] = await (0, license_comments_1.readFirstLines)(file, 2);
            }
            const { groups = {} } = firstLine.match(/Copyright (?<startYearWithDash>\d{4}-)?(?<endYearString>\d{4}) Signal Messenger, LLC/) || [];
            const { startYearWithDash, endYearString } = groups;
            const endYear = Number(endYearString);
            // We added these comments in 2020.
            chai_1.assert.isAtLeast(endYear, 2020, `First line of ${file} is missing correct license header comment`);
            if (startYearWithDash) {
                const startYear = Number(startYearWithDash.slice(0, -1));
                chai_1.assert.isBelow(startYear, endYear, `Starting license year of ${file} is not below the ending year`);
            }
            chai_1.assert.include(secondLine, 'SPDX-License-Identifier: AGPL-3.0-only', `Second line of ${file} is missing correct license header comment`);
        });
    });
});
