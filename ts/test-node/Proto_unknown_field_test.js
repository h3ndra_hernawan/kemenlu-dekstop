"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const protobufjs_1 = require("protobufjs");
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const { Partial, Full } = protobufjs_1.Root.fromJSON({
    nested: {
        test: {
            nested: {
                Partial: {
                    fields: {
                        a: {
                            type: 'string',
                            id: 1,
                        },
                        c: {
                            type: 'int32',
                            id: 3,
                        },
                    },
                },
                Full: {
                    fields: {
                        a: {
                            type: 'string',
                            id: 1,
                        },
                        b: {
                            type: 'bool',
                            id: 2,
                        },
                        c: {
                            type: 'int32',
                            id: 3,
                        },
                        d: {
                            type: 'bytes',
                            id: 4,
                        },
                    },
                },
            },
        },
    },
}).nested.test;
describe('Proto#__unknownFields', () => {
    it('should encode and decode with unknown fields', () => {
        const full = Full.encode({
            a: 'hello',
            b: true,
            c: 42,
            d: Buffer.from('ohai'),
        }).finish();
        const partial = Partial.decode(full);
        chai_1.assert.strictEqual(partial.a, 'hello');
        chai_1.assert.strictEqual(partial.c, 42);
        chai_1.assert.strictEqual(partial.__unknownFields.length, 2);
        chai_1.assert.strictEqual(Buffer.from(partial.__unknownFields[0]).toString('hex'), '1001');
        chai_1.assert.strictEqual(Buffer.from(partial.__unknownFields[1]).toString('hex'), '22046f686169');
        const encoded = Partial.encode({
            a: partial.a,
            c: partial.c,
            __unknownFields: partial.__unknownFields,
        }).finish();
        const decoded = Full.decode(encoded);
        chai_1.assert.strictEqual(decoded.a, 'hello');
        chai_1.assert.strictEqual(decoded.b, true);
        chai_1.assert.strictEqual(decoded.c, 42);
        chai_1.assert.strictEqual(Buffer.from(decoded.d).toString(), 'ohai');
        const concat = Partial.encode({
            a: partial.a,
            c: partial.c,
            __unknownFields: [Buffer.concat(partial.__unknownFields)],
        }).finish();
        const decodedConcat = Full.decode(concat);
        chai_1.assert.strictEqual(decodedConcat.a, 'hello');
        chai_1.assert.isTrue(decodedConcat.b);
        chai_1.assert.strictEqual(decodedConcat.c, 42);
        chai_1.assert.strictEqual(Buffer.from(decodedConcat.d).toString(), 'ohai');
    });
    it('should decode unknown fields before reencoding them', () => {
        const full = Full.encode({
            a: 'hello',
            b: true,
            c: 42,
            d: Buffer.from('ohai'),
        }).finish();
        const partial = Partial.decode(full);
        chai_1.assert.isUndefined(partial.b);
        const encoded = Full.encode(Object.assign(Object.assign({}, partial), { b: false })).finish();
        const decoded = Full.decode(encoded);
        chai_1.assert.strictEqual(decoded.a, 'hello');
        chai_1.assert.isFalse(decoded.b);
        chai_1.assert.strictEqual(decoded.c, 42);
        chai_1.assert.strictEqual(Buffer.from(decoded.d).toString(), 'ohai');
    });
});
