"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const heic_convert_1 = __importDefault(require("heic-convert"));
const worker_threads_1 = require("worker_threads");
if (!worker_threads_1.parentPort) {
    throw new Error('Must run as a worker thread');
}
const port = worker_threads_1.parentPort;
function respond(uuid, error, response) {
    const wrappedResponse = {
        uuid,
        error: error ? error.stack : undefined,
        response,
    };
    port.postMessage(wrappedResponse);
}
port.on('message', async ({ uuid, data }) => {
    try {
        const file = await (0, heic_convert_1.default)({
            buffer: new Uint8Array(data),
            format: 'JPEG',
            quality: 0.75,
        });
        respond(uuid, undefined, file);
    }
    catch (error) {
        respond(uuid, error, undefined);
    }
});
