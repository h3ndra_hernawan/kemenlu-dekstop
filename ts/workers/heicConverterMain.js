"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getHeicConverter = void 0;
const path_1 = require("path");
const worker_threads_1 = require("worker_threads");
const ASAR_PATTERN = /app\.asar$/;
function getHeicConverter() {
    let appDir = (0, path_1.join)(__dirname, '..', '..');
    let isBundled = false;
    if (ASAR_PATTERN.test(appDir)) {
        appDir = appDir.replace(ASAR_PATTERN, 'app.asar.unpacked');
        isBundled = true;
    }
    const scriptDir = (0, path_1.join)(appDir, 'ts', 'workers');
    const worker = new worker_threads_1.Worker((0, path_1.join)(scriptDir, isBundled ? 'heicConverter.bundle.js' : 'heicConverterWorker.js'));
    const ResponseMap = new Map();
    worker.on('message', (wrappedResponse) => {
        const { uuid } = wrappedResponse;
        const resolve = ResponseMap.get(uuid);
        if (!resolve) {
            throw new Error(`Cannot find resolver for ${uuid}`);
        }
        resolve(wrappedResponse);
    });
    return async (uuid, data) => {
        const wrappedRequest = {
            uuid,
            data,
        };
        const result = new Promise(resolve => {
            ResponseMap.set(uuid, resolve);
        });
        worker.postMessage(wrappedRequest);
        return result;
    };
}
exports.getHeicConverter = getHeicConverter;
