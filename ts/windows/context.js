"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignalContext = void 0;
const electron_1 = require("electron");
const url_1 = __importDefault(require("url"));
const Bytes_1 = require("../context/Bytes");
const Crypto_1 = require("../context/Crypto");
const Timers_1 = require("../context/Timers");
const setupI18n_1 = require("../util/setupI18n");
const environment_1 = require("../environment");
const assert_1 = require("../util/assert");
const preload_1 = require("../util/preload");
const set_up_renderer_logging_1 = require("../logging/set_up_renderer_logging");
const waitForSettingsChange_1 = require("./waitForSettingsChange");
const createNativeThemeListener_1 = require("../context/createNativeThemeListener");
const config = url_1.default.parse(window.location.toString(), true).query;
const { locale } = config;
(0, assert_1.strictAssert)(locale, 'locale could not be parsed from config');
(0, assert_1.strictAssert)(typeof locale === 'string', 'locale is not a string');
const localeMessages = electron_1.ipcRenderer.sendSync('locale-data');
(0, environment_1.setEnvironment)((0, environment_1.parseEnvironment)(config.environment));
(0, assert_1.strictAssert)(Boolean(window.SignalContext), 'context must be defined');
(0, set_up_renderer_logging_1.initialize)();
exports.SignalContext = {
    Settings: {
        themeSetting: (0, preload_1.createSetting)('themeSetting', { setter: false }),
        waitForChange: waitForSettingsChange_1.waitForSettingsChange,
    },
    bytes: new Bytes_1.Bytes(),
    config,
    crypto: new Crypto_1.Crypto(),
    getAppInstance: () => config.appInstance ? String(config.appInstance) : undefined,
    getEnvironment: environment_1.getEnvironment,
    getNodeVersion: () => String(config.node_version),
    getVersion: () => String(config.version),
    getPath: (name) => {
        return String(config[`${name}Path`]);
    },
    i18n: (0, setupI18n_1.setupI18n)(locale, localeMessages),
    log: window.SignalContext.log,
    nativeThemeListener: (0, createNativeThemeListener_1.createNativeThemeListener)(electron_1.ipcRenderer, window),
    setIsCallActive(isCallActive) {
        electron_1.ipcRenderer.send('set-is-call-active', isCallActive);
    },
    timers: new Timers_1.Timers(),
};
window.SignalContext = exports.SignalContext;
