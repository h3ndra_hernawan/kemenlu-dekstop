"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_dom_1 = __importDefault(require("react-dom"));
const electron_1 = require("electron");
const context_1 = require("../context");
const About_1 = require("../../components/About");
electron_1.contextBridge.exposeInMainWorld('SignalContext', Object.assign(Object.assign({}, context_1.SignalContext), { renderWindow: () => {
        const environmentText = [context_1.SignalContext.getEnvironment()];
        const appInstance = context_1.SignalContext.getAppInstance();
        if (appInstance) {
            environmentText.push(appInstance);
        }
        react_dom_1.default.render(react_1.default.createElement(About_1.About, {
            closeAbout: () => electron_1.ipcRenderer.send('close-about'),
            environment: environmentText.join(' - '),
            i18n: context_1.SignalContext.i18n,
            version: context_1.SignalContext.getVersion(),
        }), document.getElementById('app'));
    } }));
