"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_dom_1 = __importDefault(require("react-dom"));
const electron_1 = require("electron");
const context_1 = require("../context");
const DebugLogWindow_1 = require("../../components/DebugLogWindow");
const debugLog = __importStar(require("../../logging/debuglogs"));
electron_1.contextBridge.exposeInMainWorld('SignalContext', Object.assign(Object.assign({}, context_1.SignalContext), { renderWindow: () => {
        const environmentText = [context_1.SignalContext.getEnvironment()];
        const appInstance = context_1.SignalContext.getAppInstance();
        if (appInstance) {
            environmentText.push(appInstance);
        }
        react_dom_1.default.render(react_1.default.createElement(DebugLogWindow_1.DebugLogWindow, {
            closeWindow: () => electron_1.ipcRenderer.send('close-debug-log'),
            downloadLog: (logText) => electron_1.ipcRenderer.send('show-debug-log-save-dialog', logText),
            i18n: context_1.SignalContext.i18n,
            fetchLogs() {
                return debugLog.fetch(context_1.SignalContext.getNodeVersion(), context_1.SignalContext.getVersion());
            },
            uploadLogs(logs) {
                return debugLog.upload(logs, context_1.SignalContext.getVersion());
            },
        }), document.getElementById('app'));
    } }));
