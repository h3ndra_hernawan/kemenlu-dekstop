"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_dom_1 = __importDefault(require("react-dom"));
const electron_1 = require("electron");
const context_1 = require("../context");
const CallingScreenSharingController_1 = require("../../components/CallingScreenSharingController");
electron_1.contextBridge.exposeInMainWorld('SignalContext', context_1.SignalContext);
function renderScreenSharingController(presentedSourceName) {
    react_dom_1.default.render(react_1.default.createElement(CallingScreenSharingController_1.CallingScreenSharingController, {
        i18n: context_1.SignalContext.i18n,
        onCloseController: () => electron_1.ipcRenderer.send('close-screen-share-controller'),
        onStopSharing: () => electron_1.ipcRenderer.send('stop-screen-share'),
        presentedSourceName,
    }), document.getElementById('app'));
}
electron_1.ipcRenderer.once('render-screen-sharing-controller', (_, name) => {
    renderScreenSharingController(name);
});
