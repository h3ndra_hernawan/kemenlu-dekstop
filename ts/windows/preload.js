"use strict";
// Copyright 2017-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
const preload_1 = require("../util/preload");
// ChatColorPicker redux hookups
(0, preload_1.installCallback)('getCustomColors');
(0, preload_1.installCallback)('getConversationsWithCustomColor');
(0, preload_1.installCallback)('addCustomColor');
(0, preload_1.installCallback)('editCustomColor');
(0, preload_1.installCallback)('removeCustomColor');
(0, preload_1.installCallback)('removeCustomColorOnConversations');
(0, preload_1.installCallback)('resetAllChatColors');
(0, preload_1.installCallback)('resetDefaultChatColor');
(0, preload_1.installCallback)('setGlobalDefaultConversationColor');
(0, preload_1.installCallback)('getDefaultConversationColor');
(0, preload_1.installCallback)('persistZoomFactor');
(0, preload_1.installCallback)('closeDB');
// Getters only. These are set by the primary device
(0, preload_1.installSetting)('blockedCount', {
    setter: false,
});
(0, preload_1.installSetting)('linkPreviewSetting', {
    setter: false,
});
(0, preload_1.installSetting)('phoneNumberDiscoverabilitySetting', {
    setter: false,
});
(0, preload_1.installSetting)('phoneNumberSharingSetting', {
    setter: false,
});
(0, preload_1.installSetting)('readReceiptSetting', {
    setter: false,
});
(0, preload_1.installSetting)('typingIndicatorSetting', {
    setter: false,
});
(0, preload_1.installSetting)('alwaysRelayCalls');
(0, preload_1.installSetting)('audioNotification');
(0, preload_1.installSetting)('autoDownloadUpdate');
(0, preload_1.installSetting)('autoLaunch');
(0, preload_1.installSetting)('countMutedConversations');
(0, preload_1.installSetting)('callRingtoneNotification');
(0, preload_1.installSetting)('callSystemNotification');
(0, preload_1.installSetting)('deviceName');
(0, preload_1.installSetting)('hideMenuBar');
(0, preload_1.installSetting)('incomingCallNotification');
(0, preload_1.installCallback)('isPhoneNumberSharingEnabled');
(0, preload_1.installCallback)('isPrimary');
(0, preload_1.installCallback)('syncRequest');
(0, preload_1.installSetting)('notificationDrawAttention');
(0, preload_1.installSetting)('notificationSetting');
(0, preload_1.installSetting)('spellCheck');
(0, preload_1.installSetting)('lastSyncTime');
(0, preload_1.installSetting)('systemTraySetting');
(0, preload_1.installSetting)('themeSetting');
(0, preload_1.installSetting)('universalExpireTimer');
(0, preload_1.installSetting)('zoomFactor');
// Media Settings
(0, preload_1.installCallback)('getAvailableIODevices');
(0, preload_1.installSetting)('preferredAudioInputDevice');
(0, preload_1.installSetting)('preferredAudioOutputDevice');
(0, preload_1.installSetting)('preferredVideoInputDevice');
window.getMediaPermissions = () => electron_1.ipcRenderer.invoke('settings:get:mediaPermissions');
window.getMediaCameraPermissions = () => electron_1.ipcRenderer.invoke('settings:get:mediaCameraPermissions');
