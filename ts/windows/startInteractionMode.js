"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.startInteractionMode = void 0;
let initialized = false;
let interactionMode = 'mouse';
function startInteractionMode() {
    if (initialized) {
        return;
    }
    initialized = true;
    document.body.classList.add('mouse-mode');
    window.enterKeyboardMode = () => {
        var _a, _b, _c, _d;
        if (interactionMode === 'keyboard') {
            return;
        }
        interactionMode = 'keyboard';
        document.body.classList.add('keyboard-mode');
        document.body.classList.remove('mouse-mode');
        const clearSelectedMessage = (_b = (_a = window.reduxActions) === null || _a === void 0 ? void 0 : _a.conversations) === null || _b === void 0 ? void 0 : _b.clearSelectedMessage;
        if (clearSelectedMessage) {
            clearSelectedMessage();
        }
        const userChanged = (_d = (_c = window.reduxActions) === null || _c === void 0 ? void 0 : _c.user) === null || _d === void 0 ? void 0 : _d.userChanged;
        if (userChanged) {
            userChanged({ interactionMode });
        }
    };
    window.enterMouseMode = () => {
        var _a, _b, _c, _d;
        if (interactionMode === 'mouse') {
            return;
        }
        interactionMode = 'mouse';
        document.body.classList.add('mouse-mode');
        document.body.classList.remove('keyboard-mode');
        const clearSelectedMessage = (_b = (_a = window.reduxActions) === null || _a === void 0 ? void 0 : _a.conversations) === null || _b === void 0 ? void 0 : _b.clearSelectedMessage;
        if (clearSelectedMessage) {
            clearSelectedMessage();
        }
        const userChanged = (_d = (_c = window.reduxActions) === null || _c === void 0 ? void 0 : _c.user) === null || _d === void 0 ? void 0 : _d.userChanged;
        if (userChanged) {
            userChanged({ interactionMode });
        }
    };
    document.addEventListener('keydown', event => {
        if (event.key === 'Tab') {
            window.enterKeyboardMode();
        }
    }, true);
    document.addEventListener('wheel', window.enterMouseMode, true);
    document.addEventListener('mousedown', window.enterMouseMode, true);
    window.getInteractionMode = () => interactionMode;
}
exports.startInteractionMode = startInteractionMode;
