"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.saveAttachmentToDisk = exports.openFileInFolder = exports.createDoesExist = exports.createAbsolutePathGetter = exports.createWriterForExisting = exports.createWriterForNew = exports.copyIntoAttachmentsDirectory = exports.createName = exports.getRelativePath = exports.createReader = void 0;
const electron_1 = require("electron");
const lodash_1 = require("lodash");
const path_1 = require("path");
const fs_extra_1 = __importDefault(require("fs-extra"));
const v4_1 = __importDefault(require("uuid/v4"));
const Crypto_1 = require("../Crypto");
const Bytes = __importStar(require("../Bytes"));
const isPathInside_1 = require("../util/isPathInside");
const windowsZoneIdentifier_1 = require("../util/windowsZoneIdentifier");
const OS_1 = require("../OS");
__exportStar(require("../util/attachments"), exports);
let xattr;
try {
    // eslint-disable-next-line max-len
    // eslint-disable-next-line global-require, import/no-extraneous-dependencies, import/no-unresolved
    xattr = require('fs-xattr');
}
catch (e) {
    (_a = window.SignalContext.log) === null || _a === void 0 ? void 0 : _a.info('x-attr dependency did not load successfully');
}
const createReader = (root) => {
    if (!(0, lodash_1.isString)(root)) {
        throw new TypeError("'root' must be a path");
    }
    return async (relativePath) => {
        if (!(0, lodash_1.isString)(relativePath)) {
            throw new TypeError("'relativePath' must be a string");
        }
        const absolutePath = (0, path_1.join)(root, relativePath);
        const normalized = (0, path_1.normalize)(absolutePath);
        if (!(0, isPathInside_1.isPathInside)(normalized, root)) {
            throw new Error('Invalid relative path');
        }
        return fs_extra_1.default.readFile(normalized);
    };
};
exports.createReader = createReader;
const getRelativePath = (name) => {
    if (!(0, lodash_1.isString)(name)) {
        throw new TypeError("'name' must be a string");
    }
    const prefix = name.slice(0, 2);
    return (0, path_1.join)(prefix, name);
};
exports.getRelativePath = getRelativePath;
const createName = (suffix = '') => `${Bytes.toHex((0, Crypto_1.getRandomBytes)(32))}${suffix}`;
exports.createName = createName;
const copyIntoAttachmentsDirectory = (root) => {
    if (!(0, lodash_1.isString)(root)) {
        throw new TypeError("'root' must be a path");
    }
    const userDataPath = window.SignalContext.getPath('userData');
    return async (sourcePath) => {
        if (!(0, lodash_1.isString)(sourcePath)) {
            throw new TypeError('sourcePath must be a string');
        }
        if (!(0, isPathInside_1.isPathInside)(sourcePath, userDataPath)) {
            throw new Error("'sourcePath' must be relative to the user config directory");
        }
        const name = (0, exports.createName)();
        const relativePath = (0, exports.getRelativePath)(name);
        const absolutePath = (0, path_1.join)(root, relativePath);
        const normalized = (0, path_1.normalize)(absolutePath);
        if (!(0, isPathInside_1.isPathInside)(normalized, root)) {
            throw new Error('Invalid relative path');
        }
        await fs_extra_1.default.ensureFile(normalized);
        await fs_extra_1.default.copy(sourcePath, normalized);
        const { size } = await fs_extra_1.default.stat(normalized);
        return {
            path: relativePath,
            size,
        };
    };
};
exports.copyIntoAttachmentsDirectory = copyIntoAttachmentsDirectory;
const createWriterForNew = (root, suffix) => {
    if (!(0, lodash_1.isString)(root)) {
        throw new TypeError("'root' must be a path");
    }
    return async (bytes) => {
        if (!(0, lodash_1.isTypedArray)(bytes)) {
            throw new TypeError("'bytes' must be a typed array");
        }
        const name = (0, exports.createName)(suffix);
        const relativePath = (0, exports.getRelativePath)(name);
        return (0, exports.createWriterForExisting)(root)({
            data: bytes,
            path: relativePath,
        });
    };
};
exports.createWriterForNew = createWriterForNew;
const createWriterForExisting = (root) => {
    if (!(0, lodash_1.isString)(root)) {
        throw new TypeError("'root' must be a path");
    }
    return async ({ data: bytes, path: relativePath, }) => {
        if (!(0, lodash_1.isString)(relativePath)) {
            throw new TypeError("'relativePath' must be a path");
        }
        if (!(0, lodash_1.isTypedArray)(bytes)) {
            throw new TypeError("'arrayBuffer' must be an array buffer");
        }
        const buffer = Buffer.from(bytes);
        const absolutePath = (0, path_1.join)(root, relativePath);
        const normalized = (0, path_1.normalize)(absolutePath);
        if (!(0, isPathInside_1.isPathInside)(normalized, root)) {
            throw new Error('Invalid relative path');
        }
        await fs_extra_1.default.ensureFile(normalized);
        await fs_extra_1.default.writeFile(normalized, buffer);
        return relativePath;
    };
};
exports.createWriterForExisting = createWriterForExisting;
const createAbsolutePathGetter = (rootPath) => (relativePath) => {
    const absolutePath = (0, path_1.join)(rootPath, relativePath);
    const normalized = (0, path_1.normalize)(absolutePath);
    if (!(0, isPathInside_1.isPathInside)(normalized, rootPath)) {
        throw new Error('Invalid relative path');
    }
    return normalized;
};
exports.createAbsolutePathGetter = createAbsolutePathGetter;
const createDoesExist = (root) => {
    if (!(0, lodash_1.isString)(root)) {
        throw new TypeError("'root' must be a path");
    }
    return async (relativePath) => {
        if (!(0, lodash_1.isString)(relativePath)) {
            throw new TypeError("'relativePath' must be a string");
        }
        const absolutePath = (0, path_1.join)(root, relativePath);
        const normalized = (0, path_1.normalize)(absolutePath);
        if (!(0, isPathInside_1.isPathInside)(normalized, root)) {
            throw new Error('Invalid relative path');
        }
        try {
            await fs_extra_1.default.access(normalized, fs_extra_1.default.constants.F_OK);
            return true;
        }
        catch (error) {
            return false;
        }
    };
};
exports.createDoesExist = createDoesExist;
const openFileInFolder = async (target) => {
    electron_1.ipcRenderer.send('show-item-in-folder', target);
};
exports.openFileInFolder = openFileInFolder;
const showSaveDialog = (defaultPath) => {
    return electron_1.ipcRenderer.invoke('show-save-dialog', { defaultPath });
};
async function writeWithAttributes(target, data) {
    var _a;
    await fs_extra_1.default.writeFile(target, Buffer.from(data));
    if (process.platform === 'darwin' && xattr) {
        // kLSQuarantineTypeInstantMessageAttachment
        const type = '0003';
        // Hexadecimal seconds since epoch
        const timestamp = Math.trunc(Date.now() / 1000).toString(16);
        const appName = 'Signal';
        const guid = (0, v4_1.default)();
        // https://ilostmynotes.blogspot.com/2012/06/gatekeeper-xprotect-and-quarantine.html
        const attrValue = `${type};${timestamp};${appName};${guid}`;
        await xattr.set(target, 'com.apple.quarantine', attrValue);
    }
    else if ((0, OS_1.isWindows)()) {
        // This operation may fail (see the function's comments), which is not a show-stopper.
        try {
            await (0, windowsZoneIdentifier_1.writeWindowsZoneIdentifier)(target);
        }
        catch (err) {
            (_a = window.SignalContext.log) === null || _a === void 0 ? void 0 : _a.warn('Failed to write Windows Zone.Identifier file; continuing');
        }
    }
}
const saveAttachmentToDisk = async ({ data, name, }) => {
    const { canceled, filePath } = await showSaveDialog(name);
    if (canceled || !filePath) {
        return null;
    }
    await writeWithAttributes(filePath, data);
    const fileBasename = (0, path_1.basename)(filePath);
    return {
        fullPath: filePath,
        name: fileBasename,
    };
};
exports.saveAttachmentToDisk = saveAttachmentToDisk;
