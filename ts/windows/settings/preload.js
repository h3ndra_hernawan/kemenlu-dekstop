"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_dom_1 = __importDefault(require("react-dom"));
const electron_1 = require("electron");
const context_1 = require("../context");
const Settings = __importStar(require("../../types/Settings"));
const Preferences_1 = require("../../components/Preferences");
const SystemTraySetting_1 = require("../../types/SystemTraySetting");
const awaitObject_1 = require("../../util/awaitObject");
const preload_1 = require("../../util/preload");
const startInteractionMode_1 = require("../startInteractionMode");
function doneRendering() {
    electron_1.ipcRenderer.send('settings-done-rendering');
}
const settingAudioNotification = (0, preload_1.createSetting)('audioNotification');
const settingAutoDownloadUpdate = (0, preload_1.createSetting)('autoDownloadUpdate');
const settingAutoLaunch = (0, preload_1.createSetting)('autoLaunch');
const settingCallRingtoneNotification = (0, preload_1.createSetting)('callRingtoneNotification');
const settingCallSystemNotification = (0, preload_1.createSetting)('callSystemNotification');
const settingCountMutedConversations = (0, preload_1.createSetting)('countMutedConversations');
const settingDeviceName = (0, preload_1.createSetting)('deviceName', { setter: false });
const settingHideMenuBar = (0, preload_1.createSetting)('hideMenuBar');
const settingIncomingCallNotification = (0, preload_1.createSetting)('incomingCallNotification');
const settingMediaCameraPermissions = (0, preload_1.createSetting)('mediaCameraPermissions');
const settingMediaPermissions = (0, preload_1.createSetting)('mediaPermissions');
const settingNotificationDrawAttention = (0, preload_1.createSetting)('notificationDrawAttention');
const settingNotificationSetting = (0, preload_1.createSetting)('notificationSetting');
const settingRelayCalls = (0, preload_1.createSetting)('alwaysRelayCalls');
const settingSpellCheck = (0, preload_1.createSetting)('spellCheck');
const settingTheme = (0, preload_1.createSetting)('themeSetting');
const settingSystemTraySetting = (0, preload_1.createSetting)('systemTraySetting');
const settingLastSyncTime = (0, preload_1.createSetting)('lastSyncTime');
const settingZoomFactor = (0, preload_1.createSetting)('zoomFactor');
// Getters only.
const settingBlockedCount = (0, preload_1.createSetting)('blockedCount');
const settingLinkPreview = (0, preload_1.createSetting)('linkPreviewSetting', {
    setter: false,
});
const settingPhoneNumberDiscoverability = (0, preload_1.createSetting)('phoneNumberDiscoverabilitySetting', { setter: false });
const settingPhoneNumberSharing = (0, preload_1.createSetting)('phoneNumberSharingSetting', {
    setter: false,
});
const settingReadReceipts = (0, preload_1.createSetting)('readReceiptSetting', {
    setter: false,
});
const settingTypingIndicators = (0, preload_1.createSetting)('typingIndicatorSetting', {
    setter: false,
});
// Media settings
const settingAudioInput = (0, preload_1.createSetting)('preferredAudioInputDevice');
const settingAudioOutput = (0, preload_1.createSetting)('preferredAudioOutputDevice');
const settingVideoInput = (0, preload_1.createSetting)('preferredVideoInputDevice');
const settingUniversalExpireTimer = (0, preload_1.createSetting)('universalExpireTimer');
// Callbacks
const ipcGetAvailableIODevices = (0, preload_1.createCallback)('getAvailableIODevices');
const ipcGetCustomColors = (0, preload_1.createCallback)('getCustomColors');
const ipcIsSyncNotSupported = (0, preload_1.createCallback)('isPrimary');
const ipcMakeSyncRequest = (0, preload_1.createCallback)('syncRequest');
const ipcPNP = (0, preload_1.createCallback)('isPhoneNumberSharingEnabled');
// ChatColorPicker redux hookups
// The redux actions update over IPC through a preferences re-render
const ipcGetDefaultConversationColor = (0, preload_1.createCallback)('getDefaultConversationColor');
const ipcGetConversationsWithCustomColor = (0, preload_1.createCallback)('getConversationsWithCustomColor');
const ipcAddCustomColor = (0, preload_1.createCallback)('addCustomColor');
const ipcEditCustomColor = (0, preload_1.createCallback)('editCustomColor');
const ipcRemoveCustomColor = (0, preload_1.createCallback)('removeCustomColor');
const ipcRemoveCustomColorOnConversations = (0, preload_1.createCallback)('removeCustomColorOnConversations');
const ipcResetAllChatColors = (0, preload_1.createCallback)('resetAllChatColors');
const ipcResetDefaultChatColor = (0, preload_1.createCallback)('resetDefaultChatColor');
const ipcSetGlobalDefaultConversationColor = (0, preload_1.createCallback)('setGlobalDefaultConversationColor');
const DEFAULT_NOTIFICATION_SETTING = 'message';
function getSystemTraySettingValues(systemTraySetting) {
    const parsedSystemTraySetting = (0, SystemTraySetting_1.parseSystemTraySetting)(systemTraySetting);
    const hasMinimizeToAndStartInSystemTray = parsedSystemTraySetting ===
        SystemTraySetting_1.SystemTraySetting.MinimizeToAndStartInSystemTray;
    const hasMinimizeToSystemTray = (0, SystemTraySetting_1.shouldMinimizeToSystemTray)(parsedSystemTraySetting);
    return {
        hasMinimizeToAndStartInSystemTray,
        hasMinimizeToSystemTray,
    };
}
const renderPreferences = async () => {
    (0, startInteractionMode_1.startInteractionMode)();
    const { blockedCount, deviceName, hasAudioNotifications, hasAutoDownloadUpdate, hasAutoLaunch, hasCallNotifications, hasCallRingtoneNotification, hasCountMutedConversations, hasHideMenuBar, hasIncomingCallNotifications, hasLinkPreviews, hasMediaCameraPermissions, hasMediaPermissions, hasNotificationAttention, hasReadReceipts, hasRelayCalls, hasSpellCheck, hasTypingIndicators, isPhoneNumberSharingSupported, lastSyncTime, notificationContent, selectedCamera, selectedMicrophone, selectedSpeaker, systemTraySetting, themeSetting, universalExpireTimer, whoCanFindMe, whoCanSeeMe, zoomFactor, availableIODevices, customColors, isSyncNotSupported, defaultConversationColor, } = await (0, awaitObject_1.awaitObject)({
        blockedCount: settingBlockedCount.getValue(),
        deviceName: settingDeviceName.getValue(),
        hasAudioNotifications: settingAudioNotification.getValue(),
        hasAutoDownloadUpdate: settingAutoDownloadUpdate.getValue(),
        hasAutoLaunch: settingAutoLaunch.getValue(),
        hasCallNotifications: settingCallSystemNotification.getValue(),
        hasCallRingtoneNotification: settingCallRingtoneNotification.getValue(),
        hasCountMutedConversations: settingCountMutedConversations.getValue(),
        hasHideMenuBar: settingHideMenuBar.getValue(),
        hasIncomingCallNotifications: settingIncomingCallNotification.getValue(),
        hasLinkPreviews: settingLinkPreview.getValue(),
        hasMediaCameraPermissions: settingMediaCameraPermissions.getValue(),
        hasMediaPermissions: settingMediaPermissions.getValue(),
        hasNotificationAttention: settingNotificationDrawAttention.getValue(),
        hasReadReceipts: settingReadReceipts.getValue(),
        hasRelayCalls: settingRelayCalls.getValue(),
        hasSpellCheck: settingSpellCheck.getValue(),
        hasTypingIndicators: settingTypingIndicators.getValue(),
        isPhoneNumberSharingSupported: ipcPNP(),
        lastSyncTime: settingLastSyncTime.getValue(),
        notificationContent: settingNotificationSetting.getValue(),
        selectedCamera: settingVideoInput.getValue(),
        selectedMicrophone: settingAudioInput.getValue(),
        selectedSpeaker: settingAudioOutput.getValue(),
        systemTraySetting: settingSystemTraySetting.getValue(),
        themeSetting: settingTheme.getValue(),
        universalExpireTimer: settingUniversalExpireTimer.getValue(),
        whoCanFindMe: settingPhoneNumberDiscoverability.getValue(),
        whoCanSeeMe: settingPhoneNumberSharing.getValue(),
        zoomFactor: settingZoomFactor.getValue(),
        // Callbacks
        availableIODevices: ipcGetAvailableIODevices(),
        customColors: ipcGetCustomColors(),
        isSyncNotSupported: ipcIsSyncNotSupported(),
        defaultConversationColor: ipcGetDefaultConversationColor(),
    });
    const { availableCameras, availableMicrophones, availableSpeakers } = availableIODevices;
    const { hasMinimizeToAndStartInSystemTray, hasMinimizeToSystemTray } = getSystemTraySettingValues(systemTraySetting);
    const props = {
        // Settings
        availableCameras,
        availableMicrophones,
        availableSpeakers,
        blockedCount,
        customColors,
        defaultConversationColor,
        deviceName,
        hasAudioNotifications,
        hasAutoDownloadUpdate,
        hasAutoLaunch,
        hasCallNotifications,
        hasCallRingtoneNotification,
        hasCountMutedConversations,
        hasHideMenuBar,
        hasIncomingCallNotifications,
        hasLinkPreviews,
        hasMediaCameraPermissions,
        hasMediaPermissions,
        hasMinimizeToAndStartInSystemTray,
        hasMinimizeToSystemTray,
        hasNotificationAttention,
        hasNotifications: notificationContent !== 'off',
        hasReadReceipts,
        hasRelayCalls,
        hasSpellCheck,
        hasTypingIndicators,
        lastSyncTime,
        notificationContent,
        selectedCamera,
        selectedMicrophone,
        selectedSpeaker,
        themeSetting,
        universalExpireTimer,
        whoCanFindMe,
        whoCanSeeMe,
        zoomFactor,
        // Actions and other props
        addCustomColor: ipcAddCustomColor,
        closeSettings: () => electron_1.ipcRenderer.send('close-settings'),
        doDeleteAllData: () => electron_1.ipcRenderer.send('delete-all-data'),
        doneRendering,
        editCustomColor: ipcEditCustomColor,
        getConversationsWithCustomColor: ipcGetConversationsWithCustomColor,
        initialSpellCheckSetting: context_1.SignalContext.config.appStartInitialSpellcheckSetting === 'true',
        makeSyncRequest: ipcMakeSyncRequest,
        removeCustomColor: ipcRemoveCustomColor,
        removeCustomColorOnConversations: ipcRemoveCustomColorOnConversations,
        resetAllChatColors: ipcResetAllChatColors,
        resetDefaultChatColor: ipcResetDefaultChatColor,
        setGlobalDefaultConversationColor: ipcSetGlobalDefaultConversationColor,
        // Limited support features
        isAudioNotificationsSupported: Settings.isAudioNotificationSupported(),
        isAutoDownloadUpdatesSupported: Settings.isAutoDownloadUpdatesSupported(),
        isAutoLaunchSupported: Settings.isAutoLaunchSupported(),
        isHideMenuBarSupported: Settings.isHideMenuBarSupported(),
        isNotificationAttentionSupported: Settings.isDrawAttentionSupported(),
        isPhoneNumberSharingSupported,
        isSyncSupported: !isSyncNotSupported,
        isSystemTraySupported: Settings.isSystemTraySupported(context_1.SignalContext.getVersion()),
        // Change handlers
        onAudioNotificationsChange: reRender(settingAudioNotification.setValue),
        onAutoDownloadUpdateChange: reRender(settingAutoDownloadUpdate.setValue),
        onAutoLaunchChange: reRender(settingAutoLaunch.setValue),
        onCallNotificationsChange: reRender(settingCallSystemNotification.setValue),
        onCallRingtoneNotificationChange: reRender(settingCallRingtoneNotification.setValue),
        onCountMutedConversationsChange: reRender(settingCountMutedConversations.setValue),
        onHideMenuBarChange: reRender(settingHideMenuBar.setValue),
        onIncomingCallNotificationsChange: reRender(settingIncomingCallNotification.setValue),
        onLastSyncTimeChange: reRender(settingLastSyncTime.setValue),
        onMediaCameraPermissionsChange: reRender(settingMediaCameraPermissions.setValue),
        onMinimizeToAndStartInSystemTrayChange: reRender(async (value) => {
            await settingSystemTraySetting.setValue(value
                ? SystemTraySetting_1.SystemTraySetting.MinimizeToAndStartInSystemTray
                : SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray);
            return value;
        }),
        onMinimizeToSystemTrayChange: reRender(async (value) => {
            await settingSystemTraySetting.setValue(value
                ? SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray
                : SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray);
            return value;
        }),
        onMediaPermissionsChange: reRender(settingMediaPermissions.setValue),
        onNotificationAttentionChange: reRender(settingNotificationDrawAttention.setValue),
        onNotificationContentChange: reRender(settingNotificationSetting.setValue),
        onNotificationsChange: reRender(async (value) => {
            await settingNotificationSetting.setValue(value ? DEFAULT_NOTIFICATION_SETTING : 'off');
            return value;
        }),
        onRelayCallsChange: reRender(settingRelayCalls.setValue),
        onSelectedCameraChange: reRender(settingVideoInput.setValue),
        onSelectedMicrophoneChange: reRender(settingAudioInput.setValue),
        onSelectedSpeakerChange: reRender(settingAudioOutput.setValue),
        onSpellCheckChange: reRender(settingSpellCheck.setValue),
        onThemeChange: reRender(settingTheme.setValue),
        onUniversalExpireTimerChange: reRender(settingUniversalExpireTimer.setValue),
        // Zoom factor change doesn't require immediate rerender since it will:
        // 1. Update the zoom factor in the main window
        // 2. Trigger `preferred-size-changed` in the main process
        // 3. Finally result in `window.storage` update which will cause the
        //    rerender.
        onZoomFactorChange: settingZoomFactor.setValue,
        i18n: context_1.SignalContext.i18n,
    };
    function reRender(f) {
        return async (value) => {
            await f(value);
            renderPreferences();
        };
    }
    react_dom_1.default.render(react_1.default.createElement(Preferences_1.Preferences, props), document.getElementById('app'));
};
electron_1.ipcRenderer.on('preferences-changed', () => renderPreferences());
electron_1.contextBridge.exposeInMainWorld('SignalContext', Object.assign(Object.assign({}, context_1.SignalContext), { renderWindow: renderPreferences }));
