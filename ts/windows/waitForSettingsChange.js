"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.waitForSettingsChange = void 0;
const electron_1 = require("electron");
const explodePromise_1 = require("../util/explodePromise");
let preferencesChangeResolvers = new Array();
electron_1.ipcRenderer.on('preferences-changed', _event => {
    const resolvers = preferencesChangeResolvers;
    preferencesChangeResolvers = [];
    for (const resolve of resolvers) {
        resolve();
    }
});
function waitForSettingsChange() {
    const { promise, resolve } = (0, explodePromise_1.explodePromise)();
    preferencesChangeResolvers.push(resolve);
    return promise;
}
exports.waitForSettingsChange = waitForSettingsChange;
