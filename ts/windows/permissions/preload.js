"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_dom_1 = __importDefault(require("react-dom"));
const electron_1 = require("electron");
const context_1 = require("../context");
const preload_1 = require("../../util/preload");
const PermissionsPopup_1 = require("../../components/PermissionsPopup");
const mediaCameraPermissions = (0, preload_1.createSetting)('mediaCameraPermissions', {
    getter: false,
});
const mediaPermissions = (0, preload_1.createSetting)('mediaPermissions', {
    getter: false,
});
electron_1.contextBridge.exposeInMainWorld('nativeThemeListener', window.SignalContext.nativeThemeListener);
electron_1.contextBridge.exposeInMainWorld('SignalContext', Object.assign(Object.assign({}, context_1.SignalContext), { renderWindow: () => {
        const forCalling = context_1.SignalContext.config.forCalling === 'true';
        const forCamera = context_1.SignalContext.config.forCamera === 'true';
        let message;
        if (forCalling) {
            if (forCamera) {
                message = context_1.SignalContext.i18n('videoCallingPermissionNeeded');
            }
            else {
                message = context_1.SignalContext.i18n('audioCallingPermissionNeeded');
            }
        }
        else {
            message = context_1.SignalContext.i18n('audioPermissionNeeded');
        }
        function onClose() {
            electron_1.ipcRenderer.send('close-permissions-popup');
        }
        react_dom_1.default.render(react_1.default.createElement(PermissionsPopup_1.PermissionsPopup, {
            i18n: context_1.SignalContext.i18n,
            message,
            onAccept: () => {
                if (!forCamera) {
                    mediaPermissions.setValue(true);
                }
                else {
                    mediaCameraPermissions.setValue(true);
                }
                onClose();
            },
            onClose,
        }), document.getElementById('app'));
    } }));
