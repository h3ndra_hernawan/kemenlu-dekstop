"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
const context_1 = require("../context");
electron_1.contextBridge.exposeInMainWorld('SignalContext', context_1.SignalContext);
