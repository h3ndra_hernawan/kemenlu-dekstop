"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignedPreKeys = exports.SenderKeys = exports.PreKeys = exports.IdentityKeys = exports.Sessions = void 0;
/* eslint-disable max-classes-per-file */
const lodash_1 = require("lodash");
const signal_client_1 = require("@signalapp/signal-client");
const SignalProtocolStore_1 = require("./SignalProtocolStore");
const Address_1 = require("./types/Address");
const QualifiedAddress_1 = require("./types/QualifiedAddress");
function encodeAddress(address) {
    const name = address.name();
    const deviceId = address.deviceId();
    return Address_1.Address.create(name, deviceId);
}
function toQualifiedAddress(ourUuid, address) {
    return new QualifiedAddress_1.QualifiedAddress(ourUuid, encodeAddress(address));
}
class Sessions extends signal_client_1.SessionStore {
    constructor({ ourUuid, zone }) {
        super();
        this.ourUuid = ourUuid;
        this.zone = zone;
    }
    async saveSession(address, record) {
        await window.textsecure.storage.protocol.storeSession(toQualifiedAddress(this.ourUuid, address), record, { zone: this.zone });
    }
    async getSession(name) {
        const encodedAddress = toQualifiedAddress(this.ourUuid, name);
        const record = await window.textsecure.storage.protocol.loadSession(encodedAddress, { zone: this.zone });
        return record || null;
    }
    async getExistingSessions(addresses) {
        const encodedAddresses = addresses.map(addr => toQualifiedAddress(this.ourUuid, addr));
        return window.textsecure.storage.protocol.loadSessions(encodedAddresses, {
            zone: this.zone,
        });
    }
}
exports.Sessions = Sessions;
class IdentityKeys extends signal_client_1.IdentityKeyStore {
    constructor({ ourUuid, zone }) {
        super();
        this.ourUuid = ourUuid;
        this.zone = zone;
    }
    async getIdentityKey() {
        const keyPair = await window.textsecure.storage.protocol.getIdentityKeyPair(this.ourUuid);
        if (!keyPair) {
            throw new Error('IdentityKeyStore/getIdentityKey: No identity key!');
        }
        return signal_client_1.PrivateKey.deserialize(Buffer.from(keyPair.privKey));
    }
    async getLocalRegistrationId() {
        const id = await window.textsecure.storage.protocol.getLocalRegistrationId(this.ourUuid);
        if (!(0, lodash_1.isNumber)(id)) {
            throw new Error('IdentityKeyStore/getLocalRegistrationId: No registration id!');
        }
        return id;
    }
    async getIdentity(address) {
        const encodedAddress = encodeAddress(address);
        const key = await window.textsecure.storage.protocol.loadIdentityKey(encodedAddress.uuid);
        if (!key) {
            return null;
        }
        return signal_client_1.PublicKey.deserialize(Buffer.from(key));
    }
    async saveIdentity(name, key) {
        const encodedAddress = encodeAddress(name);
        const publicKey = key.serialize();
        // Pass `zone` to let `saveIdentity` archive sibling sessions when identity
        // key changes.
        return window.textsecure.storage.protocol.saveIdentity(encodedAddress, publicKey, false, { zone: this.zone });
    }
    async isTrustedIdentity(name, key, direction) {
        const encodedAddress = encodeAddress(name);
        const publicKey = key.serialize();
        return window.textsecure.storage.protocol.isTrustedIdentity(encodedAddress, publicKey, direction);
    }
}
exports.IdentityKeys = IdentityKeys;
class PreKeys extends signal_client_1.PreKeyStore {
    constructor({ ourUuid }) {
        super();
        this.ourUuid = ourUuid;
    }
    async savePreKey(id, record) {
        await window.textsecure.storage.protocol.storePreKey(this.ourUuid, id, (0, SignalProtocolStore_1.freezePreKey)(record));
    }
    async getPreKey(id) {
        const preKey = await window.textsecure.storage.protocol.loadPreKey(this.ourUuid, id);
        if (preKey === undefined) {
            throw new Error(`getPreKey: PreKey ${id} not found`);
        }
        return preKey;
    }
    async removePreKey(id) {
        await window.textsecure.storage.protocol.removePreKey(this.ourUuid, id);
    }
}
exports.PreKeys = PreKeys;
class SenderKeys extends signal_client_1.SenderKeyStore {
    constructor({ ourUuid }) {
        super();
        this.ourUuid = ourUuid;
    }
    async saveSenderKey(sender, distributionId, record) {
        const encodedAddress = toQualifiedAddress(this.ourUuid, sender);
        await window.textsecure.storage.protocol.saveSenderKey(encodedAddress, distributionId, record);
    }
    async getSenderKey(sender, distributionId) {
        const encodedAddress = toQualifiedAddress(this.ourUuid, sender);
        const senderKey = await window.textsecure.storage.protocol.getSenderKey(encodedAddress, distributionId);
        return senderKey || null;
    }
}
exports.SenderKeys = SenderKeys;
class SignedPreKeys extends signal_client_1.SignedPreKeyStore {
    constructor({ ourUuid }) {
        super();
        this.ourUuid = ourUuid;
    }
    async saveSignedPreKey(id, record) {
        await window.textsecure.storage.protocol.storeSignedPreKey(this.ourUuid, id, (0, SignalProtocolStore_1.freezeSignedPreKey)(record), true);
    }
    async getSignedPreKey(id) {
        const signedPreKey = await window.textsecure.storage.protocol.loadSignedPreKey(this.ourUuid, id);
        if (!signedPreKey) {
            throw new Error(`getSignedPreKey: SignedPreKey ${id} not found`);
        }
        return signedPreKey;
    }
}
exports.SignedPreKeys = SignedPreKeys;
