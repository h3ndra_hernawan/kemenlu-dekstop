"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.setLogAtLevel = exports.trace = exports.debug = exports.info = exports.warn = exports.error = exports.fatal = void 0;
const lodash_1 = require("lodash");
const Logging_1 = require("../types/Logging");
let logAtLevel = lodash_1.noop;
let hasInitialized = false;
const fatal = (...args) => logAtLevel(Logging_1.LogLevel.Fatal, ...args);
exports.fatal = fatal;
const error = (...args) => logAtLevel(Logging_1.LogLevel.Error, ...args);
exports.error = error;
const warn = (...args) => logAtLevel(Logging_1.LogLevel.Warn, ...args);
exports.warn = warn;
const info = (...args) => logAtLevel(Logging_1.LogLevel.Info, ...args);
exports.info = info;
const debug = (...args) => logAtLevel(Logging_1.LogLevel.Debug, ...args);
exports.debug = debug;
const trace = (...args) => logAtLevel(Logging_1.LogLevel.Trace, ...args);
exports.trace = trace;
/**
 * Sets the low-level logging interface. Should be called early in a process's life, and
 * can only be called once.
 */
function setLogAtLevel(log) {
    if (hasInitialized) {
        throw new Error('Logger has already been initialized');
    }
    logAtLevel = log;
    hasInitialized = true;
}
exports.setLogAtLevel = setLogAtLevel;
