"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.levelMaxLength = exports.cleanArgs = exports.getLogLevelString = exports.isLogEntry = exports.isFetchLogIpcData = exports.LogLevel = void 0;
const pino_1 = __importDefault(require("pino"));
const isRecord_1 = require("../util/isRecord");
const privacy_1 = require("../util/privacy");
const missingCaseError_1 = require("../util/missingCaseError");
const reallyJsonStringify_1 = require("../util/reallyJsonStringify");
const Logging_1 = require("../types/Logging");
Object.defineProperty(exports, "LogLevel", { enumerable: true, get: function () { return Logging_1.LogLevel; } });
// We don't use Zod here because it'd be slow parsing all of the log entries.
//   Unfortunately, Zod is a bit slow even with `z.array(z.unknown())`.
const isFetchLogIpcData = (data) => (0, isRecord_1.isRecord)(data) &&
    (0, isRecord_1.isRecord)(data.capabilities) &&
    (0, isRecord_1.isRecord)(data.remoteConfig) &&
    (0, isRecord_1.isRecord)(data.statistics) &&
    (0, isRecord_1.isRecord)(data.user) &&
    Array.isArray(data.logEntries);
exports.isFetchLogIpcData = isFetchLogIpcData;
// The code below is performance sensitive since it runs for > 100k log entries
// whenever we want to send the debug log. We can't use `zod` because it clones
// the data on successful parse and ruins the performance.
const isLogEntry = (data) => {
    if (!(0, isRecord_1.isRecord)(data)) {
        return false;
    }
    const { level, msg, time } = data;
    if (typeof level !== 'number') {
        return false;
    }
    if (!Logging_1.LogLevel[level]) {
        return false;
    }
    if (typeof msg !== 'string') {
        return false;
    }
    if (typeof time !== 'string') {
        return false;
    }
    return !Number.isNaN(new Date(time).getTime());
};
exports.isLogEntry = isLogEntry;
function getLogLevelString(value) {
    switch (value) {
        case Logging_1.LogLevel.Fatal:
            return 'fatal';
        case Logging_1.LogLevel.Error:
            return 'error';
        case Logging_1.LogLevel.Warn:
            return 'warn';
        case Logging_1.LogLevel.Info:
            return 'info';
        case Logging_1.LogLevel.Debug:
            return 'debug';
        case Logging_1.LogLevel.Trace:
            return 'trace';
        default:
            throw (0, missingCaseError_1.missingCaseError)(value);
    }
}
exports.getLogLevelString = getLogLevelString;
function cleanArgs(args) {
    return (0, privacy_1.redactAll)(args
        .map(item => typeof item === 'string' ? item : (0, reallyJsonStringify_1.reallyJsonStringify)(item))
        .join(' '));
}
exports.cleanArgs = cleanArgs;
// To make it easier to visually scan logs, we make all levels the same length
const levelFromName = (0, pino_1.default)().levels.values;
exports.levelMaxLength = Object.keys(levelFromName).reduce((maxLength, level) => Math.max(maxLength, level.length), 0);
