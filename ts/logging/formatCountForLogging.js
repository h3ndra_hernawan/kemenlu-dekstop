"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatCountForLogging = void 0;
const formatCountForLogging = (count) => {
    if (count === 0 || Number.isNaN(count)) {
        return String(count);
    }
    return `at least ${10 ** Math.floor(Math.log10(count))}`;
};
exports.formatCountForLogging = formatCountForLogging;
