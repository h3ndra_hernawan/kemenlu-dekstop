"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fetch = exports.upload = void 0;
const lodash_1 = require("lodash");
const os_1 = __importDefault(require("os"));
const electron_1 = require("electron");
const zod_1 = require("zod");
const form_data_1 = __importDefault(require("form-data"));
const zlib_1 = require("zlib");
const pify_1 = __importDefault(require("pify"));
const got_1 = __importDefault(require("got"));
const getUserAgent_1 = require("../util/getUserAgent");
const url_1 = require("../util/url");
const log = __importStar(require("./log"));
const reallyJsonStringify_1 = require("../util/reallyJsonStringify");
const shared_1 = require("./shared");
const privacy_1 = require("../util/privacy");
const environment_1 = require("../environment");
const BASE_URL = 'https://debuglogs.org';
const tokenBodySchema = zod_1.z
    .object({
    fields: zod_1.z.record(zod_1.z.unknown()),
    url: zod_1.z.string(),
})
    .nonstrict();
const parseTokenBody = (rawBody) => {
    const body = tokenBodySchema.parse(rawBody);
    const parsedUrl = (0, url_1.maybeParseUrl)(body.url);
    if (!parsedUrl) {
        throw new Error("Token body's URL was not a valid URL");
    }
    if (parsedUrl.protocol !== 'https:') {
        throw new Error("Token body's URL was not HTTPS");
    }
    return body;
};
const upload = async (content, appVersion) => {
    const headers = { 'User-Agent': (0, getUserAgent_1.getUserAgent)(appVersion) };
    const signedForm = await got_1.default.get(BASE_URL, { responseType: 'json', headers });
    const { fields, url } = parseTokenBody(signedForm.body);
    const uploadKey = `${fields.key}.gz`;
    const form = new form_data_1.default();
    // The API expects `key` to be the first field:
    form.append('key', uploadKey);
    Object.entries(fields)
        .filter(([key]) => key !== 'key')
        .forEach(([key, value]) => {
        form.append(key, value);
    });
    const contentBuffer = await (0, pify_1.default)(zlib_1.gzip)(Buffer.from(content, 'utf8'));
    const contentType = 'application/gzip';
    form.append('Content-Type', contentType);
    form.append('file', contentBuffer, {
        contentType,
        filename: `signal-desktop-debug-log-${appVersion}.txt.gz`,
    });
    log.info('Debug log upload starting...');
    try {
        const { statusCode, body } = await got_1.default.post(url, { headers, body: form });
        if (statusCode !== 204) {
            throw new Error(`Failed to upload to S3, got status ${statusCode}, body '${body}'`);
        }
    }
    catch (error) {
        const response = error.response;
        throw new Error(`Got threw on upload to S3, got status ${response === null || response === void 0 ? void 0 : response.statusCode}, body '${response === null || response === void 0 ? void 0 : response.body}'  `);
    }
    log.info('Debug log upload complete.');
    return `${BASE_URL}/${uploadKey}`;
};
exports.upload = upload;
// The mechanics of preparing a log for publish
const headerSectionTitle = (title) => `========= ${title} =========`;
const headerSection = (title, data) => {
    const sortedEntries = (0, lodash_1.sortBy)(Object.entries(data), ([key]) => key);
    return [
        headerSectionTitle(title),
        ...sortedEntries.map(([key, value]) => `${key}: ${(0, privacy_1.redactAll)(String(value))}`),
        '',
    ].join('\n');
};
const getHeader = ({ capabilities, remoteConfig, statistics, user, }, nodeVersion, appVersion) => [
    headerSection('System info', {
        Time: Date.now(),
        'User agent': window.navigator.userAgent,
        'Node version': nodeVersion,
        Environment: (0, environment_1.getEnvironment)(),
        'App version': appVersion,
        'OS version': os_1.default.version(),
    }),
    headerSection('User info', user),
    headerSection('Capabilities', capabilities),
    headerSection('Remote config', remoteConfig),
    headerSection('Statistics', statistics),
    headerSectionTitle('Logs'),
].join('\n');
const getLevel = (0, lodash_1.memoize)((level) => {
    const text = (0, shared_1.getLogLevelString)(level);
    return text.toUpperCase().padEnd(shared_1.levelMaxLength, ' ');
});
function formatLine(mightBeEntry) {
    const entry = (0, shared_1.isLogEntry)(mightBeEntry)
        ? mightBeEntry
        : {
            level: shared_1.LogLevel.Error,
            msg: `Invalid IPC data when fetching logs. Here's what we could recover: ${(0, reallyJsonStringify_1.reallyJsonStringify)(mightBeEntry)}`,
            time: new Date().toISOString(),
        };
    return `${getLevel(entry.level)} ${entry.time} ${entry.msg}`;
}
function fetch(nodeVersion, appVersion) {
    return new Promise(resolve => {
        electron_1.ipcRenderer.send('fetch-log');
        electron_1.ipcRenderer.on('fetched-log', (_event, data) => {
            let header;
            let body;
            if ((0, shared_1.isFetchLogIpcData)(data)) {
                const { logEntries } = data;
                header = getHeader(data, nodeVersion, appVersion);
                body = logEntries.map(formatLine).join('\n');
            }
            else {
                header = headerSectionTitle('Partial logs');
                const entry = {
                    level: shared_1.LogLevel.Error,
                    msg: 'Invalid IPC data when fetching logs; dropping all logs',
                    time: new Date().toISOString(),
                };
                body = formatLine(entry);
            }
            const result = `${header}\n${body}`;
            resolve(result);
        });
    });
}
exports.fetch = fetch;
