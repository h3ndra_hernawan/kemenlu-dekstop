"use strict";
// Copyright 2017-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialize = exports.beforeRestart = void 0;
/* eslint-env node */
/* eslint-disable no-console */
const electron_1 = require("electron");
const path = __importStar(require("path"));
const pino_1 = __importDefault(require("pino"));
const rotating_file_stream_1 = require("rotating-file-stream");
const signal_client_1 = require("@signalapp/signal-client");
const shared_1 = require("./shared");
const log = __importStar(require("./log"));
const environment_1 = require("../environment");
// Backwards-compatible logging, simple strings and no level (defaulted to INFO)
function now() {
    const date = new Date();
    return date.toJSON();
}
function consoleLog(...args) {
    logAtLevel(shared_1.LogLevel.Info, ...args);
}
if (window.console) {
    console._log = console.log;
    console.log = consoleLog;
}
let globalLogger;
let shouldRestart = false;
function beforeRestart() {
    shouldRestart = true;
}
exports.beforeRestart = beforeRestart;
function initialize() {
    if (globalLogger) {
        throw new Error('Already called initialize!');
    }
    const basePath = electron_1.ipcRenderer.sendSync('get-user-data-path');
    const logFile = path.join(basePath, 'logs', 'app.log');
    const stream = (0, rotating_file_stream_1.createStream)(logFile, {
        interval: '1d',
        rotate: 3,
    });
    const onClose = () => {
        globalLogger = undefined;
        if (shouldRestart) {
            initialize();
        }
    };
    stream.on('close', onClose);
    stream.on('error', onClose);
    globalLogger = (0, pino_1.default)({
        timestamp: pino_1.default.stdTimeFunctions.isoTime,
    }, stream);
}
exports.initialize = initialize;
// A modern logging interface for the browser
function logAtLevel(level, ...args) {
    if ((0, environment_1.getEnvironment)() !== environment_1.Environment.Production) {
        const prefix = (0, shared_1.getLogLevelString)(level)
            .toUpperCase()
            .padEnd(shared_1.levelMaxLength, ' ');
        console._log(prefix, now(), ...args);
    }
    const levelString = (0, shared_1.getLogLevelString)(level);
    const msg = (0, shared_1.cleanArgs)(args);
    if (!globalLogger) {
        throw new Error('Logger has not been initialized yet');
        return;
    }
    globalLogger[levelString](msg);
}
log.setLogAtLevel(logAtLevel);
window.SignalContext = window.SignalContext || {};
window.SignalContext.log = {
    fatal: log.fatal,
    error: log.error,
    warn: log.warn,
    info: log.info,
    debug: log.debug,
    trace: log.trace,
};
window.onerror = (_message, _script, _line, _col, error) => {
    const errorInfo = error && error.stack ? error.stack : JSON.stringify(error);
    log.error(`Top-level unhandled error: ${errorInfo}`);
};
window.addEventListener('unhandledrejection', rejectionEvent => {
    const error = rejectionEvent.reason;
    const errorString = error && error.stack ? error.stack : JSON.stringify(error);
    log.error(`Top-level unhandled promise rejection: ${errorString}`);
});
(0, signal_client_1.initLogger)(3 /* Info */, (level, target, file, line, message) => {
    let fileString = '';
    if (file && line) {
        fileString = ` ${file}:${line}`;
    }
    else if (file) {
        fileString = ` ${file}`;
    }
    const logString = `@signalapp/signal-client ${message} ${target}${fileString}`;
    if (level === 5 /* Trace */) {
        log.trace(logString);
    }
    else if (level === 4 /* Debug */) {
        log.debug(logString);
    }
    else if (level === 3 /* Info */) {
        log.info(logString);
    }
    else if (level === 2 /* Warn */) {
        log.warn(logString);
    }
    else if (level === 1 /* Error */) {
        log.error(logString);
    }
    else {
        log.error(`${logString} (unknown log level ${level})`);
    }
});
