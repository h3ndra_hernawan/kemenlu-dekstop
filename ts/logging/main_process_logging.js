"use strict";
// Copyright 2017-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fetchAdditionalLogData = exports.fetchLogs = exports.fetchLog = exports.eliminateOldEntries = exports.eliminateOutOfDateFiles = exports.isLineAfterDate = exports.initialize = void 0;
// NOTE: Temporarily allow `then` until we convert the entire file to `async` / `await`:
/* eslint-disable more/no-then */
/* eslint-disable no-console */
const path_1 = require("path");
const split2_1 = __importDefault(require("split2"));
const fs_1 = require("fs");
const electron_1 = require("electron");
const pino_multi_stream_1 = __importDefault(require("pino-multi-stream"));
const pino_1 = __importDefault(require("pino"));
const mkdirp = __importStar(require("mkdirp"));
const lodash_1 = require("lodash");
const firstline_1 = __importDefault(require("firstline"));
const read_last_lines_1 = require("read-last-lines");
const rimraf_1 = __importDefault(require("rimraf"));
const rotating_file_stream_1 = require("rotating-file-stream");
const log = __importStar(require("./log"));
const environment_1 = require("../environment");
const shared_1 = require("./shared");
const MAX_LOG_LINES = 1000000;
let globalLogger;
let shouldRestart = false;
const isRunningFromConsole = Boolean(process.stdout.isTTY) ||
    (0, environment_1.getEnvironment)() === environment_1.Environment.Test ||
    (0, environment_1.getEnvironment)() === environment_1.Environment.TestLib;
async function initialize(getMainWindow) {
    if (globalLogger) {
        throw new Error('Already called initialize!');
    }
    const basePath = electron_1.app.getPath('userData');
    const logPath = (0, path_1.join)(basePath, 'logs');
    mkdirp.sync(logPath);
    try {
        await cleanupLogs(logPath);
    }
    catch (error) {
        const errorString = `Failed to clean logs; deleting all. Error: ${error.stack}`;
        console.error(errorString);
        await deleteAllLogs(logPath);
        mkdirp.sync(logPath);
        // If we want this log entry to persist on disk, we need to wait until we've
        //   set up our logging infrastructure.
        setTimeout(() => {
            console.error(errorString);
        }, 500);
    }
    const logFile = (0, path_1.join)(logPath, 'main.log');
    const stream = (0, rotating_file_stream_1.createStream)(logFile, {
        interval: '1d',
        rotate: 3,
    });
    const onClose = () => {
        globalLogger = undefined;
        if (shouldRestart) {
            initialize(getMainWindow);
        }
    };
    stream.on('close', onClose);
    stream.on('error', onClose);
    const streams = [];
    streams.push({ stream });
    if (isRunningFromConsole) {
        streams.push({
            level: 'debug',
            stream: process.stdout,
        });
    }
    const logger = (0, pino_multi_stream_1.default)({
        streams,
        timestamp: pino_1.default.stdTimeFunctions.isoTime,
    });
    electron_1.ipcMain.on('fetch-log', async (event) => {
        const mainWindow = getMainWindow();
        if (!mainWindow) {
            logger.info('Logs were requested, but the main window is missing');
            return;
        }
        let data;
        try {
            const [logEntries, rest] = await Promise.all([
                fetchLogs(logPath),
                (0, exports.fetchAdditionalLogData)(mainWindow),
            ]);
            data = Object.assign({ logEntries }, rest);
        }
        catch (error) {
            logger.error(`Problem loading log data: ${error.stack}`);
            return;
        }
        try {
            event.sender.send('fetched-log', data);
        }
        catch (err) {
            // NOTE(evanhahn): We don't want to send a message to a window that's closed.
            //   I wanted to use `event.sender.isDestroyed()` but that seems to fail.
            //   Instead, we attempt the send and catch the failure as best we can.
            const hasUserClosedWindow = isProbablyObjectHasBeenDestroyedError(err);
            if (hasUserClosedWindow) {
                logger.info('Logs were requested, but it seems the window was closed');
            }
            else {
                logger.error('Problem replying with fetched logs', err instanceof Error && err.stack ? err.stack : err);
            }
        }
    });
    electron_1.ipcMain.on('delete-all-logs', async (event) => {
        // Restart logging when the streams will close
        shouldRestart = true;
        try {
            await deleteAllLogs(logPath);
        }
        catch (error) {
            logger.error(`Problem deleting all logs: ${error.stack}`);
        }
        event.sender.send('delete-all-logs-complete');
    });
    globalLogger = logger;
    return log;
}
exports.initialize = initialize;
async function deleteAllLogs(logPath) {
    return new Promise((resolve, reject) => {
        (0, rimraf_1.default)(logPath, {
            disableGlob: true,
        }, error => {
            if (error) {
                return reject(error);
            }
            return resolve();
        });
    });
}
async function cleanupLogs(logPath) {
    const now = new Date();
    const earliestDate = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() - 3));
    try {
        const remaining = await eliminateOutOfDateFiles(logPath, earliestDate);
        const files = (0, lodash_1.filter)(remaining, file => !file.start && file.end);
        if (!files.length) {
            return;
        }
        await eliminateOldEntries(files, earliestDate);
    }
    catch (error) {
        console.error('Error cleaning logs; deleting and starting over from scratch.', error.stack);
        // delete and re-create the log directory
        await deleteAllLogs(logPath);
        mkdirp.sync(logPath);
    }
}
// Exported for testing only.
function isLineAfterDate(line, date) {
    if (!line) {
        return false;
    }
    try {
        const data = JSON.parse(line);
        return new Date(data.time).getTime() > date.getTime();
    }
    catch (e) {
        console.log('error parsing log line', e.stack, line);
        return false;
    }
}
exports.isLineAfterDate = isLineAfterDate;
// Exported for testing only.
function eliminateOutOfDateFiles(logPath, date) {
    const files = (0, fs_1.readdirSync)(logPath);
    const paths = files.map(file => (0, path_1.join)(logPath, file));
    return Promise.all((0, lodash_1.map)(paths, target => Promise.all([(0, firstline_1.default)(target), (0, read_last_lines_1.read)(target, 2)]).then(results => {
        const start = results[0];
        const end = results[1].split('\n');
        const file = {
            path: target,
            start: isLineAfterDate(start, date),
            end: isLineAfterDate(end[end.length - 1], date) ||
                isLineAfterDate(end[end.length - 2], date),
        };
        if (!file.start && !file.end) {
            (0, fs_1.unlinkSync)(file.path);
        }
        return file;
    })));
}
exports.eliminateOutOfDateFiles = eliminateOutOfDateFiles;
// Exported for testing only.
async function eliminateOldEntries(files, date) {
    await Promise.all((0, lodash_1.map)(files, file => fetchLog(file.path).then(lines => {
        const recent = (0, lodash_1.filter)(lines, line => new Date(line.time) >= date);
        const text = (0, lodash_1.map)(recent, line => JSON.stringify(line)).join('\n');
        return (0, fs_1.writeFileSync)(file.path, `${text}\n`);
    })));
}
exports.eliminateOldEntries = eliminateOldEntries;
// Exported for testing only.
async function fetchLog(logFile) {
    var e_1, _a;
    const results = new Array();
    const rawStream = (0, fs_1.createReadStream)(logFile);
    const jsonStream = rawStream.pipe((0, split2_1.default)(line => {
        try {
            return JSON.parse(line);
        }
        catch (e) {
            return undefined;
        }
    }));
    // Propagate fs errors down to the json stream so that for loop below handles
    // them.
    rawStream.on('error', error => jsonStream.emit('error', error));
    try {
        for (var jsonStream_1 = __asyncValues(jsonStream), jsonStream_1_1; jsonStream_1_1 = await jsonStream_1.next(), !jsonStream_1_1.done;) {
            const line = jsonStream_1_1.value;
            const result = line && (0, lodash_1.pick)(line, ['level', 'time', 'msg']);
            if (!(0, shared_1.isLogEntry)(result)) {
                continue;
            }
            results.push(result);
            if (results.length > MAX_LOG_LINES) {
                results.shift();
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (jsonStream_1_1 && !jsonStream_1_1.done && (_a = jsonStream_1.return)) await _a.call(jsonStream_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return results;
}
exports.fetchLog = fetchLog;
// Exported for testing only.
function fetchLogs(logPath) {
    const files = (0, fs_1.readdirSync)(logPath);
    const paths = files.map(file => (0, path_1.join)(logPath, file));
    // creating a manual log entry for the final log result
    const fileListEntry = {
        level: shared_1.LogLevel.Info,
        time: new Date().toISOString(),
        msg: `Loaded this list of log files from logPath: ${files.join(', ')}`,
    };
    return Promise.all(paths.map(fetchLog)).then(results => {
        const data = (0, lodash_1.flatten)(results);
        data.push(fileListEntry);
        return (0, lodash_1.sortBy)(data, logEntry => logEntry.time);
    });
}
exports.fetchLogs = fetchLogs;
const fetchAdditionalLogData = (mainWindow) => new Promise(resolve => {
    mainWindow.webContents.send('additional-log-data-request');
    electron_1.ipcMain.once('additional-log-data-response', (_event, data) => {
        resolve(data);
    });
});
exports.fetchAdditionalLogData = fetchAdditionalLogData;
function logAtLevel(level, ...args) {
    if (globalLogger) {
        const levelString = (0, shared_1.getLogLevelString)(level);
        globalLogger[levelString]((0, shared_1.cleanArgs)(args));
    }
    else if (isRunningFromConsole && !process.stdout.destroyed) {
        console._log(...args);
    }
}
function isProbablyObjectHasBeenDestroyedError(err) {
    return err instanceof Error && err.message === 'Object has been destroyed';
}
// This blows up using mocha --watch, so we ensure it is run just once
if (!console._log) {
    log.setLogAtLevel(logAtLevel);
    console._log = console.log;
    console.log = log.info;
    console._error = console.error;
    console.error = log.error;
    console._warn = console.warn;
    console.warn = log.warn;
}
