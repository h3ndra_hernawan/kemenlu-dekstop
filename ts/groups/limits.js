"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getGroupSizeHardLimit = exports.getGroupSizeRecommendedLimit = void 0;
const lodash_1 = require("lodash");
const parseIntOrThrow_1 = require("../util/parseIntOrThrow");
const RemoteConfig_1 = require("../RemoteConfig");
function makeGetter(configKey) {
    return fallback => {
        try {
            return (0, parseIntOrThrow_1.parseIntOrThrow)((0, RemoteConfig_1.getValue)(configKey), `Failed to parse ${configKey} as an integer`);
        }
        catch (err) {
            if ((0, lodash_1.isNumber)(fallback)) {
                return fallback;
            }
            throw err;
        }
    };
}
exports.getGroupSizeRecommendedLimit = makeGetter('global.groupsv2.maxGroupSize');
exports.getGroupSizeHardLimit = makeGetter('global.groupsv2.groupSizeHardLimit');
