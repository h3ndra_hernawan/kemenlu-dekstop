"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.toggleSelectedContactForGroupAddition = exports.OneTimeModalState = void 0;
const lodash_1 = require("lodash");
var OneTimeModalState;
(function (OneTimeModalState) {
    OneTimeModalState[OneTimeModalState["NeverShown"] = 0] = "NeverShown";
    OneTimeModalState[OneTimeModalState["Showing"] = 1] = "Showing";
    OneTimeModalState[OneTimeModalState["Shown"] = 2] = "Shown";
})(OneTimeModalState = exports.OneTimeModalState || (exports.OneTimeModalState = {}));
function toggleSelectedContactForGroupAddition(conversationId, currentState) {
    const { maxGroupSize, maxRecommendedGroupSize, numberOfContactsAlreadyInGroup, selectedConversationIds: oldSelectedConversationIds, } = currentState;
    let { maximumGroupSizeModalState, recommendedGroupSizeModalState } = currentState;
    const selectedConversationIds = (0, lodash_1.without)(oldSelectedConversationIds, conversationId);
    const shouldAdd = selectedConversationIds.length === oldSelectedConversationIds.length;
    if (shouldAdd) {
        const newExpectedMemberCount = selectedConversationIds.length + numberOfContactsAlreadyInGroup + 1;
        if (newExpectedMemberCount <= maxGroupSize) {
            if (newExpectedMemberCount === maxGroupSize &&
                maximumGroupSizeModalState === OneTimeModalState.NeverShown) {
                maximumGroupSizeModalState = OneTimeModalState.Showing;
            }
            else if (newExpectedMemberCount >= maxRecommendedGroupSize &&
                recommendedGroupSizeModalState === OneTimeModalState.NeverShown) {
                recommendedGroupSizeModalState = OneTimeModalState.Showing;
            }
            selectedConversationIds.push(conversationId);
        }
    }
    return {
        selectedConversationIds,
        maximumGroupSizeModalState,
        recommendedGroupSizeModalState,
    };
}
exports.toggleSelectedContactForGroupAddition = toggleSelectedContactForGroupAddition;
