"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.joinViaLink = void 0;
const groups_1 = require("../groups");
const Bytes = __importStar(require("../Bytes"));
const longRunningTaskWrapper_1 = require("../util/longRunningTaskWrapper");
const whatTypeOfConversation_1 = require("../util/whatTypeOfConversation");
const explodePromise_1 = require("../util/explodePromise");
const protobuf_1 = require("../protobuf");
const log = __importStar(require("../logging/log"));
const showToast_1 = require("../util/showToast");
const ToastAlreadyGroupMember_1 = require("../components/ToastAlreadyGroupMember");
const ToastAlreadyRequestedToJoin_1 = require("../components/ToastAlreadyRequestedToJoin");
async function joinViaLink(hash) {
    let inviteLinkPassword;
    let masterKey;
    try {
        ({ inviteLinkPassword, masterKey } = (0, groups_1.parseGroupLink)(hash));
    }
    catch (error) {
        const errorString = error && error.stack ? error.stack : error;
        log.error(`joinViaLink: Failed to parse group link ${errorString}`);
        if (error && error.name === groups_1.LINK_VERSION_ERROR) {
            showErrorDialog(window.i18n('GroupV2--join--unknown-link-version'), window.i18n('GroupV2--join--unknown-link-version--title'));
        }
        else {
            showErrorDialog(window.i18n('GroupV2--join--invalid-link'), window.i18n('GroupV2--join--invalid-link--title'));
        }
        return;
    }
    const data = (0, groups_1.deriveGroupFields)(Bytes.fromBase64(masterKey));
    const id = Bytes.toBase64(data.id);
    const logId = `groupv2(${id})`;
    const secretParams = Bytes.toBase64(data.secretParams);
    const publicParams = Bytes.toBase64(data.publicParams);
    const existingConversation = window.ConversationController.get(id) ||
        window.ConversationController.getByDerivedGroupV2Id(id);
    const ourConversationId = window.ConversationController.getOurConversationIdOrThrow();
    if (existingConversation &&
        existingConversation.hasMember(ourConversationId)) {
        log.warn(`joinViaLink/${logId}: Already a member of group, opening conversation`);
        window.reduxActions.conversations.openConversationInternal({
            conversationId: existingConversation.id,
        });
        (0, showToast_1.showToast)(ToastAlreadyGroupMember_1.ToastAlreadyGroupMember);
        return;
    }
    let result;
    try {
        result = await (0, longRunningTaskWrapper_1.longRunningTaskWrapper)({
            name: 'getPreJoinGroupInfo',
            idForLogging: (0, groups_1.idForLogging)(id),
            // If an error happens here, we won't show a dialog. We'll rely on the catch a few
            //   lines below.
            suppressErrorDialog: true,
            task: () => (0, groups_1.getPreJoinGroupInfo)(inviteLinkPassword, masterKey),
        });
    }
    catch (error) {
        const errorString = error && error.stack ? error.stack : error;
        log.error(`joinViaLink/${logId}: Failed to fetch group info - ${errorString}`);
        showErrorDialog(error.code && error.code === 403
            ? window.i18n('GroupV2--join--link-revoked')
            : window.i18n('GroupV2--join--general-join-failure'), error.code && error.code === 403
            ? window.i18n('GroupV2--join--link-revoked--title')
            : window.i18n('GroupV2--join--general-join-failure--title'));
        return;
    }
    const ACCESS_ENUM = protobuf_1.SignalService.AccessControl.AccessRequired;
    if (result.addFromInviteLink !== ACCESS_ENUM.ADMINISTRATOR &&
        result.addFromInviteLink !== ACCESS_ENUM.ANY) {
        log.error(`joinViaLink/${logId}: addFromInviteLink value of ${result.addFromInviteLink} is invalid`);
        showErrorDialog(window.i18n('GroupV2--join--link-revoked'), window.i18n('GroupV2--join--link-revoked--title'));
        return;
    }
    let localAvatar = result.avatar ? { loading: true } : undefined;
    const memberCount = result.memberCount || 1;
    const approvalRequired = result.addFromInviteLink === ACCESS_ENUM.ADMINISTRATOR;
    const title = (0, groups_1.decryptGroupTitle)(result.title, secretParams) ||
        window.i18n('unknownGroup');
    const groupDescription = (0, groups_1.decryptGroupDescription)(result.descriptionBytes, secretParams);
    if (approvalRequired &&
        existingConversation &&
        existingConversation.isMemberAwaitingApproval(ourConversationId)) {
        log.warn(`joinViaLink/${logId}: Already awaiting approval, opening conversation`);
        window.reduxActions.conversations.openConversationInternal({
            conversationId: existingConversation.id,
        });
        (0, showToast_1.showToast)(ToastAlreadyRequestedToJoin_1.ToastAlreadyRequestedToJoin);
        return;
    }
    const getPreJoinConversation = () => {
        let avatar;
        if (!localAvatar) {
            avatar = undefined;
        }
        else if (localAvatar && localAvatar.loading) {
            avatar = {
                loading: true,
            };
        }
        else if (localAvatar && localAvatar.path) {
            avatar = {
                url: window.Signal.Migrations.getAbsoluteAttachmentPath(localAvatar.path),
            };
        }
        return {
            approvalRequired,
            avatar,
            groupDescription,
            memberCount,
            title,
        };
    };
    // Explode a promise so we know when this whole join process is complete
    const { promise, resolve, reject } = (0, explodePromise_1.explodePromise)();
    const closeDialog = async () => {
        try {
            if (groupV2InfoDialog) {
                groupV2InfoDialog.remove();
                groupV2InfoDialog = undefined;
            }
            window.reduxActions.conversations.setPreJoinConversation(undefined);
            if (localAvatar && localAvatar.path) {
                await window.Signal.Migrations.deleteAttachmentData(localAvatar.path);
            }
            resolve();
        }
        catch (error) {
            reject(error);
        }
    };
    const join = async () => {
        try {
            if (groupV2InfoDialog) {
                groupV2InfoDialog.remove();
                groupV2InfoDialog = undefined;
            }
            window.reduxActions.conversations.setPreJoinConversation(undefined);
            await (0, longRunningTaskWrapper_1.longRunningTaskWrapper)({
                name: 'joinViaLink',
                idForLogging: (0, groups_1.idForLogging)(id),
                // If an error happens here, we won't show a dialog. We'll rely on a top-level
                //   error dialog provided by the caller of this function.
                suppressErrorDialog: true,
                task: async () => {
                    let targetConversation = existingConversation ||
                        window.ConversationController.get(id) ||
                        window.ConversationController.getByDerivedGroupV2Id(id);
                    let tempConversation;
                    // Check again to ensure that we haven't already joined or requested to join
                    //   via some other process. If so, just open that conversation.
                    if (targetConversation &&
                        (targetConversation.hasMember(ourConversationId) ||
                            (approvalRequired &&
                                targetConversation.isMemberAwaitingApproval(ourConversationId)))) {
                        log.warn(`joinViaLink/${logId}: User is part of group on second check, opening conversation`);
                        window.reduxActions.conversations.openConversationInternal({
                            conversationId: targetConversation.id,
                        });
                        return;
                    }
                    try {
                        if (!targetConversation) {
                            // Note: we save this temp conversation in the database, so we'll need to
                            //   clean it up if something goes wrong
                            tempConversation = window.ConversationController.getOrCreate(id, 'group', {
                                // This will cause this conversation to be deleted at next startup
                                isTemporary: true,
                                groupVersion: 2,
                                masterKey,
                                secretParams,
                                publicParams,
                                left: true,
                                revision: result.version,
                                avatar: localAvatar && localAvatar.path && result.avatar
                                    ? {
                                        url: result.avatar,
                                        path: localAvatar.path,
                                    }
                                    : undefined,
                                groupInviteLinkPassword: inviteLinkPassword,
                                name: title,
                                temporaryMemberCount: memberCount,
                            });
                            targetConversation = tempConversation;
                        }
                        else {
                            // Ensure the group maintains the title and avatar you saw when attempting
                            //   to join it.
                            targetConversation.set({
                                avatar: localAvatar && localAvatar.path && result.avatar
                                    ? {
                                        url: result.avatar,
                                        path: localAvatar.path,
                                    }
                                    : undefined,
                                groupInviteLinkPassword: inviteLinkPassword,
                                name: title,
                                temporaryMemberCount: memberCount,
                            });
                            window.Signal.Data.updateConversation(targetConversation.attributes);
                        }
                        if ((0, whatTypeOfConversation_1.isGroupV1)(targetConversation.attributes)) {
                            await targetConversation.joinGroupV2ViaLinkAndMigrate({
                                approvalRequired,
                                inviteLinkPassword,
                                revision: result.version || 0,
                            });
                        }
                        else {
                            await targetConversation.joinGroupV2ViaLink({
                                inviteLinkPassword,
                                approvalRequired,
                            });
                        }
                        if (tempConversation) {
                            tempConversation.set({
                                // We want to keep this conversation around, since the join succeeded
                                isTemporary: undefined,
                            });
                            window.Signal.Data.updateConversation(tempConversation.attributes);
                        }
                        window.reduxActions.conversations.openConversationInternal({
                            conversationId: targetConversation.id,
                        });
                    }
                    catch (error) {
                        // Delete newly-created conversation if we encountered any errors
                        if (tempConversation) {
                            window.ConversationController.dangerouslyRemoveById(tempConversation.id);
                            await window.Signal.Data.removeConversation(tempConversation.id, {
                                Conversation: window.Whisper.Conversation,
                            });
                        }
                        throw error;
                    }
                },
            });
            resolve();
        }
        catch (error) {
            reject(error);
        }
    };
    // Initial add to redux, with basic group information
    window.reduxActions.conversations.setPreJoinConversation(getPreJoinConversation());
    log.info(`joinViaLink/${logId}: Showing modal`);
    let groupV2InfoDialog = new window.Whisper.ReactWrapperView({
        className: 'group-v2-join-dialog-wrapper',
        JSX: window.Signal.State.Roots.createGroupV2JoinModal(window.reduxStore, {
            join,
            onClose: closeDialog,
        }),
    });
    // We declare a new function here so we can await but not block
    const fetchAvatar = async () => {
        if (result.avatar) {
            localAvatar = {
                loading: true,
            };
            const attributes = {
                avatar: null,
                secretParams,
            };
            await (0, groups_1.applyNewAvatar)(result.avatar, attributes, logId);
            if (attributes.avatar && attributes.avatar.path) {
                localAvatar = {
                    path: attributes.avatar.path,
                };
                // Dialog has been dismissed; we'll delete the unneeeded avatar
                if (!groupV2InfoDialog) {
                    await window.Signal.Migrations.deleteAttachmentData(attributes.avatar.path);
                    return;
                }
            }
            else {
                localAvatar = undefined;
            }
            // Update join dialog with newly-downloaded avatar
            window.reduxActions.conversations.setPreJoinConversation(getPreJoinConversation());
        }
    };
    fetchAvatar();
    await promise;
}
exports.joinViaLink = joinViaLink;
function showErrorDialog(description, title) {
    const errorView = new window.Whisper.ReactWrapperView({
        className: 'error-modal-wrapper',
        Component: window.Signal.Components.ErrorModal,
        props: {
            title,
            description,
            onClose: () => {
                errorView.remove();
            },
        },
    });
}
