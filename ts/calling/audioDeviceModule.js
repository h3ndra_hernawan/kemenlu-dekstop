"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAudioDeviceModule = exports.parseAudioDeviceModule = exports.AudioDeviceModule = void 0;
const enum_1 = require("../util/enum");
const RemoteConfig_1 = require("../RemoteConfig");
const version_1 = require("../util/version");
const OS = __importStar(require("../OS"));
var AudioDeviceModule;
(function (AudioDeviceModule) {
    AudioDeviceModule["Default"] = "Default";
    AudioDeviceModule["WindowsAdm2"] = "WindowsAdm2";
})(AudioDeviceModule = exports.AudioDeviceModule || (exports.AudioDeviceModule = {}));
exports.parseAudioDeviceModule = (0, enum_1.makeEnumParser)(AudioDeviceModule, AudioDeviceModule.Default);
function getAudioDeviceModule() {
    if (!OS.isWindows()) {
        return AudioDeviceModule.Default;
    }
    const appVersion = window.getVersion();
    if ((0, RemoteConfig_1.isEnabled)('desktop.calling.useWindowsAdm2') ||
        (0, version_1.isBeta)(appVersion) ||
        (0, version_1.isAlpha)(appVersion)) {
        return AudioDeviceModule.WindowsAdm2;
    }
    return AudioDeviceModule.Default;
}
exports.getAudioDeviceModule = getAudioDeviceModule;
