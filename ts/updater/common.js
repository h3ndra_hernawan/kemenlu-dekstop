"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCliOptions = exports.deleteTempDir = exports.createTempDir = exports.parseYaml = exports.getUpdateFileName = exports.isUpdateFileNameValid = exports.getVersion = exports.getUpdatesFileName = exports.getProxyUrl = exports.getCertificateAuthority = exports.getUpdatesBase = exports.getUpdateCheckUrl = exports.validatePath = exports.Updater = exports.GOT_SOCKET_TIMEOUT = exports.GOT_LOOKUP_TIMEOUT = exports.GOT_CONNECT_TIMEOUT = void 0;
/* eslint-disable no-console */
const fs_1 = require("fs");
const path_1 = require("path");
const os_1 = require("os");
const lodash_1 = require("lodash");
const dashdash_1 = require("dashdash");
const proxy_agent_1 = __importDefault(require("proxy-agent"));
const js_yaml_1 = require("js-yaml");
const semver_1 = require("semver");
const config_1 = __importDefault(require("config"));
const got_1 = __importDefault(require("got"));
const uuid_1 = require("uuid");
const pify_1 = __importDefault(require("pify"));
const mkdirp_1 = __importDefault(require("mkdirp"));
const rimraf_1 = __importDefault(require("rimraf"));
const electron_1 = require("electron");
const durations = __importStar(require("../util/durations"));
const attachments_1 = require("../util/attachments");
const Dialogs_1 = require("../types/Dialogs");
const Errors = __importStar(require("../types/errors"));
const getUserAgent_1 = require("../util/getUserAgent");
const version_1 = require("../util/version");
const packageJson = __importStar(require("../../package.json"));
const signature_1 = require("./signature");
const isPathInside_1 = require("../util/isPathInside");
const writeFile = (0, pify_1.default)(fs_1.writeFile);
const mkdirpPromise = (0, pify_1.default)(mkdirp_1.default);
const rimrafPromise = (0, pify_1.default)(rimraf_1.default);
const { platform } = process;
exports.GOT_CONNECT_TIMEOUT = 2 * 60 * 1000;
exports.GOT_LOOKUP_TIMEOUT = 2 * 60 * 1000;
exports.GOT_SOCKET_TIMEOUT = 2 * 60 * 1000;
const INTERVAL = 30 * durations.MINUTE;
class Updater {
    constructor(logger, settingsChannel, getMainWindow) {
        this.logger = logger;
        this.settingsChannel = settingsChannel;
        this.getMainWindow = getMainWindow;
    }
    //
    // Public APIs
    //
    async force() {
        return this.checkForUpdatesMaybeInstall(true);
    }
    async start() {
        this.logger.info('updater/start: starting checks...');
        electron_1.app.once('quit', () => this.quitHandler());
        setInterval(async () => {
            try {
                await this.checkForUpdatesMaybeInstall();
            }
            catch (error) {
                this.logger.error(`updater/start: ${Errors.toLogFormat(error)}`);
            }
        }, INTERVAL);
        await this.deletePreviousInstallers();
        await this.checkForUpdatesMaybeInstall();
    }
    quitHandler() {
        if (this.updateFilePath) {
            this.deleteCache(this.updateFilePath);
        }
    }
    //
    // Protected methods
    //
    setUpdateListener(performUpdateCallback) {
        electron_1.ipcMain.removeAllListeners('start-update');
        electron_1.ipcMain.once('start-update', performUpdateCallback);
    }
    //
    // Private methods
    //
    async downloadAndInstall(newFileName, newVersion, updateOnProgress) {
        const { logger } = this;
        try {
            const oldFileName = this.fileName;
            const oldVersion = this.version;
            if (this.updateFilePath) {
                this.deleteCache(this.updateFilePath);
            }
            this.fileName = newFileName;
            this.version = newVersion;
            try {
                this.updateFilePath = await this.downloadUpdate(this.fileName, updateOnProgress);
            }
            catch (error) {
                // Restore state in case of download error
                this.fileName = oldFileName;
                this.version = oldVersion;
                throw error;
            }
            const publicKey = (0, signature_1.hexToBinary)(config_1.default.get('updatesPublicKey'));
            const verified = await (0, signature_1.verifySignature)(this.updateFilePath, this.version, publicKey);
            if (!verified) {
                // Note: We don't delete the cache here, because we don't want to continually
                //   re-download the broken release. We will download it only once per launch.
                throw new Error('Downloaded update did not pass signature verification ' +
                    `(version: '${this.version}'; fileName: '${this.fileName}')`);
            }
            await this.installUpdate(this.updateFilePath);
            const mainWindow = this.getMainWindow();
            if (mainWindow) {
                mainWindow.webContents.send('show-update-dialog', Dialogs_1.DialogType.Update, {
                    version: this.version,
                });
            }
            else {
                logger.warn('downloadAndInstall: no mainWindow, cannot show update dialog');
            }
        }
        catch (error) {
            logger.error(`downloadAndInstall: ${Errors.toLogFormat(error)}`);
        }
    }
    async checkForUpdatesMaybeInstall(force = false) {
        const { logger } = this;
        logger.info('checkForUpdatesMaybeInstall: checking for update...');
        const result = await this.checkForUpdates(force);
        if (!result) {
            return;
        }
        const { fileName: newFileName, version: newVersion } = result;
        if (force ||
            this.fileName !== newFileName ||
            !this.version ||
            (0, semver_1.gt)(newVersion, this.version)) {
            const autoDownloadUpdates = await this.getAutoDownloadUpdateSetting();
            if (!autoDownloadUpdates) {
                this.setUpdateListener(async () => {
                    logger.info('checkForUpdatesMaybeInstall: have not downloaded update, going to download');
                    await this.downloadAndInstall(newFileName, newVersion, true);
                });
                const mainWindow = this.getMainWindow();
                if (mainWindow) {
                    mainWindow.webContents.send('show-update-dialog', Dialogs_1.DialogType.DownloadReady, {
                        downloadSize: result.size,
                        version: result.version,
                    });
                }
                else {
                    logger.warn('checkForUpdatesMaybeInstall: no mainWindow, cannot show update dialog');
                }
                return;
            }
            await this.downloadAndInstall(newFileName, newVersion);
        }
    }
    async checkForUpdates(forceUpdate = false) {
        const yaml = await getUpdateYaml();
        const parsedYaml = parseYaml(yaml);
        const version = getVersion(parsedYaml);
        if (!version) {
            this.logger.warn('checkForUpdates: no version extracted from downloaded yaml');
            return null;
        }
        if (forceUpdate || isVersionNewer(version)) {
            this.logger.info(`checkForUpdates: found newer version ${version} ` +
                `forceUpdate=${forceUpdate}`);
            const fileName = getUpdateFileName(parsedYaml);
            return {
                fileName,
                size: getSize(parsedYaml, fileName),
                version,
            };
        }
        this.logger.info(`checkForUpdates: ${version} is not newer; no new update available`);
        return null;
    }
    async downloadUpdate(fileName, updateOnProgress) {
        const baseUrl = getUpdatesBase();
        const updateFileUrl = `${baseUrl}/${fileName}`;
        const signatureFileName = (0, signature_1.getSignatureFileName)(fileName);
        const signatureUrl = `${baseUrl}/${signatureFileName}`;
        let tempDir;
        try {
            tempDir = await createTempDir();
            const targetUpdatePath = (0, path_1.join)(tempDir, fileName);
            const targetSignaturePath = (0, path_1.join)(tempDir, (0, signature_1.getSignatureFileName)(fileName));
            validatePath(tempDir, targetUpdatePath);
            validatePath(tempDir, targetSignaturePath);
            this.logger.info(`downloadUpdate: Downloading signature ${signatureUrl}`);
            const { body } = await got_1.default.get(signatureUrl, getGotOptions());
            await writeFile(targetSignaturePath, body);
            this.logger.info(`downloadUpdate: Downloading update ${updateFileUrl}`);
            const downloadStream = got_1.default.stream(updateFileUrl, getGotOptions());
            const writeStream = (0, fs_1.createWriteStream)(targetUpdatePath);
            await new Promise((resolve, reject) => {
                const mainWindow = this.getMainWindow();
                if (updateOnProgress && mainWindow) {
                    let downloadedSize = 0;
                    const throttledSend = (0, lodash_1.throttle)(() => {
                        mainWindow.webContents.send('show-update-dialog', Dialogs_1.DialogType.Downloading, { downloadedSize });
                    }, 500);
                    downloadStream.on('data', data => {
                        downloadedSize += data.length;
                        throttledSend();
                    });
                }
                downloadStream.on('error', error => {
                    reject(error);
                });
                downloadStream.on('end', () => {
                    resolve();
                });
                writeStream.on('error', error => {
                    reject(error);
                });
                downloadStream.pipe(writeStream);
            });
            return targetUpdatePath;
        }
        catch (error) {
            if (tempDir) {
                await deleteTempDir(tempDir);
            }
            throw error;
        }
    }
    async getAutoDownloadUpdateSetting() {
        try {
            return await this.settingsChannel.getSettingFromMainWindow('autoDownloadUpdate');
        }
        catch (error) {
            this.logger.warn('getAutoDownloadUpdateSetting: Failed to fetch, returning false', Errors.toLogFormat(error));
            return false;
        }
    }
    async deleteCache(filePath) {
        if (!filePath) {
            return;
        }
        const tempDir = (0, path_1.dirname)(filePath);
        try {
            await deleteTempDir(tempDir);
        }
        catch (error) {
            this.logger.error(`quitHandler: ${Errors.toLogFormat(error)}`);
        }
    }
}
exports.Updater = Updater;
function validatePath(basePath, targetPath) {
    const normalized = (0, path_1.normalize)(targetPath);
    if (!(0, isPathInside_1.isPathInside)(normalized, basePath)) {
        throw new Error(`validatePath: Path ${normalized} is not under base path ${basePath}`);
    }
}
exports.validatePath = validatePath;
// Helper functions
function getUpdateCheckUrl() {
    return `${getUpdatesBase()}/${getUpdatesFileName()}`;
}
exports.getUpdateCheckUrl = getUpdateCheckUrl;
function getUpdatesBase() {
    return config_1.default.get('updatesUrl');
}
exports.getUpdatesBase = getUpdatesBase;
function getCertificateAuthority() {
    return config_1.default.get('certificateAuthority');
}
exports.getCertificateAuthority = getCertificateAuthority;
function getProxyUrl() {
    return process.env.HTTPS_PROXY || process.env.https_proxy;
}
exports.getProxyUrl = getProxyUrl;
function getUpdatesFileName() {
    const prefix = getChannel();
    if (platform === 'darwin') {
        return `${prefix}-mac.yml`;
    }
    return `${prefix}.yml`;
}
exports.getUpdatesFileName = getUpdatesFileName;
function getChannel() {
    const { version } = packageJson;
    if ((0, version_1.isAlpha)(version)) {
        return 'alpha';
    }
    if ((0, version_1.isBeta)(version)) {
        return 'beta';
    }
    return 'latest';
}
function isVersionNewer(newVersion) {
    const { version } = packageJson;
    return (0, semver_1.gt)(newVersion, version);
}
function getVersion(info) {
    return info && info.version;
}
exports.getVersion = getVersion;
const validFile = /^[A-Za-z0-9.-]+$/;
function isUpdateFileNameValid(name) {
    return validFile.test(name);
}
exports.isUpdateFileNameValid = isUpdateFileNameValid;
function getUpdateFileName(info) {
    if (!info || !info.path) {
        throw new Error('getUpdateFileName: No path present in YAML file');
    }
    const { path } = info;
    if (!isUpdateFileNameValid(path)) {
        throw new Error(`getUpdateFileName: Path '${path}' contains invalid characters`);
    }
    return path;
}
exports.getUpdateFileName = getUpdateFileName;
function getSize(info, fileName) {
    if (!info || !info.files) {
        throw new Error('getUpdateFileName: No files present in YAML file');
    }
    const foundFile = info.files.find(file => file.url === fileName);
    return Number(foundFile === null || foundFile === void 0 ? void 0 : foundFile.size) || 0;
}
function parseYaml(yaml) {
    return (0, js_yaml_1.safeLoad)(yaml, { schema: js_yaml_1.FAILSAFE_SCHEMA, json: true });
}
exports.parseYaml = parseYaml;
async function getUpdateYaml() {
    const targetUrl = getUpdateCheckUrl();
    const body = await (0, got_1.default)(targetUrl, getGotOptions()).text();
    if (!body) {
        throw new Error('Got unexpected response back from update check');
    }
    return body;
}
function getGotOptions() {
    const certificateAuthority = getCertificateAuthority();
    const proxyUrl = getProxyUrl();
    const agent = proxyUrl
        ? {
            http: new proxy_agent_1.default(proxyUrl),
            https: new proxy_agent_1.default(proxyUrl),
        }
        : undefined;
    return {
        agent,
        https: {
            certificateAuthority,
        },
        headers: {
            'Cache-Control': 'no-cache',
            'User-Agent': (0, getUserAgent_1.getUserAgent)(packageJson.version),
        },
        timeout: {
            connect: exports.GOT_CONNECT_TIMEOUT,
            lookup: exports.GOT_LOOKUP_TIMEOUT,
            // This timeout is reset whenever we get new data on the socket
            socket: exports.GOT_SOCKET_TIMEOUT,
        },
    };
}
function getBaseTempDir() {
    // We only use tmpdir() when this code is run outside of an Electron app (as in: tests)
    return electron_1.app ? (0, attachments_1.getTempPath)(electron_1.app.getPath('userData')) : (0, os_1.tmpdir)();
}
async function createTempDir() {
    const baseTempDir = getBaseTempDir();
    const uniqueName = (0, uuid_1.v4)();
    const targetDir = (0, path_1.join)(baseTempDir, uniqueName);
    await mkdirpPromise(targetDir);
    return targetDir;
}
exports.createTempDir = createTempDir;
async function deleteTempDir(targetDir) {
    const pathInfo = (0, fs_1.statSync)(targetDir);
    if (!pathInfo.isDirectory()) {
        throw new Error(`deleteTempDir: Cannot delete path '${targetDir}' because it is not a directory`);
    }
    const baseTempDir = getBaseTempDir();
    if (!(0, isPathInside_1.isPathInside)(targetDir, baseTempDir)) {
        throw new Error(`deleteTempDir: Cannot delete path '${targetDir}' since it is not within base temp dir`);
    }
    await rimrafPromise(targetDir);
}
exports.deleteTempDir = deleteTempDir;
function getCliOptions(options) {
    const parser = (0, dashdash_1.createParser)({ options });
    const cliOptions = parser.parse(process.argv);
    if (cliOptions.help) {
        const help = parser.help().trimRight();
        console.log(help);
        process.exit(0);
    }
    return cliOptions;
}
exports.getCliOptions = getCliOptions;
