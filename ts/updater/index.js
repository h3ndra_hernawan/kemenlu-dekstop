"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.force = exports.start = void 0;
const config_1 = __importDefault(require("config"));
const macos_1 = require("./macos");
const windows_1 = require("./windows");
let initialized = false;
let updater;
async function start(settingsChannel, logger, getMainWindow) {
    const { platform } = process;
    if (initialized) {
        throw new Error('updater/start: Updates have already been initialized!');
    }
    initialized = true;
    if (!logger) {
        throw new Error('updater/start: Must provide logger!');
    }
    if (autoUpdateDisabled()) {
        logger.info('updater/start: Updates disabled - not starting new version checks');
        return;
    }
    if (platform === 'win32') {
        updater = new windows_1.WindowsUpdater(logger, settingsChannel, getMainWindow);
    }
    else if (platform === 'darwin') {
        updater = new macos_1.MacOSUpdater(logger, settingsChannel, getMainWindow);
    }
    else {
        throw new Error('updater/start: Unsupported platform');
    }
    await updater.start();
}
exports.start = start;
async function force() {
    if (!initialized) {
        throw new Error("updater/force: Updates haven't been initialized!");
    }
    if (updater) {
        await updater.force();
    }
}
exports.force = force;
function autoUpdateDisabled() {
    return (process.platform === 'linux' || process.mas || !config_1.default.get('updatesEnabled'));
}
