"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-console */
const path_1 = require("path");
const fs_1 = require("fs");
const pify_1 = __importDefault(require("pify"));
const Errors = __importStar(require("../types/errors"));
const common_1 = require("./common");
const signature_1 = require("./signature");
const packageJson = __importStar(require("../../package.json"));
const readdir = (0, pify_1.default)(fs_1.readdir);
const OPTIONS = [
    {
        names: ['help', 'h'],
        type: 'bool',
        help: 'Print this help and exit.',
    },
    {
        names: ['private', 'p'],
        type: 'string',
        help: 'Path to private key file (default: ./private.key)',
        default: 'private.key',
    },
    {
        names: ['update', 'u'],
        type: 'string',
        help: 'Path to the update package (default: the .exe or .zip in ./release)',
    },
    {
        names: ['version', 'v'],
        type: 'string',
        help: `Version number of this package (default: ${packageJson.version})`,
        default: packageJson.version,
    },
];
const cliOptions = (0, common_1.getCliOptions)(OPTIONS);
go(cliOptions).catch(error => {
    console.error('Something went wrong!', Errors.toLogFormat(error));
});
async function go(options) {
    const { private: privateKeyPath, version } = options;
    let { update: updatePath } = options;
    if (!updatePath) {
        updatePath = await findUpdatePath();
    }
    console.log('Signing with...');
    console.log(`  version: ${version}`);
    console.log(`  update file: ${updatePath}`);
    console.log(`  private key file: ${privateKeyPath}`);
    await (0, signature_1.writeSignature)(updatePath, version, privateKeyPath);
}
const IS_EXE = /\.exe$/;
const IS_ZIP = /\.zip$/;
async function findUpdatePath() {
    const releaseDir = (0, path_1.resolve)('release');
    const files = await readdir(releaseDir);
    const max = files.length;
    for (let i = 0; i < max; i += 1) {
        const file = files[i];
        const fullPath = (0, path_1.join)(releaseDir, file);
        if (IS_EXE.test(file) || IS_ZIP.test(file)) {
            return fullPath;
        }
    }
    throw new Error("No suitable file found in 'release' folder!");
}
