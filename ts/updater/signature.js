"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.writeHexToPath = exports.loadHexFromPath = exports.binaryToHex = exports.hexToBinary = exports.getSignaturePath = exports.getSignatureFileName = exports._getFileHash = exports.writeSignature = exports.verifySignature = exports.generateSignature = void 0;
const crypto_1 = require("crypto");
const fs_1 = require("fs");
const path_1 = require("path");
const pify_1 = __importDefault(require("pify"));
const curve_1 = require("./curve");
const readFile = (0, pify_1.default)(fs_1.readFile);
const writeFile = (0, pify_1.default)(fs_1.writeFile);
async function generateSignature(updatePackagePath, version, privateKeyPath) {
    const privateKey = await loadHexFromPath(privateKeyPath);
    const message = await generateMessage(updatePackagePath, version);
    return (0, curve_1.sign)(privateKey, message);
}
exports.generateSignature = generateSignature;
async function verifySignature(updatePackagePath, version, publicKey) {
    const signaturePath = getSignaturePath(updatePackagePath);
    const signature = await loadHexFromPath(signaturePath);
    const message = await generateMessage(updatePackagePath, version);
    return (0, curve_1.verify)(publicKey, message, signature);
}
exports.verifySignature = verifySignature;
// Helper methods
async function generateMessage(updatePackagePath, version) {
    const hash = await _getFileHash(updatePackagePath);
    const messageString = `${Buffer.from(hash).toString('hex')}-${version}`;
    return Buffer.from(messageString);
}
async function writeSignature(updatePackagePath, version, privateKeyPath) {
    const signaturePath = getSignaturePath(updatePackagePath);
    const signature = await generateSignature(updatePackagePath, version, privateKeyPath);
    await writeHexToPath(signaturePath, signature);
}
exports.writeSignature = writeSignature;
async function _getFileHash(updatePackagePath) {
    const hash = (0, crypto_1.createHash)('sha256');
    const stream = (0, fs_1.createReadStream)(updatePackagePath);
    return new Promise((resolve, reject) => {
        stream.on('data', data => {
            hash.update(data);
        });
        stream.on('close', () => {
            resolve(hash.digest());
        });
        stream.on('error', error => {
            reject(error);
        });
    });
}
exports._getFileHash = _getFileHash;
function getSignatureFileName(fileName) {
    return `${fileName}.sig`;
}
exports.getSignatureFileName = getSignatureFileName;
function getSignaturePath(updatePackagePath) {
    const updateFullPath = (0, path_1.resolve)(updatePackagePath);
    const updateDir = (0, path_1.dirname)(updateFullPath);
    const updateFileName = (0, path_1.basename)(updateFullPath);
    return (0, path_1.join)(updateDir, getSignatureFileName(updateFileName));
}
exports.getSignaturePath = getSignaturePath;
function hexToBinary(target) {
    return Buffer.from(target, 'hex');
}
exports.hexToBinary = hexToBinary;
function binaryToHex(data) {
    return Buffer.from(data).toString('hex');
}
exports.binaryToHex = binaryToHex;
async function loadHexFromPath(target) {
    const hexString = await readFile(target, 'utf8');
    return hexToBinary(hexString);
}
exports.loadHexFromPath = loadHexFromPath;
async function writeHexToPath(target, data) {
    await writeFile(target, binaryToHex(data));
}
exports.writeHexToPath = writeHexToPath;
