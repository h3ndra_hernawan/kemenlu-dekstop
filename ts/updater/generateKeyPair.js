"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-console */
const Errors = __importStar(require("../types/errors"));
const common_1 = require("./common");
const curve_1 = require("./curve");
const signature_1 = require("./signature");
const OPTIONS = [
    {
        names: ['help', 'h'],
        type: 'bool',
        help: 'Print this help and exit.',
    },
    {
        names: ['key', 'k'],
        type: 'string',
        help: 'Path where public key will go',
        default: 'public.key',
    },
    {
        names: ['private', 'p'],
        type: 'string',
        help: 'Path where private key will go',
        default: 'private.key',
    },
];
const cliOptions = (0, common_1.getCliOptions)(OPTIONS);
go(cliOptions).catch(error => {
    console.error('Something went wrong!', Errors.toLogFormat(error));
});
async function go(options) {
    const { key: publicKeyPath, private: privateKeyPath } = options;
    const { publicKey, privateKey } = (0, curve_1.keyPair)();
    await Promise.all([
        (0, signature_1.writeHexToPath)(publicKeyPath, publicKey),
        (0, signature_1.writeHexToPath)(privateKeyPath, privateKey),
    ]);
}
