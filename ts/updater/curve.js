"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.verify = exports.sign = exports.keyPair = void 0;
const signal_client_1 = require("@signalapp/signal-client");
function keyPair() {
    const privKey = signal_client_1.PrivateKey.generate();
    const pubKey = privKey.getPublicKey();
    return {
        publicKey: pubKey.serialize(),
        privateKey: privKey.serialize(),
    };
}
exports.keyPair = keyPair;
function sign(privateKey, message) {
    const privKeyObj = signal_client_1.PrivateKey.deserialize(privateKey);
    const signature = privKeyObj.sign(message);
    return signature;
}
exports.sign = sign;
function verify(publicKey, message, signature) {
    const pubKeyObj = signal_client_1.PublicKey.deserialize(publicKey);
    const result = pubKeyObj.verify(message, signature);
    return result;
}
exports.verify = verify;
