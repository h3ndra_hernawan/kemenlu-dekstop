"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MacOSUpdater = void 0;
const fs_1 = require("fs");
const http_1 = require("http");
const uuid_1 = require("uuid");
const electron_1 = require("electron");
const got_1 = __importDefault(require("got"));
const common_1 = require("./common");
const explodePromise_1 = require("../util/explodePromise");
const Errors = __importStar(require("../types/errors"));
const window_state_1 = require("../../app/window_state");
const Dialogs_1 = require("../types/Dialogs");
class MacOSUpdater extends common_1.Updater {
    async deletePreviousInstallers() {
        // No installers are cache on macOS
    }
    async installUpdate(updateFilePath) {
        const { logger } = this;
        try {
            await this.handToAutoUpdate(updateFilePath);
        }
        catch (error) {
            const readOnly = 'Cannot update while running on a read-only volume';
            const message = error.message || '';
            const mainWindow = this.getMainWindow();
            if (mainWindow && message.includes(readOnly)) {
                logger.info('downloadAndInstall: showing read-only dialog...');
                mainWindow.webContents.send('show-update-dialog', Dialogs_1.DialogType.MacOS_Read_Only);
            }
            else if (mainWindow) {
                logger.info('downloadAndInstall: showing general update failure dialog...');
                mainWindow.webContents.send('show-update-dialog', Dialogs_1.DialogType.Cannot_Update);
            }
            else {
                logger.warn('downloadAndInstall: no mainWindow, cannot show update dialog');
            }
            throw error;
        }
        // At this point, closing the app will cause the update to be installed automatically
        //   because Squirrel has cached the update file and will do the right thing.
        logger.info('downloadAndInstall: showing update dialog...');
        this.setUpdateListener(() => {
            logger.info('performUpdate: calling quitAndInstall...');
            (0, window_state_1.markShouldQuit)();
            electron_1.autoUpdater.quitAndInstall();
        });
    }
    async handToAutoUpdate(filePath) {
        const { logger } = this;
        const { promise, resolve, reject } = (0, explodePromise_1.explodePromise)();
        const token = (0, uuid_1.v4)();
        const updateFileUrl = generateFileUrl();
        const server = (0, http_1.createServer)();
        let serverUrl;
        server.on('error', (error) => {
            logger.error(`handToAutoUpdate: ${Errors.toLogFormat(error)}`);
            this.shutdown(server);
            reject(error);
        });
        server.on('request', (request, response) => {
            const { url } = request;
            if (url === '/') {
                const absoluteUrl = `${serverUrl}${updateFileUrl}`;
                writeJSONResponse(absoluteUrl, response);
                return;
            }
            if (url === '/token') {
                writeTokenResponse(token, response);
                return;
            }
            if (!url || !url.startsWith(updateFileUrl)) {
                this.logger.error(`write404: Squirrel requested unexpected url '${url}'`);
                response.writeHead(404);
                response.end();
                return;
            }
            this.pipeUpdateToSquirrel(filePath, server, response, reject);
        });
        server.listen(0, '127.0.0.1', async () => {
            try {
                serverUrl = getServerUrl(server);
                electron_1.autoUpdater.on('error', (...args) => {
                    logger.error('autoUpdater: error', ...args.map(Errors.toLogFormat));
                    const [error] = args;
                    reject(error);
                });
                electron_1.autoUpdater.on('update-downloaded', () => {
                    logger.info('autoUpdater: update-downloaded event fired');
                    this.shutdown(server);
                    resolve();
                });
                const response = await got_1.default.get(`${serverUrl}/token`);
                if (JSON.parse(response.body).token !== token) {
                    throw new Error('autoUpdater: did not receive token back from updates server');
                }
                electron_1.autoUpdater.setFeedURL({
                    url: serverUrl,
                    headers: { 'Cache-Control': 'no-cache' },
                });
                electron_1.autoUpdater.checkForUpdates();
            }
            catch (error) {
                reject(error);
            }
        });
        return promise;
    }
    pipeUpdateToSquirrel(filePath, server, response, reject) {
        const { logger } = this;
        const updateFileSize = getFileSize(filePath);
        const readStream = (0, fs_1.createReadStream)(filePath);
        response.on('error', (error) => {
            logger.error(`pipeUpdateToSquirrel: update file download request had an error ${Errors.toLogFormat(error)}`);
            this.shutdown(server);
            reject(error);
        });
        readStream.on('error', (error) => {
            logger.error(`pipeUpdateToSquirrel: read stream error response: ${Errors.toLogFormat(error)}`);
            this.shutdown(server, response);
            reject(error);
        });
        response.writeHead(200, {
            'Content-Type': 'application/zip',
            'Content-Length': updateFileSize,
        });
        readStream.pipe(response);
    }
    shutdown(server, response) {
        const { logger } = this;
        try {
            if (server) {
                server.close();
            }
        }
        catch (error) {
            logger.error(`shutdown: Error closing server ${Errors.toLogFormat(error)}`);
        }
        try {
            if (response) {
                response.end();
            }
        }
        catch (endError) {
            logger.error(`shutdown: couldn't end response ${Errors.toLogFormat(endError)}`);
        }
    }
}
exports.MacOSUpdater = MacOSUpdater;
// Helpers
function writeJSONResponse(url, response) {
    const data = Buffer.from(JSON.stringify({
        url,
    }));
    response.writeHead(200, {
        'Content-Type': 'application/json',
        'Content-Length': data.byteLength,
    });
    response.end(data);
}
function writeTokenResponse(token, response) {
    const data = Buffer.from(JSON.stringify({
        token,
    }));
    response.writeHead(200, {
        'Content-Type': 'application/json',
        'Content-Length': data.byteLength,
    });
    response.end(data);
}
function getServerUrl(server) {
    const address = server.address();
    return `http://127.0.0.1:${address.port}`;
}
function generateFileUrl() {
    return `/${(0, uuid_1.v4)()}.zip`;
}
function getFileSize(targetPath) {
    const { size } = (0, fs_1.statSync)(targetPath);
    return size;
}
