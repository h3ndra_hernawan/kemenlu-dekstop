"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WindowsUpdater = void 0;
const path_1 = require("path");
const child_process_1 = require("child_process");
const fs_1 = require("fs");
const electron_1 = require("electron");
const pify_1 = __importDefault(require("pify"));
const common_1 = require("./common");
const window_state_1 = require("../../app/window_state");
const Dialogs_1 = require("../types/Dialogs");
const readdir = (0, pify_1.default)(fs_1.readdir);
const unlink = (0, pify_1.default)(fs_1.unlink);
const IS_EXE = /\.exe$/i;
class WindowsUpdater extends common_1.Updater {
    constructor() {
        super(...arguments);
        this.installing = false;
    }
    // This is fixed by our new install mechanisms...
    //   https://github.com/signalapp/Signal-Desktop/issues/2369
    // ...but we should also clean up those old installers.
    async deletePreviousInstallers() {
        const userDataPath = electron_1.app.getPath('userData');
        const files = await readdir(userDataPath);
        await Promise.all(files.map(async (file) => {
            const isExe = IS_EXE.test(file);
            if (!isExe) {
                return;
            }
            const fullPath = (0, path_1.join)(userDataPath, file);
            try {
                await unlink(fullPath);
            }
            catch (error) {
                this.logger.error(`deletePreviousInstallers: couldn't delete file ${file}`);
            }
        }));
    }
    async installUpdate(updateFilePath) {
        const { logger } = this;
        logger.info('downloadAndInstall: showing dialog...');
        this.setUpdateListener(async () => {
            try {
                await this.install(updateFilePath);
                this.installing = true;
            }
            catch (error) {
                const mainWindow = this.getMainWindow();
                if (mainWindow) {
                    logger.info('createUpdater: showing general update failure dialog...');
                    mainWindow.webContents.send('show-update-dialog', Dialogs_1.DialogType.Cannot_Update);
                }
                else {
                    logger.warn('createUpdater: no mainWindow, just failing over...');
                }
                throw error;
            }
            (0, window_state_1.markShouldQuit)();
            electron_1.app.quit();
        });
    }
    async install(filePath) {
        if (this.installing) {
            return;
        }
        const { logger } = this;
        logger.info('windows/install: installing package...');
        const args = ['--updated'];
        const options = {
            detached: true,
            stdio: 'ignore', // TypeScript considers this a plain string without help
        };
        try {
            await spawn(filePath, args, options);
        }
        catch (error) {
            if (error.code === 'UNKNOWN' || error.code === 'EACCES') {
                logger.warn('windows/install: Error running installer; Trying again with elevate.exe');
                await spawn(getElevatePath(), [filePath, ...args], options);
                return;
            }
            throw error;
        }
    }
}
exports.WindowsUpdater = WindowsUpdater;
// Helpers
function getElevatePath() {
    const installPath = electron_1.app.getAppPath();
    return (0, path_1.join)(installPath, 'resources', 'elevate.exe');
}
async function spawn(exe, args, options) {
    return new Promise((resolve, reject) => {
        const emitter = (0, child_process_1.spawn)(exe, args, options);
        emitter.on('error', reject);
        emitter.unref();
        setTimeout(resolve, 200);
    });
}
