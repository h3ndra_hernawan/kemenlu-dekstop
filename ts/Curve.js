"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.clampPrivateKey = exports.setPublicKeyTypeByte = exports.calculateSignature = exports.verifySignature = exports.calculateAgreement = exports.prefixPublicKey = exports.createKeyPair = exports.generateKeyPair = exports.generatePreKey = exports.generateSignedPreKey = exports.isNonNegativeInteger = void 0;
const client = __importStar(require("@signalapp/signal-client"));
const Bytes = __importStar(require("./Bytes"));
const Crypto_1 = require("./Crypto");
const log = __importStar(require("./logging/log"));
function isNonNegativeInteger(n) {
    return typeof n === 'number' && n % 1 === 0 && n >= 0;
}
exports.isNonNegativeInteger = isNonNegativeInteger;
function generateSignedPreKey(identityKeyPair, keyId) {
    if (!isNonNegativeInteger(keyId)) {
        throw new TypeError(`generateSignedPreKey: Invalid argument for keyId: ${keyId}`);
    }
    if (!(identityKeyPair.privKey instanceof Uint8Array) ||
        identityKeyPair.privKey.byteLength !== 32 ||
        !(identityKeyPair.pubKey instanceof Uint8Array) ||
        identityKeyPair.pubKey.byteLength !== 33) {
        throw new TypeError('generateSignedPreKey: Invalid argument for identityKeyPair');
    }
    const keyPair = generateKeyPair();
    const signature = calculateSignature(identityKeyPair.privKey, keyPair.pubKey);
    return {
        keyId,
        keyPair,
        signature,
    };
}
exports.generateSignedPreKey = generateSignedPreKey;
function generatePreKey(keyId) {
    if (!isNonNegativeInteger(keyId)) {
        throw new TypeError(`generatePreKey: Invalid argument for keyId: ${keyId}`);
    }
    const keyPair = generateKeyPair();
    return {
        keyId,
        keyPair,
    };
}
exports.generatePreKey = generatePreKey;
function generateKeyPair() {
    const privKey = client.PrivateKey.generate();
    const pubKey = privKey.getPublicKey();
    return {
        privKey: privKey.serialize(),
        pubKey: pubKey.serialize(),
    };
}
exports.generateKeyPair = generateKeyPair;
function createKeyPair(incomingKey) {
    const copy = new Uint8Array(incomingKey);
    clampPrivateKey(copy);
    if (!(0, Crypto_1.constantTimeEqual)(copy, incomingKey)) {
        log.warn('createKeyPair: incoming private key was not clamped!');
    }
    const incomingKeyBuffer = Buffer.from(incomingKey);
    if (incomingKeyBuffer.length !== 32) {
        throw new Error('key must be 32 bytes long');
    }
    const privKey = client.PrivateKey.deserialize(incomingKeyBuffer);
    const pubKey = privKey.getPublicKey();
    return {
        privKey: privKey.serialize(),
        pubKey: pubKey.serialize(),
    };
}
exports.createKeyPair = createKeyPair;
function prefixPublicKey(pubKey) {
    return Bytes.concatenate([
        new Uint8Array([0x05]),
        validatePubKeyFormat(pubKey),
    ]);
}
exports.prefixPublicKey = prefixPublicKey;
function calculateAgreement(pubKey, privKey) {
    const privKeyBuffer = Buffer.from(privKey);
    const pubKeyObj = client.PublicKey.deserialize(Buffer.from(prefixPublicKey(pubKey)));
    const privKeyObj = client.PrivateKey.deserialize(privKeyBuffer);
    const sharedSecret = privKeyObj.agree(pubKeyObj);
    return sharedSecret;
}
exports.calculateAgreement = calculateAgreement;
function verifySignature(pubKey, message, signature) {
    const pubKeyBuffer = Buffer.from(pubKey);
    const messageBuffer = Buffer.from(message);
    const signatureBuffer = Buffer.from(signature);
    const pubKeyObj = client.PublicKey.deserialize(pubKeyBuffer);
    const result = pubKeyObj.verify(messageBuffer, signatureBuffer);
    return result;
}
exports.verifySignature = verifySignature;
function calculateSignature(privKey, plaintext) {
    const privKeyBuffer = Buffer.from(privKey);
    const plaintextBuffer = Buffer.from(plaintext);
    const privKeyObj = client.PrivateKey.deserialize(privKeyBuffer);
    const signature = privKeyObj.sign(plaintextBuffer);
    return signature;
}
exports.calculateSignature = calculateSignature;
function validatePubKeyFormat(pubKey) {
    if (pubKey === undefined ||
        ((pubKey.byteLength !== 33 || pubKey[0] !== 5) && pubKey.byteLength !== 32)) {
        throw new Error('Invalid public key');
    }
    if (pubKey.byteLength === 33) {
        return pubKey.slice(1);
    }
    return pubKey;
}
function setPublicKeyTypeByte(publicKey) {
    // eslint-disable-next-line no-param-reassign
    publicKey[0] = 5;
}
exports.setPublicKeyTypeByte = setPublicKeyTypeByte;
function clampPrivateKey(privateKey) {
    // eslint-disable-next-line no-bitwise, no-param-reassign
    privateKey[0] &= 248;
    // eslint-disable-next-line no-bitwise, no-param-reassign
    privateKey[31] &= 127;
    // eslint-disable-next-line no-bitwise, no-param-reassign
    privateKey[31] |= 64;
}
exports.clampPrivateKey = clampPrivateKey;
