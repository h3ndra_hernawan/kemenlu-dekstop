"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseBadgeCategory = exports.BadgeCategory = void 0;
const enum_1 = require("../util/enum");
// The server may return "testing", which we should parse as "other".
var BadgeCategory;
(function (BadgeCategory) {
    BadgeCategory["Donor"] = "donor";
    BadgeCategory["Other"] = "other";
})(BadgeCategory = exports.BadgeCategory || (exports.BadgeCategory = {}));
exports.parseBadgeCategory = (0, enum_1.makeEnumParser)(BadgeCategory, BadgeCategory.Other);
