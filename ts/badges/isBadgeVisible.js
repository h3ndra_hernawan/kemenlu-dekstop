"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isBadgeVisible = void 0;
const isBadgeVisible = (badge) => 'isVisible' in badge ? badge.isVisible : true;
exports.isBadgeVisible = isBadgeVisible;
