"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBadgeImageFileLocalPath = void 0;
const lodash_1 = require("lodash");
function getBadgeImageFileLocalPath(badge, size, theme) {
    var _a;
    if (!badge) {
        return undefined;
    }
    const { images } = badge;
    // We expect this to be defined for valid input, but defend against unexpected array
    //   lengths.
    let idealImage;
    if (size < 24) {
        idealImage = (0, lodash_1.first)(images);
    }
    else if (size < 36) {
        idealImage = images[1] || (0, lodash_1.first)(images);
    }
    else if (size < 160) {
        idealImage = images[2] || (0, lodash_1.first)(images);
    }
    else {
        idealImage = (0, lodash_1.last)(images);
    }
    return (_a = idealImage === null || idealImage === void 0 ? void 0 : idealImage[theme]) === null || _a === void 0 ? void 0 : _a.localPath;
}
exports.getBadgeImageFileLocalPath = getBadgeImageFileLocalPath;
