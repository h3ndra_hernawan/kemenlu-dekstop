"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isBadgeImageFileUrlValid = void 0;
const url_1 = require("../util/url");
function isBadgeImageFileUrlValid(url, updatesUrl) {
    const expectedPrefix = new URL('/static/badges', updatesUrl).href;
    return url.startsWith(expectedPrefix) && Boolean((0, url_1.maybeParseUrl)(url));
}
exports.isBadgeImageFileUrlValid = isBadgeImageFileUrlValid;
