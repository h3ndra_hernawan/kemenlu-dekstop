"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.shouldShowBadges = void 0;
const RemoteConfig_1 = require("../RemoteConfig");
const environment_1 = require("../environment");
function shouldShowBadges() {
    return ((0, RemoteConfig_1.isEnabled)('desktop.showUserBadges') ||
        (0, RemoteConfig_1.isEnabled)('desktop.internalUser') ||
        (0, environment_1.getEnvironment)() === environment_1.Environment.Staging ||
        (0, environment_1.getEnvironment)() === environment_1.Environment.Development ||
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        Boolean(window.STORYBOOK_ENV));
}
exports.shouldShowBadges = shouldShowBadges;
