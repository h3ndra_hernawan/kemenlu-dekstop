"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseBadgeImageTheme = exports.BadgeImageTheme = void 0;
const enum_1 = require("../util/enum");
var BadgeImageTheme;
(function (BadgeImageTheme) {
    BadgeImageTheme["Light"] = "light";
    BadgeImageTheme["Dark"] = "dark";
    BadgeImageTheme["Transparent"] = "transparent";
})(BadgeImageTheme = exports.BadgeImageTheme || (exports.BadgeImageTheme = {}));
exports.parseBadgeImageTheme = (0, enum_1.makeEnumParser)(BadgeImageTheme, BadgeImageTheme.Transparent);
