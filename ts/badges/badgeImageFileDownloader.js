"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.badgeImageFileDownloader = void 0;
const p_queue_1 = __importDefault(require("p-queue"));
const log = __importStar(require("../logging/log"));
const durations_1 = require("../util/durations");
const missingCaseError_1 = require("../util/missingCaseError");
const waitForOnline_1 = require("../util/waitForOnline");
var BadgeDownloaderState;
(function (BadgeDownloaderState) {
    BadgeDownloaderState[BadgeDownloaderState["Idle"] = 0] = "Idle";
    BadgeDownloaderState[BadgeDownloaderState["Checking"] = 1] = "Checking";
    BadgeDownloaderState[BadgeDownloaderState["CheckingWithAnotherCheckEnqueued"] = 2] = "CheckingWithAnotherCheckEnqueued";
})(BadgeDownloaderState || (BadgeDownloaderState = {}));
class BadgeImageFileDownloader {
    constructor() {
        this.state = BadgeDownloaderState.Idle;
        this.queue = new p_queue_1.default({ concurrency: 3 });
    }
    async checkForFilesToDownload() {
        switch (this.state) {
            case BadgeDownloaderState.CheckingWithAnotherCheckEnqueued:
                log.info('BadgeDownloader#checkForFilesToDownload: not enqueuing another check');
                return;
            case BadgeDownloaderState.Checking:
                log.info('BadgeDownloader#checkForFilesToDownload: enqueuing another check');
                this.state = BadgeDownloaderState.CheckingWithAnotherCheckEnqueued;
                return;
            case BadgeDownloaderState.Idle: {
                this.state = BadgeDownloaderState.Checking;
                const urlsToDownload = getUrlsToDownload();
                log.info(`BadgeDownloader#checkForFilesToDownload: downloading ${urlsToDownload.length} badge(s)`);
                try {
                    await this.queue.addAll(urlsToDownload.map(url => () => downloadBadgeImageFile(url)));
                }
                catch (err) {
                    // Errors are ignored.
                }
                // Without this cast, TypeScript has an incorrect type for this value, assuming
                //   it's a constant when it could've changed. This is a [long-standing TypeScript
                //   issue][0].
                //
                // [0]: https://github.com/microsoft/TypeScript/issues/9998
                const previousState = this.state;
                this.state = BadgeDownloaderState.Idle;
                if (previousState ===
                    BadgeDownloaderState.CheckingWithAnotherCheckEnqueued) {
                    this.checkForFilesToDownload();
                }
                return;
            }
            default:
                throw (0, missingCaseError_1.missingCaseError)(this.state);
        }
    }
}
exports.badgeImageFileDownloader = new BadgeImageFileDownloader();
function getUrlsToDownload() {
    const result = [];
    const badges = Object.values(window.reduxStore.getState().badges.byId);
    for (const badge of badges) {
        for (const image of badge.images) {
            for (const imageFile of Object.values(image)) {
                if (!imageFile.localPath) {
                    result.push(imageFile.url);
                }
            }
        }
    }
    return result;
}
async function downloadBadgeImageFile(url) {
    await (0, waitForOnline_1.waitForOnline)(navigator, window, { timeout: 1 * durations_1.MINUTE });
    const imageFileData = await window.textsecure.server.getBadgeImageFile(url);
    const localPath = await window.Signal.Migrations.writeNewBadgeImageFileData(imageFileData);
    await window.Signal.Data.badgeImageFileDownloaded(url, localPath);
    window.reduxActions.badges.badgeImageFileDownloaded(url, localPath);
    return localPath;
}
