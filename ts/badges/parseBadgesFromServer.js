"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseBadgesFromServer = void 0;
const z = __importStar(require("zod"));
const lodash_1 = require("lodash");
const isRecord_1 = require("../util/isRecord");
const isNormalNumber_1 = require("../util/isNormalNumber");
const log = __importStar(require("../logging/log"));
const BadgeCategory_1 = require("./BadgeCategory");
const BadgeImageTheme_1 = require("./BadgeImageTheme");
const MAX_BADGES = 1000;
const badgeFromServerSchema = z.object({
    category: z.string(),
    description: z.string(),
    id: z.string(),
    name: z.string(),
    svg: z.string(),
    svgs: z.array(z.record(z.string())).length(3),
    expiration: z.number().optional(),
    visible: z.boolean().optional(),
});
function parseBadgesFromServer(value, updatesUrl) {
    if (!Array.isArray(value)) {
        return [];
    }
    const result = [];
    const numberOfBadgesToParse = Math.min(value.length, MAX_BADGES);
    for (let i = 0; i < numberOfBadgesToParse; i += 1) {
        const item = value[i];
        const parseResult = badgeFromServerSchema.safeParse(item);
        if (!parseResult.success) {
            log.warn('parseBadgesFromServer got an invalid item', parseResult.error.format());
            continue;
        }
        const { category, description: descriptionTemplate, expiration, id, name, svg, svgs, visible, } = parseResult.data;
        const images = parseImages(svgs, svg, updatesUrl);
        if (images.length !== 4) {
            log.warn('Got invalid number of SVGs from the server');
            continue;
        }
        result.push(Object.assign({ id, category: (0, BadgeCategory_1.parseBadgeCategory)(category), name,
            descriptionTemplate,
            images }, ((0, isNormalNumber_1.isNormalNumber)(expiration) && typeof visible === 'boolean'
            ? {
                expiresAt: expiration * 1000,
                isVisible: visible,
            }
            : {})));
    }
    return result;
}
exports.parseBadgesFromServer = parseBadgesFromServer;
const parseImages = (rawSvgs, rawSvg, updatesUrl) => {
    const result = [];
    for (const item of rawSvgs) {
        if (!(0, isRecord_1.isRecord)(item)) {
            log.warn('Got invalid SVG from the server');
            continue;
        }
        const image = {};
        for (const [rawTheme, filename] of Object.entries(item)) {
            if (typeof filename !== 'string') {
                log.warn('Got an SVG from the server that lacked a valid filename');
                continue;
            }
            const theme = (0, BadgeImageTheme_1.parseBadgeImageTheme)(rawTheme);
            image[theme] = { url: parseImageFilename(filename, updatesUrl) };
        }
        if ((0, lodash_1.isEmpty)(image)) {
            log.warn('Got an SVG from the server that lacked valid values');
        }
        else {
            result.push(image);
        }
    }
    result.push({
        [BadgeImageTheme_1.BadgeImageTheme.Transparent]: {
            url: parseImageFilename(rawSvg, updatesUrl),
        },
    });
    return result;
};
const parseImageFilename = (filename, updatesUrl) => new URL(`/static/badges/${filename}`, updatesUrl).toString();
