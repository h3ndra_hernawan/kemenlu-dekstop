"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMessageById = void 0;
const log = __importStar(require("../logging/log"));
const Errors = __importStar(require("../types/errors"));
async function getMessageById(messageId) {
    let message = window.MessageController.getById(messageId);
    if (message) {
        return message;
    }
    try {
        message = await window.Signal.Data.getMessageById(messageId, {
            Message: window.Whisper.Message,
        });
    }
    catch (err) {
        log.error(`failed to load message with id ${messageId} ` +
            `due to error ${Errors.toLogFormat(err)}`);
    }
    if (!message) {
        return undefined;
    }
    message = window.MessageController.register(message.id, message);
    return message;
}
exports.getMessageById = getMessageById;
