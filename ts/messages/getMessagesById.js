"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMessagesById = void 0;
const log = __importStar(require("../logging/log"));
const Errors = __importStar(require("../types/errors"));
async function getMessagesById(messageIds) {
    const messagesFromMemory = [];
    const messageIdsToLookUpInDatabase = [];
    messageIds.forEach(messageId => {
        const message = window.MessageController.getById(messageId);
        if (message) {
            messagesFromMemory.push(message);
        }
        else {
            messageIdsToLookUpInDatabase.push(messageId);
        }
    });
    let rawMessagesFromDatabase;
    try {
        rawMessagesFromDatabase = await window.Signal.Data.getMessagesById(messageIdsToLookUpInDatabase);
    }
    catch (err) {
        log.error(`failed to load ${messageIdsToLookUpInDatabase.length} message(s) from database. ${Errors.toLogFormat(err)}`);
        return [];
    }
    const messagesFromDatabase = rawMessagesFromDatabase.map(rawMessage => {
        // We use `window.Whisper.Message` instead of `MessageModel` here to avoid a circular
        //   import.
        const message = new window.Whisper.Message(rawMessage);
        return window.MessageController.register(message.id, message);
    });
    return [...messagesFromMemory, ...messagesFromDatabase];
}
exports.getMessagesById = getMessagesById;
