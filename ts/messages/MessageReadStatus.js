"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.maxReadStatus = exports.ReadStatus = void 0;
/**
 * `ReadStatus` represents your local read/viewed status of a single incoming message.
 * Messages go from Unread to Read to Viewed; they never go "backwards".
 *
 * Note that a conversation can be marked unread, which is not at the message level.
 *
 * Be careful when changing these values, as they are persisted. Notably, we previously
 * had a field called "unread", which is why Unread corresponds to 1 and Read to 0.
 */
var ReadStatus;
(function (ReadStatus) {
    ReadStatus[ReadStatus["Unread"] = 1] = "Unread";
    ReadStatus[ReadStatus["Read"] = 0] = "Read";
    ReadStatus[ReadStatus["Viewed"] = 2] = "Viewed";
})(ReadStatus = exports.ReadStatus || (exports.ReadStatus = {}));
const STATUS_NUMBERS = {
    [ReadStatus.Unread]: 0,
    [ReadStatus.Read]: 1,
    [ReadStatus.Viewed]: 2,
};
const maxReadStatus = (a, b) => STATUS_NUMBERS[a] > STATUS_NUMBERS[b] ? a : b;
exports.maxReadStatus = maxReadStatus;
