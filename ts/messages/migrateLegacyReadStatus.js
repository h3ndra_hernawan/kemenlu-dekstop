"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.migrateLegacyReadStatus = void 0;
const MessageReadStatus_1 = require("./MessageReadStatus");
function migrateLegacyReadStatus(message) {
    const shouldMigrate = message.readStatus == null;
    if (!shouldMigrate) {
        return;
    }
    const legacyUnread = message.unread;
    return legacyUnread ? MessageReadStatus_1.ReadStatus.Unread : MessageReadStatus_1.ReadStatus.Read;
}
exports.migrateLegacyReadStatus = migrateLegacyReadStatus;
