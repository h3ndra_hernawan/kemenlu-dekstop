"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isMessageJustForMe = exports.someSendStatus = exports.SendActionType = exports.sendStateReducer = exports.isFailed = exports.isSent = exports.isDelivered = exports.isRead = exports.isViewed = exports.maxStatus = exports.parseMessageSendStatus = exports.SendStatus = void 0;
const enum_1 = require("../util/enum");
/**
 * `SendStatus` represents the send status of a message to a single recipient. For
 * example, if a message is sent to 5 people, there would be 5 `SendStatus`es.
 *
 * Under normal conditions, the status will go down this list, in order:
 *
 * 1. `Pending`; the message has not been sent, and we are continuing to try
 * 2. `Sent`; the message has been delivered to the server
 * 3. `Delivered`; we've received a delivery receipt
 * 4. `Read`; we've received a read receipt (not applicable if the recipient has disabled
 *    sending these receipts)
 * 5. `Viewed`; we've received a viewed receipt (not applicable for all message types, or
 *    if the recipient has disabled sending these receipts)
 *
 * There's also a `Failed` state, which represents an error we don't want to recover from.
 *
 * There are some unusual cases where messages don't follow this pattern. For example, if
 * we receive a read receipt before we receive a delivery receipt, we might skip the
 * Delivered state. However, we should never go "backwards".
 *
 * Be careful when changing these values, as they are persisted.
 */
var SendStatus;
(function (SendStatus) {
    SendStatus["Failed"] = "Failed";
    SendStatus["Pending"] = "Pending";
    SendStatus["Sent"] = "Sent";
    SendStatus["Delivered"] = "Delivered";
    SendStatus["Read"] = "Read";
    SendStatus["Viewed"] = "Viewed";
})(SendStatus = exports.SendStatus || (exports.SendStatus = {}));
exports.parseMessageSendStatus = (0, enum_1.makeEnumParser)(SendStatus, SendStatus.Pending);
const STATUS_NUMBERS = {
    [SendStatus.Failed]: 0,
    [SendStatus.Pending]: 1,
    [SendStatus.Sent]: 2,
    [SendStatus.Delivered]: 3,
    [SendStatus.Read]: 4,
    [SendStatus.Viewed]: 5,
};
const maxStatus = (a, b) => STATUS_NUMBERS[a] > STATUS_NUMBERS[b] ? a : b;
exports.maxStatus = maxStatus;
const isViewed = (status) => status === SendStatus.Viewed;
exports.isViewed = isViewed;
const isRead = (status) => STATUS_NUMBERS[status] >= STATUS_NUMBERS[SendStatus.Read];
exports.isRead = isRead;
const isDelivered = (status) => STATUS_NUMBERS[status] >= STATUS_NUMBERS[SendStatus.Delivered];
exports.isDelivered = isDelivered;
const isSent = (status) => STATUS_NUMBERS[status] >= STATUS_NUMBERS[SendStatus.Sent];
exports.isSent = isSent;
const isFailed = (status) => status === SendStatus.Failed;
exports.isFailed = isFailed;
/**
 * The reducer advances the little `SendState` state machine. It mostly follows the steps
 * in the `SendStatus` documentation above, but it also handles edge cases.
 */
function sendStateReducer(state, action) {
    const oldStatus = state.status;
    let newStatus;
    if (oldStatus === SendStatus.Pending &&
        action.type === SendActionType.Failed) {
        newStatus = SendStatus.Failed;
    }
    else {
        newStatus = (0, exports.maxStatus)(oldStatus, STATE_TRANSITIONS[action.type]);
    }
    return newStatus === oldStatus
        ? state
        : {
            status: newStatus,
            updatedAt: action.updatedAt,
        };
}
exports.sendStateReducer = sendStateReducer;
var SendActionType;
(function (SendActionType) {
    SendActionType[SendActionType["Failed"] = 0] = "Failed";
    SendActionType[SendActionType["ManuallyRetried"] = 1] = "ManuallyRetried";
    SendActionType[SendActionType["Sent"] = 2] = "Sent";
    SendActionType[SendActionType["GotDeliveryReceipt"] = 3] = "GotDeliveryReceipt";
    SendActionType[SendActionType["GotReadReceipt"] = 4] = "GotReadReceipt";
    SendActionType[SendActionType["GotViewedReceipt"] = 5] = "GotViewedReceipt";
})(SendActionType = exports.SendActionType || (exports.SendActionType = {}));
const STATE_TRANSITIONS = {
    [SendActionType.Failed]: SendStatus.Failed,
    [SendActionType.ManuallyRetried]: SendStatus.Pending,
    [SendActionType.Sent]: SendStatus.Sent,
    [SendActionType.GotDeliveryReceipt]: SendStatus.Delivered,
    [SendActionType.GotReadReceipt]: SendStatus.Read,
    [SendActionType.GotViewedReceipt]: SendStatus.Viewed,
};
const someSendStatus = (sendStateByConversationId, predicate) => Object.values(sendStateByConversationId || {}).some(sendState => predicate(sendState.status));
exports.someSendStatus = someSendStatus;
const isMessageJustForMe = (sendStateByConversationId, ourConversationId) => {
    const conversationIds = Object.keys(sendStateByConversationId || {});
    return (conversationIds.length === 1 && conversationIds[0] === ourConversationId);
};
exports.isMessageJustForMe = isMessageJustForMe;
