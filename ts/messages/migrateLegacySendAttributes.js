"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.migrateLegacySendAttributes = void 0;
const lodash_1 = require("lodash");
const getOwn_1 = require("../util/getOwn");
const iterables_1 = require("../util/iterables");
const message_1 = require("../state/selectors/message");
const MessageSendState_1 = require("./MessageSendState");
/**
 * This converts legacy message fields, such as `sent_to`, into the new
 * `sendStateByConversationId` format. These legacy fields aren't typed to prevent their
 * usage, so we treat them carefully (i.e., as if they are `unknown`).
 *
 * Old data isn't dropped, in case we need to revert this change. We should safely be able
 * to remove the following attributes once we're confident in this new format:
 *
 * - delivered
 * - delivered_to
 * - read_by
 * - recipients
 * - sent
 * - sent_to
 */
function migrateLegacySendAttributes(message, getConversation, ourConversationId) {
    const shouldMigrate = (0, lodash_1.isEmpty)(message.sendStateByConversationId) && (0, message_1.isOutgoing)(message);
    if (!shouldMigrate) {
        return undefined;
    }
    const pendingSendState = {
        status: MessageSendState_1.SendStatus.Pending,
        updatedAt: message.sent_at,
    };
    const sendStateByConversationId = (0, iterables_1.zipObject)(getConversationIdsFromLegacyAttribute(message, 'recipients', getConversation), (0, iterables_1.repeat)(pendingSendState));
    // We use `get` because `sent` is a legacy, and therefore untyped, attribute.
    const wasSentToSelf = Boolean((0, lodash_1.get)(message, 'sent'));
    const actions = (0, iterables_1.concat)((0, iterables_1.map)(getConversationIdsFromErrors(message.errors, getConversation), conversationId => ({
        type: MessageSendState_1.SendActionType.Failed,
        conversationId,
    })), (0, iterables_1.map)(getConversationIdsFromLegacyAttribute(message, 'sent_to', getConversation), conversationId => ({
        type: MessageSendState_1.SendActionType.Sent,
        conversationId,
    })), (0, iterables_1.map)(getConversationIdsFromLegacyAttribute(message, 'delivered_to', getConversation), conversationId => ({
        type: MessageSendState_1.SendActionType.GotDeliveryReceipt,
        conversationId,
    })), (0, iterables_1.map)(getConversationIdsFromLegacyAttribute(message, 'read_by', getConversation), conversationId => ({
        type: MessageSendState_1.SendActionType.GotReadReceipt,
        conversationId,
    })), [
        {
            type: wasSentToSelf ? MessageSendState_1.SendActionType.Sent : MessageSendState_1.SendActionType.Failed,
            conversationId: ourConversationId,
        },
    ]);
    for (const { conversationId, type } of actions) {
        const oldSendState = (0, getOwn_1.getOwn)(sendStateByConversationId, conversationId) || pendingSendState;
        sendStateByConversationId[conversationId] = (0, MessageSendState_1.sendStateReducer)(oldSendState, {
            type,
            updatedAt: undefined,
        });
    }
    return sendStateByConversationId;
}
exports.migrateLegacySendAttributes = migrateLegacySendAttributes;
function getConversationIdsFromErrors(errors, getConversation) {
    const result = [];
    (errors || []).forEach(error => {
        const conversation = getConversation(error.identifier) || getConversation(error.number);
        if (conversation) {
            result.push(conversation.id);
        }
    });
    return result;
}
function getConversationIdsFromLegacyAttribute(message, attributeName, getConversation) {
    const rawValue = message[attributeName];
    const value = Array.isArray(rawValue) ? rawValue : [];
    const result = [];
    value.forEach(identifier => {
        if (typeof identifier !== 'string') {
            return;
        }
        const conversation = getConversation(identifier);
        if (conversation) {
            result.push(conversation.id);
        }
    });
    return result;
}
