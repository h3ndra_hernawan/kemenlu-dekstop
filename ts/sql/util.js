"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableIterator = exports.getCountFromTable = exports.getAllFromTable = exports.removeAllFromTable = exports.removeById = exports.getById = exports.bulkAdd = exports.createOrUpdate = exports.batchMultiVarQuery = exports.getSQLCipherVersion = exports.getUserVersion = exports.setUserVersion = exports.getSchemaVersion = exports.getSQLiteVersion = exports.jsonToObject = exports.objectToJSON = void 0;
const lodash_1 = require("lodash");
// This value needs to be below SQLITE_MAX_VARIABLE_NUMBER.
const MAX_VARIABLE_COUNT = 100;
function objectToJSON(data) {
    return JSON.stringify(data);
}
exports.objectToJSON = objectToJSON;
function jsonToObject(json) {
    return JSON.parse(json);
}
exports.jsonToObject = jsonToObject;
//
// Database helpers
//
function getSQLiteVersion(db) {
    const { sqlite_version: version } = db
        .prepare('select sqlite_version() AS sqlite_version')
        .get();
    return version;
}
exports.getSQLiteVersion = getSQLiteVersion;
function getSchemaVersion(db) {
    return db.pragma('schema_version', { simple: true });
}
exports.getSchemaVersion = getSchemaVersion;
function setUserVersion(db, version) {
    if (!(0, lodash_1.isNumber)(version)) {
        throw new Error(`setUserVersion: version ${version} is not a number`);
    }
    db.pragma(`user_version = ${version}`);
}
exports.setUserVersion = setUserVersion;
function getUserVersion(db) {
    return db.pragma('user_version', { simple: true });
}
exports.getUserVersion = getUserVersion;
function getSQLCipherVersion(db) {
    return db.pragma('cipher_version', { simple: true });
}
exports.getSQLCipherVersion = getSQLCipherVersion;
function batchMultiVarQuery(db, values, query) {
    if (values.length > MAX_VARIABLE_COUNT) {
        const result = [];
        db.transaction(() => {
            for (let i = 0; i < values.length; i += MAX_VARIABLE_COUNT) {
                const batch = values.slice(i, i + MAX_VARIABLE_COUNT);
                const batchResult = query(batch);
                if (Array.isArray(batchResult)) {
                    result.push(...batchResult);
                }
            }
        })();
        return result;
    }
    const result = query(values);
    return Array.isArray(result) ? result : [];
}
exports.batchMultiVarQuery = batchMultiVarQuery;
function createOrUpdate(db, table, data) {
    const { id } = data;
    if (!id) {
        throw new Error('createOrUpdate: Provided data did not have a truthy id');
    }
    db.prepare(`
    INSERT OR REPLACE INTO ${table} (
      id,
      json
    ) values (
      $id,
      $json
    )
    `).run({
        id,
        json: objectToJSON(data),
    });
}
exports.createOrUpdate = createOrUpdate;
function bulkAdd(db, table, array) {
    db.transaction(() => {
        for (const data of array) {
            createOrUpdate(db, table, data);
        }
    })();
}
exports.bulkAdd = bulkAdd;
function getById(db, table, id) {
    const row = db
        .prepare(`
      SELECT *
      FROM ${table}
      WHERE id = $id;
      `)
        .get({
        id,
    });
    if (!row) {
        return undefined;
    }
    return jsonToObject(row.json);
}
exports.getById = getById;
function removeById(db, table, id) {
    if (!Array.isArray(id)) {
        db.prepare(`
      DELETE FROM ${table}
      WHERE id = $id;
      `).run({ id });
        return;
    }
    if (!id.length) {
        throw new Error('removeById: No ids to delete!');
    }
    const removeByIdsSync = (ids) => {
        db.prepare(`
      DELETE FROM ${table}
      WHERE id IN ( ${id.map(() => '?').join(', ')} );
      `).run(ids);
    };
    batchMultiVarQuery(db, id, removeByIdsSync);
}
exports.removeById = removeById;
function removeAllFromTable(db, table) {
    db.prepare(`DELETE FROM ${table};`).run();
}
exports.removeAllFromTable = removeAllFromTable;
function getAllFromTable(db, table) {
    const rows = db
        .prepare(`SELECT json FROM ${table};`)
        .all();
    return rows.map(row => jsonToObject(row.json));
}
exports.getAllFromTable = getAllFromTable;
function getCountFromTable(db, table) {
    const result = db
        .prepare(`SELECT count(*) from ${table};`)
        .pluck(true)
        .get();
    if ((0, lodash_1.isNumber)(result)) {
        return result;
    }
    throw new Error(`getCountFromTable: Unable to get count from table ${table}`);
}
exports.getCountFromTable = getCountFromTable;
class TableIterator {
    constructor(db, table, pageSize = 500) {
        this.db = db;
        this.table = table;
        this.pageSize = pageSize;
    }
    *[Symbol.iterator]() {
        const fetchObject = this.db.prepare(`
        SELECT json FROM ${this.table}
        WHERE id > $id
        ORDER BY id ASC
        LIMIT $pageSize;
      `);
        let complete = false;
        let id = '';
        while (!complete) {
            const rows = fetchObject.all({
                id,
                pageSize: this.pageSize,
            });
            const messages = rows.map(row => jsonToObject(row.json));
            yield* messages;
            const lastMessage = (0, lodash_1.last)(messages);
            if (lastMessage) {
                ({ id } = lastMessage);
            }
            complete = messages.length < this.pageSize;
        }
    }
}
exports.TableIterator = TableIterator;
