"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const isNotNil_1 = require("../../util/isNotNil");
const assert_1 = require("../../util/assert");
const util_1 = require("../util");
function updateToSchemaVersion43(currentVersion, db, logger) {
    if (currentVersion >= 43) {
        return;
    }
    const getConversationUuid = db
        .prepare(`
      SELECT uuid
      FROM
        conversations
      WHERE
        id = $conversationId
      `)
        .pluck();
    const updateConversationStmt = db.prepare(`
    UPDATE conversations SET
      json = $json,
      members = $members
    WHERE id = $id;
    `);
    const updateMessageStmt = db.prepare(`
    UPDATE messages SET
      json = $json,
      sourceUuid = $sourceUuid
    WHERE id = $id;
    `);
    const upgradeConversation = (convo) => {
        const legacy = convo;
        let result = convo;
        const logId = `(${legacy.id}) groupv2(${legacy.groupId})`;
        const memberKeys = [
            'membersV2',
            'pendingMembersV2',
            'pendingAdminApprovalV2',
        ];
        for (const key of memberKeys) {
            const oldValue = legacy[key];
            if (!Array.isArray(oldValue)) {
                continue;
            }
            let addedByCount = 0;
            const newValue = oldValue
                .map(member => {
                const uuid = getConversationUuid.get({
                    conversationId: member.conversationId,
                });
                if (!uuid) {
                    logger.warn(`updateToSchemaVersion43: ${logId}.${key} UUID not found ` +
                        `for ${member.conversationId}`);
                    return undefined;
                }
                const updated = Object.assign(Object.assign({}, (0, lodash_1.omit)(member, 'conversationId')), { uuid });
                // We previously stored our conversation
                if (!('addedByUserId' in member) || !member.addedByUserId) {
                    return updated;
                }
                const addedByUserId = getConversationUuid.get({
                    conversationId: member.addedByUserId,
                });
                if (!addedByUserId) {
                    return updated;
                }
                addedByCount += 1;
                return Object.assign(Object.assign({}, updated), { addedByUserId });
            })
                .filter(isNotNil_1.isNotNil);
            result = Object.assign(Object.assign({}, result), { [key]: newValue });
            if (oldValue.length !== 0) {
                logger.info(`updateToSchemaVersion43: migrated ${oldValue.length} ${key} ` +
                    `entries to ${newValue.length} for ${logId}`);
            }
            if (addedByCount > 0) {
                logger.info(`updateToSchemaVersion43: migrated ${addedByCount} addedByUserId ` +
                    `in ${key} for ${logId}`);
            }
        }
        if (result === convo) {
            return;
        }
        let dbMembers;
        if (result.membersV2) {
            dbMembers = result.membersV2.map(item => item.uuid).join(' ');
        }
        else if (result.members) {
            dbMembers = result.members.join(' ');
        }
        else {
            dbMembers = null;
        }
        updateConversationStmt.run({
            id: result.id,
            json: (0, util_1.objectToJSON)(result),
            members: dbMembers,
        });
    };
    const upgradeMessage = (message) => {
        var _a;
        const { id, groupV2Change, sourceUuid, invitedGV2Members } = message;
        let result = message;
        if (groupV2Change) {
            (0, assert_1.assert)(result.groupV2Change, 'Pacify typescript');
            const from = getConversationUuid.get({
                conversationId: groupV2Change.from,
            });
            if (from) {
                result = Object.assign(Object.assign({}, result), { groupV2Change: Object.assign(Object.assign({}, result.groupV2Change), { from }) });
            }
            else {
                result = Object.assign(Object.assign({}, result), { groupV2Change: (0, lodash_1.omit)(result.groupV2Change, ['from']) });
            }
            let changedDetails = false;
            const details = groupV2Change.details
                .map((legacyDetail, i) => {
                var _a;
                const oldDetail = (_a = result.groupV2Change) === null || _a === void 0 ? void 0 : _a.details[i];
                (0, assert_1.assert)(oldDetail, 'Pacify typescript');
                let newDetail = oldDetail;
                for (const key of ['conversationId', 'inviter']) {
                    const oldValue = legacyDetail[key];
                    const newKey = key === 'conversationId' ? 'uuid' : key;
                    if (oldValue === undefined) {
                        continue;
                    }
                    changedDetails = true;
                    const newValue = getConversationUuid.get({
                        conversationId: oldValue,
                    });
                    if (key === 'inviter' && !newValue) {
                        continue;
                    }
                    if (!newValue) {
                        logger.warn(`updateToSchemaVersion43: ${id}.groupV2Change.details.${key} ` +
                            `UUID not found for ${oldValue}`);
                        return undefined;
                    }
                    (0, assert_1.assert)(newDetail.type === legacyDetail.type, 'Pacify typescript');
                    newDetail = Object.assign(Object.assign({}, (0, lodash_1.omit)(newDetail, key)), { [newKey]: newValue });
                }
                return newDetail;
            })
                .filter(isNotNil_1.isNotNil);
            if (changedDetails) {
                result = Object.assign(Object.assign({}, result), { groupV2Change: Object.assign(Object.assign({}, result.groupV2Change), { details }) });
            }
        }
        if (sourceUuid) {
            const newValue = getConversationUuid.get({
                conversationId: sourceUuid,
            });
            if (newValue) {
                result = Object.assign(Object.assign({}, result), { sourceUuid: newValue });
            }
        }
        if (invitedGV2Members) {
            const newMembers = invitedGV2Members
                .map(({ addedByUserId, conversationId }, i) => {
                const uuid = getConversationUuid.get({
                    conversationId,
                });
                const oldMember = result.invitedGV2Members && result.invitedGV2Members[i];
                (0, assert_1.assert)(oldMember !== undefined, 'Pacify typescript');
                if (!uuid) {
                    logger.warn(`updateToSchemaVersion43: ${id}.invitedGV2Members UUID ` +
                        `not found for ${conversationId}`);
                    return undefined;
                }
                const newMember = Object.assign(Object.assign({}, (0, lodash_1.omit)(oldMember, ['conversationId'])), { uuid });
                if (!addedByUserId) {
                    return newMember;
                }
                const newAddedBy = getConversationUuid.get({
                    conversationId: addedByUserId,
                });
                if (!newAddedBy) {
                    return newMember;
                }
                return Object.assign(Object.assign({}, newMember), { addedByUserId: newAddedBy });
            })
                .filter(isNotNil_1.isNotNil);
            result = Object.assign(Object.assign({}, result), { invitedGV2Members: newMembers });
        }
        if (result === message) {
            return false;
        }
        updateMessageStmt.run({
            id: result.id,
            json: JSON.stringify(result),
            sourceUuid: (_a = result.sourceUuid) !== null && _a !== void 0 ? _a : null,
        });
        return true;
    };
    db.transaction(() => {
        const allConversations = db
            .prepare(`
      SELECT json, profileLastFetchedAt
      FROM conversations
      ORDER BY id ASC;
      `)
            .all()
            .map(({ json }) => (0, util_1.jsonToObject)(json));
        logger.info('updateToSchemaVersion43: About to iterate through ' +
            `${allConversations.length} conversations`);
        for (const convo of allConversations) {
            upgradeConversation(convo);
        }
        const messageCount = (0, util_1.getCountFromTable)(db, 'messages');
        logger.info('updateToSchemaVersion43: About to iterate through ' +
            `${messageCount} messages`);
        let updatedCount = 0;
        for (const message of new util_1.TableIterator(db, 'messages')) {
            if (upgradeMessage(message)) {
                updatedCount += 1;
            }
        }
        logger.info(`updateToSchemaVersion43: Updated ${updatedCount} messages`);
        db.pragma('user_version = 43');
    })();
    logger.info('updateToSchemaVersion43: success!');
}
exports.default = updateToSchemaVersion43;
