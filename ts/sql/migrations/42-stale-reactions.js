"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../util");
function updateToSchemaVersion42(currentVersion, db, logger) {
    if (currentVersion >= 42) {
        return;
    }
    db.transaction(() => {
        // First, recreate messages table delete trigger with reaction support
        db.exec(`
      DROP TRIGGER messages_on_delete;

      CREATE TRIGGER messages_on_delete AFTER DELETE ON messages BEGIN
        DELETE FROM messages_fts WHERE rowid = old.rowid;
        DELETE FROM sendLogPayloads WHERE id IN (
          SELECT payloadId FROM sendLogMessageIds
          WHERE messageId = old.id
        );
        DELETE FROM reactions WHERE rowid IN (
          SELECT rowid FROM reactions
          WHERE messageId = old.id
        );
      END;
    `);
        // Then, delete previously-orphaned reactions
        // Note: we use `pluck` here to fetch only the first column of
        //   returned row.
        const messageIdList = db
            .prepare('SELECT id FROM messages ORDER BY id ASC;')
            .pluck()
            .all();
        const allReactions = db.prepare('SELECT rowid, messageId FROM reactions;').all();
        const messageIds = new Set(messageIdList);
        const reactionsToDelete = [];
        allReactions.forEach(reaction => {
            if (!messageIds.has(reaction.messageId)) {
                reactionsToDelete.push(reaction.rowid);
            }
        });
        function deleteReactions(rowids) {
            db.prepare(`
        DELETE FROM reactions
        WHERE rowid IN ( ${rowids.map(() => '?').join(', ')} );
        `).run(rowids);
        }
        if (reactionsToDelete.length > 0) {
            logger.info(`Deleting ${reactionsToDelete.length} orphaned reactions`);
            (0, util_1.batchMultiVarQuery)(db, reactionsToDelete, deleteReactions);
        }
        db.pragma('user_version = 42');
    })();
    logger.info('updateToSchemaVersion42: success!');
}
exports.default = updateToSchemaVersion42;
