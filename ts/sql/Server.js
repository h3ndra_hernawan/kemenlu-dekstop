"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable camelcase */
const path_1 = require("path");
const mkdirp_1 = __importDefault(require("mkdirp"));
const rimraf_1 = __importDefault(require("rimraf"));
const better_sqlite3_1 = __importDefault(require("better-sqlite3"));
const p_props_1 = __importDefault(require("p-props"));
const lodash_1 = require("lodash");
const MessageReadStatus_1 = require("../messages/MessageReadStatus");
const StorageUIKeys_1 = require("../types/StorageUIKeys");
const UUID_1 = require("../types/UUID");
const assert_1 = require("../util/assert");
const combineNames_1 = require("../util/combineNames");
const consoleLogger_1 = require("../util/consoleLogger");
const dropNull_1 = require("../util/dropNull");
const isNormalNumber_1 = require("../util/isNormalNumber");
const isNotNil_1 = require("../util/isNotNil");
const missingCaseError_1 = require("../util/missingCaseError");
const parseIntOrThrow_1 = require("../util/parseIntOrThrow");
const durations = __importStar(require("../util/durations"));
const formatCountForLogging_1 = require("../logging/formatCountForLogging");
const Calling_1 = require("../types/Calling");
const RemoveAllConfiguration_1 = require("../types/RemoveAllConfiguration");
const BadgeCategory_1 = require("../badges/BadgeCategory");
const BadgeImageTheme_1 = require("../badges/BadgeImageTheme");
const log = __importStar(require("../logging/log"));
const util_1 = require("./util");
const migrations_1 = require("./migrations");
// Because we can't force this module to conform to an interface, we narrow our exports
//   to this one default export, which does conform to the interface.
// Note: In Javascript, you need to access the .default property when requiring it
// https://github.com/microsoft/TypeScript/issues/420
const dataInterface = {
    close,
    removeDB,
    removeIndexedDBFiles,
    createOrUpdateIdentityKey,
    getIdentityKeyById,
    bulkAddIdentityKeys,
    removeIdentityKeyById,
    removeAllIdentityKeys,
    getAllIdentityKeys,
    createOrUpdatePreKey,
    getPreKeyById,
    bulkAddPreKeys,
    removePreKeyById,
    removeAllPreKeys,
    getAllPreKeys,
    createOrUpdateSignedPreKey,
    getSignedPreKeyById,
    getAllSignedPreKeys,
    bulkAddSignedPreKeys,
    removeSignedPreKeyById,
    removeAllSignedPreKeys,
    createOrUpdateItem,
    getItemById,
    getAllItems,
    removeItemById,
    removeAllItems,
    createOrUpdateSenderKey,
    getSenderKeyById,
    removeAllSenderKeys,
    getAllSenderKeys,
    removeSenderKeyById,
    insertSentProto,
    deleteSentProtosOlderThan,
    deleteSentProtoByMessageId,
    insertProtoRecipients,
    deleteSentProtoRecipient,
    getSentProtoByRecipient,
    removeAllSentProtos,
    getAllSentProtos,
    _getAllSentProtoRecipients,
    _getAllSentProtoMessageIds,
    createOrUpdateSession,
    createOrUpdateSessions,
    commitSessionsAndUnprocessed,
    bulkAddSessions,
    removeSessionById,
    removeSessionsByConversation,
    removeAllSessions,
    getAllSessions,
    getConversationCount,
    saveConversation,
    saveConversations,
    getConversationById,
    updateConversation,
    updateConversations,
    removeConversation,
    eraseStorageServiceStateFromConversations,
    getAllConversations,
    getAllConversationIds,
    getAllPrivateConversations,
    getAllGroupsInvolvingUuid,
    updateAllConversationColors,
    searchConversations,
    searchMessages,
    searchMessagesInConversation,
    getMessageCount,
    saveMessage,
    saveMessages,
    removeMessage,
    removeMessages,
    getUnreadCountForConversation,
    getUnreadByConversationAndMarkRead,
    getUnreadReactionsAndMarkRead,
    markReactionAsRead,
    addReaction,
    removeReactionFromConversation,
    _getAllReactions,
    getMessageBySender,
    getMessageById,
    getMessagesById,
    _getAllMessages,
    getAllMessageIds,
    getMessagesBySentAt,
    getExpiredMessages,
    getMessagesUnexpectedlyMissingExpirationStartTimestamp,
    getSoonestMessageExpiry,
    getNextTapToViewMessageTimestampToAgeOut,
    getTapToViewMessagesNeedingErase,
    getOlderMessagesByConversation,
    getNewerMessagesByConversation,
    getMessageMetricsForConversation,
    getLastConversationMessages,
    hasGroupCallHistoryMessage,
    migrateConversationMessages,
    getUnprocessedCount,
    getAllUnprocessed,
    updateUnprocessedWithData,
    updateUnprocessedsWithData,
    getUnprocessedById,
    removeUnprocessed,
    removeAllUnprocessed,
    getNextAttachmentDownloadJobs,
    saveAttachmentDownloadJob,
    setAttachmentDownloadJobPending,
    resetAttachmentDownloadPending,
    removeAttachmentDownloadJob,
    removeAllAttachmentDownloadJobs,
    createOrUpdateStickerPack,
    updateStickerPackStatus,
    createOrUpdateSticker,
    updateStickerLastUsed,
    addStickerPackReference,
    deleteStickerPackReference,
    getStickerCount,
    deleteStickerPack,
    getAllStickerPacks,
    getAllStickers,
    getRecentStickers,
    clearAllErrorStickerPackAttempts,
    updateEmojiUsage,
    getRecentEmojis,
    getAllBadges,
    updateOrCreateBadges,
    badgeImageFileDownloaded,
    removeAll,
    removeAllConfiguration,
    getMessagesNeedingUpgrade,
    getMessagesWithVisualMediaAttachments,
    getMessagesWithFileAttachments,
    getMessageServerGuidsForSpam,
    getJobsInQueue,
    insertJob,
    deleteJob,
    processGroupCallRingRequest,
    processGroupCallRingCancelation,
    cleanExpiredGroupCallRings,
    getMaxMessageCounter,
    getStatisticsForLogging,
    // Server-only
    getCorruptionLog,
    initialize,
    initializeRenderer,
    removeKnownAttachments,
    removeKnownStickers,
    removeKnownDraftAttachments,
    getAllBadgeImageFileLocalPaths,
};
exports.default = dataInterface;
const statementCache = new WeakMap();
function prepare(db, query) {
    let dbCache = statementCache.get(db);
    if (!dbCache) {
        dbCache = new Map();
        statementCache.set(db, dbCache);
    }
    let result = dbCache.get(query);
    if (!result) {
        result = db.prepare(query);
        dbCache.set(query, result);
    }
    return result;
}
function rowToConversation(row) {
    const parsedJson = JSON.parse(row.json);
    let profileLastFetchedAt;
    if ((0, isNormalNumber_1.isNormalNumber)(row.profileLastFetchedAt)) {
        profileLastFetchedAt = row.profileLastFetchedAt;
    }
    else {
        (0, assert_1.assert)((0, lodash_1.isNil)(row.profileLastFetchedAt), 'profileLastFetchedAt contained invalid data; defaulting to undefined');
        profileLastFetchedAt = undefined;
    }
    return Object.assign(Object.assign({}, parsedJson), { profileLastFetchedAt });
}
function rowToSticker(row) {
    return Object.assign(Object.assign({}, row), { isCoverOnly: Boolean(row.isCoverOnly), emoji: (0, dropNull_1.dropNull)(row.emoji) });
}
function isRenderer() {
    if (typeof process === 'undefined' || !process) {
        return true;
    }
    return process.type === 'renderer';
}
function keyDatabase(db, key) {
    // https://www.zetetic.net/sqlcipher/sqlcipher-api/#key
    db.pragma(`key = "x'${key}'"`);
}
function switchToWAL(db) {
    // https://sqlite.org/wal.html
    db.pragma('journal_mode = WAL');
    db.pragma('synchronous = FULL');
}
function migrateSchemaVersion(db) {
    const userVersion = (0, util_1.getUserVersion)(db);
    if (userVersion > 0) {
        return;
    }
    const schemaVersion = (0, util_1.getSchemaVersion)(db);
    const newUserVersion = schemaVersion > 18 ? 16 : schemaVersion;
    logger.info('migrateSchemaVersion: Migrating from schema_version ' +
        `${schemaVersion} to user_version ${newUserVersion}`);
    (0, util_1.setUserVersion)(db, newUserVersion);
}
function openAndMigrateDatabase(filePath, key) {
    let db;
    // First, we try to open the database without any cipher changes
    try {
        db = new better_sqlite3_1.default(filePath);
        keyDatabase(db, key);
        switchToWAL(db);
        migrateSchemaVersion(db);
        return db;
    }
    catch (error) {
        if (db) {
            db.close();
        }
        logger.info('migrateDatabase: Migration without cipher change failed');
    }
    // If that fails, we try to open the database with 3.x compatibility to extract the
    //   user_version (previously stored in schema_version, blown away by cipher_migrate).
    db = new better_sqlite3_1.default(filePath);
    keyDatabase(db, key);
    // https://www.zetetic.net/blog/2018/11/30/sqlcipher-400-release/#compatability-sqlcipher-4-0-0
    db.pragma('cipher_compatibility = 3');
    migrateSchemaVersion(db);
    db.close();
    // After migrating user_version -> schema_version, we reopen database, because we can't
    //   migrate to the latest ciphers after we've modified the defaults.
    db = new better_sqlite3_1.default(filePath);
    keyDatabase(db, key);
    db.pragma('cipher_migrate');
    switchToWAL(db);
    return db;
}
const INVALID_KEY = /[^0-9A-Fa-f]/;
function openAndSetUpSQLCipher(filePath, { key }) {
    const match = INVALID_KEY.exec(key);
    if (match) {
        throw new Error(`setupSQLCipher: key '${key}' is not valid`);
    }
    const db = openAndMigrateDatabase(filePath, key);
    // Because foreign key support is not enabled by default!
    db.pragma('foreign_keys = ON');
    return db;
}
let globalInstance;
let logger = consoleLogger_1.consoleLogger;
let globalInstanceRenderer;
let databaseFilePath;
let indexedDBPath;
let corruptionLog = new Array();
better_sqlite3_1.default.setCorruptionLogger(line => {
    logger.error(`SQL corruption: ${line}`);
    corruptionLog.push(line);
});
function getCorruptionLog() {
    const result = corruptionLog.join('\n');
    corruptionLog = [];
    return result;
}
async function initialize({ configDir, key, logger: suppliedLogger, }) {
    if (globalInstance) {
        throw new Error('Cannot initialize more than once!');
    }
    if (!(0, lodash_1.isString)(configDir)) {
        throw new Error('initialize: configDir is required!');
    }
    if (!(0, lodash_1.isString)(key)) {
        throw new Error('initialize: key is required!');
    }
    logger = suppliedLogger;
    indexedDBPath = (0, path_1.join)(configDir, 'IndexedDB');
    const dbDir = (0, path_1.join)(configDir, 'sql');
    mkdirp_1.default.sync(dbDir);
    databaseFilePath = (0, path_1.join)(dbDir, 'db.sqlite');
    let db;
    try {
        db = openAndSetUpSQLCipher(databaseFilePath, { key });
        // For profiling use:
        // db.pragma('cipher_profile=\'sqlcipher.log\'');
        (0, migrations_1.updateSchema)(db, logger);
        // At this point we can allow general access to the database
        globalInstance = db;
        // test database
        getMessageCountSync();
    }
    catch (error) {
        logger.error('Database startup error:', error.stack);
        if (db) {
            db.close();
        }
        throw error;
    }
}
async function initializeRenderer({ configDir, key, }) {
    if (!isRenderer()) {
        throw new Error('Cannot call from main process.');
    }
    if (globalInstanceRenderer) {
        throw new Error('Cannot initialize more than once!');
    }
    if (!(0, lodash_1.isString)(configDir)) {
        throw new Error('initialize: configDir is required!');
    }
    if (!(0, lodash_1.isString)(key)) {
        throw new Error('initialize: key is required!');
    }
    if (!indexedDBPath) {
        indexedDBPath = (0, path_1.join)(configDir, 'IndexedDB');
    }
    const dbDir = (0, path_1.join)(configDir, 'sql');
    if (!databaseFilePath) {
        databaseFilePath = (0, path_1.join)(dbDir, 'db.sqlite');
    }
    let promisified;
    try {
        promisified = openAndSetUpSQLCipher(databaseFilePath, { key });
        // At this point we can allow general access to the database
        globalInstanceRenderer = promisified;
        // test database
        getMessageCountSync();
    }
    catch (error) {
        log.error('Database startup error:', error.stack);
        throw error;
    }
}
async function close() {
    for (const dbRef of [globalInstanceRenderer, globalInstance]) {
        // SQLLite documentation suggests that we run `PRAGMA optimize` right
        // before closing the database connection.
        dbRef === null || dbRef === void 0 ? void 0 : dbRef.pragma('optimize');
        dbRef === null || dbRef === void 0 ? void 0 : dbRef.close();
    }
    globalInstance = undefined;
    globalInstanceRenderer = undefined;
}
async function removeDB() {
    if (globalInstance) {
        try {
            globalInstance.close();
        }
        catch (error) {
            logger.error('removeDB: Failed to close database:', error.stack);
        }
        globalInstance = undefined;
    }
    if (!databaseFilePath) {
        throw new Error('removeDB: Cannot erase database without a databaseFilePath!');
    }
    rimraf_1.default.sync(databaseFilePath);
    rimraf_1.default.sync(`${databaseFilePath}-shm`);
    rimraf_1.default.sync(`${databaseFilePath}-wal`);
}
async function removeIndexedDBFiles() {
    if (!indexedDBPath) {
        throw new Error('removeIndexedDBFiles: Need to initialize and set indexedDBPath first!');
    }
    const pattern = (0, path_1.join)(indexedDBPath, '*.leveldb');
    rimraf_1.default.sync(pattern);
    indexedDBPath = undefined;
}
function getInstance() {
    if (isRenderer()) {
        if (!globalInstanceRenderer) {
            throw new Error('getInstance: globalInstanceRenderer not set!');
        }
        return globalInstanceRenderer;
    }
    if (!globalInstance) {
        throw new Error('getInstance: globalInstance not set!');
    }
    return globalInstance;
}
const IDENTITY_KEYS_TABLE = 'identityKeys';
async function createOrUpdateIdentityKey(data) {
    return (0, util_1.createOrUpdate)(getInstance(), IDENTITY_KEYS_TABLE, data);
}
async function getIdentityKeyById(id) {
    return (0, util_1.getById)(getInstance(), IDENTITY_KEYS_TABLE, id);
}
async function bulkAddIdentityKeys(array) {
    return (0, util_1.bulkAdd)(getInstance(), IDENTITY_KEYS_TABLE, array);
}
async function removeIdentityKeyById(id) {
    return (0, util_1.removeById)(getInstance(), IDENTITY_KEYS_TABLE, id);
}
async function removeAllIdentityKeys() {
    return (0, util_1.removeAllFromTable)(getInstance(), IDENTITY_KEYS_TABLE);
}
async function getAllIdentityKeys() {
    return (0, util_1.getAllFromTable)(getInstance(), IDENTITY_KEYS_TABLE);
}
const PRE_KEYS_TABLE = 'preKeys';
async function createOrUpdatePreKey(data) {
    return (0, util_1.createOrUpdate)(getInstance(), PRE_KEYS_TABLE, data);
}
async function getPreKeyById(id) {
    return (0, util_1.getById)(getInstance(), PRE_KEYS_TABLE, id);
}
async function bulkAddPreKeys(array) {
    return (0, util_1.bulkAdd)(getInstance(), PRE_KEYS_TABLE, array);
}
async function removePreKeyById(id) {
    return (0, util_1.removeById)(getInstance(), PRE_KEYS_TABLE, id);
}
async function removeAllPreKeys() {
    return (0, util_1.removeAllFromTable)(getInstance(), PRE_KEYS_TABLE);
}
async function getAllPreKeys() {
    return (0, util_1.getAllFromTable)(getInstance(), PRE_KEYS_TABLE);
}
const SIGNED_PRE_KEYS_TABLE = 'signedPreKeys';
async function createOrUpdateSignedPreKey(data) {
    return (0, util_1.createOrUpdate)(getInstance(), SIGNED_PRE_KEYS_TABLE, data);
}
async function getSignedPreKeyById(id) {
    return (0, util_1.getById)(getInstance(), SIGNED_PRE_KEYS_TABLE, id);
}
async function bulkAddSignedPreKeys(array) {
    return (0, util_1.bulkAdd)(getInstance(), SIGNED_PRE_KEYS_TABLE, array);
}
async function removeSignedPreKeyById(id) {
    return (0, util_1.removeById)(getInstance(), SIGNED_PRE_KEYS_TABLE, id);
}
async function removeAllSignedPreKeys() {
    return (0, util_1.removeAllFromTable)(getInstance(), SIGNED_PRE_KEYS_TABLE);
}
async function getAllSignedPreKeys() {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json
      FROM signedPreKeys
      ORDER BY id ASC;
      `)
        .all();
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
const ITEMS_TABLE = 'items';
async function createOrUpdateItem(data) {
    return (0, util_1.createOrUpdate)(getInstance(), ITEMS_TABLE, data);
}
async function getItemById(id) {
    return (0, util_1.getById)(getInstance(), ITEMS_TABLE, id);
}
async function getAllItems() {
    const db = getInstance();
    const rows = db
        .prepare('SELECT json FROM items ORDER BY id ASC;')
        .all();
    const items = rows.map(row => (0, util_1.jsonToObject)(row.json));
    const result = Object.create(null);
    for (const { id, value } of items) {
        result[id] = value;
    }
    return result;
}
async function removeItemById(id) {
    return (0, util_1.removeById)(getInstance(), ITEMS_TABLE, id);
}
async function removeAllItems() {
    return (0, util_1.removeAllFromTable)(getInstance(), ITEMS_TABLE);
}
async function createOrUpdateSenderKey(key) {
    const db = getInstance();
    prepare(db, `
    INSERT OR REPLACE INTO senderKeys (
      id,
      senderId,
      distributionId,
      data,
      lastUpdatedDate
    ) values (
      $id,
      $senderId,
      $distributionId,
      $data,
      $lastUpdatedDate
    )
    `).run(key);
}
async function getSenderKeyById(id) {
    const db = getInstance();
    const row = prepare(db, 'SELECT * FROM senderKeys WHERE id = $id').get({
        id,
    });
    return row;
}
async function removeAllSenderKeys() {
    const db = getInstance();
    prepare(db, 'DELETE FROM senderKeys').run();
}
async function getAllSenderKeys() {
    const db = getInstance();
    const rows = prepare(db, 'SELECT * FROM senderKeys').all();
    return rows;
}
async function removeSenderKeyById(id) {
    const db = getInstance();
    prepare(db, 'DELETE FROM senderKeys WHERE id = $id').run({ id });
}
async function insertSentProto(proto, options) {
    const db = getInstance();
    const { recipients, messageIds } = options;
    // Note: we use `pluck` in this function to fetch only the first column of returned row.
    return db.transaction(() => {
        // 1. Insert the payload, fetching its primary key id
        const info = prepare(db, `
      INSERT INTO sendLogPayloads (
        contentHint,
        proto,
        timestamp
      ) VALUES (
        $contentHint,
        $proto,
        $timestamp
      );
      `).run(proto);
        const id = (0, parseIntOrThrow_1.parseIntOrThrow)(info.lastInsertRowid, 'insertSentProto/lastInsertRowid');
        // 2. Insert a record for each recipient device.
        const recipientStatement = prepare(db, `
      INSERT INTO sendLogRecipients (
        payloadId,
        recipientUuid,
        deviceId
      ) VALUES (
        $id,
        $recipientUuid,
        $deviceId
      );
      `);
        const recipientUuids = Object.keys(recipients);
        for (const recipientUuid of recipientUuids) {
            const deviceIds = recipients[recipientUuid];
            for (const deviceId of deviceIds) {
                recipientStatement.run({
                    id,
                    recipientUuid,
                    deviceId,
                });
            }
        }
        // 2. Insert a record for each message referenced by this payload.
        const messageStatement = prepare(db, `
      INSERT INTO sendLogMessageIds (
        payloadId,
        messageId
      ) VALUES (
        $id,
        $messageId
      );
      `);
        for (const messageId of messageIds) {
            messageStatement.run({
                id,
                messageId,
            });
        }
        return id;
    })();
}
async function deleteSentProtosOlderThan(timestamp) {
    const db = getInstance();
    prepare(db, `
    DELETE FROM sendLogPayloads
    WHERE
      timestamp IS NULL OR
      timestamp < $timestamp;
    `).run({
        timestamp,
    });
}
async function deleteSentProtoByMessageId(messageId) {
    const db = getInstance();
    prepare(db, `
    DELETE FROM sendLogPayloads WHERE id IN (
      SELECT payloadId FROM sendLogMessageIds
      WHERE messageId = $messageId
    );
    `).run({
        messageId,
    });
}
async function insertProtoRecipients({ id, recipientUuid, deviceIds, }) {
    const db = getInstance();
    db.transaction(() => {
        const statement = prepare(db, `
      INSERT INTO sendLogRecipients (
        payloadId,
        recipientUuid,
        deviceId
      ) VALUES (
        $id,
        $recipientUuid,
        $deviceId
      );
      `);
        for (const deviceId of deviceIds) {
            statement.run({
                id,
                recipientUuid,
                deviceId,
            });
        }
    })();
}
async function deleteSentProtoRecipient(options) {
    const db = getInstance();
    const items = Array.isArray(options) ? options : [options];
    // Note: we use `pluck` in this function to fetch only the first column of
    // returned row.
    db.transaction(() => {
        for (const item of items) {
            const { timestamp, recipientUuid, deviceId } = item;
            // 1. Figure out what payload we're talking about.
            const rows = prepare(db, `
        SELECT sendLogPayloads.id FROM sendLogPayloads
        INNER JOIN sendLogRecipients
          ON sendLogRecipients.payloadId = sendLogPayloads.id
        WHERE
          sendLogPayloads.timestamp = $timestamp AND
          sendLogRecipients.recipientUuid = $recipientUuid AND
          sendLogRecipients.deviceId = $deviceId;
       `).all({ timestamp, recipientUuid, deviceId });
            if (!rows.length) {
                continue;
            }
            if (rows.length > 1) {
                logger.warn('deleteSentProtoRecipient: More than one payload matches ' +
                    `recipient and timestamp ${timestamp}. Using the first.`);
                continue;
            }
            const { id } = rows[0];
            // 2. Delete the recipient/device combination in question.
            prepare(db, `
        DELETE FROM sendLogRecipients
        WHERE
          payloadId = $id AND
          recipientUuid = $recipientUuid AND
          deviceId = $deviceId;
        `).run({ id, recipientUuid, deviceId });
            // 3. See how many more recipient devices there were for this payload.
            const remaining = prepare(db, 'SELECT count(*) FROM sendLogRecipients WHERE payloadId = $id;')
                .pluck(true)
                .get({ id });
            if (!(0, lodash_1.isNumber)(remaining)) {
                throw new Error('deleteSentProtoRecipient: select count() returned non-number!');
            }
            if (remaining > 0) {
                continue;
            }
            // 4. Delete the entire payload if there are no more recipients left.
            logger.info('deleteSentProtoRecipient: ' +
                `Deleting proto payload for timestamp ${timestamp}`);
            prepare(db, 'DELETE FROM sendLogPayloads WHERE id = $id;').run({
                id,
            });
        }
    })();
}
async function getSentProtoByRecipient({ now, recipientUuid, timestamp, }) {
    const db = getInstance();
    const HOUR = 1000 * 60 * 60;
    const oneDayAgo = now - HOUR * 24;
    await deleteSentProtosOlderThan(oneDayAgo);
    const row = prepare(db, `
    SELECT
      sendLogPayloads.*,
      GROUP_CONCAT(DISTINCT sendLogMessageIds.messageId) AS messageIds
    FROM sendLogPayloads
    INNER JOIN sendLogRecipients ON sendLogRecipients.payloadId = sendLogPayloads.id
    LEFT JOIN sendLogMessageIds ON sendLogMessageIds.payloadId = sendLogPayloads.id
    WHERE
      sendLogPayloads.timestamp = $timestamp AND
      sendLogRecipients.recipientUuid = $recipientUuid
    GROUP BY sendLogPayloads.id;
    `).get({
        timestamp,
        recipientUuid,
    });
    if (!row) {
        return undefined;
    }
    const { messageIds } = row;
    return Object.assign(Object.assign({}, row), { messageIds: messageIds ? messageIds.split(',') : [] });
}
async function removeAllSentProtos() {
    const db = getInstance();
    prepare(db, 'DELETE FROM sendLogPayloads;').run();
}
async function getAllSentProtos() {
    const db = getInstance();
    const rows = prepare(db, 'SELECT * FROM sendLogPayloads;').all();
    return rows;
}
async function _getAllSentProtoRecipients() {
    const db = getInstance();
    const rows = prepare(db, 'SELECT * FROM sendLogRecipients;').all();
    return rows;
}
async function _getAllSentProtoMessageIds() {
    const db = getInstance();
    const rows = prepare(db, 'SELECT * FROM sendLogMessageIds;').all();
    return rows;
}
const SESSIONS_TABLE = 'sessions';
function createOrUpdateSessionSync(data) {
    const db = getInstance();
    const { id, conversationId, ourUuid, uuid } = data;
    if (!id) {
        throw new Error('createOrUpdateSession: Provided data did not have a truthy id');
    }
    if (!conversationId) {
        throw new Error('createOrUpdateSession: Provided data did not have a truthy conversationId');
    }
    prepare(db, `
    INSERT OR REPLACE INTO sessions (
      id,
      conversationId,
      ourUuid,
      uuid,
      json
    ) values (
      $id,
      $conversationId,
      $ourUuid,
      $uuid,
      $json
    )
    `).run({
        id,
        conversationId,
        ourUuid,
        uuid,
        json: (0, util_1.objectToJSON)(data),
    });
}
async function createOrUpdateSession(data) {
    return createOrUpdateSessionSync(data);
}
async function createOrUpdateSessions(array) {
    const db = getInstance();
    db.transaction(() => {
        for (const item of array) {
            (0, assert_1.assertSync)(createOrUpdateSessionSync(item));
        }
    })();
}
async function commitSessionsAndUnprocessed({ sessions, unprocessed, }) {
    const db = getInstance();
    db.transaction(() => {
        for (const item of sessions) {
            (0, assert_1.assertSync)(createOrUpdateSessionSync(item));
        }
        for (const item of unprocessed) {
            (0, assert_1.assertSync)(saveUnprocessedSync(item));
        }
    })();
}
async function bulkAddSessions(array) {
    return (0, util_1.bulkAdd)(getInstance(), SESSIONS_TABLE, array);
}
async function removeSessionById(id) {
    return (0, util_1.removeById)(getInstance(), SESSIONS_TABLE, id);
}
async function removeSessionsByConversation(conversationId) {
    const db = getInstance();
    db.prepare(`
    DELETE FROM sessions
    WHERE conversationId = $conversationId;
    `).run({
        conversationId,
    });
}
async function removeAllSessions() {
    return (0, util_1.removeAllFromTable)(getInstance(), SESSIONS_TABLE);
}
async function getAllSessions() {
    return (0, util_1.getAllFromTable)(getInstance(), SESSIONS_TABLE);
}
// Conversations
async function getConversationCount() {
    return (0, util_1.getCountFromTable)(getInstance(), 'conversations');
}
function getConversationMembersList({ members, membersV2 }) {
    if (membersV2) {
        return membersV2.map((item) => item.uuid).join(' ');
    }
    if (members) {
        return members.join(' ');
    }
    return null;
}
function saveConversationSync(data, db = getInstance()) {
    const { active_at, e164, groupId, id, name, profileFamilyName, profileName, profileLastFetchedAt, type, uuid, } = data;
    const membersList = getConversationMembersList(data);
    db.prepare(`
    INSERT INTO conversations (
      id,
      json,

      e164,
      uuid,
      groupId,

      active_at,
      type,
      members,
      name,
      profileName,
      profileFamilyName,
      profileFullName,
      profileLastFetchedAt
    ) values (
      $id,
      $json,

      $e164,
      $uuid,
      $groupId,

      $active_at,
      $type,
      $members,
      $name,
      $profileName,
      $profileFamilyName,
      $profileFullName,
      $profileLastFetchedAt
    );
    `).run({
        id,
        json: (0, util_1.objectToJSON)((0, lodash_1.omit)(data, ['profileLastFetchedAt', 'unblurredAvatarPath'])),
        e164: e164 || null,
        uuid: uuid || null,
        groupId: groupId || null,
        active_at: active_at || null,
        type,
        members: membersList,
        name: name || null,
        profileName: profileName || null,
        profileFamilyName: profileFamilyName || null,
        profileFullName: (0, combineNames_1.combineNames)(profileName, profileFamilyName) || null,
        profileLastFetchedAt: profileLastFetchedAt || null,
    });
}
async function saveConversation(data, db = getInstance()) {
    return saveConversationSync(data, db);
}
async function saveConversations(arrayOfConversations) {
    const db = getInstance();
    db.transaction(() => {
        for (const conversation of arrayOfConversations) {
            (0, assert_1.assertSync)(saveConversationSync(conversation));
        }
    })();
}
function updateConversationSync(data, db = getInstance()) {
    const { id, active_at, type, name, profileName, profileFamilyName, profileLastFetchedAt, e164, uuid, } = data;
    const membersList = getConversationMembersList(data);
    db.prepare(`
    UPDATE conversations SET
      json = $json,

      e164 = $e164,
      uuid = $uuid,

      active_at = $active_at,
      type = $type,
      members = $members,
      name = $name,
      profileName = $profileName,
      profileFamilyName = $profileFamilyName,
      profileFullName = $profileFullName,
      profileLastFetchedAt = $profileLastFetchedAt
    WHERE id = $id;
    `).run({
        id,
        json: (0, util_1.objectToJSON)((0, lodash_1.omit)(data, ['profileLastFetchedAt', 'unblurredAvatarPath'])),
        e164: e164 || null,
        uuid: uuid || null,
        active_at: active_at || null,
        type,
        members: membersList,
        name: name || null,
        profileName: profileName || null,
        profileFamilyName: profileFamilyName || null,
        profileFullName: (0, combineNames_1.combineNames)(profileName, profileFamilyName) || null,
        profileLastFetchedAt: profileLastFetchedAt || null,
    });
}
async function updateConversation(data) {
    return updateConversationSync(data);
}
async function updateConversations(array) {
    const db = getInstance();
    db.transaction(() => {
        for (const item of array) {
            (0, assert_1.assertSync)(updateConversationSync(item));
        }
    })();
}
function removeConversationsSync(ids) {
    const db = getInstance();
    // Our node interface doesn't seem to allow you to replace one single ? with an array
    db.prepare(`
    DELETE FROM conversations
    WHERE id IN ( ${ids.map(() => '?').join(', ')} );
    `).run(ids);
}
async function removeConversation(id) {
    const db = getInstance();
    if (!Array.isArray(id)) {
        db.prepare('DELETE FROM conversations WHERE id = $id;').run({
            id,
        });
        return;
    }
    if (!id.length) {
        throw new Error('removeConversation: No ids to delete!');
    }
    (0, util_1.batchMultiVarQuery)(db, id, removeConversationsSync);
}
async function getConversationById(id) {
    const db = getInstance();
    const row = db
        .prepare('SELECT json FROM conversations WHERE id = $id;')
        .get({ id });
    if (!row) {
        return undefined;
    }
    return (0, util_1.jsonToObject)(row.json);
}
async function eraseStorageServiceStateFromConversations() {
    const db = getInstance();
    db.prepare(`
    UPDATE conversations
    SET
      json = json_remove(json, '$.storageID', '$.needsStorageServiceSync', '$.unknownFields', '$.storageProfileKey');
    `).run();
}
function getAllConversationsSync(db = getInstance()) {
    const rows = db
        .prepare(`
      SELECT json, profileLastFetchedAt
      FROM conversations
      ORDER BY id ASC;
      `)
        .all();
    return rows.map(row => rowToConversation(row));
}
async function getAllConversations() {
    return getAllConversationsSync();
}
async function getAllConversationIds() {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT id FROM conversations ORDER BY id ASC;
      `)
        .all();
    return rows.map(row => row.id);
}
async function getAllPrivateConversations() {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json, profileLastFetchedAt
      FROM conversations
      WHERE type = 'private'
      ORDER BY id ASC;
      `)
        .all();
    return rows.map(row => rowToConversation(row));
}
async function getAllGroupsInvolvingUuid(uuid) {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json, profileLastFetchedAt
      FROM conversations WHERE
        type = 'group' AND
        members LIKE $uuid
      ORDER BY id ASC;
      `)
        .all({
        uuid: `%${uuid}%`,
    });
    return rows.map(row => rowToConversation(row));
}
async function searchConversations(query, { limit } = {}) {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json, profileLastFetchedAt
      FROM conversations WHERE
        (
          e164 LIKE $query OR
          name LIKE $query OR
          profileFullName LIKE $query
        )
      ORDER BY active_at DESC
      LIMIT $limit
      `)
        .all({
        query: `%${query}%`,
        limit: limit || 100,
    });
    return rows.map(row => rowToConversation(row));
}
async function searchMessages(query, params = {}) {
    const { limit = 500, conversationId } = params;
    const db = getInstance();
    // sqlite queries with a join on a virtual table (like FTS5) are de-optimized
    // and can't use indices for ordering results. Instead an in-memory index of
    // the join rows is sorted on the fly, and this becomes substantially
    // slower when there are large columns in it (like `messages.json`).
    //
    // Thus here we take an indirect approach and store `rowid`s in a temporary
    // table for all messages that match the FTS query. Then we create another
    // table to sort and limit the results, and finally join on it when fetch
    // the snippets and json. The benefit of this is that the `ORDER BY` and
    // `LIMIT` happen without virtual table and are thus covered by
    // `messages_searchOrder` index.
    return db.transaction(() => {
        db.exec(`
      CREATE TEMP TABLE tmp_results(rowid INTEGER PRIMARY KEY ASC);
      CREATE TEMP TABLE tmp_filtered_results(rowid INTEGER PRIMARY KEY ASC);
      `);
        db.prepare(`
        INSERT INTO tmp_results (rowid)
        SELECT
          rowid
        FROM
          messages_fts
        WHERE
          messages_fts.body MATCH $query;
      `).run({ query });
        if (conversationId === undefined) {
            db.prepare(`
          INSERT INTO tmp_filtered_results (rowid)
          SELECT
            tmp_results.rowid
          FROM
            tmp_results
          INNER JOIN
            messages ON messages.rowid = tmp_results.rowid
          ORDER BY messages.received_at DESC, messages.sent_at DESC
          LIMIT $limit;
        `).run({ limit });
        }
        else {
            db.prepare(`
          INSERT INTO tmp_filtered_results (rowid)
          SELECT
            tmp_results.rowid
          FROM
            tmp_results
          INNER JOIN
            messages ON messages.rowid = tmp_results.rowid
          WHERE
            messages.conversationId = $conversationId
          ORDER BY messages.received_at DESC, messages.sent_at DESC
          LIMIT $limit;
        `).run({ conversationId, limit });
        }
        // The `MATCH` is necessary in order to for `snippet()` helper function to
        // give us the right results. We can't call `snippet()` in the query above
        // because it would bloat the temporary table with text data and we want
        // to keep its size minimal for `ORDER BY` + `LIMIT` to be fast.
        const result = db
            .prepare(`
        SELECT
          messages.json,
          snippet(messages_fts, -1, '<<left>>', '<<right>>', '...', 10)
            AS snippet
        FROM tmp_filtered_results
        INNER JOIN messages_fts
          ON messages_fts.rowid = tmp_filtered_results.rowid
        INNER JOIN messages
          ON messages.rowid = tmp_filtered_results.rowid
        WHERE
          messages_fts.body MATCH $query
        ORDER BY messages.received_at DESC, messages.sent_at DESC;
        `)
            .all({ query });
        db.exec(`
      DROP TABLE tmp_results;
      DROP TABLE tmp_filtered_results;
      `);
        return result;
    })();
}
async function searchMessagesInConversation(query, conversationId, { limit = 100 } = {}) {
    return searchMessages(query, { conversationId, limit });
}
function getMessageCountSync(conversationId, db = getInstance()) {
    if (conversationId === undefined) {
        return (0, util_1.getCountFromTable)(db, 'messages');
    }
    const count = db
        .prepare(`
        SELECT count(*)
        FROM messages
        WHERE conversationId = $conversationId;
        `)
        .pluck()
        .get({ conversationId });
    return count;
}
async function getMessageCount(conversationId) {
    return getMessageCountSync(conversationId);
}
function hasUserInitiatedMessages(conversationId) {
    const db = getInstance();
    // We apply the limit in the sub-query so that `json_extract` wouldn't run
    // for additional messages.
    const row = db
        .prepare(`
      SELECT COUNT(*) as count FROM
        (
          SELECT 1 FROM messages
          WHERE
            conversationId = $conversationId AND
            (type IS NULL
              OR
              type NOT IN (
                'profile-change',
                'verified-change',
                'message-history-unsynced',
                'keychange',
                'group-v1-migration',
                'universal-timer-notification',
                'change-number-notification',
                'group-v2-change'
              )
            )
          LIMIT 1
        );
      `)
        .get({ conversationId });
    return row.count !== 0;
}
function saveMessageSync(data, options = {}) {
    const { jobToInsert, forceSave, alreadyInTransaction, db = getInstance(), } = options;
    if (!alreadyInTransaction) {
        return db.transaction(() => {
            return (0, assert_1.assertSync)(saveMessageSync(data, Object.assign(Object.assign({}, options), { alreadyInTransaction: true })));
        })();
    }
    const { body, conversationId, hasAttachments, hasFileAttachments, hasVisualMediaAttachments, id, isErased, isViewOnce, received_at, schemaVersion, sent_at, serverGuid, source, sourceUuid, sourceDevice, type, readStatus, expireTimer, expirationStartTimestamp, } = data;
    const payload = {
        id,
        json: (0, util_1.objectToJSON)(data),
        body: body || null,
        conversationId,
        expirationStartTimestamp: expirationStartTimestamp || null,
        expireTimer: expireTimer || null,
        hasAttachments: hasAttachments ? 1 : 0,
        hasFileAttachments: hasFileAttachments ? 1 : 0,
        hasVisualMediaAttachments: hasVisualMediaAttachments ? 1 : 0,
        isErased: isErased ? 1 : 0,
        isViewOnce: isViewOnce ? 1 : 0,
        received_at: received_at || null,
        schemaVersion: schemaVersion || 0,
        serverGuid: serverGuid || null,
        sent_at: sent_at || null,
        source: source || null,
        sourceUuid: sourceUuid || null,
        sourceDevice: sourceDevice || null,
        type: type || null,
        readStatus: readStatus !== null && readStatus !== void 0 ? readStatus : null,
    };
    if (id && !forceSave) {
        prepare(db, `
      UPDATE messages SET
        id = $id,
        json = $json,

        body = $body,
        conversationId = $conversationId,
        expirationStartTimestamp = $expirationStartTimestamp,
        expireTimer = $expireTimer,
        hasAttachments = $hasAttachments,
        hasFileAttachments = $hasFileAttachments,
        hasVisualMediaAttachments = $hasVisualMediaAttachments,
        isErased = $isErased,
        isViewOnce = $isViewOnce,
        received_at = $received_at,
        schemaVersion = $schemaVersion,
        serverGuid = $serverGuid,
        sent_at = $sent_at,
        source = $source,
        sourceUuid = $sourceUuid,
        sourceDevice = $sourceDevice,
        type = $type,
        readStatus = $readStatus
      WHERE id = $id;
      `).run(payload);
        if (jobToInsert) {
            insertJobSync(db, jobToInsert);
        }
        return id;
    }
    const toCreate = Object.assign(Object.assign({}, data), { id: id || UUID_1.UUID.generate().toString() });
    prepare(db, `
    INSERT INTO messages (
      id,
      json,

      body,
      conversationId,
      expirationStartTimestamp,
      expireTimer,
      hasAttachments,
      hasFileAttachments,
      hasVisualMediaAttachments,
      isErased,
      isViewOnce,
      received_at,
      schemaVersion,
      serverGuid,
      sent_at,
      source,
      sourceUuid,
      sourceDevice,
      type,
      readStatus
    ) values (
      $id,
      $json,

      $body,
      $conversationId,
      $expirationStartTimestamp,
      $expireTimer,
      $hasAttachments,
      $hasFileAttachments,
      $hasVisualMediaAttachments,
      $isErased,
      $isViewOnce,
      $received_at,
      $schemaVersion,
      $serverGuid,
      $sent_at,
      $source,
      $sourceUuid,
      $sourceDevice,
      $type,
      $readStatus
    );
    `).run(Object.assign(Object.assign({}, payload), { id: toCreate.id, json: (0, util_1.objectToJSON)(toCreate) }));
    if (jobToInsert) {
        insertJobSync(db, jobToInsert);
    }
    return toCreate.id;
}
async function saveMessage(data, options) {
    return saveMessageSync(data, options);
}
async function saveMessages(arrayOfMessages, options) {
    const db = getInstance();
    const { forceSave } = options || {};
    db.transaction(() => {
        for (const message of arrayOfMessages) {
            (0, assert_1.assertSync)(saveMessageSync(message, { forceSave, alreadyInTransaction: true }));
        }
    })();
}
async function removeMessage(id) {
    const db = getInstance();
    db.prepare('DELETE FROM messages WHERE id = $id;').run({ id });
}
function removeMessagesSync(ids) {
    const db = getInstance();
    db.prepare(`
    DELETE FROM messages
    WHERE id IN ( ${ids.map(() => '?').join(', ')} );
    `).run(ids);
}
async function removeMessages(ids) {
    (0, util_1.batchMultiVarQuery)(getInstance(), ids, removeMessagesSync);
}
async function getMessageById(id) {
    const db = getInstance();
    const row = db
        .prepare('SELECT json FROM messages WHERE id = $id;')
        .get({
        id,
    });
    if (!row) {
        return undefined;
    }
    return (0, util_1.jsonToObject)(row.json);
}
async function getMessagesById(messageIds) {
    const db = getInstance();
    return (0, util_1.batchMultiVarQuery)(db, messageIds, (batch) => {
        const query = db.prepare(`SELECT json FROM messages WHERE id IN (${Array(batch.length)
            .fill('?')
            .join(',')});`);
        const rows = query.all(batch);
        return rows.map(row => (0, util_1.jsonToObject)(row.json));
    });
}
async function _getAllMessages() {
    const db = getInstance();
    const rows = db
        .prepare('SELECT json FROM messages ORDER BY id ASC;')
        .all();
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
async function getAllMessageIds() {
    const db = getInstance();
    const rows = db
        .prepare('SELECT id FROM messages ORDER BY id ASC;')
        .all();
    return rows.map(row => row.id);
}
async function getMessageBySender({ source, sourceUuid, sourceDevice, sent_at, }) {
    const db = getInstance();
    const rows = prepare(db, `
    SELECT json FROM messages WHERE
      (source = $source OR sourceUuid = $sourceUuid) AND
      sourceDevice = $sourceDevice AND
      sent_at = $sent_at;
    `).all({
        source,
        sourceUuid,
        sourceDevice,
        sent_at,
    });
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
async function getUnreadCountForConversation(conversationId) {
    const db = getInstance();
    const row = db
        .prepare(`
      SELECT COUNT(*) AS unreadCount FROM messages
      WHERE readStatus = ${MessageReadStatus_1.ReadStatus.Unread} AND
      conversationId = $conversationId AND
      type = 'incoming';
      `)
        .get({
        conversationId,
    });
    return row.unreadCount;
}
async function getUnreadByConversationAndMarkRead(conversationId, newestUnreadId, readAt) {
    const db = getInstance();
    return db.transaction(() => {
        const expirationStartTimestamp = Math.min(Date.now(), readAt !== null && readAt !== void 0 ? readAt : Infinity);
        db.prepare(`
      UPDATE messages
      INDEXED BY expiring_message_by_conversation_and_received_at
      SET
        expirationStartTimestamp = $expirationStartTimestamp,
        json = json_patch(json, $jsonPatch)
      WHERE
        (
          expirationStartTimestamp IS NULL OR
          expirationStartTimestamp > $expirationStartTimestamp
        ) AND
        expireTimer IS NOT NULL AND
        conversationId = $conversationId AND
        received_at <= $newestUnreadId;
      `).run({
            conversationId,
            expirationStartTimestamp,
            jsonPatch: JSON.stringify({ expirationStartTimestamp }),
            newestUnreadId,
        });
        const rows = db
            .prepare(`
        SELECT id, json FROM messages
        INDEXED BY messages_unread
        WHERE
          readStatus = ${MessageReadStatus_1.ReadStatus.Unread} AND
          conversationId = $conversationId AND
          received_at <= $newestUnreadId
        ORDER BY received_at DESC, sent_at DESC;
        `)
            .all({
            conversationId,
            newestUnreadId,
        });
        db.prepare(`
        UPDATE messages
        SET
          readStatus = ${MessageReadStatus_1.ReadStatus.Read},
          json = json_patch(json, $jsonPatch)
        WHERE
          readStatus = ${MessageReadStatus_1.ReadStatus.Unread} AND
          conversationId = $conversationId AND
          received_at <= $newestUnreadId;
        `).run({
            conversationId,
            jsonPatch: JSON.stringify({ readStatus: MessageReadStatus_1.ReadStatus.Read }),
            newestUnreadId,
        });
        return rows.map(row => {
            const json = (0, util_1.jsonToObject)(row.json);
            return Object.assign({ readStatus: MessageReadStatus_1.ReadStatus.Read }, (0, lodash_1.pick)(json, [
                'expirationStartTimestamp',
                'id',
                'sent_at',
                'source',
                'sourceUuid',
                'type',
            ]));
        });
    })();
}
async function getUnreadReactionsAndMarkRead(conversationId, newestUnreadId) {
    const db = getInstance();
    return db.transaction(() => {
        const unreadMessages = db
            .prepare(`
        SELECT targetAuthorUuid, targetTimestamp, messageId
        FROM reactions WHERE
          unread = 1 AND
          conversationId = $conversationId AND
          messageReceivedAt <= $newestUnreadId;
      `)
            .all({
            conversationId,
            newestUnreadId,
        });
        db.prepare(`
      UPDATE reactions SET
      unread = 0 WHERE
      conversationId = $conversationId AND
      messageReceivedAt <= $newestUnreadId;
    `).run({
            conversationId,
            newestUnreadId,
        });
        return unreadMessages;
    })();
}
async function markReactionAsRead(targetAuthorUuid, targetTimestamp) {
    const db = getInstance();
    return db.transaction(() => {
        const readReaction = db
            .prepare(`
          SELECT *
          FROM reactions
          WHERE
            targetAuthorUuid = $targetAuthorUuid AND
            targetTimestamp = $targetTimestamp AND
            unread = 1
          ORDER BY rowId DESC
          LIMIT 1;
        `)
            .get({
            targetAuthorUuid,
            targetTimestamp,
        });
        db.prepare(`
        UPDATE reactions SET
        unread = 0 WHERE
        targetAuthorUuid = $targetAuthorUuid AND
        targetTimestamp = $targetTimestamp;
      `).run({
            targetAuthorUuid,
            targetTimestamp,
        });
        return readReaction;
    })();
}
async function addReaction({ conversationId, emoji, fromId, messageId, messageReceivedAt, targetAuthorUuid, targetTimestamp, }) {
    const db = getInstance();
    await db
        .prepare(`INSERT INTO reactions (
      conversationId,
      emoji,
      fromId,
      messageId,
      messageReceivedAt,
      targetAuthorUuid,
      targetTimestamp,
      unread
    ) VALUES (
      $conversationId,
      $emoji,
      $fromId,
      $messageId,
      $messageReceivedAt,
      $targetAuthorUuid,
      $targetTimestamp,
      $unread
    );`)
        .run({
        conversationId,
        emoji,
        fromId,
        messageId,
        messageReceivedAt,
        targetAuthorUuid,
        targetTimestamp,
        unread: 1,
    });
}
async function removeReactionFromConversation({ emoji, fromId, targetAuthorUuid, targetTimestamp, }) {
    const db = getInstance();
    await db
        .prepare(`DELETE FROM reactions WHERE
      emoji = $emoji AND
      fromId = $fromId AND
      targetAuthorUuid = $targetAuthorUuid AND
      targetTimestamp = $targetTimestamp;`)
        .run({
        emoji,
        fromId,
        targetAuthorUuid,
        targetTimestamp,
    });
}
async function _getAllReactions() {
    const db = getInstance();
    return db.prepare('SELECT * from reactions;').all();
}
async function getOlderMessagesByConversation(conversationId, { limit = 100, receivedAt = Number.MAX_VALUE, sentAt = Number.MAX_VALUE, messageId, } = {}) {
    const db = getInstance();
    let rows;
    if (messageId) {
        rows = db
            .prepare(`
        SELECT json FROM messages WHERE
          conversationId = $conversationId AND
          id != $messageId AND
          (
            (received_at = $received_at AND sent_at < $sent_at) OR
            received_at < $received_at
          )
        ORDER BY received_at DESC, sent_at DESC
        LIMIT $limit;
        `)
            .all({
            conversationId,
            received_at: receivedAt,
            sent_at: sentAt,
            limit,
            messageId,
        });
    }
    else {
        rows = db
            .prepare(`
        SELECT json FROM messages WHERE
        conversationId = $conversationId AND
        (
          (received_at = $received_at AND sent_at < $sent_at) OR
          received_at < $received_at
        )
        ORDER BY received_at DESC, sent_at DESC
        LIMIT $limit;
        `)
            .all({
            conversationId,
            received_at: receivedAt,
            sent_at: sentAt,
            limit,
        });
    }
    return rows.reverse();
}
async function getNewerMessagesByConversation(conversationId, { limit = 100, receivedAt = 0, sentAt = 0, } = {}) {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json FROM messages WHERE
        conversationId = $conversationId AND
        (
          (received_at = $received_at AND sent_at > $sent_at) OR
          received_at > $received_at
        )
      ORDER BY received_at ASC, sent_at ASC
      LIMIT $limit;
      `)
        .all({
        conversationId,
        received_at: receivedAt,
        sent_at: sentAt,
        limit,
    });
    return rows;
}
function getOldestMessageForConversation(conversationId) {
    const db = getInstance();
    const row = db
        .prepare(`
      SELECT * FROM messages WHERE
        conversationId = $conversationId
      ORDER BY received_at ASC, sent_at ASC
      LIMIT 1;
      `)
        .get({
        conversationId,
    });
    if (!row) {
        return undefined;
    }
    return row;
}
function getNewestMessageForConversation(conversationId) {
    const db = getInstance();
    const row = db
        .prepare(`
      SELECT * FROM messages WHERE
        conversationId = $conversationId
      ORDER BY received_at DESC, sent_at DESC
      LIMIT 1;
      `)
        .get({
        conversationId,
    });
    if (!row) {
        return undefined;
    }
    return row;
}
function getLastConversationActivity({ conversationId, ourUuid, }) {
    const db = getInstance();
    const row = prepare(db, `
      SELECT json FROM messages
      WHERE
        conversationId = $conversationId AND
        (type IS NULL
          OR
          type NOT IN (
            'profile-change',
            'verified-change',
            'message-history-unsynced',
            'keychange',
            'group-v1-migration',
            'universal-timer-notification',
            'change-number-notification'
          )
        ) AND
        (
          json_extract(json, '$.expirationTimerUpdate.fromSync') IS NULL
          OR
          json_extract(json, '$.expirationTimerUpdate.fromSync') != 1
        ) AND NOT
        (
          type = 'group-v2-change' AND
          json_extract(json, '$.groupV2Change.from') != $ourUuid AND
          json_extract(json, '$.groupV2Change.details.length') = 1 AND
          json_extract(json, '$.groupV2Change.details[0].type') = 'member-remove' AND
          json_extract(json, '$.groupV2Change.details[0].uuid') != $ourUuid
        )
      ORDER BY received_at DESC, sent_at DESC
      LIMIT 1;
      `).get({
        conversationId,
        ourUuid,
    });
    if (!row) {
        return undefined;
    }
    return (0, util_1.jsonToObject)(row.json);
}
function getLastConversationPreview({ conversationId, ourUuid, }) {
    const db = getInstance();
    const row = prepare(db, `
      SELECT json FROM messages
      WHERE
        conversationId = $conversationId AND
        (
          expiresAt IS NULL OR
          (expiresAt > $now)
        ) AND
        (
          type IS NULL
          OR
          type NOT IN (
            'profile-change',
            'verified-change',
            'message-history-unsynced',
            'group-v1-migration',
            'universal-timer-notification',
            'change-number-notification'
          )
        ) AND NOT
        (
          type = 'group-v2-change' AND
          json_extract(json, '$.groupV2Change.from') != $ourUuid AND
          json_extract(json, '$.groupV2Change.details.length') = 1 AND
          json_extract(json, '$.groupV2Change.details[0].type') = 'member-remove' AND
          json_extract(json, '$.groupV2Change.details[0].uuid') != $ourUuid
        )
      ORDER BY received_at DESC, sent_at DESC
      LIMIT 1;
      `).get({
        conversationId,
        ourUuid,
        now: Date.now(),
    });
    if (!row) {
        return undefined;
    }
    return (0, util_1.jsonToObject)(row.json);
}
async function getLastConversationMessages({ conversationId, ourUuid, }) {
    const db = getInstance();
    return db.transaction(() => {
        return {
            activity: getLastConversationActivity({
                conversationId,
                ourUuid,
            }),
            preview: getLastConversationPreview({
                conversationId,
                ourUuid,
            }),
            hasUserInitiatedMessages: hasUserInitiatedMessages(conversationId),
        };
    })();
}
function getOldestUnreadMessageForConversation(conversationId) {
    const db = getInstance();
    const row = db
        .prepare(`
      SELECT * FROM messages WHERE
        conversationId = $conversationId AND
        readStatus = ${MessageReadStatus_1.ReadStatus.Unread}
      ORDER BY received_at ASC, sent_at ASC
      LIMIT 1;
      `)
        .get({
        conversationId,
    });
    if (!row) {
        return undefined;
    }
    return row;
}
function getTotalUnreadForConversation(conversationId) {
    const db = getInstance();
    const row = db
        .prepare(`
      SELECT count(id)
      FROM messages
      WHERE
        conversationId = $conversationId AND
        readStatus = ${MessageReadStatus_1.ReadStatus.Unread};
      `)
        .get({
        conversationId,
    });
    if (!row) {
        throw new Error('getTotalUnreadForConversation: Unable to get count');
    }
    return row['count(id)'];
}
async function getMessageMetricsForConversation(conversationId) {
    const oldest = getOldestMessageForConversation(conversationId);
    const newest = getNewestMessageForConversation(conversationId);
    const oldestUnread = getOldestUnreadMessageForConversation(conversationId);
    const totalUnread = getTotalUnreadForConversation(conversationId);
    return {
        oldest: oldest ? (0, lodash_1.pick)(oldest, ['received_at', 'sent_at', 'id']) : undefined,
        newest: newest ? (0, lodash_1.pick)(newest, ['received_at', 'sent_at', 'id']) : undefined,
        oldestUnread: oldestUnread
            ? (0, lodash_1.pick)(oldestUnread, ['received_at', 'sent_at', 'id'])
            : undefined,
        totalUnread,
    };
}
async function hasGroupCallHistoryMessage(conversationId, eraId) {
    const db = getInstance();
    const row = db
        .prepare(`
      SELECT count(*) FROM messages
      WHERE conversationId = $conversationId
      AND type = 'call-history'
      AND json_extract(json, '$.callHistoryDetails.callMode') = 'Group'
      AND json_extract(json, '$.callHistoryDetails.eraId') = $eraId
      LIMIT 1;
      `)
        .get({
        conversationId,
        eraId,
    });
    if (row) {
        return Boolean(row['count(*)']);
    }
    return false;
}
async function migrateConversationMessages(obsoleteId, currentId) {
    const db = getInstance();
    db.prepare(`
    UPDATE messages SET
      conversationId = $currentId,
      json = json_set(json, '$.conversationId', $currentId)
    WHERE conversationId = $obsoleteId;
    `).run({
        obsoleteId,
        currentId,
    });
}
async function getMessagesBySentAt(sentAt) {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json FROM messages
      WHERE sent_at = $sent_at
      ORDER BY received_at DESC, sent_at DESC;
      `)
        .all({
        sent_at: sentAt,
    });
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
async function getExpiredMessages() {
    const db = getInstance();
    const now = Date.now();
    const rows = db
        .prepare(`
      SELECT json FROM messages WHERE
        expiresAt IS NOT NULL AND
        expiresAt <= $now
      ORDER BY expiresAt ASC;
      `)
        .all({ now });
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
async function getMessagesUnexpectedlyMissingExpirationStartTimestamp() {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json FROM messages
      INDEXED BY messages_unexpectedly_missing_expiration_start_timestamp
      WHERE
        expireTimer > 0 AND
        expirationStartTimestamp IS NULL AND
        (
          type IS 'outgoing' OR
          (type IS 'incoming' AND (
            readStatus = ${MessageReadStatus_1.ReadStatus.Read} OR
            readStatus = ${MessageReadStatus_1.ReadStatus.Viewed} OR
            readStatus IS NULL
          ))
        );
      `)
        .all();
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
async function getSoonestMessageExpiry() {
    const db = getInstance();
    // Note: we use `pluck` to only get the first column.
    const result = db
        .prepare(`
      SELECT MIN(expiresAt)
      FROM messages;
      `)
        .pluck(true)
        .get();
    return result || undefined;
}
async function getNextTapToViewMessageTimestampToAgeOut() {
    const db = getInstance();
    const row = db
        .prepare(`
      SELECT json FROM messages
      WHERE
        isViewOnce = 1
        AND (isErased IS NULL OR isErased != 1)
      ORDER BY received_at ASC, sent_at ASC
      LIMIT 1;
      `)
        .get();
    if (!row) {
        return undefined;
    }
    const data = (0, util_1.jsonToObject)(row.json);
    const result = data.received_at_ms || data.received_at;
    return (0, isNormalNumber_1.isNormalNumber)(result) ? result : undefined;
}
async function getTapToViewMessagesNeedingErase() {
    const db = getInstance();
    const THIRTY_DAYS_AGO = Date.now() - 30 * 24 * 60 * 60 * 1000;
    const rows = db
        .prepare(`
      SELECT json
      FROM messages
      WHERE
        isViewOnce = 1
        AND (isErased IS NULL OR isErased != 1)
        AND received_at <= $THIRTY_DAYS_AGO
      ORDER BY received_at ASC, sent_at ASC;
      `)
        .all({
        THIRTY_DAYS_AGO,
    });
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
const MAX_UNPROCESSED_ATTEMPTS = 3;
function saveUnprocessedSync(data) {
    const db = getInstance();
    const { id, timestamp, version, attempts, envelope, source, sourceUuid, sourceDevice, serverGuid, serverTimestamp, decrypted, } = data;
    if (!id) {
        throw new Error('saveUnprocessedSync: id was falsey');
    }
    if (attempts >= MAX_UNPROCESSED_ATTEMPTS) {
        removeUnprocessedSync(id);
        return id;
    }
    prepare(db, `
    INSERT OR REPLACE INTO unprocessed (
      id,
      timestamp,
      version,
      attempts,
      envelope,
      source,
      sourceUuid,
      sourceDevice,
      serverGuid,
      serverTimestamp,
      decrypted
    ) values (
      $id,
      $timestamp,
      $version,
      $attempts,
      $envelope,
      $source,
      $sourceUuid,
      $sourceDevice,
      $serverGuid,
      $serverTimestamp,
      $decrypted
    );
    `).run({
        id,
        timestamp,
        version,
        attempts,
        envelope: envelope || null,
        source: source || null,
        sourceUuid: sourceUuid || null,
        sourceDevice: sourceDevice || null,
        serverGuid: serverGuid || null,
        serverTimestamp: serverTimestamp || null,
        decrypted: decrypted || null,
    });
    return id;
}
function updateUnprocessedWithDataSync(id, data) {
    const db = getInstance();
    const { source, sourceUuid, sourceDevice, serverGuid, serverTimestamp, decrypted, } = data;
    prepare(db, `
    UPDATE unprocessed SET
      source = $source,
      sourceUuid = $sourceUuid,
      sourceDevice = $sourceDevice,
      serverGuid = $serverGuid,
      serverTimestamp = $serverTimestamp,
      decrypted = $decrypted
    WHERE id = $id;
    `).run({
        id,
        source: source || null,
        sourceUuid: sourceUuid || null,
        sourceDevice: sourceDevice || null,
        serverGuid: serverGuid || null,
        serverTimestamp: serverTimestamp || null,
        decrypted: decrypted || null,
    });
}
async function updateUnprocessedWithData(id, data) {
    return updateUnprocessedWithDataSync(id, data);
}
async function updateUnprocessedsWithData(arrayOfUnprocessed) {
    const db = getInstance();
    db.transaction(() => {
        for (const { id, data } of arrayOfUnprocessed) {
            (0, assert_1.assertSync)(updateUnprocessedWithDataSync(id, data));
        }
    })();
}
async function getUnprocessedById(id) {
    const db = getInstance();
    const row = db
        .prepare('SELECT * FROM unprocessed WHERE id = $id;')
        .get({
        id,
    });
    return row;
}
async function getUnprocessedCount() {
    return (0, util_1.getCountFromTable)(getInstance(), 'unprocessed');
}
async function getAllUnprocessed() {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT *
      FROM unprocessed
      ORDER BY timestamp ASC;
      `)
        .all();
    return rows;
}
function removeUnprocessedsSync(ids) {
    const db = getInstance();
    db.prepare(`
    DELETE FROM unprocessed
    WHERE id IN ( ${ids.map(() => '?').join(', ')} );
    `).run(ids);
}
function removeUnprocessedSync(id) {
    const db = getInstance();
    if (!Array.isArray(id)) {
        prepare(db, 'DELETE FROM unprocessed WHERE id = $id;').run({ id });
        return;
    }
    if (!id.length) {
        throw new Error('removeUnprocessedSync: No ids to delete!');
    }
    (0, assert_1.assertSync)((0, util_1.batchMultiVarQuery)(db, id, removeUnprocessedsSync));
}
async function removeUnprocessed(id) {
    removeUnprocessedSync(id);
}
async function removeAllUnprocessed() {
    const db = getInstance();
    db.prepare('DELETE FROM unprocessed;').run();
}
// Attachment Downloads
const ATTACHMENT_DOWNLOADS_TABLE = 'attachment_downloads';
async function getNextAttachmentDownloadJobs(limit, options = {}) {
    const db = getInstance();
    const timestamp = options && options.timestamp ? options.timestamp : Date.now();
    const rows = db
        .prepare(`
      SELECT json
      FROM attachment_downloads
      WHERE pending = 0 AND timestamp <= $timestamp
      ORDER BY timestamp DESC
      LIMIT $limit;
      `)
        .all({
        limit: limit || 3,
        timestamp,
    });
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
async function saveAttachmentDownloadJob(job) {
    const db = getInstance();
    const { id, pending, timestamp } = job;
    if (!id) {
        throw new Error('saveAttachmentDownloadJob: Provided job did not have a truthy id');
    }
    db.prepare(`
    INSERT OR REPLACE INTO attachment_downloads (
      id,
      pending,
      timestamp,
      json
    ) values (
      $id,
      $pending,
      $timestamp,
      $json
    )
    `).run({
        id,
        pending,
        timestamp,
        json: (0, util_1.objectToJSON)(job),
    });
}
async function setAttachmentDownloadJobPending(id, pending) {
    const db = getInstance();
    db.prepare(`
    UPDATE attachment_downloads
    SET pending = $pending
    WHERE id = $id;
    `).run({
        id,
        pending: pending ? 1 : 0,
    });
}
async function resetAttachmentDownloadPending() {
    const db = getInstance();
    db.prepare(`
    UPDATE attachment_downloads
    SET pending = 0
    WHERE pending != 0;
    `).run();
}
async function removeAttachmentDownloadJob(id) {
    return (0, util_1.removeById)(getInstance(), ATTACHMENT_DOWNLOADS_TABLE, id);
}
async function removeAllAttachmentDownloadJobs() {
    return (0, util_1.removeAllFromTable)(getInstance(), ATTACHMENT_DOWNLOADS_TABLE);
}
// Stickers
async function createOrUpdateStickerPack(pack) {
    const db = getInstance();
    const { attemptedStatus, author, coverStickerId, createdAt, downloadAttempts, id, installedAt, key, lastUsed, status, stickerCount, title, } = pack;
    if (!id) {
        throw new Error('createOrUpdateStickerPack: Provided data did not have a truthy id');
    }
    const rows = db
        .prepare(`
      SELECT id
      FROM sticker_packs
      WHERE id = $id;
      `)
        .all({ id });
    const payload = {
        attemptedStatus: attemptedStatus !== null && attemptedStatus !== void 0 ? attemptedStatus : null,
        author,
        coverStickerId,
        createdAt: createdAt || Date.now(),
        downloadAttempts: downloadAttempts || 1,
        id,
        installedAt: installedAt !== null && installedAt !== void 0 ? installedAt : null,
        key,
        lastUsed: lastUsed || null,
        status,
        stickerCount,
        title,
    };
    if (rows && rows.length) {
        db.prepare(`
      UPDATE sticker_packs SET
        attemptedStatus = $attemptedStatus,
        author = $author,
        coverStickerId = $coverStickerId,
        createdAt = $createdAt,
        downloadAttempts = $downloadAttempts,
        installedAt = $installedAt,
        key = $key,
        lastUsed = $lastUsed,
        status = $status,
        stickerCount = $stickerCount,
        title = $title
      WHERE id = $id;
      `).run(payload);
        return;
    }
    db.prepare(`
    INSERT INTO sticker_packs (
      attemptedStatus,
      author,
      coverStickerId,
      createdAt,
      downloadAttempts,
      id,
      installedAt,
      key,
      lastUsed,
      status,
      stickerCount,
      title
    ) values (
      $attemptedStatus,
      $author,
      $coverStickerId,
      $createdAt,
      $downloadAttempts,
      $id,
      $installedAt,
      $key,
      $lastUsed,
      $status,
      $stickerCount,
      $title
    )
    `).run(payload);
}
async function updateStickerPackStatus(id, status, options) {
    const db = getInstance();
    const timestamp = options ? options.timestamp || Date.now() : Date.now();
    const installedAt = status === 'installed' ? timestamp : null;
    db.prepare(`
    UPDATE sticker_packs
    SET status = $status, installedAt = $installedAt
    WHERE id = $id;
    `).run({
        id,
        status,
        installedAt,
    });
}
async function clearAllErrorStickerPackAttempts() {
    const db = getInstance();
    db.prepare(`
    UPDATE sticker_packs
    SET downloadAttempts = 0
    WHERE status = 'error';
    `).run();
}
async function createOrUpdateSticker(sticker) {
    const db = getInstance();
    const { emoji, height, id, isCoverOnly, lastUsed, packId, path, width } = sticker;
    if (!(0, lodash_1.isNumber)(id)) {
        throw new Error('createOrUpdateSticker: Provided data did not have a numeric id');
    }
    if (!packId) {
        throw new Error('createOrUpdateSticker: Provided data did not have a truthy id');
    }
    db.prepare(`
    INSERT OR REPLACE INTO stickers (
      emoji,
      height,
      id,
      isCoverOnly,
      lastUsed,
      packId,
      path,
      width
    ) values (
      $emoji,
      $height,
      $id,
      $isCoverOnly,
      $lastUsed,
      $packId,
      $path,
      $width
    )
    `).run({
        emoji: emoji !== null && emoji !== void 0 ? emoji : null,
        height,
        id,
        isCoverOnly: isCoverOnly ? 1 : 0,
        lastUsed: lastUsed || null,
        packId,
        path,
        width,
    });
}
async function updateStickerLastUsed(packId, stickerId, lastUsed) {
    const db = getInstance();
    db.prepare(`
    UPDATE stickers
    SET lastUsed = $lastUsed
    WHERE id = $id AND packId = $packId;
    `).run({
        id: stickerId,
        packId,
        lastUsed,
    });
    db.prepare(`
    UPDATE sticker_packs
    SET lastUsed = $lastUsed
    WHERE id = $id;
    `).run({
        id: packId,
        lastUsed,
    });
}
async function addStickerPackReference(messageId, packId) {
    const db = getInstance();
    if (!messageId) {
        throw new Error('addStickerPackReference: Provided data did not have a truthy messageId');
    }
    if (!packId) {
        throw new Error('addStickerPackReference: Provided data did not have a truthy packId');
    }
    db.prepare(`
    INSERT OR REPLACE INTO sticker_references (
      messageId,
      packId
    ) values (
      $messageId,
      $packId
    )
    `).run({
        messageId,
        packId,
    });
}
async function deleteStickerPackReference(messageId, packId) {
    const db = getInstance();
    if (!messageId) {
        throw new Error('addStickerPackReference: Provided data did not have a truthy messageId');
    }
    if (!packId) {
        throw new Error('addStickerPackReference: Provided data did not have a truthy packId');
    }
    return db
        .transaction(() => {
        // We use an immediate transaction here to immediately acquire an exclusive lock,
        //   which would normally only happen when we did our first write.
        // We need this to ensure that our five queries are all atomic, with no
        // other changes happening while we do it:
        // 1. Delete our target messageId/packId references
        // 2. Check the number of references still pointing at packId
        // 3. If that number is zero, get pack from sticker_packs database
        // 4. If it's not installed, then grab all of its sticker paths
        // 5. If it's not installed, then sticker pack (which cascades to all
        //    stickers and references)
        db.prepare(`
        DELETE FROM sticker_references
        WHERE messageId = $messageId AND packId = $packId;
        `).run({
            messageId,
            packId,
        });
        const countRow = db
            .prepare(`
          SELECT count(*) FROM sticker_references
          WHERE packId = $packId;
          `)
            .get({ packId });
        if (!countRow) {
            throw new Error('deleteStickerPackReference: Unable to get count of references');
        }
        const count = countRow['count(*)'];
        if (count > 0) {
            return undefined;
        }
        const packRow = db
            .prepare(`
          SELECT status FROM sticker_packs
          WHERE id = $packId;
          `)
            .get({ packId });
        if (!packRow) {
            logger.warn('deleteStickerPackReference: did not find referenced pack');
            return undefined;
        }
        const { status } = packRow;
        if (status === 'installed') {
            return undefined;
        }
        const stickerPathRows = db
            .prepare(`
          SELECT path FROM stickers
          WHERE packId = $packId;
          `)
            .all({
            packId,
        });
        db.prepare(`
        DELETE FROM sticker_packs
        WHERE id = $packId;
        `).run({
            packId,
        });
        return (stickerPathRows || []).map(row => row.path);
    })
        .immediate();
}
async function deleteStickerPack(packId) {
    const db = getInstance();
    if (!packId) {
        throw new Error('deleteStickerPack: Provided data did not have a truthy packId');
    }
    return db
        .transaction(() => {
        // We use an immediate transaction here to immediately acquire an exclusive lock,
        //   which would normally only happen when we did our first write.
        // We need this to ensure that our two queries are atomic, with no other changes
        //   happening while we do it:
        // 1. Grab all of target pack's sticker paths
        // 2. Delete sticker pack (which cascades to all stickers and references)
        const stickerPathRows = db
            .prepare(`
          SELECT path FROM stickers
          WHERE packId = $packId;
          `)
            .all({
            packId,
        });
        db.prepare(`
        DELETE FROM sticker_packs
        WHERE id = $packId;
        `).run({ packId });
        return (stickerPathRows || []).map(row => row.path);
    })
        .immediate();
}
async function getStickerCount() {
    return (0, util_1.getCountFromTable)(getInstance(), 'stickers');
}
async function getAllStickerPacks() {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT * FROM sticker_packs
      ORDER BY installedAt DESC, createdAt DESC
      `)
        .all();
    return rows || [];
}
async function getAllStickers() {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT * FROM stickers
      ORDER BY packId ASC, id ASC
      `)
        .all();
    return (rows || []).map(row => rowToSticker(row));
}
async function getRecentStickers({ limit } = {}) {
    const db = getInstance();
    // Note: we avoid 'IS NOT NULL' here because it does seem to bypass our index
    const rows = db
        .prepare(`
      SELECT stickers.* FROM stickers
      JOIN sticker_packs on stickers.packId = sticker_packs.id
      WHERE stickers.lastUsed > 0 AND sticker_packs.status = 'installed'
      ORDER BY stickers.lastUsed DESC
      LIMIT $limit
      `)
        .all({
        limit: limit || 24,
    });
    return (rows || []).map(row => rowToSticker(row));
}
// Emojis
async function updateEmojiUsage(shortName, timeUsed = Date.now()) {
    const db = getInstance();
    db.transaction(() => {
        const rows = db
            .prepare(`
        SELECT * FROM emojis
        WHERE shortName = $shortName;
        `)
            .get({
            shortName,
        });
        if (rows) {
            db.prepare(`
        UPDATE emojis
        SET lastUsage = $timeUsed
        WHERE shortName = $shortName;
        `).run({ shortName, timeUsed });
        }
        else {
            db.prepare(`
        INSERT INTO emojis(shortName, lastUsage)
        VALUES ($shortName, $timeUsed);
        `).run({ shortName, timeUsed });
        }
    })();
}
async function getRecentEmojis(limit = 32) {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT *
      FROM emojis
      ORDER BY lastUsage DESC
      LIMIT $limit;
      `)
        .all({ limit });
    return rows || [];
}
async function getAllBadges() {
    const db = getInstance();
    const [badgeRows, badgeImageFileRows] = db.transaction(() => [
        db.prepare('SELECT * FROM badges').all(),
        db.prepare('SELECT * FROM badgeImageFiles').all(),
    ])();
    const badgeImagesByBadge = new Map();
    for (const badgeImageFileRow of badgeImageFileRows) {
        const { badgeId, order, localPath, url, theme } = badgeImageFileRow;
        const badgeImages = badgeImagesByBadge.get(badgeId) || [];
        badgeImages[order] = Object.assign(Object.assign({}, (badgeImages[order] || {})), { [(0, BadgeImageTheme_1.parseBadgeImageTheme)(theme)]: {
                localPath: (0, dropNull_1.dropNull)(localPath),
                url,
            } });
        badgeImagesByBadge.set(badgeId, badgeImages);
    }
    return badgeRows.map(badgeRow => ({
        id: badgeRow.id,
        category: (0, BadgeCategory_1.parseBadgeCategory)(badgeRow.category),
        name: badgeRow.name,
        descriptionTemplate: badgeRow.descriptionTemplate,
        images: (badgeImagesByBadge.get(badgeRow.id) || []).filter(isNotNil_1.isNotNil),
    }));
}
// This should match the logic in the badges Redux reducer.
async function updateOrCreateBadges(badges) {
    const db = getInstance();
    const insertBadge = prepare(db, `
    INSERT OR REPLACE INTO badges (
      id,
      category,
      name,
      descriptionTemplate
    ) VALUES (
      $id,
      $category,
      $name,
      $descriptionTemplate
    );
    `);
    const getImageFilesForBadge = prepare(db, 'SELECT url, localPath FROM badgeImageFiles WHERE badgeId = $badgeId');
    const insertBadgeImageFile = prepare(db, `
    INSERT INTO badgeImageFiles (
      badgeId,
      'order',
      url,
      localPath,
      theme
    ) VALUES (
      $badgeId,
      $order,
      $url,
      $localPath,
      $theme
    );
    `);
    db.transaction(() => {
        badges.forEach(badge => {
            const { id: badgeId } = badge;
            const oldLocalPaths = new Map();
            for (const { url, localPath } of getImageFilesForBadge.all({ badgeId })) {
                if (localPath) {
                    oldLocalPaths.set(url, localPath);
                }
            }
            insertBadge.run({
                id: badgeId,
                category: badge.category,
                name: badge.name,
                descriptionTemplate: badge.descriptionTemplate,
            });
            for (const [order, image] of badge.images.entries()) {
                for (const [theme, imageFile] of Object.entries(image)) {
                    insertBadgeImageFile.run({
                        badgeId,
                        localPath: imageFile.localPath || oldLocalPaths.get(imageFile.url) || null,
                        order,
                        theme,
                        url: imageFile.url,
                    });
                }
            }
        });
    })();
}
async function badgeImageFileDownloaded(url, localPath) {
    const db = getInstance();
    prepare(db, 'UPDATE badgeImageFiles SET localPath = $localPath WHERE url = $url').run({ url, localPath });
}
async function getAllBadgeImageFileLocalPaths() {
    const db = getInstance();
    const localPaths = db
        .prepare('SELECT localPath FROM badgeImageFiles WHERE localPath IS NOT NULL')
        .pluck()
        .all();
    return new Set(localPaths);
}
// All data in database
async function removeAll() {
    const db = getInstance();
    db.transaction(() => {
        db.exec(`
      DELETE FROM badges;
      DELETE FROM badgeImageFiles;
      DELETE FROM conversations;
      DELETE FROM identityKeys;
      DELETE FROM items;
      DELETE FROM messages;
      DELETE FROM preKeys;
      DELETE FROM senderKeys;
      DELETE FROM sessions;
      DELETE FROM signedPreKeys;
      DELETE FROM unprocessed;
      DELETE FROM attachment_downloads;
      DELETE FROM messages_fts;
      DELETE FROM stickers;
      DELETE FROM sticker_packs;
      DELETE FROM sticker_references;
      DELETE FROM jobs;
    `);
    })();
}
// Anything that isn't user-visible data
async function removeAllConfiguration(mode = RemoveAllConfiguration_1.RemoveAllConfiguration.Full) {
    const db = getInstance();
    db.transaction(() => {
        db.exec(`
      DELETE FROM identityKeys;
      DELETE FROM preKeys;
      DELETE FROM senderKeys;
      DELETE FROM sessions;
      DELETE FROM signedPreKeys;
      DELETE FROM unprocessed;
      DELETE FROM jobs;
      `);
        if (mode === RemoveAllConfiguration_1.RemoveAllConfiguration.Full) {
            db.exec(`
        DELETE FROM items;
        `);
        }
        else if (mode === RemoveAllConfiguration_1.RemoveAllConfiguration.Soft) {
            const itemIds = db
                .prepare('SELECT id FROM items')
                .pluck(true)
                .all();
            const allowedSet = new Set(StorageUIKeys_1.STORAGE_UI_KEYS);
            for (const id of itemIds) {
                if (!allowedSet.has(id)) {
                    (0, util_1.removeById)(db, 'items', id);
                }
            }
        }
        else {
            throw (0, missingCaseError_1.missingCaseError)(mode);
        }
        db.exec("UPDATE conversations SET json = json_remove(json, '$.senderKeyInfo');");
    })();
}
async function getMessagesNeedingUpgrade(limit, { maxVersion }) {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json
      FROM messages
      WHERE schemaVersion IS NULL OR schemaVersion < $maxVersion
      LIMIT $limit;
      `)
        .all({
        maxVersion,
        limit,
    });
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
async function getMessagesWithVisualMediaAttachments(conversationId, { limit }) {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json FROM messages WHERE
        conversationId = $conversationId AND
        hasVisualMediaAttachments = 1
      ORDER BY received_at DESC, sent_at DESC
      LIMIT $limit;
      `)
        .all({
        conversationId,
        limit,
    });
    return rows.map(row => (0, util_1.jsonToObject)(row.json));
}
async function getMessagesWithFileAttachments(conversationId, { limit }) {
    const db = getInstance();
    const rows = db
        .prepare(`
      SELECT json FROM messages WHERE
        conversationId = $conversationId AND
        hasFileAttachments = 1
      ORDER BY received_at DESC, sent_at DESC
      LIMIT $limit;
      `)
        .all({
        conversationId,
        limit,
    });
    return (0, lodash_1.map)(rows, row => (0, util_1.jsonToObject)(row.json));
}
async function getMessageServerGuidsForSpam(conversationId) {
    const db = getInstance();
    // The server's maximum is 3, which is why you see `LIMIT 3` in this query. Note that we
    //   use `pluck` here to only get the first column!
    return db
        .prepare(`
      SELECT serverGuid
      FROM messages
      WHERE conversationId = $conversationId
      AND type = 'incoming'
      AND serverGuid IS NOT NULL
      ORDER BY received_at DESC, sent_at DESC
      LIMIT 3;
      `)
        .pluck(true)
        .all({ conversationId });
}
function getExternalFilesForMessage(message) {
    const { attachments, contact, quote, preview, sticker } = message;
    const files = [];
    (0, lodash_1.forEach)(attachments, attachment => {
        const { path: file, thumbnail, screenshot } = attachment;
        if (file) {
            files.push(file);
        }
        if (thumbnail && thumbnail.path) {
            files.push(thumbnail.path);
        }
        if (screenshot && screenshot.path) {
            files.push(screenshot.path);
        }
    });
    if (quote && quote.attachments && quote.attachments.length) {
        (0, lodash_1.forEach)(quote.attachments, attachment => {
            const { thumbnail } = attachment;
            if (thumbnail && thumbnail.path) {
                files.push(thumbnail.path);
            }
        });
    }
    if (contact && contact.length) {
        (0, lodash_1.forEach)(contact, item => {
            const { avatar } = item;
            if (avatar && avatar.avatar && avatar.avatar.path) {
                files.push(avatar.avatar.path);
            }
        });
    }
    if (preview && preview.length) {
        (0, lodash_1.forEach)(preview, item => {
            const { image } = item;
            if (image && image.path) {
                files.push(image.path);
            }
        });
    }
    if (sticker && sticker.data && sticker.data.path) {
        files.push(sticker.data.path);
        if (sticker.data.thumbnail && sticker.data.thumbnail.path) {
            files.push(sticker.data.thumbnail.path);
        }
    }
    return files;
}
function getExternalFilesForConversation(conversation) {
    const { avatar, profileAvatar } = conversation;
    const files = [];
    if (avatar && avatar.path) {
        files.push(avatar.path);
    }
    if (profileAvatar && profileAvatar.path) {
        files.push(profileAvatar.path);
    }
    return files;
}
function getExternalDraftFilesForConversation(conversation) {
    const draftAttachments = conversation.draftAttachments || [];
    const files = [];
    (0, lodash_1.forEach)(draftAttachments, attachment => {
        if (attachment.pending) {
            return;
        }
        const { path: file, screenshotPath } = attachment;
        if (file) {
            files.push(file);
        }
        if (screenshotPath) {
            files.push(screenshotPath);
        }
    });
    return files;
}
async function removeKnownAttachments(allAttachments) {
    const db = getInstance();
    const lookup = (0, lodash_1.fromPairs)((0, lodash_1.map)(allAttachments, file => [file, true]));
    const chunkSize = 500;
    const total = getMessageCountSync();
    logger.info(`removeKnownAttachments: About to iterate through ${total} messages`);
    let count = 0;
    for (const message of new util_1.TableIterator(db, 'messages')) {
        const externalFiles = getExternalFilesForMessage(message);
        (0, lodash_1.forEach)(externalFiles, file => {
            delete lookup[file];
        });
        count += 1;
    }
    logger.info(`removeKnownAttachments: Done processing ${count} messages`);
    let complete = false;
    count = 0;
    let id = '';
    const conversationTotal = await getConversationCount();
    logger.info(`removeKnownAttachments: About to iterate through ${conversationTotal} conversations`);
    const fetchConversations = db.prepare(`
      SELECT json FROM conversations
      WHERE id > $id
      ORDER BY id ASC
      LIMIT $chunkSize;
    `);
    while (!complete) {
        const rows = fetchConversations.all({
            id,
            chunkSize,
        });
        const conversations = (0, lodash_1.map)(rows, row => (0, util_1.jsonToObject)(row.json));
        conversations.forEach(conversation => {
            const externalFiles = getExternalFilesForConversation(conversation);
            externalFiles.forEach(file => {
                delete lookup[file];
            });
        });
        const lastMessage = (0, lodash_1.last)(conversations);
        if (lastMessage) {
            ({ id } = lastMessage);
        }
        complete = conversations.length < chunkSize;
        count += conversations.length;
    }
    logger.info(`removeKnownAttachments: Done processing ${count} conversations`);
    return Object.keys(lookup);
}
async function removeKnownStickers(allStickers) {
    const db = getInstance();
    const lookup = (0, lodash_1.fromPairs)((0, lodash_1.map)(allStickers, file => [file, true]));
    const chunkSize = 50;
    const total = await getStickerCount();
    logger.info(`removeKnownStickers: About to iterate through ${total} stickers`);
    let count = 0;
    let complete = false;
    let rowid = 0;
    while (!complete) {
        const rows = db
            .prepare(`
        SELECT rowid, path FROM stickers
        WHERE rowid > $rowid
        ORDER BY rowid ASC
        LIMIT $chunkSize;
        `)
            .all({
            rowid,
            chunkSize,
        });
        const files = rows.map(row => row.path);
        files.forEach(file => {
            delete lookup[file];
        });
        const lastSticker = (0, lodash_1.last)(rows);
        if (lastSticker) {
            ({ rowid } = lastSticker);
        }
        complete = rows.length < chunkSize;
        count += rows.length;
    }
    logger.info(`removeKnownStickers: Done processing ${count} stickers`);
    return Object.keys(lookup);
}
async function removeKnownDraftAttachments(allStickers) {
    const db = getInstance();
    const lookup = (0, lodash_1.fromPairs)((0, lodash_1.map)(allStickers, file => [file, true]));
    const chunkSize = 50;
    const total = await getConversationCount();
    logger.info(`removeKnownDraftAttachments: About to iterate through ${total} conversations`);
    let complete = false;
    let count = 0;
    // Though conversations.id is a string, this ensures that, when coerced, this
    //   value is still a string but it's smaller than every other string.
    let id = 0;
    while (!complete) {
        const rows = db
            .prepare(`
        SELECT json FROM conversations
        WHERE id > $id
        ORDER BY id ASC
        LIMIT $chunkSize;
        `)
            .all({
            id,
            chunkSize,
        });
        const conversations = rows.map(row => (0, util_1.jsonToObject)(row.json));
        conversations.forEach(conversation => {
            const externalFiles = getExternalDraftFilesForConversation(conversation);
            externalFiles.forEach(file => {
                delete lookup[file];
            });
        });
        const lastMessage = (0, lodash_1.last)(conversations);
        if (lastMessage) {
            ({ id } = lastMessage);
        }
        complete = conversations.length < chunkSize;
        count += conversations.length;
    }
    logger.info(`removeKnownDraftAttachments: Done processing ${count} conversations`);
    return Object.keys(lookup);
}
async function getJobsInQueue(queueType) {
    const db = getInstance();
    return db
        .prepare(`
      SELECT id, timestamp, data
      FROM jobs
      WHERE queueType = $queueType
      ORDER BY timestamp;
      `)
        .all({ queueType })
        .map(row => ({
        id: row.id,
        queueType,
        timestamp: row.timestamp,
        data: (0, isNotNil_1.isNotNil)(row.data) ? JSON.parse(row.data) : undefined,
    }));
}
function insertJobSync(db, job) {
    db.prepare(`
      INSERT INTO jobs
      (id, queueType, timestamp, data)
      VALUES
      ($id, $queueType, $timestamp, $data);
    `).run({
        id: job.id,
        queueType: job.queueType,
        timestamp: job.timestamp,
        data: (0, isNotNil_1.isNotNil)(job.data) ? JSON.stringify(job.data) : null,
    });
}
async function insertJob(job) {
    const db = getInstance();
    return insertJobSync(db, job);
}
async function deleteJob(id) {
    const db = getInstance();
    db.prepare('DELETE FROM jobs WHERE id = $id').run({ id });
}
async function processGroupCallRingRequest(ringId) {
    const db = getInstance();
    return db.transaction(() => {
        let result;
        const wasRingPreviouslyCanceled = Boolean(db
            .prepare(`
          SELECT 1 FROM groupCallRings
          WHERE ringId = $ringId AND isActive = 0
          LIMIT 1;
          `)
            .pluck(true)
            .get({ ringId }));
        if (wasRingPreviouslyCanceled) {
            result = Calling_1.ProcessGroupCallRingRequestResult.RingWasPreviouslyCanceled;
        }
        else {
            const isThereAnotherActiveRing = Boolean(db
                .prepare(`
            SELECT 1 FROM groupCallRings
            WHERE isActive = 1
            LIMIT 1;
            `)
                .pluck(true)
                .get());
            if (isThereAnotherActiveRing) {
                result = Calling_1.ProcessGroupCallRingRequestResult.ThereIsAnotherActiveRing;
            }
            else {
                result = Calling_1.ProcessGroupCallRingRequestResult.ShouldRing;
            }
            db.prepare(`
        INSERT OR IGNORE INTO groupCallRings (ringId, isActive, createdAt)
        VALUES ($ringId, 1, $createdAt);
        `);
        }
        return result;
    })();
}
async function processGroupCallRingCancelation(ringId) {
    const db = getInstance();
    db.prepare(`
    INSERT INTO groupCallRings (ringId, isActive, createdAt)
    VALUES ($ringId, 0, $createdAt)
    ON CONFLICT (ringId) DO
    UPDATE SET isActive = 0;
    `).run({ ringId, createdAt: Date.now() });
}
// This age, in milliseconds, should be longer than any group call ring duration. Beyond
//   that, it doesn't really matter what the value is.
const MAX_GROUP_CALL_RING_AGE = 30 * durations.MINUTE;
async function cleanExpiredGroupCallRings() {
    const db = getInstance();
    db.prepare(`
    DELETE FROM groupCallRings
    WHERE createdAt < $expiredRingTime;
    `).run({
        expiredRingTime: Date.now() - MAX_GROUP_CALL_RING_AGE,
    });
}
async function getMaxMessageCounter() {
    const db = getInstance();
    return db
        .prepare(`
    SELECT MAX(counter)
    FROM
      (
        SELECT MAX(received_at) AS counter FROM messages
        UNION
        SELECT MAX(timestamp) AS counter FROM unprocessed
      )
    `)
        .pluck()
        .get();
}
async function getStatisticsForLogging() {
    const db = getInstance();
    const counts = await (0, p_props_1.default)({
        messageCount: getMessageCount(),
        conversationCount: getConversationCount(),
        sessionCount: (0, util_1.getCountFromTable)(db, 'sessions'),
        senderKeyCount: (0, util_1.getCountFromTable)(db, 'senderKeys'),
    });
    return (0, lodash_1.mapValues)(counts, formatCountForLogging_1.formatCountForLogging);
}
async function updateAllConversationColors(conversationColor, customColorData) {
    const db = getInstance();
    db.prepare(`
    UPDATE conversations
    SET json = JSON_PATCH(json, $patch);
    `).run({
        patch: JSON.stringify({
            conversationColor: conversationColor || null,
            customColor: (customColorData === null || customColorData === void 0 ? void 0 : customColorData.value) || null,
            customColorId: (customColorData === null || customColorData === void 0 ? void 0 : customColorData.id) || null,
        }),
    });
}
