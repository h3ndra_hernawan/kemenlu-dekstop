"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.StickerPackStatuses = void 0;
exports.StickerPackStatuses = [
    'known',
    'ephemeral',
    'downloaded',
    'installed',
    'pending',
    'error',
];
