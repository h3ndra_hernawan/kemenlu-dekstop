"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isCorruptionError = void 0;
function isCorruptionError(error) {
    var _a, _b;
    return (((_a = error === null || error === void 0 ? void 0 : error.message) === null || _a === void 0 ? void 0 : _a.includes('SQLITE_CORRUPT')) ||
        ((_b = error === null || error === void 0 ? void 0 : error.message) === null || _b === void 0 ? void 0 : _b.includes('database disk image is malformed')) ||
        false);
}
exports.isCorruptionError = isCorruptionError;
