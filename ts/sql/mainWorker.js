"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const worker_threads_1 = require("worker_threads");
const Server_1 = __importDefault(require("./Server"));
if (!worker_threads_1.parentPort) {
    throw new Error('Must run as a worker thread');
}
const port = worker_threads_1.parentPort;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function respond(seq, error, response) {
    const corruptionLog = Server_1.default.getCorruptionLog();
    const errorMessage = [
        ...(error ? [error.stack] : []),
        ...(corruptionLog ? [corruptionLog] : []),
    ].join('\n');
    const wrappedResponse = {
        type: 'response',
        seq,
        error: errorMessage,
        response,
    };
    port.postMessage(wrappedResponse);
}
const log = (level, args) => {
    const wrappedResponse = {
        type: 'log',
        level,
        args,
    };
    port.postMessage(wrappedResponse);
};
const logger = {
    fatal(...args) {
        log('fatal', args);
    },
    error(...args) {
        log('error', args);
    },
    warn(...args) {
        log('warn', args);
    },
    info(...args) {
        log('info', args);
    },
    debug(...args) {
        log('debug', args);
    },
    trace(...args) {
        log('trace', args);
    },
};
port.on('message', async ({ seq, request }) => {
    try {
        if (request.type === 'init') {
            await Server_1.default.initialize(Object.assign(Object.assign({}, request.options), { logger }));
            respond(seq, undefined, undefined);
            return;
        }
        if (request.type === 'close') {
            await Server_1.default.close();
            respond(seq, undefined, undefined);
            process.exit(0);
            return;
        }
        if (request.type === 'removeDB') {
            await Server_1.default.removeDB();
            respond(seq, undefined, undefined);
            return;
        }
        if (request.type === 'sqlCall') {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const method = Server_1.default[request.method];
            if (typeof method !== 'function') {
                throw new Error(`Invalid sql method: ${method}`);
            }
            const start = Date.now();
            const result = await method.apply(Server_1.default, request.args);
            const end = Date.now();
            respond(seq, undefined, { result, duration: end - start });
        }
        else {
            throw new Error('Unexpected request type');
        }
    }
    catch (error) {
        respond(seq, error, undefined);
    }
});
