"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainSQL = void 0;
const path_1 = require("path");
const worker_threads_1 = require("worker_threads");
const util_1 = require("util");
const assert_1 = require("../util/assert");
const explodePromise_1 = require("../util/explodePromise");
const errors_1 = require("./errors");
const ASAR_PATTERN = /app\.asar$/;
const MIN_TRACE_DURATION = 40;
class MainSQL {
    constructor() {
        this.isReady = false;
        this.seq = 0;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.onResponse = new Map();
        let appDir = (0, path_1.join)(__dirname, '..', '..');
        let isBundled = false;
        if (ASAR_PATTERN.test(appDir)) {
            appDir = appDir.replace(ASAR_PATTERN, 'app.asar.unpacked');
            isBundled = true;
        }
        const scriptDir = (0, path_1.join)(appDir, 'ts', 'sql');
        this.worker = new worker_threads_1.Worker((0, path_1.join)(scriptDir, isBundled ? 'mainWorker.bundle.js' : 'mainWorker.js'));
        const { promise: onCorruption, resolve: resolveCorruption } = (0, explodePromise_1.explodePromise)();
        this.onCorruption = onCorruption;
        this.worker.on('message', (wrappedResponse) => {
            if (wrappedResponse.type === 'log') {
                const { level, args } = wrappedResponse;
                (0, assert_1.strictAssert)(this.logger !== undefined, 'Logger not initialized');
                this.logger[level](`MainSQL: ${(0, util_1.format)(...args)}`);
                return;
            }
            const { seq, error, response } = wrappedResponse;
            const pair = this.onResponse.get(seq);
            this.onResponse.delete(seq);
            if (!pair) {
                throw new Error(`Unexpected worker response with seq: ${seq}`);
            }
            if (error) {
                const errorObj = new Error(error);
                if ((0, errors_1.isCorruptionError)(errorObj)) {
                    resolveCorruption(errorObj);
                }
                pair.reject(errorObj);
            }
            else {
                pair.resolve(response);
            }
        });
        this.onExit = new Promise(resolve => {
            this.worker.once('exit', resolve);
        });
    }
    async initialize({ configDir, key, logger, }) {
        if (this.isReady || this.onReady) {
            throw new Error('Already initialized');
        }
        this.logger = logger;
        this.onReady = this.send({
            type: 'init',
            options: { configDir, key },
        });
        await this.onReady;
        this.onReady = undefined;
        this.isReady = true;
    }
    whenCorrupted() {
        return this.onCorruption;
    }
    async close() {
        if (!this.isReady) {
            throw new Error('Not initialized');
        }
        await this.send({ type: 'close' });
        await this.onExit;
    }
    async removeDB() {
        await this.send({ type: 'removeDB' });
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async sqlCall(method, args) {
        if (this.onReady) {
            await this.onReady;
        }
        if (!this.isReady) {
            throw new Error('Not initialized');
        }
        const { result, duration } = await this.send({
            type: 'sqlCall',
            method,
            args,
        });
        if (duration > MIN_TRACE_DURATION) {
            (0, assert_1.strictAssert)(this.logger !== undefined, 'Logger not initialized');
            this.logger.info(`MainSQL: slow query ${method} duration=${duration}ms`);
        }
        return result;
    }
    async send(request) {
        const { seq } = this;
        this.seq += 1;
        const result = new Promise((resolve, reject) => {
            this.onResponse.set(seq, { resolve, reject });
        });
        const wrappedRequest = {
            seq,
            request,
        };
        this.worker.postMessage(wrappedRequest);
        return result;
    }
}
exports.MainSQL = MainSQL;
