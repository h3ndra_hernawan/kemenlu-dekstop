"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-await-in-loop */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-types */
const electron_1 = require("electron");
const fs_extra_1 = __importDefault(require("fs-extra"));
const pify_1 = __importDefault(require("pify"));
const lodash_1 = require("lodash");
const Bytes = __importStar(require("../Bytes"));
const message_1 = require("../../js/modules/types/message");
const batcher_1 = require("../util/batcher");
const assert_1 = require("../util/assert");
const cleanDataForIpc_1 = require("./cleanDataForIpc");
const TaskWithTimeout_1 = __importDefault(require("../textsecure/TaskWithTimeout"));
const log = __importStar(require("../logging/log"));
const formatJobForInsert_1 = require("../jobs/formatJobForInsert");
const Server_1 = __importDefault(require("./Server"));
const errors_1 = require("./errors");
// We listen to a lot of events on ipc, often on the same channel. This prevents
//   any warnings that might be sent to the console in that case.
if (electron_1.ipcRenderer && electron_1.ipcRenderer.setMaxListeners) {
    electron_1.ipcRenderer.setMaxListeners(0);
}
else {
    log.warn('sql/Client: ipc is not available!');
}
const getRealPath = (0, pify_1.default)(fs_extra_1.default.realpath);
const MIN_TRACE_DURATION = 10;
const SQL_CHANNEL_KEY = 'sql-channel';
const ERASE_SQL_KEY = 'erase-sql-key';
const ERASE_ATTACHMENTS_KEY = 'erase-attachments';
const ERASE_STICKERS_KEY = 'erase-stickers';
const ERASE_TEMP_KEY = 'erase-temp';
const ERASE_DRAFTS_KEY = 'erase-drafts';
const CLEANUP_ORPHANED_ATTACHMENTS_KEY = 'cleanup-orphaned-attachments';
const ENSURE_FILE_PERMISSIONS = 'ensure-file-permissions';
var RendererState;
(function (RendererState) {
    RendererState["InMain"] = "InMain";
    RendererState["Opening"] = "Opening";
    RendererState["InRenderer"] = "InRenderer";
    RendererState["Closing"] = "Closing";
})(RendererState || (RendererState = {}));
const _jobs = Object.create(null);
const _DEBUG = false;
let _jobCounter = 0;
let _shuttingDown = false;
let _shutdownCallback = null;
let _shutdownPromise = null;
let state = RendererState.InMain;
const startupQueries = new Map();
// Because we can't force this module to conform to an interface, we narrow our exports
//   to this one default export, which does conform to the interface.
// Note: In Javascript, you need to access the .default property when requiring it
// https://github.com/microsoft/TypeScript/issues/420
const dataInterface = {
    close,
    removeDB,
    removeIndexedDBFiles,
    createOrUpdateIdentityKey,
    getIdentityKeyById,
    bulkAddIdentityKeys,
    removeIdentityKeyById,
    removeAllIdentityKeys,
    getAllIdentityKeys,
    createOrUpdatePreKey,
    getPreKeyById,
    bulkAddPreKeys,
    removePreKeyById,
    removeAllPreKeys,
    getAllPreKeys,
    createOrUpdateSignedPreKey,
    getSignedPreKeyById,
    getAllSignedPreKeys,
    bulkAddSignedPreKeys,
    removeSignedPreKeyById,
    removeAllSignedPreKeys,
    createOrUpdateItem,
    getItemById,
    getAllItems,
    removeItemById,
    removeAllItems,
    createOrUpdateSenderKey,
    getSenderKeyById,
    removeAllSenderKeys,
    getAllSenderKeys,
    removeSenderKeyById,
    insertSentProto,
    deleteSentProtosOlderThan,
    deleteSentProtoByMessageId,
    insertProtoRecipients,
    deleteSentProtoRecipient,
    getSentProtoByRecipient,
    removeAllSentProtos,
    getAllSentProtos,
    _getAllSentProtoRecipients,
    _getAllSentProtoMessageIds,
    createOrUpdateSession,
    createOrUpdateSessions,
    commitSessionsAndUnprocessed,
    bulkAddSessions,
    removeSessionById,
    removeSessionsByConversation,
    removeAllSessions,
    getAllSessions,
    getConversationCount,
    saveConversation,
    saveConversations,
    getConversationById,
    updateConversation,
    updateConversations,
    removeConversation,
    updateAllConversationColors,
    eraseStorageServiceStateFromConversations,
    getAllConversations,
    getAllConversationIds,
    getAllPrivateConversations,
    getAllGroupsInvolvingUuid,
    searchConversations,
    searchMessages,
    searchMessagesInConversation,
    getMessageCount,
    saveMessage,
    saveMessages,
    removeMessage,
    removeMessages,
    getUnreadCountForConversation,
    getUnreadByConversationAndMarkRead,
    getUnreadReactionsAndMarkRead,
    markReactionAsRead,
    removeReactionFromConversation,
    addReaction,
    _getAllReactions,
    getMessageBySender,
    getMessageById,
    getMessagesById,
    getAllMessageIds,
    getMessagesBySentAt,
    getExpiredMessages,
    getMessagesUnexpectedlyMissingExpirationStartTimestamp,
    getSoonestMessageExpiry,
    getNextTapToViewMessageTimestampToAgeOut,
    getTapToViewMessagesNeedingErase,
    getOlderMessagesByConversation,
    getNewerMessagesByConversation,
    getLastConversationMessages,
    getMessageMetricsForConversation,
    hasGroupCallHistoryMessage,
    migrateConversationMessages,
    getUnprocessedCount,
    getAllUnprocessed,
    getUnprocessedById,
    updateUnprocessedWithData,
    updateUnprocessedsWithData,
    removeUnprocessed,
    removeAllUnprocessed,
    getNextAttachmentDownloadJobs,
    saveAttachmentDownloadJob,
    resetAttachmentDownloadPending,
    setAttachmentDownloadJobPending,
    removeAttachmentDownloadJob,
    removeAllAttachmentDownloadJobs,
    getStickerCount,
    createOrUpdateStickerPack,
    updateStickerPackStatus,
    createOrUpdateSticker,
    updateStickerLastUsed,
    addStickerPackReference,
    deleteStickerPackReference,
    deleteStickerPack,
    getAllStickerPacks,
    getAllStickers,
    getRecentStickers,
    clearAllErrorStickerPackAttempts,
    updateEmojiUsage,
    getRecentEmojis,
    getAllBadges,
    updateOrCreateBadges,
    badgeImageFileDownloaded,
    removeAll,
    removeAllConfiguration,
    getMessagesNeedingUpgrade,
    getMessagesWithVisualMediaAttachments,
    getMessagesWithFileAttachments,
    getMessageServerGuidsForSpam,
    getJobsInQueue,
    insertJob,
    deleteJob,
    processGroupCallRingRequest,
    processGroupCallRingCancelation,
    cleanExpiredGroupCallRings,
    getMaxMessageCounter,
    getStatisticsForLogging,
    // Test-only
    _getAllMessages,
    // Client-side only
    shutdown,
    removeAllMessagesInConversation,
    removeOtherData,
    cleanupOrphanedAttachments,
    ensureFilePermissions,
    // Client-side only, and test-only
    startInRendererProcess,
    goBackToMainProcess,
    _removeConversations,
    _jobs,
};
exports.default = dataInterface;
async function startInRendererProcess(isTesting = false) {
    (0, assert_1.strictAssert)(state === RendererState.InMain, `startInRendererProcess: expected ${state} to be ${RendererState.InMain}`);
    log.info('data.startInRendererProcess: switching to renderer process');
    state = RendererState.Opening;
    if (!isTesting) {
        electron_1.ipcRenderer.send('database-ready');
        await new Promise(resolve => {
            electron_1.ipcRenderer.once('database-ready', () => {
                resolve();
            });
        });
    }
    const configDir = await getRealPath(electron_1.ipcRenderer.sendSync('get-user-data-path'));
    const key = electron_1.ipcRenderer.sendSync('user-config-key');
    await Server_1.default.initializeRenderer({ configDir, key });
    log.info('data.startInRendererProcess: switched to renderer process');
    state = RendererState.InRenderer;
}
async function goBackToMainProcess() {
    if (state === RendererState.InMain) {
        log.info('goBackToMainProcess: Already in the main process');
        return;
    }
    (0, assert_1.strictAssert)(state === RendererState.InRenderer, `goBackToMainProcess: expected ${state} to be ${RendererState.InRenderer}`);
    // We don't need to wait for pending queries since they are synchronous.
    log.info('data.goBackToMainProcess: switching to main process');
    const closePromise = close();
    // It should be the last query we run in renderer process
    state = RendererState.Closing;
    await closePromise;
    state = RendererState.InMain;
    // Print query statistics for whole startup
    const entries = Array.from(startupQueries.entries());
    startupQueries.clear();
    // Sort by decreasing duration
    entries
        .sort((a, b) => b[1] - a[1])
        .filter(([_, duration]) => duration > MIN_TRACE_DURATION)
        .forEach(([query, duration]) => {
        log.info(`startup query: ${query} ${duration}ms`);
    });
    log.info('data.goBackToMainProcess: switched to main process');
}
const channelsAsUnknown = (0, lodash_1.fromPairs)((0, lodash_1.compact)((0, lodash_1.map)((0, lodash_1.toPairs)(dataInterface), ([name, value]) => {
    if ((0, lodash_1.isFunction)(value)) {
        return [name, makeChannel(name)];
    }
    return null;
})));
const channels = channelsAsUnknown;
function _cleanData(data) {
    const { cleaned, pathsChanged } = (0, cleanDataForIpc_1.cleanDataForIpc)(data);
    if (pathsChanged.length) {
        log.info(`_cleanData cleaned the following paths: ${pathsChanged.join(', ')}`);
    }
    return cleaned;
}
function _cleanMessageData(data) {
    // Ensure that all messages have the received_at set properly
    if (!data.received_at) {
        (0, assert_1.assert)(false, 'received_at was not set on the message');
        data.received_at = window.Signal.Util.incrementMessageCounter();
    }
    return _cleanData((0, lodash_1.omit)(data, ['dataMessage']));
}
async function _shutdown() {
    const jobKeys = Object.keys(_jobs);
    log.info(`data.shutdown: shutdown requested. ${jobKeys.length} jobs outstanding`);
    if (_shutdownPromise) {
        await _shutdownPromise;
        return;
    }
    _shuttingDown = true;
    // No outstanding jobs, return immediately
    if (jobKeys.length === 0 || _DEBUG) {
        return;
    }
    // Outstanding jobs; we need to wait until the last one is done
    _shutdownPromise = new Promise((resolve, reject) => {
        _shutdownCallback = (error) => {
            log.info('data.shutdown: process complete');
            if (error) {
                reject(error);
                return;
            }
            resolve();
        };
    });
    await _shutdownPromise;
}
function _makeJob(fnName) {
    if (_shuttingDown && fnName !== 'close') {
        throw new Error(`Rejecting SQL channel job (${fnName}); application is shutting down`);
    }
    _jobCounter += 1;
    const id = _jobCounter;
    if (_DEBUG) {
        log.info(`SQL channel job ${id} (${fnName}) started`);
    }
    _jobs[id] = {
        fnName,
        start: Date.now(),
    };
    return id;
}
function _updateJob(id, data) {
    const { resolve, reject } = data;
    const { fnName, start } = _jobs[id];
    _jobs[id] = Object.assign(Object.assign(Object.assign({}, _jobs[id]), data), { resolve: (value) => {
            _removeJob(id);
            const end = Date.now();
            if (_DEBUG) {
                log.info(`SQL channel job ${id} (${fnName}) succeeded in ${end - start}ms`);
            }
            return resolve(value);
        }, reject: (error) => {
            _removeJob(id);
            const end = Date.now();
            log.info(`SQL channel job ${id} (${fnName}) failed in ${end - start}ms`);
            return reject(error);
        } });
}
function _removeJob(id) {
    if (_DEBUG) {
        _jobs[id].complete = true;
        return;
    }
    delete _jobs[id];
    if (_shutdownCallback) {
        const keys = Object.keys(_jobs);
        if (keys.length === 0) {
            _shutdownCallback();
        }
    }
}
function _getJob(id) {
    return _jobs[id];
}
if (electron_1.ipcRenderer && electron_1.ipcRenderer.on) {
    electron_1.ipcRenderer.on(`${SQL_CHANNEL_KEY}-done`, (_, jobId, errorForDisplay, result) => {
        const job = _getJob(jobId);
        if (!job) {
            throw new Error(`Received SQL channel reply to job ${jobId}, but did not have it in our registry!`);
        }
        const { resolve, reject, fnName } = job;
        if (!resolve || !reject) {
            throw new Error(`SQL channel job ${jobId} (${fnName}): didn't have a resolve or reject`);
        }
        if (errorForDisplay) {
            return reject(new Error(`Error received from SQL channel job ${jobId} (${fnName}): ${errorForDisplay}`));
        }
        return resolve(result);
    });
}
else {
    log.warn('sql/Client: ipc.on is not available!');
}
function makeChannel(fnName) {
    return async (...args) => {
        // During startup we want to avoid the high overhead of IPC so we utilize
        // the db that exists in the renderer process to be able to boot up quickly
        // once the app is running we switch back to the main process to avoid the
        // UI from locking up whenever we do costly db operations.
        if (state === RendererState.InRenderer) {
            const serverFnName = fnName;
            const start = Date.now();
            try {
                // Ignoring this error TS2556: Expected 3 arguments, but got 0 or more.
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                return await Server_1.default[serverFnName](...args);
            }
            catch (error) {
                if ((0, errors_1.isCorruptionError)(error)) {
                    log.error('Detected sql corruption in renderer process. ' +
                        `Restarting the application immediately. Error: ${error.message}`);
                    electron_1.ipcRenderer === null || electron_1.ipcRenderer === void 0 ? void 0 : electron_1.ipcRenderer.send('database-error', `${error.stack}\n${Server_1.default.getCorruptionLog()}`);
                }
                log.error(`Renderer SQL channel job (${fnName}) error ${error.message}`);
                throw error;
            }
            finally {
                const duration = Date.now() - start;
                startupQueries.set(serverFnName, (startupQueries.get(serverFnName) || 0) + duration);
                if (duration > MIN_TRACE_DURATION || _DEBUG) {
                    log.info(`Renderer SQL channel job (${fnName}) completed in ${duration}ms`);
                }
            }
        }
        const jobId = _makeJob(fnName);
        return (0, TaskWithTimeout_1.default)(() => new Promise((resolve, reject) => {
            try {
                electron_1.ipcRenderer.send(SQL_CHANNEL_KEY, jobId, fnName, ...args);
                _updateJob(jobId, {
                    resolve,
                    reject,
                    args: _DEBUG ? args : undefined,
                });
            }
            catch (error) {
                _removeJob(jobId);
                reject(error);
            }
        }), `SQL channel job ${jobId} (${fnName})`)();
    };
}
function keysToBytes(keys, data) {
    const updated = (0, lodash_1.cloneDeep)(data);
    const max = keys.length;
    for (let i = 0; i < max; i += 1) {
        const key = keys[i];
        const value = (0, lodash_1.get)(data, key);
        if (value) {
            (0, lodash_1.set)(updated, key, Bytes.fromBase64(value));
        }
    }
    return updated;
}
function keysFromBytes(keys, data) {
    const updated = (0, lodash_1.cloneDeep)(data);
    const max = keys.length;
    for (let i = 0; i < max; i += 1) {
        const key = keys[i];
        const value = (0, lodash_1.get)(data, key);
        if (value) {
            (0, lodash_1.set)(updated, key, Bytes.toBase64(value));
        }
    }
    return updated;
}
// Top-level calls
async function shutdown() {
    // Stop accepting new SQL jobs, flush outstanding queue
    await _shutdown();
    // Close database
    await close();
}
// Note: will need to restart the app after calling this, to set up afresh
async function close() {
    await channels.close();
}
// Note: will need to restart the app after calling this, to set up afresh
async function removeDB() {
    await channels.removeDB();
}
async function removeIndexedDBFiles() {
    await channels.removeIndexedDBFiles();
}
// Identity Keys
const IDENTITY_KEY_KEYS = ['publicKey'];
async function createOrUpdateIdentityKey(data) {
    const updated = keysFromBytes(IDENTITY_KEY_KEYS, data);
    await channels.createOrUpdateIdentityKey(updated);
}
async function getIdentityKeyById(id) {
    const data = await channels.getIdentityKeyById(id);
    return keysToBytes(IDENTITY_KEY_KEYS, data);
}
async function bulkAddIdentityKeys(array) {
    const updated = (0, lodash_1.map)(array, data => keysFromBytes(IDENTITY_KEY_KEYS, data));
    await channels.bulkAddIdentityKeys(updated);
}
async function removeIdentityKeyById(id) {
    await channels.removeIdentityKeyById(id);
}
async function removeAllIdentityKeys() {
    await channels.removeAllIdentityKeys();
}
async function getAllIdentityKeys() {
    const keys = await channels.getAllIdentityKeys();
    return keys.map(key => keysToBytes(IDENTITY_KEY_KEYS, key));
}
// Pre Keys
async function createOrUpdatePreKey(data) {
    const updated = keysFromBytes(PRE_KEY_KEYS, data);
    await channels.createOrUpdatePreKey(updated);
}
async function getPreKeyById(id) {
    const data = await channels.getPreKeyById(id);
    return keysToBytes(PRE_KEY_KEYS, data);
}
async function bulkAddPreKeys(array) {
    const updated = (0, lodash_1.map)(array, data => keysFromBytes(PRE_KEY_KEYS, data));
    await channels.bulkAddPreKeys(updated);
}
async function removePreKeyById(id) {
    await channels.removePreKeyById(id);
}
async function removeAllPreKeys() {
    await channels.removeAllPreKeys();
}
async function getAllPreKeys() {
    const keys = await channels.getAllPreKeys();
    return keys.map(key => keysToBytes(PRE_KEY_KEYS, key));
}
// Signed Pre Keys
const PRE_KEY_KEYS = ['privateKey', 'publicKey'];
async function createOrUpdateSignedPreKey(data) {
    const updated = keysFromBytes(PRE_KEY_KEYS, data);
    await channels.createOrUpdateSignedPreKey(updated);
}
async function getSignedPreKeyById(id) {
    const data = await channels.getSignedPreKeyById(id);
    return keysToBytes(PRE_KEY_KEYS, data);
}
async function getAllSignedPreKeys() {
    const keys = await channels.getAllSignedPreKeys();
    return keys.map((key) => keysToBytes(PRE_KEY_KEYS, key));
}
async function bulkAddSignedPreKeys(array) {
    const updated = (0, lodash_1.map)(array, data => keysFromBytes(PRE_KEY_KEYS, data));
    await channels.bulkAddSignedPreKeys(updated);
}
async function removeSignedPreKeyById(id) {
    await channels.removeSignedPreKeyById(id);
}
async function removeAllSignedPreKeys() {
    await channels.removeAllSignedPreKeys();
}
// Items
const ITEM_KEYS = {
    senderCertificate: ['value.serialized'],
    senderCertificateNoE164: ['value.serialized'],
    profileKey: ['value'],
};
async function createOrUpdateItem(data) {
    const { id } = data;
    if (!id) {
        throw new Error('createOrUpdateItem: Provided data did not have a truthy id');
    }
    const keys = ITEM_KEYS[id];
    const updated = Array.isArray(keys) ? keysFromBytes(keys, data) : data;
    await channels.createOrUpdateItem(updated);
}
async function getItemById(id) {
    const keys = ITEM_KEYS[id];
    const data = await channels.getItemById(id);
    return Array.isArray(keys) ? keysToBytes(keys, data) : data;
}
async function getAllItems() {
    const items = await channels.getAllItems();
    const result = Object.create(null);
    for (const id of Object.keys(items)) {
        const key = id;
        const value = items[key];
        const keys = ITEM_KEYS[key];
        const deserializedValue = Array.isArray(keys)
            ? keysToBytes(keys, { value }).value
            : value;
        result[key] = deserializedValue;
    }
    return result;
}
async function removeItemById(id) {
    await channels.removeItemById(id);
}
async function removeAllItems() {
    await channels.removeAllItems();
}
// Sender Keys
async function createOrUpdateSenderKey(key) {
    await channels.createOrUpdateSenderKey(key);
}
async function getSenderKeyById(id) {
    return channels.getSenderKeyById(id);
}
async function removeAllSenderKeys() {
    await channels.removeAllSenderKeys();
}
async function getAllSenderKeys() {
    return channels.getAllSenderKeys();
}
async function removeSenderKeyById(id) {
    return channels.removeSenderKeyById(id);
}
// Sent Protos
async function insertSentProto(proto, options) {
    return channels.insertSentProto(proto, Object.assign(Object.assign({}, options), { messageIds: (0, lodash_1.uniq)(options.messageIds) }));
}
async function deleteSentProtosOlderThan(timestamp) {
    await channels.deleteSentProtosOlderThan(timestamp);
}
async function deleteSentProtoByMessageId(messageId) {
    await channels.deleteSentProtoByMessageId(messageId);
}
async function insertProtoRecipients(options) {
    await channels.insertProtoRecipients(options);
}
async function deleteSentProtoRecipient(options) {
    await channels.deleteSentProtoRecipient(options);
}
async function getSentProtoByRecipient(options) {
    return channels.getSentProtoByRecipient(options);
}
async function removeAllSentProtos() {
    await channels.removeAllSentProtos();
}
async function getAllSentProtos() {
    return channels.getAllSentProtos();
}
// Test-only:
async function _getAllSentProtoRecipients() {
    return channels._getAllSentProtoRecipients();
}
async function _getAllSentProtoMessageIds() {
    return channels._getAllSentProtoMessageIds();
}
// Sessions
async function createOrUpdateSession(data) {
    await channels.createOrUpdateSession(data);
}
async function createOrUpdateSessions(array) {
    await channels.createOrUpdateSessions(array);
}
async function commitSessionsAndUnprocessed(options) {
    await channels.commitSessionsAndUnprocessed(options);
}
async function bulkAddSessions(array) {
    await channels.bulkAddSessions(array);
}
async function removeSessionById(id) {
    await channels.removeSessionById(id);
}
async function removeSessionsByConversation(conversationId) {
    await channels.removeSessionsByConversation(conversationId);
}
async function removeAllSessions() {
    await channels.removeAllSessions();
}
async function getAllSessions() {
    const sessions = await channels.getAllSessions();
    return sessions;
}
// Conversation
async function getConversationCount() {
    return channels.getConversationCount();
}
async function saveConversation(data) {
    await channels.saveConversation(data);
}
async function saveConversations(array) {
    await channels.saveConversations(array);
}
async function getConversationById(id, { Conversation }) {
    const data = await channels.getConversationById(id);
    if (!data) {
        return undefined;
    }
    return new Conversation(data);
}
const updateConversationBatcher = (0, batcher_1.createBatcher)({
    name: 'sql.Client.updateConversationBatcher',
    wait: 500,
    maxSize: 20,
    processBatch: async (items) => {
        // We only care about the most recent update for each conversation
        const byId = (0, lodash_1.groupBy)(items, item => item.id);
        const ids = Object.keys(byId);
        const mostRecent = ids.map((id) => {
            const maybeLast = (0, lodash_1.last)(byId[id]);
            (0, assert_1.assert)(maybeLast !== undefined, 'Empty array in `groupBy` result');
            return maybeLast;
        });
        await updateConversations(mostRecent);
    },
});
function updateConversation(data) {
    updateConversationBatcher.add(data);
}
async function updateConversations(array) {
    const { cleaned, pathsChanged } = (0, cleanDataForIpc_1.cleanDataForIpc)(array);
    (0, assert_1.assert)(!pathsChanged.length, `Paths were cleaned: ${JSON.stringify(pathsChanged)}`);
    await channels.updateConversations(cleaned);
}
async function removeConversation(id, { Conversation }) {
    const existing = await getConversationById(id, { Conversation });
    // Note: It's important to have a fully database-hydrated model to delete here because
    //   it needs to delete all associated on-disk files along with the database delete.
    if (existing) {
        await channels.removeConversation(id);
        await existing.cleanup();
    }
}
// Note: this method will not clean up external files, just delete from SQL
async function _removeConversations(ids) {
    await channels.removeConversation(ids);
}
async function eraseStorageServiceStateFromConversations() {
    await channels.eraseStorageServiceStateFromConversations();
}
async function getAllConversations({ ConversationCollection, }) {
    const conversations = await channels.getAllConversations();
    const collection = new ConversationCollection();
    collection.add(conversations);
    return collection;
}
async function getAllConversationIds() {
    const ids = await channels.getAllConversationIds();
    return ids;
}
async function getAllPrivateConversations({ ConversationCollection, }) {
    const conversations = await channels.getAllPrivateConversations();
    const collection = new ConversationCollection();
    collection.add(conversations);
    return collection;
}
async function getAllGroupsInvolvingUuid(uuid, { ConversationCollection, }) {
    const conversations = await channels.getAllGroupsInvolvingUuid(uuid);
    const collection = new ConversationCollection();
    collection.add(conversations);
    return collection;
}
async function searchConversations(query) {
    const conversations = await channels.searchConversations(query);
    return conversations;
}
function handleSearchMessageJSON(messages) {
    return messages.map(message => (Object.assign(Object.assign({ json: message.json, 
        // Empty array is a default value. `message.json` has the real field
        bodyRanges: [] }, JSON.parse(message.json)), { snippet: message.snippet })));
}
async function searchMessages(query, { limit } = {}) {
    const messages = await channels.searchMessages(query, { limit });
    return handleSearchMessageJSON(messages);
}
async function searchMessagesInConversation(query, conversationId, { limit } = {}) {
    const messages = await channels.searchMessagesInConversation(query, conversationId, { limit });
    return handleSearchMessageJSON(messages);
}
// Message
async function getMessageCount(conversationId) {
    return channels.getMessageCount(conversationId);
}
async function saveMessage(data, options = {}) {
    const id = await channels.saveMessage(_cleanMessageData(data), Object.assign(Object.assign({}, options), { jobToInsert: options.jobToInsert && (0, formatJobForInsert_1.formatJobForInsert)(options.jobToInsert) }));
    window.Whisper.ExpiringMessagesListener.update();
    window.Whisper.TapToViewMessagesListener.update();
    return id;
}
async function saveMessages(arrayOfMessages, options) {
    await channels.saveMessages(arrayOfMessages.map(message => _cleanMessageData(message)), options);
    window.Whisper.ExpiringMessagesListener.update();
    window.Whisper.TapToViewMessagesListener.update();
}
async function removeMessage(id, { Message }) {
    const message = await getMessageById(id, { Message });
    // Note: It's important to have a fully database-hydrated model to delete here because
    //   it needs to delete all associated on-disk files along with the database delete.
    if (message) {
        await channels.removeMessage(id);
        await message.cleanup();
    }
}
// Note: this method will not clean up external files, just delete from SQL
async function removeMessages(ids) {
    await channels.removeMessages(ids);
}
async function getMessageById(id, { Message }) {
    const message = await channels.getMessageById(id);
    if (!message) {
        return undefined;
    }
    return new Message(message);
}
async function getMessagesById(messageIds) {
    if (!messageIds.length) {
        return [];
    }
    return channels.getMessagesById(messageIds);
}
// For testing only
async function _getAllMessages({ MessageCollection, }) {
    const messages = await channels._getAllMessages();
    return new MessageCollection(messages);
}
async function getAllMessageIds() {
    const ids = await channels.getAllMessageIds();
    return ids;
}
async function getMessageBySender({ source, sourceUuid, sourceDevice, sent_at, }, { Message }) {
    const messages = await channels.getMessageBySender({
        source,
        sourceUuid,
        sourceDevice,
        sent_at,
    });
    if (!messages || !messages.length) {
        return null;
    }
    return new Message(messages[0]);
}
async function getUnreadCountForConversation(conversationId) {
    return channels.getUnreadCountForConversation(conversationId);
}
async function getUnreadByConversationAndMarkRead(conversationId, newestUnreadId, readAt) {
    return channels.getUnreadByConversationAndMarkRead(conversationId, newestUnreadId, readAt);
}
async function getUnreadReactionsAndMarkRead(conversationId, newestUnreadId) {
    return channels.getUnreadReactionsAndMarkRead(conversationId, newestUnreadId);
}
async function markReactionAsRead(targetAuthorUuid, targetTimestamp) {
    return channels.markReactionAsRead(targetAuthorUuid, targetTimestamp);
}
async function removeReactionFromConversation(reaction) {
    return channels.removeReactionFromConversation(reaction);
}
async function addReaction(reactionObj) {
    return channels.addReaction(reactionObj);
}
async function _getAllReactions() {
    return channels._getAllReactions();
}
function handleMessageJSON(messages) {
    return messages.map(message => JSON.parse(message.json));
}
async function getOlderMessagesByConversation(conversationId, { limit = 100, receivedAt = Number.MAX_VALUE, sentAt = Number.MAX_VALUE, messageId, MessageCollection, }) {
    const messages = await channels.getOlderMessagesByConversation(conversationId, {
        limit,
        receivedAt,
        sentAt,
        messageId,
    });
    return new MessageCollection(handleMessageJSON(messages));
}
async function getNewerMessagesByConversation(conversationId, { limit = 100, receivedAt = 0, sentAt = 0, MessageCollection, }) {
    const messages = await channels.getNewerMessagesByConversation(conversationId, {
        limit,
        receivedAt,
        sentAt,
    });
    return new MessageCollection(handleMessageJSON(messages));
}
async function getLastConversationMessages({ conversationId, ourUuid, Message, }) {
    const { preview, activity, hasUserInitiatedMessages } = await channels.getLastConversationMessages({
        conversationId,
        ourUuid,
    });
    return {
        preview: preview ? new Message(preview) : undefined,
        activity: activity ? new Message(activity) : undefined,
        hasUserInitiatedMessages,
    };
}
async function getMessageMetricsForConversation(conversationId) {
    const result = await channels.getMessageMetricsForConversation(conversationId);
    return result;
}
function hasGroupCallHistoryMessage(conversationId, eraId) {
    return channels.hasGroupCallHistoryMessage(conversationId, eraId);
}
async function migrateConversationMessages(obsoleteId, currentId) {
    await channels.migrateConversationMessages(obsoleteId, currentId);
}
async function removeAllMessagesInConversation(conversationId, { logId, MessageCollection, }) {
    let messages;
    do {
        const chunkSize = 20;
        log.info(`removeAllMessagesInConversation/${logId}: Fetching chunk of ${chunkSize} messages`);
        // Yes, we really want the await in the loop. We're deleting a chunk at a
        //   time so we don't use too much memory.
        messages = await getOlderMessagesByConversation(conversationId, {
            limit: chunkSize,
            MessageCollection,
        });
        if (!messages.length) {
            return;
        }
        const ids = messages.map((message) => message.id);
        log.info(`removeAllMessagesInConversation/${logId}: Cleanup...`);
        // Note: It's very important that these models are fully hydrated because
        //   we need to delete all associated on-disk files along with the database delete.
        const queue = new window.PQueue({ concurrency: 3, timeout: 1000 * 60 * 2 });
        queue.addAll(messages.map((message) => async () => message.cleanup()));
        await queue.onIdle();
        log.info(`removeAllMessagesInConversation/${logId}: Deleting...`);
        await channels.removeMessages(ids);
    } while (messages.length > 0);
}
async function getMessagesBySentAt(sentAt, { MessageCollection, }) {
    const messages = await channels.getMessagesBySentAt(sentAt);
    return new MessageCollection(messages);
}
async function getExpiredMessages({ MessageCollection, }) {
    const messages = await channels.getExpiredMessages();
    return new MessageCollection(messages);
}
function getMessagesUnexpectedlyMissingExpirationStartTimestamp() {
    return channels.getMessagesUnexpectedlyMissingExpirationStartTimestamp();
}
function getSoonestMessageExpiry() {
    return channels.getSoonestMessageExpiry();
}
async function getNextTapToViewMessageTimestampToAgeOut() {
    return channels.getNextTapToViewMessageTimestampToAgeOut();
}
async function getTapToViewMessagesNeedingErase({ MessageCollection, }) {
    const messages = await channels.getTapToViewMessagesNeedingErase();
    return new MessageCollection(messages);
}
// Unprocessed
async function getUnprocessedCount() {
    return channels.getUnprocessedCount();
}
async function getAllUnprocessed() {
    return channels.getAllUnprocessed();
}
async function getUnprocessedById(id) {
    return channels.getUnprocessedById(id);
}
async function updateUnprocessedWithData(id, data) {
    await channels.updateUnprocessedWithData(id, data);
}
async function updateUnprocessedsWithData(array) {
    await channels.updateUnprocessedsWithData(array);
}
async function removeUnprocessed(id) {
    await channels.removeUnprocessed(id);
}
async function removeAllUnprocessed() {
    await channels.removeAllUnprocessed();
}
// Attachment downloads
async function getNextAttachmentDownloadJobs(limit, options) {
    return channels.getNextAttachmentDownloadJobs(limit, options);
}
async function saveAttachmentDownloadJob(job) {
    await channels.saveAttachmentDownloadJob(_cleanData(job));
}
async function setAttachmentDownloadJobPending(id, pending) {
    await channels.setAttachmentDownloadJobPending(id, pending);
}
async function resetAttachmentDownloadPending() {
    await channels.resetAttachmentDownloadPending();
}
async function removeAttachmentDownloadJob(id) {
    await channels.removeAttachmentDownloadJob(id);
}
async function removeAllAttachmentDownloadJobs() {
    await channels.removeAllAttachmentDownloadJobs();
}
// Stickers
async function getStickerCount() {
    return channels.getStickerCount();
}
async function createOrUpdateStickerPack(pack) {
    await channels.createOrUpdateStickerPack(pack);
}
async function updateStickerPackStatus(packId, status, options) {
    await channels.updateStickerPackStatus(packId, status, options);
}
async function createOrUpdateSticker(sticker) {
    await channels.createOrUpdateSticker(sticker);
}
async function updateStickerLastUsed(packId, stickerId, timestamp) {
    await channels.updateStickerLastUsed(packId, stickerId, timestamp);
}
async function addStickerPackReference(messageId, packId) {
    await channels.addStickerPackReference(messageId, packId);
}
async function deleteStickerPackReference(messageId, packId) {
    return channels.deleteStickerPackReference(messageId, packId);
}
async function deleteStickerPack(packId) {
    const paths = await channels.deleteStickerPack(packId);
    return paths;
}
async function getAllStickerPacks() {
    const packs = await channels.getAllStickerPacks();
    return packs;
}
async function getAllStickers() {
    const stickers = await channels.getAllStickers();
    return stickers;
}
async function getRecentStickers() {
    const recentStickers = await channels.getRecentStickers();
    return recentStickers;
}
async function clearAllErrorStickerPackAttempts() {
    await channels.clearAllErrorStickerPackAttempts();
}
// Emojis
async function updateEmojiUsage(shortName) {
    await channels.updateEmojiUsage(shortName);
}
async function getRecentEmojis(limit = 32) {
    return channels.getRecentEmojis(limit);
}
// Badges
function getAllBadges() {
    return channels.getAllBadges();
}
async function updateOrCreateBadges(badges) {
    if (badges.length) {
        await channels.updateOrCreateBadges(badges);
    }
}
function badgeImageFileDownloaded(url, localPath) {
    return channels.badgeImageFileDownloaded(url, localPath);
}
// Other
async function removeAll() {
    await channels.removeAll();
}
async function removeAllConfiguration(type) {
    await channels.removeAllConfiguration(type);
}
async function cleanupOrphanedAttachments() {
    await callChannel(CLEANUP_ORPHANED_ATTACHMENTS_KEY);
}
async function ensureFilePermissions() {
    await callChannel(ENSURE_FILE_PERMISSIONS);
}
// Note: will need to restart the app after calling this, to set up afresh
async function removeOtherData() {
    await Promise.all([
        callChannel(ERASE_SQL_KEY),
        callChannel(ERASE_ATTACHMENTS_KEY),
        callChannel(ERASE_STICKERS_KEY),
        callChannel(ERASE_TEMP_KEY),
        callChannel(ERASE_DRAFTS_KEY),
    ]);
}
async function callChannel(name) {
    return (0, TaskWithTimeout_1.default)(() => new Promise((resolve, reject) => {
        electron_1.ipcRenderer.send(name);
        electron_1.ipcRenderer.once(`${name}-done`, (_, error) => {
            if (error) {
                reject(error);
                return;
            }
            resolve();
        });
    }), `callChannel call to ${name}`)();
}
async function getMessagesNeedingUpgrade(limit, { maxVersion = message_1.CURRENT_SCHEMA_VERSION }) {
    const messages = await channels.getMessagesNeedingUpgrade(limit, {
        maxVersion,
    });
    return messages;
}
async function getMessagesWithVisualMediaAttachments(conversationId, { limit }) {
    return channels.getMessagesWithVisualMediaAttachments(conversationId, {
        limit,
    });
}
async function getMessagesWithFileAttachments(conversationId, { limit }) {
    return channels.getMessagesWithFileAttachments(conversationId, {
        limit,
    });
}
function getMessageServerGuidsForSpam(conversationId) {
    return channels.getMessageServerGuidsForSpam(conversationId);
}
function getJobsInQueue(queueType) {
    return channels.getJobsInQueue(queueType);
}
function insertJob(job) {
    return channels.insertJob(job);
}
function deleteJob(id) {
    return channels.deleteJob(id);
}
function processGroupCallRingRequest(ringId) {
    return channels.processGroupCallRingRequest(ringId);
}
function processGroupCallRingCancelation(ringId) {
    return channels.processGroupCallRingCancelation(ringId);
}
async function cleanExpiredGroupCallRings() {
    await channels.cleanExpiredGroupCallRings();
}
async function updateAllConversationColors(conversationColor, customColorData) {
    return channels.updateAllConversationColors(conversationColor, customColorData);
}
function getMaxMessageCounter() {
    return channels.getMaxMessageCounter();
}
function getStatisticsForLogging() {
    return channels.getStatisticsForLogging();
}
