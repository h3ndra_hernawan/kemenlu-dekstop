"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useIntersectionObserver = void 0;
const react_1 = require("react");
const log = __importStar(require("../logging/log"));
/**
 * A light hook wrapper around `IntersectionObserver`.
 *
 * Example usage:
 *
 *     function MyComponent() {
 *       const [intersectionRef, intersectionEntry] = useIntersectionObserver();
 *       const isVisible = intersectionEntry
 *         ? intersectionEntry.isIntersecting
 *         : true;
 *
 *       return (
 *         <div ref={intersectionRef}>
 *           I am {isVisible ? 'on the screen' : 'invisible'}
 *         </div>
 *       );
 *    }
 */
function useIntersectionObserver() {
    const [intersectionObserverEntry, setIntersectionObserverEntry] = (0, react_1.useState)(null);
    const unobserveRef = (0, react_1.useRef)(null);
    const setRef = (0, react_1.useCallback)((el) => {
        if (unobserveRef.current) {
            unobserveRef.current();
            unobserveRef.current = null;
        }
        if (!el) {
            return;
        }
        const observer = new IntersectionObserver(entries => {
            if (entries.length !== 1) {
                log.error('IntersectionObserverWrapper was observing the wrong number of elements');
                return;
            }
            entries.forEach(entry => {
                setIntersectionObserverEntry(entry);
            });
        });
        unobserveRef.current = observer.unobserve.bind(observer, el);
        observer.observe(el);
    }, []);
    return [setRef, intersectionObserverEntry];
}
exports.useIntersectionObserver = useIntersectionObserver;
