"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.useReducedMotion = void 0;
const react_1 = require("react");
function getReducedMotionQuery() {
    return window.matchMedia('(prefers-reduced-motion: reduce)');
}
// Inspired by <https://github.com/infiniteluke/react-reduce-motion>.
function useReducedMotion() {
    const initialQuery = getReducedMotionQuery();
    const [prefersReducedMotion, setPrefersReducedMotion] = (0, react_1.useState)(initialQuery.matches);
    (0, react_1.useEffect)(() => {
        const query = getReducedMotionQuery();
        function changePreference() {
            setPrefersReducedMotion(query.matches);
        }
        changePreference();
        query.addEventListener('change', changePreference);
        return () => {
            query.removeEventListener('change', changePreference);
        };
    });
    return prefersReducedMotion;
}
exports.useReducedMotion = useReducedMotion;
