"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.useIsOnline = void 0;
const react_1 = require("react");
function useIsOnline() {
    const [isOnline, setIsOnline] = (0, react_1.useState)(navigator.onLine);
    (0, react_1.useEffect)(() => {
        const update = () => {
            setIsOnline(navigator.onLine);
        };
        update();
        window.addEventListener('offline', update);
        window.addEventListener('online', update);
        return () => {
            window.removeEventListener('offline', update);
            window.removeEventListener('online', update);
        };
    }, []);
    return isOnline;
}
exports.useIsOnline = useIsOnline;
