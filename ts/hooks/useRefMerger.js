"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.useRefMerger = void 0;
const react_1 = require("react");
const refMerger_1 = require("../util/refMerger");
const useRefMerger = () => (0, react_1.useMemo)(refMerger_1.createRefMerger, []);
exports.useRefMerger = useRefMerger;
