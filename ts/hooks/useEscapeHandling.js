"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.useEscapeHandling = void 0;
const react_1 = require("react");
function useEscapeHandling(handleEscape) {
    (0, react_1.useEffect)(() => {
        if (!handleEscape) {
            return;
        }
        const handler = (event) => {
            if (event.key === 'Escape') {
                handleEscape();
                event.preventDefault();
                event.stopPropagation();
            }
        };
        document.addEventListener('keydown', handler);
        return () => {
            document.removeEventListener('keydown', handler);
        };
    }, [handleEscape]);
}
exports.useEscapeHandling = useEscapeHandling;
