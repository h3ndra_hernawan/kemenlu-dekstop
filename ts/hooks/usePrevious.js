"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.usePrevious = void 0;
const react_1 = require("react");
function usePrevious(initialValue, currentValue) {
    const previousValueRef = (0, react_1.useRef)(initialValue);
    const result = previousValueRef.current;
    previousValueRef.current = currentValue;
    return result;
}
exports.usePrevious = usePrevious;
