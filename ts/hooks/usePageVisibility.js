"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.usePageVisibility = void 0;
const react_1 = require("react");
function usePageVisibility() {
    const [result, setResult] = (0, react_1.useState)(!document.hidden);
    (0, react_1.useEffect)(() => {
        const updatePageVisibility = () => {
            setResult(!document.hidden);
        };
        updatePageVisibility();
        document.addEventListener('visibilitychange', updatePageVisibility, false);
        return () => {
            document.removeEventListener('visibilitychange', updatePageVisibility, false);
        };
    }, []);
    return result;
}
exports.usePageVisibility = usePageVisibility;
