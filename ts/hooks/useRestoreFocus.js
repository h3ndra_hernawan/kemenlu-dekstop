"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useDelayedRestoreFocus = exports.useRestoreFocus = void 0;
const React = __importStar(require("react"));
// Restore focus on teardown
const useRestoreFocus = () => {
    const toFocusRef = React.useRef(null);
    const lastFocusedRef = React.useRef(null);
    // We need to use a callback here because refs aren't necessarily populated on first
    //   render. For example, ModalHost makes a top-level parent div first, and then renders
    //   into it. And the children you pass it don't have access to that root div.
    const setFocusRef = React.useCallback((toFocus) => {
        if (!toFocus) {
            return;
        }
        // We only want to do this once.
        if (toFocusRef.current) {
            return;
        }
        toFocusRef.current = toFocus;
        // Remember last-focused element, focus this new target element.
        lastFocusedRef.current = document.activeElement;
        toFocus.focus();
    }, []);
    React.useEffect(() => {
        return () => {
            // On unmount, returned focus to element focused before we set the focus
            setTimeout(() => {
                if (lastFocusedRef.current && lastFocusedRef.current.focus) {
                    lastFocusedRef.current.focus();
                }
            });
        };
    }, []);
    return [setFocusRef];
};
exports.useRestoreFocus = useRestoreFocus;
// Panels are initially rendered outside the DOM, and then added to it. We need to
//   delay our attempts to set focus.
// Just like the above hook, but with a debounce.
const useDelayedRestoreFocus = () => {
    const toFocusRef = React.useRef(null);
    const lastFocusedRef = React.useRef(null);
    const setFocusRef = React.useCallback((toFocus) => {
        function setFocus() {
            if (!toFocus) {
                return;
            }
            // We only want to do this once.
            if (toFocusRef.current) {
                return;
            }
            toFocusRef.current = toFocus;
            // Remember last-focused element, focus this new target element.
            lastFocusedRef.current = document.activeElement;
            toFocus.focus();
        }
        const timeout = setTimeout(setFocus, 250);
        return () => {
            clearTimeout(timeout);
        };
    }, []);
    React.useEffect(() => {
        return () => {
            // On unmount, returned focus to element focused before we set the focus
            setTimeout(() => {
                if (lastFocusedRef.current && lastFocusedRef.current.focus) {
                    lastFocusedRef.current.focus();
                }
            });
        };
    }, []);
    return [setFocusRef];
};
exports.useDelayedRestoreFocus = useDelayedRestoreFocus;
