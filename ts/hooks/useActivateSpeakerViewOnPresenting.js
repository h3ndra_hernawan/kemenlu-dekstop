"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.useActivateSpeakerViewOnPresenting = void 0;
const react_1 = require("react");
const usePrevious_1 = require("./usePrevious");
function useActivateSpeakerViewOnPresenting(remoteParticipants, isInSpeakerView, toggleSpeakerView) {
    var _a;
    const presenterUuid = (_a = remoteParticipants.find(participant => participant.presenting)) === null || _a === void 0 ? void 0 : _a.uuid;
    const prevPresenterUuid = (0, usePrevious_1.usePrevious)(presenterUuid, presenterUuid);
    (0, react_1.useEffect)(() => {
        if (prevPresenterUuid !== presenterUuid && !isInSpeakerView) {
            toggleSpeakerView();
        }
    }, [isInSpeakerView, presenterUuid, prevPresenterUuid, toggleSpeakerView]);
}
exports.useActivateSpeakerViewOnPresenting = useActivateSpeakerViewOnPresenting;
