"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.useHasWrapped = void 0;
const react_1 = require("react");
const lodash_1 = require("lodash");
function getTop(element) {
    return element.getBoundingClientRect().top;
}
function isWrapped(element) {
    if (!element) {
        return false;
    }
    const { children } = element;
    const firstChild = (0, lodash_1.first)(children);
    const lastChild = (0, lodash_1.last)(children);
    return Boolean(firstChild &&
        lastChild &&
        firstChild !== lastChild &&
        getTop(firstChild) !== getTop(lastChild));
}
/**
 * A hook that returns a ref (to put on your element) and a boolean. The boolean will be
 * `true` if the element's children have different `top`s, and `false` otherwise.
 */
function useHasWrapped() {
    const [element, setElement] = (0, react_1.useState)(null);
    const [hasWrapped, setHasWrapped] = (0, react_1.useState)(isWrapped(element));
    (0, react_1.useEffect)(() => {
        if (!element) {
            return lodash_1.noop;
        }
        const observer = new ResizeObserver(() => {
            setHasWrapped(isWrapped(element));
        });
        observer.observe(element);
        return () => {
            observer.disconnect();
        };
    }, [element]);
    return [setElement, hasWrapped];
}
exports.useHasWrapped = useHasWrapped;
