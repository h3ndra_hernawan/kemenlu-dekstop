"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.useAnimated = void 0;
const react_1 = require("react");
const web_1 = require("@react-spring/web");
function useAnimated(onClose, { getFrom, getTo, }) {
    const [isOpen, setIsOpen] = (0, react_1.useState)(true);
    const modalRef = (0, web_1.useSpringRef)();
    const modalStyles = (0, web_1.useSpring)({
        from: getFrom(isOpen),
        to: getTo(isOpen),
        onRest: () => {
            if (!isOpen) {
                onClose();
            }
        },
        config: {
            clamp: true,
            friction: 20,
            mass: 0.5,
            tension: 350,
        },
        ref: modalRef,
    });
    const overlayRef = (0, web_1.useSpringRef)();
    const overlayStyles = (0, web_1.useSpring)({
        from: { opacity: 0 },
        to: { opacity: isOpen ? 1 : 0 },
        config: {
            clamp: true,
            friction: 22,
            tension: 360,
        },
        ref: overlayRef,
    });
    (0, web_1.useChain)(isOpen ? [overlayRef, modalRef] : [modalRef, overlayRef]);
    return {
        close: () => setIsOpen(false),
        overlayStyles,
        modalStyles,
    };
}
exports.useAnimated = useAnimated;
