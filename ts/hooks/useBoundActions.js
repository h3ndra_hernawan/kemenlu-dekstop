"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.useBoundActions = void 0;
const redux_1 = require("redux");
const react_redux_1 = require("react-redux");
const react_1 = require("react");
const useBoundActions = (actions) => {
    const dispatch = (0, react_redux_1.useDispatch)();
    return (0, react_1.useMemo)(() => {
        return (0, redux_1.bindActionCreators)(actions, dispatch);
    }, [actions, dispatch]);
};
exports.useBoundActions = useBoundActions;
