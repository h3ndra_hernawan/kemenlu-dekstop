"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useKeyboardShortcuts = exports.useAttachFileShortcut = exports.useStartRecordingShortcut = void 0;
const react_1 = require("react");
const lodash_1 = require("lodash");
const KeyboardLayout = __importStar(require("../services/keyboardLayout"));
function isCmdOrCtrl(ev) {
    const { ctrlKey, metaKey } = ev;
    const commandKey = (0, lodash_1.get)(window, 'platform') === 'darwin' && metaKey;
    const controlKey = (0, lodash_1.get)(window, 'platform') !== 'darwin' && ctrlKey;
    return commandKey || controlKey;
}
function useStartRecordingShortcut(startAudioRecording) {
    return (0, react_1.useCallback)(ev => {
        const { shiftKey } = ev;
        const key = KeyboardLayout.lookup(ev);
        if (isCmdOrCtrl(ev) && shiftKey && (key === 'v' || key === 'V')) {
            ev.preventDefault();
            ev.stopPropagation();
            startAudioRecording();
            return true;
        }
        return false;
    }, [startAudioRecording]);
}
exports.useStartRecordingShortcut = useStartRecordingShortcut;
function useAttachFileShortcut(attachFile) {
    return (0, react_1.useCallback)(ev => {
        const { shiftKey } = ev;
        const key = KeyboardLayout.lookup(ev);
        if (isCmdOrCtrl(ev) && !shiftKey && (key === 'u' || key === 'U')) {
            ev.preventDefault();
            ev.stopPropagation();
            attachFile();
            return true;
        }
        return false;
    }, [attachFile]);
}
exports.useAttachFileShortcut = useAttachFileShortcut;
function useKeyboardShortcuts(...eventHandlers) {
    (0, react_1.useEffect)(() => {
        function handleKeydown(ev) {
            eventHandlers.some(eventHandler => eventHandler(ev));
        }
        document.addEventListener('keydown', handleKeydown);
        return () => {
            document.removeEventListener('keydown', handleKeydown);
        };
    }, [eventHandlers]);
}
exports.useKeyboardShortcuts = useKeyboardShortcuts;
