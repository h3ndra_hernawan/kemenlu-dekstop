"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.areEqual = exports.isNotEmpty = exports.isEmpty = exports.concatenate = exports.byteLength = exports.toString = exports.toBinary = exports.toHex = exports.toBase64 = exports.fromString = exports.fromBinary = exports.fromHex = exports.fromBase64 = void 0;
const Bytes_1 = require("./context/Bytes");
const bytes = ((_a = window.SignalContext) === null || _a === void 0 ? void 0 : _a.bytes) || new Bytes_1.Bytes();
function fromBase64(value) {
    return bytes.fromBase64(value);
}
exports.fromBase64 = fromBase64;
function fromHex(value) {
    return bytes.fromHex(value);
}
exports.fromHex = fromHex;
// TODO(indutny): deprecate it
function fromBinary(value) {
    return bytes.fromBinary(value);
}
exports.fromBinary = fromBinary;
function fromString(value) {
    return bytes.fromString(value);
}
exports.fromString = fromString;
function toBase64(data) {
    return bytes.toBase64(data);
}
exports.toBase64 = toBase64;
function toHex(data) {
    return bytes.toHex(data);
}
exports.toHex = toHex;
// TODO(indutny): deprecate it
function toBinary(data) {
    return bytes.toBinary(data);
}
exports.toBinary = toBinary;
function toString(data) {
    return bytes.toString(data);
}
exports.toString = toString;
function byteLength(value) {
    return bytes.byteLength(value);
}
exports.byteLength = byteLength;
function concatenate(list) {
    return bytes.concatenate(list);
}
exports.concatenate = concatenate;
function isEmpty(data) {
    return bytes.isEmpty(data);
}
exports.isEmpty = isEmpty;
function isNotEmpty(data) {
    return !bytes.isEmpty(data);
}
exports.isNotEmpty = isNotEmpty;
function areEqual(a, b) {
    return bytes.areEqual(a, b);
}
exports.areEqual = areEqual;
