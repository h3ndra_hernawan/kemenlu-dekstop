"use strict";
// Copyright 2016-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignalProtocolStore = exports.freezeSignedPreKey = exports.freezePreKey = exports.freezePublicKey = exports.freezeSession = exports.hydrateSignedPreKey = exports.hydratePreKey = exports.hydratePublicKey = exports.hydrateSession = exports.GLOBAL_ZONE = void 0;
const p_queue_1 = __importDefault(require("p-queue"));
const lodash_1 = require("lodash");
const zod_1 = require("zod");
const signal_client_1 = require("@signalapp/signal-client");
const Bytes = __importStar(require("./Bytes"));
const Crypto_1 = require("./Crypto");
const assert_1 = require("./util/assert");
const handleMessageSend_1 = require("./util/handleMessageSend");
const isNotNil_1 = require("./util/isNotNil");
const Zone_1 = require("./util/Zone");
const timestamp_1 = require("./util/timestamp");
const sessionTranslation_1 = require("./util/sessionTranslation");
const getSendOptions_1 = require("./util/getSendOptions");
const UUID_1 = require("./types/UUID");
const QualifiedAddress_1 = require("./types/QualifiedAddress");
const log = __importStar(require("./logging/log"));
const TIMESTAMP_THRESHOLD = 5 * 1000; // 5 seconds
const VerifiedStatus = {
    DEFAULT: 0,
    VERIFIED: 1,
    UNVERIFIED: 2,
};
function validateVerifiedStatus(status) {
    if (status === VerifiedStatus.DEFAULT ||
        status === VerifiedStatus.VERIFIED ||
        status === VerifiedStatus.UNVERIFIED) {
        return true;
    }
    return false;
}
const identityKeySchema = zod_1.z.object({
    id: zod_1.z.string(),
    publicKey: zod_1.z.instanceof(Uint8Array),
    firstUse: zod_1.z.boolean(),
    timestamp: zod_1.z.number().refine((value) => value % 1 === 0 && value > 0),
    verified: zod_1.z.number().refine(validateVerifiedStatus),
    nonblockingApproval: zod_1.z.boolean(),
});
function validateIdentityKey(attrs) {
    // We'll throw if this doesn't match
    identityKeySchema.parse(attrs);
    return true;
}
exports.GLOBAL_ZONE = new Zone_1.Zone('GLOBAL_ZONE');
async function _fillCaches(object, field, itemsPromise) {
    const items = await itemsPromise;
    const cache = new Map();
    for (let i = 0, max = items.length; i < max; i += 1) {
        const fromDB = items[i];
        const { id } = fromDB;
        cache.set(id, {
            fromDB,
            hydrated: false,
        });
    }
    log.info(`SignalProtocolStore: Finished caching ${field} data`);
    // eslint-disable-next-line no-param-reassign, @typescript-eslint/no-explicit-any
    object[field] = cache;
}
function hydrateSession(session) {
    return signal_client_1.SessionRecord.deserialize(Buffer.from(session.record, 'base64'));
}
exports.hydrateSession = hydrateSession;
function hydratePublicKey(identityKey) {
    return signal_client_1.PublicKey.deserialize(Buffer.from(identityKey.publicKey));
}
exports.hydratePublicKey = hydratePublicKey;
function hydratePreKey(preKey) {
    const publicKey = signal_client_1.PublicKey.deserialize(Buffer.from(preKey.publicKey));
    const privateKey = signal_client_1.PrivateKey.deserialize(Buffer.from(preKey.privateKey));
    return signal_client_1.PreKeyRecord.new(preKey.keyId, publicKey, privateKey);
}
exports.hydratePreKey = hydratePreKey;
function hydrateSignedPreKey(signedPreKey) {
    const createdAt = signedPreKey.created_at;
    const pubKey = signal_client_1.PublicKey.deserialize(Buffer.from(signedPreKey.publicKey));
    const privKey = signal_client_1.PrivateKey.deserialize(Buffer.from(signedPreKey.privateKey));
    const signature = Buffer.from([]);
    return signal_client_1.SignedPreKeyRecord.new(signedPreKey.keyId, createdAt, pubKey, privKey, signature);
}
exports.hydrateSignedPreKey = hydrateSignedPreKey;
function freezeSession(session) {
    return session.serialize().toString('base64');
}
exports.freezeSession = freezeSession;
function freezePublicKey(publicKey) {
    return publicKey.serialize();
}
exports.freezePublicKey = freezePublicKey;
function freezePreKey(preKey) {
    const keyPair = {
        pubKey: preKey.publicKey().serialize(),
        privKey: preKey.privateKey().serialize(),
    };
    return keyPair;
}
exports.freezePreKey = freezePreKey;
function freezeSignedPreKey(signedPreKey) {
    const keyPair = {
        pubKey: signedPreKey.publicKey().serialize(),
        privKey: signedPreKey.privateKey().serialize(),
    };
    return keyPair;
}
exports.freezeSignedPreKey = freezeSignedPreKey;
// We add a this parameter to avoid an 'implicit any' error on the next line
const EventsMixin = function EventsMixin() {
    window._.assign(this, window.Backbone.Events);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
};
class SignalProtocolStore extends EventsMixin {
    constructor() {
        // Enums used across the app
        super(...arguments);
        this.VerifiedStatus = VerifiedStatus;
        // Cached values
        this.ourIdentityKeys = new Map();
        this.ourRegistrationIds = new Map();
        this.senderKeyQueues = new Map();
        this.sessionQueues = new Map();
        this.currentZoneDepth = 0;
        this.zoneQueue = [];
        this.pendingSessions = new Map();
        this.pendingUnprocessed = new Map();
    }
    async hydrateCaches() {
        await Promise.all([
            (async () => {
                this.ourIdentityKeys.clear();
                const map = await window.Signal.Data.getItemById('identityKeyMap');
                if (!map) {
                    return;
                }
                for (const key of Object.keys(map.value)) {
                    const { privKey, pubKey } = map.value[key];
                    this.ourIdentityKeys.set(new UUID_1.UUID(key).toString(), {
                        privKey: Bytes.fromBase64(privKey),
                        pubKey: Bytes.fromBase64(pubKey),
                    });
                }
            })(),
            (async () => {
                this.ourRegistrationIds.clear();
                const map = await window.Signal.Data.getItemById('registrationIdMap');
                if (!map) {
                    return;
                }
                for (const key of Object.keys(map.value)) {
                    this.ourRegistrationIds.set(new UUID_1.UUID(key).toString(), map.value[key]);
                }
            })(),
            _fillCaches(this, 'identityKeys', window.Signal.Data.getAllIdentityKeys()),
            _fillCaches(this, 'sessions', window.Signal.Data.getAllSessions()),
            _fillCaches(this, 'preKeys', window.Signal.Data.getAllPreKeys()),
            _fillCaches(this, 'senderKeys', window.Signal.Data.getAllSenderKeys()),
            _fillCaches(this, 'signedPreKeys', window.Signal.Data.getAllSignedPreKeys()),
        ]);
    }
    async getIdentityKeyPair(ourUuid) {
        return this.ourIdentityKeys.get(ourUuid.toString());
    }
    async getLocalRegistrationId(ourUuid) {
        return this.ourRegistrationIds.get(ourUuid.toString());
    }
    // PreKeys
    async loadPreKey(ourUuid, keyId) {
        if (!this.preKeys) {
            throw new Error('loadPreKey: this.preKeys not yet cached!');
        }
        const id = `${ourUuid.toString()}:${keyId}`;
        const entry = this.preKeys.get(id);
        if (!entry) {
            log.error('Failed to fetch prekey:', id);
            return undefined;
        }
        if (entry.hydrated) {
            log.info('Successfully fetched prekey (cache hit):', id);
            return entry.item;
        }
        const item = hydratePreKey(entry.fromDB);
        this.preKeys.set(id, {
            hydrated: true,
            fromDB: entry.fromDB,
            item,
        });
        log.info('Successfully fetched prekey (cache miss):', id);
        return item;
    }
    async storePreKey(ourUuid, keyId, keyPair) {
        if (!this.preKeys) {
            throw new Error('storePreKey: this.preKeys not yet cached!');
        }
        const id = `${ourUuid.toString()}:${keyId}`;
        if (this.preKeys.has(id)) {
            throw new Error(`storePreKey: prekey ${id} already exists!`);
        }
        const fromDB = {
            id,
            keyId,
            ourUuid: ourUuid.toString(),
            publicKey: keyPair.pubKey,
            privateKey: keyPair.privKey,
        };
        await window.Signal.Data.createOrUpdatePreKey(fromDB);
        this.preKeys.set(id, {
            hydrated: false,
            fromDB,
        });
    }
    async removePreKey(ourUuid, keyId) {
        if (!this.preKeys) {
            throw new Error('removePreKey: this.preKeys not yet cached!');
        }
        const id = `${ourUuid.toString()}:${keyId}`;
        try {
            this.trigger('removePreKey');
        }
        catch (error) {
            log.error('removePreKey error triggering removePreKey:', error && error.stack ? error.stack : error);
        }
        this.preKeys.delete(id);
        await window.Signal.Data.removePreKeyById(id);
    }
    async clearPreKeyStore() {
        if (this.preKeys) {
            this.preKeys.clear();
        }
        await window.Signal.Data.removeAllPreKeys();
    }
    // Signed PreKeys
    async loadSignedPreKey(ourUuid, keyId) {
        if (!this.signedPreKeys) {
            throw new Error('loadSignedPreKey: this.signedPreKeys not yet cached!');
        }
        const id = `${ourUuid.toString()}:${keyId}`;
        const entry = this.signedPreKeys.get(id);
        if (!entry) {
            log.error('Failed to fetch signed prekey:', id);
            return undefined;
        }
        if (entry.hydrated) {
            log.info('Successfully fetched signed prekey (cache hit):', id);
            return entry.item;
        }
        const item = hydrateSignedPreKey(entry.fromDB);
        this.signedPreKeys.set(id, {
            hydrated: true,
            item,
            fromDB: entry.fromDB,
        });
        log.info('Successfully fetched signed prekey (cache miss):', id);
        return item;
    }
    async loadSignedPreKeys(ourUuid) {
        if (!this.signedPreKeys) {
            throw new Error('loadSignedPreKeys: this.signedPreKeys not yet cached!');
        }
        if (arguments.length > 1) {
            throw new Error('loadSignedPreKeys takes one argument');
        }
        const entries = Array.from(this.signedPreKeys.values());
        return entries
            .filter(({ fromDB }) => fromDB.ourUuid === ourUuid.toString())
            .map(entry => {
            const preKey = entry.fromDB;
            return {
                pubKey: preKey.publicKey,
                privKey: preKey.privateKey,
                created_at: preKey.created_at,
                keyId: preKey.keyId,
                confirmed: preKey.confirmed,
            };
        });
    }
    // Note that this is also called in update scenarios, for confirming that signed prekeys
    //   have indeed been accepted by the server.
    async storeSignedPreKey(ourUuid, keyId, keyPair, confirmed) {
        if (!this.signedPreKeys) {
            throw new Error('storeSignedPreKey: this.signedPreKeys not yet cached!');
        }
        const id = `${ourUuid.toString()}:${keyId}`;
        const fromDB = {
            id,
            ourUuid: ourUuid.toString(),
            keyId,
            publicKey: keyPair.pubKey,
            privateKey: keyPair.privKey,
            created_at: Date.now(),
            confirmed: Boolean(confirmed),
        };
        await window.Signal.Data.createOrUpdateSignedPreKey(fromDB);
        this.signedPreKeys.set(id, {
            hydrated: false,
            fromDB,
        });
    }
    async removeSignedPreKey(ourUuid, keyId) {
        if (!this.signedPreKeys) {
            throw new Error('removeSignedPreKey: this.signedPreKeys not yet cached!');
        }
        const id = `${ourUuid.toString()}:${keyId}`;
        this.signedPreKeys.delete(id);
        await window.Signal.Data.removeSignedPreKeyById(id);
    }
    async clearSignedPreKeysStore() {
        if (this.signedPreKeys) {
            this.signedPreKeys.clear();
        }
        await window.Signal.Data.removeAllSignedPreKeys();
    }
    // Sender Key Queue
    async enqueueSenderKeyJob(qualifiedAddress, task, zone = exports.GLOBAL_ZONE) {
        return this.withZone(zone, 'enqueueSenderKeyJob', async () => {
            const queue = this._getSenderKeyQueue(qualifiedAddress);
            return queue.add(task);
        });
    }
    _createSenderKeyQueue() {
        return new p_queue_1.default({ concurrency: 1, timeout: 1000 * 60 * 2 });
    }
    _getSenderKeyQueue(senderId) {
        const cachedQueue = this.senderKeyQueues.get(senderId.toString());
        if (cachedQueue) {
            return cachedQueue;
        }
        const freshQueue = this._createSenderKeyQueue();
        this.senderKeyQueues.set(senderId.toString(), freshQueue);
        return freshQueue;
    }
    // Sender Keys
    getSenderKeyId(senderKeyId, distributionId) {
        return `${senderKeyId.toString()}--${distributionId}`;
    }
    async saveSenderKey(qualifiedAddress, distributionId, record) {
        if (!this.senderKeys) {
            throw new Error('saveSenderKey: this.senderKeys not yet cached!');
        }
        const senderId = qualifiedAddress.toString();
        try {
            const id = this.getSenderKeyId(qualifiedAddress, distributionId);
            const fromDB = {
                id,
                senderId,
                distributionId,
                data: record.serialize(),
                lastUpdatedDate: Date.now(),
            };
            await window.Signal.Data.createOrUpdateSenderKey(fromDB);
            this.senderKeys.set(id, {
                hydrated: true,
                fromDB,
                item: record,
            });
        }
        catch (error) {
            const errorString = error && error.stack ? error.stack : error;
            log.error(`saveSenderKey: failed to save senderKey ${senderId}/${distributionId}: ${errorString}`);
        }
    }
    async getSenderKey(qualifiedAddress, distributionId) {
        if (!this.senderKeys) {
            throw new Error('getSenderKey: this.senderKeys not yet cached!');
        }
        const senderId = qualifiedAddress.toString();
        try {
            const id = this.getSenderKeyId(qualifiedAddress, distributionId);
            const entry = this.senderKeys.get(id);
            if (!entry) {
                log.error('Failed to fetch sender key:', id);
                return undefined;
            }
            if (entry.hydrated) {
                log.info('Successfully fetched sender key (cache hit):', id);
                return entry.item;
            }
            const item = signal_client_1.SenderKeyRecord.deserialize(Buffer.from(entry.fromDB.data));
            this.senderKeys.set(id, {
                hydrated: true,
                item,
                fromDB: entry.fromDB,
            });
            log.info('Successfully fetched sender key(cache miss):', id);
            return item;
        }
        catch (error) {
            const errorString = error && error.stack ? error.stack : error;
            log.error(`getSenderKey: failed to load sender key ${senderId}/${distributionId}: ${errorString}`);
            return undefined;
        }
    }
    async removeSenderKey(qualifiedAddress, distributionId) {
        if (!this.senderKeys) {
            throw new Error('getSenderKey: this.senderKeys not yet cached!');
        }
        const senderId = qualifiedAddress.toString();
        try {
            const id = this.getSenderKeyId(qualifiedAddress, distributionId);
            await window.Signal.Data.removeSenderKeyById(id);
            this.senderKeys.delete(id);
        }
        catch (error) {
            const errorString = error && error.stack ? error.stack : error;
            log.error(`removeSenderKey: failed to remove senderKey ${senderId}/${distributionId}: ${errorString}`);
        }
    }
    async clearSenderKeyStore() {
        if (this.senderKeys) {
            this.senderKeys.clear();
        }
        await window.Signal.Data.removeAllSenderKeys();
    }
    // Session Queue
    async enqueueSessionJob(qualifiedAddress, task, zone = exports.GLOBAL_ZONE) {
        return this.withZone(zone, 'enqueueSessionJob', async () => {
            const queue = this._getSessionQueue(qualifiedAddress);
            return queue.add(task);
        });
    }
    _createSessionQueue() {
        return new p_queue_1.default({ concurrency: 1, timeout: 1000 * 60 * 2 });
    }
    _getSessionQueue(id) {
        const cachedQueue = this.sessionQueues.get(id.toString());
        if (cachedQueue) {
            return cachedQueue;
        }
        const freshQueue = this._createSessionQueue();
        this.sessionQueues.set(id.toString(), freshQueue);
        return freshQueue;
    }
    // Sessions
    // Re-entrant session transaction routine. Only one session transaction could
    // be running at the same time.
    //
    // While in transaction:
    //
    // - `storeSession()` adds the updated session to the `pendingSessions`
    // - `loadSession()` looks up the session first in `pendingSessions` and only
    //   then in the main `sessions` store
    //
    // When transaction ends:
    //
    // - successfully: pending session stores are batched into the database
    // - with an error: pending session stores are reverted
    async withZone(zone, name, body) {
        const debugName = `withZone(${zone.name}:${name})`;
        // Allow re-entering from LibSignalStores
        if (this.currentZone && this.currentZone !== zone) {
            const start = Date.now();
            log.info(`${debugName}: locked by ${this.currentZone.name}, waiting`);
            return new Promise((resolve, reject) => {
                const callback = async () => {
                    const duration = Date.now() - start;
                    log.info(`${debugName}: unlocked after ${duration}ms`);
                    // Call `.withZone` synchronously from `this.zoneQueue` to avoid
                    // extra in-between ticks while we are on microtasks queue.
                    try {
                        resolve(await this.withZone(zone, name, body));
                    }
                    catch (error) {
                        reject(error);
                    }
                };
                this.zoneQueue.push({ zone, callback });
            });
        }
        this.enterZone(zone, name);
        let result;
        try {
            result = await body();
        }
        catch (error) {
            if (this.isInTopLevelZone()) {
                await this.revertZoneChanges(name, error);
            }
            this.leaveZone(zone);
            throw error;
        }
        if (this.isInTopLevelZone()) {
            await this.commitZoneChanges(name);
        }
        this.leaveZone(zone);
        return result;
    }
    async commitZoneChanges(name) {
        const { pendingSessions, pendingUnprocessed } = this;
        if (pendingSessions.size === 0 && pendingUnprocessed.size === 0) {
            return;
        }
        log.info(`commitZoneChanges(${name}): pending sessions ${pendingSessions.size} ` +
            `pending unprocessed ${pendingUnprocessed.size}`);
        this.pendingSessions = new Map();
        this.pendingUnprocessed = new Map();
        // Commit both unprocessed and sessions in the same database transaction
        // to unroll both on error.
        await window.Signal.Data.commitSessionsAndUnprocessed({
            sessions: Array.from(pendingSessions.values()).map(({ fromDB }) => fromDB),
            unprocessed: Array.from(pendingUnprocessed.values()),
        });
        const { sessions } = this;
        (0, assert_1.assert)(sessions !== undefined, "Can't commit unhydrated storage");
        // Apply changes to in-memory storage after successful DB write.
        pendingSessions.forEach((value, key) => {
            sessions.set(key, value);
        });
    }
    async revertZoneChanges(name, error) {
        log.info(`revertZoneChanges(${name}): ` +
            `pending sessions size ${this.pendingSessions.size} ` +
            `pending unprocessed size ${this.pendingUnprocessed.size}`, error && error.stack);
        this.pendingSessions.clear();
        this.pendingUnprocessed.clear();
    }
    isInTopLevelZone() {
        return this.currentZoneDepth === 1;
    }
    enterZone(zone, name) {
        this.currentZoneDepth += 1;
        if (this.currentZoneDepth === 1) {
            (0, assert_1.assert)(this.currentZone === undefined, 'Should not be in the zone');
            this.currentZone = zone;
            if (zone !== exports.GLOBAL_ZONE) {
                log.info(`SignalProtocolStore.enterZone(${zone.name}:${name})`);
            }
        }
    }
    leaveZone(zone) {
        var _a;
        (0, assert_1.assert)(this.currentZone === zone, 'Should be in the correct zone');
        this.currentZoneDepth -= 1;
        (0, assert_1.assert)(this.currentZoneDepth >= 0, 'Unmatched number of leaveZone calls');
        // Since we allow re-entering zones we might actually be in two overlapping
        // async calls. Leave the zone and yield to another one only if there are
        // no active zone users anymore.
        if (this.currentZoneDepth !== 0) {
            return;
        }
        if (zone !== exports.GLOBAL_ZONE) {
            log.info(`SignalProtocolStore.leaveZone(${zone.name})`);
        }
        this.currentZone = undefined;
        const next = this.zoneQueue.shift();
        if (!next) {
            return;
        }
        const toEnter = [next];
        while (((_a = this.zoneQueue[0]) === null || _a === void 0 ? void 0 : _a.zone) === next.zone) {
            const elem = this.zoneQueue.shift();
            (0, assert_1.assert)(elem, 'Zone element should be present');
            toEnter.push(elem);
        }
        log.info(`SignalProtocolStore: running blocked ${toEnter.length} jobs in ` +
            `zone ${next.zone.name}`);
        for (const { callback } of toEnter) {
            callback();
        }
    }
    async loadSession(qualifiedAddress, { zone = exports.GLOBAL_ZONE } = {}) {
        return this.withZone(zone, 'loadSession', async () => {
            if (!this.sessions) {
                throw new Error('loadSession: this.sessions not yet cached!');
            }
            if (qualifiedAddress === null || qualifiedAddress === undefined) {
                throw new Error('loadSession: qualifiedAddress was undefined/null');
            }
            const id = qualifiedAddress.toString();
            try {
                const map = this.pendingSessions.has(id)
                    ? this.pendingSessions
                    : this.sessions;
                const entry = map.get(id);
                if (!entry) {
                    return undefined;
                }
                if (entry.hydrated) {
                    return entry.item;
                }
                // We'll either just hydrate the item or we'll fully migrate the session
                //   and save it to the database.
                return await this._maybeMigrateSession(entry.fromDB, { zone });
            }
            catch (error) {
                const errorString = error && error.stack ? error.stack : error;
                log.error(`loadSession: failed to load session ${id}: ${errorString}`);
                return undefined;
            }
        });
    }
    async loadSessions(qualifiedAddresses, { zone = exports.GLOBAL_ZONE } = {}) {
        return this.withZone(zone, 'loadSessions', async () => {
            const sessions = await Promise.all(qualifiedAddresses.map(async (address) => this.loadSession(address, { zone })));
            return sessions.filter(isNotNil_1.isNotNil);
        });
    }
    async _maybeMigrateSession(session, { zone = exports.GLOBAL_ZONE } = {}) {
        if (!this.sessions) {
            throw new Error('_maybeMigrateSession: this.sessions not yet cached!');
        }
        // Already migrated, hydrate and update cache
        if (session.version === 2) {
            const item = hydrateSession(session);
            const map = this.pendingSessions.has(session.id)
                ? this.pendingSessions
                : this.sessions;
            map.set(session.id, {
                hydrated: true,
                item,
                fromDB: session,
            });
            return item;
        }
        // Not yet converted, need to translate to new format and save
        if (session.version !== undefined) {
            throw new Error('_maybeMigrateSession: Unknown session version type!');
        }
        const ourUuid = new UUID_1.UUID(session.ourUuid);
        const keyPair = await this.getIdentityKeyPair(ourUuid);
        if (!keyPair) {
            throw new Error('_maybeMigrateSession: No identity key for ourself!');
        }
        const localRegistrationId = await this.getLocalRegistrationId(ourUuid);
        if (!(0, lodash_1.isNumber)(localRegistrationId)) {
            throw new Error('_maybeMigrateSession: No registration id for ourself!');
        }
        const localUserData = {
            identityKeyPublic: keyPair.pubKey,
            registrationId: localRegistrationId,
        };
        log.info(`_maybeMigrateSession: Migrating session with id ${session.id}`);
        const sessionProto = (0, sessionTranslation_1.sessionRecordToProtobuf)(JSON.parse(session.record), localUserData);
        const record = signal_client_1.SessionRecord.deserialize(Buffer.from((0, sessionTranslation_1.sessionStructureToBytes)(sessionProto)));
        await this.storeSession(QualifiedAddress_1.QualifiedAddress.parse(session.id), record, {
            zone,
        });
        return record;
    }
    async storeSession(qualifiedAddress, record, { zone = exports.GLOBAL_ZONE } = {}) {
        await this.withZone(zone, 'storeSession', async () => {
            if (!this.sessions) {
                throw new Error('storeSession: this.sessions not yet cached!');
            }
            if (qualifiedAddress === null || qualifiedAddress === undefined) {
                throw new Error('storeSession: qualifiedAddress was undefined/null');
            }
            const { uuid, deviceId } = qualifiedAddress;
            const conversationId = window.ConversationController.ensureContactIds({
                uuid: uuid.toString(),
            });
            (0, assert_1.strictAssert)(conversationId !== undefined, 'storeSession: Ensure contact ids failed');
            const id = qualifiedAddress.toString();
            try {
                const fromDB = {
                    id,
                    version: 2,
                    ourUuid: qualifiedAddress.ourUuid.toString(),
                    conversationId,
                    uuid: uuid.toString(),
                    deviceId,
                    record: record.serialize().toString('base64'),
                };
                const newSession = {
                    hydrated: true,
                    fromDB,
                    item: record,
                };
                (0, assert_1.assert)(this.currentZone, 'Must run in the zone');
                this.pendingSessions.set(id, newSession);
                // Current zone doesn't support pending sessions - commit immediately
                if (!zone.supportsPendingSessions()) {
                    await this.commitZoneChanges('storeSession');
                }
            }
            catch (error) {
                const errorString = error && error.stack ? error.stack : error;
                log.error(`storeSession: Save failed for ${id}: ${errorString}`);
                throw error;
            }
        });
    }
    async getOpenDevices(ourUuid, identifiers, { zone = exports.GLOBAL_ZONE } = {}) {
        return this.withZone(zone, 'getOpenDevices', async () => {
            if (!this.sessions) {
                throw new Error('getOpenDevices: this.sessions not yet cached!');
            }
            if (identifiers.length === 0) {
                throw new Error('getOpenDevices: No identifiers provided!');
            }
            try {
                const uuidsOrIdentifiers = new Set(identifiers.map(identifier => { var _a; return ((_a = UUID_1.UUID.lookup(identifier)) === null || _a === void 0 ? void 0 : _a.toString()) || identifier; }));
                const allSessions = this._getAllSessions();
                const entries = allSessions.filter(({ fromDB }) => fromDB.ourUuid === ourUuid.toString() &&
                    uuidsOrIdentifiers.has(fromDB.uuid));
                const openEntries = await Promise.all(entries.map(async (entry) => {
                    if (entry.hydrated) {
                        const record = entry.item;
                        if (record.hasCurrentState()) {
                            return { record, entry };
                        }
                        return undefined;
                    }
                    const record = await this._maybeMigrateSession(entry.fromDB, {
                        zone,
                    });
                    if (record.hasCurrentState()) {
                        return { record, entry };
                    }
                    return undefined;
                }));
                const devices = openEntries
                    .map(item => {
                    if (!item) {
                        return undefined;
                    }
                    const { entry, record } = item;
                    const { uuid } = entry.fromDB;
                    uuidsOrIdentifiers.delete(uuid);
                    const id = entry.fromDB.deviceId;
                    const registrationId = record.remoteRegistrationId();
                    return {
                        identifier: uuid,
                        id,
                        registrationId,
                    };
                })
                    .filter(isNotNil_1.isNotNil);
                const emptyIdentifiers = Array.from(uuidsOrIdentifiers.values());
                return {
                    devices,
                    emptyIdentifiers,
                };
            }
            catch (error) {
                log.error('getOpenDevices: Failed to get devices', error && error.stack ? error.stack : error);
                throw error;
            }
        });
    }
    async getDeviceIds({ ourUuid, identifier, }) {
        const { devices } = await this.getOpenDevices(ourUuid, [identifier]);
        return devices.map((device) => device.id);
    }
    async removeSession(qualifiedAddress) {
        return this.withZone(exports.GLOBAL_ZONE, 'removeSession', async () => {
            if (!this.sessions) {
                throw new Error('removeSession: this.sessions not yet cached!');
            }
            const id = qualifiedAddress.toString();
            log.info('removeSession: deleting session for', id);
            try {
                await window.Signal.Data.removeSessionById(id);
                this.sessions.delete(id);
                this.pendingSessions.delete(id);
            }
            catch (e) {
                log.error(`removeSession: Failed to delete session for ${id}`);
            }
        });
    }
    async removeAllSessions(identifier) {
        return this.withZone(exports.GLOBAL_ZONE, 'removeAllSessions', async () => {
            if (!this.sessions) {
                throw new Error('removeAllSessions: this.sessions not yet cached!');
            }
            if (identifier === null || identifier === undefined) {
                throw new Error('removeAllSessions: identifier was undefined/null');
            }
            log.info('removeAllSessions: deleting sessions for', identifier);
            const id = window.ConversationController.getConversationId(identifier);
            (0, assert_1.strictAssert)(id, `removeAllSessions: Conversation not found: ${identifier}`);
            const entries = Array.from(this.sessions.values());
            for (let i = 0, max = entries.length; i < max; i += 1) {
                const entry = entries[i];
                if (entry.fromDB.conversationId === id) {
                    this.sessions.delete(entry.fromDB.id);
                    this.pendingSessions.delete(entry.fromDB.id);
                }
            }
            await window.Signal.Data.removeSessionsByConversation(id);
        });
    }
    async _archiveSession(entry, zone) {
        if (!entry) {
            return;
        }
        const addr = QualifiedAddress_1.QualifiedAddress.parse(entry.fromDB.id);
        await this.enqueueSessionJob(addr, async () => {
            const item = entry.hydrated
                ? entry.item
                : await this._maybeMigrateSession(entry.fromDB, { zone });
            if (!item.hasCurrentState()) {
                return;
            }
            item.archiveCurrentState();
            await this.storeSession(addr, item, { zone });
        }, zone);
    }
    async archiveSession(qualifiedAddress) {
        return this.withZone(exports.GLOBAL_ZONE, 'archiveSession', async () => {
            if (!this.sessions) {
                throw new Error('archiveSession: this.sessions not yet cached!');
            }
            const id = qualifiedAddress.toString();
            log.info(`archiveSession: session for ${id}`);
            const entry = this.pendingSessions.get(id) || this.sessions.get(id);
            await this._archiveSession(entry);
        });
    }
    async archiveSiblingSessions(encodedAddress, { zone = exports.GLOBAL_ZONE } = {}) {
        return this.withZone(zone, 'archiveSiblingSessions', async () => {
            if (!this.sessions) {
                throw new Error('archiveSiblingSessions: this.sessions not yet cached!');
            }
            log.info('archiveSiblingSessions: archiving sibling sessions for', encodedAddress.toString());
            const { uuid, deviceId } = encodedAddress;
            const allEntries = this._getAllSessions();
            const entries = allEntries.filter(entry => entry.fromDB.uuid === uuid.toString() &&
                entry.fromDB.deviceId !== deviceId);
            await Promise.all(entries.map(async (entry) => {
                await this._archiveSession(entry, zone);
            }));
        });
    }
    async archiveAllSessions(uuid) {
        return this.withZone(exports.GLOBAL_ZONE, 'archiveAllSessions', async () => {
            if (!this.sessions) {
                throw new Error('archiveAllSessions: this.sessions not yet cached!');
            }
            log.info('archiveAllSessions: archiving all sessions for', uuid.toString());
            const allEntries = this._getAllSessions();
            const entries = allEntries.filter(entry => entry.fromDB.uuid === uuid.toString());
            await Promise.all(entries.map(async (entry) => {
                await this._archiveSession(entry);
            }));
        });
    }
    async clearSessionStore() {
        return this.withZone(exports.GLOBAL_ZONE, 'clearSessionStore', async () => {
            if (this.sessions) {
                this.sessions.clear();
            }
            this.pendingSessions.clear();
            await window.Signal.Data.removeAllSessions();
        });
    }
    async lightSessionReset(qualifiedAddress) {
        const id = qualifiedAddress.toString();
        const sessionResets = window.storage.get('sessionResets', {});
        const lastReset = sessionResets[id];
        const ONE_HOUR = 60 * 60 * 1000;
        if (lastReset && (0, timestamp_1.isMoreRecentThan)(lastReset, ONE_HOUR)) {
            log.warn(`lightSessionReset/${id}: Skipping session reset, last reset at ${lastReset}`);
            return;
        }
        sessionResets[id] = Date.now();
        window.storage.put('sessionResets', sessionResets);
        try {
            const { uuid } = qualifiedAddress;
            // First, fetch this conversation
            const conversationId = window.ConversationController.ensureContactIds({
                uuid: uuid.toString(),
            });
            (0, assert_1.assert)(conversationId, `lightSessionReset/${id}: missing conversationId`);
            const conversation = window.ConversationController.get(conversationId);
            (0, assert_1.assert)(conversation, `lightSessionReset/${id}: missing conversation`);
            log.warn(`lightSessionReset/${id}: Resetting session`);
            // Archive open session with this device
            await this.archiveSession(qualifiedAddress);
            // Send a null message with newly-created session
            const sendOptions = await (0, getSendOptions_1.getSendOptions)(conversation.attributes);
            const result = await (0, handleMessageSend_1.handleMessageSend)(window.textsecure.messaging.sendNullMessage({
                uuid: uuid.toString(),
            }, sendOptions), { messageIds: [], sendType: 'nullMessage' });
            if (result && result.errors && result.errors.length) {
                throw result.errors[0];
            }
        }
        catch (error) {
            // If we failed to do the session reset, then we'll allow another attempt sooner
            //   than one hour from now.
            delete sessionResets[id];
            window.storage.put('sessionResets', sessionResets);
            const errorString = error && error.stack ? error.stack : error;
            log.error(`lightSessionReset/${id}: Encountered error`, errorString);
        }
    }
    // Identity Keys
    getIdentityRecord(uuid) {
        if (!this.identityKeys) {
            throw new Error('getIdentityRecord: this.identityKeys not yet cached!');
        }
        const id = uuid.toString();
        try {
            const entry = this.identityKeys.get(id);
            if (!entry) {
                return undefined;
            }
            return entry.fromDB;
        }
        catch (e) {
            log.error(`getIdentityRecord: Failed to get identity record for identifier ${id}`);
            return undefined;
        }
    }
    async getOrMigrateIdentityRecord(uuid) {
        if (!this.identityKeys) {
            throw new Error('getOrMigrateIdentityRecord: this.identityKeys not yet cached!');
        }
        const result = this.getIdentityRecord(uuid);
        if (result) {
            return result;
        }
        const newId = uuid.toString();
        const conversation = window.ConversationController.get(newId);
        if (!conversation) {
            return undefined;
        }
        const conversationId = conversation.id;
        const record = this.identityKeys.get(`conversation:${conversationId}`);
        if (!record) {
            return undefined;
        }
        const newRecord = Object.assign(Object.assign({}, record.fromDB), { id: newId });
        log.info(`SignalProtocolStore: migrating identity key from ${record.fromDB.id} ` +
            `to ${newRecord.id}`);
        await this._saveIdentityKey(newRecord);
        this.identityKeys.delete(record.fromDB.id);
        await window.Signal.Data.removeIdentityKeyById(record.fromDB.id);
        return newRecord;
    }
    async isTrustedIdentity(encodedAddress, publicKey, direction) {
        if (!this.identityKeys) {
            throw new Error('isTrustedIdentity: this.identityKeys not yet cached!');
        }
        if (encodedAddress === null || encodedAddress === undefined) {
            throw new Error('isTrustedIdentity: encodedAddress was undefined/null');
        }
        const ourUuid = window.textsecure.storage.user.getCheckedUuid();
        const isOurIdentifier = encodedAddress.uuid.isEqual(ourUuid);
        const identityRecord = await this.getOrMigrateIdentityRecord(encodedAddress.uuid);
        if (isOurIdentifier) {
            if (identityRecord && identityRecord.publicKey) {
                return (0, Crypto_1.constantTimeEqual)(identityRecord.publicKey, publicKey);
            }
            log.warn('isTrustedIdentity: No local record for our own identifier. Returning true.');
            return true;
        }
        switch (direction) {
            case 0 /* Sending */:
                return this.isTrustedForSending(publicKey, identityRecord);
            case 1 /* Receiving */:
                return true;
            default:
                throw new Error(`isTrustedIdentity: Unknown direction: ${direction}`);
        }
    }
    isTrustedForSending(publicKey, identityRecord) {
        if (!identityRecord) {
            log.info('isTrustedForSending: No previous record, returning true...');
            return true;
        }
        const existing = identityRecord.publicKey;
        if (!existing) {
            log.info('isTrustedForSending: Nothing here, returning true...');
            return true;
        }
        if (!(0, Crypto_1.constantTimeEqual)(existing, publicKey)) {
            log.info("isTrustedForSending: Identity keys don't match...");
            return false;
        }
        if (identityRecord.verified === VerifiedStatus.UNVERIFIED) {
            log.error('isTrustedIdentity: Needs unverified approval!');
            return false;
        }
        if (this.isNonBlockingApprovalRequired(identityRecord)) {
            log.error('isTrustedForSending: Needs non-blocking approval!');
            return false;
        }
        return true;
    }
    async loadIdentityKey(uuid) {
        if (uuid === null || uuid === undefined) {
            throw new Error('loadIdentityKey: uuid was undefined/null');
        }
        const identityRecord = await this.getOrMigrateIdentityRecord(uuid);
        if (identityRecord) {
            return identityRecord.publicKey;
        }
        return undefined;
    }
    async _saveIdentityKey(data) {
        if (!this.identityKeys) {
            throw new Error('_saveIdentityKey: this.identityKeys not yet cached!');
        }
        const { id } = data;
        await window.Signal.Data.createOrUpdateIdentityKey(data);
        this.identityKeys.set(id, {
            hydrated: false,
            fromDB: data,
        });
    }
    async saveIdentity(encodedAddress, publicKey, nonblockingApproval = false, { zone } = {}) {
        if (!this.identityKeys) {
            throw new Error('saveIdentity: this.identityKeys not yet cached!');
        }
        if (encodedAddress === null || encodedAddress === undefined) {
            throw new Error('saveIdentity: encodedAddress was undefined/null');
        }
        if (!(publicKey instanceof Uint8Array)) {
            // eslint-disable-next-line no-param-reassign
            publicKey = Bytes.fromBinary(publicKey);
        }
        if (typeof nonblockingApproval !== 'boolean') {
            // eslint-disable-next-line no-param-reassign
            nonblockingApproval = false;
        }
        const identityRecord = await this.getOrMigrateIdentityRecord(encodedAddress.uuid);
        const id = encodedAddress.uuid.toString();
        if (!identityRecord || !identityRecord.publicKey) {
            // Lookup failed, or the current key was removed, so save this one.
            log.info('saveIdentity: Saving new identity...');
            await this._saveIdentityKey({
                id,
                publicKey,
                firstUse: true,
                timestamp: Date.now(),
                verified: VerifiedStatus.DEFAULT,
                nonblockingApproval,
            });
            return false;
        }
        const oldpublicKey = identityRecord.publicKey;
        if (!(0, Crypto_1.constantTimeEqual)(oldpublicKey, publicKey)) {
            log.info('saveIdentity: Replacing existing identity...');
            const previousStatus = identityRecord.verified;
            let verifiedStatus;
            if (previousStatus === VerifiedStatus.VERIFIED ||
                previousStatus === VerifiedStatus.UNVERIFIED) {
                verifiedStatus = VerifiedStatus.UNVERIFIED;
            }
            else {
                verifiedStatus = VerifiedStatus.DEFAULT;
            }
            await this._saveIdentityKey({
                id,
                publicKey,
                firstUse: false,
                timestamp: Date.now(),
                verified: verifiedStatus,
                nonblockingApproval,
            });
            try {
                this.trigger('keychange', encodedAddress.uuid);
            }
            catch (error) {
                log.error('saveIdentity: error triggering keychange:', error && error.stack ? error.stack : error);
            }
            // Pass the zone to facilitate transactional session use in
            // MessageReceiver.ts
            await this.archiveSiblingSessions(encodedAddress, {
                zone,
            });
            return true;
        }
        if (this.isNonBlockingApprovalRequired(identityRecord)) {
            log.info('saveIdentity: Setting approval status...');
            identityRecord.nonblockingApproval = nonblockingApproval;
            await this._saveIdentityKey(identityRecord);
            return false;
        }
        return false;
    }
    isNonBlockingApprovalRequired(identityRecord) {
        return (!identityRecord.firstUse &&
            (0, timestamp_1.isMoreRecentThan)(identityRecord.timestamp, TIMESTAMP_THRESHOLD) &&
            !identityRecord.nonblockingApproval);
    }
    async saveIdentityWithAttributes(uuid, attributes) {
        if (uuid === null || uuid === undefined) {
            throw new Error('saveIdentityWithAttributes: uuid was undefined/null');
        }
        const identityRecord = await this.getOrMigrateIdentityRecord(uuid);
        const id = uuid.toString();
        window.ConversationController.getOrCreate(id, 'private');
        const updates = Object.assign(Object.assign(Object.assign({}, identityRecord), attributes), { id });
        if (validateIdentityKey(updates)) {
            await this._saveIdentityKey(updates);
        }
    }
    async setApproval(uuid, nonblockingApproval) {
        if (uuid === null || uuid === undefined) {
            throw new Error('setApproval: uuid was undefined/null');
        }
        if (typeof nonblockingApproval !== 'boolean') {
            throw new Error('setApproval: Invalid approval status');
        }
        const identityRecord = await this.getOrMigrateIdentityRecord(uuid);
        if (!identityRecord) {
            throw new Error(`setApproval: No identity record for ${uuid}`);
        }
        identityRecord.nonblockingApproval = nonblockingApproval;
        await this._saveIdentityKey(identityRecord);
    }
    async setVerified(uuid, verifiedStatus, publicKey) {
        if (uuid === null || uuid === undefined) {
            throw new Error('setVerified: uuid was undefined/null');
        }
        if (!validateVerifiedStatus(verifiedStatus)) {
            throw new Error('setVerified: Invalid verified status');
        }
        if (arguments.length > 2 && !(publicKey instanceof Uint8Array)) {
            throw new Error('setVerified: Invalid public key');
        }
        const identityRecord = await this.getOrMigrateIdentityRecord(uuid);
        if (!identityRecord) {
            throw new Error(`setVerified: No identity record for ${uuid.toString()}`);
        }
        if (!publicKey || (0, Crypto_1.constantTimeEqual)(identityRecord.publicKey, publicKey)) {
            identityRecord.verified = verifiedStatus;
            if (validateIdentityKey(identityRecord)) {
                await this._saveIdentityKey(identityRecord);
            }
        }
        else {
            log.info('setVerified: No identity record for specified publicKey');
        }
    }
    async getVerified(uuid) {
        if (uuid === null || uuid === undefined) {
            throw new Error('getVerified: uuid was undefined/null');
        }
        const identityRecord = await this.getOrMigrateIdentityRecord(uuid);
        if (!identityRecord) {
            throw new Error(`getVerified: No identity record for ${uuid}`);
        }
        const verifiedStatus = identityRecord.verified;
        if (validateVerifiedStatus(verifiedStatus)) {
            return verifiedStatus;
        }
        return VerifiedStatus.DEFAULT;
    }
    // Resolves to true if a new identity key was saved
    processContactSyncVerificationState(uuid, verifiedStatus, publicKey) {
        if (verifiedStatus === VerifiedStatus.UNVERIFIED) {
            return this.processUnverifiedMessage(uuid, verifiedStatus, publicKey);
        }
        return this.processVerifiedMessage(uuid, verifiedStatus, publicKey);
    }
    // This function encapsulates the non-Java behavior, since the mobile apps don't
    //   currently receive contact syncs and therefore will see a verify sync with
    //   UNVERIFIED status
    async processUnverifiedMessage(uuid, verifiedStatus, publicKey) {
        if (uuid === null || uuid === undefined) {
            throw new Error('processUnverifiedMessage: uuid was undefined/null');
        }
        if (publicKey !== undefined && !(publicKey instanceof Uint8Array)) {
            throw new Error('processUnverifiedMessage: Invalid public key');
        }
        const identityRecord = await this.getOrMigrateIdentityRecord(uuid);
        let isEqual = false;
        if (identityRecord && publicKey) {
            isEqual = (0, Crypto_1.constantTimeEqual)(publicKey, identityRecord.publicKey);
        }
        if (identityRecord &&
            isEqual &&
            identityRecord.verified !== VerifiedStatus.UNVERIFIED) {
            await this.setVerified(uuid, verifiedStatus, publicKey);
            return false;
        }
        if (!identityRecord || !isEqual) {
            await this.saveIdentityWithAttributes(uuid, {
                publicKey,
                verified: verifiedStatus,
                firstUse: false,
                timestamp: Date.now(),
                nonblockingApproval: true,
            });
            if (identityRecord && !isEqual) {
                try {
                    this.trigger('keychange', uuid);
                }
                catch (error) {
                    log.error('processUnverifiedMessage: error triggering keychange:', error && error.stack ? error.stack : error);
                }
                await this.archiveAllSessions(uuid);
                return true;
            }
        }
        // The situation which could get us here is:
        //   1. had a previous key
        //   2. new key is the same
        //   3. desired new status is same as what we had before
        return false;
    }
    // This matches the Java method as of
    //   https://github.com/signalapp/Signal-Android/blob/d0bb68e1378f689e4d10ac6a46014164992ca4e4/src/org/thoughtcrime/securesms/util/IdentityUtil.java#L188
    async processVerifiedMessage(uuid, verifiedStatus, publicKey) {
        if (uuid === null || uuid === undefined) {
            throw new Error('processVerifiedMessage: uuid was undefined/null');
        }
        if (!validateVerifiedStatus(verifiedStatus)) {
            throw new Error('processVerifiedMessage: Invalid verified status');
        }
        if (publicKey !== undefined && !(publicKey instanceof Uint8Array)) {
            throw new Error('processVerifiedMessage: Invalid public key');
        }
        const identityRecord = await this.getOrMigrateIdentityRecord(uuid);
        let isEqual = false;
        if (identityRecord && publicKey) {
            isEqual = (0, Crypto_1.constantTimeEqual)(publicKey, identityRecord.publicKey);
        }
        if (!identityRecord && verifiedStatus === VerifiedStatus.DEFAULT) {
            log.info('processVerifiedMessage: No existing record for default status');
            return false;
        }
        if (identityRecord &&
            isEqual &&
            identityRecord.verified !== VerifiedStatus.DEFAULT &&
            verifiedStatus === VerifiedStatus.DEFAULT) {
            await this.setVerified(uuid, verifiedStatus, publicKey);
            return false;
        }
        if (verifiedStatus === VerifiedStatus.VERIFIED &&
            (!identityRecord ||
                (identityRecord && !isEqual) ||
                (identityRecord && identityRecord.verified !== VerifiedStatus.VERIFIED))) {
            await this.saveIdentityWithAttributes(uuid, {
                publicKey,
                verified: verifiedStatus,
                firstUse: false,
                timestamp: Date.now(),
                nonblockingApproval: true,
            });
            if (identityRecord && !isEqual) {
                try {
                    this.trigger('keychange', uuid);
                }
                catch (error) {
                    log.error('processVerifiedMessage error triggering keychange:', error && error.stack ? error.stack : error);
                }
                await this.archiveAllSessions(uuid);
                // true signifies that we overwrote a previous key with a new one
                return true;
            }
        }
        // We get here if we got a new key and the status is DEFAULT. If the
        //   message is out of date, we don't want to lose whatever more-secure
        //   state we had before.
        return false;
    }
    isUntrusted(uuid) {
        if (uuid === null || uuid === undefined) {
            throw new Error('isUntrusted: uuid was undefined/null');
        }
        const identityRecord = this.getIdentityRecord(uuid);
        if (!identityRecord) {
            throw new Error(`isUntrusted: No identity record for ${uuid.toString()}`);
        }
        if ((0, timestamp_1.isMoreRecentThan)(identityRecord.timestamp, TIMESTAMP_THRESHOLD) &&
            !identityRecord.nonblockingApproval &&
            !identityRecord.firstUse) {
            return true;
        }
        return false;
    }
    async removeIdentityKey(uuid) {
        if (!this.identityKeys) {
            throw new Error('removeIdentityKey: this.identityKeys not yet cached!');
        }
        const id = uuid.toString();
        this.identityKeys.delete(id);
        await window.Signal.Data.removeIdentityKeyById(id);
        await this.removeAllSessions(id);
    }
    // Not yet processed messages - for resiliency
    getUnprocessedCount() {
        return this.withZone(exports.GLOBAL_ZONE, 'getUnprocessedCount', async () => {
            return window.Signal.Data.getUnprocessedCount();
        });
    }
    getAllUnprocessed() {
        return this.withZone(exports.GLOBAL_ZONE, 'getAllUnprocessed', async () => {
            return window.Signal.Data.getAllUnprocessed();
        });
    }
    getUnprocessedById(id) {
        return this.withZone(exports.GLOBAL_ZONE, 'getUnprocessedById', async () => {
            return window.Signal.Data.getUnprocessedById(id);
        });
    }
    addUnprocessed(data, { zone = exports.GLOBAL_ZONE } = {}) {
        return this.withZone(zone, 'addUnprocessed', async () => {
            this.pendingUnprocessed.set(data.id, data);
            // Current zone doesn't support pending unprocessed - commit immediately
            if (!zone.supportsPendingUnprocessed()) {
                await this.commitZoneChanges('addUnprocessed');
            }
        });
    }
    addMultipleUnprocessed(array, { zone = exports.GLOBAL_ZONE } = {}) {
        return this.withZone(zone, 'addMultipleUnprocessed', async () => {
            for (const elem of array) {
                this.pendingUnprocessed.set(elem.id, elem);
            }
            // Current zone doesn't support pending unprocessed - commit immediately
            if (!zone.supportsPendingUnprocessed()) {
                await this.commitZoneChanges('addMultipleUnprocessed');
            }
        });
    }
    updateUnprocessedWithData(id, data) {
        return this.withZone(exports.GLOBAL_ZONE, 'updateUnprocessedWithData', async () => {
            await window.Signal.Data.updateUnprocessedWithData(id, data);
        });
    }
    updateUnprocessedsWithData(items) {
        return this.withZone(exports.GLOBAL_ZONE, 'updateUnprocessedsWithData', async () => {
            await window.Signal.Data.updateUnprocessedsWithData(items);
        });
    }
    removeUnprocessed(idOrArray) {
        return this.withZone(exports.GLOBAL_ZONE, 'removeUnprocessed', async () => {
            await window.Signal.Data.removeUnprocessed(idOrArray);
        });
    }
    removeAllUnprocessed() {
        return this.withZone(exports.GLOBAL_ZONE, 'removeAllUnprocessed', async () => {
            await window.Signal.Data.removeAllUnprocessed();
        });
    }
    async removeAllData() {
        await window.Signal.Data.removeAll();
        await this.hydrateCaches();
        window.storage.reset();
        await window.storage.fetch();
        window.ConversationController.reset();
        await window.ConversationController.load();
    }
    async removeAllConfiguration(mode) {
        await window.Signal.Data.removeAllConfiguration(mode);
        await this.hydrateCaches();
        window.storage.reset();
        await window.storage.fetch();
    }
    _getAllSessions() {
        var _a;
        const union = new Map();
        (_a = this.sessions) === null || _a === void 0 ? void 0 : _a.forEach((value, key) => {
            union.set(key, value);
        });
        this.pendingSessions.forEach((value, key) => {
            union.set(key, value);
        });
        return Array.from(union.values());
    }
}
exports.SignalProtocolStore = SignalProtocolStore;
window.SignalProtocolStore = SignalProtocolStore;
