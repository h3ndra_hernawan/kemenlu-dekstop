"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteForEveryone = void 0;
const log = __importStar(require("../logging/log"));
const ONE_DAY = 24 * 60 * 60 * 1000;
async function deleteForEveryone(message, doe, shouldPersist = true) {
    const messageTimestamp = message.get('serverTimestamp') || message.get('sent_at') || 0;
    // Make sure the server timestamps for the DOE and the matching message
    // are less than one day apart
    const delta = Math.abs(doe.get('serverTimestamp') - messageTimestamp);
    if (delta > ONE_DAY) {
        log.info('Received late DOE. Dropping.', {
            fromId: doe.get('fromId'),
            targetSentTimestamp: doe.get('targetSentTimestamp'),
            messageServerTimestamp: message.get('serverTimestamp'),
            messageSentAt: message.get('sent_at'),
            deleteServerTimestamp: doe.get('serverTimestamp'),
        });
        return;
    }
    await message.handleDeleteForEveryone(doe, shouldPersist);
}
exports.deleteForEveryone = deleteForEveryone;
