"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.format = exports.DEFAULT_DURATIONS_SET = exports.DEFAULT_DURATIONS_IN_SECONDS = void 0;
const moment = __importStar(require("moment"));
const humanize_duration_1 = __importDefault(require("humanize-duration"));
const SECONDS_PER_WEEK = 604800;
exports.DEFAULT_DURATIONS_IN_SECONDS = [
    0,
    moment.duration(4, 'weeks').asSeconds(),
    moment.duration(1, 'week').asSeconds(),
    moment.duration(1, 'day').asSeconds(),
    moment.duration(8, 'hours').asSeconds(),
    moment.duration(1, 'hour').asSeconds(),
    moment.duration(5, 'minutes').asSeconds(),
    moment.duration(30, 'seconds').asSeconds(),
];
exports.DEFAULT_DURATIONS_SET = new Set(exports.DEFAULT_DURATIONS_IN_SECONDS);
function format(i18n, dirtySeconds, { capitalizeOff = false } = {}) {
    let seconds = Math.abs(dirtySeconds || 0);
    if (!seconds) {
        return i18n(capitalizeOff ? 'off' : 'disappearingMessages__off');
    }
    seconds = Math.max(Math.floor(seconds), 1);
    const locale = i18n.getLocale();
    const localeWithoutRegion = locale.split('_', 1)[0];
    const fallbacks = [];
    if (localeWithoutRegion !== locale) {
        fallbacks.push(localeWithoutRegion);
    }
    if (localeWithoutRegion === 'nb' || localeWithoutRegion === 'nn') {
        fallbacks.push('no');
    }
    if (localeWithoutRegion !== 'en') {
        fallbacks.push('en');
    }
    return (0, humanize_duration_1.default)(seconds * 1000, Object.assign({ units: seconds % SECONDS_PER_WEEK === 0 ? ['w'] : ['d', 'h', 'm', 's'], language: locale }, (fallbacks.length ? { fallbacks } : {})));
}
exports.format = format;
