"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isAttachmentSizeOkay = void 0;
const Attachment_1 = require("../types/Attachment");
const showToast_1 = require("./showToast");
const ToastFileSize_1 = require("../components/ToastFileSize");
function isAttachmentSizeOkay(attachment) {
    const limitKb = (0, Attachment_1.getUploadSizeLimitKb)(attachment.contentType);
    // this needs to be cast properly
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if ((attachment.data.byteLength / 1024).toFixed(4) >= limitKb) {
        const units = ['kB', 'MB', 'GB'];
        let u = -1;
        let limit = limitKb * 1000;
        do {
            limit /= 1000;
            u += 1;
        } while (limit >= 1000 && u < units.length - 1);
        (0, showToast_1.showToast)(ToastFileSize_1.ToastFileSize, {
            limit,
            units: units[u],
        });
        return false;
    }
    return true;
}
exports.isAttachmentSizeOkay = isAttachmentSizeOkay;
