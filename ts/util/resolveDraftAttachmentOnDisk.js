"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolveDraftAttachmentOnDisk = void 0;
const lodash_1 = require("lodash");
const log = __importStar(require("../logging/log"));
const Attachment_1 = require("../types/Attachment");
function resolveDraftAttachmentOnDisk(attachment) {
    let url = '';
    if (attachment.pending) {
        return attachment;
    }
    if (attachment.screenshotPath) {
        url = window.Signal.Migrations.getAbsoluteDraftPath(attachment.screenshotPath);
    }
    else if (!(0, Attachment_1.isVideoAttachment)(attachment) && attachment.path) {
        url = window.Signal.Migrations.getAbsoluteDraftPath(attachment.path);
    }
    else {
        log.warn('resolveOnDiskAttachment: Attachment was missing both screenshotPath and path fields');
    }
    return Object.assign(Object.assign({}, (0, lodash_1.pick)(attachment, [
        'blurHash',
        'caption',
        'contentType',
        'fileName',
        'path',
        'size',
    ])), { pending: false, url });
}
exports.resolveDraftAttachmentOnDisk = resolveDraftAttachmentOnDisk;
