"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteDraftAttachment = void 0;
async function deleteDraftAttachment(attachment) {
    if (attachment.screenshotPath) {
        await window.Signal.Migrations.deleteDraftFile(attachment.screenshotPath);
    }
    if (attachment.path) {
        await window.Signal.Migrations.deleteDraftFile(attachment.path);
    }
}
exports.deleteDraftAttachment = deleteDraftAttachment;
