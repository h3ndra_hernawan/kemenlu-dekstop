"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.parsePhoneNumberDiscoverability = exports.PhoneNumberDiscoverability = void 0;
const enum_1 = require("./enum");
// These strings are saved to disk, so be careful when changing them.
var PhoneNumberDiscoverability;
(function (PhoneNumberDiscoverability) {
    PhoneNumberDiscoverability["Discoverable"] = "Discoverable";
    PhoneNumberDiscoverability["NotDiscoverable"] = "NotDiscoverable";
})(PhoneNumberDiscoverability = exports.PhoneNumberDiscoverability || (exports.PhoneNumberDiscoverability = {}));
exports.parsePhoneNumberDiscoverability = (0, enum_1.makeEnumParser)(PhoneNumberDiscoverability, PhoneNumberDiscoverability.Discoverable);
