"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.everDone = exports.isDone = exports.remove = exports.markDone = exports.markEverDone = void 0;
function markEverDone() {
    window.storage.put('chromiumRegistrationDoneEver', '');
}
exports.markEverDone = markEverDone;
function markDone() {
    markEverDone();
    window.storage.put('chromiumRegistrationDone', '');
}
exports.markDone = markDone;
async function remove() {
    await window.storage.remove('chromiumRegistrationDone');
}
exports.remove = remove;
function isDone() {
    return window.storage.get('chromiumRegistrationDone') === '';
}
exports.isDone = isDone;
function everDone() {
    return window.storage.get('chromiumRegistrationDoneEver') === '' || isDone();
}
exports.everDone = everDone;
