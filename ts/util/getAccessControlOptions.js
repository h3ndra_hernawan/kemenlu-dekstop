"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAccessControlOptions = void 0;
const protobuf_1 = require("../protobuf");
const AccessControlEnum = protobuf_1.SignalService.AccessControl.AccessRequired;
function getAccessControlOptions(i18n) {
    return [
        {
            text: i18n('GroupV2--all-members'),
            value: AccessControlEnum.MEMBER,
        },
        {
            text: i18n('GroupV2--only-admins'),
            value: AccessControlEnum.ADMINISTRATOR,
        },
    ];
}
exports.getAccessControlOptions = getAccessControlOptions;
