"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sessionRecordToProtobuf = exports.sessionStructureToBytes = void 0;
const lodash_1 = require("lodash");
const compiled_1 = require("../protobuf/compiled");
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const { RecordStructure, SessionStructure } = compiled_1.signal.proto.storage;
const { Chain } = SessionStructure;
function sessionStructureToBytes(recordStructure) {
    return compiled_1.signal.proto.storage.RecordStructure.encode(recordStructure).finish();
}
exports.sessionStructureToBytes = sessionStructureToBytes;
function sessionRecordToProtobuf(record, ourData) {
    const proto = new RecordStructure();
    proto.previousSessions = [];
    const sessionGroup = record.sessions || {};
    const sessions = Object.values(sessionGroup);
    const first = sessions.find(session => {
        var _a;
        return ((_a = session === null || session === void 0 ? void 0 : session.indexInfo) === null || _a === void 0 ? void 0 : _a.closed) === -1;
    });
    if (first) {
        proto.currentSession = toProtobufSession(first, ourData);
    }
    sessions.sort((left, right) => {
        var _a, _b;
        // Descending - we want recently-closed sessions to be first
        return (((_a = right === null || right === void 0 ? void 0 : right.indexInfo) === null || _a === void 0 ? void 0 : _a.closed) || 0) - (((_b = left === null || left === void 0 ? void 0 : left.indexInfo) === null || _b === void 0 ? void 0 : _b.closed) || 0);
    });
    const onlyClosed = sessions.filter(session => { var _a; return ((_a = session === null || session === void 0 ? void 0 : session.indexInfo) === null || _a === void 0 ? void 0 : _a.closed) !== -1; });
    if (onlyClosed.length < sessions.length - 1) {
        throw new Error('toProtobuf: More than one open session!');
    }
    proto.previousSessions = [];
    onlyClosed.forEach(session => {
        proto.previousSessions.push(toProtobufSession(session, ourData));
    });
    if (!proto.currentSession && proto.previousSessions.length === 0) {
        throw new Error('toProtobuf: Record had no sessions!');
    }
    return proto;
}
exports.sessionRecordToProtobuf = sessionRecordToProtobuf;
function toProtobufSession(session, ourData) {
    var _a, _b, _c;
    const proto = new SessionStructure();
    // Core Fields
    proto.aliceBaseKey = binaryToUint8Array(session, 'indexInfo.baseKey', 33);
    proto.localIdentityPublic = ourData.identityKeyPublic;
    proto.localRegistrationId = ourData.registrationId;
    proto.previousCounter =
        getInteger(session, 'currentRatchet.previousCounter') + 1;
    proto.remoteIdentityPublic = binaryToUint8Array(session, 'indexInfo.remoteIdentityKey', 33);
    proto.remoteRegistrationId = getInteger(session, 'registrationId');
    proto.rootKey = binaryToUint8Array(session, 'currentRatchet.rootKey', 32);
    proto.sessionVersion = 3;
    // Note: currently unused
    // proto.needsRefresh = null;
    // Pending PreKey
    if (session.pendingPreKey) {
        proto.pendingPreKey =
            new compiled_1.signal.proto.storage.SessionStructure.PendingPreKey();
        proto.pendingPreKey.baseKey = binaryToUint8Array(session, 'pendingPreKey.baseKey', 33);
        proto.pendingPreKey.signedPreKeyId = getInteger(session, 'pendingPreKey.signedKeyId');
        if (session.pendingPreKey.preKeyId !== undefined) {
            proto.pendingPreKey.preKeyId = getInteger(session, 'pendingPreKey.preKeyId');
        }
    }
    // Sender Chain
    const senderBaseKey = (_b = (_a = session.currentRatchet) === null || _a === void 0 ? void 0 : _a.ephemeralKeyPair) === null || _b === void 0 ? void 0 : _b.pubKey;
    if (!senderBaseKey) {
        throw new Error('toProtobufSession: No sender base key!');
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const senderChain = session[senderBaseKey];
    if (!senderChain) {
        throw new Error('toProtobufSession: No matching chain found with senderBaseKey!');
    }
    if (senderChain.chainType !== 1) {
        throw new Error(`toProtobufSession: Expected sender chain type for senderChain, got ${senderChain.chainType}`);
    }
    const protoSenderChain = toProtobufChain(senderChain);
    protoSenderChain.senderRatchetKey = binaryToUint8Array(session, 'currentRatchet.ephemeralKeyPair.pubKey', 33);
    protoSenderChain.senderRatchetKeyPrivate = binaryToUint8Array(session, 'currentRatchet.ephemeralKeyPair.privKey', 32);
    proto.senderChain = protoSenderChain;
    // First Receiver Chain
    proto.receiverChains = [];
    const firstReceiverChainBaseKey = (_c = session.currentRatchet) === null || _c === void 0 ? void 0 : _c.lastRemoteEphemeralKey;
    if (!firstReceiverChainBaseKey) {
        throw new Error('toProtobufSession: No receiver base key!');
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const firstReceiverChain = session[firstReceiverChainBaseKey];
    // If the session was just initialized, then there will be no receiver chain
    if (firstReceiverChain) {
        const protoFirstReceiverChain = toProtobufChain(firstReceiverChain);
        if (firstReceiverChain.chainType !== 2) {
            throw new Error(`toProtobufSession: Expected receiver chain type for firstReceiverChain, got ${firstReceiverChain.chainType}`);
        }
        protoFirstReceiverChain.senderRatchetKey = binaryToUint8Array(session, 'currentRatchet.lastRemoteEphemeralKey', 33);
        proto.receiverChains.push(protoFirstReceiverChain);
    }
    // Old Receiver Chains
    const oldChains = (session.oldRatchetList || [])
        .slice(0)
        .sort((left, right) => (right.added || 0) - (left.added || 0));
    oldChains.forEach(oldRatchet => {
        const baseKey = oldRatchet.ephemeralKey;
        if (!baseKey) {
            throw new Error('toProtobufSession: No base key for old receiver chain!');
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const chain = session[baseKey];
        if (!chain) {
            throw new Error('toProtobufSession: No chain for old receiver chain base key!');
        }
        if (chain.chainType !== 2) {
            throw new Error(`toProtobufSession: Expected receiver chain type, got ${chain.chainType}`);
        }
        const protoChain = toProtobufChain(chain);
        protoChain.senderRatchetKey = binaryToUint8Array(oldRatchet, 'ephemeralKey', 33);
        proto.receiverChains.push(protoChain);
    });
    return proto;
}
function toProtobufChain(chain) {
    var _a;
    const proto = new Chain();
    const protoChainKey = new Chain.ChainKey();
    protoChainKey.index = getInteger(chain, 'chainKey.counter') + 1;
    if (((_a = chain.chainKey) === null || _a === void 0 ? void 0 : _a.key) !== undefined) {
        protoChainKey.key = binaryToUint8Array(chain, 'chainKey.key', 32);
    }
    proto.chainKey = protoChainKey;
    const messageKeys = Object.entries(chain.messageKeys || {});
    proto.messageKeys = messageKeys.map(entry => {
        const protoMessageKey = new SessionStructure.Chain.MessageKey();
        protoMessageKey.index = getInteger(entry, '0') + 1;
        const key = binaryToUint8Array(entry, '1', 32);
        const { cipherKey, macKey, iv } = translateMessageKey(key);
        protoMessageKey.cipherKey = cipherKey;
        protoMessageKey.macKey = macKey;
        protoMessageKey.iv = iv;
        return protoMessageKey;
    });
    return proto;
}
// Utility functions
const WHISPER_MESSAGE_KEYS = 'WhisperMessageKeys';
function translateMessageKey(key) {
    const input = key;
    const salt = new Uint8Array(32);
    const info = Bytes.fromString(WHISPER_MESSAGE_KEYS);
    const [cipherKey, macKey, ivContainer] = (0, Crypto_1.deriveSecrets)(input, salt, info);
    return {
        cipherKey,
        macKey,
        iv: ivContainer.slice(0, 16),
    };
}
function binaryToUint8Array(
// eslint-disable-next-line @typescript-eslint/no-explicit-any
object, path, length) {
    const target = (0, lodash_1.get)(object, path);
    if (target === null || target === undefined) {
        throw new Error(`binaryToUint8Array: Falsey path ${path}`);
    }
    if (!(0, lodash_1.isString)(target)) {
        throw new Error(`binaryToUint8Array: String not found at path ${path}`);
    }
    const buffer = Bytes.fromBinary(target);
    if (length && buffer.byteLength !== length) {
        throw new Error(`binaryToUint8Array: Got unexpected length ${buffer.byteLength} instead of ${length} at path ${path}`);
    }
    return buffer;
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function getInteger(object, path) {
    const target = (0, lodash_1.get)(object, path);
    if (target === null || target === undefined) {
        throw new Error(`getInteger: Falsey path ${path}`);
    }
    if ((0, lodash_1.isString)(target)) {
        const result = parseInt(target, 10);
        if (!(0, lodash_1.isFinite)(result)) {
            throw new Error(`getInteger: Value could not be parsed as number at ${path}: {target}`);
        }
        if (!(0, lodash_1.isInteger)(result)) {
            throw new Error(`getInteger: Parsed value not an integer at ${path}: {target}`);
        }
        return result;
    }
    if (!(0, lodash_1.isInteger)(target)) {
        throw new Error(`getInteger: Value not an integer at ${path}: {target}`);
    }
    return target;
}
