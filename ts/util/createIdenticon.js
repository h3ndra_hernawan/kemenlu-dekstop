"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createIdenticon = void 0;
const react_1 = __importDefault(require("react"));
const blueimp_load_image_1 = __importDefault(require("blueimp-load-image"));
const server_1 = require("react-dom/server");
const Colors_1 = require("../types/Colors");
const IdenticonSVG_1 = require("../components/IdenticonSVG");
function createIdenticon(color, content) {
    const [defaultColorValue] = Array.from(Colors_1.AvatarColorMap.values());
    const avatarColor = Colors_1.AvatarColorMap.get(color);
    const html = (0, server_1.renderToString)(react_1.default.createElement(IdenticonSVG_1.IdenticonSVG, { backgroundColor: (avatarColor === null || avatarColor === void 0 ? void 0 : avatarColor.bg) || defaultColorValue.bg, content: content, foregroundColor: (avatarColor === null || avatarColor === void 0 ? void 0 : avatarColor.fg) || defaultColorValue.fg }));
    const svg = new Blob([html], { type: 'image/svg+xml;charset=utf-8' });
    const svgUrl = URL.createObjectURL(svg);
    return new Promise(resolve => {
        const img = document.createElement('img');
        img.onload = () => {
            const canvas = blueimp_load_image_1.default.scale(img, {
                canvas: true,
                maxWidth: 100,
                maxHeight: 100,
            });
            if (!(canvas instanceof HTMLCanvasElement)) {
                resolve('');
                return;
            }
            const ctx = canvas.getContext('2d');
            if (ctx) {
                ctx.drawImage(img, 0, 0);
            }
            URL.revokeObjectURL(svgUrl);
            resolve(canvas.toDataURL('image/png'));
        };
        img.onerror = () => {
            URL.revokeObjectURL(svgUrl);
            resolve('');
        };
        img.src = svgUrl;
    });
}
exports.createIdenticon = createIdenticon;
