"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.installCallback = exports.installSetting = exports.createCallback = exports.createSetting = void 0;
const electron_1 = require("electron");
const assert_1 = require("./assert");
function capitalize(name) {
    const result = name.slice(0, 1).toUpperCase() + name.slice(1);
    return result;
}
function getSetterName(name) {
    return `set${capitalize(name)}`;
}
function getGetterName(name) {
    return `get${capitalize(name)}`;
}
function createSetting(name, overrideOptions = {}) {
    const options = Object.assign({ getter: true, setter: true }, overrideOptions);
    function getValue() {
        (0, assert_1.strictAssert)(options.getter, `${name} has no getter`);
        return electron_1.ipcRenderer.invoke(`settings:get:${name}`);
    }
    function setValue(value) {
        (0, assert_1.strictAssert)(options.setter, `${name} has no setter`);
        return electron_1.ipcRenderer.invoke(`settings:set:${name}`, value);
    }
    return {
        getValue,
        setValue,
    };
}
exports.createSetting = createSetting;
function createCallback(name) {
    return (...args) => {
        return electron_1.ipcRenderer.invoke(`settings:call:${name}`, args);
    };
}
exports.createCallback = createCallback;
function installSetting(name, { getter = true, setter = true } = {}) {
    const getterName = getGetterName(name);
    const setterName = getSetterName(name);
    if (getter) {
        electron_1.ipcRenderer.on(`settings:get:${name}`, async (_event, { seq }) => {
            const getFn = window.Events[getterName];
            if (!getFn) {
                electron_1.ipcRenderer.send(`settings:get:${name}`, `installGetter: ${getterName} not found for event ${name}`);
                return;
            }
            try {
                electron_1.ipcRenderer.send('settings:response', seq, null, await getFn());
            }
            catch (error) {
                electron_1.ipcRenderer.send('settings:response', seq, error && error.stack ? error.stack : error);
            }
        });
    }
    if (setter) {
        electron_1.ipcRenderer.on(`settings:set:${name}`, async (_event, { seq, value }) => {
            // Some settings do not have setters...
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const setFn = window.Events[setterName];
            if (!setFn) {
                electron_1.ipcRenderer.send('settings:response', seq, `installSetter: ${setterName} not found for event ${name}`);
                return;
            }
            try {
                await setFn(value);
                electron_1.ipcRenderer.send('settings:response', seq, null);
            }
            catch (error) {
                electron_1.ipcRenderer.send('settings:response', seq, error && error.stack ? error.stack : error);
            }
        });
    }
}
exports.installSetting = installSetting;
function installCallback(name) {
    electron_1.ipcRenderer.on(`settings:call:${name}`, async (_, { seq, args }) => {
        const hook = window.Events[name];
        try {
            electron_1.ipcRenderer.send('settings:response', seq, null, await hook(...args));
        }
        catch (error) {
            electron_1.ipcRenderer.send('settings:response', seq, error && error.stack ? error.stack : error);
        }
    });
}
exports.installCallback = installCallback;
