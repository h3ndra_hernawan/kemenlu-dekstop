"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.downloadAttachment = void 0;
const downloadAttachment_1 = require("../textsecure/downloadAttachment");
async function downloadAttachment(attachmentData) {
    let migratedAttachment;
    const { id: legacyId } = attachmentData;
    if (legacyId === undefined) {
        migratedAttachment = attachmentData;
    }
    else {
        migratedAttachment = Object.assign(Object.assign({}, attachmentData), { cdnId: String(legacyId) });
    }
    let downloaded;
    try {
        downloaded = await (0, downloadAttachment_1.downloadAttachment)(window.textsecure.server, migratedAttachment);
    }
    catch (error) {
        // Attachments on the server expire after 30 days, then start returning 404
        if (error && error.code === 404) {
            return null;
        }
        throw error;
    }
    return downloaded;
}
exports.downloadAttachment = downloadAttachment;
