"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.invertIdsByTitle = exports.hasUnacknowledgedCollisions = exports.getCollisionsFromMemberships = exports.dehydrateCollisionsWithConversations = void 0;
const lodash_1 = require("lodash");
const iterables_1 = require("./iterables");
const getOwn_1 = require("./getOwn");
const isConversationNameKnown_1 = require("./isConversationNameKnown");
const isInSystemContacts_1 = require("./isInSystemContacts");
const dehydrateCollisionsWithConversations = (withConversations) => (0, lodash_1.mapValues)(withConversations, conversations => conversations.map(c => c.id));
exports.dehydrateCollisionsWithConversations = dehydrateCollisionsWithConversations;
function getCollisionsFromMemberships(memberships) {
    const members = (0, iterables_1.map)(memberships, membership => membership.member);
    const candidateMembers = (0, iterables_1.filter)(members, member => !member.isMe && (0, isConversationNameKnown_1.isConversationNameKnown)(member));
    const groupedByTitle = (0, iterables_1.groupBy)(candidateMembers, member => member.title);
    // This cast is here because `pickBy` returns a `Partial`, which is incompatible with
    //   `Record`. [This demonstates the problem][0], but I don't believe it's an actual
    //   issue in the code.
    //
    // Alternatively, we could filter undefined keys or something like that.
    //
    // [0]: https://www.typescriptlang.org/play?#code/C4TwDgpgBAYg9nKBeKAFAhgJ2AS3QGwB4AlCAYzkwBNCBnYTHAOwHMAaKJgVwFsAjCJgB8QgNwAoCk3pQAZgC5YCZFADeUABY5FAVigBfCeNCQoAISwrSFanQbN2nXgOESpMvoouYVs0UA
    return (0, lodash_1.pickBy)(groupedByTitle, group => group.length >= 2 && !group.every(person => (0, isInSystemContacts_1.isInSystemContacts)(person)));
}
exports.getCollisionsFromMemberships = getCollisionsFromMemberships;
/**
 * Returns `true` if the user should see a group member name collision warning, and
 * `false` otherwise. Users should see these warnings if any collisions appear that they
 * haven't dismissed.
 */
const hasUnacknowledgedCollisions = (previous, current) => Object.entries(current).some(([title, currentIds]) => {
    const previousIds = new Set((0, getOwn_1.getOwn)(previous, title) || []);
    return currentIds.some(currentId => !previousIds.has(currentId));
});
exports.hasUnacknowledgedCollisions = hasUnacknowledgedCollisions;
const invertIdsByTitle = (idsByTitle) => {
    const result = Object.create(null);
    Object.entries(idsByTitle).forEach(([title, ids]) => {
        ids.forEach(id => {
            result[id] = title;
        });
    });
    return result;
};
exports.invertIdsByTitle = invertIdsByTitle;
