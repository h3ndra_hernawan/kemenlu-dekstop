"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getProfile = void 0;
const SealedSender_1 = require("../types/SealedSender");
const Address_1 = require("../types/Address");
const QualifiedAddress_1 = require("../types/QualifiedAddress");
const UUID_1 = require("../types/UUID");
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const zkgroup_1 = require("./zkgroup");
const getSendOptions_1 = require("./getSendOptions");
const whatTypeOfConversation_1 = require("./whatTypeOfConversation");
const log = __importStar(require("../logging/log"));
const userLanguages_1 = require("./userLanguages");
const parseBadgesFromServer_1 = require("../badges/parseBadgesFromServer");
async function getProfile(providedUuid, providedE164) {
    if (!window.textsecure.messaging) {
        throw new Error('Conversation.getProfile: window.textsecure.messaging not available');
    }
    const { updatesUrl } = window.SignalContext.config;
    if (typeof updatesUrl !== 'string') {
        throw new Error('getProfile expected updatesUrl to be a defined string');
    }
    const id = window.ConversationController.ensureContactIds({
        uuid: providedUuid,
        e164: providedE164,
    });
    const c = window.ConversationController.get(id);
    if (!c) {
        log.error('getProfile: failed to find conversation; doing nothing');
        return;
    }
    const clientZkProfileCipher = (0, zkgroup_1.getClientZkProfileOperations)(window.getServerPublicParams());
    const userLanguages = (0, userLanguages_1.getUserLanguages)(navigator.languages, window.getLocale());
    let profile;
    try {
        await Promise.all([
            c.deriveAccessKeyIfNeeded(),
            c.deriveProfileKeyVersionIfNeeded(),
        ]);
        const profileKey = c.get('profileKey');
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const uuid = c.get('uuid');
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const identifier = c.getSendTarget();
        const targetUuid = UUID_1.UUID.checkedLookup(identifier);
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const profileKeyVersionHex = c.get('profileKeyVersion');
        const existingProfileKeyCredential = c.get('profileKeyCredential');
        let profileKeyCredentialRequestHex;
        let profileCredentialRequestContext;
        if (profileKey &&
            uuid &&
            profileKeyVersionHex &&
            !existingProfileKeyCredential) {
            log.info('Generating request...');
            ({
                requestHex: profileKeyCredentialRequestHex,
                context: profileCredentialRequestContext,
            } = (0, zkgroup_1.generateProfileKeyCredentialRequest)(clientZkProfileCipher, uuid, profileKey));
        }
        const { sendMetadata = {} } = await (0, getSendOptions_1.getSendOptions)(c.attributes);
        const getInfo = 
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        sendMetadata[c.get('uuid')] || sendMetadata[c.get('e164')] || {};
        if (getInfo.accessKey) {
            try {
                profile = await window.textsecure.messaging.getProfile(identifier, {
                    accessKey: getInfo.accessKey,
                    profileKeyVersion: profileKeyVersionHex,
                    profileKeyCredentialRequest: profileKeyCredentialRequestHex,
                    userLanguages,
                });
            }
            catch (error) {
                if (error.code === 401 || error.code === 403) {
                    log.info(`Setting sealedSender to DISABLED for conversation ${c.idForLogging()}`);
                    c.set({ sealedSender: SealedSender_1.SEALED_SENDER.DISABLED });
                    profile = await window.textsecure.messaging.getProfile(identifier, {
                        profileKeyVersion: profileKeyVersionHex,
                        profileKeyCredentialRequest: profileKeyCredentialRequestHex,
                        userLanguages,
                    });
                }
                else {
                    throw error;
                }
            }
        }
        else {
            profile = await window.textsecure.messaging.getProfile(identifier, {
                profileKeyVersion: profileKeyVersionHex,
                profileKeyCredentialRequest: profileKeyCredentialRequestHex,
                userLanguages,
            });
        }
        if (profile.identityKey) {
            const identityKey = Bytes.fromBase64(profile.identityKey);
            const changed = await window.textsecure.storage.protocol.saveIdentity(new Address_1.Address(targetUuid, 1), identityKey, false);
            if (changed) {
                // save identity will close all sessions except for .1, so we
                // must close that one manually.
                const ourUuid = window.textsecure.storage.user.getCheckedUuid();
                await window.textsecure.storage.protocol.archiveSession(new QualifiedAddress_1.QualifiedAddress(ourUuid, new Address_1.Address(targetUuid, 1)));
            }
        }
        const accessKey = c.get('accessKey');
        if (profile.unrestrictedUnidentifiedAccess && profile.unidentifiedAccess) {
            log.info(`Setting sealedSender to UNRESTRICTED for conversation ${c.idForLogging()}`);
            c.set({
                sealedSender: SealedSender_1.SEALED_SENDER.UNRESTRICTED,
            });
        }
        else if (accessKey && profile.unidentifiedAccess) {
            const haveCorrectKey = (0, Crypto_1.verifyAccessKey)(Bytes.fromBase64(accessKey), Bytes.fromBase64(profile.unidentifiedAccess));
            if (haveCorrectKey) {
                log.info(`Setting sealedSender to ENABLED for conversation ${c.idForLogging()}`);
                c.set({
                    sealedSender: SealedSender_1.SEALED_SENDER.ENABLED,
                });
            }
            else {
                log.info(`Setting sealedSender to DISABLED for conversation ${c.idForLogging()}`);
                c.set({
                    sealedSender: SealedSender_1.SEALED_SENDER.DISABLED,
                });
            }
        }
        else {
            log.info(`Setting sealedSender to DISABLED for conversation ${c.idForLogging()}`);
            c.set({
                sealedSender: SealedSender_1.SEALED_SENDER.DISABLED,
            });
        }
        const { username } = profile;
        if (username) {
            c.set({ username });
        }
        else {
            c.unset('username');
        }
        if (profile.about) {
            const key = c.get('profileKey');
            if (key) {
                const keyBuffer = Bytes.fromBase64(key);
                const decrypted = (0, Crypto_1.decryptProfile)(Bytes.fromBase64(profile.about), keyBuffer);
                c.set('about', Bytes.toString((0, Crypto_1.trimForDisplay)(decrypted)));
            }
        }
        else {
            c.unset('about');
        }
        if (profile.aboutEmoji) {
            const key = c.get('profileKey');
            if (key) {
                const keyBuffer = Bytes.fromBase64(key);
                const decrypted = (0, Crypto_1.decryptProfile)(Bytes.fromBase64(profile.aboutEmoji), keyBuffer);
                c.set('aboutEmoji', Bytes.toString((0, Crypto_1.trimForDisplay)(decrypted)));
            }
        }
        else {
            c.unset('aboutEmoji');
        }
        if (profile.paymentAddress && (0, whatTypeOfConversation_1.isMe)(c.attributes)) {
            window.storage.put('paymentAddress', profile.paymentAddress);
        }
        if (profile.capabilities) {
            c.set({ capabilities: profile.capabilities });
        }
        else {
            c.unset('capabilities');
        }
        const badges = (0, parseBadgesFromServer_1.parseBadgesFromServer)(profile.badges, updatesUrl);
        if (badges.length) {
            await window.reduxActions.badges.updateOrCreate(badges);
            c.set({
                badges: badges.map(badge => (Object.assign({ id: badge.id }, ('expiresAt' in badge
                    ? {
                        expiresAt: badge.expiresAt,
                        isVisible: badge.isVisible,
                    }
                    : {})))),
            });
        }
        else {
            c.unset('badges');
        }
        if (profileCredentialRequestContext) {
            if (profile.credential) {
                const profileKeyCredential = (0, zkgroup_1.handleProfileKeyCredential)(clientZkProfileCipher, profileCredentialRequestContext, profile.credential);
                c.set({ profileKeyCredential });
            }
            else {
                c.unset('profileKeyCredential');
            }
        }
    }
    catch (error) {
        switch (error === null || error === void 0 ? void 0 : error.code) {
            case 403:
                throw error;
            case 404:
                log.warn(`getProfile failure: failed to find a profile for ${c.idForLogging()}`, error && error.stack ? error.stack : error);
                c.setUnregistered();
                return;
            default:
                log.warn('getProfile failure:', c.idForLogging(), error && error.stack ? error.stack : error);
                return;
        }
    }
    if (profile.name) {
        try {
            await c.setEncryptedProfileName(profile.name);
        }
        catch (error) {
            log.warn('getProfile decryption failure:', c.idForLogging(), error && error.stack ? error.stack : error);
            await c.set({
                profileName: undefined,
                profileFamilyName: undefined,
            });
        }
    }
    else {
        c.set({
            profileName: undefined,
            profileFamilyName: undefined,
        });
    }
    try {
        await c.setProfileAvatar(profile.avatar);
    }
    catch (error) {
        if (error.code === 403 || error.code === 404) {
            log.info(`Clearing profile avatar for conversation ${c.idForLogging()}`);
            c.set({
                profileAvatar: null,
            });
        }
    }
    c.set('profileLastFetchedAt', Date.now());
    window.Signal.Data.updateConversation(c.attributes);
}
exports.getProfile = getProfile;
