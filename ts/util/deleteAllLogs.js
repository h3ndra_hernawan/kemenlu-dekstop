"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAllLogs = void 0;
const electron_1 = require("electron");
const set_up_renderer_logging_1 = require("../logging/set_up_renderer_logging");
function deleteAllLogs() {
    return new Promise((resolve, reject) => {
        // Restart logging again when the file stream close
        (0, set_up_renderer_logging_1.beforeRestart)();
        const timeout = setTimeout(() => {
            reject(new Error('Request to delete all logs timed out'));
        }, 5000);
        electron_1.ipcRenderer.once('delete-all-logs-complete', () => {
            clearTimeout(timeout);
            resolve();
        });
        electron_1.ipcRenderer.send('delete-all-logs');
    });
}
exports.deleteAllLogs = deleteAllLogs;
