"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMessageTimestamp = void 0;
function getMessageTimestamp(message) {
    return message.received_at_ms || message.received_at;
}
exports.getMessageTimestamp = getMessageTimestamp;
