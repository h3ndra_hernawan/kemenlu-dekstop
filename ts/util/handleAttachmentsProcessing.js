"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleAttachmentsProcessing = void 0;
const processAttachment_1 = require("./processAttachment");
const AttachmentToastType_1 = require("../types/AttachmentToastType");
const log = __importStar(require("../logging/log"));
async function handleAttachmentsProcessing({ addAttachment, addPendingAttachment, conversationId, draftAttachments, files, onShowToast, removeAttachment, }) {
    if (!files.length) {
        return;
    }
    const nextDraftAttachments = [...draftAttachments];
    const filesToProcess = [];
    for (let i = 0; i < files.length; i += 1) {
        const file = files[i];
        const processingResult = (0, processAttachment_1.preProcessAttachment)(file, nextDraftAttachments);
        if (processingResult) {
            onShowToast(processingResult);
        }
        else {
            const pendingAttachment = (0, processAttachment_1.getPendingAttachment)(file);
            if (pendingAttachment) {
                addPendingAttachment(conversationId, pendingAttachment);
                filesToProcess.push(file);
                // we keep a running count of the draft attachments so we can show a
                // toast in case we add too many attachments at once
                nextDraftAttachments.push(pendingAttachment);
            }
        }
    }
    await Promise.all(filesToProcess.map(async (file) => {
        try {
            const attachment = await (0, processAttachment_1.processAttachment)(file);
            if (!attachment) {
                removeAttachment(conversationId, file.path);
                return;
            }
            addAttachment(conversationId, attachment);
        }
        catch (err) {
            log.error('handleAttachmentsProcessing: failed to process attachment:', err.stack);
            removeAttachment(conversationId, file.path);
            onShowToast(AttachmentToastType_1.AttachmentToastType.ToastUnableToLoadAttachment);
        }
    }));
}
exports.handleAttachmentsProcessing = handleAttachmentsProcessing;
