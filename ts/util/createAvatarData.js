"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.createAvatarData = void 0;
const uuid_1 = require("uuid");
function createAvatarData(partialAvatarData) {
    return Object.assign({ id: (0, uuid_1.v4)() }, partialAvatarData);
}
exports.createAvatarData = createAvatarData;
