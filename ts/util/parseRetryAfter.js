"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseRetryAfter = void 0;
const isNormalNumber_1 = require("./isNormalNumber");
const ONE_SECOND = 1000;
const MINIMAL_RETRY_AFTER = ONE_SECOND;
function parseRetryAfter(value) {
    if (typeof value !== 'string') {
        return MINIMAL_RETRY_AFTER;
    }
    let retryAfter = parseInt(value, 10);
    if (!(0, isNormalNumber_1.isNormalNumber)(retryAfter) || retryAfter.toString() !== value) {
        retryAfter = 0;
    }
    return Math.max(retryAfter * ONE_SECOND, MINIMAL_RETRY_AFTER);
}
exports.parseRetryAfter = parseRetryAfter;
