"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.normalizeUuid = void 0;
const UUID_1 = require("../types/UUID");
const assert_1 = require("./assert");
function normalizeUuid(uuid, context) {
    (0, assert_1.assert)((0, UUID_1.isValidUuid)(uuid), `Normalizing invalid uuid: ${uuid} in context "${context}"`);
    return uuid.toLowerCase();
}
exports.normalizeUuid = normalizeUuid;
