"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.canvasToBytes = void 0;
const canvasToBlob_1 = require("./canvasToBlob");
async function canvasToBytes(canvas, mimeType, quality) {
    const blob = await (0, canvasToBlob_1.canvasToBlob)(canvas, mimeType, quality);
    return new Uint8Array(await blob.arrayBuffer());
}
exports.canvasToBytes = canvasToBytes;
