"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.toggleMaximizedBrowserWindow = void 0;
function toggleMaximizedBrowserWindow(browserWindow) {
    if (browserWindow.isMaximized()) {
        browserWindow.unmaximize();
    }
    else {
        browserWindow.maximize();
    }
}
exports.toggleMaximizedBrowserWindow = toggleMaximizedBrowserWindow;
