"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.retryMessageSend = void 0;
async function retryMessageSend(messageId) {
    const message = window.MessageController.getById(messageId);
    if (!message) {
        throw new Error(`retryMessageSend: Message ${messageId} missing!`);
    }
    await message.retrySend();
}
exports.retryMessageSend = retryMessageSend;
