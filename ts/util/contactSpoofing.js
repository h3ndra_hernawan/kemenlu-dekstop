"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactSpoofingType = void 0;
var ContactSpoofingType;
(function (ContactSpoofingType) {
    ContactSpoofingType[ContactSpoofingType["DirectConversationWithSameTitle"] = 0] = "DirectConversationWithSameTitle";
    ContactSpoofingType[ContactSpoofingType["MultipleGroupMembersWithSameTitle"] = 1] = "MultipleGroupMembersWithSameTitle";
})(ContactSpoofingType = exports.ContactSpoofingType || (exports.ContactSpoofingType = {}));
