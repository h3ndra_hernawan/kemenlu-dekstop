"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageController = void 0;
const durations = __importStar(require("./durations"));
const iterables_1 = require("./iterables");
const isNotNil_1 = require("./isNotNil");
const FIVE_MINUTES = 5 * durations.MINUTE;
class MessageController {
    constructor() {
        this.messageLookup = Object.create(null);
        this.msgIDsBySender = new Map();
        this.msgIDsBySentAt = new Map();
    }
    static install() {
        const instance = new MessageController();
        window.MessageController = instance;
        instance.startCleanupInterval();
        return instance;
    }
    register(id, message) {
        if (!id || !message) {
            return message;
        }
        const existing = this.messageLookup[id];
        if (existing) {
            this.messageLookup[id] = {
                message: existing.message,
                timestamp: Date.now(),
            };
            return existing.message;
        }
        this.messageLookup[id] = {
            message,
            timestamp: Date.now(),
        };
        const sentAt = message.get('sent_at');
        const previousIdsBySentAt = this.msgIDsBySentAt.get(sentAt);
        if (previousIdsBySentAt) {
            previousIdsBySentAt.add(id);
        }
        else {
            this.msgIDsBySentAt.set(sentAt, new Set([id]));
        }
        this.msgIDsBySender.set(message.getSenderIdentifier(), id);
        return message;
    }
    unregister(id) {
        const { message } = this.messageLookup[id] || {};
        if (message) {
            this.msgIDsBySender.delete(message.getSenderIdentifier());
            const sentAt = message.get('sent_at');
            const idsBySentAt = this.msgIDsBySentAt.get(sentAt) || new Set();
            idsBySentAt.delete(id);
            if (!idsBySentAt.size) {
                this.msgIDsBySentAt.delete(sentAt);
            }
        }
        delete this.messageLookup[id];
    }
    cleanup() {
        var _a;
        const messages = Object.values(this.messageLookup);
        const now = Date.now();
        for (let i = 0, max = messages.length; i < max; i += 1) {
            const { message, timestamp } = messages[i];
            const conversation = message.getConversation();
            const state = window.reduxStore.getState();
            const selectedId = (_a = state === null || state === void 0 ? void 0 : state.conversations) === null || _a === void 0 ? void 0 : _a.selectedConversationId;
            const inActiveConversation = conversation && selectedId && conversation.id === selectedId;
            if (now - timestamp > FIVE_MINUTES && !inActiveConversation) {
                this.unregister(message.id);
            }
        }
    }
    getById(id) {
        const existing = this.messageLookup[id];
        return existing && existing.message ? existing.message : undefined;
    }
    filterBySentAt(sentAt) {
        const ids = this.msgIDsBySentAt.get(sentAt) || [];
        const maybeMessages = (0, iterables_1.map)(ids, id => this.getById(id));
        return (0, iterables_1.filter)(maybeMessages, isNotNil_1.isNotNil);
    }
    findBySender(sender) {
        const id = this.msgIDsBySender.get(sender);
        if (!id) {
            return undefined;
        }
        return this.getById(id);
    }
    _get() {
        return this.messageLookup;
    }
    startCleanupInterval() {
        return setInterval(this.cleanup.bind(this), durations.HOUR);
    }
}
exports.MessageController = MessageController;
