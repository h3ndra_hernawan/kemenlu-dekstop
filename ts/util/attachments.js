"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createDeleter = exports.getTempPath = exports.getStickersPath = exports.getPath = exports.getDraftPath = exports.getBadgesPath = exports.getAvatarsPath = void 0;
const lodash_1 = require("lodash");
const path_1 = require("path");
const fs_extra_1 = __importDefault(require("fs-extra"));
const isPathInside_1 = require("./isPathInside");
const PATH = 'attachments.noindex';
const AVATAR_PATH = 'avatars.noindex';
const BADGES_PATH = 'badges.noindex';
const STICKER_PATH = 'stickers.noindex';
const TEMP_PATH = 'temp';
const DRAFT_PATH = 'drafts.noindex';
const createPathGetter = (subpath) => (userDataPath) => {
    if (!(0, lodash_1.isString)(userDataPath)) {
        throw new TypeError("'userDataPath' must be a string");
    }
    return (0, path_1.join)(userDataPath, subpath);
};
exports.getAvatarsPath = createPathGetter(AVATAR_PATH);
exports.getBadgesPath = createPathGetter(BADGES_PATH);
exports.getDraftPath = createPathGetter(DRAFT_PATH);
exports.getPath = createPathGetter(PATH);
exports.getStickersPath = createPathGetter(STICKER_PATH);
exports.getTempPath = createPathGetter(TEMP_PATH);
const createDeleter = (root) => {
    if (!(0, lodash_1.isString)(root)) {
        throw new TypeError("'root' must be a path");
    }
    return async (relativePath) => {
        if (!(0, lodash_1.isString)(relativePath)) {
            throw new TypeError("'relativePath' must be a string");
        }
        const absolutePath = (0, path_1.join)(root, relativePath);
        const normalized = (0, path_1.normalize)(absolutePath);
        if (!(0, isPathInside_1.isPathInside)(normalized, root)) {
            throw new Error('Invalid relative path');
        }
        await fs_extra_1.default.remove(absolutePath);
    };
};
exports.createDeleter = createDeleter;
