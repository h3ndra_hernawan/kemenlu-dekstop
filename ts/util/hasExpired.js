"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.hasExpired = void 0;
const environment_1 = require("../environment");
const timestamp_1 = require("./timestamp");
const log = __importStar(require("../logging/log"));
const ONE_DAY_MS = 86400 * 1000;
const NINETY_ONE_DAYS = 91 * ONE_DAY_MS;
const THIRTY_ONE_DAYS = 31 * ONE_DAY_MS;
function hasExpired() {
    const { getExpiration } = window;
    let buildExpiration = 0;
    try {
        buildExpiration = parseInt(getExpiration(), 10);
        if (buildExpiration) {
            log.info('Build expires: ', new Date(buildExpiration).toISOString());
        }
    }
    catch (e) {
        log.error('Error retrieving build expiration date', e.stack);
        return true;
    }
    if ((0, environment_1.getEnvironment)() === environment_1.Environment.Production) {
        const safeExpirationMs = window.Events.getAutoDownloadUpdate()
            ? NINETY_ONE_DAYS
            : THIRTY_ONE_DAYS;
        const buildExpirationDuration = buildExpiration - Date.now();
        const tooFarIntoFuture = buildExpirationDuration > safeExpirationMs;
        if (tooFarIntoFuture) {
            log.error('Build expiration is set too far into the future', buildExpiration);
        }
        return tooFarIntoFuture || (0, timestamp_1.isInPast)(buildExpiration);
    }
    return buildExpiration !== 0 && (0, timestamp_1.isInPast)(buildExpiration);
}
exports.hasExpired = hasExpired;
