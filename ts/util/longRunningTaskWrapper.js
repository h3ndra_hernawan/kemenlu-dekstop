"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.longRunningTaskWrapper = void 0;
const log = __importStar(require("../logging/log"));
async function longRunningTaskWrapper({ name, idForLogging, task, suppressErrorDialog, }) {
    const idLog = `${name}/${idForLogging}`;
    const ONE_SECOND = 1000;
    const TWO_SECONDS = 2000;
    let progressView;
    let spinnerStart;
    let progressTimeout = setTimeout(() => {
        log.info(`longRunningTaskWrapper/${idLog}: Creating spinner`);
        // Note: this component uses a portal to render itself into the top-level DOM. No
        //   need to attach it to the DOM here.
        progressView = new window.Whisper.ReactWrapperView({
            className: 'progress-modal-wrapper',
            Component: window.Signal.Components.ProgressModal,
        });
        spinnerStart = Date.now();
    }, TWO_SECONDS);
    // Note: any task we put here needs to have its own safety valve; this function will
    //   show a spinner until it's done
    try {
        log.info(`longRunningTaskWrapper/${idLog}: Starting task`);
        const result = await task();
        log.info(`longRunningTaskWrapper/${idLog}: Task completed successfully`);
        if (progressTimeout) {
            clearTimeout(progressTimeout);
            progressTimeout = undefined;
        }
        if (progressView) {
            const now = Date.now();
            if (spinnerStart && now - spinnerStart < ONE_SECOND) {
                log.info(`longRunningTaskWrapper/${idLog}: Spinner shown for less than second, showing for another second`);
                await window.Signal.Util.sleep(ONE_SECOND);
            }
            progressView.remove();
            progressView = undefined;
        }
        return result;
    }
    catch (error) {
        log.error(`longRunningTaskWrapper/${idLog}: Error!`, error && error.stack ? error.stack : error);
        if (progressTimeout) {
            clearTimeout(progressTimeout);
            progressTimeout = undefined;
        }
        if (progressView) {
            progressView.remove();
            progressView = undefined;
        }
        if (!suppressErrorDialog) {
            log.info(`longRunningTaskWrapper/${idLog}: Showing error dialog`);
            // Note: this component uses a portal to render itself into the top-level DOM. No
            //   need to attach it to the DOM here.
            const errorView = new window.Whisper.ReactWrapperView({
                className: 'error-modal-wrapper',
                Component: window.Signal.Components.ErrorModal,
                props: {
                    onClose: () => {
                        errorView.remove();
                    },
                },
            });
        }
        throw error;
    }
}
exports.longRunningTaskWrapper = longRunningTaskWrapper;
