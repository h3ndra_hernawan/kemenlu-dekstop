"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getStringForProfileChange = void 0;
function getStringForProfileChange(change, changedContact, i18n) {
    if (change.type === 'name') {
        return changedContact.name
            ? i18n('contactChangedProfileName', {
                sender: changedContact.title,
                oldProfile: change.oldName,
                newProfile: change.newName,
            })
            : i18n('changedProfileName', {
                oldProfile: change.oldName,
                newProfile: change.newName,
            });
    }
    throw new Error('TimelineItem: Unknown type!');
}
exports.getStringForProfileChange = getStringForProfileChange;
