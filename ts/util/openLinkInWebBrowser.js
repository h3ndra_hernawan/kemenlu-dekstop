"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.openLinkInWebBrowser = void 0;
function openLinkInWebBrowser(url) {
    window.location.href = url;
}
exports.openLinkInWebBrowser = openLinkInWebBrowser;
