"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isRecord = void 0;
const isRecord = (value) => typeof value === 'object' && !Array.isArray(value) && value !== null;
exports.isRecord = isRecord;
