"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendReadReceiptsFor = void 0;
const lodash_1 = require("lodash");
const getSendOptions_1 = require("./getSendOptions");
const handleMessageSend_1 = require("./handleMessageSend");
const isConversationAccepted_1 = require("./isConversationAccepted");
const log = __importStar(require("../logging/log"));
const CHUNK_SIZE = 100;
async function sendReadReceiptsFor(conversationAttrs, items) {
    // Only send read receipts for accepted conversations
    if (window.Events.getReadReceiptSetting() &&
        (0, isConversationAccepted_1.isConversationAccepted)(conversationAttrs)) {
        log.info(`Sending ${items.length} read receipts`);
        const sendOptions = await (0, getSendOptions_1.getSendOptions)(conversationAttrs);
        const receiptsBySender = (0, lodash_1.groupBy)(items, 'senderId');
        await Promise.all((0, lodash_1.map)(receiptsBySender, async (receipts, senderId) => {
            const conversation = window.ConversationController.get(senderId);
            if (!conversation) {
                return;
            }
            const batches = (0, lodash_1.chunk)(receipts, CHUNK_SIZE);
            await Promise.all(batches.map(batch => {
                const timestamps = (0, lodash_1.map)(batch, item => item.timestamp);
                const messageIds = (0, lodash_1.map)(batch, item => item.messageId);
                return (0, handleMessageSend_1.handleMessageSend)(window.textsecure.messaging.sendReadReceipts({
                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                    senderE164: conversation.get('e164'),
                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                    senderUuid: conversation.get('uuid'),
                    timestamps,
                    options: sendOptions,
                }), { messageIds, sendType: 'readReceipt' });
            }));
        }));
    }
}
exports.sendReadReceiptsFor = sendReadReceiptsFor;
