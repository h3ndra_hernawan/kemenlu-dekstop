"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.markAllAsApproved = void 0;
async function markAllAsApproved(untrusted) {
    await Promise.all(untrusted.map(contact => contact.setApproved()));
}
exports.markAllAsApproved = markAllAsApproved;
