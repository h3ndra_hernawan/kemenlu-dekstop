"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setupI18n = void 0;
const log = __importStar(require("../logging/log"));
function setupI18n(locale, messages) {
    if (!locale) {
        throw new Error('i18n: locale parameter is required');
    }
    if (!messages) {
        throw new Error('i18n: messages parameter is required');
    }
    const getMessage = (key, substitutions) => {
        const entry = messages[key];
        if (!entry) {
            log.error(`i18n: Attempted to get translation for nonexistent key '${key}'`);
            return '';
        }
        if (Array.isArray(substitutions) && substitutions.length > 1) {
            throw new Error('Array syntax is not supported with more than one placeholder');
        }
        if (typeof substitutions === 'string' ||
            typeof substitutions === 'number') {
            throw new Error('You must provide either a map or an array');
        }
        const { message } = entry;
        if (!substitutions) {
            return message;
        }
        if (Array.isArray(substitutions)) {
            return substitutions.reduce((result, substitution) => result.replace(/\$.+?\$/, substitution), message);
        }
        const FIND_REPLACEMENTS = /\$([^$]+)\$/g;
        let match = FIND_REPLACEMENTS.exec(message);
        let builder = '';
        let lastTextIndex = 0;
        while (match) {
            if (lastTextIndex < match.index) {
                builder += message.slice(lastTextIndex, match.index);
            }
            const placeholderName = match[1];
            const value = substitutions[placeholderName];
            if (!value) {
                log.error(`i18n: Value not provided for placeholder ${placeholderName} in key '${key}'`);
            }
            builder += value || '';
            lastTextIndex = FIND_REPLACEMENTS.lastIndex;
            match = FIND_REPLACEMENTS.exec(message);
        }
        if (lastTextIndex < message.length) {
            builder += message.slice(lastTextIndex);
        }
        return builder;
    };
    getMessage.getLocale = () => locale;
    return getMessage;
}
exports.setupI18n = setupI18n;
