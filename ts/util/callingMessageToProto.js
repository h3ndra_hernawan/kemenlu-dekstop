"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.callingMessageToProto = void 0;
const ringrtc_1 = require("ringrtc");
const protobuf_1 = require("../protobuf");
const log = __importStar(require("../logging/log"));
const missingCaseError_1 = require("./missingCaseError");
function callingMessageToProto({ offer, answer, iceCandidates, legacyHangup, busy, hangup, supportsMultiRing, destinationDeviceId, opaque, }, urgency) {
    let opaqueField;
    if (opaque) {
        opaqueField = Object.assign(Object.assign({}, opaque), { data: bufferToProto(opaque.data) });
    }
    if (urgency !== undefined) {
        opaqueField = Object.assign(Object.assign({}, (opaqueField !== null && opaqueField !== void 0 ? opaqueField : {})), { urgency: urgencyToProto(urgency) });
    }
    return {
        offer: offer
            ? Object.assign(Object.assign({}, offer), { type: offer.type, opaque: bufferToProto(offer.opaque) }) : undefined,
        answer: answer
            ? Object.assign(Object.assign({}, answer), { opaque: bufferToProto(answer.opaque) }) : undefined,
        iceCandidates: iceCandidates
            ? iceCandidates.map(candidate => {
                return Object.assign(Object.assign({}, candidate), { opaque: bufferToProto(candidate.opaque) });
            })
            : undefined,
        legacyHangup: legacyHangup
            ? Object.assign(Object.assign({}, legacyHangup), { type: legacyHangup.type }) : undefined,
        busy,
        hangup: hangup
            ? Object.assign(Object.assign({}, hangup), { type: hangup.type }) : undefined,
        supportsMultiRing,
        destinationDeviceId,
        opaque: opaqueField,
    };
}
exports.callingMessageToProto = callingMessageToProto;
function bufferToProto(value) {
    if (!value) {
        return undefined;
    }
    if (value instanceof Uint8Array) {
        return value;
    }
    return new Uint8Array(value.toArrayBuffer());
}
function urgencyToProto(urgency) {
    switch (urgency) {
        case ringrtc_1.CallMessageUrgency.Droppable:
            return protobuf_1.SignalService.CallingMessage.Opaque.Urgency.DROPPABLE;
        case ringrtc_1.CallMessageUrgency.HandleImmediately:
            return protobuf_1.SignalService.CallingMessage.Opaque.Urgency.HANDLE_IMMEDIATELY;
        default:
            log.error((0, missingCaseError_1.missingCaseError)(urgency));
            return protobuf_1.SignalService.CallingMessage.Opaque.Urgency.DROPPABLE;
    }
}
