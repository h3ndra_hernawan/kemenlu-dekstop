"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.count = void 0;
function count(str) {
    return Array.from(str).length;
}
exports.count = count;
