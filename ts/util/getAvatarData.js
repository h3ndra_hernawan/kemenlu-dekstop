"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAvatarData = void 0;
const Avatar_1 = require("../types/Avatar");
const whatTypeOfConversation_1 = require("./whatTypeOfConversation");
function getAvatarData(conversationAttrs) {
    const { avatars } = conversationAttrs;
    if (avatars && avatars.length) {
        return avatars;
    }
    const isGroup = !(0, whatTypeOfConversation_1.isDirectConversation)(conversationAttrs);
    return (0, Avatar_1.getDefaultAvatars)(isGroup);
}
exports.getAvatarData = getAvatarData;
