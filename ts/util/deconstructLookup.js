"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.deconstructLookup = void 0;
const getOwn_1 = require("./getOwn");
const assert_1 = require("./assert");
const deconstructLookup = (lookup, keys) => {
    const result = [];
    keys.forEach((key) => {
        const value = (0, getOwn_1.getOwn)(lookup, key);
        if (value) {
            result.push(value);
        }
        else {
            (0, assert_1.assert)(false, `deconstructLookup: lookup failed for ${key}; dropping`);
        }
    });
    return result;
};
exports.deconstructLookup = deconstructLookup;
