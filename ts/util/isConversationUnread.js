"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isConversationUnread = void 0;
const lodash_1 = require("lodash");
const isConversationUnread = ({ markedUnread, unreadCount, }) => Boolean(markedUnread || ((0, lodash_1.isNumber)(unreadCount) && unreadCount > 0));
exports.isConversationUnread = isConversationUnread;
