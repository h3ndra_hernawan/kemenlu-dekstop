"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.shallowDropNull = exports.dropNull = void 0;
function dropNull(value) {
    if (value === null) {
        return undefined;
    }
    return value;
}
exports.dropNull = dropNull;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function shallowDropNull(value) {
    if (value === null || value === undefined) {
        return undefined;
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const result = {};
    for (const [key, propertyValue] of Object.entries(value)) {
        result[key] = dropNull(propertyValue);
    }
    return result;
}
exports.shallowDropNull = shallowDropNull;
