"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.showToast = void 0;
const react_1 = __importDefault(require("react"));
const react_dom_1 = require("react-dom");
// eslint-disable-next-line max-len
// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
function showToast(Toast, props = {}) {
    const node = document.getElementById('toast');
    function onClose() {
        if (!node) {
            return;
        }
        (0, react_dom_1.unmountComponentAtNode)(node);
    }
    (0, react_dom_1.render)(react_1.default.createElement(Toast, Object.assign({ i18n: window.i18n, onClose: onClose }, props)), node);
}
exports.showToast = showToast;
