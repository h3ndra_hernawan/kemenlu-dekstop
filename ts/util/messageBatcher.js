"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.saveNewMessageBatcher = exports.setBatchingStrategy = exports.queueUpdateMessage = void 0;
const batcher_1 = require("./batcher");
const waitBatcher_1 = require("./waitBatcher");
const log = __importStar(require("../logging/log"));
const updateMessageBatcher = (0, batcher_1.createBatcher)({
    name: 'messageBatcher.updateMessageBatcher',
    wait: 75,
    maxSize: 50,
    processBatch: async (messageAttrs) => {
        log.info('updateMessageBatcher', messageAttrs.length);
        await window.Signal.Data.saveMessages(messageAttrs);
    },
});
let shouldBatch = true;
function queueUpdateMessage(messageAttr) {
    if (shouldBatch) {
        updateMessageBatcher.add(messageAttr);
    }
    else {
        window.Signal.Data.saveMessage(messageAttr);
    }
}
exports.queueUpdateMessage = queueUpdateMessage;
function setBatchingStrategy(keepBatching = false) {
    shouldBatch = keepBatching;
}
exports.setBatchingStrategy = setBatchingStrategy;
exports.saveNewMessageBatcher = (0, waitBatcher_1.createWaitBatcher)({
    name: 'messageBatcher.saveNewMessageBatcher',
    wait: 75,
    maxSize: 30,
    processBatch: async (messageAttrs) => {
        log.info('saveNewMessageBatcher', messageAttrs.length);
        await window.Signal.Data.saveMessages(messageAttrs, {
            forceSave: true,
        });
    },
});
