"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.markAllAsVerifiedDefault = void 0;
async function markAllAsVerifiedDefault(unverified) {
    await Promise.all(unverified.map(contact => {
        if (contact.isUnverified()) {
            return contact.setVerifiedDefault();
        }
        return null;
    }));
}
exports.markAllAsVerifiedDefault = markAllAsVerifiedDefault;
