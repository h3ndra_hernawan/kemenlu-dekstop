"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isInSystemContacts = void 0;
const isInSystemContacts = ({ type, name, }) => 
// `direct` for redux, `private` for models and the database
(type === 'direct' || type === 'private') && typeof name === 'string';
exports.isInSystemContacts = isInSystemContacts;
