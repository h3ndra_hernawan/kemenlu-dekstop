"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.groupBy = void 0;
const iterables_1 = require("./iterables");
/**
 * Like Lodash's `groupBy`, but returns a `Map`.
 */
const groupBy = (iterable, fn) => (0, iterables_1.reduce)(iterable, (result, value) => {
    const key = fn(value);
    const existingGroup = result.get(key);
    if (existingGroup) {
        existingGroup.push(value);
    }
    else {
        result.set(key, [value]);
    }
    return result;
}, new Map());
exports.groupBy = groupBy;
