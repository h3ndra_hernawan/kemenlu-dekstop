"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isWindowDragElement = void 0;
function isWindowDragElement(el) {
    let currentEl = el;
    do {
        const appRegion = getComputedStyle(currentEl).getPropertyValue('-webkit-app-region');
        switch (appRegion) {
            case 'no-drag':
                return false;
            case 'drag':
                return true;
            default:
                currentEl = currentEl.parentElement;
                break;
        }
    } while (currentEl);
    return false;
}
exports.isWindowDragElement = isWindowDragElement;
