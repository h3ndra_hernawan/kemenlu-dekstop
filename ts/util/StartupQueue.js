"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StartupQueue = void 0;
const Errors = __importStar(require("../types/errors"));
const log = __importStar(require("../logging/log"));
class StartupQueue {
    constructor() {
        this.map = new Map();
    }
    add(id, value, f) {
        const existing = this.map.get(id);
        if (existing && existing.value >= value) {
            return;
        }
        this.map.set(id, { value, callback: f });
    }
    flush() {
        log.info('StartupQueue: Processing', this.map.size, 'actions');
        const values = Array.from(this.map.values());
        this.map.clear();
        for (const { callback } of values) {
            try {
                callback();
            }
            catch (error) {
                log.error('StartupQueue: Failed to process item due to error', Errors.toLogFormat(error));
            }
        }
    }
}
exports.StartupQueue = StartupQueue;
