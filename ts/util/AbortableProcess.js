"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
/* eslint-disable no-restricted-syntax */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AbortableProcess = void 0;
const explodePromise_1 = require("./explodePromise");
class AbortableProcess {
    constructor(name, controller, resultPromise) {
        this.name = name;
        this.controller = controller;
        const { promise: abortPromise, reject: abortReject } = (0, explodePromise_1.explodePromise)();
        this.abortReject = abortReject;
        this.resultPromise = Promise.race([abortPromise, resultPromise]);
    }
    abort() {
        this.controller.abort();
        this.abortReject(new Error(`Process "${this.name}" was aborted`));
    }
    getResult() {
        return this.resultPromise;
    }
}
exports.AbortableProcess = AbortableProcess;
