"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.fileToBytes = void 0;
function fileToBytes(file) {
    return new Promise((resolve, rejectPromise) => {
        const FR = new FileReader();
        FR.onload = () => {
            if (!FR.result || typeof FR.result === 'string') {
                rejectPromise(new Error('bytesFromFile: No result!'));
                return;
            }
            resolve(new Uint8Array(FR.result));
        };
        FR.onerror = rejectPromise;
        FR.onabort = rejectPromise;
        FR.readAsArrayBuffer(file);
    });
}
exports.fileToBytes = fileToBytes;
