"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.fromWebSafeBase64 = exports.toWebSafeBase64 = void 0;
function toWebSafeBase64(base64) {
    return base64.replace(/\//g, '_').replace(/\+/g, '-').replace(/=/g, '');
}
exports.toWebSafeBase64 = toWebSafeBase64;
function fromWebSafeBase64(webSafeBase64) {
    const base64 = webSafeBase64.replace(/_/g, '/').replace(/-/g, '+');
    // Ensure that the character count is a multiple of four, filling in the extra
    //   space needed with '='
    const remainder = base64.length % 4;
    if (remainder === 3) {
        return `${base64}=`;
    }
    if (remainder === 2) {
        return `${base64}==`;
    }
    if (remainder === 1) {
        return `${base64}===`;
    }
    return base64;
}
exports.fromWebSafeBase64 = fromWebSafeBase64;
