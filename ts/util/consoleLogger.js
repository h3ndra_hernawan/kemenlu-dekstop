"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.consoleLogger = void 0;
/* eslint-disable no-console */
exports.consoleLogger = {
    fatal(...args) {
        console.error(...args);
    },
    error(...args) {
        console.error(...args);
    },
    warn(...args) {
        console.warn(...args);
    },
    info(...args) {
        console.info(...args);
    },
    debug(...args) {
        console.debug(...args);
    },
    trace(...args) {
        console.log(...args);
    },
};
/* eslint-enable no-console */
