"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isSorted = void 0;
function isSorted(list) {
    let previousItem;
    for (const item of list) {
        if (previousItem !== undefined && item < previousItem) {
            return false;
        }
        previousItem = item;
    }
    return true;
}
exports.isSorted = isSorted;
