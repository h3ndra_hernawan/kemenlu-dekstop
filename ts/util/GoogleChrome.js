"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isVideoTypeSupported = exports.isImageTypeSupported = void 0;
// See: https://en.wikipedia.org/wiki/Comparison_of_web_browsers#Image_format_support
const SUPPORTED_IMAGE_MIME_TYPES = {
    'image/bmp': true,
    'image/gif': true,
    'image/jpeg': true,
    // No need to support SVG
    'image/svg+xml': false,
    'image/webp': true,
    'image/x-xbitmap': true,
    // ICO
    'image/vnd.microsoft.icon': true,
    'image/ico': true,
    'image/icon': true,
    'image/x-icon': true,
    // PNG
    'image/apng': true,
    'image/png': true,
};
const isImageTypeSupported = (mimeType) => SUPPORTED_IMAGE_MIME_TYPES[mimeType] === true;
exports.isImageTypeSupported = isImageTypeSupported;
const SUPPORTED_VIDEO_MIME_TYPES = {
    'video/mp4': true,
    'video/ogg': true,
    'video/webm': true,
};
// See: https://www.chromium.org/audio-video
const isVideoTypeSupported = (mimeType) => SUPPORTED_VIDEO_MIME_TYPES[mimeType] === true;
exports.isVideoTypeSupported = isVideoTypeSupported;
