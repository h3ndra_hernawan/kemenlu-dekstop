"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.markConversationRead = void 0;
const sendReadReceiptsFor_1 = require("./sendReadReceiptsFor");
const message_1 = require("../state/selectors/message");
const readSyncJobQueue_1 = require("../jobs/readSyncJobQueue");
const notifications_1 = require("../services/notifications");
const log = __importStar(require("../logging/log"));
async function markConversationRead(conversationAttrs, newestUnreadId, options = {
    sendReadReceipts: true,
}) {
    const { id: conversationId } = conversationAttrs;
    const [unreadMessages, unreadReactions] = await Promise.all([
        window.Signal.Data.getUnreadByConversationAndMarkRead(conversationId, newestUnreadId, options.readAt),
        window.Signal.Data.getUnreadReactionsAndMarkRead(conversationId, newestUnreadId),
    ]);
    log.info('markConversationRead', {
        conversationId,
        newestUnreadId,
        unreadMessages: unreadMessages.length,
        unreadReactions: unreadReactions.length,
    });
    if (!unreadMessages.length && !unreadReactions.length) {
        return false;
    }
    notifications_1.notificationService.removeBy({ conversationId });
    const unreadReactionSyncData = new Map();
    unreadReactions.forEach(reaction => {
        const targetKey = `${reaction.targetAuthorUuid}/${reaction.targetTimestamp}`;
        if (unreadReactionSyncData.has(targetKey)) {
            return;
        }
        unreadReactionSyncData.set(targetKey, {
            messageId: reaction.messageId,
            senderE164: undefined,
            senderUuid: reaction.targetAuthorUuid,
            timestamp: reaction.targetTimestamp,
        });
    });
    const allReadMessagesSync = unreadMessages.map(messageSyncData => {
        const message = window.MessageController.getById(messageSyncData.id);
        // we update the in-memory MessageModel with the fresh database call data
        if (message) {
            message.set(messageSyncData);
        }
        return {
            messageId: messageSyncData.id,
            senderE164: messageSyncData.source,
            senderUuid: messageSyncData.sourceUuid,
            senderId: window.ConversationController.ensureContactIds({
                e164: messageSyncData.source,
                uuid: messageSyncData.sourceUuid,
            }),
            timestamp: messageSyncData.sent_at,
            hasErrors: message ? (0, message_1.hasErrors)(message.attributes) : false,
        };
    });
    // Some messages we're marking read are local notifications with no sender
    // If a message has errors, we don't want to send anything out about it.
    //   read syncs - let's wait for a client that really understands the message
    //      to mark it read. we'll mark our local error read locally, though.
    //   read receipts - here we can run into infinite loops, where each time the
    //      conversation is viewed, another error message shows up for the contact
    const unreadMessagesSyncData = allReadMessagesSync.filter(item => Boolean(item.senderId) && !item.hasErrors);
    const readSyncs = [...unreadMessagesSyncData, ...unreadReactionSyncData.values()];
    if (readSyncs.length && options.sendReadReceipts) {
        log.info(`Sending ${readSyncs.length} read syncs`);
        // Because syncReadMessages sends to our other devices, and sendReadReceipts goes
        //   to a contact, we need accessKeys for both.
        if (window.ConversationController.areWePrimaryDevice()) {
            log.warn('markConversationRead: We are primary device; not sending read syncs');
        }
        else {
            readSyncJobQueue_1.readSyncJobQueue.add({ readSyncs });
        }
        await (0, sendReadReceiptsFor_1.sendReadReceiptsFor)(conversationAttrs, unreadMessagesSyncData);
    }
    window.Whisper.ExpiringMessagesListener.update();
    window.Whisper.TapToViewMessagesListener.update();
    return true;
}
exports.markConversationRead = markConversationRead;
