"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getConversationMembers = void 0;
const lodash_1 = require("lodash");
const whatTypeOfConversation_1 = require("./whatTypeOfConversation");
function getConversationMembers(conversationAttrs, options = {}) {
    if ((0, whatTypeOfConversation_1.isDirectConversation)(conversationAttrs)) {
        return [conversationAttrs];
    }
    if (conversationAttrs.membersV2) {
        const { includePendingMembers } = options;
        const members = includePendingMembers
            ? [
                ...(conversationAttrs.membersV2 || []),
                ...(conversationAttrs.pendingMembersV2 || []),
            ]
            : conversationAttrs.membersV2 || [];
        return (0, lodash_1.compact)(members.map(member => {
            const conversation = window.ConversationController.get(member.uuid);
            // In groups we won't sent to contacts we believe are unregistered
            if (conversation && conversation.isUnregistered()) {
                return null;
            }
            return conversation === null || conversation === void 0 ? void 0 : conversation.attributes;
        }));
    }
    if (conversationAttrs.members) {
        return (0, lodash_1.compact)(conversationAttrs.members.map(id => {
            const conversation = window.ConversationController.get(id);
            // In groups we won't send to contacts we believe are unregistered
            if (conversation && conversation.isUnregistered()) {
                return null;
            }
            return conversation === null || conversation === void 0 ? void 0 : conversation.attributes;
        }));
    }
    return [];
}
exports.getConversationMembers = getConversationMembers;
