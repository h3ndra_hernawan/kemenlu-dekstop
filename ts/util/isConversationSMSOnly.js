"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isConversationSMSOnly = void 0;
function isConversationSMSOnly(conversation) {
    const { e164, uuid, type } = conversation;
    // `direct` for redux, `private` for models and the database
    if (type !== 'direct' && type !== 'private') {
        return false;
    }
    if (e164 && !uuid) {
        return true;
    }
    return conversation.discoveredUnregisteredAt !== undefined;
}
exports.isConversationSMSOnly = isConversationSMSOnly;
