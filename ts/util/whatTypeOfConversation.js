"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.typeofConversation = exports.isGroupV2 = exports.isGroupV1 = exports.isMe = exports.isDirectConversation = exports.ConversationTypes = void 0;
const Bytes = __importStar(require("../Bytes"));
const log = __importStar(require("../logging/log"));
var ConversationTypes;
(function (ConversationTypes) {
    ConversationTypes["Me"] = "Me";
    ConversationTypes["Direct"] = "Direct";
    ConversationTypes["GroupV1"] = "GroupV1";
    ConversationTypes["GroupV2"] = "GroupV2";
})(ConversationTypes = exports.ConversationTypes || (exports.ConversationTypes = {}));
function isDirectConversation(conversationAttrs) {
    return (conversationAttrs.type === 'private' || conversationAttrs.type === 'direct');
}
exports.isDirectConversation = isDirectConversation;
function isMe(conversationAttrs) {
    var _a;
    const { e164, uuid } = conversationAttrs;
    const ourNumber = window.textsecure.storage.user.getNumber();
    const ourUuid = (_a = window.textsecure.storage.user.getUuid()) === null || _a === void 0 ? void 0 : _a.toString();
    return Boolean((e164 && e164 === ourNumber) || (uuid && uuid === ourUuid));
}
exports.isMe = isMe;
function isGroupV1(conversationAttrs) {
    const { groupId } = conversationAttrs;
    if (!groupId) {
        return false;
    }
    const buffer = Bytes.fromBinary(groupId);
    return buffer.byteLength === window.Signal.Groups.ID_V1_LENGTH;
}
exports.isGroupV1 = isGroupV1;
function isGroupV2(conversationAttrs) {
    const { groupId, groupVersion = 0 } = conversationAttrs;
    if (!groupId) {
        return false;
    }
    try {
        return (groupVersion === 2 &&
            Bytes.fromBase64(groupId).byteLength === window.Signal.Groups.ID_LENGTH);
    }
    catch (error) {
        log.error('isGroupV2: Failed to process groupId in base64!');
        return false;
    }
}
exports.isGroupV2 = isGroupV2;
function typeofConversation(conversationAttrs) {
    if (isMe(conversationAttrs)) {
        return ConversationTypes.Me;
    }
    if (isDirectConversation(conversationAttrs)) {
        return ConversationTypes.Direct;
    }
    if (isGroupV2(conversationAttrs)) {
        return ConversationTypes.GroupV2;
    }
    if (isGroupV1(conversationAttrs)) {
        return ConversationTypes.GroupV1;
    }
    return undefined;
}
exports.typeofConversation = typeofConversation;
