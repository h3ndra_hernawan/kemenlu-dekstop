"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createBatcher = void 0;
const p_queue_1 = __importDefault(require("p-queue"));
const sleep_1 = require("./sleep");
const log = __importStar(require("../logging/log"));
window.batchers = [];
window.waitForAllBatchers = async () => {
    await Promise.all(window.batchers.map(item => item.flushAndWait()));
};
function createBatcher(options) {
    let batcher;
    let timeout;
    let items = [];
    const queue = new p_queue_1.default({ concurrency: 1, timeout: 1000 * 60 * 2 });
    function _kickBatchOff() {
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        const itemsRef = items;
        items = [];
        queue.add(async () => {
            await options.processBatch(itemsRef);
        });
    }
    function add(item) {
        items.push(item);
        if (items.length === 1) {
            // Set timeout once when we just pushed the first item so that the wait
            // time is bounded by `options.wait` and not extended by further pushes.
            timeout = setTimeout(_kickBatchOff, options.wait);
        }
        else if (items.length >= options.maxSize) {
            _kickBatchOff();
        }
    }
    function removeAll(needle) {
        items = items.filter(item => item !== needle);
    }
    function anyPending() {
        return queue.size > 0 || queue.pending > 0 || items.length > 0;
    }
    async function onIdle() {
        while (anyPending()) {
            if (queue.size > 0 || queue.pending > 0) {
                // eslint-disable-next-line no-await-in-loop
                await queue.onIdle();
            }
            if (items.length > 0) {
                // eslint-disable-next-line no-await-in-loop
                await (0, sleep_1.sleep)(options.wait * 2);
            }
        }
    }
    function unregister() {
        window.batchers = window.batchers.filter(item => item !== batcher);
    }
    async function flushAndWait() {
        log.info(`Flushing ${options.name} batcher items.length=${items.length}`);
        while (anyPending()) {
            _kickBatchOff();
            if (queue.size > 0 || queue.pending > 0) {
                // eslint-disable-next-line no-await-in-loop
                await queue.onIdle();
            }
        }
        log.info(`Flushing complete ${options.name} for batcher`);
    }
    batcher = {
        add,
        removeAll,
        anyPending,
        onIdle,
        flushAndWait,
        unregister,
    };
    window.batchers.push(batcher);
    return batcher;
}
exports.createBatcher = createBatcher;
