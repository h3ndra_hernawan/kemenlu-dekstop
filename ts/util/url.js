"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.setUrlSearchParams = exports.maybeParseUrl = void 0;
const lodash_1 = require("lodash");
function maybeParseUrl(value) {
    if (typeof value === 'string') {
        try {
            return new URL(value);
        }
        catch (err) {
            /* Errors are ignored. */
        }
    }
    return undefined;
}
exports.maybeParseUrl = maybeParseUrl;
function setUrlSearchParams(url, searchParams) {
    const result = cloneUrl(url);
    result.search = new URLSearchParams((0, lodash_1.mapValues)(searchParams, stringifySearchParamValue)).toString();
    return result;
}
exports.setUrlSearchParams = setUrlSearchParams;
function cloneUrl(url) {
    return new URL(url.href);
}
function stringifySearchParamValue(value) {
    return value == null ? '' : String(value);
}
