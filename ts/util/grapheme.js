"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.count = exports.getGraphemes = void 0;
const iterables_1 = require("./iterables");
function getGraphemes(str) {
    const segments = new Intl.Segmenter().segment(str);
    return (0, iterables_1.map)(segments, s => s.segment);
}
exports.getGraphemes = getGraphemes;
function count(str) {
    const segments = new Intl.Segmenter().segment(str);
    return (0, iterables_1.size)(segments);
}
exports.count = count;
