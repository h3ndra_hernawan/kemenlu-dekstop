"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.avatarDataToBytes = void 0;
const Colors_1 = require("../types/Colors");
const canvasToBytes_1 = require("./canvasToBytes");
const avatarTextSizeCalculator_1 = require("./avatarTextSizeCalculator");
const CANVAS_SIZE = 1024;
function getAvatarColor(color) {
    return Colors_1.AvatarColorMap.get(color) || { bg: 'black', fg: 'white' };
}
function setCanvasBackground(bg, context, canvas) {
    context.fillStyle = bg;
    context.fillRect(0, 0, canvas.width, canvas.height);
}
async function drawImage(src, context, canvas) {
    const image = new Image();
    image.src = src;
    await image.decode();
    // eslint-disable-next-line no-param-reassign
    canvas.width = image.width;
    // eslint-disable-next-line no-param-reassign
    canvas.height = image.height;
    context.drawImage(image, 0, 0);
}
async function getFont(text) {
    const font = new window.FontFace('Inter', 'url("fonts/inter-v3.10/Inter-Regular.woff2")');
    await font.load();
    const measurerCanvas = document.createElement('canvas');
    measurerCanvas.width = CANVAS_SIZE;
    measurerCanvas.height = CANVAS_SIZE;
    const measurerContext = measurerCanvas.getContext('2d');
    if (!measurerContext) {
        throw new Error('getFont: could not get canvas rendering context');
    }
    const fontSize = (0, avatarTextSizeCalculator_1.getFittedFontSize)(CANVAS_SIZE, text, candidateFontSize => {
        const candidateFont = `${candidateFontSize}px Inter`;
        measurerContext.font = candidateFont;
        const { actualBoundingBoxLeft, actualBoundingBoxRight, actualBoundingBoxAscent, actualBoundingBoxDescent, } = measurerContext.measureText(text);
        const width = Math.abs(actualBoundingBoxLeft) + Math.abs(actualBoundingBoxRight);
        const height = Math.abs(actualBoundingBoxAscent) + Math.abs(actualBoundingBoxDescent);
        return { height, width };
    });
    return `${fontSize}px Inter`;
}
async function avatarDataToBytes(avatarData) {
    var _a;
    const canvas = document.createElement('canvas');
    canvas.width = CANVAS_SIZE;
    canvas.height = CANVAS_SIZE;
    const context = canvas.getContext('2d');
    if (!context) {
        throw new Error('avatarDataToBytes: could not get canvas rendering context');
    }
    const { color, icon, imagePath, text } = avatarData;
    if (imagePath) {
        await drawImage(((_a = window.Signal) === null || _a === void 0 ? void 0 : _a.Migrations)
            ? window.Signal.Migrations.getAbsoluteAvatarPath(imagePath)
            : imagePath, context, canvas);
    }
    else if (color && text) {
        const { bg, fg } = getAvatarColor(color);
        const textToWrite = text.toLocaleUpperCase();
        setCanvasBackground(bg, context, canvas);
        context.fillStyle = fg;
        const font = await getFont(textToWrite);
        context.font = font;
        context.textBaseline = 'middle';
        context.textAlign = 'center';
        context.fillText(textToWrite, CANVAS_SIZE / 2, CANVAS_SIZE / 2 + 30);
    }
    else if (color && icon) {
        const iconPath = `images/avatars/avatar_${icon}.svg`;
        await drawImage(iconPath, context, canvas);
        context.globalCompositeOperation = 'destination-over';
        const { bg } = getAvatarColor(color);
        setCanvasBackground(bg, context, canvas);
    }
    return (0, canvasToBytes_1.canvasToBytes)(canvas);
}
exports.avatarDataToBytes = avatarDataToBytes;
