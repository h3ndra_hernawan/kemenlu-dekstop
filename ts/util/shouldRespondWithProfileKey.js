"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.shouldRespondWithProfileKey = void 0;
const whatTypeOfConversation_1 = require("./whatTypeOfConversation");
async function shouldRespondWithProfileKey(sender) {
    var _a, _b;
    if ((0, whatTypeOfConversation_1.isMe)(sender.attributes) || !sender.getAccepted() || sender.isBlocked()) {
        return false;
    }
    // We do message check in an attempt to avoid a database lookup. If someone was EVER in
    //   a shared group with us, we should've shared our profile key with them in the past,
    //   so we should respond with a profile key now.
    if ((_a = sender.get('sharedGroupNames')) === null || _a === void 0 ? void 0 : _a.length) {
        return true;
    }
    await sender.updateSharedGroups();
    return Boolean((_b = sender.get('sharedGroupNames')) === null || _b === void 0 ? void 0 : _b.length);
}
exports.shouldRespondWithProfileKey = shouldRespondWithProfileKey;
