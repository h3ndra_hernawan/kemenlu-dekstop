"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.parsePhoneNumberSharingMode = exports.PhoneNumberSharingMode = void 0;
const enum_1 = require("./enum");
// These strings are saved to disk, so be careful when changing them.
var PhoneNumberSharingMode;
(function (PhoneNumberSharingMode) {
    PhoneNumberSharingMode["Everybody"] = "Everybody";
    PhoneNumberSharingMode["ContactsOnly"] = "ContactsOnly";
    PhoneNumberSharingMode["Nobody"] = "Nobody";
})(PhoneNumberSharingMode = exports.PhoneNumberSharingMode || (exports.PhoneNumberSharingMode = {}));
exports.parsePhoneNumberSharingMode = (0, enum_1.makeEnumParser)(PhoneNumberSharingMode, PhoneNumberSharingMode.Everybody);
