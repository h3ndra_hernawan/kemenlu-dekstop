"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.sameWidthModifier = exports.offsetDistanceModifier = void 0;
/**
 * Shorthand for the [offset modifier][0] when you just wanna set the distance.
 *
 * [0]: https://popper.js.org/docs/v2/modifiers/offset/
 */
const offsetDistanceModifier = (distance) => ({
    name: 'offset',
    options: { offset: [undefined, distance] },
});
exports.offsetDistanceModifier = offsetDistanceModifier;
/**
 * Make the popper element the same width as the reference, even when you resize.
 *
 * Should probably be used with the "top-start", "top-end", "bottom-start", or
 * "bottom-end" placement.
 */
exports.sameWidthModifier = {
    name: 'sameWidth',
    enabled: true,
    phase: 'write',
    fn({ state }) {
        // eslint-disable-next-line no-param-reassign
        state.elements.popper.style.width = `${state.rects.reference.width}px`;
    },
};
