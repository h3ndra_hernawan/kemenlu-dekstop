"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getEmojiReducerState = exports.loadRecentEmojis = void 0;
const lodash_1 = require("lodash");
const Client_1 = __importDefault(require("../sql/Client"));
let initialState;
async function getRecentEmojisForRedux() {
    const recent = await Client_1.default.getRecentEmojis();
    return recent.map(e => e.shortName);
}
async function loadRecentEmojis() {
    const recents = await getRecentEmojisForRedux();
    initialState = {
        recents: (0, lodash_1.take)(recents, 32),
    };
}
exports.loadRecentEmojis = loadRecentEmojis;
function getEmojiReducerState() {
    return initialState;
}
exports.getEmojiReducerState = getEmojiReducerState;
