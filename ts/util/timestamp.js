"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.toDayMillis = exports.isInFuture = exports.isInPast = exports.isOlderThan = exports.isMoreRecentThan = void 0;
const ONE_DAY = 24 * 3600 * 1000;
function isMoreRecentThan(timestamp, delta) {
    return timestamp > Date.now() - delta;
}
exports.isMoreRecentThan = isMoreRecentThan;
function isOlderThan(timestamp, delta) {
    return timestamp <= Date.now() - delta;
}
exports.isOlderThan = isOlderThan;
function isInPast(timestamp) {
    return isOlderThan(timestamp, 0);
}
exports.isInPast = isInPast;
function isInFuture(timestamp) {
    return isMoreRecentThan(timestamp, 0);
}
exports.isInFuture = isInFuture;
function toDayMillis(timestamp) {
    return timestamp - (timestamp % ONE_DAY);
}
exports.toDayMillis = toDayMillis;
