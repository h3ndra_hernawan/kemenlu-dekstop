"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateSecurityNumberBlock = exports.generateSecurityNumber = void 0;
const signal_client_1 = require("@signalapp/signal-client");
const UUID_1 = require("../types/UUID");
const assert_1 = require("./assert");
const log = __importStar(require("../logging/log"));
async function generateSecurityNumber(ourNumber, ourKey, theirNumber, theirKey) {
    const ourNumberBuf = Buffer.from(ourNumber);
    const ourKeyObj = signal_client_1.PublicKey.deserialize(Buffer.from(ourKey));
    const theirNumberBuf = Buffer.from(theirNumber);
    const theirKeyObj = signal_client_1.PublicKey.deserialize(Buffer.from(theirKey));
    const fingerprint = signal_client_1.Fingerprint.new(5200, 2, ourNumberBuf, ourKeyObj, theirNumberBuf, theirKeyObj);
    const fingerprintString = fingerprint.displayableFingerprint().toString();
    return Promise.resolve(fingerprintString);
}
exports.generateSecurityNumber = generateSecurityNumber;
async function generateSecurityNumberBlock(contact) {
    const { storage } = window.textsecure;
    const ourNumber = storage.user.getNumber();
    const ourUuid = storage.user.getCheckedUuid();
    const us = storage.protocol.getIdentityRecord(ourUuid);
    const ourKey = us ? us.publicKey : null;
    const theirUuid = UUID_1.UUID.lookup(contact.id);
    const them = theirUuid
        ? await storage.protocol.getOrMigrateIdentityRecord(theirUuid)
        : undefined;
    const theirKey = them === null || them === void 0 ? void 0 : them.publicKey;
    if (!ourKey) {
        throw new Error('Could not load our key');
    }
    if (!theirKey) {
        throw new Error('Could not load their key');
    }
    if (!contact.e164) {
        log.error('generateSecurityNumberBlock: Attempted to generate security number for contact with no e164');
        return [];
    }
    (0, assert_1.assert)(ourNumber, 'Should have our number');
    const securityNumber = await generateSecurityNumber(ourNumber, ourKey, contact.e164, theirKey);
    const chunks = [];
    for (let i = 0; i < securityNumber.length; i += 5) {
        chunks.push(securityNumber.substring(i, i + 5));
    }
    return chunks;
}
exports.generateSecurityNumberBlock = generateSecurityNumberBlock;
