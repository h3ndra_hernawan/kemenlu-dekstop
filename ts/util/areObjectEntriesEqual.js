"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.areObjectEntriesEqual = void 0;
const areObjectEntriesEqual = (a, b, keys) => a === b || keys.every(key => a[key] === b[key]);
exports.areObjectEntriesEqual = areObjectEntriesEqual;
