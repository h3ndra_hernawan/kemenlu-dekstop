"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.arrayBufferToObjectURL = void 0;
const is_1 = __importDefault(require("@sindresorhus/is"));
const arrayBufferToObjectURL = ({ data, type, }) => {
    if (!is_1.default.arrayBuffer(data)) {
        throw new TypeError('`data` must be an ArrayBuffer');
    }
    const blob = new Blob([data], { type });
    return URL.createObjectURL(blob);
};
exports.arrayBufferToObjectURL = arrayBufferToObjectURL;
