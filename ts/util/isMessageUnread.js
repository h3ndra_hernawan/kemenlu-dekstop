"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isMessageUnread = void 0;
const MessageReadStatus_1 = require("../messages/MessageReadStatus");
const isMessageUnread = (message) => message.readStatus === MessageReadStatus_1.ReadStatus.Unread;
exports.isMessageUnread = isMessageUnread;
