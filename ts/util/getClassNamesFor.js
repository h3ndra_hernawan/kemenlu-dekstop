"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getClassNamesFor = void 0;
const classnames_1 = __importDefault(require("classnames"));
function getClassNamesFor(...modules) {
    return modifier => {
        const cx = modules.map(parentModule => parentModule && modifier !== undefined
            ? `${parentModule}${modifier}`
            : undefined);
        return (0, classnames_1.default)(cx);
    };
}
exports.getClassNamesFor = getClassNamesFor;
