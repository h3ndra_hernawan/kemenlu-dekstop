"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateAlphaVersion = exports.isAlpha = exports.isBeta = exports.isProduction = void 0;
const semver = __importStar(require("semver"));
const moment_1 = __importDefault(require("moment"));
const isProduction = (version) => {
    const parsed = semver.parse(version);
    if (!parsed) {
        return false;
    }
    return !parsed.prerelease.length && !parsed.build.length;
};
exports.isProduction = isProduction;
const isBeta = (version) => { var _a; return ((_a = semver.parse(version)) === null || _a === void 0 ? void 0 : _a.prerelease[0]) === 'beta'; };
exports.isBeta = isBeta;
const isAlpha = (version) => { var _a; return ((_a = semver.parse(version)) === null || _a === void 0 ? void 0 : _a.prerelease[0]) === 'alpha'; };
exports.isAlpha = isAlpha;
const generateAlphaVersion = (options) => {
    const { currentVersion, shortSha } = options;
    const parsed = semver.parse(currentVersion);
    if (!parsed) {
        throw new Error(`generateAlphaVersion: Invalid version ${currentVersion}`);
    }
    const formattedDate = (0, moment_1.default)().utc().format('YYYYMMDD.HH');
    const formattedVersion = `${parsed.major}.${parsed.minor}.${parsed.patch}`;
    return `${formattedVersion}-alpha.${formattedDate}-${shortSha}`;
};
exports.generateAlphaVersion = generateAlphaVersion;
