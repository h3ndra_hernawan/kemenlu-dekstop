"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.migrateColor = void 0;
const lodash_1 = require("lodash");
const Colors_1 = require("../types/Colors");
const NEW_COLOR_NAMES = new Set(Colors_1.AvatarColors);
function migrateColor(color) {
    if (color && NEW_COLOR_NAMES.has(color)) {
        return color;
    }
    return (0, lodash_1.sample)(Colors_1.AvatarColors) || Colors_1.AvatarColors[0];
}
exports.migrateColor = migrateColor;
