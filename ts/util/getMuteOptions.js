"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMuteOptions = void 0;
const durations = __importStar(require("./durations"));
const getMutedUntilText_1 = require("./getMutedUntilText");
const isMuted_1 = require("./isMuted");
function getMuteOptions(muteExpiresAt, i18n) {
    return [
        ...((0, isMuted_1.isMuted)(muteExpiresAt)
            ? [
                {
                    name: (0, getMutedUntilText_1.getMutedUntilText)(muteExpiresAt, i18n),
                    disabled: true,
                    value: -1,
                },
                {
                    name: i18n('unmute'),
                    value: 0,
                },
            ]
            : []),
        {
            name: i18n('muteHour'),
            value: durations.HOUR,
        },
        {
            name: i18n('muteEightHours'),
            value: 8 * durations.HOUR,
        },
        {
            name: i18n('muteDay'),
            value: durations.DAY,
        },
        {
            name: i18n('muteWeek'),
            value: durations.WEEK,
        },
        {
            name: i18n('muteAlways'),
            value: Number.MAX_SAFE_INTEGER,
        },
    ];
}
exports.getMuteOptions = getMuteOptions;
