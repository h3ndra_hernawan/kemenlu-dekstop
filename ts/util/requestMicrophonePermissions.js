"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.requestMicrophonePermissions = void 0;
async function requestMicrophonePermissions() {
    const microphonePermission = await window.getMediaPermissions();
    if (!microphonePermission) {
        await window.showCallingPermissionsPopup(false);
        // Check the setting again (from the source of truth).
        return window.getMediaPermissions();
    }
    return true;
}
exports.requestMicrophonePermissions = requestMicrophonePermissions;
