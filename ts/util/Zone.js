"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.Zone = void 0;
class Zone {
    constructor(name, options = {}) {
        this.name = name;
        this.options = options;
    }
    supportsPendingSessions() {
        return this.options.pendingSessions === true;
    }
    supportsPendingUnprocessed() {
        return this.options.pendingUnprocessed === true;
    }
}
exports.Zone = Zone;
