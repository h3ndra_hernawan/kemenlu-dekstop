"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.processAttachment = exports.preProcessAttachment = exports.getPendingAttachment = void 0;
const path_1 = __importDefault(require("path"));
const log = __importStar(require("../logging/log"));
const AttachmentToastType_1 = require("../types/AttachmentToastType");
const fileToBytes_1 = require("./fileToBytes");
const handleImageAttachment_1 = require("./handleImageAttachment");
const handleVideoAttachment_1 = require("./handleVideoAttachment");
const isAttachmentSizeOkay_1 = require("./isAttachmentSizeOkay");
const isFileDangerous_1 = require("./isFileDangerous");
const MIME_1 = require("../types/MIME");
const GoogleChrome_1 = require("./GoogleChrome");
function getPendingAttachment(file) {
    if (!file) {
        return;
    }
    const fileType = (0, MIME_1.stringToMIMEType)(file.type);
    const { name: fileName } = path_1.default.parse(file.name);
    return {
        contentType: fileType,
        fileName,
        size: file.size,
        path: file.name,
        pending: true,
    };
}
exports.getPendingAttachment = getPendingAttachment;
function preProcessAttachment(file, draftAttachments) {
    if (!file) {
        return;
    }
    const MB = 1000 * 1024;
    if (file.size > 100 * MB) {
        return AttachmentToastType_1.AttachmentToastType.ToastFileSize;
    }
    if ((0, isFileDangerous_1.isFileDangerous)(file.name)) {
        return AttachmentToastType_1.AttachmentToastType.ToastDangerousFileType;
    }
    if (draftAttachments.length >= 32) {
        return AttachmentToastType_1.AttachmentToastType.ToastMaxAttachments;
    }
    const haveNonImage = draftAttachments.some((attachment) => !(0, MIME_1.isImage)(attachment.contentType));
    // You can't add another attachment if you already have a non-image staged
    if (haveNonImage) {
        return AttachmentToastType_1.AttachmentToastType.ToastOneNonImageAtATime;
    }
    const fileType = (0, MIME_1.stringToMIMEType)(file.type);
    // You can't add a non-image attachment if you already have attachments staged
    if (!(0, MIME_1.isImage)(fileType) && draftAttachments.length > 0) {
        return AttachmentToastType_1.AttachmentToastType.ToastCannotMixImageAndNonImageAttachments;
    }
    return undefined;
}
exports.preProcessAttachment = preProcessAttachment;
async function processAttachment(file) {
    const fileType = (0, MIME_1.stringToMIMEType)(file.type);
    let attachment;
    try {
        if ((0, GoogleChrome_1.isImageTypeSupported)(fileType) || (0, MIME_1.isHeic)(fileType)) {
            attachment = await (0, handleImageAttachment_1.handleImageAttachment)(file);
        }
        else if ((0, GoogleChrome_1.isVideoTypeSupported)(fileType)) {
            attachment = await (0, handleVideoAttachment_1.handleVideoAttachment)(file);
        }
        else {
            const data = await (0, fileToBytes_1.fileToBytes)(file);
            attachment = {
                contentType: fileType,
                data,
                fileName: file.name,
                path: file.name,
                pending: false,
                size: data.byteLength,
            };
        }
    }
    catch (e) {
        log.error(`Was unable to generate thumbnail for fileType ${fileType}`, e && e.stack ? e.stack : e);
        const data = await (0, fileToBytes_1.fileToBytes)(file);
        attachment = {
            contentType: fileType,
            data,
            fileName: file.name,
            path: file.name,
            pending: false,
            size: data.byteLength,
        };
    }
    try {
        if ((0, isAttachmentSizeOkay_1.isAttachmentSizeOkay)(attachment)) {
            return attachment;
        }
    }
    catch (error) {
        log.error('Error ensuring that image is properly sized:', error && error.stack ? error.stack : error);
        throw error;
    }
}
exports.processAttachment = processAttachment;
