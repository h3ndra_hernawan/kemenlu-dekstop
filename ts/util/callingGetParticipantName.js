"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getParticipantName = void 0;
function getParticipantName(participant) {
    return participant.firstName || participant.title;
}
exports.getParticipantName = getParticipantName;
