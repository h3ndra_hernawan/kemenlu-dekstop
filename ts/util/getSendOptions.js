"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSendOptions = void 0;
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const getConversationMembers_1 = require("./getConversationMembers");
const whatTypeOfConversation_1 = require("./whatTypeOfConversation");
const isInSystemContacts_1 = require("./isInSystemContacts");
const missingCaseError_1 = require("./missingCaseError");
const senderCertificate_1 = require("../services/senderCertificate");
const phoneNumberSharingMode_1 = require("./phoneNumberSharingMode");
const SEALED_SENDER = {
    UNKNOWN: 0,
    ENABLED: 1,
    DISABLED: 2,
    UNRESTRICTED: 3,
};
async function getSendOptions(conversationAttrs, options = {}) {
    const { syncMessage } = options;
    if (!(0, whatTypeOfConversation_1.isDirectConversation)(conversationAttrs)) {
        const contactCollection = (0, getConversationMembers_1.getConversationMembers)(conversationAttrs);
        const sendMetadata = {};
        await Promise.all(contactCollection.map(async (contactAttrs) => {
            const conversation = window.ConversationController.get(contactAttrs.id);
            if (!conversation) {
                return;
            }
            const { sendMetadata: conversationSendMetadata } = await getSendOptions(conversation.attributes, options);
            Object.assign(sendMetadata, conversationSendMetadata || {});
        }));
        return { sendMetadata };
    }
    const { accessKey, sealedSender } = conversationAttrs;
    // We never send sync messages or to our own account as sealed sender
    if (syncMessage || (0, whatTypeOfConversation_1.isMe)(conversationAttrs)) {
        return {
            sendMetadata: undefined,
        };
    }
    const { e164, uuid } = conversationAttrs;
    const senderCertificate = await getSenderCertificateForDirectConversation(conversationAttrs);
    // If we've never fetched user's profile, we default to what we have
    if (sealedSender === SEALED_SENDER.UNKNOWN) {
        const identifierData = {
            accessKey: accessKey || Bytes.toBase64((0, Crypto_1.getRandomBytes)(16)),
            senderCertificate,
        };
        return {
            sendMetadata: Object.assign(Object.assign({}, (e164 ? { [e164]: identifierData } : {})), (uuid ? { [uuid]: identifierData } : {})),
        };
    }
    if (sealedSender === SEALED_SENDER.DISABLED) {
        return {
            sendMetadata: undefined,
        };
    }
    const identifierData = {
        accessKey: accessKey && sealedSender === SEALED_SENDER.ENABLED
            ? accessKey
            : Bytes.toBase64((0, Crypto_1.getRandomBytes)(16)),
        senderCertificate,
    };
    return {
        sendMetadata: Object.assign(Object.assign({}, (e164 ? { [e164]: identifierData } : {})), (uuid ? { [uuid]: identifierData } : {})),
    };
}
exports.getSendOptions = getSendOptions;
function getSenderCertificateForDirectConversation(conversationAttrs) {
    if (!(0, whatTypeOfConversation_1.isDirectConversation)(conversationAttrs)) {
        throw new Error('getSenderCertificateForDirectConversation should only be called for direct conversations');
    }
    const phoneNumberSharingMode = (0, phoneNumberSharingMode_1.parsePhoneNumberSharingMode)(window.storage.get('phoneNumberSharingMode'));
    let certificateMode;
    switch (phoneNumberSharingMode) {
        case phoneNumberSharingMode_1.PhoneNumberSharingMode.Everybody:
            certificateMode = 0 /* WithE164 */;
            break;
        case phoneNumberSharingMode_1.PhoneNumberSharingMode.ContactsOnly:
            certificateMode = (0, isInSystemContacts_1.isInSystemContacts)(conversationAttrs)
                ? 0 /* WithE164 */
                : 1 /* WithoutE164 */;
            break;
        case phoneNumberSharingMode_1.PhoneNumberSharingMode.Nobody:
            certificateMode = 1 /* WithoutE164 */;
            break;
        default:
            throw (0, missingCaseError_1.missingCaseError)(phoneNumberSharingMode);
    }
    return senderCertificate_1.senderCertificateService.get(certificateMode);
}
