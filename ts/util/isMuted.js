"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isMuted = void 0;
function isMuted(muteExpiresAt) {
    return Boolean(muteExpiresAt && Date.now() < muteExpiresAt);
}
exports.isMuted = isMuted;
