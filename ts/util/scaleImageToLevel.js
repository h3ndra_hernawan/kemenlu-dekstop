"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.scaleImageToLevel = void 0;
const blueimp_load_image_1 = __importDefault(require("blueimp-load-image"));
const MIME_1 = require("../types/MIME");
const canvasToBlob_1 = require("./canvasToBlob");
const RemoteConfig_1 = require("../RemoteConfig");
var MediaQualityLevels;
(function (MediaQualityLevels) {
    MediaQualityLevels[MediaQualityLevels["One"] = 1] = "One";
    MediaQualityLevels[MediaQualityLevels["Two"] = 2] = "Two";
    MediaQualityLevels[MediaQualityLevels["Three"] = 3] = "Three";
})(MediaQualityLevels || (MediaQualityLevels = {}));
const DEFAULT_LEVEL = MediaQualityLevels.One;
const MiB = 1024 * 1024;
const DEFAULT_LEVEL_DATA = {
    maxDimensions: 1600,
    quality: 0.7,
    size: MiB,
    thresholdSize: 0.2 * MiB,
};
const MEDIA_QUALITY_LEVEL_DATA = new Map([
    [MediaQualityLevels.One, DEFAULT_LEVEL_DATA],
    [
        MediaQualityLevels.Two,
        {
            maxDimensions: 2048,
            quality: 0.75,
            size: MiB * 1.5,
            thresholdSize: 0.3 * MiB,
        },
    ],
    [
        MediaQualityLevels.Three,
        {
            maxDimensions: 4096,
            quality: 0.75,
            size: MiB * 3,
            thresholdSize: 0.4 * MiB,
        },
    ],
]);
const SCALABLE_DIMENSIONS = [3072, 2048, 1600, 1024, 768];
const MIN_DIMENSIONS = 512;
function parseCountryValues(values) {
    const map = new Map();
    values.split(',').forEach(value => {
        const [countryCode, level] = value.split(':');
        map.set(countryCode, Number(level) === 2 ? MediaQualityLevels.Two : MediaQualityLevels.One);
    });
    return map;
}
function getMediaQualityLevel() {
    const values = (0, RemoteConfig_1.getValue)('desktop.mediaQuality.levels');
    if (!values) {
        return DEFAULT_LEVEL;
    }
    const countryValues = parseCountryValues(values);
    const e164 = window.textsecure.storage.user.getNumber();
    if (!e164) {
        return DEFAULT_LEVEL;
    }
    const parsedPhoneNumber = window.libphonenumber.util.parseNumber(e164);
    if (!parsedPhoneNumber.isValidNumber) {
        return DEFAULT_LEVEL;
    }
    const level = countryValues.get(parsedPhoneNumber.countryCode);
    if (level) {
        return level;
    }
    return countryValues.get('*') || DEFAULT_LEVEL;
}
async function getCanvasBlobAsJPEG(image, dimensions, quality) {
    const canvas = blueimp_load_image_1.default.scale(image, {
        canvas: true,
        maxHeight: dimensions,
        maxWidth: dimensions,
    });
    if (!(canvas instanceof HTMLCanvasElement)) {
        throw new Error('image not a canvas');
    }
    return (0, canvasToBlob_1.canvasToBlob)(canvas, MIME_1.IMAGE_JPEG, quality);
}
async function scaleImageToLevel(fileOrBlobOrURL, contentType, sendAsHighQuality) {
    let image;
    try {
        const data = await (0, blueimp_load_image_1.default)(fileOrBlobOrURL, {
            canvas: true,
            orientation: true,
        });
        if (!(data.image instanceof HTMLCanvasElement)) {
            throw new Error('image not a canvas');
        }
        ({ image } = data);
    }
    catch (err) {
        const error = new Error('scaleImageToLevel: Failed to process image');
        error.originalError = err;
        throw error;
    }
    const level = sendAsHighQuality
        ? MediaQualityLevels.Three
        : getMediaQualityLevel();
    const { maxDimensions, quality, size, thresholdSize } = MEDIA_QUALITY_LEVEL_DATA.get(level) || DEFAULT_LEVEL_DATA;
    if (fileOrBlobOrURL.size <= thresholdSize) {
        const blob = await (0, canvasToBlob_1.canvasToBlob)(image, contentType);
        return {
            blob,
            contentType,
        };
    }
    for (let i = 0; i < SCALABLE_DIMENSIONS.length; i += 1) {
        const scalableDimensions = SCALABLE_DIMENSIONS[i];
        if (maxDimensions < scalableDimensions) {
            continue;
        }
        // We need these operations to be in serial
        // eslint-disable-next-line no-await-in-loop
        const blob = await getCanvasBlobAsJPEG(image, scalableDimensions, quality);
        if (blob.size <= size) {
            return {
                blob,
                contentType: MIME_1.IMAGE_JPEG,
            };
        }
    }
    const blob = await getCanvasBlobAsJPEG(image, MIN_DIMENSIONS, quality);
    return {
        blob,
        contentType: MIME_1.IMAGE_JPEG,
    };
}
exports.scaleImageToLevel = scaleImageToLevel;
