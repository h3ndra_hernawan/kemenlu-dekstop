"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.imagePathToBytes = void 0;
const canvasToBytes_1 = require("./canvasToBytes");
async function imagePathToBytes(src) {
    const image = new Image();
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    if (!context) {
        throw new Error('imagePathToArrayBuffer: could not get canvas rendering context');
    }
    image.src = src;
    await image.decode();
    canvas.width = image.width;
    canvas.height = image.height;
    context.drawImage(image, 0, 0);
    return (0, canvasToBytes_1.canvasToBytes)(canvas);
}
exports.imagePathToBytes = imagePathToBytes;
