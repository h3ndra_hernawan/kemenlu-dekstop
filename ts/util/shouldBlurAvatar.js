"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.shouldBlurAvatar = void 0;
const shouldBlurAvatar = ({ acceptedMessageRequest, avatarPath, isMe, sharedGroupNames, unblurredAvatarPath, }) => Boolean(!isMe &&
    !acceptedMessageRequest &&
    !sharedGroupNames.length &&
    avatarPath &&
    avatarPath !== unblurredAvatarPath);
exports.shouldBlurAvatar = shouldBlurAvatar;
