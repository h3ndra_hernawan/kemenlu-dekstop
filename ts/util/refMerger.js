"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createRefMerger = exports.refMerger = void 0;
const memoizee_1 = __importDefault(require("memoizee"));
/**
 * Merges multiple refs.
 *
 * Returns a new function each time, which may cause unnecessary re-renders. Try
 * `createRefMerger` if you want to cache the function.
 */
function refMerger(...refs) {
    return (el) => {
        refs.forEach(ref => {
            // This is a simplified version of [what React does][0] to set a ref.
            // [0]: https://github.com/facebook/react/blob/29b7b775f2ecf878eaf605be959d959030598b07/packages/react-reconciler/src/ReactFiberCommitWork.js#L661-L677
            if (typeof ref === 'function') {
                ref(el);
            }
            else if (ref) {
                // I believe the types for `ref` are wrong in this case, as `ref.current` should
                //   not be `readonly`. That's why we do this cast. See [the React source][1].
                // [1]: https://github.com/facebook/react/blob/29b7b775f2ecf878eaf605be959d959030598b07/packages/shared/ReactTypes.js#L78-L80
                // eslint-disable-next-line no-param-reassign
                ref.current = el;
            }
        });
    };
}
exports.refMerger = refMerger;
function createRefMerger() {
    return (0, memoizee_1.default)(refMerger, { length: false, max: 1 });
}
exports.createRefMerger = createRefMerger;
