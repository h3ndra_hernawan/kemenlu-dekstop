"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.copyGroupLink = void 0;
const showToast_1 = require("./showToast");
const ToastGroupLinkCopied_1 = require("../components/ToastGroupLinkCopied");
async function copyGroupLink(groupLink) {
    await window.navigator.clipboard.writeText(groupLink);
    (0, showToast_1.showToast)(ToastGroupLinkCopied_1.ToastGroupLinkCopied);
}
exports.copyGroupLink = copyGroupLink;
