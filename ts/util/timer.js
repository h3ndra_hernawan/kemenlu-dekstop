"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTimerBucket = exports.getIncrement = void 0;
const lodash_1 = require("lodash");
function getIncrement(length) {
    if (length < 0) {
        return 1000;
    }
    return Math.ceil(length / 12);
}
exports.getIncrement = getIncrement;
function getTimerBucket(expiration, length) {
    const delta = expiration - Date.now();
    if (delta < 0) {
        return '00';
    }
    if (delta > length) {
        return '60';
    }
    const bucket = Math.round((delta / length) * 12);
    return (0, lodash_1.padStart)(String(bucket * 5), 2, '0');
}
exports.getTimerBucket = getTimerBucket;
