"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.writeWindowsZoneIdentifier = void 0;
const fs = __importStar(require("fs"));
const OS_1 = require("../OS");
const ZONE_IDENTIFIER_CONTENTS = Buffer.from('[ZoneTransfer]\r\nZoneId=3');
/**
 * Internet Explorer introduced the concept of "Security Zones". For our purposes, we
 * just need to set the security zone to the "Internet" zone, which Windows will use to
 * offer some protections. This is customizable by the user (or, more likely, by IT).
 *
 * To do this, we write the "Zone.Identifier" for the NTFS alternative data stream.
 *
 * This can fail in a bunch of situations:
 *
 * - The OS is not Windows.
 * - The filesystem is not NTFS.
 * - Writing the metadata file fails for some reason (permissions, for example).
 * - The metadata file already exists. (We could choose to overwrite it.)
 * - The original file is deleted between the time that we check for its existence and
 *   when we write the metadata. This is a rare race condition, but is possible.
 *
 * Consumers of this module should probably tolerate failures.
 */
async function writeWindowsZoneIdentifier(filePath) {
    if (!(0, OS_1.isWindows)()) {
        throw new Error('writeWindowsZoneIdentifier should only run on Windows');
    }
    if (!fs.existsSync(filePath)) {
        throw new Error('writeWindowsZoneIdentifier could not find the original file');
    }
    await fs.promises.writeFile(`${filePath}:Zone.Identifier`, ZONE_IDENTIFIER_CONTENTS, {
        flag: 'wx',
    });
}
exports.writeWindowsZoneIdentifier = writeWindowsZoneIdentifier;
