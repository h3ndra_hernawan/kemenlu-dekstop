"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.wrapPromise = exports.concat = void 0;
function concat(iterables) {
    return new ConcatAsyncIterable(iterables);
}
exports.concat = concat;
class ConcatAsyncIterable {
    constructor(iterables) {
        this.iterables = iterables;
    }
    [Symbol.asyncIterator]() {
        return __asyncGenerator(this, arguments, function* _a() {
            var e_1, _b;
            for (const iterable of this.iterables) {
                try {
                    for (var iterable_1 = (e_1 = void 0, __asyncValues(iterable)), iterable_1_1; iterable_1_1 = yield __await(iterable_1.next()), !iterable_1_1.done;) {
                        const value = iterable_1_1.value;
                        yield yield __await(value);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (iterable_1_1 && !iterable_1_1.done && (_b = iterable_1.return)) yield __await(_b.call(iterable_1));
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
        });
    }
}
function wrapPromise(promise) {
    return new WrapPromiseAsyncIterable(promise);
}
exports.wrapPromise = wrapPromise;
class WrapPromiseAsyncIterable {
    constructor(promise) {
        this.promise = promise;
    }
    [Symbol.asyncIterator]() {
        return __asyncGenerator(this, arguments, function* _a() {
            var e_2, _b;
            try {
                for (var _c = __asyncValues(yield __await(this.promise)), _d; _d = yield __await(_c.next()), !_d.done;) {
                    const value = _d.value;
                    yield yield __await(value);
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_d && !_d.done && (_b = _c.return)) yield __await(_b.call(_c));
                }
                finally { if (e_2) throw e_2.error; }
            }
        });
    }
}
