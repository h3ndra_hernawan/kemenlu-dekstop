"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseIntOrThrow = void 0;
function parseIntOrThrow(value, message) {
    let result;
    switch (typeof value) {
        case 'number':
            result = value;
            break;
        case 'string':
            result = parseInt(value, 10);
            break;
        default:
            result = NaN;
            break;
    }
    if (!Number.isInteger(result)) {
        throw new Error(message);
    }
    return result;
}
exports.parseIntOrThrow = parseIntOrThrow;
