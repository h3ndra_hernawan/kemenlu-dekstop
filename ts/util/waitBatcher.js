"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createWaitBatcher = void 0;
const p_queue_1 = __importDefault(require("p-queue"));
const sleep_1 = require("./sleep");
const log = __importStar(require("../logging/log"));
window.waitBatchers = [];
window.flushAllWaitBatchers = async () => {
    log.info('waitBatcher#flushAllWaitBatchers');
    await Promise.all(window.waitBatchers.map(item => item.flushAndWait()));
};
window.waitForAllWaitBatchers = async () => {
    log.info('waitBatcher#waitForAllWaitBatchers');
    await Promise.all(window.waitBatchers.map(item => item.onIdle()));
};
function createWaitBatcher(options) {
    let waitBatcher;
    let timeout;
    let items = [];
    const queue = new p_queue_1.default({ concurrency: 1, timeout: 1000 * 60 * 2 });
    async function _kickBatchOff() {
        const itemsRef = items;
        items = [];
        await queue.add(async () => {
            try {
                await options.processBatch(itemsRef.map(item => item.item));
                itemsRef.forEach(item => {
                    if (item.resolve) {
                        item.resolve();
                    }
                });
            }
            catch (error) {
                itemsRef.forEach(item => {
                    if (item.reject) {
                        item.reject(error);
                    }
                });
            }
        });
    }
    function _makeExplodedPromise() {
        let resolve;
        let reject;
        const promise = new Promise((resolveParam, rejectParam) => {
            resolve = resolveParam;
            reject = rejectParam;
        });
        return { promise, resolve, reject };
    }
    async function add(item) {
        const { promise, resolve, reject } = _makeExplodedPromise();
        items.push({
            resolve,
            reject,
            item,
        });
        if (items.length === 1) {
            // Set timeout once when we just pushed the first item so that the wait
            // time is bounded by `options.wait` and not extended by further pushes.
            timeout = setTimeout(() => {
                timeout = null;
                _kickBatchOff();
            }, options.wait);
        }
        if (items.length >= options.maxSize) {
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
            _kickBatchOff();
        }
        await promise;
    }
    function anyPending() {
        return queue.size > 0 || queue.pending > 0 || items.length > 0;
    }
    async function onIdle() {
        while (anyPending()) {
            if (queue.size > 0 || queue.pending > 0) {
                // eslint-disable-next-line no-await-in-loop
                await queue.onIdle();
            }
            if (items.length > 0) {
                // eslint-disable-next-line no-await-in-loop
                await (0, sleep_1.sleep)(options.wait * 2);
            }
        }
    }
    function unregister() {
        window.waitBatchers = window.waitBatchers.filter(item => item !== waitBatcher);
    }
    async function flushAndWait() {
        log.info(`Flushing start ${options.name} for waitBatcher ` +
            `items.length=${items.length}`);
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        while (anyPending()) {
            // eslint-disable-next-line no-await-in-loop
            await _kickBatchOff();
            if (queue.size > 0 || queue.pending > 0) {
                // eslint-disable-next-line no-await-in-loop
                await queue.onIdle();
            }
        }
        log.info(`Flushing complete ${options.name} for waitBatcher`);
    }
    waitBatcher = {
        add,
        anyPending,
        onIdle,
        unregister,
        flushAndWait,
    };
    window.waitBatchers.push(waitBatcher);
    return waitBatcher;
}
exports.createWaitBatcher = createWaitBatcher;
