"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isConversationNameKnown = void 0;
const missingCaseError_1 = require("./missingCaseError");
function isConversationNameKnown(conversation) {
    switch (conversation.type) {
        case 'direct':
            return Boolean(conversation.name || conversation.profileName || conversation.e164);
        case 'group':
            return Boolean(conversation.name);
        default:
            throw (0, missingCaseError_1.missingCaseError)(conversation.type);
    }
}
exports.isConversationNameKnown = isConversationNameKnown;
