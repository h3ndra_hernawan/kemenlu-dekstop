"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.arePinnedConversationsEqual = void 0;
const Bytes = __importStar(require("../Bytes"));
function arePinnedConversationsEqual(localValue, remoteValue) {
    if (localValue.length !== remoteValue.length) {
        return false;
    }
    return localValue.every((localPinnedConversation, index) => {
        const remotePinnedConversation = remoteValue[index];
        const { contact, groupMasterKey, legacyGroupId } = localPinnedConversation;
        if (contact) {
            return (remotePinnedConversation.contact &&
                contact.uuid === remotePinnedConversation.contact.uuid);
        }
        if (groupMasterKey && groupMasterKey.length) {
            return Bytes.areEqual(groupMasterKey, remotePinnedConversation.groupMasterKey);
        }
        if (legacyGroupId && legacyGroupId.length) {
            return Bytes.areEqual(legacyGroupId, remotePinnedConversation.legacyGroupId);
        }
        return false;
    });
}
exports.arePinnedConversationsEqual = arePinnedConversationsEqual;
