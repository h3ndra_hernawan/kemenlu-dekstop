"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFittedFontSize = exports.getFontSizes = void 0;
const lib_1 = require("../components/emoji/lib");
const isEmojiOnlyText_1 = require("./isEmojiOnlyText");
function getFontSizes(bubbleSize) {
    return {
        diameter: Math.ceil(bubbleSize * 0.75),
        singleEmoji: Math.ceil(bubbleSize * 0.6),
        smol: Math.ceil(bubbleSize * 0.05),
        text: Math.ceil(bubbleSize * 0.45),
    };
}
exports.getFontSizes = getFontSizes;
function getFittedFontSize(bubbleSize, text, measure) {
    const sizes = getFontSizes(bubbleSize);
    let candidateFontSize = sizes.text;
    if ((0, isEmojiOnlyText_1.isEmojiOnlyText)(text) && (0, lib_1.getEmojiCount)(text) === 1) {
        candidateFontSize = sizes.singleEmoji;
    }
    for (candidateFontSize; candidateFontSize >= sizes.smol; candidateFontSize -= 1) {
        const { height, width } = measure(candidateFontSize);
        if (width < sizes.diameter && height < sizes.diameter) {
            return candidateFontSize;
        }
    }
    return candidateFontSize;
}
exports.getFittedFontSize = getFittedFontSize;
