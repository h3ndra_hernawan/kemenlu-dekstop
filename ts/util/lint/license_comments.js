"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.readFirstLines = exports.forEachRelevantFile = exports.getExtension = void 0;
// This file doesn't check the format of license files, just the end year. See
//   `license_comments_test.ts` for those checks, which are meant to be run more often.
const assert_1 = __importDefault(require("assert"));
const readline = __importStar(require("readline"));
const path = __importStar(require("path"));
const fs = __importStar(require("fs"));
const util_1 = require("util");
const childProcess = __importStar(require("child_process"));
const p_map_1 = __importDefault(require("p-map"));
const exec = (0, util_1.promisify)(childProcess.exec);
const rootPath = path.join(__dirname, '..', '..', '..');
const EXTENSIONS_TO_CHECK = new Set([
    '.eslintignore',
    '.gitattributes',
    '.gitignore',
    '.nvmrc',
    '.prettierignore',
    '.sh',
    '.snyk',
    '.yarnclean',
    '.yml',
    '.js',
    '.scss',
    '.ts',
    '.tsx',
    '.html',
    '.md',
    '.plist',
]);
const FILES_TO_IGNORE = new Set([
    '.github/ISSUE_TEMPLATE/bug_report.md',
    '.github/PULL_REQUEST_TEMPLATE.md',
    'components/indexeddb-backbonejs-adapter/backbone-indexeddb.js',
    'components/mp3lameencoder/lib/Mp3LameEncoder.js',
    'components/qrcode/qrcode.js',
    'components/recorderjs/recorder.js',
    'components/recorderjs/recorderWorker.js',
    'components/webaudiorecorder/lib/WebAudioRecorder.js',
    'components/webaudiorecorder/lib/WebAudioRecorderMp3.js',
    'js/Mp3LameEncoder.min.js',
    'js/WebAudioRecorderMp3.js',
].map(
// This makes sure the files are correct on Windows.
path.normalize));
// This is not technically the real extension.
function getExtension(file) {
    if (file.startsWith('.')) {
        return getExtension(`x.${file}`);
    }
    return path.extname(file);
}
exports.getExtension = getExtension;
async function forEachRelevantFile(fn) {
    const gitFiles = (await exec('git ls-files', { cwd: rootPath, env: {} })).stdout
        .split(/\n/g)
        .map(line => line.trim())
        .filter(Boolean)
        .map(file => path.join(rootPath, file));
    await (0, p_map_1.default)(gitFiles, async (file) => {
        const repoPath = path.relative(rootPath, file);
        if (FILES_TO_IGNORE.has(repoPath)) {
            return;
        }
        const extension = getExtension(file);
        if (!EXTENSIONS_TO_CHECK.has(extension)) {
            return;
        }
        await fn(file);
    }, 
    // Without this, we may run into "too many open files" errors.
    { concurrency: 100 });
}
exports.forEachRelevantFile = forEachRelevantFile;
function readFirstLines(file, count) {
    return new Promise(resolve => {
        const lines = [];
        const lineReader = readline.createInterface({
            input: fs.createReadStream(file),
        });
        lineReader.on('line', line => {
            lines.push(line);
            if (lines.length >= count) {
                lineReader.close();
            }
        });
        lineReader.on('close', () => {
            resolve(lines);
        });
    });
}
exports.readFirstLines = readFirstLines;
async function getLatestCommitYearForFile(file) {
    const dateString = (await new Promise((resolve, reject) => {
        var _a;
        let result = '';
        // We use the more verbose `spawn` to avoid command injection, in case the filename
        //   has strange characters.
        const gitLog = childProcess.spawn('git', ['log', '-1', '--format=%as', file], {
            cwd: rootPath,
            env: { PATH: process.env.PATH },
        });
        (_a = gitLog.stdout) === null || _a === void 0 ? void 0 : _a.on('data', data => {
            result += data.toString('utf8');
        });
        gitLog.on('close', code => {
            if (code === 0) {
                resolve(result);
            }
            else {
                reject(new Error(`git log failed with exit code ${code}`));
            }
        });
    })).trim();
    const result = new Date(dateString).getFullYear();
    (0, assert_1.default)(!Number.isNaN(result), `Could not read commit year for ${file}`);
    return result;
}
async function main() {
    const currentYear = new Date().getFullYear() + 1;
    await forEachRelevantFile(async (file) => {
        const [firstLine] = await readFirstLines(file, 1);
        const { groups = {} } = firstLine.match(/(?:\d{4}-)?(?<endYearString>\d{4})/) || [];
        const { endYearString } = groups;
        const endYear = Number(endYearString);
        (0, assert_1.default)(endYear === currentYear ||
            endYear === (await getLatestCommitYearForFile(file)), `${file} has an invalid end license year`);
    });
}
// Note: this check will fail if we switch to ES modules. See
//  <https://stackoverflow.com/a/60309682>.
if (require.main === module) {
    main().catch(err => {
        // eslint-disable-next-line no-console
        console.error(err);
        process.exit(1);
    });
}
