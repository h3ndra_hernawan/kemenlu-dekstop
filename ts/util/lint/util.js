"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.sortExceptions = exports.loadJSON = exports.ENCODING = void 0;
/* eslint-disable no-console */
const fs_1 = require("fs");
const lodash_1 = require("lodash");
exports.ENCODING = 'utf8';
function loadJSON(target) {
    try {
        const contents = (0, fs_1.readFileSync)(target, exports.ENCODING);
        return JSON.parse(contents);
    }
    catch (error) {
        console.log(`Error loading JSON from ${target}: ${error.stack}`);
        throw error;
    }
}
exports.loadJSON = loadJSON;
function sortExceptions(exceptions) {
    return (0, lodash_1.orderBy)(exceptions, [
        'path',
        'rule',
        'reasonCategory',
        'updated',
        'reasonDetail',
    ]).map(removeLegacyAttributes);
}
exports.sortExceptions = sortExceptions;
// This is here in case any open changesets still touch `lineNumber`. We should remove
//   this after 2021-06-01 to be conservative.
function removeLegacyAttributes(exception) {
    return (0, lodash_1.omit)(exception, ['lineNumber']);
}
