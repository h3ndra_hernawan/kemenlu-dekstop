"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const fs_1 = require("fs");
const util_1 = require("./util");
const exceptionsPath = (0, path_1.join)(__dirname, 'exceptions.json');
const exceptions = (0, util_1.loadJSON)(exceptionsPath);
const sorted = (0, util_1.sortExceptions)(exceptions);
(0, fs_1.writeFileSync)(exceptionsPath, JSON.stringify(sorted, null, '  '));
