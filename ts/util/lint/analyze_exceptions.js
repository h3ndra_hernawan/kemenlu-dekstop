"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const lodash_1 = require("lodash");
const util_1 = require("./util");
const exceptionsPath = (0, path_1.join)(__dirname, 'exceptions.json');
const exceptions = (0, util_1.loadJSON)(exceptionsPath);
const byRule = (0, lodash_1.groupBy)(exceptions, 'rule');
const byRuleThenByCategory = (0, lodash_1.fromPairs)((0, lodash_1.map)(byRule, (list, ruleName) => {
    const byCategory = (0, lodash_1.groupBy)(list, 'reasonCategory');
    return [
        ruleName,
        (0, lodash_1.fromPairs)((0, lodash_1.map)(byCategory, (innerList, categoryName) => {
            return [categoryName, innerList.length];
        })),
    ];
}));
// eslint-disable-next-line no-console
console.log(JSON.stringify(byRuleThenByCategory, null, '  '));
