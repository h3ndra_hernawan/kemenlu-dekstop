"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-console */
const fs = __importStar(require("fs"));
const path_1 = require("path");
const normalize_path_1 = __importDefault(require("normalize-path"));
const p_map_1 = __importDefault(require("p-map"));
const fast_glob_1 = __importDefault(require("fast-glob"));
const types_1 = require("./types");
const util_1 = require("./util");
const ALL_REASONS = types_1.REASONS.join('|');
const rulesPath = (0, path_1.join)(__dirname, 'rules.json');
const exceptionsPath = (0, path_1.join)(__dirname, 'exceptions.json');
const basePath = (0, path_1.join)(__dirname, '../../..');
const searchPattern = (0, normalize_path_1.default)((0, path_1.join)(basePath, '**/*.{js,ts,tsx}'));
const excludedFilesRegexp = RegExp([
    '^release/',
    '^preload.bundle.js(LICENSE.txt|map)?',
    '^storybook-static/',
    // Non-distributed files
    '\\.d\\.ts$',
    '.+\\.stories\\.js',
    '.+\\.stories\\.tsx',
    // High-traffic files in our project
    '^app/.+(ts|js)',
    '^ts/models/messages.js',
    '^ts/models/messages.ts',
    '^ts/models/conversations.js',
    '^ts/models/conversations.ts',
    '^ts/views/conversation_view.js',
    '^ts/views/conversation_view.ts',
    '^ts/background.js',
    '^ts/background.ts',
    '^ts/Crypto.js',
    '^ts/Crypto.ts',
    '^ts/textsecure/MessageReceiver.js',
    '^ts/textsecure/MessageReceiver.ts',
    '^ts/ConversationController.js',
    '^ts/ConversationController.ts',
    '^ts/SignalProtocolStore.ts',
    '^ts/SignalProtocolStore.js',
    '^ts/textsecure/[^./]+.ts',
    '^ts/textsecure/[^./]+.js',
    // Generated files
    '^js/components.js',
    '^js/curve/',
    '^js/util_worker.js',
    '^libtextsecure/test/test.js',
    '^sticker-creator/dist/bundle.js',
    '^test/test.js',
    '^ts/workers/heicConverter.bundle.js',
    '^ts/sql/mainWorker.bundle.js',
    // Copied from dependency
    '^js/Mp3LameEncoder.min.js',
    // Test files
    '^libtextsecure/test/.+',
    '^test/.+',
    '^ts/test[^/]*/.+',
    // Github workflows
    '^.github/.+',
    // Modules we trust
    '^node_modules/core-js-pure/.+',
    '^node_modules/core-js/.+',
    '^node_modules/fbjs/.+',
    '^node_modules/lodash/.+',
    '^node_modules/react/.+',
    '^node_modules/react-contextmenu/.+',
    '^node_modules/react-dom/.+',
    '^node_modules/react-dropzone/.+',
    '^node_modules/react-hot-loader/.+',
    '^node_modules/react-icon-base/.+',
    '^node_modules/react-input-autosize/.+',
    '^node_modules/react-measure/.+',
    '^node_modules/react-popper/.+',
    '^node_modules/react-redux/.+',
    '^node_modules/react-router/.+',
    '^node_modules/react-router-dom/.+',
    '^node_modules/react-select/.+',
    '^node_modules/react-sortable-hoc/.+',
    '^node_modules/react-transition-group/.+',
    '^node_modules/react-virtualized/.+',
    '^node_modules/reactcss/.+',
    '^node_modules/snyk/.+',
    '^node_modules/snyk-resolve-deps/.+',
    '^node_modules/snyk-try-require/.+',
    '^node_modules/@snyk/.+',
    // Submodules we trust
    '^node_modules/react-color/.+/(?:core-js|fbjs|lodash)/.+',
    // Modules used only in test/development scenarios
    '^node_modules/@babel/.+',
    '^node_modules/@svgr/.+',
    '^node_modules/@types/.+',
    '^node_modules/@webassemblyjs/.+',
    '^node_modules/@electron/.+',
    '^node_modules/ajv/.+',
    '^node_modules/ajv-keywords/.+',
    '^node_modules/amdefine/.+',
    '^node_modules/ansi-styles/.+',
    '^node_modules/ansi-colors/.+',
    '^node_modules/anymatch/.+',
    '^node_modules/app-builder-lib/.+',
    '^node_modules/archiver-utils/.+',
    '^node_modules/archiver/.+',
    '^node_modules/asn1\\.js/.+',
    '^node_modules/autoprefixer/.+',
    '^node_modules/babel.+',
    '^node_modules/bluebird/.+',
    '^node_modules/body-parser/.+',
    '^node_modules/bower/.+',
    '^node_modules/braces/.+',
    '^node_modules/buble/.+',
    '^node_modules/builder-util-runtime/.+',
    '^node_modules/builder-util/.+',
    '^node_modules/catharsis/.+',
    '^node_modules/chai/.+',
    '^node_modules/clean-css/.+',
    '^node_modules/cli-table2/.+',
    '^node_modules/cliui/.+',
    '^node_modules/codemirror/.+',
    '^node_modules/coffee-script/.+',
    '^node_modules/compression/.+',
    '^node_modules/cross-env/.+',
    '^node_modules/css-loader/.+',
    '^node_modules/css-modules-loader-core/.+',
    '^node_modules/css-selector-tokenizer/.+',
    '^node_modules/css-tree/.+',
    '^node_modules/csso/.+',
    '^node_modules/default-gateway/.+',
    '^node_modules/degenerator/.+',
    '^node_modules/detect-port-alt/.+',
    '^node_modules/dmg-builder/.+',
    '^node_modules/electron-builder/.+',
    '^node_modules/electron-chromedriver/.+',
    '^node_modules/electron-icon-maker/.+',
    '^node_modules/electron-mocha/',
    '^node_modules/electron-osx-sign/.+',
    '^node_modules/electron-publish/.+',
    '^node_modules/emotion/.+',
    '^node_modules/es-abstract/.+',
    '^node_modules/es5-shim/.+',
    '^node_modules/es6-shim/.+',
    '^node_modules/escodegen/.+',
    '^node_modules/eslint.+',
    '^node_modules/@typescript-eslint.+',
    '^node_modules/esprima/.+',
    '^node_modules/express/.+',
    '^node_modules/file-loader/.+',
    '^node_modules/file-system-cache/.+',
    '^node_modules/finalhandler/.+',
    '^node_modules/fsevents/.+',
    '^node_modules/global-agent/.+',
    '^node_modules/globule/.+',
    '^node_modules/grunt-cli/.+',
    '^node_modules/grunt-contrib-concat/.+',
    '^node_modules/grunt-contrib-watch/.+',
    '^node_modules/grunt-gitinfo/.+',
    '^node_modules/grunt-legacy-log-utils/.+',
    '^node_modules/grunt-legacy-log/.+',
    '^node_modules/grunt-legacy-util/.+',
    '^node_modules/grunt/.+',
    '^node_modules/handle-thing/.+',
    '^node_modules/handlebars/.+',
    '^node_modules/har-validator/.+',
    '^node_modules/highlight\\.js/.+',
    '^node_modules/hpack\\.js/.+',
    '^node_modules/http-proxy-middlewar/.+',
    '^node_modules/icss-utils/.+',
    '^node_modules/intl-tel-input/examples/.+',
    '^node_modules/istanbul.+',
    '^node_modules/jimp/.+',
    '^node_modules/jquery/.+',
    '^node_modules/jss-global/.+',
    '^node_modules/jss/.+',
    '^node_modules/liftup/.+',
    '^node_modules/livereload-js/.+',
    '^node_modules/lolex/.+',
    '^node_modules/log-symbols/.+',
    '^node_modules/magic-string/.+',
    '^node_modules/markdown-it/.+',
    '^node_modules/meow/.+',
    '^node_modules/minimatch/.+',
    '^node_modules/mocha/.+',
    '^node_modules/needle/.+',
    '^node_modules/nise/.+',
    '^node_modules/node-gyp/.+',
    '^node_modules/node-sass-import-once/.+',
    '^node_modules/node-sass/.+',
    '^node_modules/npm-run-all/.+',
    '^node_modules/nsp/.+',
    '^node_modules/nyc/.+',
    '^node_modules/optionator/.+',
    '^node_modules/plist/.+',
    '^node_modules/phantomjs-prebuilt/.+',
    '^node_modules/postcss.+',
    '^node_modules/preserve/.+',
    '^node_modules/prettier/.+',
    '^node_modules/prop-types/.+',
    '^node_modules/protobufjs/cli/.+',
    '^node_modules/ramda/.+',
    '^node_modules/react-dev-utils/.+',
    '^node_modules/react-docgen/.+',
    '^node_modules/react-error-overlay/.+',
    '^node_modules/read-config-file/.+',
    '^node_modules/read-pkg/.+',
    '^node_modules/recast/.+',
    '^node_modules/reduce-css-calc/.+',
    '^node_modules/requizzle/.+',
    '^node_modules/resolve/.+',
    '^node_modules/sass-graph/.+',
    '^node_modules/sass-loader/.+',
    '^node_modules/schema-utils/.+',
    '^node_modules/scss-tokenizer/.+',
    '^node_modules/send/.+',
    '^node_modules/serve-index/.+',
    '^node_modules/sinon/.+',
    '^node_modules/snapdragon-util/.+',
    '^node_modules/snapdragon/.+',
    '^node_modules/sockjs-client/.+',
    '^node_modules/spectron/.+',
    '^node_modules/style-loader/.+',
    '^node_modules/svgo/.+',
    '^node_modules/terser/.+',
    '^node_modules/testcheck/.+',
    '^node_modules/text-encoding/.+',
    '^node_modules/tiny-lr/.+',
    '^node_modules/tinycolor2/.+',
    '^node_modules/to-ast/.+',
    '^node_modules/trough/.+',
    '^node_modules/ts-loader/.+',
    '^node_modules/ts-node/.+',
    '^node_modules/tweetnacl/.+',
    '^node_modules/typed-scss-modules/.+',
    '^node_modules/typescript/.+',
    '^node_modules/uglify-es/.+',
    '^node_modules/uglify-js/.+',
    '^node_modules/url-loader/.+',
    '^node_modules/use/.+',
    '^node_modules/vary/.+',
    '^node_modules/vm-browserify/.+',
    '^node_modules/webdriverio/.+',
    '^node_modules/webpack/.+',
    '^node_modules/xml-parse-from-string/.+',
    '^node_modules/xmlbuilder/.+',
    '^node_modules/xmldom/.+',
    '^node_modules/yargs-unparser/',
    '^node_modules/yargs/.+',
    '^node_modules/find-yarn-workspace-root/.+',
    '^node_modules/update-notifier/.+',
    '^node_modules/windows-release/.+',
    // Used by Storybook
    '^node_modules/@emotion/.+',
    '^node_modules/@storybook/.+',
    '^node_modules/cosmiconfig/.+',
    '^node_modules/create-emotion/.+',
    '^node_modules/gzip-size/.+',
    '^node_modules/markdown-to-jsx/.+',
    '^node_modules/mini-css-extract-plugin/.+',
    '^node_modules/polished.+',
    '^node_modules/prismjs/.+',
    '^node_modules/react-draggable/.+',
    '^node_modules/refractor/.+',
    '^node_modules/regexpu-core/.+',
    '^node_modules/shelljs/.+',
    '^node_modules/simplebar/.+',
    '^node_modules/store2/.+',
    '^node_modules/telejson/.+',
    // Used by Webpack
    '^node_modules/css-select/.+',
    '^node_modules/dotenv-webpack/.+',
    '^node_modules/follow-redirects/.+',
    '^node_modules/html-webpack-plugin/.+',
    '^node_modules/selfsigned/.+',
    '^node_modules/portfinder/.+',
    '^node_modules/renderkid/.+',
    '^node_modules/spdy-transport/.+',
    '^node_modules/spdy/.+',
    '^node_modules/uglifyjs-webpack-plugin/.+',
    '^node_modules/v8-compile-cache/.+',
    '^node_modules/watchpack/.+',
    '^node_modules/webpack-cli/.+',
    '^node_modules/webpack-dev-middleware/.+',
    '^node_modules/webpack-dev-server/.+',
    '^node_modules/webpack-hot-middleware/.+',
    '^node_modules/webpack-merge/.+',
    '^node_modules/webpack/.+',
].join('|'));
function setupRules(allRules) {
    allRules.forEach((rule, index) => {
        if (!rule.name) {
            throw new Error(`Rule at index ${index} is missing a name`);
        }
        if (!rule.expression) {
            throw new Error(`Rule '${rule.name}' is missing an expression`);
        }
        // eslint-disable-next-line no-param-reassign
        rule.regex = new RegExp(rule.expression, 'g');
    });
}
async function main() {
    const now = new Date();
    const rules = (0, util_1.loadJSON)(rulesPath);
    setupRules(rules);
    const exceptions = (0, util_1.loadJSON)(exceptionsPath);
    let unusedExceptions = exceptions;
    const results = [];
    let scannedCount = 0;
    await (0, p_map_1.default)(await (0, fast_glob_1.default)(searchPattern, { onlyFiles: true }), async (file) => {
        const relativePath = (0, path_1.relative)(basePath, file).replace(/\\/g, '/');
        const isFileExcluded = excludedFilesRegexp.test(relativePath);
        if (isFileExcluded) {
            return;
        }
        scannedCount += 1;
        const lines = (await fs.promises.readFile(file, util_1.ENCODING)).split(/\r?\n/);
        rules.forEach((rule) => {
            const excludedModules = rule.excludedModules || [];
            if (excludedModules.some(module => relativePath.startsWith(module))) {
                return;
            }
            lines.forEach((line) => {
                if (!rule.regex.test(line)) {
                    return;
                }
                // recreate this rule since it has g flag, and carries local state
                if (rule.expression) {
                    // eslint-disable-next-line no-param-reassign
                    rule.regex = new RegExp(rule.expression, 'g');
                }
                const matchedException = unusedExceptions.find(exception => exception.rule === rule.name &&
                    exception.path === relativePath &&
                    (line.length < 300
                        ? exception.line === line
                        : exception.line === undefined));
                if (matchedException) {
                    unusedExceptions = unusedExceptions.filter(exception => exception !== matchedException);
                }
                else {
                    results.push({
                        rule: rule.name,
                        path: relativePath,
                        line: line.length < 300 ? line : undefined,
                        reasonCategory: ALL_REASONS,
                        updated: now.toJSON(),
                        reasonDetail: '<optional>',
                    });
                }
            });
        });
    }, 
    // Without this, we may run into "too many open files" errors.
    { concurrency: 100 });
    console.log(`${scannedCount} files scanned.`, `${results.length} questionable lines,`, `${unusedExceptions.length} unused exceptions,`, `${exceptions.length} total exceptions.`);
    if (results.length === 0 && unusedExceptions.length === 0) {
        process.exit();
    }
    console.log();
    console.log('Questionable lines:');
    console.log(JSON.stringify((0, util_1.sortExceptions)(results), null, '  '));
    if (unusedExceptions.length) {
        console.log();
        console.log('Unused exceptions!');
        console.log(JSON.stringify((0, util_1.sortExceptions)(unusedExceptions), null, '  '));
    }
    process.exit(1);
}
main().catch(err => {
    console.error(err);
    process.exit(1);
});
