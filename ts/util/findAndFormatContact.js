"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.findAndFormatContact = void 0;
const PhoneNumber_1 = require("../types/PhoneNumber");
const PLACEHOLDER_CONTACT = {
    acceptedMessageRequest: false,
    badges: [],
    id: 'placeholder-contact',
    isMe: false,
    sharedGroupNames: [],
    title: window.i18n('unknownContact'),
    type: 'direct',
};
function findAndFormatContact(identifier) {
    if (!identifier) {
        return PLACEHOLDER_CONTACT;
    }
    const contactModel = window.ConversationController.get(identifier.toLowerCase());
    if (contactModel) {
        return contactModel.format();
    }
    const regionCode = window.storage.get('regionCode');
    if (!(0, PhoneNumber_1.isValidNumber)(identifier, { regionCode })) {
        return PLACEHOLDER_CONTACT;
    }
    const phoneNumber = (0, PhoneNumber_1.format)(identifier, { ourRegionCode: regionCode });
    return {
        acceptedMessageRequest: false,
        badges: [],
        id: 'phone-only',
        isMe: false,
        phoneNumber,
        sharedGroupNames: [],
        title: phoneNumber,
        type: 'direct',
    };
}
exports.findAndFormatContact = findAndFormatContact;
