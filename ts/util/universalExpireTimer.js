"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.set = exports.get = exports.ITEM_NAME = void 0;
exports.ITEM_NAME = 'universalExpireTimer';
function get() {
    return window.storage.get(exports.ITEM_NAME) || 0;
}
exports.get = get;
function set(newValue) {
    return window.storage.put(exports.ITEM_NAME, newValue || 0);
}
exports.set = set;
