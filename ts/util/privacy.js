"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.redactAll = exports.addSensitivePath = exports.redactGroupIds = exports.redactUuids = exports.redactPhoneNumbers = exports._pathToRegExp = exports._redactPath = exports.APP_ROOT_PATH = void 0;
/* eslint-env node */
const is_1 = __importDefault(require("@sindresorhus/is"));
const path_1 = require("path");
const fp_1 = require("lodash/fp");
const lodash_1 = require("lodash");
exports.APP_ROOT_PATH = (0, path_1.join)(__dirname, '..', '..');
const PHONE_NUMBER_PATTERN = /\+\d{7,12}(\d{3})/g;
const UUID_PATTERN = /[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{9}([0-9A-F]{3})/gi;
const GROUP_ID_PATTERN = /(group\()([^)]+)(\))/g;
const GROUP_V2_ID_PATTERN = /(groupv2\()([^=)]+)(=?=?\))/g;
const REDACTION_PLACEHOLDER = '[REDACTED]';
const _redactPath = (filePath) => {
    if (!is_1.default.string(filePath)) {
        throw new TypeError("'filePath' must be a string");
    }
    const filePathPattern = (0, exports._pathToRegExp)(filePath);
    return (text) => {
        if (!is_1.default.string(text)) {
            throw new TypeError("'text' must be a string");
        }
        if (!is_1.default.regExp(filePathPattern)) {
            return text;
        }
        return text.replace(filePathPattern, REDACTION_PLACEHOLDER);
    };
};
exports._redactPath = _redactPath;
const _pathToRegExp = (filePath) => {
    try {
        const pathWithNormalizedSlashes = filePath.replace(/\//g, '\\');
        const pathWithEscapedSlashes = filePath.replace(/\\/g, '\\\\');
        const urlEncodedPath = encodeURI(filePath);
        // Safe `String::replaceAll`:
        // https://github.com/lodash/lodash/issues/1084#issuecomment-86698786
        const patternString = [
            filePath,
            pathWithNormalizedSlashes,
            pathWithEscapedSlashes,
            urlEncodedPath,
        ]
            .map(lodash_1.escapeRegExp)
            .join('|');
        return new RegExp(patternString, 'g');
    }
    catch (error) {
        return undefined;
    }
};
exports._pathToRegExp = _pathToRegExp;
// Public API
const redactPhoneNumbers = (text) => {
    if (!is_1.default.string(text)) {
        throw new TypeError("'text' must be a string");
    }
    return text.replace(PHONE_NUMBER_PATTERN, `+${REDACTION_PLACEHOLDER}$1`);
};
exports.redactPhoneNumbers = redactPhoneNumbers;
const redactUuids = (text) => {
    if (!is_1.default.string(text)) {
        throw new TypeError("'text' must be a string");
    }
    return text.replace(UUID_PATTERN, `${REDACTION_PLACEHOLDER}$1`);
};
exports.redactUuids = redactUuids;
const redactGroupIds = (text) => {
    if (!is_1.default.string(text)) {
        throw new TypeError("'text' must be a string");
    }
    return text
        .replace(GROUP_ID_PATTERN, (_, before, id, after) => `${before}${REDACTION_PLACEHOLDER}${removeNewlines(id).slice(-3)}${after}`)
        .replace(GROUP_V2_ID_PATTERN, (_, before, id, after) => `${before}${REDACTION_PLACEHOLDER}${removeNewlines(id).slice(-3)}${after}`);
};
exports.redactGroupIds = redactGroupIds;
const createRedactSensitivePaths = (paths) => {
    return (0, fp_1.compose)(paths.map(filePath => (0, exports._redactPath)(filePath)));
};
const sensitivePaths = [];
let redactSensitivePaths = (text) => text;
const addSensitivePath = (filePath) => {
    sensitivePaths.push(filePath);
    redactSensitivePaths = createRedactSensitivePaths(sensitivePaths);
};
exports.addSensitivePath = addSensitivePath;
(0, exports.addSensitivePath)(exports.APP_ROOT_PATH);
exports.redactAll = (0, fp_1.compose)((text) => redactSensitivePaths(text), exports.redactGroupIds, exports.redactPhoneNumbers, exports.redactUuids);
const removeNewlines = text => text.replace(/\r?\n|\r/g, '');
