"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isConversationUnregistered = void 0;
const timestamp_1 = require("./timestamp");
const SIX_HOURS = 1000 * 60 * 60 * 6;
function isConversationUnregistered({ discoveredUnregisteredAt, }) {
    return Boolean(discoveredUnregisteredAt &&
        (0, timestamp_1.isMoreRecentThan)(discoveredUnregisteredAt, SIX_HOURS));
}
exports.isConversationUnregistered = isConversationUnregistered;
