"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.scrollToBottom = void 0;
function scrollToBottom(el) {
    // We want to mutate the parameter here.
    // eslint-disable-next-line no-param-reassign
    el.scrollTop = el.scrollHeight;
}
exports.scrollToBottom = scrollToBottom;
