"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseE164FromSignalDotMeHash = exports.parseSignalHttpsLink = exports.parseCaptchaHref = exports.parseSgnlHref = exports.isSignalHttpsLink = exports.isCaptchaHref = exports.isSgnlHref = void 0;
const url_1 = require("./url");
const isValidE164_1 = require("./isValidE164");
const SIGNAL_DOT_ME_HASH_PREFIX = 'p/';
function parseUrl(value, logger) {
    if (value instanceof URL) {
        return value;
    }
    if (typeof value === 'string') {
        return (0, url_1.maybeParseUrl)(value);
    }
    logger.warn('Tried to parse a sgnl:// URL but got an unexpected type');
    return undefined;
}
function isSgnlHref(value, logger) {
    const url = parseUrl(value, logger);
    return Boolean((url === null || url === void 0 ? void 0 : url.protocol) === 'sgnl:');
}
exports.isSgnlHref = isSgnlHref;
function isCaptchaHref(value, logger) {
    const url = parseUrl(value, logger);
    return Boolean((url === null || url === void 0 ? void 0 : url.protocol) === 'signalcaptcha:');
}
exports.isCaptchaHref = isCaptchaHref;
function isSignalHttpsLink(value, logger) {
    const url = parseUrl(value, logger);
    return Boolean(url &&
        !url.username &&
        !url.password &&
        !url.port &&
        url.protocol === 'https:' &&
        (url.host === 'signal.group' ||
            url.host === 'signal.art' ||
            url.host === 'signal.me'));
}
exports.isSignalHttpsLink = isSignalHttpsLink;
function parseSgnlHref(href, logger) {
    const url = parseUrl(href, logger);
    if (!url || !isSgnlHref(url, logger)) {
        return { command: null, args: new Map(), hash: undefined };
    }
    const args = new Map();
    url.searchParams.forEach((value, key) => {
        if (!args.has(key)) {
            args.set(key, value);
        }
    });
    return {
        command: url.host,
        args,
        hash: url.hash ? url.hash.slice(1) : undefined,
    };
}
exports.parseSgnlHref = parseSgnlHref;
function parseCaptchaHref(href, logger) {
    const url = parseUrl(href, logger);
    if (!url || !isCaptchaHref(url, logger)) {
        throw new Error('Not a captcha href');
    }
    return {
        captcha: url.host,
    };
}
exports.parseCaptchaHref = parseCaptchaHref;
function parseSignalHttpsLink(href, logger) {
    const url = parseUrl(href, logger);
    if (!url || !isSignalHttpsLink(url, logger)) {
        return { command: null, args: new Map(), hash: undefined };
    }
    if (url.host === 'signal.art') {
        const hash = url.hash.slice(1);
        const hashParams = new URLSearchParams(hash);
        const args = new Map();
        hashParams.forEach((value, key) => {
            if (!args.has(key)) {
                args.set(key, value);
            }
        });
        if (!args.get('pack_id') || !args.get('pack_key')) {
            return { command: null, args: new Map(), hash: undefined };
        }
        return {
            command: url.pathname.replace(/\//g, ''),
            args,
            hash: url.hash ? url.hash.slice(1) : undefined,
        };
    }
    if (url.host === 'signal.group' || url.host === 'signal.me') {
        return {
            command: url.host,
            args: new Map(),
            hash: url.hash ? url.hash.slice(1) : undefined,
        };
    }
    return { command: null, args: new Map(), hash: undefined };
}
exports.parseSignalHttpsLink = parseSignalHttpsLink;
function parseE164FromSignalDotMeHash(hash) {
    if (!hash.startsWith(SIGNAL_DOT_ME_HASH_PREFIX)) {
        return;
    }
    const maybeE164 = hash.slice(SIGNAL_DOT_ME_HASH_PREFIX.length);
    return (0, isValidE164_1.isValidE164)(maybeE164, true) ? maybeE164 : undefined;
}
exports.parseE164FromSignalDotMeHash = parseE164FromSignalDotMeHash;
