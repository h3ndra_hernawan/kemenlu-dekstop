"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.requestCameraPermissions = void 0;
async function requestCameraPermissions() {
    if (!(await window.getMediaCameraPermissions())) {
        await window.showCallingPermissionsPopup(true);
        // Check the setting again (from the source of truth).
        return window.getMediaCameraPermissions();
    }
    return true;
}
exports.requestCameraPermissions = requestCameraPermissions;
