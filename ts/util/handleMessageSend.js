"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleMessageSend = exports.shouldSaveProto = exports.SEALED_SENDER = void 0;
const lodash_1 = require("lodash");
const Client_1 = __importDefault(require("../sql/Client"));
const log = __importStar(require("../logging/log"));
const { insertSentProto } = Client_1.default;
exports.SEALED_SENDER = {
    UNKNOWN: 0,
    ENABLED: 1,
    DISABLED: 2,
    UNRESTRICTED: 3,
};
function shouldSaveProto(sendType) {
    if (sendType === 'callingMessage') {
        return false;
    }
    if (sendType === 'nullMessage') {
        return false;
    }
    if (sendType === 'resendFromLog') {
        return false;
    }
    if (sendType === 'retryRequest') {
        return false;
    }
    if (sendType === 'typing') {
        return false;
    }
    return true;
}
exports.shouldSaveProto = shouldSaveProto;
async function handleMessageSend(promise, options) {
    try {
        const result = await promise;
        await maybeSaveToSendLog(result, options);
        await handleMessageSendResult(result.failoverIdentifiers, result.unidentifiedDeliveries);
        return result;
    }
    catch (err) {
        if (err) {
            await handleMessageSendResult(err.failoverIdentifiers, err.unidentifiedDeliveries);
        }
        throw err;
    }
}
exports.handleMessageSend = handleMessageSend;
async function handleMessageSendResult(failoverIdentifiers, unidentifiedDeliveries) {
    await Promise.all((failoverIdentifiers || []).map(async (identifier) => {
        const conversation = window.ConversationController.get(identifier);
        if (conversation &&
            conversation.get('sealedSender') !== exports.SEALED_SENDER.DISABLED) {
            log.info(`Setting sealedSender to DISABLED for conversation ${conversation.idForLogging()}`);
            conversation.set({
                sealedSender: exports.SEALED_SENDER.DISABLED,
            });
            window.Signal.Data.updateConversation(conversation.attributes);
        }
    }));
    await Promise.all((unidentifiedDeliveries || []).map(async (identifier) => {
        const conversation = window.ConversationController.get(identifier);
        if (conversation &&
            conversation.get('sealedSender') === exports.SEALED_SENDER.UNKNOWN) {
            if (conversation.get('accessKey')) {
                log.info(`Setting sealedSender to ENABLED for conversation ${conversation.idForLogging()}`);
                conversation.set({
                    sealedSender: exports.SEALED_SENDER.ENABLED,
                });
            }
            else {
                log.info(`Setting sealedSender to UNRESTRICTED for conversation ${conversation.idForLogging()}`);
                conversation.set({
                    sealedSender: exports.SEALED_SENDER.UNRESTRICTED,
                });
            }
            window.Signal.Data.updateConversation(conversation.attributes);
        }
    }));
}
async function maybeSaveToSendLog(result, { messageIds, sendType, }) {
    const { contentHint, contentProto, recipients, timestamp } = result;
    if (!shouldSaveProto(sendType)) {
        return;
    }
    if (!(0, lodash_1.isNumber)(contentHint) || !contentProto || !recipients || !timestamp) {
        log.warn(`handleMessageSend: Missing necessary information to save to log for ${sendType} message ${timestamp}`);
        return;
    }
    const identifiers = Object.keys(recipients);
    if (identifiers.length === 0) {
        log.warn(`handleMessageSend: ${sendType} message ${timestamp} had no recipients`);
        return;
    }
    // If the identifier count is greater than one, we've done the save elsewhere
    if (identifiers.length > 1) {
        return;
    }
    await insertSentProto({
        timestamp,
        proto: Buffer.from(contentProto),
        contentHint,
    }, {
        messageIds,
        recipients,
    });
}
