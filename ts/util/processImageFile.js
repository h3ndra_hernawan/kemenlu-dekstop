"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.processImageFile = void 0;
const blueimp_load_image_1 = __importDefault(require("blueimp-load-image"));
const canvasToBytes_1 = require("./canvasToBytes");
async function processImageFile(file) {
    const { image } = await (0, blueimp_load_image_1.default)(file, {
        canvas: true,
        cover: true,
        crop: true,
        imageSmoothingQuality: 'medium',
        maxHeight: 512,
        maxWidth: 512,
        minHeight: 2,
        minWidth: 2,
        // `imageSmoothingQuality` is not present in `loadImage`'s types, but it is
        //   documented and supported. Updating DefinitelyTyped is the long-term solution
        //   here.
    });
    // NOTE: The types for `loadImage` say this can never be a canvas, but it will be if
    //   `canvas: true`, at least in our case. Again, updating DefinitelyTyped should
    //   address this.
    if (!(image instanceof HTMLCanvasElement)) {
        throw new Error('Loaded image was not a canvas');
    }
    return (0, canvasToBytes_1.canvasToBytes)(image);
}
exports.processImageFile = processImageFile;
