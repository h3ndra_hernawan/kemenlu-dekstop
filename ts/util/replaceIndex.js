"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.replaceIndex = void 0;
function replaceIndex(arr, index, newItem) {
    if (!(index in arr)) {
        throw new RangeError(`replaceIndex: ${index} out of range`);
    }
    const result = [...arr];
    result[index] = newItem;
    return result;
}
exports.replaceIndex = replaceIndex;
