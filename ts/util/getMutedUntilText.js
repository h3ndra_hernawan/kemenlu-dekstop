"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMutedUntilText = void 0;
const moment_1 = __importDefault(require("moment"));
/**
 * Returns something like "Muted until 6:09 PM", localized.
 *
 * Shouldn't be called with `0`.
 */
function getMutedUntilText(muteExpiresAt, i18n) {
    if (Number(muteExpiresAt) >= Number.MAX_SAFE_INTEGER) {
        return i18n('muteExpirationLabelAlways');
    }
    const expires = (0, moment_1.default)(muteExpiresAt);
    const muteExpirationUntil = (0, moment_1.default)().isSame(expires, 'day')
        ? expires.format('LT')
        : expires.format('L, LT');
    return i18n('muteExpirationLabel', [muteExpirationUntil]);
}
exports.getMutedUntilText = getMutedUntilText;
