"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.filterAndSortConversationsByTitle = exports.filterAndSortConversationsByRecent = void 0;
const fuse_js_1 = __importDefault(require("fuse.js"));
const FUSE_OPTIONS = {
    // A small-but-nonzero threshold lets us match parts of E164s better, and makes the
    //   search a little more forgiving.
    threshold: 0.05,
    tokenize: true,
    keys: [
        {
            name: 'searchableTitle',
            weight: 1,
        },
        {
            name: 'title',
            weight: 1,
        },
        {
            name: 'name',
            weight: 1,
        },
        {
            name: 'username',
            weight: 1,
        },
        {
            name: 'e164',
            weight: 0.5,
        },
    ],
};
const collator = new Intl.Collator();
function searchConversations(conversations, searchTerm) {
    return new fuse_js_1.default(conversations, FUSE_OPTIONS).search(searchTerm);
}
function filterAndSortConversationsByRecent(conversations, searchTerm) {
    if (searchTerm.length) {
        return searchConversations(conversations, searchTerm);
    }
    return conversations.concat().sort((a, b) => {
        if (a.activeAt && b.activeAt) {
            return a.activeAt > b.activeAt ? -1 : 1;
        }
        return a.activeAt && !b.activeAt ? -1 : 1;
    });
}
exports.filterAndSortConversationsByRecent = filterAndSortConversationsByRecent;
function filterAndSortConversationsByTitle(conversations, searchTerm) {
    if (searchTerm.length) {
        return searchConversations(conversations, searchTerm);
    }
    return conversations.concat().sort((a, b) => {
        const aHasName = hasName(a);
        const bHasName = hasName(b);
        if (aHasName === bHasName) {
            return collator.compare(a.title, b.title);
        }
        return aHasName && !bHasName ? -1 : 1;
    });
}
exports.filterAndSortConversationsByTitle = filterAndSortConversationsByTitle;
function hasName(contact) {
    return Boolean(contact.name || contact.profileName);
}
