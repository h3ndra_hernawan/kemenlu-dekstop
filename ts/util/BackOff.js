"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.BackOff = exports.FIBONACCI_TIMEOUTS = void 0;
const SECOND = 1000;
exports.FIBONACCI_TIMEOUTS = [
    1 * SECOND,
    2 * SECOND,
    3 * SECOND,
    5 * SECOND,
    8 * SECOND,
    13 * SECOND,
    21 * SECOND,
    34 * SECOND,
    55 * SECOND,
];
const DEFAULT_RANDOM = () => Math.random();
class BackOff {
    constructor(timeouts, options = {}) {
        this.timeouts = timeouts;
        this.options = options;
        this.count = 0;
    }
    get() {
        let result = this.timeouts[this.count];
        const { jitter = 0, random = DEFAULT_RANDOM } = this.options;
        // Do not apply jitter larger than the timeout value. It is supposed to be
        // activated for longer timeouts.
        if (jitter < result) {
            result += random() * jitter;
        }
        return result;
    }
    getAndIncrement() {
        const result = this.get();
        if (!this.isFull()) {
            this.count += 1;
        }
        return result;
    }
    reset() {
        this.count = 0;
    }
    isFull() {
        return this.count === this.timeouts.length - 1;
    }
    getIndex() {
        return this.count;
    }
}
exports.BackOff = BackOff;
