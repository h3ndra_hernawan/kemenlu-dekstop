"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getGroupMemberships = void 0;
const isConversationUnregistered_1 = require("./isConversationUnregistered");
const getGroupMemberships = ({ memberships = [], pendingApprovalMemberships = [], pendingMemberships = [], }, getConversationByUuid) => ({
    memberships: memberships.reduce((result, membership) => {
        const member = getConversationByUuid(membership.uuid);
        if (!member) {
            return result;
        }
        return [...result, { isAdmin: membership.isAdmin, member }];
    }, []),
    pendingApprovalMemberships: pendingApprovalMemberships.reduce((result, membership) => {
        const member = getConversationByUuid(membership.uuid);
        if (!member || (0, isConversationUnregistered_1.isConversationUnregistered)(member)) {
            return result;
        }
        return [...result, { member }];
    }, []),
    pendingMemberships: pendingMemberships.reduce((result, membership) => {
        const member = getConversationByUuid(membership.uuid);
        if (!member || (0, isConversationUnregistered_1.isConversationUnregistered)(member)) {
            return result;
        }
        return [
            ...result,
            {
                member,
                metadata: { addedByUserId: membership.addedByUserId },
            },
        ];
    }, []),
});
exports.getGroupMemberships = getGroupMemberships;
