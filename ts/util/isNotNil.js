"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isNotNil = void 0;
function isNotNil(value) {
    if (value === null || value === undefined) {
        return false;
    }
    return true;
}
exports.isNotNil = isNotNil;
