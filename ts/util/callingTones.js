"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.callingTones = void 0;
const p_queue_1 = __importDefault(require("p-queue"));
const Sound_1 = require("./Sound");
const ringtoneEventQueue = new p_queue_1.default({
    concurrency: 1,
    timeout: 1000 * 60 * 2,
});
class CallingTones {
    async playEndCall() {
        const canPlayTone = window.Events.getCallRingtoneNotification();
        if (!canPlayTone) {
            return;
        }
        const tone = new Sound_1.Sound({
            src: 'sounds/navigation-cancel.ogg',
        });
        await tone.play();
    }
    async playRingtone() {
        await ringtoneEventQueue.add(async () => {
            if (this.ringtone) {
                this.ringtone.stop();
                this.ringtone = undefined;
            }
            const canPlayTone = window.Events.getCallRingtoneNotification();
            if (!canPlayTone) {
                return;
            }
            this.ringtone = new Sound_1.Sound({
                loop: true,
                src: 'sounds/ringtone_minimal.ogg',
            });
            await this.ringtone.play();
        });
    }
    async stopRingtone() {
        await ringtoneEventQueue.add(async () => {
            if (this.ringtone) {
                this.ringtone.stop();
                this.ringtone = undefined;
            }
        });
    }
    async someonePresenting() {
        const canPlayTone = window.Events.getCallRingtoneNotification();
        if (!canPlayTone) {
            return;
        }
        const tone = new Sound_1.Sound({
            src: 'sounds/navigation_selection-complete-celebration.ogg',
        });
        await tone.play();
    }
}
exports.callingTones = new CallingTones();
