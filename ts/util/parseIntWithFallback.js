"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseIntWithFallback = void 0;
const parseIntOrThrow_1 = require("./parseIntOrThrow");
function parseIntWithFallback(value, fallback) {
    try {
        return (0, parseIntOrThrow_1.parseIntOrThrow)(value, 'Failed to parse');
    }
    catch (err) {
        return fallback;
    }
}
exports.parseIntWithFallback = parseIntWithFallback;
