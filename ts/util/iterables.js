"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.zipObject = exports.take = exports.repeat = exports.reduce = exports.map = exports.isEmpty = exports.groupBy = exports.find = exports.filter = exports.concat = exports.size = exports.isIterable = void 0;
/* eslint-disable max-classes-per-file */
const getOwn_1 = require("./getOwn");
function isIterable(value) {
    return ((typeof value === 'object' && value !== null && Symbol.iterator in value) ||
        typeof value === 'string');
}
exports.isIterable = isIterable;
function size(iterable) {
    // We check for common types as an optimization.
    if (typeof iterable === 'string' || Array.isArray(iterable)) {
        return iterable.length;
    }
    if (iterable instanceof Set || iterable instanceof Map) {
        return iterable.size;
    }
    const iterator = iterable[Symbol.iterator]();
    let result = -1;
    for (let done = false; !done; result += 1) {
        done = Boolean(iterator.next().done);
    }
    return result;
}
exports.size = size;
function concat(...iterables) {
    return new ConcatIterable(iterables);
}
exports.concat = concat;
class ConcatIterable {
    constructor(iterables) {
        this.iterables = iterables;
    }
    *[Symbol.iterator]() {
        for (const iterable of this.iterables) {
            yield* iterable;
        }
    }
}
function filter(iterable, predicate) {
    return new FilterIterable(iterable, predicate);
}
exports.filter = filter;
class FilterIterable {
    constructor(iterable, predicate) {
        this.iterable = iterable;
        this.predicate = predicate;
    }
    [Symbol.iterator]() {
        return new FilterIterator(this.iterable[Symbol.iterator](), this.predicate);
    }
}
class FilterIterator {
    constructor(iterator, predicate) {
        this.iterator = iterator;
        this.predicate = predicate;
    }
    next() {
        // eslint-disable-next-line no-constant-condition
        while (true) {
            const nextIteration = this.iterator.next();
            if (nextIteration.done || this.predicate(nextIteration.value)) {
                return nextIteration;
            }
        }
    }
}
function find(iterable, predicate) {
    for (const value of iterable) {
        if (predicate(value)) {
            return value;
        }
    }
    return undefined;
}
exports.find = find;
function groupBy(iterable, fn) {
    const result = Object.create(null);
    for (const value of iterable) {
        const key = fn(value);
        const existingGroup = (0, getOwn_1.getOwn)(result, key);
        if (existingGroup) {
            existingGroup.push(value);
        }
        else {
            result[key] = [value];
        }
    }
    return result;
}
exports.groupBy = groupBy;
const isEmpty = (iterable) => Boolean(iterable[Symbol.iterator]().next().done);
exports.isEmpty = isEmpty;
function map(iterable, fn) {
    return new MapIterable(iterable, fn);
}
exports.map = map;
class MapIterable {
    constructor(iterable, fn) {
        this.iterable = iterable;
        this.fn = fn;
    }
    [Symbol.iterator]() {
        return new MapIterator(this.iterable[Symbol.iterator](), this.fn);
    }
}
class MapIterator {
    constructor(iterator, fn) {
        this.iterator = iterator;
        this.fn = fn;
    }
    next() {
        const nextIteration = this.iterator.next();
        if (nextIteration.done) {
            return nextIteration;
        }
        return {
            done: false,
            value: this.fn(nextIteration.value),
        };
    }
}
function reduce(iterable, fn, accumulator) {
    let result = accumulator;
    for (const value of iterable) {
        result = fn(result, value);
    }
    return result;
}
exports.reduce = reduce;
function repeat(value) {
    return new RepeatIterable(value);
}
exports.repeat = repeat;
class RepeatIterable {
    constructor(value) {
        this.value = value;
    }
    [Symbol.iterator]() {
        return new RepeatIterator(this.value);
    }
}
class RepeatIterator {
    constructor(value) {
        this.iteratorResult = {
            done: false,
            value,
        };
    }
    next() {
        return this.iteratorResult;
    }
}
function take(iterable, amount) {
    return new TakeIterable(iterable, amount);
}
exports.take = take;
class TakeIterable {
    constructor(iterable, amount) {
        this.iterable = iterable;
        this.amount = amount;
    }
    [Symbol.iterator]() {
        return new TakeIterator(this.iterable[Symbol.iterator](), this.amount);
    }
}
class TakeIterator {
    constructor(iterator, amount) {
        this.iterator = iterator;
        this.amount = amount;
    }
    next() {
        const nextIteration = this.iterator.next();
        if (nextIteration.done || this.amount === 0) {
            return { done: true, value: undefined };
        }
        this.amount -= 1;
        return nextIteration;
    }
}
// In the future, this could support number and symbol property names.
function zipObject(props, values) {
    const result = {};
    const propsIterator = props[Symbol.iterator]();
    const valuesIterator = values[Symbol.iterator]();
    // eslint-disable-next-line no-constant-condition
    while (true) {
        const propIteration = propsIterator.next();
        if (propIteration.done) {
            break;
        }
        const valueIteration = valuesIterator.next();
        if (valueIteration.done) {
            break;
        }
        result[propIteration.value] = valueIteration.value;
    }
    return result;
}
exports.zipObject = zipObject;
