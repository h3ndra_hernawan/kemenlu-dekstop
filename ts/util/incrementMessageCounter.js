"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.flushMessageCounter = exports.incrementMessageCounter = exports.initializeMessageCounter = void 0;
const lodash_1 = require("lodash");
const assert_1 = require("./assert");
const Client_1 = __importDefault(require("../sql/Client"));
const log = __importStar(require("../logging/log"));
let receivedAtCounter;
async function initializeMessageCounter() {
    (0, assert_1.strictAssert)(receivedAtCounter === undefined, 'incrementMessageCounter: already initialized');
    const storedCounter = Number(localStorage.getItem('lastReceivedAtCounter'));
    const dbCounter = await Client_1.default.getMaxMessageCounter();
    if ((0, lodash_1.isNumber)(dbCounter) && (0, lodash_1.isNumber)(storedCounter)) {
        log.info('initializeMessageCounter: picking max of db/stored counters');
        receivedAtCounter = Math.max(dbCounter, storedCounter);
        if (receivedAtCounter !== storedCounter) {
            log.warn('initializeMessageCounter: mismatch between db/stored counters');
        }
    }
    else if ((0, lodash_1.isNumber)(storedCounter)) {
        log.info('initializeMessageCounter: picking stored counter');
        receivedAtCounter = storedCounter;
    }
    else if ((0, lodash_1.isNumber)(dbCounter)) {
        log.info('initializeMessageCounter: picking fallback counter from the database');
        receivedAtCounter = dbCounter;
    }
    else {
        log.info('initializeMessageCounter: defaulting to Date.now()');
        receivedAtCounter = Date.now();
    }
    if (storedCounter !== receivedAtCounter) {
        localStorage.setItem('lastReceivedAtCounter', String(receivedAtCounter));
    }
}
exports.initializeMessageCounter = initializeMessageCounter;
function incrementMessageCounter() {
    (0, assert_1.strictAssert)(receivedAtCounter !== undefined, 'incrementMessageCounter: not initialized');
    receivedAtCounter += 1;
    debouncedUpdateLastReceivedAt();
    return receivedAtCounter;
}
exports.incrementMessageCounter = incrementMessageCounter;
function flushMessageCounter() {
    debouncedUpdateLastReceivedAt.flush();
}
exports.flushMessageCounter = flushMessageCounter;
const debouncedUpdateLastReceivedAt = (0, lodash_1.debounce)(() => {
    localStorage.setItem('lastReceivedAtCounter', String(receivedAtCounter));
}, 25, {
    maxWait: 25,
});
