"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.encryptProfileData = void 0;
const assert_1 = require("./assert");
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const zkgroup_1 = require("./zkgroup");
async function encryptProfileData(conversation, avatarBuffer) {
    const { aboutEmoji, aboutText, familyName, firstName, profileKey, uuid } = conversation;
    (0, assert_1.assert)(profileKey, 'profileKey');
    (0, assert_1.assert)(uuid, 'uuid');
    const keyBuffer = Bytes.fromBase64(profileKey);
    const fullName = [firstName, familyName].filter(Boolean).join('\0');
    const bytesName = (0, Crypto_1.encryptProfileItemWithPadding)(Bytes.fromString(fullName), keyBuffer, Crypto_1.PaddedLengths.Name);
    const bytesAbout = aboutText
        ? (0, Crypto_1.encryptProfileItemWithPadding)(Bytes.fromString(aboutText), keyBuffer, Crypto_1.PaddedLengths.About)
        : null;
    const bytesAboutEmoji = aboutEmoji
        ? (0, Crypto_1.encryptProfileItemWithPadding)(Bytes.fromString(aboutEmoji), keyBuffer, Crypto_1.PaddedLengths.AboutEmoji)
        : null;
    const encryptedAvatarData = avatarBuffer
        ? (0, Crypto_1.encryptProfile)(avatarBuffer, keyBuffer)
        : undefined;
    const profileData = {
        version: (0, zkgroup_1.deriveProfileKeyVersion)(profileKey, uuid),
        name: Bytes.toBase64(bytesName),
        about: bytesAbout ? Bytes.toBase64(bytesAbout) : null,
        aboutEmoji: bytesAboutEmoji ? Bytes.toBase64(bytesAboutEmoji) : null,
        paymentAddress: window.storage.get('paymentAddress') || null,
        avatar: Boolean(avatarBuffer),
        commitment: (0, zkgroup_1.deriveProfileKeyCommitment)(profileKey, uuid),
    };
    return [profileData, encryptedAvatarData];
}
exports.encryptProfileData = encryptProfileData;
