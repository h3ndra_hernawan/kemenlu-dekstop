"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RetryPlaceholders = exports.getDeltaIntoPast = exports.STORAGE_KEY = exports.getItemId = void 0;
const zod_1 = require("zod");
const lodash_1 = require("lodash");
const log = __importStar(require("../logging/log"));
const retryItemSchema = zod_1.z
    .object({
    conversationId: zod_1.z.string(),
    sentAt: zod_1.z.number(),
    receivedAt: zod_1.z.number(),
    receivedAtCounter: zod_1.z.number(),
    senderUuid: zod_1.z.string(),
    wasOpened: zod_1.z.boolean().optional(),
})
    .passthrough();
const retryItemListSchema = zod_1.z.array(retryItemSchema);
function getItemId(conversationId, sentAt) {
    return `${conversationId}--${sentAt}`;
}
exports.getItemId = getItemId;
const HOUR = 60 * 60 * 1000;
exports.STORAGE_KEY = 'retryPlaceholders';
function getDeltaIntoPast(delta) {
    return Date.now() - (delta || HOUR);
}
exports.getDeltaIntoPast = getDeltaIntoPast;
class RetryPlaceholders {
    constructor(options = {}) {
        if (!window.storage) {
            throw new Error('RetryPlaceholders.constructor: window.storage not available!');
        }
        const parsed = retryItemListSchema.safeParse(window.storage.get(exports.STORAGE_KEY, new Array()));
        if (!parsed.success) {
            log.warn(`RetryPlaceholders.constructor: Data fetched from storage did not match schema: ${JSON.stringify(parsed.error.flatten())}`);
        }
        this.items = parsed.success ? parsed.data : [];
        this.sortByExpiresAtAsc();
        this.byConversation = this.makeByConversationLookup();
        this.byMessage = this.makeByMessageLookup();
        this.retryReceiptLifespan = options.retryReceiptLifespan || HOUR;
        log.info(`RetryPlaceholders.constructor: Started with ${this.items.length} items, lifespan of ${this.retryReceiptLifespan}`);
    }
    // Arranging local data for efficiency
    sortByExpiresAtAsc() {
        this.items.sort((left, right) => left.receivedAt - right.receivedAt);
    }
    makeByConversationLookup() {
        return (0, lodash_1.groupBy)(this.items, item => item.conversationId);
    }
    makeByMessageLookup() {
        const lookup = new Map();
        this.items.forEach(item => {
            lookup.set(getItemId(item.conversationId, item.sentAt), item);
        });
        return lookup;
    }
    makeLookups() {
        this.byConversation = this.makeByConversationLookup();
        this.byMessage = this.makeByMessageLookup();
    }
    // Basic data management
    async add(item) {
        const parsed = retryItemSchema.safeParse(item);
        if (!parsed.success) {
            throw new Error(`RetryPlaceholders.add: Item did not match schema ${JSON.stringify(parsed.error.flatten())}`);
        }
        this.items.push(item);
        this.sortByExpiresAtAsc();
        this.makeLookups();
        await this.save();
    }
    async save() {
        await window.storage.put(exports.STORAGE_KEY, this.items);
    }
    // Finding items in different ways
    getCount() {
        return this.items.length;
    }
    getNextToExpire() {
        return this.items[0];
    }
    async getExpiredAndRemove() {
        const expiration = getDeltaIntoPast(this.retryReceiptLifespan);
        const max = this.items.length;
        const result = [];
        for (let i = 0; i < max; i += 1) {
            const item = this.items[i];
            if (item.receivedAt <= expiration) {
                result.push(item);
            }
            else {
                break;
            }
        }
        log.info(`RetryPlaceholders.getExpiredAndRemove: Found ${result.length} expired items`);
        this.items.splice(0, result.length);
        this.makeLookups();
        await this.save();
        return result;
    }
    async findByConversationAndMarkOpened(conversationId) {
        let changed = 0;
        const items = this.byConversation[conversationId];
        (items || []).forEach(item => {
            if (!item.wasOpened) {
                changed += 1;
                // eslint-disable-next-line no-param-reassign
                item.wasOpened = true;
            }
        });
        if (changed > 0) {
            log.info(`RetryPlaceholders.findByConversationAndMarkOpened: Updated ${changed} items for conversation ${conversationId}`);
            await this.save();
        }
    }
    async findByMessageAndRemove(conversationId, sentAt) {
        const result = this.byMessage.get(getItemId(conversationId, sentAt));
        if (!result) {
            return undefined;
        }
        const index = this.items.findIndex(item => item === result);
        this.items.splice(index, 1);
        this.makeLookups();
        await this.save();
        return result;
    }
}
exports.RetryPlaceholders = RetryPlaceholders;
