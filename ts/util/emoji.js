"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.splitByEmoji = exports.replaceEmojiWithSpaces = void 0;
const RGI_Emoji_1 = __importDefault(require("emoji-regex/es2015/RGI_Emoji"));
const assert_1 = require("./assert");
const iterables_1 = require("./iterables");
const REGEXP = (0, RGI_Emoji_1.default)();
const MAX_EMOJI_TO_MATCH = 5000;
function replaceEmojiWithSpaces(value) {
    return value.replace(REGEXP, ' ');
}
exports.replaceEmojiWithSpaces = replaceEmojiWithSpaces;
function splitByEmoji(value) {
    const emojis = (0, iterables_1.take)(value.matchAll(REGEXP), MAX_EMOJI_TO_MATCH);
    const result = [];
    let lastIndex = 0;
    for (const match of emojis) {
        const nonEmojiText = value.slice(lastIndex, match.index);
        if (nonEmojiText) {
            result.push({ type: 'text', value: nonEmojiText });
        }
        result.push({ type: 'emoji', value: match[0] });
        (0, assert_1.assert)(match.index !== undefined, '`matchAll` should provide indices');
        lastIndex = match.index + match[0].length;
    }
    const finalNonEmojiText = value.slice(lastIndex);
    if (finalNonEmojiText) {
        result.push({ type: 'text', value: finalNonEmojiText });
    }
    return result;
}
exports.splitByEmoji = splitByEmoji;
