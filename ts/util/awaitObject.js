"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.awaitObject = void 0;
async function awaitObject(settings) {
    const keys = Object.keys(settings);
    const promises = new Array();
    for (const key of keys) {
        promises.push(settings[key]);
    }
    const values = await Promise.all(promises);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const result = {};
    for (const [i, key] of keys.entries()) {
        result[key] = values[i];
    }
    return result;
}
exports.awaitObject = awaitObject;
