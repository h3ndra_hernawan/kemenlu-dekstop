"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.autoScale = exports.handleImageAttachment = void 0;
const path_1 = __importDefault(require("path"));
const electron_1 = require("electron");
const uuid_1 = require("uuid");
const VisualAttachment_1 = require("../types/VisualAttachment");
const MIME_1 = require("../types/MIME");
const Attachment_1 = require("../types/Attachment");
const imageToBlurHash_1 = require("./imageToBlurHash");
const scaleImageToLevel_1 = require("./scaleImageToLevel");
async function handleImageAttachment(file) {
    let processedFile = file;
    if ((0, MIME_1.isHeic)(file.type)) {
        const uuid = (0, uuid_1.v4)();
        const bytes = new Uint8Array(await file.arrayBuffer());
        const convertedFile = await new Promise((resolve, reject) => {
            electron_1.ipcRenderer.once(`convert-image:${uuid}`, (_, { error, response }) => {
                if (response) {
                    resolve(response);
                }
                else {
                    reject(error);
                }
            });
            electron_1.ipcRenderer.send('convert-image', uuid, bytes);
        });
        processedFile = new Blob([convertedFile]);
    }
    const { contentType, file: resizedBlob, fileName, } = await autoScale({
        contentType: (0, MIME_1.isHeic)(file.type) ? MIME_1.IMAGE_JPEG : (0, MIME_1.stringToMIMEType)(file.type),
        fileName: file.name,
        file: processedFile,
    });
    const data = await (0, VisualAttachment_1.blobToArrayBuffer)(resizedBlob);
    const blurHash = await (0, imageToBlurHash_1.imageToBlurHash)(resizedBlob);
    return {
        blurHash,
        contentType,
        data: new Uint8Array(data),
        fileName: fileName || file.name,
        path: file.name,
        pending: false,
        size: data.byteLength,
    };
}
exports.handleImageAttachment = handleImageAttachment;
async function autoScale({ contentType, file, fileName, }) {
    if (!(0, Attachment_1.canBeTranscoded)({ contentType })) {
        return { contentType, file, fileName };
    }
    const { blob, contentType: newContentType } = await (0, scaleImageToLevel_1.scaleImageToLevel)(file, contentType, true);
    if (newContentType !== MIME_1.IMAGE_JPEG) {
        return {
            contentType,
            file: blob,
            fileName,
        };
    }
    const { name } = path_1.default.parse(fileName);
    return {
        contentType: MIME_1.IMAGE_JPEG,
        file: blob,
        fileName: `${name}.jpg`,
    };
}
exports.autoScale = autoScale;
