"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isConversationAccepted = void 0;
const protobuf_1 = require("../protobuf");
const whatTypeOfConversation_1 = require("./whatTypeOfConversation");
const isInSystemContacts_1 = require("./isInSystemContacts");
/**
 * Determine if this conversation should be considered "accepted" in terms
 * of message requests
 */
function isConversationAccepted(conversationAttrs) {
    const messageRequestsEnabled = window.Signal.RemoteConfig.isEnabled('desktop.messageRequests');
    if (!messageRequestsEnabled) {
        return true;
    }
    if ((0, whatTypeOfConversation_1.isMe)(conversationAttrs)) {
        return true;
    }
    const messageRequestEnum = protobuf_1.SignalService.SyncMessage.MessageRequestResponse.Type;
    const { messageRequestResponseType } = conversationAttrs;
    if (messageRequestResponseType === messageRequestEnum.ACCEPT) {
        return true;
    }
    const { sentMessageCount } = conversationAttrs;
    const hasSentMessages = sentMessageCount > 0;
    const hasMessagesBeforeMessageRequests = (conversationAttrs.messageCountBeforeMessageRequests || 0) > 0;
    const hasNoMessages = (conversationAttrs.messageCount || 0) === 0;
    const isEmptyPrivateConvo = hasNoMessages && (0, whatTypeOfConversation_1.isDirectConversation)(conversationAttrs);
    const isEmptyWhitelistedGroup = hasNoMessages &&
        !(0, whatTypeOfConversation_1.isDirectConversation)(conversationAttrs) &&
        conversationAttrs.profileSharing;
    return (isFromOrAddedByTrustedContact(conversationAttrs) ||
        hasSentMessages ||
        hasMessagesBeforeMessageRequests ||
        // an empty group is the scenario where we need to rely on
        // whether the profile has already been shared or not
        isEmptyPrivateConvo ||
        isEmptyWhitelistedGroup);
}
exports.isConversationAccepted = isConversationAccepted;
// Is this someone who is a contact, or are we sharing our profile with them?
//   Or is the person who added us to this group a contact or are we sharing profile
//   with them?
function isFromOrAddedByTrustedContact(conversationAttrs) {
    if ((0, whatTypeOfConversation_1.isDirectConversation)(conversationAttrs)) {
        return ((0, isInSystemContacts_1.isInSystemContacts)(conversationAttrs) ||
            Boolean(conversationAttrs.profileSharing));
    }
    const { addedBy } = conversationAttrs;
    if (!addedBy) {
        return false;
    }
    const conversation = window.ConversationController.get(addedBy);
    if (!conversation) {
        return false;
    }
    return Boolean((0, whatTypeOfConversation_1.isMe)(conversation.attributes) ||
        conversation.get('name') ||
        conversation.get('profileSharing'));
}
