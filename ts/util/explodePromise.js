"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.explodePromise = void 0;
function explodePromise() {
    let resolve;
    let reject;
    const promise = new Promise((innerResolve, innerReject) => {
        resolve = innerResolve;
        reject = innerReject;
    });
    return {
        promise,
        // Typescript thinks that resolve and reject can be undefined here.
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        resolve: resolve,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        reject: reject,
    };
}
exports.explodePromise = explodePromise;
