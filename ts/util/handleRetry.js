"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.onDecryptionError = exports.onRetryRequest = void 0;
const signal_client_1 = require("@signalapp/signal-client");
const lodash_1 = require("lodash");
const version_1 = require("./version");
const assert_1 = require("./assert");
const getSendOptions_1 = require("./getSendOptions");
const handleMessageSend_1 = require("./handleMessageSend");
const whatTypeOfConversation_1 = require("./whatTypeOfConversation");
const timestamp_1 = require("./timestamp");
const parseIntOrThrow_1 = require("./parseIntOrThrow");
const RemoteConfig = __importStar(require("../RemoteConfig"));
const Address_1 = require("../types/Address");
const QualifiedAddress_1 = require("../types/QualifiedAddress");
const ToastDecryptionError_1 = require("../components/ToastDecryptionError");
const showToast_1 = require("./showToast");
const protobuf_1 = require("../protobuf");
const log = __importStar(require("../logging/log"));
// Entrypoints
async function onRetryRequest(event) {
    const { confirm, retryRequest } = event;
    const { groupId: requestGroupId, requesterDevice, requesterUuid, senderDevice, sentAt, } = retryRequest;
    const logId = `${requesterUuid}.${requesterDevice} ${sentAt}.${senderDevice}`;
    log.info(`onRetryRequest/${logId}: Starting...`);
    if (!RemoteConfig.isEnabled('desktop.senderKey.retry')) {
        log.warn(`onRetryRequest/${logId}: Feature flag disabled, returning early.`);
        confirm();
        return;
    }
    if (window.RETRY_DELAY) {
        log.warn(`onRetryRequest/${logId}: Delaying because RETRY_DELAY is set...`);
        await new Promise(resolve => setTimeout(resolve, 5000));
    }
    const HOUR = 60 * 60 * 1000;
    const DAY = 24 * HOUR;
    let retryRespondMaxAge = 14 * DAY;
    try {
        retryRespondMaxAge = (0, parseIntOrThrow_1.parseIntOrThrow)(RemoteConfig.getValue('desktop.retryRespondMaxAge'), 'retryRespondMaxAge');
    }
    catch (error) {
        log.warn(`onRetryRequest/${logId}: Failed to parse integer from desktop.retryRespondMaxAge feature flag`, error && error.stack ? error.stack : error);
    }
    await archiveSessionOnMatch(retryRequest);
    if ((0, timestamp_1.isOlderThan)(sentAt, retryRespondMaxAge)) {
        log.info(`onRetryRequest/${logId}: Message is too old, refusing to send again.`);
        await sendDistributionMessageOrNullMessage(logId, retryRequest);
        confirm();
        return;
    }
    const sentProto = await window.Signal.Data.getSentProtoByRecipient({
        now: Date.now(),
        recipientUuid: requesterUuid,
        timestamp: sentAt,
    });
    if (!sentProto) {
        log.info(`onRetryRequest/${logId}: Did not find sent proto`);
        await sendDistributionMessageOrNullMessage(logId, retryRequest);
        confirm();
        return;
    }
    log.info(`onRetryRequest/${logId}: Resending message`);
    const { contentHint, messageIds, proto, timestamp } = sentProto;
    const { contentProto, groupId } = await maybeAddSenderKeyDistributionMessage({
        contentProto: protobuf_1.SignalService.Content.decode(proto),
        logId,
        messageIds,
        requestGroupId,
        requesterUuid,
    });
    const recipientConversation = window.ConversationController.getOrCreate(requesterUuid, 'private');
    const sendOptions = await (0, getSendOptions_1.getSendOptions)(recipientConversation.attributes);
    const promise = window.textsecure.messaging.sendMessageProtoAndWait({
        timestamp,
        recipients: [requesterUuid],
        proto: new protobuf_1.SignalService.Content(contentProto),
        contentHint,
        groupId,
        options: sendOptions,
    });
    await (0, handleMessageSend_1.handleMessageSend)(promise, {
        messageIds: [],
        sendType: 'resendFromLog',
    });
    confirm();
    log.info(`onRetryRequest/${logId}: Resend complete.`);
}
exports.onRetryRequest = onRetryRequest;
function maybeShowDecryptionToast(logId) {
    if ((0, version_1.isProduction)(window.getVersion())) {
        return;
    }
    log.info(`maybeShowDecryptionToast/${logId}: Showing decryption error toast`);
    (0, showToast_1.showToast)(ToastDecryptionError_1.ToastDecryptionError, {
        onShowDebugLog: () => window.showDebugLog(),
    });
}
async function onDecryptionError(event) {
    var _a, _b;
    const { confirm, decryptionError } = event;
    const { senderUuid, senderDevice, timestamp } = decryptionError;
    const logId = `${senderUuid}.${senderDevice} ${timestamp}`;
    log.info(`onDecryptionError/${logId}: Starting...`);
    const conversation = window.ConversationController.getOrCreate(senderUuid, 'private');
    if (!((_a = conversation.get('capabilities')) === null || _a === void 0 ? void 0 : _a.senderKey)) {
        await conversation.getProfiles();
    }
    maybeShowDecryptionToast(logId);
    if (((_b = conversation.get('capabilities')) === null || _b === void 0 ? void 0 : _b.senderKey) &&
        RemoteConfig.isEnabled('desktop.senderKey.retry')) {
        await requestResend(decryptionError);
    }
    else {
        await startAutomaticSessionReset(decryptionError);
    }
    confirm();
    log.info(`onDecryptionError/${logId}: ...complete`);
}
exports.onDecryptionError = onDecryptionError;
// Helpers
async function archiveSessionOnMatch({ ratchetKey, requesterUuid, requesterDevice, senderDevice, }) {
    const ourDeviceId = (0, parseIntOrThrow_1.parseIntOrThrow)(window.textsecure.storage.user.getDeviceId(), 'archiveSessionOnMatch/getDeviceId');
    if (ourDeviceId !== senderDevice || !ratchetKey) {
        return;
    }
    const ourUuid = window.textsecure.storage.user.getCheckedUuid();
    const address = new QualifiedAddress_1.QualifiedAddress(ourUuid, Address_1.Address.create(requesterUuid, requesterDevice));
    const session = await window.textsecure.storage.protocol.loadSession(address);
    if (session && session.currentRatchetKeyMatches(ratchetKey)) {
        log.info('archiveSessionOnMatch: Matching device and ratchetKey, archiving session');
        await window.textsecure.storage.protocol.archiveSession(address);
    }
}
async function sendDistributionMessageOrNullMessage(logId, options) {
    var _a;
    const { groupId, requesterUuid } = options;
    let sentDistributionMessage = false;
    log.info(`sendDistributionMessageOrNullMessage/${logId}: Starting...`);
    const conversation = window.ConversationController.getOrCreate(requesterUuid, 'private');
    if (groupId) {
        const group = window.ConversationController.get(groupId);
        const distributionId = (_a = group === null || group === void 0 ? void 0 : group.get('senderKeyInfo')) === null || _a === void 0 ? void 0 : _a.distributionId;
        if (group && !group.hasMember(requesterUuid)) {
            throw new Error(`sendDistributionMessageOrNullMessage/${logId}: Requester ${requesterUuid} is not a member of ${conversation.idForLogging()}`);
        }
        if (group && distributionId) {
            log.info(`sendDistributionMessageOrNullMessage/${logId}: Found matching group, sending sender key distribution message`);
            try {
                const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
                const result = await (0, handleMessageSend_1.handleMessageSend)(window.textsecure.messaging.sendSenderKeyDistributionMessage({
                    contentHint: ContentHint.RESENDABLE,
                    distributionId,
                    groupId,
                    identifiers: [requesterUuid],
                }), { messageIds: [], sendType: 'senderKeyDistributionMessage' });
                if (result && result.errors && result.errors.length > 0) {
                    throw result.errors[0];
                }
                sentDistributionMessage = true;
            }
            catch (error) {
                log.error(`sendDistributionMessageOrNullMessage/${logId}: Failed to send sender key distribution message`, error && error.stack ? error.stack : error);
            }
        }
    }
    if (!sentDistributionMessage) {
        log.info(`sendDistributionMessageOrNullMessage/${logId}: Did not send distribution message, sending null message`);
        try {
            const sendOptions = await (0, getSendOptions_1.getSendOptions)(conversation.attributes);
            const result = await (0, handleMessageSend_1.handleMessageSend)(window.textsecure.messaging.sendNullMessage({ uuid: requesterUuid }, sendOptions), { messageIds: [], sendType: 'nullMessage' });
            if (result && result.errors && result.errors.length > 0) {
                throw result.errors[0];
            }
        }
        catch (error) {
            log.error(`maybeSendDistributionMessage/${logId}: Failed to send null message`, error && error.stack ? error.stack : error);
        }
    }
}
async function getRetryConversation({ logId, messageIds, requestGroupId, }) {
    if (messageIds.length !== 1) {
        // Fail over to requested groupId
        return window.ConversationController.get(requestGroupId);
    }
    const [messageId] = messageIds;
    const message = await window.Signal.Data.getMessageById(messageId, {
        Message: window.Whisper.Message,
    });
    if (!message) {
        log.warn(`maybeAddSenderKeyDistributionMessage/${logId}: Unable to find message ${messageId}`);
        // Fail over to requested groupId
        return window.ConversationController.get(requestGroupId);
    }
    const conversationId = message.get('conversationId');
    return window.ConversationController.get(conversationId);
}
async function maybeAddSenderKeyDistributionMessage({ contentProto, logId, messageIds, requestGroupId, requesterUuid, }) {
    const conversation = await getRetryConversation({
        logId,
        messageIds,
        requestGroupId,
    });
    if (!conversation) {
        log.warn(`maybeAddSenderKeyDistributionMessage/${logId}: Unable to find conversation`);
        return {
            contentProto,
        };
    }
    if (!conversation.hasMember(requesterUuid)) {
        throw new Error(`maybeAddSenderKeyDistributionMessage/${logId}: Recipient ${requesterUuid} is not a member of ${conversation.idForLogging()}`);
    }
    if (!(0, whatTypeOfConversation_1.isGroupV2)(conversation.attributes)) {
        return {
            contentProto,
        };
    }
    const senderKeyInfo = conversation.get('senderKeyInfo');
    if (senderKeyInfo && senderKeyInfo.distributionId) {
        const senderKeyDistributionMessage = await window.textsecure.messaging.getSenderKeyDistributionMessage(senderKeyInfo.distributionId);
        return {
            contentProto: Object.assign(Object.assign({}, contentProto), { senderKeyDistributionMessage: senderKeyDistributionMessage.serialize() }),
            groupId: conversation.get('groupId'),
        };
    }
    return {
        contentProto,
        groupId: conversation.get('groupId'),
    };
}
async function requestResend(decryptionError) {
    const { cipherTextBytes, cipherTextType, contentHint, groupId, receivedAtCounter, receivedAtDate, senderDevice, senderUuid, timestamp, } = decryptionError;
    const logId = `${senderUuid}.${senderDevice} ${timestamp}`;
    log.info(`requestResend/${logId}: Starting...`, {
        cipherTextBytesLength: cipherTextBytes === null || cipherTextBytes === void 0 ? void 0 : cipherTextBytes.byteLength,
        cipherTextType,
        contentHint,
        groupId: groupId ? `groupv2(${groupId})` : undefined,
    });
    // 1. Find the target conversation
    const group = groupId
        ? window.ConversationController.get(groupId)
        : undefined;
    const sender = window.ConversationController.getOrCreate(senderUuid, 'private');
    const conversation = group || sender;
    // 2. Send resend request
    if (!cipherTextBytes || !(0, lodash_1.isNumber)(cipherTextType)) {
        log.warn(`requestResend/${logId}: Missing cipherText information, failing over to automatic reset`);
        startAutomaticSessionReset(decryptionError);
        return;
    }
    try {
        const message = signal_client_1.DecryptionErrorMessage.forOriginal(Buffer.from(cipherTextBytes), cipherTextType, timestamp, senderDevice);
        const plaintext = signal_client_1.PlaintextContent.from(message);
        const options = await (0, getSendOptions_1.getSendOptions)(conversation.attributes);
        const result = await (0, handleMessageSend_1.handleMessageSend)(window.textsecure.messaging.sendRetryRequest({
            plaintext,
            options,
            groupId,
            uuid: senderUuid,
        }), { messageIds: [], sendType: 'retryRequest' });
        if (result && result.errors && result.errors.length > 0) {
            throw result.errors[0];
        }
    }
    catch (error) {
        log.error(`requestResend/${logId}: Failed to send retry request, failing over to automatic reset`, error && error.stack ? error.stack : error);
        startAutomaticSessionReset(decryptionError);
        return;
    }
    const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
    // 3. Determine how to represent this to the user. Three different options.
    // We believe that it could be successfully re-sent, so we'll add a placeholder.
    if (contentHint === ContentHint.RESENDABLE) {
        const { retryPlaceholders } = window.Signal.Services;
        (0, assert_1.strictAssert)(retryPlaceholders, 'requestResend: adding placeholder');
        log.info(`requestResend/${logId}: Adding placeholder`);
        const state = window.reduxStore.getState();
        const selectedId = state.conversations.selectedConversationId;
        const wasOpened = selectedId === conversation.id;
        await retryPlaceholders.add({
            conversationId: conversation.get('id'),
            receivedAt: receivedAtDate,
            receivedAtCounter,
            sentAt: timestamp,
            senderUuid,
            wasOpened,
        });
        return;
    }
    // This message cannot be resent. We'll show no error and trust the other side to
    //   reset their session.
    if (contentHint === ContentHint.IMPLICIT) {
        log.info(`requestResend/${logId}: contentHint is IMPLICIT, doing nothing.`);
        return;
    }
    log.warn(`requestResend/${logId}: No content hint, adding error immediately`);
    conversation.queueJob('addDeliveryIssue', async () => {
        conversation.addDeliveryIssue({
            receivedAt: receivedAtDate,
            receivedAtCounter,
            senderUuid,
            sentAt: timestamp,
        });
    });
}
function scheduleSessionReset(senderUuid, senderDevice) {
    // Postpone sending light session resets until the queue is empty
    const { lightSessionResetQueue } = window.Signal.Services;
    if (!lightSessionResetQueue) {
        throw new Error('scheduleSessionReset: lightSessionResetQueue is not available!');
    }
    lightSessionResetQueue.add(() => {
        const ourUuid = window.textsecure.storage.user.getCheckedUuid();
        window.textsecure.storage.protocol.lightSessionReset(new QualifiedAddress_1.QualifiedAddress(ourUuid, Address_1.Address.create(senderUuid, senderDevice)));
    });
}
function startAutomaticSessionReset(decryptionError) {
    const { senderUuid, senderDevice, timestamp } = decryptionError;
    const logId = `${senderUuid}.${senderDevice} ${timestamp}`;
    log.info(`startAutomaticSessionReset/${logId}: Starting...`);
    scheduleSessionReset(senderUuid, senderDevice);
    const conversationId = window.ConversationController.ensureContactIds({
        uuid: senderUuid,
    });
    if (!conversationId) {
        log.warn('onLightSessionReset: No conversation id, cannot add message to timeline');
        return;
    }
    const conversation = window.ConversationController.get(conversationId);
    if (!conversation) {
        log.warn('onLightSessionReset: No conversation, cannot add message to timeline');
        return;
    }
    const receivedAt = Date.now();
    const receivedAtCounter = window.Signal.Util.incrementMessageCounter();
    conversation.queueJob('addChatSessionRefreshed', async () => {
        conversation.addChatSessionRefreshed({ receivedAt, receivedAtCounter });
    });
}
