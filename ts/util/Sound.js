"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sound = void 0;
const log = __importStar(require("../logging/log"));
class Sound {
    constructor(options) {
        this.context = new AudioContext();
        this.loop = Boolean(options.loop);
        this.src = options.src;
    }
    async play() {
        if (!Sound.sounds.has(this.src)) {
            try {
                const buffer = await Sound.loadSoundFile(this.src);
                const decodedBuffer = await this.context.decodeAudioData(buffer);
                Sound.sounds.set(this.src, decodedBuffer);
            }
            catch (err) {
                log.error(`Sound error: ${err}`);
                return;
            }
        }
        const soundBuffer = Sound.sounds.get(this.src);
        const soundNode = this.context.createBufferSource();
        soundNode.buffer = soundBuffer;
        const volumeNode = this.context.createGain();
        soundNode.connect(volumeNode);
        volumeNode.connect(this.context.destination);
        soundNode.loop = this.loop;
        soundNode.start(0, 0);
        this.node = soundNode;
    }
    stop() {
        if (this.node) {
            this.node.stop(0);
            this.node = undefined;
        }
    }
    static async loadSoundFile(src) {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', src, true);
        xhr.responseType = 'arraybuffer';
        return new Promise((resolve, reject) => {
            xhr.onload = () => {
                if (xhr.status === 200) {
                    resolve(xhr.response);
                    return;
                }
                reject(new Error(`Request failed: ${xhr.statusText}`));
            };
            xhr.onerror = () => {
                reject(new Error(`Request failed, most likely file not found: ${src}`));
            };
            xhr.send();
        });
    }
}
exports.Sound = Sound;
Sound.sounds = new Map();
