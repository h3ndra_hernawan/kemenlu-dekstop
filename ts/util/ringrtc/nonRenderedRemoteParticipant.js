"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.nonRenderedRemoteParticipant = void 0;
const nonRenderedRemoteParticipant = ({ demuxId, }) => ({
    demuxId,
    width: 0,
    height: 0,
});
exports.nonRenderedRemoteParticipant = nonRenderedRemoteParticipant;
