"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTimestampFromLong = exports.getSafeLongFromTimestamp = void 0;
const long_1 = __importDefault(require("long"));
const normalizeNumber_1 = require("./normalizeNumber");
function getSafeLongFromTimestamp(timestamp = 0) {
    if (timestamp >= Number.MAX_SAFE_INTEGER) {
        return long_1.default.MAX_VALUE;
    }
    return long_1.default.fromNumber(timestamp);
}
exports.getSafeLongFromTimestamp = getSafeLongFromTimestamp;
function getTimestampFromLong(value) {
    if (!value) {
        return 0;
    }
    const num = (0, normalizeNumber_1.normalizeNumber)(value);
    if (num >= Number.MAX_SAFE_INTEGER) {
        return Number.MAX_SAFE_INTEGER;
    }
    return num;
}
exports.getTimestampFromLong = getTimestampFromLong;
