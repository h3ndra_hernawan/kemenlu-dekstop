"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createIPCEvents = void 0;
const electron_1 = require("electron");
const Colors_1 = require("../types/Colors");
const Stickers = __importStar(require("../types/Stickers"));
const SystemTraySetting_1 = require("../types/SystemTraySetting");
const calling_1 = require("../services/calling");
const conversations_1 = require("../state/selectors/conversations");
const items_1 = require("../state/selectors/items");
const events_1 = require("../shims/events");
const themeChanged_1 = require("../shims/themeChanged");
const renderClearingDataView_1 = require("../shims/renderClearingDataView");
const universalExpireTimer = __importStar(require("./universalExpireTimer"));
const phoneNumberDiscoverability_1 = require("./phoneNumberDiscoverability");
const phoneNumberSharingMode_1 = require("./phoneNumberSharingMode");
const assert_1 = require("./assert");
const durations = __importStar(require("./durations"));
const isPhoneNumberSharingEnabled_1 = require("./isPhoneNumberSharingEnabled");
const sgnlHref_1 = require("./sgnlHref");
const log = __importStar(require("../logging/log"));
function createIPCEvents(overrideEvents = {}) {
    return Object.assign({ getDeviceName: () => window.textsecure.storage.user.getDeviceName(), getZoomFactor: () => window.storage.get('zoomFactor', 1), setZoomFactor: async (zoomFactor) => {
            electron_1.webFrame.setZoomFactor(zoomFactor);
        }, getPreferredAudioInputDevice: () => window.storage.get('preferred-audio-input-device'), setPreferredAudioInputDevice: device => window.storage.put('preferred-audio-input-device', device), getPreferredAudioOutputDevice: () => window.storage.get('preferred-audio-output-device'), setPreferredAudioOutputDevice: device => window.storage.put('preferred-audio-output-device', device), getPreferredVideoInputDevice: () => window.storage.get('preferred-video-input-device'), setPreferredVideoInputDevice: device => window.storage.put('preferred-video-input-device', device), 
        // Chat Color redux hookups
        getCustomColors: () => {
            return (0, items_1.getCustomColors)(window.reduxStore.getState()) || {};
        }, getConversationsWithCustomColor: colorId => {
            return (0, conversations_1.getConversationsWithCustomColorSelector)(window.reduxStore.getState())(colorId);
        }, addCustomColor: (...args) => window.reduxActions.items.addCustomColor(...args), editCustomColor: (...args) => window.reduxActions.items.editCustomColor(...args), removeCustomColor: colorId => window.reduxActions.items.removeCustomColor(colorId), removeCustomColorOnConversations: colorId => window.reduxActions.conversations.removeCustomColorOnConversations(colorId), resetAllChatColors: () => window.reduxActions.conversations.resetAllChatColors(), resetDefaultChatColor: () => window.reduxActions.items.resetDefaultChatColor(), setGlobalDefaultConversationColor: (...args) => window.reduxActions.items.setGlobalDefaultConversationColor(...args), 
        // Getters only
        getAvailableIODevices: async () => {
            const { availableCameras, availableMicrophones, availableSpeakers } = await calling_1.calling.getAvailableIODevices();
            return {
                // mapping it to a pojo so that it is IPC friendly
                availableCameras: availableCameras.map((inputDeviceInfo) => ({
                    deviceId: inputDeviceInfo.deviceId,
                    groupId: inputDeviceInfo.groupId,
                    kind: inputDeviceInfo.kind,
                    label: inputDeviceInfo.label,
                })),
                availableMicrophones,
                availableSpeakers,
            };
        }, getBlockedCount: () => window.storage.blocked.getBlockedUuids().length +
            window.storage.blocked.getBlockedGroups().length, getDefaultConversationColor: () => window.storage.get('defaultConversationColor', Colors_1.DEFAULT_CONVERSATION_COLOR), getLinkPreviewSetting: () => window.storage.get('linkPreviews', false), getPhoneNumberDiscoverabilitySetting: () => window.storage.get('phoneNumberDiscoverability', phoneNumberDiscoverability_1.PhoneNumberDiscoverability.NotDiscoverable), getPhoneNumberSharingSetting: () => window.storage.get('phoneNumberSharingMode', phoneNumberSharingMode_1.PhoneNumberSharingMode.Nobody), getReadReceiptSetting: () => window.storage.get('read-receipt-setting', false), getTypingIndicatorSetting: () => window.storage.get('typingIndicators', false), 
        // Configurable settings
        getAutoDownloadUpdate: () => window.storage.get('auto-download-update', true), setAutoDownloadUpdate: value => window.storage.put('auto-download-update', value), getThemeSetting: () => window.storage.get('theme-setting', window.platform === 'darwin' ? 'system' : 'light'), setThemeSetting: value => {
            const promise = window.storage.put('theme-setting', value);
            (0, themeChanged_1.themeChanged)();
            return promise;
        }, getHideMenuBar: () => window.storage.get('hide-menu-bar'), setHideMenuBar: value => {
            const promise = window.storage.put('hide-menu-bar', value);
            window.setAutoHideMenuBar(value);
            window.setMenuBarVisibility(!value);
            return promise;
        }, getSystemTraySetting: () => (0, SystemTraySetting_1.parseSystemTraySetting)(window.storage.get('system-tray-setting')), setSystemTraySetting: value => {
            const promise = window.storage.put('system-tray-setting', value);
            window.updateSystemTraySetting(value);
            return promise;
        }, getNotificationSetting: () => window.storage.get('notification-setting', 'message'), setNotificationSetting: (value) => window.storage.put('notification-setting', value), getNotificationDrawAttention: () => window.storage.get('notification-draw-attention', true), setNotificationDrawAttention: value => window.storage.put('notification-draw-attention', value), getAudioNotification: () => window.storage.get('audio-notification'), setAudioNotification: value => window.storage.put('audio-notification', value), getCountMutedConversations: () => window.storage.get('badge-count-muted-conversations', false), setCountMutedConversations: value => {
            const promise = window.storage.put('badge-count-muted-conversations', value);
            window.Whisper.events.trigger('updateUnreadCount');
            return promise;
        }, getCallRingtoneNotification: () => window.storage.get('call-ringtone-notification', true), setCallRingtoneNotification: value => window.storage.put('call-ringtone-notification', value), getCallSystemNotification: () => window.storage.get('call-system-notification', true), setCallSystemNotification: value => window.storage.put('call-system-notification', value), getIncomingCallNotification: () => window.storage.get('incoming-call-notification', true), setIncomingCallNotification: value => window.storage.put('incoming-call-notification', value), getSpellCheck: () => window.storage.get('spell-check', true), setSpellCheck: value => window.storage.put('spell-check', value), getAlwaysRelayCalls: () => window.storage.get('always-relay-calls'), setAlwaysRelayCalls: value => window.storage.put('always-relay-calls', value), getAutoLaunch: () => window.getAutoLaunch(), setAutoLaunch: async (value) => {
            return window.setAutoLaunch(value);
        }, isPhoneNumberSharingEnabled: () => (0, isPhoneNumberSharingEnabled_1.isPhoneNumberSharingEnabled)(), isPrimary: () => window.textsecure.storage.user.getDeviceId() === 1, syncRequest: () => new Promise((resolve, reject) => {
            const FIVE_MINUTES = 5 * durations.MINUTE;
            const syncRequest = window.getSyncRequest(FIVE_MINUTES);
            syncRequest.addEventListener('success', () => resolve());
            syncRequest.addEventListener('timeout', () => reject(new Error('timeout')));
        }), getLastSyncTime: () => window.storage.get('synced_at'), setLastSyncTime: value => window.storage.put('synced_at', value), getUniversalExpireTimer: () => universalExpireTimer.get(), setUniversalExpireTimer: async (newValue) => {
            await universalExpireTimer.set(newValue);
            // Update account in Storage Service
            const conversationId = window.ConversationController.getOurConversationIdOrThrow();
            const account = window.ConversationController.get(conversationId);
            (0, assert_1.assert)(account, "Account wasn't found");
            account.captureChange('universalExpireTimer');
            // Add a notification to the currently open conversation
            const state = window.reduxStore.getState();
            const selectedId = state.conversations.selectedConversationId;
            if (selectedId) {
                const conversation = window.ConversationController.get(selectedId);
                (0, assert_1.assert)(conversation, "Conversation wasn't found");
                await conversation.updateLastMessage();
            }
        }, addDarkOverlay: () => {
            if ($('.dark-overlay').length) {
                return;
            }
            $(document.body).prepend('<div class="dark-overlay"></div>');
            $('.dark-overlay').on('click', () => $('.dark-overlay').remove());
        }, removeDarkOverlay: () => $('.dark-overlay').remove(), showKeyboardShortcuts: () => window.showKeyboardShortcuts(), deleteAllData: async () => {
            await window.Signal.Data.goBackToMainProcess();
            (0, renderClearingDataView_1.renderClearingDataView)();
        }, closeDB: async () => {
            await window.Signal.Data.goBackToMainProcess();
        }, showStickerPack: (packId, key) => {
            // We can get these events even if the user has never linked this instance.
            if (!window.Signal.Util.Registration.everDone()) {
                log.warn('showStickerPack: Not registered, returning early');
                return;
            }
            if (window.isShowingModal) {
                log.warn('showStickerPack: Already showing modal, returning early');
                return;
            }
            try {
                window.isShowingModal = true;
                // Kick off the download
                Stickers.downloadEphemeralPack(packId, key);
                const props = {
                    packId,
                    onClose: async () => {
                        window.isShowingModal = false;
                        stickerPreviewModalView.remove();
                        await Stickers.removeEphemeralPack(packId);
                    },
                };
                const stickerPreviewModalView = new window.Whisper.ReactWrapperView({
                    className: 'sticker-preview-modal-wrapper',
                    JSX: window.Signal.State.Roots.createStickerPreviewModal(window.reduxStore, props),
                });
            }
            catch (error) {
                window.isShowingModal = false;
                log.error('showStickerPack: Ran into an error!', error && error.stack ? error.stack : error);
                const errorView = new window.Whisper.ReactWrapperView({
                    className: 'error-modal-wrapper',
                    Component: window.Signal.Components.ErrorModal,
                    props: {
                        onClose: () => {
                            errorView.remove();
                        },
                    },
                });
            }
        }, showGroupViaLink: async (hash) => {
            // We can get these events even if the user has never linked this instance.
            if (!window.Signal.Util.Registration.everDone()) {
                log.warn('showGroupViaLink: Not registered, returning early');
                return;
            }
            if (window.isShowingModal) {
                log.warn('showGroupViaLink: Already showing modal, returning early');
                return;
            }
            try {
                await window.Signal.Groups.joinViaLink(hash);
            }
            catch (error) {
                log.error('showGroupViaLink: Ran into an error!', error && error.stack ? error.stack : error);
                const errorView = new window.Whisper.ReactWrapperView({
                    className: 'error-modal-wrapper',
                    Component: window.Signal.Components.ErrorModal,
                    props: {
                        title: window.i18n('GroupV2--join--general-join-failure--title'),
                        description: window.i18n('GroupV2--join--general-join-failure'),
                        onClose: () => {
                            errorView.remove();
                        },
                    },
                });
            }
            window.isShowingModal = false;
        }, showConversationViaSignalDotMe(hash) {
            if (!window.Signal.Util.Registration.everDone()) {
                log.info('showConversationViaSignalDotMe: Not registered, returning early');
                return;
            }
            const maybeE164 = (0, sgnlHref_1.parseE164FromSignalDotMeHash)(hash);
            if (maybeE164) {
                (0, events_1.trigger)('showConversation', maybeE164);
                return;
            }
            log.info('showConversationViaSignalDotMe: invalid E164');
            if (window.isShowingModal) {
                log.info('showConversationViaSignalDotMe: a modal is already showing. Doing nothing');
            }
            else {
                showUnknownSgnlLinkModal();
            }
        }, unknownSignalLink: () => {
            log.warn('unknownSignalLink: Showing error dialog');
            showUnknownSgnlLinkModal();
        }, installStickerPack: async (packId, key) => {
            Stickers.downloadStickerPack(packId, key, {
                finalStatus: 'installed',
            });
        }, shutdown: () => Promise.resolve(), showReleaseNotes: () => {
            const { showWhatsNewModal } = window.reduxActions.globalModals;
            showWhatsNewModal();
        }, getMediaPermissions: window.getMediaPermissions, getMediaCameraPermissions: window.getMediaCameraPermissions, persistZoomFactor: zoomFactor => window.storage.put('zoomFactor', zoomFactor) }, overrideEvents);
}
exports.createIPCEvents = createIPCEvents;
function showUnknownSgnlLinkModal() {
    const errorView = new window.Whisper.ReactWrapperView({
        className: 'error-modal-wrapper',
        Component: window.Signal.Components.ErrorModal,
        props: {
            description: window.i18n('unknown-sgnl-link'),
            onClose: () => {
                errorView.remove();
            },
        },
    });
}
