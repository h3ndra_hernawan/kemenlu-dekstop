"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapToSupportLocale = void 0;
function mapToSupportLocale(ourLocale) {
    if (ourLocale === 'ar') {
        return ourLocale;
    }
    if (ourLocale === 'de') {
        return ourLocale;
    }
    if (ourLocale === 'es') {
        return ourLocale;
    }
    if (ourLocale === 'fr') {
        return ourLocale;
    }
    if (ourLocale === 'it') {
        return ourLocale;
    }
    if (ourLocale === 'ja') {
        return ourLocale;
    }
    if (ourLocale === 'pl') {
        return ourLocale;
    }
    if (ourLocale === 'pt_BR') {
        return 'pt-br';
    }
    if (ourLocale === 'ru') {
        return ourLocale;
    }
    if (ourLocale === 'sq') {
        return ourLocale;
    }
    if (ourLocale === 'zh_TW') {
        return 'zh-tw';
    }
    return 'en-us';
}
exports.mapToSupportLocale = mapToSupportLocale;
