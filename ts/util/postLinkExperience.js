"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.postLinkExperience = void 0;
const durations_1 = require("./durations");
class PostLinkExperience {
    constructor() {
        this.hasNotFinishedSync = false;
    }
    start() {
        this.hasNotFinishedSync = true;
        // timeout "post link" after 10 minutes in case the syncs don't complete
        // in time or are never called.
        setTimeout(() => {
            this.stop();
        }, 10 * durations_1.MINUTE);
    }
    stop() {
        this.hasNotFinishedSync = false;
    }
    isActive() {
        return this.hasNotFinishedSync === true;
    }
}
exports.postLinkExperience = new PostLinkExperience();
