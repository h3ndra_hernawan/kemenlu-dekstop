"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCustomColorStyle = void 0;
const Util_1 = require("../types/Util");
const getHSL_1 = require("./getHSL");
const getUserTheme_1 = require("../shims/getUserTheme");
function getCustomColorStyle(color, isQuote = false) {
    if (!color) {
        return undefined;
    }
    const extraQuoteProps = {};
    let adjustedLightness = 0;
    if (isQuote) {
        const theme = (0, getUserTheme_1.getUserTheme)();
        if (theme === Util_1.ThemeType.light) {
            adjustedLightness = 0.6;
        }
        if (theme === Util_1.ThemeType.dark) {
            adjustedLightness = -0.4;
        }
        extraQuoteProps.borderLeftColor = (0, getHSL_1.getHSL)(color.start);
    }
    if (!color.end) {
        return Object.assign(Object.assign({}, extraQuoteProps), { backgroundColor: (0, getHSL_1.getHSL)(color.start, adjustedLightness) });
    }
    return Object.assign(Object.assign({}, extraQuoteProps), { backgroundImage: `linear-gradient(${270 - (color.deg || 0)}deg, ${(0, getHSL_1.getHSL)(color.start, adjustedLightness)}, ${(0, getHSL_1.getHSL)(color.end, adjustedLightness)})` });
}
exports.getCustomColorStyle = getCustomColorStyle;
