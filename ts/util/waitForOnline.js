"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.waitForOnline = void 0;
function waitForOnline(navigator, onlineEventTarget, options = {}) {
    const { timeout } = options;
    return new Promise((resolve, reject) => {
        if (navigator.onLine) {
            resolve();
            return;
        }
        let timeoutId;
        const listener = () => {
            cleanup();
            resolve();
        };
        const cleanup = () => {
            onlineEventTarget.removeEventListener('online', listener);
            if (typeof timeoutId === 'number') {
                clearTimeout(timeoutId);
            }
        };
        onlineEventTarget.addEventListener('online', listener);
        if (timeout !== undefined) {
            timeoutId = setTimeout(() => {
                cleanup();
                reject(new Error('waitForOnline: did not come online in time'));
            }, timeout);
        }
    });
}
exports.waitForOnline = waitForOnline;
