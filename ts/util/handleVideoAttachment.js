"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleVideoAttachment = void 0;
const blob_util_1 = require("blob-util");
const log = __importStar(require("../logging/log"));
const VisualAttachment_1 = require("../types/VisualAttachment");
const MIME_1 = require("../types/MIME");
const fileToBytes_1 = require("./fileToBytes");
async function handleVideoAttachment(file) {
    const objectUrl = URL.createObjectURL(file);
    if (!objectUrl) {
        throw new Error('Failed to create object url for video!');
    }
    try {
        const screenshotContentType = MIME_1.IMAGE_PNG;
        const screenshotBlob = await (0, VisualAttachment_1.makeVideoScreenshot)({
            objectUrl,
            contentType: screenshotContentType,
            logger: log,
        });
        const screenshotData = await (0, blob_util_1.blobToArrayBuffer)(screenshotBlob);
        const data = await (0, fileToBytes_1.fileToBytes)(file);
        return {
            contentType: (0, MIME_1.stringToMIMEType)(file.type),
            data,
            fileName: file.name,
            path: file.name,
            pending: false,
            screenshotContentType,
            screenshotData: new Uint8Array(screenshotData),
            screenshotSize: screenshotData.byteLength,
            size: data.byteLength,
        };
    }
    finally {
        URL.revokeObjectURL(objectUrl);
    }
}
exports.handleVideoAttachment = handleVideoAttachment;
