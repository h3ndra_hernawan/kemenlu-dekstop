"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.normalizeNumber = void 0;
function normalizeNumber(value) {
    if (value === undefined) {
        return undefined;
    }
    if (typeof value === 'number') {
        return value;
    }
    return value.toNumber();
}
exports.normalizeNumber = normalizeNumber;
