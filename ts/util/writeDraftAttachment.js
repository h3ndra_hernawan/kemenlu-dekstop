"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.writeDraftAttachment = void 0;
const lodash_1 = require("lodash");
async function writeDraftAttachment(attachment) {
    if (attachment.pending) {
        throw new Error('writeDraftAttachment: Cannot write pending attachment');
    }
    const path = await window.Signal.Migrations.writeNewDraftData(attachment.data);
    const screenshotPath = attachment.screenshotData
        ? await window.Signal.Migrations.writeNewDraftData(attachment.screenshotData)
        : undefined;
    return Object.assign(Object.assign({}, (0, lodash_1.omit)(attachment, ['data', 'screenshotData'])), { path,
        screenshotPath, pending: false });
}
exports.writeDraftAttachment = writeDraftAttachment;
