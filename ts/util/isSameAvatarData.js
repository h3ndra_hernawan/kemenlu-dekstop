"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isSameAvatarData = void 0;
function isSameAvatarData(a, b) {
    if (!a || !b) {
        return false;
    }
    if (a.buffer && b.buffer) {
        return a.buffer === b.buffer;
    }
    if (a.imagePath && b.imagePath) {
        return a.imagePath === b.imagePath;
    }
    return a.id === b.id;
}
exports.isSameAvatarData = isSameAvatarData;
