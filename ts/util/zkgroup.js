"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deriveProfileKeyCommitment = exports.handleProfileKeyCredential = exports.getClientZkProfileOperations = exports.getClientZkGroupCipher = exports.getClientZkAuthOperations = exports.createProfileKeyCredentialPresentation = exports.getAuthCredentialPresentation = exports.generateProfileKeyCredentialRequest = exports.encryptUuid = exports.encryptGroupBlob = exports.deriveGroupSecretParams = exports.deriveGroupID = exports.deriveGroupPublicParams = exports.deriveProfileKeyVersion = exports.decryptUuid = exports.decryptProfileKey = exports.decryptProfileKeyCredentialPresentation = exports.decryptGroupBlob = void 0;
const zkgroup_1 = require("@signalapp/signal-client/zkgroup");
const UUID_1 = require("../types/UUID");
__exportStar(require("@signalapp/signal-client/zkgroup"), exports);
// Scenarios
function decryptGroupBlob(clientZkGroupCipher, ciphertext) {
    return clientZkGroupCipher.decryptBlob(Buffer.from(ciphertext));
}
exports.decryptGroupBlob = decryptGroupBlob;
function decryptProfileKeyCredentialPresentation(clientZkGroupCipher, presentationBuffer) {
    const presentation = new zkgroup_1.ProfileKeyCredentialPresentation(Buffer.from(presentationBuffer));
    const uuidCiphertext = presentation.getUuidCiphertext();
    const uuid = clientZkGroupCipher.decryptUuid(uuidCiphertext);
    const profileKeyCiphertext = presentation.getProfileKeyCiphertext();
    const profileKey = clientZkGroupCipher.decryptProfileKey(profileKeyCiphertext, uuid);
    return {
        profileKey: profileKey.serialize(),
        uuid: UUID_1.UUID.cast(uuid),
    };
}
exports.decryptProfileKeyCredentialPresentation = decryptProfileKeyCredentialPresentation;
function decryptProfileKey(clientZkGroupCipher, profileKeyCiphertextBuffer, uuid) {
    const profileKeyCiphertext = new zkgroup_1.ProfileKeyCiphertext(Buffer.from(profileKeyCiphertextBuffer));
    const profileKey = clientZkGroupCipher.decryptProfileKey(profileKeyCiphertext, uuid);
    return profileKey.serialize();
}
exports.decryptProfileKey = decryptProfileKey;
function decryptUuid(clientZkGroupCipher, uuidCiphertextBuffer) {
    const uuidCiphertext = new zkgroup_1.UuidCiphertext(Buffer.from(uuidCiphertextBuffer));
    return clientZkGroupCipher.decryptUuid(uuidCiphertext);
}
exports.decryptUuid = decryptUuid;
function deriveProfileKeyVersion(profileKeyBase64, uuid) {
    const profileKeyArray = Buffer.from(profileKeyBase64, 'base64');
    const profileKey = new zkgroup_1.ProfileKey(profileKeyArray);
    const profileKeyVersion = profileKey.getProfileKeyVersion(uuid);
    return profileKeyVersion.toString();
}
exports.deriveProfileKeyVersion = deriveProfileKeyVersion;
function deriveGroupPublicParams(groupSecretParamsBuffer) {
    const groupSecretParams = new zkgroup_1.GroupSecretParams(Buffer.from(groupSecretParamsBuffer));
    return groupSecretParams.getPublicParams().serialize();
}
exports.deriveGroupPublicParams = deriveGroupPublicParams;
function deriveGroupID(groupSecretParamsBuffer) {
    const groupSecretParams = new zkgroup_1.GroupSecretParams(Buffer.from(groupSecretParamsBuffer));
    return groupSecretParams.getPublicParams().getGroupIdentifier().serialize();
}
exports.deriveGroupID = deriveGroupID;
function deriveGroupSecretParams(masterKeyBuffer) {
    const masterKey = new zkgroup_1.GroupMasterKey(Buffer.from(masterKeyBuffer));
    const groupSecretParams = zkgroup_1.GroupSecretParams.deriveFromMasterKey(masterKey);
    return groupSecretParams.serialize();
}
exports.deriveGroupSecretParams = deriveGroupSecretParams;
function encryptGroupBlob(clientZkGroupCipher, plaintext) {
    return clientZkGroupCipher.encryptBlob(Buffer.from(plaintext));
}
exports.encryptGroupBlob = encryptGroupBlob;
function encryptUuid(clientZkGroupCipher, uuidPlaintext) {
    const uuidCiphertext = clientZkGroupCipher.encryptUuid(uuidPlaintext);
    return uuidCiphertext.serialize();
}
exports.encryptUuid = encryptUuid;
function generateProfileKeyCredentialRequest(clientZkProfileCipher, uuid, profileKeyBase64) {
    const profileKeyArray = Buffer.from(profileKeyBase64, 'base64');
    const profileKey = new zkgroup_1.ProfileKey(profileKeyArray);
    const context = clientZkProfileCipher.createProfileKeyCredentialRequestContext(uuid, profileKey);
    const request = context.getRequest();
    const requestArray = request.serialize();
    return {
        context,
        requestHex: requestArray.toString('hex'),
    };
}
exports.generateProfileKeyCredentialRequest = generateProfileKeyCredentialRequest;
function getAuthCredentialPresentation(clientZkAuthOperations, authCredentialBase64, groupSecretParamsBase64) {
    const authCredential = new zkgroup_1.AuthCredential(Buffer.from(authCredentialBase64, 'base64'));
    const secretParams = new zkgroup_1.GroupSecretParams(Buffer.from(groupSecretParamsBase64, 'base64'));
    const presentation = clientZkAuthOperations.createAuthCredentialPresentation(secretParams, authCredential);
    return presentation.serialize();
}
exports.getAuthCredentialPresentation = getAuthCredentialPresentation;
function createProfileKeyCredentialPresentation(clientZkProfileCipher, profileKeyCredentialBase64, groupSecretParamsBase64) {
    const profileKeyCredentialArray = Buffer.from(profileKeyCredentialBase64, 'base64');
    const profileKeyCredential = new zkgroup_1.ProfileKeyCredential(profileKeyCredentialArray);
    const secretParams = new zkgroup_1.GroupSecretParams(Buffer.from(groupSecretParamsBase64, 'base64'));
    const presentation = clientZkProfileCipher.createProfileKeyCredentialPresentation(secretParams, profileKeyCredential);
    return presentation.serialize();
}
exports.createProfileKeyCredentialPresentation = createProfileKeyCredentialPresentation;
function getClientZkAuthOperations(serverPublicParamsBase64) {
    const serverPublicParams = new zkgroup_1.ServerPublicParams(Buffer.from(serverPublicParamsBase64, 'base64'));
    return new zkgroup_1.ClientZkAuthOperations(serverPublicParams);
}
exports.getClientZkAuthOperations = getClientZkAuthOperations;
function getClientZkGroupCipher(groupSecretParamsBase64) {
    const serverPublicParams = new zkgroup_1.GroupSecretParams(Buffer.from(groupSecretParamsBase64, 'base64'));
    return new zkgroup_1.ClientZkGroupCipher(serverPublicParams);
}
exports.getClientZkGroupCipher = getClientZkGroupCipher;
function getClientZkProfileOperations(serverPublicParamsBase64) {
    const serverPublicParams = new zkgroup_1.ServerPublicParams(Buffer.from(serverPublicParamsBase64, 'base64'));
    return new zkgroup_1.ClientZkProfileOperations(serverPublicParams);
}
exports.getClientZkProfileOperations = getClientZkProfileOperations;
function handleProfileKeyCredential(clientZkProfileCipher, context, responseBase64) {
    const response = new zkgroup_1.ProfileKeyCredentialResponse(Buffer.from(responseBase64, 'base64'));
    const profileKeyCredential = clientZkProfileCipher.receiveProfileKeyCredential(context, response);
    const credentialArray = profileKeyCredential.serialize();
    return credentialArray.toString('base64');
}
exports.handleProfileKeyCredential = handleProfileKeyCredential;
function deriveProfileKeyCommitment(profileKeyBase64, uuid) {
    const profileKeyArray = Buffer.from(profileKeyBase64, 'base64');
    const profileKey = new zkgroup_1.ProfileKey(profileKeyArray);
    return profileKey.getCommitment(uuid).contents.toString('base64');
}
exports.deriveProfileKeyCommitment = deriveProfileKeyCommitment;
