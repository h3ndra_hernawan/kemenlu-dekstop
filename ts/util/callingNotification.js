"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCallingIcon = exports.getCallingNotificationText = void 0;
const Calling_1 = require("../types/Calling");
const missingCaseError_1 = require("./missingCaseError");
const log = __importStar(require("../logging/log"));
function getDirectCallNotificationText({ wasIncoming, wasVideoCall, wasDeclined, acceptedTime, }, i18n) {
    const wasAccepted = Boolean(acceptedTime);
    if (wasIncoming) {
        if (wasDeclined) {
            if (wasVideoCall) {
                return i18n('declinedIncomingVideoCall');
            }
            return i18n('declinedIncomingAudioCall');
        }
        if (wasAccepted) {
            if (wasVideoCall) {
                return i18n('acceptedIncomingVideoCall');
            }
            return i18n('acceptedIncomingAudioCall');
        }
        if (wasVideoCall) {
            return i18n('missedIncomingVideoCall');
        }
        return i18n('missedIncomingAudioCall');
    }
    if (wasAccepted) {
        if (wasVideoCall) {
            return i18n('acceptedOutgoingVideoCall');
        }
        return i18n('acceptedOutgoingAudioCall');
    }
    if (wasVideoCall) {
        return i18n('missedOrDeclinedOutgoingVideoCall');
    }
    return i18n('missedOrDeclinedOutgoingAudioCall');
}
function getGroupCallNotificationText(notification, i18n) {
    if (notification.ended) {
        return i18n('calling__call-notification__ended');
    }
    if (!notification.creator) {
        return i18n('calling__call-notification__started-by-someone');
    }
    if (notification.creator.isMe) {
        return i18n('calling__call-notification__started-by-you');
    }
    return i18n('calling__call-notification__started', [
        notification.creator.firstName || notification.creator.title,
    ]);
}
function getCallingNotificationText(notification, i18n) {
    switch (notification.callMode) {
        case Calling_1.CallMode.Direct:
            return getDirectCallNotificationText(notification, i18n);
        case Calling_1.CallMode.Group:
            return getGroupCallNotificationText(notification, i18n);
        default:
            log.error(`getCallingNotificationText: missing case ${(0, missingCaseError_1.missingCaseError)(notification)}`);
            return '';
    }
}
exports.getCallingNotificationText = getCallingNotificationText;
function getDirectCallingIcon({ wasIncoming, wasVideoCall, acceptedTime, }) {
    const wasAccepted = Boolean(acceptedTime);
    // video
    if (wasVideoCall) {
        if (wasAccepted) {
            return wasIncoming ? 'video-incoming' : 'video-outgoing';
        }
        return 'video-missed';
    }
    if (wasAccepted) {
        return wasIncoming ? 'audio-incoming' : 'audio-outgoing';
    }
    return 'audio-missed';
}
function getCallingIcon(notification) {
    switch (notification.callMode) {
        case Calling_1.CallMode.Direct:
            return getDirectCallingIcon(notification);
        case Calling_1.CallMode.Group:
            return 'video';
        default:
            log.error(`getCallingNotificationText: missing case ${(0, missingCaseError_1.missingCaseError)(notification)}`);
            return 'phone';
    }
}
exports.getCallingIcon = getCallingIcon;
