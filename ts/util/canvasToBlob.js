"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.canvasToBlob = void 0;
const MIME_1 = require("../types/MIME");
/**
 * Similar to [the built-in `toBlob` method][0], but returns a Promise.
 *
 * [0]: https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toBlob
 */
async function canvasToBlob(canvas, mimeType = MIME_1.IMAGE_JPEG, quality) {
    return new Promise((resolve, reject) => canvas.toBlob(result => {
        if (result) {
            resolve(result);
        }
        else {
            reject(new Error("Couldn't convert the canvas to a Blob"));
        }
    }, mimeType, quality));
}
exports.canvasToBlob = canvasToBlob;
