"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initializeNetworkObserver = void 0;
const socketStatus_1 = require("../shims/socketStatus");
const log = __importStar(require("../logging/log"));
const durations_1 = require("../util/durations");
function initializeNetworkObserver(networkActions) {
    log.info('Initializing network observer');
    const refresh = () => {
        networkActions.checkNetworkStatus({
            isOnline: navigator.onLine,
            socketStatus: (0, socketStatus_1.getSocketStatus)(),
        });
    };
    window.Whisper.events.on('socketStatusChange', refresh);
    window.addEventListener('online', refresh);
    window.addEventListener('offline', refresh);
    window.setTimeout(() => {
        networkActions.closeConnectingGracePeriod();
    }, 5 * durations_1.SECOND);
}
exports.initializeNetworkObserver = initializeNetworkObserver;
