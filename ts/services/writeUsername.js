"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.writeUsername = void 0;
const Client_1 = __importDefault(require("../sql/Client"));
const handleMessageSend_1 = require("../util/handleMessageSend");
async function writeUsername({ username, previousUsername, }) {
    const me = window.ConversationController.getOurConversationOrThrow();
    await me.getProfiles();
    if (me.get('username') !== previousUsername) {
        throw new Error('Username has changed on another device');
    }
    if (username) {
        await window.textsecure.messaging.putUsername(username);
    }
    else {
        await window.textsecure.messaging.deleteUsername();
    }
    // Update backbone, update DB, then tell linked devices about profile update
    me.set({
        username,
    });
    Client_1.default.updateConversation(me.attributes);
    await (0, handleMessageSend_1.handleMessageSend)(window.textsecure.messaging.sendFetchLocalProfileSyncMessage(), { messageIds: [], sendType: 'otherSync' });
}
exports.writeUsername = writeUsername;
