"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sortCredentials = exports.getDatesForRequest = exports.maybeFetchNewCredentials = exports.getCredentialsForToday = exports.runWithRetry = exports.initializeGroupCredentialFetcher = exports.GROUP_CREDENTIALS_KEY = void 0;
const lodash_1 = require("lodash");
const zkgroup_1 = require("@signalapp/signal-client/zkgroup");
const zkgroup_2 = require("../util/zkgroup");
const durations = __importStar(require("../util/durations"));
const BackOff_1 = require("../util/BackOff");
const sleep_1 = require("../util/sleep");
const log = __importStar(require("../logging/log"));
exports.GROUP_CREDENTIALS_KEY = 'groupCredentials';
function getTodayInEpoch() {
    return Math.floor(Date.now() / durations.DAY);
}
let started = false;
async function initializeGroupCredentialFetcher() {
    if (started) {
        return;
    }
    log.info('initializeGroupCredentialFetcher: starting...');
    started = true;
    // Because we fetch eight days of credentials at a time, we really only need to run
    //   this about once a week. But there's no problem running it more often; it will do
    //   nothing if no new credentials are needed, and will only request needed credentials.
    await runWithRetry(maybeFetchNewCredentials, {
        scheduleAnother: 4 * durations.HOUR,
    });
}
exports.initializeGroupCredentialFetcher = initializeGroupCredentialFetcher;
const BACKOFF_TIMEOUTS = [
    durations.SECOND,
    5 * durations.SECOND,
    30 * durations.SECOND,
    2 * durations.MINUTE,
    5 * durations.MINUTE,
];
async function runWithRetry(fn, options = {}) {
    const backOff = new BackOff_1.BackOff(BACKOFF_TIMEOUTS);
    // eslint-disable-next-line no-constant-condition
    while (true) {
        try {
            // eslint-disable-next-line no-await-in-loop
            await fn();
            return;
        }
        catch (error) {
            const wait = backOff.getAndIncrement();
            log.info(`runWithRetry: ${fn.name} failed. Waiting ${wait}ms for retry. Error: ${error.stack}`);
            // eslint-disable-next-line no-await-in-loop
            await (0, sleep_1.sleep)(wait);
        }
    }
    // It's important to schedule our next run here instead of the level above; otherwise we
    //   could end up with multiple endlessly-retrying runs.
    const duration = options.scheduleAnother;
    if (duration) {
        log.info(`runWithRetry: scheduling another run with a setTimeout duration of ${duration}ms`);
        setTimeout(async () => runWithRetry(fn, options), duration);
    }
}
exports.runWithRetry = runWithRetry;
// In cases where we are at a day boundary, we might need to use tomorrow in a retry
function getCredentialsForToday(data) {
    if (!data) {
        throw new Error('getCredentialsForToday: No credentials fetched!');
    }
    const todayInEpoch = getTodayInEpoch();
    const todayIndex = data.findIndex((item) => item.redemptionTime === todayInEpoch);
    if (todayIndex < 0) {
        throw new Error('getCredentialsForToday: Cannot find credentials for today');
    }
    return {
        today: data[todayIndex],
        tomorrow: data[todayIndex + 1],
    };
}
exports.getCredentialsForToday = getCredentialsForToday;
async function maybeFetchNewCredentials() {
    var _a;
    const uuid = (_a = window.textsecure.storage.user.getUuid()) === null || _a === void 0 ? void 0 : _a.toString();
    if (!uuid) {
        log.info('maybeFetchCredentials: no UUID, returning early');
        return;
    }
    const previous = window.storage.get(exports.GROUP_CREDENTIALS_KEY);
    const requestDates = getDatesForRequest(previous);
    if (!requestDates) {
        log.info('maybeFetchCredentials: no new credentials needed');
        return;
    }
    const accountManager = window.getAccountManager();
    if (!accountManager) {
        log.info('maybeFetchCredentials: unable to get AccountManager');
        return;
    }
    const { startDay, endDay } = requestDates;
    log.info(`maybeFetchCredentials: fetching credentials for ${startDay} through ${endDay}`);
    const serverPublicParamsBase64 = window.getServerPublicParams();
    const clientZKAuthOperations = (0, zkgroup_2.getClientZkAuthOperations)(serverPublicParamsBase64);
    const newCredentials = sortCredentials(await accountManager.getGroupCredentials(startDay, endDay)).map((item) => {
        const authCredential = clientZKAuthOperations.receiveAuthCredential(uuid, item.redemptionTime, new zkgroup_1.AuthCredentialResponse(Buffer.from(item.credential, 'base64')));
        const credential = authCredential.serialize().toString('base64');
        return {
            redemptionTime: item.redemptionTime,
            credential,
        };
    });
    const todayInEpoch = getTodayInEpoch();
    const previousCleaned = previous
        ? previous.filter((item) => item.redemptionTime >= todayInEpoch)
        : [];
    const finalCredentials = [...previousCleaned, ...newCredentials];
    log.info('maybeFetchCredentials: Saving new credentials...');
    // Note: we don't wait for this to finish
    window.storage.put(exports.GROUP_CREDENTIALS_KEY, finalCredentials);
    log.info('maybeFetchCredentials: Save complete.');
}
exports.maybeFetchNewCredentials = maybeFetchNewCredentials;
function getDatesForRequest(data) {
    const todayInEpoch = getTodayInEpoch();
    const oneWeekOut = todayInEpoch + 7;
    const lastCredential = (0, lodash_1.last)(data);
    if (!lastCredential || lastCredential.redemptionTime < todayInEpoch) {
        return {
            startDay: todayInEpoch,
            endDay: oneWeekOut,
        };
    }
    if (lastCredential.redemptionTime >= oneWeekOut) {
        return undefined;
    }
    return {
        startDay: lastCredential.redemptionTime + 1,
        endDay: oneWeekOut,
    };
}
exports.getDatesForRequest = getDatesForRequest;
function sortCredentials(data) {
    return (0, lodash_1.sortBy)(data, (item) => item.redemptionTime);
}
exports.sortCredentials = sortCredentials;
