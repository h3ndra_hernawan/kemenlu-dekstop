"use strict";
// Copyright 2015-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.notificationService = exports.FALLBACK_NOTIFICATION_TITLE = exports.NotificationSetting = void 0;
const lodash_1 = require("lodash");
const events_1 = __importDefault(require("events"));
const Sound_1 = require("../util/Sound");
const Settings_1 = require("../types/Settings");
const OS = __importStar(require("../OS"));
const log = __importStar(require("../logging/log"));
const enum_1 = require("../util/enum");
const missingCaseError_1 = require("../util/missingCaseError");
// The keys and values don't match here. This is because the values correspond to old
//   setting names. In the future, we may wish to migrate these to match.
var NotificationSetting;
(function (NotificationSetting) {
    NotificationSetting["Off"] = "off";
    NotificationSetting["NoNameOrMessage"] = "count";
    NotificationSetting["NameOnly"] = "name";
    NotificationSetting["NameAndMessage"] = "message";
})(NotificationSetting = exports.NotificationSetting || (exports.NotificationSetting = {}));
const parseNotificationSetting = (0, enum_1.makeEnumParser)(NotificationSetting, NotificationSetting.NameAndMessage);
exports.FALLBACK_NOTIFICATION_TITLE = 'Signal';
// Electron, at least on Windows and macOS, only shows one notification at a time (see
//   issues [#15364][0] and [#21646][1], among others). Because of that, we have a
//   single slot for notifications, and once a notification is dismissed, all of
//   Signal's notifications are dismissed.
// [0]: https://github.com/electron/electron/issues/15364
// [1]: https://github.com/electron/electron/issues/21646
class NotificationService extends events_1.default {
    constructor() {
        super();
        this.isEnabled = false;
        this.lastNotification = null;
        this.notificationData = null;
        this.update = (0, lodash_1.debounce)(this.fastUpdate.bind(this), 1000);
    }
    initialize({ i18n, storage, }) {
        log.info('NotificationService initialized');
        this.i18n = i18n;
        this.storage = storage;
    }
    getStorage() {
        if (this.storage) {
            return this.storage;
        }
        log.error('NotificationService not initialized. Falling back to window.storage, but you should fix this');
        return window.storage;
    }
    getI18n() {
        if (this.i18n) {
            return this.i18n;
        }
        log.error('NotificationService not initialized. Falling back to window.i18n, but you should fix this');
        return window.i18n;
    }
    /**
     * A higher-level wrapper around `window.Notification`. You may prefer to use `notify`,
     * which doesn't check permissions, do any filtering, etc.
     */
    add(notificationData) {
        log.info('NotificationService: adding a notification and requesting an update');
        this.notificationData = notificationData;
        this.update();
    }
    /**
     * A lower-level wrapper around `window.Notification`. You may prefer to use `add`,
     * which includes debouncing and user permission logic.
     */
    notify({ icon, message, onNotificationClick, silent, title, }) {
        var _a;
        log.info('NotificationService: showing a notification');
        (_a = this.lastNotification) === null || _a === void 0 ? void 0 : _a.close();
        const audioNotificationSupport = (0, Settings_1.getAudioNotificationSupport)();
        const notification = new window.Notification(title, {
            body: OS.isLinux() ? filterNotificationText(message) : message,
            icon,
            silent: silent || audioNotificationSupport !== Settings_1.AudioNotificationSupport.Native,
        });
        notification.onclick = onNotificationClick;
        if (!silent &&
            audioNotificationSupport === Settings_1.AudioNotificationSupport.Custom) {
            // We kick off the sound to be played. No need to await it.
            new Sound_1.Sound({ src: 'sounds/notification.ogg' }).play();
        }
        this.lastNotification = notification;
    }
    // Remove the last notification if both conditions hold:
    //
    // 1. Either `conversationId` or `messageId` matches (if present)
    // 2. `emoji`, `targetAuthorUuid`, `targetTimestamp` matches (if present)
    removeBy({ conversationId, messageId, emoji, targetAuthorUuid, targetTimestamp, }) {
        if (!this.notificationData) {
            log.info('NotificationService#removeBy: no notification data');
            return;
        }
        let shouldClear = false;
        if (conversationId &&
            this.notificationData.conversationId === conversationId) {
            log.info('NotificationService#removeBy: conversation ID matches');
            shouldClear = true;
        }
        if (messageId && this.notificationData.messageId === messageId) {
            log.info('NotificationService#removeBy: message ID matches');
            shouldClear = true;
        }
        if (!shouldClear) {
            return;
        }
        const { reaction } = this.notificationData;
        if (reaction &&
            emoji &&
            targetAuthorUuid &&
            targetTimestamp &&
            (reaction.emoji !== emoji ||
                reaction.targetAuthorUuid !== targetAuthorUuid ||
                reaction.targetTimestamp !== targetTimestamp)) {
            return;
        }
        this.clear();
        this.update();
    }
    fastUpdate() {
        const storage = this.getStorage();
        const i18n = this.getI18n();
        if (this.lastNotification) {
            this.lastNotification.close();
            this.lastNotification = null;
        }
        const { notificationData } = this;
        const isAppFocused = window.isActive();
        const userSetting = this.getNotificationSetting();
        // This isn't a boolean because TypeScript isn't smart enough to know that, if
        //   `Boolean(notificationData)` is true, `notificationData` is truthy.
        const shouldShowNotification = this.isEnabled && !isAppFocused && notificationData;
        if (!shouldShowNotification) {
            log.info(`NotificationService not updating notifications. Notifications are ${this.isEnabled ? 'enabled' : 'disabled'}; app is ${isAppFocused ? '' : 'not '}focused; there is ${notificationData ? '' : 'no '}notification data`);
            if (isAppFocused) {
                this.notificationData = null;
            }
            return;
        }
        const shouldPlayNotificationSound = Boolean(storage.get('audio-notification'));
        const shouldDrawAttention = storage.get('notification-draw-attention', true);
        if (shouldDrawAttention) {
            log.info('NotificationService: drawing attention');
            window.drawAttention();
        }
        let notificationTitle;
        let notificationMessage;
        let notificationIconUrl;
        const { conversationId, messageId, senderTitle, message, isExpiringMessage, reaction, } = notificationData;
        switch (userSetting) {
            case NotificationSetting.Off:
                log.info('NotificationService not showing a notification because user has disabled it');
                return;
            case NotificationSetting.NameOnly:
            case NotificationSetting.NameAndMessage: {
                notificationTitle = senderTitle;
                ({ notificationIconUrl } = notificationData);
                const shouldHideExpiringMessageBody = isExpiringMessage && (OS.isMacOS() || OS.isWindows());
                if (shouldHideExpiringMessageBody) {
                    notificationMessage = i18n('newMessage');
                }
                else if (userSetting === NotificationSetting.NameOnly) {
                    if (reaction) {
                        notificationMessage = i18n('notificationReaction', {
                            sender: senderTitle,
                            emoji: reaction.emoji,
                        });
                    }
                    else {
                        notificationMessage = i18n('newMessage');
                    }
                }
                else if (reaction) {
                    notificationMessage = i18n('notificationReactionMessage', {
                        sender: senderTitle,
                        emoji: reaction.emoji,
                        message,
                    });
                }
                else {
                    notificationMessage = message;
                }
                break;
            }
            case NotificationSetting.NoNameOrMessage:
                notificationTitle = exports.FALLBACK_NOTIFICATION_TITLE;
                notificationMessage = i18n('newMessage');
                break;
            default:
                log.error((0, missingCaseError_1.missingCaseError)(userSetting));
                notificationTitle = exports.FALLBACK_NOTIFICATION_TITLE;
                notificationMessage = i18n('newMessage');
                break;
        }
        log.info('NotificationService: requesting a notification to be shown');
        this.notify({
            title: notificationTitle,
            icon: notificationIconUrl,
            message: notificationMessage,
            silent: !shouldPlayNotificationSound,
            onNotificationClick: () => {
                this.emit('click', conversationId, messageId);
            },
        });
    }
    getNotificationSetting() {
        return parseNotificationSetting(this.getStorage().get('notification-setting'));
    }
    clear() {
        log.info('NotificationService: clearing notification and requesting an update');
        this.notificationData = null;
        this.update();
    }
    // We don't usually call this, but when the process is shutting down, we should at
    //   least try to remove the notification immediately instead of waiting for the
    //   normal debounce.
    fastClear() {
        log.info('NotificationService: clearing notification and updating');
        this.notificationData = null;
        this.fastUpdate();
    }
    enable() {
        log.info('NotificationService enabling');
        const needUpdate = !this.isEnabled;
        this.isEnabled = true;
        if (needUpdate) {
            this.update();
        }
    }
    disable() {
        log.info('NotificationService disabling');
        this.isEnabled = false;
    }
}
exports.notificationService = new NotificationService();
function filterNotificationText(text) {
    return (text || '')
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&apos;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}
