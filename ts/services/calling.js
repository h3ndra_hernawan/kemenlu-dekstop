"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.calling = exports.CallingClass = void 0;
const electron_1 = require("electron");
const ringrtc_1 = require("ringrtc");
const lodash_1 = require("lodash");
const conversations_1 = require("../state/ducks/conversations");
const isConversationTooBigToRing_1 = require("../conversations/isConversationTooBigToRing");
const whatTypeOfConversation_1 = require("../util/whatTypeOfConversation");
const Calling_1 = require("../types/Calling");
const audioDeviceModule_1 = require("../calling/audioDeviceModule");
const findBestMatchingDevice_1 = require("../calling/findBestMatchingDevice");
const UUID_1 = require("../types/UUID");
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const dropNull_1 = require("../util/dropNull");
const getOwn_1 = require("../util/getOwn");
const isNormalNumber_1 = require("../util/isNormalNumber");
const durations = __importStar(require("../util/durations"));
const handleMessageSend_1 = require("../util/handleMessageSend");
const groups_1 = require("../groups");
const missingCaseError_1 = require("../util/missingCaseError");
const normalizeGroupCallTimestamp_1 = require("../util/ringrtc/normalizeGroupCallTimestamp");
const constants_1 = require("../calling/constants");
const callingMessageToProto_1 = require("../util/callingMessageToProto");
const getSendOptions_1 = require("../util/getSendOptions");
const requestMicrophonePermissions_1 = require("../util/requestMicrophonePermissions");
const protobuf_1 = require("../protobuf");
const Client_1 = __importDefault(require("../sql/Client"));
const notifications_1 = require("./notifications");
const log = __importStar(require("../logging/log"));
const { processGroupCallRingRequest, processGroupCallRingCancelation, cleanExpiredGroupCallRings, } = Client_1.default;
const RINGRTC_HTTP_METHOD_TO_OUR_HTTP_METHOD = new Map([
    [ringrtc_1.HttpMethod.Get, 'GET'],
    [ringrtc_1.HttpMethod.Put, 'PUT'],
    [ringrtc_1.HttpMethod.Post, 'POST'],
    [ringrtc_1.HttpMethod.Delete, 'DELETE'],
]);
const CLEAN_EXPIRED_GROUP_CALL_RINGS_INTERVAL = 10 * durations.MINUTE;
// We send group call update messages to tell other clients to peek, which triggers
//   notifications, timeline messages, big green "Join" buttons, and so on. This enum
//   represents the three possible states we can be in. This helps ensure that we don't
//   send an update on disconnect if we never sent one when we joined.
var GroupCallUpdateMessageState;
(function (GroupCallUpdateMessageState) {
    GroupCallUpdateMessageState[GroupCallUpdateMessageState["SentNothing"] = 0] = "SentNothing";
    GroupCallUpdateMessageState[GroupCallUpdateMessageState["SentJoin"] = 1] = "SentJoin";
    GroupCallUpdateMessageState[GroupCallUpdateMessageState["SentLeft"] = 2] = "SentLeft";
})(GroupCallUpdateMessageState || (GroupCallUpdateMessageState = {}));
function isScreenSource(source) {
    return source.id.startsWith('screen');
}
function translateSourceName(i18n, source) {
    const { name } = source;
    if (!isScreenSource(source)) {
        return name;
    }
    if (name === 'Entire Screen') {
        return i18n('calling__SelectPresentingSourcesModal--entireScreen');
    }
    const match = name.match(/^Screen (\d+)$/);
    if (match) {
        return i18n('calling__SelectPresentingSourcesModal--screen', {
            id: match[1],
        });
    }
    return name;
}
function protoToCallingMessage({ offer, answer, iceCandidates, legacyHangup, busy, hangup, supportsMultiRing, destinationDeviceId, opaque, }) {
    return {
        offer: offer
            ? Object.assign(Object.assign({}, (0, dropNull_1.shallowDropNull)(offer)), { type: (0, dropNull_1.dropNull)(offer.type), opaque: offer.opaque ? Buffer.from(offer.opaque) : undefined }) : undefined,
        answer: answer
            ? Object.assign(Object.assign({}, (0, dropNull_1.shallowDropNull)(answer)), { opaque: answer.opaque ? Buffer.from(answer.opaque) : undefined }) : undefined,
        iceCandidates: iceCandidates
            ? iceCandidates.map(candidate => {
                return Object.assign(Object.assign({}, (0, dropNull_1.shallowDropNull)(candidate)), { opaque: candidate.opaque
                        ? Buffer.from(candidate.opaque)
                        : undefined });
            })
            : undefined,
        legacyHangup: legacyHangup
            ? Object.assign(Object.assign({}, (0, dropNull_1.shallowDropNull)(legacyHangup)), { type: (0, dropNull_1.dropNull)(legacyHangup.type) }) : undefined,
        busy: (0, dropNull_1.shallowDropNull)(busy),
        hangup: hangup
            ? Object.assign(Object.assign({}, (0, dropNull_1.shallowDropNull)(hangup)), { type: (0, dropNull_1.dropNull)(hangup.type) }) : undefined,
        supportsMultiRing: (0, dropNull_1.dropNull)(supportsMultiRing),
        destinationDeviceId: (0, dropNull_1.dropNull)(destinationDeviceId),
        opaque: opaque
            ? {
                data: opaque.data ? Buffer.from(opaque.data) : undefined,
            }
            : undefined,
    };
}
class CallingClass {
    constructor() {
        this.videoCapturer = new ringrtc_1.GumVideoCapturer({
            maxWidth: constants_1.REQUESTED_VIDEO_WIDTH,
            maxHeight: constants_1.REQUESTED_VIDEO_HEIGHT,
            maxFramerate: constants_1.REQUESTED_VIDEO_FRAMERATE,
        });
        this.videoRenderer = new ringrtc_1.CanvasVideoRenderer();
        this.callsByConversation = {};
    }
    initialize(uxActions, sfuUrl) {
        this.uxActions = uxActions;
        if (!uxActions) {
            throw new Error('CallingClass.initialize: Invalid uxActions.');
        }
        this.sfuUrl = sfuUrl;
        this.previousAudioDeviceModule = (0, audioDeviceModule_1.parseAudioDeviceModule)(window.storage.get('previousAudioDeviceModule'));
        this.currentAudioDeviceModule = (0, audioDeviceModule_1.getAudioDeviceModule)();
        window.storage.put('previousAudioDeviceModule', this.currentAudioDeviceModule);
        ringrtc_1.RingRTC.setConfig({
            use_new_audio_device_module: this.currentAudioDeviceModule === audioDeviceModule_1.AudioDeviceModule.WindowsAdm2,
        });
        ringrtc_1.RingRTC.handleOutgoingSignaling = this.handleOutgoingSignaling.bind(this);
        ringrtc_1.RingRTC.handleIncomingCall = this.handleIncomingCall.bind(this);
        ringrtc_1.RingRTC.handleAutoEndedIncomingCallRequest =
            this.handleAutoEndedIncomingCallRequest.bind(this);
        ringrtc_1.RingRTC.handleLogMessage = this.handleLogMessage.bind(this);
        ringrtc_1.RingRTC.handleSendHttpRequest = this.handleSendHttpRequest.bind(this);
        ringrtc_1.RingRTC.handleSendCallMessage = this.handleSendCallMessage.bind(this);
        ringrtc_1.RingRTC.handleSendCallMessageToGroup =
            this.handleSendCallMessageToGroup.bind(this);
        ringrtc_1.RingRTC.handleGroupCallRingUpdate =
            this.handleGroupCallRingUpdate.bind(this);
        this.attemptToGiveOurUuidToRingRtc();
        window.Whisper.events.on('userChanged', () => {
            this.attemptToGiveOurUuidToRingRtc();
        });
        electron_1.ipcRenderer.on('stop-screen-share', () => {
            uxActions.setPresenting();
        });
        this.cleanExpiredGroupCallRingsAndLoop();
    }
    attemptToGiveOurUuidToRingRtc() {
        var _a;
        const ourUuid = (_a = window.textsecure.storage.user.getUuid()) === null || _a === void 0 ? void 0 : _a.toString();
        if (!ourUuid) {
            // This can happen if we're not linked. It's okay if we hit this case.
            return;
        }
        ringrtc_1.RingRTC.setSelfUuid(Buffer.from((0, Crypto_1.uuidToBytes)(ourUuid)));
    }
    async startCallingLobby(conversationId, isVideoCall) {
        log.info('CallingClass.startCallingLobby()');
        const conversation = window.ConversationController.get(conversationId);
        if (!conversation) {
            log.error('Could not find conversation, cannot start call lobby');
            return;
        }
        const conversationProps = conversation.format();
        const callMode = (0, conversations_1.getConversationCallMode)(conversationProps);
        switch (callMode) {
            case Calling_1.CallMode.None:
                log.error('Conversation does not support calls, new call not allowed.');
                return;
            case Calling_1.CallMode.Direct:
                if (!this.getRemoteUserIdFromConversation(conversation)) {
                    log.error('Missing remote user identifier, new call not allowed.');
                    return;
                }
                break;
            case Calling_1.CallMode.Group:
                break;
            default:
                throw (0, missingCaseError_1.missingCaseError)(callMode);
        }
        if (!this.uxActions) {
            log.error('Missing uxActions, new call not allowed.');
            return;
        }
        if (!this.localDeviceId) {
            log.error('Missing local device identifier, new call not allowed.');
            return;
        }
        const haveMediaPermissions = await this.requestPermissions(isVideoCall);
        if (!haveMediaPermissions) {
            log.info('Permissions were denied, new call not allowed.');
            return;
        }
        log.info('CallingClass.startCallingLobby(): Starting lobby');
        // It's important that this function comes before any calls to
        //   `videoCapturer.enableCapture` or `videoCapturer.enableCaptureAndSend` because of
        //   a small RingRTC bug.
        //
        // If we tell RingRTC to start capturing video (with those methods or with
        //   `RingRTC.setPreferredDevice`, which also captures video) multiple times in quick
        //   succession, it will call the asynchronous `getUserMedia` twice. It'll save the
        //   results in the same variable, which means the first call can be overridden.
        //   Later, when we try to turn the camera off, we'll only disable the *second* result
        //   of `getUserMedia` and the camera will stay on.
        //
        // We get around this by `await`ing, making sure we're all done with `getUserMedia`,
        //   and then continuing.
        //
        // We should be able to move this below `this.connectGroupCall` once that RingRTC bug
        //   is fixed. See DESKTOP-1032.
        await this.startDeviceReselectionTimer();
        switch (callMode) {
            case Calling_1.CallMode.Direct:
                this.uxActions.showCallLobby({
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: conversationProps.id,
                    hasLocalAudio: true,
                    hasLocalVideo: isVideoCall,
                });
                break;
            case Calling_1.CallMode.Group: {
                if (!conversationProps.groupId ||
                    !conversationProps.publicParams ||
                    !conversationProps.secretParams) {
                    log.error('Conversation is missing required parameters. Cannot connect group call');
                    return;
                }
                const groupCall = this.connectGroupCall(conversationProps.id, {
                    groupId: conversationProps.groupId,
                    publicParams: conversationProps.publicParams,
                    secretParams: conversationProps.secretParams,
                });
                groupCall.setOutgoingAudioMuted(false);
                groupCall.setOutgoingVideoMuted(!isVideoCall);
                this.uxActions.showCallLobby(Object.assign({ callMode: Calling_1.CallMode.Group, conversationId: conversationProps.id, isConversationTooBigToRing: (0, isConversationTooBigToRing_1.isConversationTooBigToRing)(conversationProps) }, this.formatGroupCallForRedux(groupCall)));
                break;
            }
            default:
                throw (0, missingCaseError_1.missingCaseError)(callMode);
        }
        if (isVideoCall) {
            this.enableLocalCamera();
        }
    }
    stopCallingLobby(conversationId) {
        var _a;
        this.disableLocalVideo();
        this.stopDeviceReselectionTimer();
        this.lastMediaDeviceSettings = undefined;
        if (conversationId) {
            (_a = this.getGroupCall(conversationId)) === null || _a === void 0 ? void 0 : _a.disconnect();
        }
    }
    async startOutgoingDirectCall(conversationId, hasLocalAudio, hasLocalVideo) {
        log.info('CallingClass.startOutgoingDirectCall()');
        if (!this.uxActions) {
            throw new Error('Redux actions not available');
        }
        const conversation = window.ConversationController.get(conversationId);
        if (!conversation) {
            log.error('Could not find conversation, cannot start call');
            this.stopCallingLobby();
            return;
        }
        const remoteUserId = this.getRemoteUserIdFromConversation(conversation);
        if (!remoteUserId || !this.localDeviceId) {
            log.error('Missing identifier, new call not allowed.');
            this.stopCallingLobby();
            return;
        }
        const haveMediaPermissions = await this.requestPermissions(hasLocalVideo);
        if (!haveMediaPermissions) {
            log.info('Permissions were denied, new call not allowed.');
            this.stopCallingLobby();
            return;
        }
        log.info('CallingClass.startOutgoingDirectCall(): Getting call settings');
        const callSettings = await this.getCallSettings(conversation);
        // Check state after awaiting to debounce call button.
        if (ringrtc_1.RingRTC.call && ringrtc_1.RingRTC.call.state !== ringrtc_1.CallState.Ended) {
            log.info('Call already in progress, new call not allowed.');
            this.stopCallingLobby();
            return;
        }
        log.info('CallingClass.startOutgoingDirectCall(): Starting in RingRTC');
        // We could make this faster by getting the call object
        // from the RingRTC before we lookup the ICE servers.
        const call = ringrtc_1.RingRTC.startOutgoingCall(remoteUserId, hasLocalVideo, this.localDeviceId, callSettings);
        ringrtc_1.RingRTC.setOutgoingAudio(call.callId, hasLocalAudio);
        ringrtc_1.RingRTC.setVideoCapturer(call.callId, this.videoCapturer);
        ringrtc_1.RingRTC.setVideoRenderer(call.callId, this.videoRenderer);
        this.attachToCall(conversation, call);
        this.uxActions.outgoingCall({
            conversationId: conversation.id,
            hasLocalAudio,
            hasLocalVideo,
        });
        await this.startDeviceReselectionTimer();
    }
    getDirectCall(conversationId) {
        const call = (0, getOwn_1.getOwn)(this.callsByConversation, conversationId);
        return call instanceof ringrtc_1.Call ? call : undefined;
    }
    getGroupCall(conversationId) {
        const call = (0, getOwn_1.getOwn)(this.callsByConversation, conversationId);
        return call instanceof ringrtc_1.GroupCall ? call : undefined;
    }
    getGroupCallMembers(conversationId) {
        return (0, groups_1.getMembershipList)(conversationId).map(member => new ringrtc_1.GroupMemberInfo(Buffer.from((0, Crypto_1.uuidToBytes)(member.uuid)), Buffer.from(member.uuidCiphertext)));
    }
    async peekGroupCall(conversationId) {
        var _a;
        // This can be undefined in two cases:
        //
        // 1. There is no group call instance. This is "stateless peeking", and is expected
        //    when we want to peek on a call that we've never connected to.
        // 2. There is a group call instance but RingRTC doesn't have the peek info yet. This
        //    should only happen for a brief period as you connect to the call. (You probably
        //    don't want to call this function while a group call is connected—you should
        //    instead be grabbing the peek info off of the instance—but we handle it here
        //    to avoid possible race conditions.)
        const statefulPeekInfo = (_a = this.getGroupCall(conversationId)) === null || _a === void 0 ? void 0 : _a.getPeekInfo();
        if (statefulPeekInfo) {
            return statefulPeekInfo;
        }
        if (!this.sfuUrl) {
            throw new Error('Missing SFU URL; not peeking group call');
        }
        const conversation = window.ConversationController.get(conversationId);
        if (!conversation) {
            throw new Error('Missing conversation; not peeking group call');
        }
        const publicParams = conversation.get('publicParams');
        const secretParams = conversation.get('secretParams');
        if (!publicParams || !secretParams) {
            throw new Error('Conversation is missing required parameters. Cannot peek group call');
        }
        const proof = await (0, groups_1.fetchMembershipProof)({ publicParams, secretParams });
        if (!proof) {
            throw new Error('No membership proof. Cannot peek group call');
        }
        const membershipProof = Bytes.fromString(proof);
        return ringrtc_1.RingRTC.peekGroupCall(this.sfuUrl, Buffer.from(membershipProof), this.getGroupCallMembers(conversationId));
    }
    /**
     * Connect to a conversation's group call and connect it to Redux.
     *
     * Should only be called with group call-compatible conversations.
     *
     * Idempotent.
     */
    connectGroupCall(conversationId, { groupId, publicParams, secretParams, }) {
        const existing = this.getGroupCall(conversationId);
        if (existing) {
            const isExistingCallNotConnected = existing.getLocalDeviceState().connectionState ===
                ringrtc_1.ConnectionState.NotConnected;
            if (isExistingCallNotConnected) {
                existing.connect();
            }
            return existing;
        }
        if (!this.sfuUrl) {
            throw new Error('Missing SFU URL; not connecting group call');
        }
        const groupIdBuffer = Buffer.from(Bytes.fromBase64(groupId));
        let updateMessageState = GroupCallUpdateMessageState.SentNothing;
        let isRequestingMembershipProof = false;
        const outerGroupCall = ringrtc_1.RingRTC.getGroupCall(groupIdBuffer, this.sfuUrl, {
            onLocalDeviceStateChanged: groupCall => {
                const localDeviceState = groupCall.getLocalDeviceState();
                const { eraId } = groupCall.getPeekInfo() || {};
                if (localDeviceState.connectionState === ringrtc_1.ConnectionState.NotConnected) {
                    // NOTE: This assumes that only one call is active at a time. For example, if
                    //   there are two calls using the camera, this will disable both of them.
                    //   That's fine for now, but this will break if that assumption changes.
                    this.disableLocalVideo();
                    delete this.callsByConversation[conversationId];
                    if (updateMessageState === GroupCallUpdateMessageState.SentJoin &&
                        eraId) {
                        updateMessageState = GroupCallUpdateMessageState.SentLeft;
                        this.sendGroupCallUpdateMessage(conversationId, eraId);
                    }
                }
                else {
                    this.callsByConversation[conversationId] = groupCall;
                    // NOTE: This assumes only one active call at a time. See comment above.
                    if (localDeviceState.videoMuted) {
                        this.disableLocalVideo();
                    }
                    else {
                        this.videoCapturer.enableCaptureAndSend(groupCall);
                    }
                    if (updateMessageState === GroupCallUpdateMessageState.SentNothing &&
                        localDeviceState.joinState === ringrtc_1.JoinState.Joined &&
                        eraId) {
                        updateMessageState = GroupCallUpdateMessageState.SentJoin;
                        this.sendGroupCallUpdateMessage(conversationId, eraId);
                    }
                }
                this.syncGroupCallToRedux(conversationId, groupCall);
            },
            onRemoteDeviceStatesChanged: groupCall => {
                this.syncGroupCallToRedux(conversationId, groupCall);
            },
            onPeekChanged: groupCall => {
                const localDeviceState = groupCall.getLocalDeviceState();
                const { eraId } = groupCall.getPeekInfo() || {};
                if (updateMessageState === GroupCallUpdateMessageState.SentNothing &&
                    localDeviceState.connectionState !== ringrtc_1.ConnectionState.NotConnected &&
                    localDeviceState.joinState === ringrtc_1.JoinState.Joined &&
                    eraId) {
                    updateMessageState = GroupCallUpdateMessageState.SentJoin;
                    this.sendGroupCallUpdateMessage(conversationId, eraId);
                }
                this.updateCallHistoryForGroupCall(conversationId, groupCall.getPeekInfo());
                this.syncGroupCallToRedux(conversationId, groupCall);
            },
            async requestMembershipProof(groupCall) {
                if (isRequestingMembershipProof) {
                    return;
                }
                isRequestingMembershipProof = true;
                try {
                    const proof = await (0, groups_1.fetchMembershipProof)({
                        publicParams,
                        secretParams,
                    });
                    if (proof) {
                        groupCall.setMembershipProof(Buffer.from(Bytes.fromString(proof)));
                    }
                }
                catch (err) {
                    log.error('Failed to fetch membership proof', err);
                }
                finally {
                    isRequestingMembershipProof = false;
                }
            },
            requestGroupMembers: groupCall => {
                groupCall.setGroupMembers(this.getGroupCallMembers(conversationId));
            },
            onEnded: lodash_1.noop,
        });
        if (!outerGroupCall) {
            // This should be very rare, likely due to RingRTC not being able to get a lock
            //   or memory or something like that.
            throw new Error('Failed to get a group call instance; cannot start call');
        }
        outerGroupCall.connect();
        this.syncGroupCallToRedux(conversationId, outerGroupCall);
        return outerGroupCall;
    }
    async joinGroupCall(conversationId, hasLocalAudio, hasLocalVideo, shouldRing) {
        var _a;
        const conversation = (_a = window.ConversationController.get(conversationId)) === null || _a === void 0 ? void 0 : _a.format();
        if (!conversation) {
            log.error('Missing conversation; not joining group call');
            return;
        }
        if (!conversation.groupId ||
            !conversation.publicParams ||
            !conversation.secretParams) {
            log.error('Conversation is missing required parameters. Cannot join group call');
            return;
        }
        await this.startDeviceReselectionTimer();
        const groupCall = this.connectGroupCall(conversationId, {
            groupId: conversation.groupId,
            publicParams: conversation.publicParams,
            secretParams: conversation.secretParams,
        });
        groupCall.setOutgoingAudioMuted(!hasLocalAudio);
        groupCall.setOutgoingVideoMuted(!hasLocalVideo);
        this.videoCapturer.enableCaptureAndSend(groupCall);
        if (shouldRing) {
            groupCall.ringAll();
        }
        groupCall.join();
    }
    getCallIdForConversation(conversationId) {
        var _a;
        return (_a = this.getDirectCall(conversationId)) === null || _a === void 0 ? void 0 : _a.callId;
    }
    setGroupCallVideoRequest(conversationId, resolutions) {
        var _a;
        (_a = this.getGroupCall(conversationId)) === null || _a === void 0 ? void 0 : _a.requestVideo(resolutions);
    }
    groupMembersChanged(conversationId) {
        // This will be called for any conversation change, so it's likely that there won't
        //   be a group call available; that's fine.
        const groupCall = this.getGroupCall(conversationId);
        if (!groupCall) {
            return;
        }
        groupCall.setGroupMembers(this.getGroupCallMembers(conversationId));
    }
    // See the comment in types/Calling.ts to explain why we have to do this conversion.
    convertRingRtcConnectionState(connectionState) {
        switch (connectionState) {
            case ringrtc_1.ConnectionState.NotConnected:
                return Calling_1.GroupCallConnectionState.NotConnected;
            case ringrtc_1.ConnectionState.Connecting:
                return Calling_1.GroupCallConnectionState.Connecting;
            case ringrtc_1.ConnectionState.Connected:
                return Calling_1.GroupCallConnectionState.Connected;
            case ringrtc_1.ConnectionState.Reconnecting:
                return Calling_1.GroupCallConnectionState.Reconnecting;
            default:
                throw (0, missingCaseError_1.missingCaseError)(connectionState);
        }
    }
    // See the comment in types/Calling.ts to explain why we have to do this conversion.
    convertRingRtcJoinState(joinState) {
        switch (joinState) {
            case ringrtc_1.JoinState.NotJoined:
                return Calling_1.GroupCallJoinState.NotJoined;
            case ringrtc_1.JoinState.Joining:
                return Calling_1.GroupCallJoinState.Joining;
            case ringrtc_1.JoinState.Joined:
                return Calling_1.GroupCallJoinState.Joined;
            default:
                throw (0, missingCaseError_1.missingCaseError)(joinState);
        }
    }
    formatGroupCallPeekInfoForRedux(peekInfo) {
        var _a;
        return {
            uuids: peekInfo.joinedMembers.map(uuidBuffer => {
                let uuid = (0, Crypto_1.bytesToUuid)(uuidBuffer);
                if (!uuid) {
                    log.error('Calling.formatGroupCallPeekInfoForRedux: could not convert peek UUID Uint8Array to string; using fallback UUID');
                    uuid = '00000000-0000-0000-0000-000000000000';
                }
                return uuid;
            }),
            creatorUuid: peekInfo.creator && (0, Crypto_1.bytesToUuid)(peekInfo.creator),
            eraId: peekInfo.eraId,
            maxDevices: (_a = peekInfo.maxDevices) !== null && _a !== void 0 ? _a : Infinity,
            deviceCount: peekInfo.deviceCount,
        };
    }
    formatGroupCallForRedux(groupCall) {
        const localDeviceState = groupCall.getLocalDeviceState();
        const peekInfo = groupCall.getPeekInfo();
        // RingRTC doesn't ensure that the demux ID is unique. This can happen if someone
        //   leaves the call and quickly rejoins; RingRTC will tell us that there are two
        //   participants with the same demux ID in the call. This should be rare.
        const remoteDeviceStates = (0, lodash_1.uniqBy)(groupCall.getRemoteDeviceStates() || [], remoteDeviceState => remoteDeviceState.demuxId);
        // It should be impossible to be disconnected and Joining or Joined. Just in case, we
        //   try to handle that case.
        const joinState = localDeviceState.connectionState === ringrtc_1.ConnectionState.NotConnected
            ? Calling_1.GroupCallJoinState.NotJoined
            : this.convertRingRtcJoinState(localDeviceState.joinState);
        return {
            connectionState: this.convertRingRtcConnectionState(localDeviceState.connectionState),
            joinState,
            hasLocalAudio: !localDeviceState.audioMuted,
            hasLocalVideo: !localDeviceState.videoMuted,
            peekInfo: peekInfo
                ? this.formatGroupCallPeekInfoForRedux(peekInfo)
                : undefined,
            remoteParticipants: remoteDeviceStates.map(remoteDeviceState => {
                let uuid = (0, Crypto_1.bytesToUuid)(remoteDeviceState.userId);
                if (!uuid) {
                    log.error('Calling.formatGroupCallForRedux: could not convert remote participant UUID Uint8Array to string; using fallback UUID');
                    uuid = '00000000-0000-0000-0000-000000000000';
                }
                return {
                    uuid,
                    demuxId: remoteDeviceState.demuxId,
                    hasRemoteAudio: !remoteDeviceState.audioMuted,
                    hasRemoteVideo: !remoteDeviceState.videoMuted,
                    presenting: Boolean(remoteDeviceState.presenting),
                    sharingScreen: Boolean(remoteDeviceState.sharingScreen),
                    speakerTime: (0, normalizeGroupCallTimestamp_1.normalizeGroupCallTimestamp)(remoteDeviceState.speakerTime),
                    // If RingRTC doesn't send us an aspect ratio, we make a guess.
                    videoAspectRatio: remoteDeviceState.videoAspectRatio ||
                        (remoteDeviceState.videoMuted ? 1 : 4 / 3),
                };
            }),
        };
    }
    getGroupCallVideoFrameSource(conversationId, demuxId) {
        const groupCall = this.getGroupCall(conversationId);
        if (!groupCall) {
            throw new Error('Could not find matching call');
        }
        return groupCall.getVideoSource(demuxId);
    }
    resendGroupCallMediaKeys(conversationId) {
        const groupCall = this.getGroupCall(conversationId);
        if (!groupCall) {
            throw new Error('Could not find matching call');
        }
        groupCall.resendMediaKeys();
    }
    syncGroupCallToRedux(conversationId, groupCall) {
        var _a;
        (_a = this.uxActions) === null || _a === void 0 ? void 0 : _a.groupCallStateChange(Object.assign({ conversationId }, this.formatGroupCallForRedux(groupCall)));
    }
    async sendGroupCallUpdateMessage(conversationId, eraId) {
        const conversation = window.ConversationController.get(conversationId);
        if (!conversation) {
            log.error('Unable to send group call update message for non-existent conversation');
            return;
        }
        const groupV2 = conversation.getGroupV2Info();
        const sendOptions = await (0, getSendOptions_1.getSendOptions)(conversation.attributes);
        if (!groupV2) {
            log.error('Unable to send group call update message for conversation that lacks groupV2 info');
            return;
        }
        const timestamp = Date.now();
        // We "fire and forget" because sending this message is non-essential.
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        (0, groups_1.wrapWithSyncMessageSend)({
            conversation,
            logId: `sendToGroup/groupCallUpdate/${conversationId}-${eraId}`,
            messageIds: [],
            send: () => conversation.queueJob('sendGroupCallUpdateMessage', () => window.Signal.Util.sendToGroup({
                groupSendOptions: {
                    groupCallUpdate: { eraId },
                    groupV2,
                    timestamp,
                },
                conversation,
                contentHint: ContentHint.DEFAULT,
                messageId: undefined,
                sendOptions,
                sendType: 'callingMessage',
            })),
            sendType: 'callingMessage',
            timestamp,
        }).catch(err => {
            log.error('Failed to send group call update:', err && err.stack ? err.stack : err);
        });
    }
    async acceptDirectCall(conversationId, asVideoCall) {
        log.info('CallingClass.acceptDirectCall()');
        const callId = this.getCallIdForConversation(conversationId);
        if (!callId) {
            log.warn('Trying to accept a non-existent call');
            return;
        }
        const haveMediaPermissions = await this.requestPermissions(asVideoCall);
        if (haveMediaPermissions) {
            await this.startDeviceReselectionTimer();
            ringrtc_1.RingRTC.setVideoCapturer(callId, this.videoCapturer);
            ringrtc_1.RingRTC.setVideoRenderer(callId, this.videoRenderer);
            ringrtc_1.RingRTC.accept(callId, asVideoCall);
        }
        else {
            log.info('Permissions were denied, call not allowed, hanging up.');
            ringrtc_1.RingRTC.hangup(callId);
        }
    }
    declineDirectCall(conversationId) {
        log.info('CallingClass.declineDirectCall()');
        const callId = this.getCallIdForConversation(conversationId);
        if (!callId) {
            log.warn('declineDirectCall: Trying to decline a non-existent call');
            return;
        }
        ringrtc_1.RingRTC.decline(callId);
    }
    declineGroupCall(conversationId, ringId) {
        var _a;
        log.info('CallingClass.declineGroupCall()');
        const groupId = (_a = window.ConversationController.get(conversationId)) === null || _a === void 0 ? void 0 : _a.get('groupId');
        if (!groupId) {
            log.error('declineGroupCall: could not find the group ID for that conversation');
            return;
        }
        const groupIdBuffer = Buffer.from(Bytes.fromBase64(groupId));
        ringrtc_1.RingRTC.cancelGroupRing(groupIdBuffer, ringId, ringrtc_1.RingCancelReason.DeclinedByUser);
    }
    hangup(conversationId) {
        log.info('CallingClass.hangup()');
        const call = (0, getOwn_1.getOwn)(this.callsByConversation, conversationId);
        if (!call) {
            log.warn('Trying to hang up a non-existent call');
            return;
        }
        electron_1.ipcRenderer.send('close-screen-share-controller');
        if (call instanceof ringrtc_1.Call) {
            ringrtc_1.RingRTC.hangup(call.callId);
        }
        else if (call instanceof ringrtc_1.GroupCall) {
            // This ensures that we turn off our devices.
            call.setOutgoingAudioMuted(true);
            call.setOutgoingVideoMuted(true);
            call.disconnect();
        }
        else {
            throw (0, missingCaseError_1.missingCaseError)(call);
        }
    }
    setOutgoingAudio(conversationId, enabled) {
        const call = (0, getOwn_1.getOwn)(this.callsByConversation, conversationId);
        if (!call) {
            log.warn('Trying to set outgoing audio for a non-existent call');
            return;
        }
        if (call instanceof ringrtc_1.Call) {
            ringrtc_1.RingRTC.setOutgoingAudio(call.callId, enabled);
        }
        else if (call instanceof ringrtc_1.GroupCall) {
            call.setOutgoingAudioMuted(!enabled);
        }
        else {
            throw (0, missingCaseError_1.missingCaseError)(call);
        }
    }
    setOutgoingVideo(conversationId, enabled) {
        const call = (0, getOwn_1.getOwn)(this.callsByConversation, conversationId);
        if (!call) {
            log.warn('Trying to set outgoing video for a non-existent call');
            return;
        }
        if (call instanceof ringrtc_1.Call) {
            ringrtc_1.RingRTC.setOutgoingVideo(call.callId, enabled);
        }
        else if (call instanceof ringrtc_1.GroupCall) {
            call.setOutgoingVideoMuted(!enabled);
        }
        else {
            throw (0, missingCaseError_1.missingCaseError)(call);
        }
    }
    setOutgoingVideoIsScreenShare(call, enabled) {
        if (call instanceof ringrtc_1.Call) {
            ringrtc_1.RingRTC.setOutgoingVideoIsScreenShare(call.callId, enabled);
            // Note: there is no "presenting" API for direct calls.
        }
        else if (call instanceof ringrtc_1.GroupCall) {
            call.setOutgoingVideoIsScreenShare(enabled);
            call.setPresenting(enabled);
        }
        else {
            throw (0, missingCaseError_1.missingCaseError)(call);
        }
    }
    async getPresentingSources() {
        const sources = await electron_1.desktopCapturer.getSources({
            fetchWindowIcons: true,
            thumbnailSize: { height: 102, width: 184 },
            types: ['window', 'screen'],
        });
        const presentableSources = [];
        sources.forEach(source => {
            // If electron can't retrieve a thumbnail then it won't be able to
            // present this source so we filter these out.
            if (source.thumbnail.isEmpty()) {
                return;
            }
            presentableSources.push({
                appIcon: source.appIcon && !source.appIcon.isEmpty()
                    ? source.appIcon.toDataURL()
                    : undefined,
                id: source.id,
                name: translateSourceName(window.i18n, source),
                isScreen: isScreenSource(source),
                thumbnail: source.thumbnail.toDataURL(),
            });
        });
        return presentableSources;
    }
    setPresenting(conversationId, hasLocalVideo, source) {
        var _a;
        const call = (0, getOwn_1.getOwn)(this.callsByConversation, conversationId);
        if (!call) {
            log.warn('Trying to set presenting for a non-existent call');
            return;
        }
        this.videoCapturer.disable();
        if (source) {
            this.hadLocalVideoBeforePresenting = hasLocalVideo;
            this.videoCapturer.enableCaptureAndSend(call, {
                // 15fps is much nicer but takes up a lot more CPU.
                maxFramerate: 5,
                maxHeight: 1080,
                maxWidth: 1920,
                screenShareSourceId: source.id,
            });
            this.setOutgoingVideo(conversationId, true);
        }
        else {
            this.setOutgoingVideo(conversationId, (_a = this.hadLocalVideoBeforePresenting) !== null && _a !== void 0 ? _a : hasLocalVideo);
            this.hadLocalVideoBeforePresenting = undefined;
        }
        const isPresenting = Boolean(source);
        this.setOutgoingVideoIsScreenShare(call, isPresenting);
        if (source) {
            electron_1.ipcRenderer.send('show-screen-share', source.name);
            notifications_1.notificationService.notify({
                icon: 'images/icons/v2/video-solid-24.svg',
                message: window.i18n('calling__presenting--notification-body'),
                onNotificationClick: () => {
                    if (this.uxActions) {
                        this.uxActions.setPresenting();
                    }
                },
                silent: true,
                title: window.i18n('calling__presenting--notification-title'),
            });
        }
        else {
            electron_1.ipcRenderer.send('close-screen-share-controller');
        }
    }
    async startDeviceReselectionTimer() {
        // Poll once
        await this.pollForMediaDevices();
        // Start the timer
        if (!this.deviceReselectionTimer) {
            this.deviceReselectionTimer = setInterval(async () => {
                await this.pollForMediaDevices();
            }, 3000);
        }
    }
    stopDeviceReselectionTimer() {
        if (this.deviceReselectionTimer) {
            clearInterval(this.deviceReselectionTimer);
            this.deviceReselectionTimer = undefined;
        }
    }
    mediaDeviceSettingsEqual(a, b) {
        if (!a && !b) {
            return true;
        }
        if (!a || !b) {
            return false;
        }
        if (a.availableCameras.length !== b.availableCameras.length ||
            a.availableMicrophones.length !== b.availableMicrophones.length ||
            a.availableSpeakers.length !== b.availableSpeakers.length) {
            return false;
        }
        for (let i = 0; i < a.availableCameras.length; i += 1) {
            if (a.availableCameras[i].deviceId !== b.availableCameras[i].deviceId ||
                a.availableCameras[i].groupId !== b.availableCameras[i].groupId ||
                a.availableCameras[i].label !== b.availableCameras[i].label) {
                return false;
            }
        }
        for (let i = 0; i < a.availableMicrophones.length; i += 1) {
            if (a.availableMicrophones[i].name !== b.availableMicrophones[i].name ||
                a.availableMicrophones[i].uniqueId !==
                    b.availableMicrophones[i].uniqueId) {
                return false;
            }
        }
        for (let i = 0; i < a.availableSpeakers.length; i += 1) {
            if (a.availableSpeakers[i].name !== b.availableSpeakers[i].name ||
                a.availableSpeakers[i].uniqueId !== b.availableSpeakers[i].uniqueId) {
                return false;
            }
        }
        if ((a.selectedCamera && !b.selectedCamera) ||
            (!a.selectedCamera && b.selectedCamera) ||
            (a.selectedMicrophone && !b.selectedMicrophone) ||
            (!a.selectedMicrophone && b.selectedMicrophone) ||
            (a.selectedSpeaker && !b.selectedSpeaker) ||
            (!a.selectedSpeaker && b.selectedSpeaker)) {
            return false;
        }
        if (a.selectedCamera &&
            b.selectedCamera &&
            a.selectedCamera !== b.selectedCamera) {
            return false;
        }
        if (a.selectedMicrophone &&
            b.selectedMicrophone &&
            a.selectedMicrophone.index !== b.selectedMicrophone.index) {
            return false;
        }
        if (a.selectedSpeaker &&
            b.selectedSpeaker &&
            a.selectedSpeaker.index !== b.selectedSpeaker.index) {
            return false;
        }
        return true;
    }
    async pollForMediaDevices() {
        var _a;
        const newSettings = await this.getMediaDeviceSettings();
        if (!this.mediaDeviceSettingsEqual(this.lastMediaDeviceSettings, newSettings)) {
            log.info('MediaDevice: available devices changed (from->to)', this.lastMediaDeviceSettings, newSettings);
            await this.selectPreferredDevices(newSettings);
            this.lastMediaDeviceSettings = newSettings;
            (_a = this.uxActions) === null || _a === void 0 ? void 0 : _a.refreshIODevices(newSettings);
        }
    }
    async getAvailableIODevices() {
        const availableCameras = await this.videoCapturer.enumerateDevices();
        const availableMicrophones = ringrtc_1.RingRTC.getAudioInputs();
        const availableSpeakers = ringrtc_1.RingRTC.getAudioOutputs();
        return {
            availableCameras,
            availableMicrophones,
            availableSpeakers,
        };
    }
    async getMediaDeviceSettings() {
        const { previousAudioDeviceModule, currentAudioDeviceModule } = this;
        if (!previousAudioDeviceModule || !currentAudioDeviceModule) {
            throw new Error('Calling#getMediaDeviceSettings cannot be called before audio device settings are set');
        }
        const { availableCameras, availableMicrophones, availableSpeakers } = await this.getAvailableIODevices();
        const preferredMicrophone = window.Events.getPreferredAudioInputDevice();
        const selectedMicIndex = (0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)({
            available: availableMicrophones,
            preferred: preferredMicrophone,
            previousAudioDeviceModule,
            currentAudioDeviceModule,
        });
        const selectedMicrophone = selectedMicIndex !== undefined
            ? availableMicrophones[selectedMicIndex]
            : undefined;
        const preferredSpeaker = window.Events.getPreferredAudioOutputDevice();
        const selectedSpeakerIndex = (0, findBestMatchingDevice_1.findBestMatchingAudioDeviceIndex)({
            available: availableSpeakers,
            preferred: preferredSpeaker,
            previousAudioDeviceModule,
            currentAudioDeviceModule,
        });
        const selectedSpeaker = selectedSpeakerIndex !== undefined
            ? availableSpeakers[selectedSpeakerIndex]
            : undefined;
        const preferredCamera = window.Events.getPreferredVideoInputDevice();
        const selectedCamera = (0, findBestMatchingDevice_1.findBestMatchingCameraId)(availableCameras, preferredCamera);
        return {
            availableMicrophones,
            availableSpeakers,
            selectedMicrophone,
            selectedSpeaker,
            availableCameras,
            selectedCamera,
        };
    }
    setPreferredMicrophone(device) {
        log.info('MediaDevice: setPreferredMicrophone', device);
        window.Events.setPreferredAudioInputDevice(device);
        ringrtc_1.RingRTC.setAudioInput(device.index);
    }
    setPreferredSpeaker(device) {
        log.info('MediaDevice: setPreferredSpeaker', device);
        window.Events.setPreferredAudioOutputDevice(device);
        ringrtc_1.RingRTC.setAudioOutput(device.index);
    }
    enableLocalCamera() {
        this.videoCapturer.enableCapture();
    }
    disableLocalVideo() {
        this.videoCapturer.disable();
    }
    async setPreferredCamera(device) {
        log.info('MediaDevice: setPreferredCamera', device);
        window.Events.setPreferredVideoInputDevice(device);
        await this.videoCapturer.setPreferredDevice(device);
    }
    async handleCallingMessage(envelope, callingMessage) {
        log.info('CallingClass.handleCallingMessage()');
        const enableIncomingCalls = window.Events.getIncomingCallNotification();
        if (callingMessage.offer && !enableIncomingCalls) {
            // Drop offers silently if incoming call notifications are disabled.
            log.info('Incoming calls are disabled, ignoring call offer.');
            return;
        }
        const remoteUserId = envelope.sourceUuid;
        const remoteDeviceId = this.parseDeviceId(envelope.sourceDevice);
        if (!remoteUserId || !remoteDeviceId || !this.localDeviceId) {
            log.error('Missing identifier, ignoring call message.');
            return;
        }
        const { storage } = window.textsecure;
        const senderIdentityRecord = await storage.protocol.getOrMigrateIdentityRecord(new UUID_1.UUID(remoteUserId));
        if (!senderIdentityRecord) {
            log.error('Missing sender identity record; ignoring call message.');
            return;
        }
        const senderIdentityKey = senderIdentityRecord.publicKey.slice(1); // Ignore the type header, it is not used.
        const ourUuid = storage.user.getCheckedUuid();
        const receiverIdentityRecord = storage.protocol.getIdentityRecord(ourUuid);
        if (!receiverIdentityRecord) {
            log.error('Missing receiver identity record; ignoring call message.');
            return;
        }
        const receiverIdentityKey = receiverIdentityRecord.publicKey.slice(1); // Ignore the type header, it is not used.
        const conversation = window.ConversationController.get(remoteUserId);
        if (!conversation) {
            log.error('Missing conversation; ignoring call message.');
            return;
        }
        if (callingMessage.offer && !conversation.getAccepted()) {
            log.info('Conversation was not approved by user; rejecting call message.');
            const hangup = new ringrtc_1.HangupMessage();
            hangup.callId = callingMessage.offer.callId;
            hangup.deviceId = remoteDeviceId;
            hangup.type = ringrtc_1.HangupType.NeedPermission;
            const message = new ringrtc_1.CallingMessage();
            message.legacyHangup = hangup;
            await this.handleOutgoingSignaling(remoteUserId, message);
            const ProtoOfferType = protobuf_1.SignalService.CallingMessage.Offer.Type;
            this.addCallHistoryForFailedIncomingCall(conversation, callingMessage.offer.type === ProtoOfferType.OFFER_VIDEO_CALL, envelope.timestamp);
            return;
        }
        const sourceUuid = envelope.sourceUuid
            ? (0, Crypto_1.uuidToBytes)(envelope.sourceUuid)
            : null;
        const messageAgeSec = envelope.messageAgeSec ? envelope.messageAgeSec : 0;
        log.info('CallingClass.handleCallingMessage(): Handling in RingRTC');
        ringrtc_1.RingRTC.handleCallingMessage(remoteUserId, sourceUuid ? Buffer.from(sourceUuid) : null, remoteDeviceId, this.localDeviceId, messageAgeSec, protoToCallingMessage(callingMessage), Buffer.from(senderIdentityKey), Buffer.from(receiverIdentityKey));
    }
    async selectPreferredDevices(settings) {
        if ((!this.lastMediaDeviceSettings && settings.selectedCamera) ||
            (this.lastMediaDeviceSettings &&
                settings.selectedCamera &&
                this.lastMediaDeviceSettings.selectedCamera !== settings.selectedCamera)) {
            log.info('MediaDevice: selecting camera', settings.selectedCamera);
            await this.videoCapturer.setPreferredDevice(settings.selectedCamera);
        }
        // Assume that the MediaDeviceSettings have been obtained very recently and
        // the index is still valid (no devices have been plugged in in between).
        if (settings.selectedMicrophone) {
            log.info('MediaDevice: selecting microphone', settings.selectedMicrophone);
            ringrtc_1.RingRTC.setAudioInput(settings.selectedMicrophone.index);
        }
        if (settings.selectedSpeaker) {
            log.info('MediaDevice: selecting speaker', settings.selectedSpeaker);
            ringrtc_1.RingRTC.setAudioOutput(settings.selectedSpeaker.index);
        }
    }
    async requestCameraPermissions() {
        const cameraPermission = await window.getMediaCameraPermissions();
        if (!cameraPermission) {
            await window.showCallingPermissionsPopup(true);
            // Check the setting again (from the source of truth).
            return window.getMediaCameraPermissions();
        }
        return true;
    }
    async requestPermissions(isVideoCall) {
        const microphonePermission = await (0, requestMicrophonePermissions_1.requestMicrophonePermissions)();
        if (microphonePermission) {
            if (isVideoCall) {
                return this.requestCameraPermissions();
            }
            return true;
        }
        return false;
    }
    async handleSendCallMessage(recipient, data, urgency) {
        const userId = (0, Crypto_1.bytesToUuid)(recipient);
        if (!userId) {
            log.error('handleSendCallMessage(): bad recipient UUID');
            return false;
        }
        const message = new ringrtc_1.CallingMessage();
        message.opaque = new ringrtc_1.OpaqueMessage();
        message.opaque.data = Buffer.from(data);
        return this.handleOutgoingSignaling(userId, message, urgency);
    }
    async handleSendCallMessageToGroup(groupIdBytes, data, urgency) {
        const groupId = groupIdBytes.toString('base64');
        const conversation = window.ConversationController.get(groupId);
        if (!conversation) {
            log.error('handleSendCallMessageToGroup(): could not find conversation');
            return;
        }
        const timestamp = Date.now();
        const callingMessage = new ringrtc_1.CallingMessage();
        callingMessage.opaque = new ringrtc_1.OpaqueMessage();
        callingMessage.opaque.data = data;
        const contentMessage = new protobuf_1.SignalService.Content();
        contentMessage.callingMessage = (0, callingMessageToProto_1.callingMessageToProto)(callingMessage, urgency);
        // We "fire and forget" because sending this message is non-essential.
        // We also don't sync this message.
        const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
        await conversation.queueJob('handleSendCallMessageToGroup', async () => (0, handleMessageSend_1.handleMessageSend)(window.Signal.Util.sendContentMessageToGroup({
            contentHint: ContentHint.DEFAULT,
            contentMessage,
            conversation,
            isPartialSend: false,
            messageId: undefined,
            recipients: conversation.getRecipients(),
            sendOptions: await (0, getSendOptions_1.getSendOptions)(conversation.attributes),
            sendType: 'callingMessage',
            timestamp,
        }), { messageIds: [], sendType: 'callingMessage' }));
    }
    async handleGroupCallRingUpdate(groupIdBytes, ringId, ringerBytes, update) {
        var _a, _b;
        log.info(`handleGroupCallRingUpdate(): got ring update ${update}`);
        const groupId = groupIdBytes.toString('base64');
        const ringerUuid = (0, Crypto_1.bytesToUuid)(ringerBytes);
        if (!ringerUuid) {
            log.error('handleGroupCallRingUpdate(): ringerUuid was invalid');
            return;
        }
        const conversation = window.ConversationController.get(groupId);
        if (!conversation) {
            log.error('handleGroupCallRingUpdate(): could not find conversation');
            return;
        }
        const conversationId = conversation.id;
        let shouldRing = false;
        if (update === ringrtc_1.RingUpdate.Requested) {
            const processResult = await processGroupCallRingRequest(ringId);
            switch (processResult) {
                case Calling_1.ProcessGroupCallRingRequestResult.ShouldRing:
                    shouldRing = true;
                    break;
                case Calling_1.ProcessGroupCallRingRequestResult.RingWasPreviouslyCanceled:
                    ringrtc_1.RingRTC.cancelGroupRing(groupIdBytes, ringId, null);
                    break;
                case Calling_1.ProcessGroupCallRingRequestResult.ThereIsAnotherActiveRing:
                    ringrtc_1.RingRTC.cancelGroupRing(groupIdBytes, ringId, ringrtc_1.RingCancelReason.Busy);
                    break;
                default:
                    throw (0, missingCaseError_1.missingCaseError)(processResult);
            }
        }
        else {
            await processGroupCallRingCancelation(ringId);
        }
        if (shouldRing) {
            log.info('handleGroupCallRingUpdate: ringing');
            (_a = this.uxActions) === null || _a === void 0 ? void 0 : _a.receiveIncomingGroupCall({
                conversationId,
                ringId,
                ringerUuid,
            });
        }
        else {
            log.info('handleGroupCallRingUpdate: canceling any existing ring');
            (_b = this.uxActions) === null || _b === void 0 ? void 0 : _b.cancelIncomingGroupCallRing({
                conversationId,
                ringId,
            });
        }
    }
    async handleOutgoingSignaling(remoteUserId, message, urgency) {
        const conversation = window.ConversationController.get(remoteUserId);
        const sendOptions = conversation
            ? await (0, getSendOptions_1.getSendOptions)(conversation.attributes)
            : undefined;
        if (!window.textsecure.messaging) {
            log.warn('handleOutgoingSignaling() returning false; offline');
            return false;
        }
        try {
            const result = await (0, handleMessageSend_1.handleMessageSend)(window.textsecure.messaging.sendCallingMessage(remoteUserId, (0, callingMessageToProto_1.callingMessageToProto)(message, urgency), sendOptions), { messageIds: [], sendType: 'callingMessage' });
            if (result && result.errors && result.errors.length) {
                throw result.errors[0];
            }
            log.info('handleOutgoingSignaling() completed successfully');
            return true;
        }
        catch (err) {
            if (err && err.errors && err.errors.length > 0) {
                log.error(`handleOutgoingSignaling() failed: ${err.errors[0].reason}`);
            }
            else {
                log.error('handleOutgoingSignaling() failed');
            }
            return false;
        }
    }
    // If we return null here, we hang up the call.
    async handleIncomingCall(call) {
        log.info('CallingClass.handleIncomingCall()');
        if (!this.uxActions || !this.localDeviceId) {
            log.error('Missing required objects, ignoring incoming call.');
            return null;
        }
        const conversation = window.ConversationController.get(call.remoteUserId);
        if (!conversation) {
            log.error('Missing conversation, ignoring incoming call.');
            return null;
        }
        try {
            // The peer must be 'trusted' before accepting a call from them.
            // This is mostly the safety number check, unverified meaning that they were
            // verified before but now they are not.
            const verifiedEnum = await conversation.safeGetVerified();
            if (verifiedEnum ===
                window.textsecure.storage.protocol.VerifiedStatus.UNVERIFIED) {
                log.info(`Peer is not trusted, ignoring incoming call for conversation: ${conversation.idForLogging()}`);
                this.addCallHistoryForFailedIncomingCall(conversation, call.isVideoCall, Date.now());
                return null;
            }
            this.attachToCall(conversation, call);
            this.uxActions.receiveIncomingDirectCall({
                conversationId: conversation.id,
                isVideoCall: call.isVideoCall,
            });
            log.info('CallingClass.handleIncomingCall(): Proceeding');
            return await this.getCallSettings(conversation);
        }
        catch (err) {
            log.error(`Ignoring incoming call: ${err.stack}`);
            this.addCallHistoryForFailedIncomingCall(conversation, call.isVideoCall, Date.now());
            return null;
        }
    }
    handleAutoEndedIncomingCallRequest(remoteUserId, reason, ageInSeconds) {
        const conversation = window.ConversationController.get(remoteUserId);
        if (!conversation) {
            return;
        }
        // This is extra defensive, just in case RingRTC passes us a bad value. (It probably
        //   won't.)
        const ageInMilliseconds = (0, isNormalNumber_1.isNormalNumber)(ageInSeconds) && ageInSeconds >= 0
            ? ageInSeconds * durations.SECOND
            : 0;
        const endedTime = Date.now() - ageInMilliseconds;
        this.addCallHistoryForAutoEndedIncomingCall(conversation, reason, endedTime);
    }
    attachToCall(conversation, call) {
        this.callsByConversation[conversation.id] = call;
        const { uxActions } = this;
        if (!uxActions) {
            return;
        }
        let acceptedTime;
        // eslint-disable-next-line no-param-reassign
        call.handleStateChanged = () => {
            if (call.state === ringrtc_1.CallState.Accepted) {
                acceptedTime = acceptedTime || Date.now();
            }
            else if (call.state === ringrtc_1.CallState.Ended) {
                this.addCallHistoryForEndedCall(conversation, call, acceptedTime);
                this.stopDeviceReselectionTimer();
                this.lastMediaDeviceSettings = undefined;
                delete this.callsByConversation[conversation.id];
            }
            uxActions.callStateChange({
                conversationId: conversation.id,
                acceptedTime,
                callState: call.state,
                callEndedReason: call.endedReason,
                isIncoming: call.isIncoming,
                isVideoCall: call.isVideoCall,
                title: conversation.getTitle(),
            });
        };
        // eslint-disable-next-line no-param-reassign
        call.handleRemoteVideoEnabled = () => {
            uxActions.remoteVideoChange({
                conversationId: conversation.id,
                hasVideo: call.remoteVideoEnabled,
            });
        };
        // eslint-disable-next-line no-param-reassign
        call.handleRemoteSharingScreen = () => {
            uxActions.remoteSharingScreenChange({
                conversationId: conversation.id,
                isSharingScreen: Boolean(call.remoteSharingScreen),
            });
        };
    }
    async handleLogMessage(level, fileName, line, message) {
        switch (level) {
            case ringrtc_1.CallLogLevel.Info:
                log.info(`${fileName}:${line} ${message}`);
                break;
            case ringrtc_1.CallLogLevel.Warn:
                log.warn(`${fileName}:${line} ${message}`);
                break;
            case ringrtc_1.CallLogLevel.Error:
                log.error(`${fileName}:${line} ${message}`);
                break;
            default:
                break;
        }
    }
    async handleSendHttpRequest(requestId, url, method, headers, body) {
        if (!window.textsecure.messaging) {
            ringrtc_1.RingRTC.httpRequestFailed(requestId, 'We are offline');
            return;
        }
        const httpMethod = RINGRTC_HTTP_METHOD_TO_OUR_HTTP_METHOD.get(method);
        if (httpMethod === undefined) {
            ringrtc_1.RingRTC.httpRequestFailed(requestId, `Unknown method: ${JSON.stringify(method)}`);
            return;
        }
        let result;
        try {
            result = await window.textsecure.messaging.server.makeSfuRequest(url, httpMethod, headers, body);
        }
        catch (err) {
            if (err.code !== -1) {
                // WebAPI treats certain response codes as errors, but RingRTC still needs to
                // see them. It does not currently look at the response body, so we're giving
                // it an empty one.
                ringrtc_1.RingRTC.receivedHttpResponse(requestId, err.code, Buffer.alloc(0));
            }
            else {
                log.error('handleSendHttpRequest: fetch failed with error', err);
                ringrtc_1.RingRTC.httpRequestFailed(requestId, String(err));
            }
            return;
        }
        ringrtc_1.RingRTC.receivedHttpResponse(requestId, result.response.status, Buffer.from(result.data));
    }
    getRemoteUserIdFromConversation(conversation) {
        const recipients = conversation.getRecipients();
        if (recipients.length !== 1) {
            return undefined;
        }
        return recipients[0];
    }
    get localDeviceId() {
        return this.parseDeviceId(window.textsecure.storage.user.getDeviceId());
    }
    parseDeviceId(deviceId) {
        if (typeof deviceId === 'string') {
            return parseInt(deviceId, 10);
        }
        if (typeof deviceId === 'number') {
            return deviceId;
        }
        return null;
    }
    async getCallSettings(conversation) {
        if (!window.textsecure.messaging) {
            throw new Error('getCallSettings: offline!');
        }
        const iceServer = await window.textsecure.messaging.server.getIceServers();
        const shouldRelayCalls = window.Events.getAlwaysRelayCalls();
        // If the peer is 'unknown', i.e. not in the contact list, force IP hiding.
        const isContactUnknown = !conversation.isFromOrAddedByTrustedContact();
        return {
            iceServer: Object.assign(Object.assign({}, iceServer), { urls: iceServer.urls.slice() }),
            hideIp: shouldRelayCalls || isContactUnknown,
            bandwidthMode: ringrtc_1.BandwidthMode.Normal,
        };
    }
    addCallHistoryForEndedCall(conversation, call, acceptedTimeParam) {
        let acceptedTime = acceptedTimeParam;
        const { endedReason, isIncoming } = call;
        const wasAccepted = Boolean(acceptedTime);
        const isOutgoing = !isIncoming;
        const wasDeclined = !wasAccepted &&
            (endedReason === ringrtc_1.CallEndedReason.Declined ||
                endedReason === ringrtc_1.CallEndedReason.DeclinedOnAnotherDevice ||
                (isIncoming && endedReason === ringrtc_1.CallEndedReason.LocalHangup) ||
                (isOutgoing && endedReason === ringrtc_1.CallEndedReason.RemoteHangup) ||
                (isOutgoing &&
                    endedReason === ringrtc_1.CallEndedReason.RemoteHangupNeedPermission));
        if (call.endedReason === ringrtc_1.CallEndedReason.AcceptedOnAnotherDevice) {
            acceptedTime = Date.now();
        }
        conversation.addCallHistory({
            callMode: Calling_1.CallMode.Direct,
            wasIncoming: call.isIncoming,
            wasVideoCall: call.isVideoCall,
            wasDeclined,
            acceptedTime,
            endedTime: Date.now(),
        });
    }
    addCallHistoryForFailedIncomingCall(conversation, wasVideoCall, timestamp) {
        conversation.addCallHistory({
            callMode: Calling_1.CallMode.Direct,
            wasIncoming: true,
            wasVideoCall,
            // Since the user didn't decline, make sure it shows up as a missed call instead
            wasDeclined: false,
            acceptedTime: undefined,
            endedTime: timestamp,
        });
    }
    addCallHistoryForAutoEndedIncomingCall(conversation, _reason, endedTime) {
        conversation.addCallHistory({
            callMode: Calling_1.CallMode.Direct,
            wasIncoming: true,
            // We don't actually know, but it doesn't seem that important in this case,
            // but we could maybe plumb this info through RingRTC
            wasVideoCall: false,
            // Since the user didn't decline, make sure it shows up as a missed call instead
            wasDeclined: false,
            acceptedTime: undefined,
            endedTime,
        });
    }
    async updateCallHistoryForGroupCall(conversationId, peekInfo) {
        // If we don't have the necessary pieces to peek, bail. (It's okay if we don't.)
        if (!peekInfo || !peekInfo.eraId || !peekInfo.creator) {
            return;
        }
        const creatorUuid = (0, Crypto_1.bytesToUuid)(peekInfo.creator);
        if (!creatorUuid) {
            log.error('updateCallHistoryForGroupCall(): bad creator UUID');
            return;
        }
        const creatorConversation = window.ConversationController.get(creatorUuid);
        const conversation = window.ConversationController.get(conversationId);
        if (!conversation) {
            log.error('updateCallHistoryForGroupCall(): could not find conversation');
            return;
        }
        const isNewCall = await conversation.updateCallHistoryForGroupCall(peekInfo.eraId, creatorUuid);
        const wasStartedByMe = Boolean(creatorConversation && (0, whatTypeOfConversation_1.isMe)(creatorConversation.attributes));
        const isAnybodyElseInGroupCall = Boolean(peekInfo.joinedMembers.length);
        if (isNewCall && !wasStartedByMe && isAnybodyElseInGroupCall) {
            this.notifyForGroupCall(conversation, creatorConversation);
        }
    }
    notifyForGroupCall(conversation, creatorConversation) {
        let notificationTitle;
        let notificationMessage;
        switch (notifications_1.notificationService.getNotificationSetting()) {
            case notifications_1.NotificationSetting.Off:
                return;
            case notifications_1.NotificationSetting.NoNameOrMessage:
                notificationTitle = notifications_1.FALLBACK_NOTIFICATION_TITLE;
                notificationMessage = window.i18n('calling__call-notification__started-by-someone');
                break;
            default:
                // These fallbacks exist just in case something unexpected goes wrong.
                notificationTitle =
                    (conversation === null || conversation === void 0 ? void 0 : conversation.getTitle()) || notifications_1.FALLBACK_NOTIFICATION_TITLE;
                notificationMessage = creatorConversation
                    ? window.i18n('calling__call-notification__started', [
                        creatorConversation.getTitle(),
                    ])
                    : window.i18n('calling__call-notification__started-by-someone');
                break;
        }
        notifications_1.notificationService.notify({
            icon: 'images/icons/v2/video-solid-24.svg',
            message: notificationMessage,
            onNotificationClick: () => {
                var _a;
                (_a = this.uxActions) === null || _a === void 0 ? void 0 : _a.startCallingLobby({
                    conversationId: conversation.id,
                    isVideoCall: true,
                });
            },
            silent: false,
            title: notificationTitle,
        });
    }
    async cleanExpiredGroupCallRingsAndLoop() {
        try {
            await cleanExpiredGroupCallRings();
        }
        catch (err) {
            // These errors are ignored here. They should be logged elsewhere and it's okay if
            //   we don't do a cleanup this time.
        }
        setTimeout(() => {
            this.cleanExpiredGroupCallRingsAndLoop();
        }, CLEAN_EXPIRED_GROUP_CALL_RINGS_INTERVAL);
    }
}
exports.CallingClass = CallingClass;
exports.calling = new CallingClass();
