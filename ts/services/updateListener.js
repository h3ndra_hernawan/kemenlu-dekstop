"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.initializeUpdateListener = void 0;
const electron_1 = require("electron");
function initializeUpdateListener(updatesActions) {
    electron_1.ipcRenderer.on('show-update-dialog', (_, dialogType, options = {}) => {
        updatesActions.showUpdateDialog(dialogType, options);
    });
}
exports.initializeUpdateListener = initializeUpdateListener;
