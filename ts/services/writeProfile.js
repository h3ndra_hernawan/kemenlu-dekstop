"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.writeProfile = void 0;
const Client_1 = __importDefault(require("../sql/Client"));
const Crypto_1 = require("../Crypto");
const encryptProfileData_1 = require("../util/encryptProfileData");
const getProfile_1 = require("../util/getProfile");
const handleMessageSend_1 = require("../util/handleMessageSend");
async function writeProfile(conversation, avatarBuffer) {
    // Before we write anything we request the user's profile so that we can
    // have an up-to-date paymentAddress to be able to include it when we write
    const model = window.ConversationController.get(conversation.id);
    if (!model) {
        return;
    }
    await (0, getProfile_1.getProfile)(model.get('uuid'), model.get('e164'));
    // Encrypt the profile data, update profile, and if needed upload the avatar
    const { aboutEmoji, aboutText, avatarHash, avatarPath, familyName, firstName, } = conversation;
    const [profileData, encryptedAvatarData] = await (0, encryptProfileData_1.encryptProfileData)(conversation, avatarBuffer);
    const avatarRequestHeaders = await window.textsecure.messaging.putProfile(profileData);
    // Upload the avatar if provided
    // delete existing files on disk if avatar has been removed
    // update the account's avatar path and hash if it's a new avatar
    let profileAvatar;
    if (avatarRequestHeaders && encryptedAvatarData && avatarBuffer) {
        await window.textsecure.messaging.uploadAvatar(avatarRequestHeaders, encryptedAvatarData);
        const hash = await (0, Crypto_1.computeHash)(avatarBuffer);
        if (hash !== avatarHash) {
            const [path] = await Promise.all([
                window.Signal.Migrations.writeNewAttachmentData(avatarBuffer),
                avatarPath
                    ? window.Signal.Migrations.deleteAttachmentData(avatarPath)
                    : undefined,
            ]);
            profileAvatar = {
                hash,
                path,
            };
        }
    }
    else if (avatarPath) {
        await window.Signal.Migrations.deleteAttachmentData(avatarPath);
    }
    const profileAvatarData = profileAvatar ? { profileAvatar } : {};
    // Update backbone, update DB, run storage service upload
    model.set(Object.assign({ about: aboutText, aboutEmoji, profileName: firstName, profileFamilyName: familyName }, profileAvatarData));
    Client_1.default.updateConversation(model.attributes);
    model.captureChange('writeProfile');
    await (0, handleMessageSend_1.handleMessageSend)(window.textsecure.messaging.sendFetchLocalProfileSyncMessage(), { messageIds: [], sendType: 'otherSync' });
}
exports.writeProfile = writeProfile;
