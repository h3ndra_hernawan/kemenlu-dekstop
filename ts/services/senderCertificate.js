"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.senderCertificateService = exports.SenderCertificateService = void 0;
const OutgoingMessage_1 = require("../textsecure/OutgoingMessage");
const Bytes = __importStar(require("../Bytes"));
const assert_1 = require("../util/assert");
const missingCaseError_1 = require("../util/missingCaseError");
const normalizeNumber_1 = require("../util/normalizeNumber");
const waitForOnline_1 = require("../util/waitForOnline");
const log = __importStar(require("../logging/log"));
const protobuf_1 = require("../protobuf");
var SenderCertificate = protobuf_1.SignalService.SenderCertificate;
function isWellFormed(data) {
    return OutgoingMessage_1.serializedCertificateSchema.safeParse(data).success;
}
// In case your clock is different from the server's, we "fake" expire certificates early.
const CLOCK_SKEW_THRESHOLD = 15 * 60 * 1000;
// This is exported for testing.
class SenderCertificateService {
    constructor() {
        this.fetchPromises = new Map();
    }
    initialize({ server, navigator, onlineEventTarget, storage, }) {
        log.info('Sender certificate service initialized');
        this.server = server;
        this.navigator = navigator;
        this.onlineEventTarget = onlineEventTarget;
        this.storage = storage;
    }
    async get(mode) {
        const storedCertificate = this.getStoredCertificate(mode);
        if (storedCertificate) {
            log.info(`Sender certificate service found a valid ${modeToLogString(mode)} certificate in storage; skipping fetch`);
            return storedCertificate;
        }
        return this.fetchCertificate(mode);
    }
    // This is intended to be called when our credentials have been deleted, so any fetches
    //   made until this function is complete would fail anyway.
    async clear() {
        log.info('Sender certificate service: Clearing in-progress fetches and ' +
            'deleting cached certificates');
        await Promise.all(this.fetchPromises.values());
        const { storage } = this;
        (0, assert_1.assert)(storage, 'Sender certificate service method was called before it was initialized');
        await storage.remove('senderCertificate');
        await storage.remove('senderCertificateNoE164');
    }
    getStoredCertificate(mode) {
        const { storage } = this;
        (0, assert_1.assert)(storage, 'Sender certificate service method was called before it was initialized');
        const valueInStorage = storage.get(modeToStorageKey(mode));
        if (isWellFormed(valueInStorage) &&
            isExpirationValid(valueInStorage.expires)) {
            return valueInStorage;
        }
        return undefined;
    }
    fetchCertificate(mode) {
        // This prevents multiple concurrent fetches.
        const existingPromise = this.fetchPromises.get(mode);
        if (existingPromise) {
            log.info(`Sender certificate service was already fetching a ${modeToLogString(mode)} certificate; piggybacking off of that`);
            return existingPromise;
        }
        let promise;
        const doFetch = async () => {
            const result = await this.fetchAndSaveCertificate(mode);
            (0, assert_1.assert)(this.fetchPromises.get(mode) === promise, 'Sender certificate service was deleting a different promise than expected');
            this.fetchPromises.delete(mode);
            return result;
        };
        promise = doFetch();
        (0, assert_1.assert)(!this.fetchPromises.has(mode), 'Sender certificate service somehow already had a promise for this mode');
        this.fetchPromises.set(mode, promise);
        return promise;
    }
    async fetchAndSaveCertificate(mode) {
        const { storage, navigator, onlineEventTarget } = this;
        (0, assert_1.assert)(storage && navigator && onlineEventTarget, 'Sender certificate service method was called before it was initialized');
        log.info(`Sender certificate service: fetching and saving a ${modeToLogString(mode)} certificate`);
        await (0, waitForOnline_1.waitForOnline)(navigator, onlineEventTarget);
        let certificateString;
        try {
            certificateString = await this.requestSenderCertificate(mode);
        }
        catch (err) {
            log.warn(`Sender certificate service could not fetch a ${modeToLogString(mode)} certificate. Returning undefined`, err && err.stack ? err.stack : err);
            return undefined;
        }
        const certificate = Bytes.fromBase64(certificateString);
        const decodedContainer = SenderCertificate.decode(certificate);
        const decodedCert = decodedContainer.certificate
            ? SenderCertificate.Certificate.decode(decodedContainer.certificate)
            : undefined;
        const expires = (0, normalizeNumber_1.normalizeNumber)(decodedCert === null || decodedCert === void 0 ? void 0 : decodedCert.expires);
        if (!isExpirationValid(expires)) {
            log.warn(`Sender certificate service fetched a ${modeToLogString(mode)} certificate from the server that was already expired (or was invalid). Is your system clock off?`);
            return undefined;
        }
        const serializedCertificate = {
            expires: expires - CLOCK_SKEW_THRESHOLD,
            serialized: certificate,
        };
        await storage.put(modeToStorageKey(mode), serializedCertificate);
        return serializedCertificate;
    }
    async requestSenderCertificate(mode) {
        const { server } = this;
        (0, assert_1.assert)(server, 'Sender certificate service method was called before it was initialized');
        const omitE164 = mode === 1 /* WithoutE164 */;
        const { certificate } = await server.getSenderCertificate(omitE164);
        return certificate;
    }
}
exports.SenderCertificateService = SenderCertificateService;
function modeToStorageKey(mode) {
    switch (mode) {
        case 0 /* WithE164 */:
            return 'senderCertificate';
        case 1 /* WithoutE164 */:
            return 'senderCertificateNoE164';
        default:
            throw (0, missingCaseError_1.missingCaseError)(mode);
    }
}
function modeToLogString(mode) {
    switch (mode) {
        case 0 /* WithE164 */:
            return 'yes-E164';
        case 1 /* WithoutE164 */:
            return 'no-E164';
        default:
            throw (0, missingCaseError_1.missingCaseError)(mode);
    }
}
function isExpirationValid(expiration) {
    return typeof expiration === 'number' && expiration > Date.now();
}
exports.senderCertificateService = new SenderCertificateService();
