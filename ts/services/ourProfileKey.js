"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ourProfileKeyService = exports.OurProfileKeyService = void 0;
const assert_1 = require("../util/assert");
const log = __importStar(require("../logging/log"));
class OurProfileKeyService {
    constructor() {
        this.promisesBlockingGet = [];
    }
    initialize(storage) {
        log.info('Our profile key service: initializing');
        const storageReadyPromise = new Promise(resolve => {
            storage.onready(() => {
                resolve();
            });
        });
        this.promisesBlockingGet = [storageReadyPromise];
        this.storage = storage;
    }
    get() {
        if (this.getPromise) {
            log.info('Our profile key service: was already fetching. Piggybacking off of that');
        }
        else {
            log.info('Our profile key service: kicking off a new fetch');
            this.getPromise = this.doGet();
        }
        return this.getPromise;
    }
    async set(newValue) {
        log.info('Our profile key service: updating profile key');
        (0, assert_1.assert)(this.storage, 'OurProfileKeyService was not initialized');
        if (newValue) {
            await this.storage.put('profileKey', newValue);
        }
        else {
            await this.storage.remove('profileKey');
        }
    }
    blockGetWithPromise(promise) {
        this.promisesBlockingGet.push(promise);
    }
    async doGet() {
        log.info(`Our profile key service: waiting for ${this.promisesBlockingGet.length} promises before fetching`);
        await Promise.allSettled(this.promisesBlockingGet);
        this.promisesBlockingGet = [];
        delete this.getPromise;
        (0, assert_1.assert)(this.storage, 'OurProfileKeyService was not initialized');
        log.info('Our profile key service: fetching profile key from storage');
        const result = this.storage.get('profileKey');
        if (result === undefined || result instanceof Uint8Array) {
            return result;
        }
        (0, assert_1.assert)(false, 'Profile key in storage was defined, but not an Uint8Array. Returning undefined');
        return undefined;
    }
}
exports.OurProfileKeyService = OurProfileKeyService;
exports.ourProfileKeyService = new OurProfileKeyService();
