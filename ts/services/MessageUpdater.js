"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.markViewed = exports.markRead = void 0;
const MessageReadStatus_1 = require("../messages/MessageReadStatus");
const notifications_1 = require("./notifications");
function markReadOrViewed(messageAttrs, readStatus, timestamp, skipSave) {
    var _a;
    const oldReadStatus = (_a = messageAttrs.readStatus) !== null && _a !== void 0 ? _a : MessageReadStatus_1.ReadStatus.Read;
    const newReadStatus = (0, MessageReadStatus_1.maxReadStatus)(oldReadStatus, readStatus);
    const nextMessageAttributes = Object.assign(Object.assign({}, messageAttrs), { readStatus: newReadStatus });
    const { id: messageId, expireTimer, expirationStartTimestamp } = messageAttrs;
    if (expireTimer && !expirationStartTimestamp) {
        nextMessageAttributes.expirationStartTimestamp = Math.min(Date.now(), timestamp || Date.now());
    }
    notifications_1.notificationService.removeBy({ messageId });
    if (!skipSave) {
        window.Signal.Util.queueUpdateMessage(nextMessageAttributes);
    }
    return nextMessageAttributes;
}
const markRead = (messageAttrs, readAt, { skipSave = false } = {}) => markReadOrViewed(messageAttrs, MessageReadStatus_1.ReadStatus.Read, readAt, skipSave);
exports.markRead = markRead;
const markViewed = (messageAttrs, viewedAt, { skipSave = false } = {}) => markReadOrViewed(messageAttrs, MessageReadStatus_1.ReadStatus.Viewed, viewedAt, skipSave);
exports.markViewed = markViewed;
