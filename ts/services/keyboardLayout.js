"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.lookup = exports.initialize = void 0;
const assert_1 = require("../util/assert");
let layoutMap;
async function initialize() {
    var _a;
    (0, assert_1.strictAssert)(layoutMap === undefined, 'keyboardLayout already initialized');
    const experimentalNavigator = window.navigator;
    (0, assert_1.strictAssert)(typeof ((_a = experimentalNavigator.keyboard) === null || _a === void 0 ? void 0 : _a.getLayoutMap) === 'function', 'No support for getLayoutMap');
    layoutMap = await experimentalNavigator.keyboard.getLayoutMap();
}
exports.initialize = initialize;
function lookup({ code, key, }) {
    var _a;
    (0, assert_1.strictAssert)(layoutMap !== undefined, 'keyboardLayout not initialized');
    return (_a = layoutMap.get(code)) !== null && _a !== void 0 ? _a : key;
}
exports.lookup = lookup;
