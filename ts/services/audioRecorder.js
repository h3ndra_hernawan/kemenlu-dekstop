"use strict";
// Copyright 2016-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.recorder = exports.RecorderClass = void 0;
const requestMicrophonePermissions_1 = require("../util/requestMicrophonePermissions");
const log = __importStar(require("../logging/log"));
class RecorderClass {
    clear() {
        this.blob = undefined;
        this.resolve = undefined;
        if (this.source) {
            this.source.disconnect();
            this.source = undefined;
        }
        if (this.recorder) {
            if (this.recorder.isRecording()) {
                this.recorder.cancelRecording();
            }
            // Reach in and terminate the web worker used by WebAudioRecorder, otherwise
            // it gets leaked due to a reference cycle with its onmessage listener
            this.recorder.worker.terminate();
            this.recorder = undefined;
        }
        this.input = undefined;
        this.stream = undefined;
        if (this.context) {
            this.context.close();
            this.context = undefined;
        }
    }
    async start() {
        const hasMicrophonePermission = await (0, requestMicrophonePermissions_1.requestMicrophonePermissions)();
        if (!hasMicrophonePermission) {
            log.info('Recorder/start: Microphone permission was denied, new audio recording not allowed.');
            return false;
        }
        this.clear();
        this.context = new AudioContext();
        this.input = this.context.createGain();
        this.recorder = new window.WebAudioRecorder(this.input, {
            encoding: 'mp3',
            workerDir: 'js/',
            options: {
                timeLimit: 360, // one minute more than our UI-imposed limit
            },
        });
        this.recorder.onComplete = this.onComplete.bind(this);
        this.recorder.onError = this.onError.bind(this);
        try {
            const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
            if (!this.context || !this.input) {
                const err = new Error('Recorder/getUserMedia/stream: Missing context or input!');
                this.onError(this.recorder, err);
                throw err;
            }
            this.source = this.context.createMediaStreamSource(stream);
            this.source.connect(this.input);
            this.stream = stream;
        }
        catch (err) {
            log.error('Recorder.onGetUserMediaError:', err && err.stack ? err.stack : err);
            this.clear();
            throw err;
        }
        if (this.recorder) {
            this.recorder.startRecording();
            return true;
        }
        return false;
    }
    async stop() {
        if (!this.recorder) {
            log.warn('Recorder/stop: Called with no recorder');
            return;
        }
        if (this.stream) {
            this.stream.getTracks().forEach(track => track.stop());
        }
        if (this.blob) {
            return this.blob;
        }
        const promise = new Promise(resolve => {
            this.resolve = resolve;
        });
        this.recorder.finishRecording();
        return promise;
    }
    onComplete(_recorder, blob) {
        var _a;
        this.blob = blob;
        (_a = this.resolve) === null || _a === void 0 ? void 0 : _a.call(this, blob);
    }
    onError(_recorder, error) {
        if (!this.recorder) {
            log.warn('Recorder/onError: Called with no recorder');
            return;
        }
        this.clear();
        log.error('Recorder/onError:', error && error.stack ? error.stack : error);
    }
    getBlob() {
        if (!this.blob) {
            throw new Error('no blob found');
        }
        return this.blob;
    }
}
exports.RecorderClass = RecorderClass;
exports.recorder = new RecorderClass();
