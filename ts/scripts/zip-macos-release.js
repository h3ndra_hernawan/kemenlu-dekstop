"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.zipMacOSRelease = void 0;
/* eslint-disable no-console */
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const rimraf_1 = __importDefault(require("rimraf"));
const child_process_1 = require("child_process");
const package_json_1 = __importDefault(require("../../package.json"));
function zipMacOSRelease() {
    if (process.platform !== 'darwin') {
        return;
    }
    const files = fs_1.default
        .readdirSync('release')
        .filter(file => path_1.default.extname(file) === '.zip');
    if (!files.length) {
        throw new Error('No zip file found. Maybe the release did not complete properly?');
    }
    if (files.length > 1) {
        throw new Error('More than one zip file found, release directory was not cleared.');
    }
    const zipFile = files[0];
    const zipPath = path_1.default.join('release', zipFile);
    console.log('Removing current zip file');
    rimraf_1.default.sync(zipPath);
    const appName = `${package_json_1.default.productName}.app`;
    const appPath = path_1.default.join('release', 'mac', appName);
    const tmpPath = path_1.default.join('release', 'tmp');
    const appDir = path_1.default.dirname(appPath);
    const tmpZip = path_1.default.join(appDir, zipFile);
    console.log('Creating temporary zip file at', tmpZip);
    try {
        (0, child_process_1.execSync)(`cd ${appDir} && zip -ro ${zipFile} "${appName}"`);
        console.log('Unzipping to remove duplicate electron references from', tmpZip);
        (0, child_process_1.execSync)(`unzip ${tmpZip} -d ${tmpPath}`);
    }
    catch (err) {
        console.log('stdout:', String(err.stdout));
        console.log('stderr:', String(err.stderr));
        throw err;
    }
    console.log('Removing temporary zip file');
    rimraf_1.default.sync(tmpZip);
    const electronFrameworkPath = path_1.default.join(tmpPath, appName, 'Contents', 'Frameworks', 'Electron Framework.framework', 'Versions');
    console.log('Removing duplicate electron framework', electronFrameworkPath);
    rimraf_1.default.sync(electronFrameworkPath);
    try {
        console.log('Creating final zip');
        (0, child_process_1.execSync)(`cd ${tmpPath} && zip -ro ${zipFile} "${appName}"`);
    }
    catch (err) {
        console.log('stdout:', String(err.stdout));
        console.log('stderr:', String(err.stderr));
        throw err;
    }
    console.log('Moving into the final destination', zipPath);
    fs_1.default.renameSync(path_1.default.join(tmpPath, zipFile), zipPath);
    rimraf_1.default.sync(tmpPath);
    console.log('zip-macos-release is done');
}
exports.zipMacOSRelease = zipMacOSRelease;
