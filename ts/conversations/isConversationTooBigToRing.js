"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.isConversationTooBigToRing = void 0;
const parseIntWithFallback_1 = require("../util/parseIntWithFallback");
const RemoteConfig_1 = require("../RemoteConfig");
const getMaxGroupCallRingSize = () => (0, parseIntWithFallback_1.parseIntWithFallback)((0, RemoteConfig_1.getValue)('global.calling.maxGroupCallRingSize'), 16);
const isConversationTooBigToRing = (conversation) => { var _a; return (((_a = conversation.memberships) === null || _a === void 0 ? void 0 : _a.length) || 0) >= getMaxGroupCallRingSize(); };
exports.isConversationTooBigToRing = isConversationTooBigToRing;
