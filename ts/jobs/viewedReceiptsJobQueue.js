"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.viewedReceiptsJobQueue = exports.ViewedReceiptsJobQueue = void 0;
const zod_1 = require("zod");
const durations = __importStar(require("../util/durations"));
const exponentialBackoff_1 = require("../util/exponentialBackoff");
const commonShouldJobContinue_1 = require("./helpers/commonShouldJobContinue");
const sendViewedReceipt_1 = require("../util/sendViewedReceipt");
const JobQueue_1 = require("./JobQueue");
const JobQueueDatabaseStore_1 = require("./JobQueueDatabaseStore");
const handleCommonJobRequestError_1 = require("./helpers/handleCommonJobRequestError");
const MAX_RETRY_TIME = durations.DAY;
const viewedReceiptsJobDataSchema = zod_1.z.object({
    viewedReceipt: zod_1.z.object({
        messageId: zod_1.z.string(),
        senderE164: zod_1.z.string().optional(),
        senderUuid: zod_1.z.string().optional(),
        timestamp: zod_1.z.number(),
    }),
});
class ViewedReceiptsJobQueue extends JobQueue_1.JobQueue {
    parseData(data) {
        return viewedReceiptsJobDataSchema.parse(data);
    }
    async run({ data, timestamp, }, { attempt, log }) {
        const timeRemaining = timestamp + MAX_RETRY_TIME - Date.now();
        const shouldContinue = await (0, commonShouldJobContinue_1.commonShouldJobContinue)({
            attempt,
            log,
            timeRemaining,
        });
        if (!shouldContinue) {
            return;
        }
        try {
            await (0, sendViewedReceipt_1.sendViewedReceipt)(data.viewedReceipt, log);
        }
        catch (err) {
            await (0, handleCommonJobRequestError_1.handleCommonJobRequestError)({ err, log, timeRemaining });
        }
    }
}
exports.ViewedReceiptsJobQueue = ViewedReceiptsJobQueue;
exports.viewedReceiptsJobQueue = new ViewedReceiptsJobQueue({
    store: JobQueueDatabaseStore_1.jobQueueDatabaseStore,
    queueType: 'viewed receipts',
    maxAttempts: (0, exponentialBackoff_1.exponentialBackoffMaxAttempts)(MAX_RETRY_TIME),
});
