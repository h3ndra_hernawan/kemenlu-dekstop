"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.Job = void 0;
/**
 * A single job instance. Shouldn't be instantiated directly, except by `JobQueue`.
 */
class Job {
    constructor(id, timestamp, queueType, data, completion) {
        this.id = id;
        this.timestamp = timestamp;
        this.queueType = queueType;
        this.data = data;
        this.completion = completion;
    }
}
exports.Job = Job;
