"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reactionJobQueue = exports.ReactionJobQueue = void 0;
const z = __importStar(require("zod"));
const iterables_1 = require("../util/iterables");
const exponentialBackoff_1 = require("../util/exponentialBackoff");
const durations = __importStar(require("../util/durations"));
const reactionUtil = __importStar(require("../reactions/util"));
const MessageSendState_1 = require("../messages/MessageSendState");
const getMessageById_1 = require("../messages/getMessageById");
const whatTypeOfConversation_1 = require("../util/whatTypeOfConversation");
const getSendOptions_1 = require("../util/getSendOptions");
const protobuf_1 = require("../protobuf");
const handleMessageSend_1 = require("../util/handleMessageSend");
const ourProfileKey_1 = require("../services/ourProfileKey");
const message_1 = require("../state/selectors/message");
const findAndFormatContact_1 = require("../util/findAndFormatContact");
const UUID_1 = require("../types/UUID");
const JobQueue_1 = require("./JobQueue");
const JobQueueDatabaseStore_1 = require("./JobQueueDatabaseStore");
const commonShouldJobContinue_1 = require("./helpers/commonShouldJobContinue");
const handleCommonJobRequestError_1 = require("./helpers/handleCommonJobRequestError");
const InMemoryQueues_1 = require("./helpers/InMemoryQueues");
const MAX_RETRY_TIME = durations.DAY;
const MAX_ATTEMPTS = (0, exponentialBackoff_1.exponentialBackoffMaxAttempts)(MAX_RETRY_TIME);
const reactionJobData = z.object({
    messageId: z.string(),
});
class ReactionJobQueue extends JobQueue_1.JobQueue {
    constructor() {
        super(...arguments);
        this.inMemoryQueues = new InMemoryQueues_1.InMemoryQueues();
    }
    parseData(data) {
        return reactionJobData.parse(data);
    }
    getInMemoryQueue({ data, }) {
        return this.inMemoryQueues.get(data.messageId);
    }
    async run({ data, timestamp }, { attempt, log }) {
        const { messageId } = data;
        const timeRemaining = timestamp + MAX_RETRY_TIME - Date.now();
        const isFinalAttempt = attempt >= MAX_ATTEMPTS;
        // We don't immediately use this value because we may want to mark the reaction
        //   failed before doing so.
        const shouldContinue = await (0, commonShouldJobContinue_1.commonShouldJobContinue)({
            attempt,
            log,
            timeRemaining,
        });
        await window.ConversationController.load();
        const ourConversationId = window.ConversationController.getOurConversationIdOrThrow();
        const message = await (0, getMessageById_1.getMessageById)(messageId);
        if (!message) {
            log.info(`message ${messageId} was not found, maybe because it was deleted. Giving up on sending its reactions`);
            return;
        }
        const { pendingReaction, emojiToRemove } = reactionUtil.getNewestPendingOutgoingReaction(getReactions(message), ourConversationId);
        if (!pendingReaction) {
            log.info(`no pending reaction for ${messageId}. Doing nothing`);
            return;
        }
        if (!(0, message_1.canReact)(message.attributes, ourConversationId, findAndFormatContact_1.findAndFormatContact)) {
            log.info(`could not react to ${messageId}. Removing this pending reaction`);
            markReactionFailed(message, pendingReaction);
            await window.Signal.Data.saveMessage(message.attributes);
            return;
        }
        if (!shouldContinue) {
            log.info(`reacting to message ${messageId} ran out of time. Giving up on sending it`);
            markReactionFailed(message, pendingReaction);
            await window.Signal.Data.saveMessage(message.attributes);
            return;
        }
        try {
            const conversation = message.getConversation();
            if (!conversation) {
                throw new Error(`could not find conversation for message with ID ${messageId}`);
            }
            const { allRecipientIdentifiers, recipientIdentifiersWithoutMe } = getRecipients(pendingReaction, conversation);
            const expireTimer = message.get('expireTimer');
            const profileKey = conversation.get('profileSharing')
                ? await ourProfileKey_1.ourProfileKeyService.get()
                : undefined;
            const reactionForSend = pendingReaction.emoji
                ? pendingReaction
                : Object.assign(Object.assign({}, pendingReaction), { emoji: emojiToRemove, remove: true });
            const ephemeralMessageForReactionSend = new window.Whisper.Message({
                id: UUID_1.UUID.generate.toString(),
                type: 'outgoing',
                conversationId: conversation.get('id'),
                sent_at: pendingReaction.timestamp,
                received_at: window.Signal.Util.incrementMessageCounter(),
                received_at_ms: pendingReaction.timestamp,
                reaction: reactionForSend,
                timestamp: pendingReaction.timestamp,
                sendStateByConversationId: (0, iterables_1.zipObject)(Object.keys(pendingReaction.isSentByConversationId || {}), (0, iterables_1.repeat)({
                    status: MessageSendState_1.SendStatus.Pending,
                    updatedAt: Date.now(),
                })),
            });
            ephemeralMessageForReactionSend.doNotSave = true;
            const successfulConversationIds = new Set();
            if (recipientIdentifiersWithoutMe.length === 0) {
                log.info('sending sync reaction message only');
                const dataMessage = await window.textsecure.messaging.getDataMessage({
                    attachments: [],
                    expireTimer,
                    groupV2: conversation.getGroupV2Info({
                        members: recipientIdentifiersWithoutMe,
                    }),
                    preview: [],
                    profileKey,
                    reaction: reactionForSend,
                    recipients: allRecipientIdentifiers,
                    timestamp: pendingReaction.timestamp,
                });
                await ephemeralMessageForReactionSend.sendSyncMessageOnly(dataMessage);
                successfulConversationIds.add(ourConversationId);
            }
            else {
                const sendOptions = await (0, getSendOptions_1.getSendOptions)(conversation.attributes);
                const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
                let promise;
                if ((0, whatTypeOfConversation_1.isDirectConversation)(conversation.attributes)) {
                    log.info('sending direct reaction message');
                    promise = window.textsecure.messaging.sendMessageToIdentifier({
                        identifier: recipientIdentifiersWithoutMe[0],
                        messageText: undefined,
                        attachments: [],
                        quote: undefined,
                        preview: [],
                        sticker: undefined,
                        reaction: reactionForSend,
                        deletedForEveryoneTimestamp: undefined,
                        timestamp: pendingReaction.timestamp,
                        expireTimer,
                        contentHint: ContentHint.RESENDABLE,
                        groupId: undefined,
                        profileKey,
                        options: sendOptions,
                    });
                }
                else {
                    log.info('sending group reaction message');
                    promise = window.Signal.Util.sendToGroup({
                        groupSendOptions: {
                            groupV1: conversation.getGroupV1Info(recipientIdentifiersWithoutMe),
                            groupV2: conversation.getGroupV2Info({
                                members: recipientIdentifiersWithoutMe,
                            }),
                            reaction: reactionForSend,
                            timestamp: pendingReaction.timestamp,
                            expireTimer,
                            profileKey,
                        },
                        conversation,
                        contentHint: ContentHint.RESENDABLE,
                        messageId,
                        sendOptions,
                        sendType: 'reaction',
                    });
                }
                await ephemeralMessageForReactionSend.send((0, handleMessageSend_1.handleMessageSend)(promise, {
                    messageIds: [messageId],
                    sendType: 'reaction',
                }));
                const reactionSendStateByConversationId = ephemeralMessageForReactionSend.get('sendStateByConversationId') ||
                    {};
                for (const [conversationId, sendState] of Object.entries(reactionSendStateByConversationId)) {
                    if ((0, MessageSendState_1.isSent)(sendState.status)) {
                        successfulConversationIds.add(conversationId);
                    }
                }
            }
            const newReactions = reactionUtil.markOutgoingReactionSent(getReactions(message), pendingReaction, successfulConversationIds);
            setReactions(message, newReactions);
            const didFullySend = true;
            if (!didFullySend) {
                throw new Error('reaction did not fully send');
            }
        }
        catch (err) {
            if (isFinalAttempt) {
                markReactionFailed(message, pendingReaction);
            }
            await (0, handleCommonJobRequestError_1.handleCommonJobRequestError)({ err, log, timeRemaining });
        }
        finally {
            await window.Signal.Data.saveMessage(message.attributes);
        }
    }
}
exports.ReactionJobQueue = ReactionJobQueue;
exports.reactionJobQueue = new ReactionJobQueue({
    store: JobQueueDatabaseStore_1.jobQueueDatabaseStore,
    queueType: 'reactions',
    maxAttempts: MAX_ATTEMPTS,
});
const getReactions = (message) => message.get('reactions') || [];
const setReactions = (message, reactions) => {
    if (reactions.length) {
        message.set('reactions', reactions);
    }
    else {
        message.unset('reactions');
    }
};
function getRecipients(reaction, conversation) {
    const allRecipientIdentifiers = [];
    const recipientIdentifiersWithoutMe = [];
    const currentConversationRecipients = conversation.getRecipientConversationIds();
    for (const id of reactionUtil.getUnsentConversationIds(reaction)) {
        const recipient = window.ConversationController.get(id);
        if (!recipient) {
            continue;
        }
        const recipientIdentifier = recipient.getSendTarget();
        const isRecipientMe = (0, whatTypeOfConversation_1.isMe)(recipient.attributes);
        if (!recipientIdentifier ||
            recipient.isUntrusted() ||
            (!currentConversationRecipients.has(id) && !isRecipientMe)) {
            continue;
        }
        allRecipientIdentifiers.push(recipientIdentifier);
        if (!isRecipientMe) {
            recipientIdentifiersWithoutMe.push(recipientIdentifier);
        }
    }
    return { allRecipientIdentifiers, recipientIdentifiersWithoutMe };
}
function markReactionFailed(message, pendingReaction) {
    const newReactions = reactionUtil.markOutgoingReactionFailed(getReactions(message), pendingReaction);
    setReactions(message, newReactions);
}
