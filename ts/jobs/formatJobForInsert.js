"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatJobForInsert = void 0;
/**
 * Format a job to be inserted into the database.
 *
 * Notably, `Job` instances (which have a promise attached) cannot be serialized without
 * some cleanup. That's what this function is most useful for.
 */
const formatJobForInsert = (job) => ({
    id: job.id,
    timestamp: job.timestamp,
    queueType: job.queueType,
    data: job.data,
});
exports.formatJobForInsert = formatJobForInsert;
