"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.jobQueueDatabaseStore = exports.JobQueueDatabaseStore = void 0;
const lodash_1 = require("lodash");
const AsyncQueue_1 = require("../util/AsyncQueue");
const asyncIterables_1 = require("../util/asyncIterables");
const formatJobForInsert_1 = require("./formatJobForInsert");
const Client_1 = __importDefault(require("../sql/Client"));
const log = __importStar(require("../logging/log"));
class JobQueueDatabaseStore {
    constructor(db) {
        this.db = db;
        this.activeQueueTypes = new Set();
        this.queues = new Map();
        this.initialFetchPromises = new Map();
    }
    async insert(job, { shouldPersist = true } = {}) {
        log.info(`JobQueueDatabaseStore adding job ${job.id} to queue ${JSON.stringify(job.queueType)}`);
        const initialFetchPromise = this.initialFetchPromises.get(job.queueType);
        if (!initialFetchPromise) {
            throw new Error(`JobQueueDatabaseStore tried to add job for queue ${JSON.stringify(job.queueType)} but streaming had not yet started`);
        }
        await initialFetchPromise;
        if (shouldPersist) {
            await this.db.insertJob((0, formatJobForInsert_1.formatJobForInsert)(job));
        }
        this.getQueue(job.queueType).add(job);
    }
    async delete(id) {
        await this.db.deleteJob(id);
    }
    stream(queueType) {
        if (this.activeQueueTypes.has(queueType)) {
            throw new Error(`Cannot stream queue type ${JSON.stringify(queueType)} more than once`);
        }
        this.activeQueueTypes.add(queueType);
        return (0, asyncIterables_1.concat)([
            (0, asyncIterables_1.wrapPromise)(this.fetchJobsAtStart(queueType)),
            this.getQueue(queueType),
        ]);
    }
    getQueue(queueType) {
        const existingQueue = this.queues.get(queueType);
        if (existingQueue) {
            return existingQueue;
        }
        const result = new AsyncQueue_1.AsyncQueue();
        this.queues.set(queueType, result);
        return result;
    }
    async fetchJobsAtStart(queueType) {
        log.info(`JobQueueDatabaseStore fetching existing jobs for queue ${JSON.stringify(queueType)}`);
        // This is initialized to `noop` because TypeScript doesn't know that `Promise` calls
        //   its callback synchronously, making sure `onFinished` is defined.
        let onFinished = lodash_1.noop;
        const initialFetchPromise = new Promise(resolve => {
            onFinished = resolve;
        });
        this.initialFetchPromises.set(queueType, initialFetchPromise);
        const result = await this.db.getJobsInQueue(queueType);
        log.info(`JobQueueDatabaseStore finished fetching existing ${result.length} jobs for queue ${JSON.stringify(queueType)}`);
        onFinished();
        return result;
    }
}
exports.JobQueueDatabaseStore = JobQueueDatabaseStore;
exports.jobQueueDatabaseStore = new JobQueueDatabaseStore(Client_1.default);
