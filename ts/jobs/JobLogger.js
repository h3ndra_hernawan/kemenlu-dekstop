"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobLogger = void 0;
class JobLogger {
    constructor(job, logger) {
        this.logger = logger;
        this.attempt = -1;
        this.id = job.id;
        this.queueType = job.queueType;
    }
    fatal(...args) {
        this.logger.fatal(this.prefix(), ...args);
    }
    error(...args) {
        this.logger.error(this.prefix(), ...args);
    }
    warn(...args) {
        this.logger.warn(this.prefix(), ...args);
    }
    info(...args) {
        this.logger.info(this.prefix(), ...args);
    }
    debug(...args) {
        this.logger.debug(this.prefix(), ...args);
    }
    trace(...args) {
        this.logger.trace(this.prefix(), ...args);
    }
    prefix() {
        return `${this.queueType} job queue, job ID ${this.id}, attempt ${this.attempt}:`;
    }
}
exports.JobLogger = JobLogger;
