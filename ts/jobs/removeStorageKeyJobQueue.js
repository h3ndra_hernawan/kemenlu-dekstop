"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeStorageKeyJobQueue = exports.RemoveStorageKeyJobQueue = void 0;
const zod_1 = require("zod");
const JobQueue_1 = require("./JobQueue");
const JobQueueDatabaseStore_1 = require("./JobQueueDatabaseStore");
const removeStorageKeyJobDataSchema = zod_1.z.object({
    key: zod_1.z.enum(['senderCertificateWithUuid']),
});
class RemoveStorageKeyJobQueue extends JobQueue_1.JobQueue {
    parseData(data) {
        return removeStorageKeyJobDataSchema.parse(data);
    }
    async run({ data, }) {
        await new Promise(resolve => {
            window.storage.onready(resolve);
        });
        await window.storage.remove(data.key);
    }
}
exports.RemoveStorageKeyJobQueue = RemoveStorageKeyJobQueue;
exports.removeStorageKeyJobQueue = new RemoveStorageKeyJobQueue({
    store: JobQueueDatabaseStore_1.jobQueueDatabaseStore,
    queueType: 'remove storage key',
    maxAttempts: 100,
});
