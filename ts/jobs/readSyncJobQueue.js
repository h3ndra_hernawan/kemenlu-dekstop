"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.readSyncJobQueue = exports.ReadSyncJobQueue = void 0;
const durations = __importStar(require("../util/durations"));
const exponentialBackoff_1 = require("../util/exponentialBackoff");
const readAndViewSyncHelpers_1 = require("./helpers/readAndViewSyncHelpers");
const assert_1 = require("../util/assert");
const isRecord_1 = require("../util/isRecord");
const JobQueue_1 = require("./JobQueue");
const JobQueueDatabaseStore_1 = require("./JobQueueDatabaseStore");
const MAX_RETRY_TIME = durations.DAY;
class ReadSyncJobQueue extends JobQueue_1.JobQueue {
    parseData(data) {
        (0, assert_1.strictAssert)((0, isRecord_1.isRecord)(data), 'data is not an object');
        return { readSyncs: (0, readAndViewSyncHelpers_1.parseRawSyncDataArray)(data.readSyncs) };
    }
    async run({ data, timestamp }, { attempt, log }) {
        await (0, readAndViewSyncHelpers_1.runReadOrViewSyncJob)({
            attempt,
            isView: false,
            log,
            maxRetryTime: MAX_RETRY_TIME,
            syncs: data.readSyncs,
            timestamp,
        });
    }
}
exports.ReadSyncJobQueue = ReadSyncJobQueue;
exports.readSyncJobQueue = new ReadSyncJobQueue({
    store: JobQueueDatabaseStore_1.jobQueueDatabaseStore,
    queueType: 'read sync',
    maxAttempts: (0, exponentialBackoff_1.exponentialBackoffMaxAttempts)(MAX_RETRY_TIME),
});
