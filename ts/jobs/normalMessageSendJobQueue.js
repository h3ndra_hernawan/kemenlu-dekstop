"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.normalMessageSendJobQueue = exports.NormalMessageSendJobQueue = void 0;
const exponentialBackoff_1 = require("../util/exponentialBackoff");
const commonShouldJobContinue_1 = require("./helpers/commonShouldJobContinue");
const sleepFor413RetryAfterTime_1 = require("./helpers/sleepFor413RetryAfterTime");
const InMemoryQueues_1 = require("./helpers/InMemoryQueues");
const getMessageById_1 = require("../messages/getMessageById");
const ourProfileKey_1 = require("../services/ourProfileKey");
const assert_1 = require("../util/assert");
const isRecord_1 = require("../util/isRecord");
const durations = __importStar(require("../util/durations"));
const whatTypeOfConversation_1 = require("../util/whatTypeOfConversation");
const getSendOptions_1 = require("../util/getSendOptions");
const protobuf_1 = require("../protobuf");
const handleMessageSend_1 = require("../util/handleMessageSend");
const MessageSendState_1 = require("../messages/MessageSendState");
const message_1 = require("../state/selectors/message");
const Errors = __importStar(require("../types/errors"));
const JobQueue_1 = require("./JobQueue");
const JobQueueDatabaseStore_1 = require("./JobQueueDatabaseStore");
const getHttpErrorCode_1 = require("./helpers/getHttpErrorCode");
const { loadAttachmentData, loadPreviewData, loadQuoteData, loadStickerData } = window.Signal.Migrations;
const { Message } = window.Signal.Types;
const MAX_RETRY_TIME = durations.DAY;
const MAX_ATTEMPTS = (0, exponentialBackoff_1.exponentialBackoffMaxAttempts)(MAX_RETRY_TIME);
class NormalMessageSendJobQueue extends JobQueue_1.JobQueue {
    constructor() {
        super(...arguments);
        this.inMemoryQueues = new InMemoryQueues_1.InMemoryQueues();
    }
    parseData(data) {
        // Because we do this so often and Zod is a bit slower, we do "manual" parsing here.
        (0, assert_1.strictAssert)((0, isRecord_1.isRecord)(data), 'Job data is not an object');
        const { messageId, conversationId } = data;
        (0, assert_1.strictAssert)(typeof messageId === 'string', 'Job data had a non-string message ID');
        (0, assert_1.strictAssert)(typeof conversationId === 'string', 'Job data had a non-string conversation ID');
        return { messageId, conversationId };
    }
    getInMemoryQueue({ data, }) {
        return this.inMemoryQueues.get(data.conversationId);
    }
    async run({ data, timestamp, }, { attempt, log }) {
        const { messageId } = data;
        const timeRemaining = timestamp + MAX_RETRY_TIME - Date.now();
        const isFinalAttempt = attempt >= MAX_ATTEMPTS;
        // We don't immediately use this value because we may want to mark the message
        //   failed before doing so.
        const shouldContinue = await (0, commonShouldJobContinue_1.commonShouldJobContinue)({
            attempt,
            log,
            timeRemaining,
        });
        await window.ConversationController.load();
        const message = await (0, getMessageById_1.getMessageById)(messageId);
        if (!message) {
            log.info(`message ${messageId} was not found, maybe because it was deleted. Giving up on sending it`);
            return;
        }
        if (!(0, message_1.isOutgoing)(message.attributes)) {
            log.error(`message ${messageId} was not an outgoing message to begin with. This is probably a bogus job. Giving up on sending it`);
            return;
        }
        if (message.isErased() || message.get('deletedForEveryone')) {
            log.info(`message ${messageId} was erased. Giving up on sending it`);
            return;
        }
        let messageSendErrors = [];
        // We don't want to save errors on messages unless we're giving up. If it's our
        //   final attempt, we know upfront that we want to give up. However, we might also
        //   want to give up if (1) we get a 508 from the server, asking us to please stop
        //   (2) we get a 428 from the server, flagging the message for spam (3) some other
        //   reason not known at the time of this writing.
        //
        // This awkward callback lets us hold onto errors we might want to save, so we can
        //   decide whether to save them later on.
        const saveErrors = isFinalAttempt
            ? undefined
            : (errors) => {
                messageSendErrors = errors;
            };
        if (!shouldContinue) {
            log.info(`message ${messageId} ran out of time. Giving up on sending it`);
            await markMessageFailed(message, messageSendErrors);
            return;
        }
        try {
            const conversation = message.getConversation();
            if (!conversation) {
                throw new Error(`could not find conversation for message with ID ${messageId}`);
            }
            const { allRecipientIdentifiers, recipientIdentifiersWithoutMe, untrustedConversationIds, } = getMessageRecipients({
                message,
                conversation,
            });
            if (untrustedConversationIds.length) {
                log.info(`message ${messageId} sending blocked because ${untrustedConversationIds.length} conversation(s) were untrusted. Giving up on the job, but it may be reborn later`);
                window.reduxActions.conversations.messageStoppedByMissingVerification(messageId, untrustedConversationIds);
                return;
            }
            if (!allRecipientIdentifiers.length) {
                log.warn(`trying to send message ${messageId} but it looks like it was already sent to everyone. This is unexpected, but we're giving up`);
                return;
            }
            const { attachments, body, deletedForEveryoneTimestamp, expireTimer, mentions, messageTimestamp, preview, profileKey, quote, sticker, } = await getMessageSendData({ conversation, log, message });
            let messageSendPromise;
            if (recipientIdentifiersWithoutMe.length === 0) {
                log.info('sending sync message only');
                const dataMessage = await window.textsecure.messaging.getDataMessage({
                    attachments,
                    body,
                    groupV2: conversation.getGroupV2Info({
                        members: recipientIdentifiersWithoutMe,
                    }),
                    deletedForEveryoneTimestamp,
                    expireTimer,
                    preview,
                    profileKey,
                    quote,
                    recipients: allRecipientIdentifiers,
                    sticker,
                    timestamp: messageTimestamp,
                });
                messageSendPromise = message.sendSyncMessageOnly(dataMessage, saveErrors);
            }
            else {
                const conversationType = conversation.get('type');
                const sendOptions = await (0, getSendOptions_1.getSendOptions)(conversation.attributes);
                const { ContentHint } = protobuf_1.SignalService.UnidentifiedSenderMessage.Message;
                let innerPromise;
                if (conversationType === Message.GROUP) {
                    log.info('sending group message');
                    innerPromise = conversation.queueJob('normalMessageSendJobQueue', () => window.Signal.Util.sendToGroup({
                        groupSendOptions: {
                            attachments,
                            deletedForEveryoneTimestamp,
                            expireTimer,
                            groupV1: conversation.getGroupV1Info(recipientIdentifiersWithoutMe),
                            groupV2: conversation.getGroupV2Info({
                                members: recipientIdentifiersWithoutMe,
                            }),
                            messageText: body,
                            preview,
                            profileKey,
                            quote,
                            sticker,
                            timestamp: messageTimestamp,
                            mentions,
                        },
                        conversation,
                        contentHint: ContentHint.RESENDABLE,
                        messageId,
                        sendOptions,
                        sendType: 'message',
                    }));
                }
                else {
                    log.info('sending direct message');
                    innerPromise = window.textsecure.messaging.sendMessageToIdentifier({
                        identifier: recipientIdentifiersWithoutMe[0],
                        messageText: body,
                        attachments,
                        quote,
                        preview,
                        sticker,
                        reaction: undefined,
                        deletedForEveryoneTimestamp,
                        timestamp: messageTimestamp,
                        expireTimer,
                        contentHint: ContentHint.RESENDABLE,
                        groupId: undefined,
                        profileKey,
                        options: sendOptions,
                    });
                }
                messageSendPromise = message.send((0, handleMessageSend_1.handleMessageSend)(innerPromise, {
                    messageIds: [messageId],
                    sendType: 'message',
                }), saveErrors);
            }
            await messageSendPromise;
            if ((0, message_1.getLastChallengeError)({
                errors: messageSendErrors,
            })) {
                log.info(`message ${messageId} hit a spam challenge. Not retrying any more`);
                await message.saveErrors(messageSendErrors);
                return;
            }
            const didFullySend = !messageSendErrors.length || didSendToEveryone(message);
            if (!didFullySend) {
                throw new Error('message did not fully send');
            }
        }
        catch (err) {
            const formattedMessageSendErrors = [];
            let serverAskedUsToStop = false;
            let retryAfterError;
            messageSendErrors.forEach((messageSendError) => {
                formattedMessageSendErrors.push(Errors.toLogFormat(messageSendError));
                switch ((0, getHttpErrorCode_1.getHttpErrorCode)(messageSendError)) {
                    case 413:
                        retryAfterError || (retryAfterError = messageSendError);
                        break;
                    case 508:
                        serverAskedUsToStop = true;
                        break;
                    default:
                        break;
                }
            });
            log.info(`${messageSendErrors.length} message send error(s): ${formattedMessageSendErrors.join(',')}`);
            if (isFinalAttempt || serverAskedUsToStop) {
                await markMessageFailed(message, messageSendErrors);
            }
            if (serverAskedUsToStop) {
                log.info('server responded with 508. Giving up on this job');
                return;
            }
            if (!isFinalAttempt && retryAfterError) {
                await (0, sleepFor413RetryAfterTime_1.sleepFor413RetryAfterTime)({
                    err: retryAfterError,
                    log,
                    timeRemaining,
                });
            }
            throw err;
        }
    }
}
exports.NormalMessageSendJobQueue = NormalMessageSendJobQueue;
exports.normalMessageSendJobQueue = new NormalMessageSendJobQueue({
    store: JobQueueDatabaseStore_1.jobQueueDatabaseStore,
    queueType: 'normal message send',
    maxAttempts: MAX_ATTEMPTS,
});
function getMessageRecipients({ conversation, message, }) {
    const allRecipientIdentifiers = [];
    const recipientIdentifiersWithoutMe = [];
    const untrustedConversationIds = [];
    const currentConversationRecipients = conversation.getRecipientConversationIds();
    Object.entries(message.get('sendStateByConversationId') || {}).forEach(([recipientConversationId, sendState]) => {
        if ((0, MessageSendState_1.isSent)(sendState.status)) {
            return;
        }
        const recipient = window.ConversationController.get(recipientConversationId);
        if (!recipient) {
            return;
        }
        const isRecipientMe = (0, whatTypeOfConversation_1.isMe)(recipient.attributes);
        if (!currentConversationRecipients.has(recipientConversationId) &&
            !isRecipientMe) {
            return;
        }
        if (recipient.isUntrusted()) {
            untrustedConversationIds.push(recipientConversationId);
        }
        const recipientIdentifier = recipient.getSendTarget();
        if (!recipientIdentifier) {
            return;
        }
        allRecipientIdentifiers.push(recipientIdentifier);
        if (!isRecipientMe) {
            recipientIdentifiersWithoutMe.push(recipientIdentifier);
        }
    });
    return {
        allRecipientIdentifiers,
        recipientIdentifiersWithoutMe,
        untrustedConversationIds,
    };
}
async function getMessageSendData({ conversation, log, message, }) {
    var _a;
    let messageTimestamp;
    const sentAt = message.get('sent_at');
    const timestamp = message.get('timestamp');
    if (sentAt) {
        messageTimestamp = sentAt;
    }
    else if (timestamp) {
        log.error('message lacked sent_at. Falling back to timestamp');
        messageTimestamp = timestamp;
    }
    else {
        log.error('message lacked sent_at and timestamp. Falling back to current time');
        messageTimestamp = Date.now();
    }
    const [attachmentsWithData, preview, quote, sticker, profileKey] = await Promise.all([
        // We don't update the caches here because (1) we expect the caches to be populated
        //   on initial send, so they should be there in the 99% case (2) if you're retrying
        //   a failed message across restarts, we don't touch the cache for simplicity. If
        //   sends are failing, let's not add the complication of a cache.
        Promise.all(((_a = message.get('attachments')) !== null && _a !== void 0 ? _a : []).map(loadAttachmentData)),
        message.cachedOutgoingPreviewData ||
            loadPreviewData(message.get('preview')),
        message.cachedOutgoingQuoteData || loadQuoteData(message.get('quote')),
        message.cachedOutgoingStickerData ||
            loadStickerData(message.get('sticker')),
        conversation.get('profileSharing')
            ? ourProfileKey_1.ourProfileKeyService.get()
            : undefined,
    ]);
    const { body, attachments } = window.Whisper.Message.getLongMessageAttachment({
        body: message.get('body'),
        attachments: attachmentsWithData,
        now: messageTimestamp,
    });
    return {
        attachments,
        body,
        deletedForEveryoneTimestamp: message.get('deletedForEveryoneTimestamp'),
        expireTimer: message.get('expireTimer'),
        mentions: message.get('bodyRanges'),
        messageTimestamp,
        preview,
        profileKey,
        quote,
        sticker,
    };
}
async function markMessageFailed(message, errors) {
    message.markFailed();
    message.saveErrors(errors, { skipSave: true });
    await window.Signal.Data.saveMessage(message.attributes);
}
function didSendToEveryone(message) {
    const sendStateByConversationId = message.get('sendStateByConversationId') || {};
    return Object.values(sendStateByConversationId).every(sendState => (0, MessageSendState_1.isSent)(sendState.status));
}
