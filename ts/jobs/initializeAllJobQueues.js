"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.initializeAllJobQueues = void 0;
const normalMessageSendJobQueue_1 = require("./normalMessageSendJobQueue");
const reactionJobQueue_1 = require("./reactionJobQueue");
const readSyncJobQueue_1 = require("./readSyncJobQueue");
const removeStorageKeyJobQueue_1 = require("./removeStorageKeyJobQueue");
const reportSpamJobQueue_1 = require("./reportSpamJobQueue");
const viewSyncJobQueue_1 = require("./viewSyncJobQueue");
const viewedReceiptsJobQueue_1 = require("./viewedReceiptsJobQueue");
/**
 * Start all of the job queues. Should be called when the database is ready.
 */
function initializeAllJobQueues({ server, }) {
    reportSpamJobQueue_1.reportSpamJobQueue.initialize({ server });
    normalMessageSendJobQueue_1.normalMessageSendJobQueue.streamJobs();
    reactionJobQueue_1.reactionJobQueue.streamJobs();
    readSyncJobQueue_1.readSyncJobQueue.streamJobs();
    removeStorageKeyJobQueue_1.removeStorageKeyJobQueue.streamJobs();
    reportSpamJobQueue_1.reportSpamJobQueue.streamJobs();
    viewSyncJobQueue_1.viewSyncJobQueue.streamJobs();
    viewedReceiptsJobQueue_1.viewedReceiptsJobQueue.streamJobs();
}
exports.initializeAllJobQueues = initializeAllJobQueues;
