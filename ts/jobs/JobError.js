"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobError = void 0;
const reallyJsonStringify_1 = require("../util/reallyJsonStringify");
/**
 * An error that wraps job errors.
 *
 * Should not be instantiated directly, except by `JobQueue`.
 */
class JobError extends Error {
    constructor(lastErrorThrownByJob) {
        super(`Job failed. Last error: ${formatError(lastErrorThrownByJob)}`);
        this.lastErrorThrownByJob = lastErrorThrownByJob;
    }
}
exports.JobError = JobError;
function formatError(err) {
    if (err instanceof Error) {
        return err.message;
    }
    return (0, reallyJsonStringify_1.reallyJsonStringify)(err);
}
