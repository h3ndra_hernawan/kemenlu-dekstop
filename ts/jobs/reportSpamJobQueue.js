"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reportSpamJobQueue = exports.ReportSpamJobQueue = void 0;
const z = __importStar(require("zod"));
const durations = __importStar(require("../util/durations"));
const assert_1 = require("../util/assert");
const waitForOnline_1 = require("../util/waitForOnline");
const registration_1 = require("../util/registration");
const iterables_1 = require("../util/iterables");
const sleep_1 = require("../util/sleep");
const JobQueue_1 = require("./JobQueue");
const JobQueueDatabaseStore_1 = require("./JobQueueDatabaseStore");
const parseIntWithFallback_1 = require("../util/parseIntWithFallback");
const Errors_1 = require("../textsecure/Errors");
const RETRY_WAIT_TIME = durations.MINUTE;
const RETRYABLE_4XX_FAILURE_STATUSES = new Set([
    404, 408, 410, 412, 413, 414, 417, 423, 424, 425, 426, 428, 429, 431, 449,
]);
const is4xxStatus = (code) => code >= 400 && code <= 499;
const is5xxStatus = (code) => code >= 500 && code <= 599;
const isRetriable4xxStatus = (code) => RETRYABLE_4XX_FAILURE_STATUSES.has(code);
const reportSpamJobDataSchema = z.object({
    e164: z.string().min(1),
    serverGuids: z.string().array().min(1).max(1000),
});
class ReportSpamJobQueue extends JobQueue_1.JobQueue {
    initialize({ server }) {
        this.server = server;
    }
    parseData(data) {
        return reportSpamJobDataSchema.parse(data);
    }
    async run({ data }, { log }) {
        const { e164, serverGuids } = data;
        await new Promise(resolve => {
            window.storage.onready(resolve);
        });
        if (!(0, registration_1.isDone)()) {
            log.info("reportSpamJobQueue: skipping this job because we're unlinked");
            return;
        }
        await (0, waitForOnline_1.waitForOnline)(window.navigator, window);
        const { server } = this;
        (0, assert_1.strictAssert)(server !== undefined, 'ReportSpamJobQueue not initialized');
        try {
            await Promise.all((0, iterables_1.map)(serverGuids, serverGuid => server.reportMessage(e164, serverGuid)));
        }
        catch (err) {
            if (!(err instanceof Errors_1.HTTPError)) {
                throw err;
            }
            const code = (0, parseIntWithFallback_1.parseIntWithFallback)(err.code, -1);
            // This is an unexpected case, except for -1, which can happen for network failures.
            if (code < 400) {
                throw err;
            }
            if (code === 508) {
                log.info('reportSpamJobQueue: server responded with 508. Giving up on this job');
                return;
            }
            if (isRetriable4xxStatus(code) || is5xxStatus(code)) {
                log.info(`reportSpamJobQueue: server responded with ${code} status code. Sleeping before our next attempt`);
                await (0, sleep_1.sleep)(RETRY_WAIT_TIME);
                throw err;
            }
            if (is4xxStatus(code)) {
                log.error(`reportSpamJobQueue: server responded with ${code} status code. Giving up on this job`);
                return;
            }
            throw err;
        }
    }
}
exports.ReportSpamJobQueue = ReportSpamJobQueue;
exports.reportSpamJobQueue = new ReportSpamJobQueue({
    store: JobQueueDatabaseStore_1.jobQueueDatabaseStore,
    queueType: 'report spam',
    maxAttempts: 25,
});
