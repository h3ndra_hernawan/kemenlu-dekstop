"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InMemoryQueues = void 0;
const p_queue_1 = __importDefault(require("p-queue"));
class InMemoryQueues {
    constructor() {
        this.queues = new Map();
    }
    get(key) {
        const existingQueue = this.queues.get(key);
        if (existingQueue) {
            return existingQueue;
        }
        const newQueue = new p_queue_1.default({ concurrency: 1 });
        newQueue.once('idle', () => {
            this.queues.delete(key);
        });
        this.queues.set(key, newQueue);
        return newQueue;
    }
}
exports.InMemoryQueues = InMemoryQueues;
