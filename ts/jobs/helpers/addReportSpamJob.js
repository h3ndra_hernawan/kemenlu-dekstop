"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addReportSpamJob = void 0;
const assert_1 = require("../../util/assert");
const log = __importStar(require("../../logging/log"));
async function addReportSpamJob({ conversation, getMessageServerGuidsForSpam, jobQueue, }) {
    (0, assert_1.assert)(conversation.type === 'direct', 'addReportSpamJob: cannot report spam for non-direct conversations');
    const { e164 } = conversation;
    if (!e164) {
        log.info('addReportSpamJob got a conversation with no E164, which the server does not support. Doing nothing');
        return;
    }
    const serverGuids = await getMessageServerGuidsForSpam(conversation.id);
    if (!serverGuids.length) {
        // This can happen under normal conditions. We haven't always stored server GUIDs, so
        //   a user might try to report spam for a conversation that doesn't have them. (It
        //   may also indicate developer error, but that's not necessarily the case.)
        log.info('addReportSpamJob got no server GUIDs from the database. Doing nothing');
        return;
    }
    await jobQueue.add({ e164, serverGuids });
}
exports.addReportSpamJob = addReportSpamJob;
