"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.commonShouldJobContinue = void 0;
const waitForOnline_1 = require("../../util/waitForOnline");
const sleep_1 = require("../../util/sleep");
const exponentialBackoff_1 = require("../../util/exponentialBackoff");
const registration_1 = require("../../util/registration");
async function commonShouldJobContinue({ attempt, log, timeRemaining, }) {
    if (timeRemaining <= 0) {
        log.info("giving up because it's been too long");
        return false;
    }
    try {
        await (0, waitForOnline_1.waitForOnline)(window.navigator, window, { timeout: timeRemaining });
    }
    catch (err) {
        log.info("didn't come online in time, giving up");
        return false;
    }
    await new Promise(resolve => {
        window.storage.onready(resolve);
    });
    if (!(0, registration_1.isDone)()) {
        log.info("skipping this job because we're unlinked");
        return false;
    }
    const sleepTime = (0, exponentialBackoff_1.exponentialBackoffSleepTime)(attempt);
    log.info(`sleeping for ${sleepTime}`);
    await (0, sleep_1.sleep)(sleepTime);
    return true;
}
exports.commonShouldJobContinue = commonShouldJobContinue;
