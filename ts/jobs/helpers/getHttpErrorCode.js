"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getHttpErrorCode = void 0;
const isRecord_1 = require("../../util/isRecord");
const parseIntWithFallback_1 = require("../../util/parseIntWithFallback");
/**
 * Looks for an HTTP code. First tries the top level error, then looks at its `httpError`
 * property.
 */
function getHttpErrorCode(maybeError) {
    if (!(0, isRecord_1.isRecord)(maybeError)) {
        return -1;
    }
    const maybeTopLevelCode = (0, parseIntWithFallback_1.parseIntWithFallback)(maybeError.code, -1);
    if (maybeTopLevelCode !== -1) {
        return maybeTopLevelCode;
    }
    const { httpError } = maybeError;
    if (!(0, isRecord_1.isRecord)(httpError)) {
        return -1;
    }
    return (0, parseIntWithFallback_1.parseIntWithFallback)(httpError.code, -1);
}
exports.getHttpErrorCode = getHttpErrorCode;
