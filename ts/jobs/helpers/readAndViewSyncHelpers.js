"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.runReadOrViewSyncJob = exports.parseRawSyncDataArray = void 0;
const lodash_1 = require("lodash");
const getSendOptions_1 = require("../../util/getSendOptions");
const handleMessageSend_1 = require("../../util/handleMessageSend");
const isNotNil_1 = require("../../util/isNotNil");
const assert_1 = require("../../util/assert");
const isRecord_1 = require("../../util/isRecord");
const commonShouldJobContinue_1 = require("./commonShouldJobContinue");
const handleCommonJobRequestError_1 = require("./handleCommonJobRequestError");
const CHUNK_SIZE = 100;
/**
 * Parse what _should_ be an array of `SyncType`s.
 *
 * Notably, `null`s made it into the job system and caused jobs to fail. This cleans that
 * up in addition to validating the data.
 */
function parseRawSyncDataArray(value) {
    (0, assert_1.strictAssert)(Array.isArray(value), 'syncs are not an array');
    return value.map((item) => {
        (0, assert_1.strictAssert)((0, isRecord_1.isRecord)(item), 'sync is not an object');
        const { messageId, senderE164, senderUuid, timestamp } = item;
        (0, assert_1.strictAssert)(typeof timestamp === 'number', 'timestamp should be a number');
        return {
            messageId: parseOptionalString('messageId', messageId),
            senderE164: parseOptionalString('senderE164', senderE164),
            senderUuid: parseOptionalString('senderUuid', senderUuid),
            timestamp,
        };
    });
}
exports.parseRawSyncDataArray = parseRawSyncDataArray;
function parseOptionalString(name, value) {
    if (typeof value === 'string') {
        return value;
    }
    if (value === undefined || value === null) {
        return undefined;
    }
    throw new Error(`${name} was not a string`);
}
async function runReadOrViewSyncJob({ attempt, isView, log, maxRetryTime, syncs, timestamp, }) {
    let sendType;
    let doSync;
    if (isView) {
        sendType = 'viewSync';
        doSync = window.textsecure.messaging.syncView.bind(window.textsecure.messaging);
    }
    else {
        sendType = 'readSync';
        doSync = window.textsecure.messaging.syncReadMessages.bind(window.textsecure.messaging);
    }
    if (!syncs.length) {
        log.info("skipping this job because there's nothing to sync");
        return;
    }
    const timeRemaining = timestamp + maxRetryTime - Date.now();
    const shouldContinue = await (0, commonShouldJobContinue_1.commonShouldJobContinue)({
        attempt,
        log,
        timeRemaining,
    });
    if (!shouldContinue) {
        return;
    }
    const ourConversation = window.ConversationController.getOurConversationOrThrow();
    const sendOptions = await (0, getSendOptions_1.getSendOptions)(ourConversation.attributes, {
        syncMessage: true,
    });
    try {
        await Promise.all((0, lodash_1.chunk)(syncs, CHUNK_SIZE).map(batch => {
            const messageIds = batch.map(item => item.messageId).filter(isNotNil_1.isNotNil);
            return (0, handleMessageSend_1.handleMessageSend)(doSync(batch, sendOptions), {
                messageIds,
                sendType,
            });
        }));
    }
    catch (err) {
        await (0, handleCommonJobRequestError_1.handleCommonJobRequestError)({ err, log, timeRemaining });
    }
}
exports.runReadOrViewSyncJob = runReadOrViewSyncJob;
