"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleCommonJobRequestError = void 0;
const sleepFor413RetryAfterTime_1 = require("./sleepFor413RetryAfterTime");
const getHttpErrorCode_1 = require("./getHttpErrorCode");
async function handleCommonJobRequestError({ err, log, timeRemaining, }) {
    switch ((0, getHttpErrorCode_1.getHttpErrorCode)(err)) {
        case 413:
            await (0, sleepFor413RetryAfterTime_1.sleepFor413RetryAfterTime)({ err, log, timeRemaining });
            return;
        case 508:
            log.info('server responded with 508. Giving up on this job');
            return;
        default:
            throw err;
    }
}
exports.handleCommonJobRequestError = handleCommonJobRequestError;
