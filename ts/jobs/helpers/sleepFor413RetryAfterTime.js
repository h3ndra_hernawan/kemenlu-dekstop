"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.sleepFor413RetryAfterTime = void 0;
const sleep_1 = require("../../util/sleep");
const parseRetryAfter_1 = require("../../util/parseRetryAfter");
const isRecord_1 = require("../../util/isRecord");
const Errors_1 = require("../../textsecure/Errors");
async function sleepFor413RetryAfterTime({ err, log, timeRemaining, }) {
    if (timeRemaining <= 0) {
        return;
    }
    const retryAfter = Math.min((0, parseRetryAfter_1.parseRetryAfter)(findRetryAfterTime(err)), timeRemaining);
    log.info(`Got a 413 response code. Sleeping for ${retryAfter} millisecond(s)`);
    await (0, sleep_1.sleep)(retryAfter);
}
exports.sleepFor413RetryAfterTime = sleepFor413RetryAfterTime;
function findRetryAfterTime(err) {
    var _a;
    if (!(0, isRecord_1.isRecord)(err)) {
        return undefined;
    }
    if ((0, isRecord_1.isRecord)(err.responseHeaders)) {
        return err.responseHeaders['retry-after'];
    }
    if (err.httpError instanceof Errors_1.HTTPError) {
        return (_a = err.httpError.responseHeaders) === null || _a === void 0 ? void 0 : _a['retry-after'];
    }
    return undefined;
}
