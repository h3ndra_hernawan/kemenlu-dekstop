"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobQueue = void 0;
const p_queue_1 = __importDefault(require("p-queue"));
const uuid_1 = require("uuid");
const lodash_1 = require("lodash");
const Job_1 = require("./Job");
const JobError_1 = require("./JobError");
const assert_1 = require("../util/assert");
const log = __importStar(require("../logging/log"));
const JobLogger_1 = require("./JobLogger");
const Errors = __importStar(require("../types/errors"));
const noopOnCompleteCallbacks = {
    resolve: lodash_1.noop,
    reject: lodash_1.noop,
};
class JobQueue {
    constructor(options) {
        var _a;
        this.onCompleteCallbacks = new Map();
        this.defaultInMemoryQueue = new p_queue_1.default();
        this.started = false;
        (0, assert_1.assert)(Number.isInteger(options.maxAttempts) && options.maxAttempts >= 1, 'maxAttempts should be a positive integer');
        (0, assert_1.assert)(options.maxAttempts <= Number.MAX_SAFE_INTEGER, 'maxAttempts is too large');
        (0, assert_1.assert)(options.queueType.trim().length, 'queueType should be a non-blank string');
        this.maxAttempts = options.maxAttempts;
        this.queueType = options.queueType;
        this.store = options.store;
        this.logger = (_a = options.logger) !== null && _a !== void 0 ? _a : log;
        this.logPrefix = `${this.queueType} job queue:`;
    }
    /**
     * Start streaming jobs from the store.
     */
    async streamJobs() {
        var e_1, _a;
        if (this.started) {
            throw new Error(`${this.logPrefix} should not start streaming more than once`);
        }
        this.started = true;
        log.info(`${this.logPrefix} starting to stream jobs`);
        const stream = this.store.stream(this.queueType);
        try {
            for (var stream_1 = __asyncValues(stream), stream_1_1; stream_1_1 = await stream_1.next(), !stream_1_1.done;) {
                const storedJob = stream_1_1.value;
                this.enqueueStoredJob(storedJob);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (stream_1_1 && !stream_1_1.done && (_a = stream_1.return)) await _a.call(stream_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
    }
    /**
     * Add a job, which should cause it to be enqueued and run.
     *
     * If `streamJobs` has not been called yet, this will throw an error.
     *
     * You can override `insert` to change the way the job is added to the database. This is
     * useful if you're trying to save a message and a job in the same database transaction.
     */
    async add(data, insert) {
        if (!this.started) {
            throw new Error(`${this.logPrefix} has not started streaming. Make sure to call streamJobs().`);
        }
        const job = this.createJob(data);
        if (insert) {
            await insert(job);
        }
        await this.store.insert(job, { shouldPersist: !insert });
        log.info(`${this.logPrefix} added new job ${job.id}`);
        return job;
    }
    createJob(data) {
        const id = (0, uuid_1.v4)();
        const timestamp = Date.now();
        const completionPromise = new Promise((resolve, reject) => {
            this.onCompleteCallbacks.set(id, { resolve, reject });
        });
        const completion = (async () => {
            try {
                await completionPromise;
            }
            catch (err) {
                throw new JobError_1.JobError(err);
            }
            finally {
                this.onCompleteCallbacks.delete(id);
            }
        })();
        return new Job_1.Job(id, timestamp, this.queueType, data, completion);
    }
    getInMemoryQueue(_parsedJob) {
        return this.defaultInMemoryQueue;
    }
    async enqueueStoredJob(storedJob) {
        (0, assert_1.assert)(storedJob.queueType === this.queueType, 'Received a mis-matched queue type');
        log.info(`${this.logPrefix} enqueuing job ${storedJob.id}`);
        // It's okay if we don't have a callback; that likely means the job was created before
        //   the process was started (e.g., from a previous run).
        const { resolve, reject } = this.onCompleteCallbacks.get(storedJob.id) || noopOnCompleteCallbacks;
        let parsedData;
        try {
            parsedData = this.parseData(storedJob.data);
        }
        catch (err) {
            log.error(`${this.logPrefix} failed to parse data for job ${storedJob.id}`);
            reject(new Error('Failed to parse job data. Was unexpected data loaded from the database?'));
            return;
        }
        const parsedJob = Object.assign(Object.assign({}, storedJob), { data: parsedData });
        const queue = this.getInMemoryQueue(parsedJob);
        const logger = new JobLogger_1.JobLogger(parsedJob, this.logger);
        const result = await queue.add(async () => {
            for (let attempt = 1; attempt <= this.maxAttempts; attempt += 1) {
                const isFinalAttempt = attempt === this.maxAttempts;
                logger.attempt = attempt;
                log.info(`${this.logPrefix} running job ${storedJob.id}, attempt ${attempt} of ${this.maxAttempts}`);
                try {
                    // We want an `await` in the loop, as we don't want a single job running more
                    //   than once at a time. Ideally, the job will succeed on the first attempt.
                    // eslint-disable-next-line no-await-in-loop
                    await this.run(parsedJob, { attempt, log: logger });
                    log.info(`${this.logPrefix} job ${storedJob.id} succeeded on attempt ${attempt}`);
                    return { success: true };
                }
                catch (err) {
                    log.error(`${this.logPrefix} job ${storedJob.id} failed on attempt ${attempt}. ${Errors.toLogFormat(err)}`);
                    if (isFinalAttempt) {
                        return { success: false, err };
                    }
                }
            }
            // This should never happen. See the assertion below.
            return undefined;
        });
        await this.store.delete(storedJob.id);
        (0, assert_1.assert)(result, 'The job never ran. This indicates a developer error in the job queue');
        if (result.success) {
            resolve();
        }
        else {
            reject(result.err);
        }
    }
}
exports.JobQueue = JobQueue;
