"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.hide = exports.show = void 0;
const show = (element) => {
    const container = document.querySelector('.lightbox-container');
    if (!container) {
        throw new TypeError("'.lightbox-container' is required");
    }
    container.innerHTML = '';
    container.style.display = 'block';
    container.appendChild(element);
};
exports.show = show;
const hide = () => {
    const container = document.querySelector('.lightbox-container');
    if (!container) {
        return;
    }
    container.innerHTML = '';
    container.style.display = 'none';
};
exports.hide = hide;
