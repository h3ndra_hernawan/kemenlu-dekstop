"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.NativeThemeNotifier = void 0;
const electron_1 = require("electron");
function getState() {
    return {
        shouldUseDarkColors: electron_1.nativeTheme.shouldUseDarkColors,
    };
}
class NativeThemeNotifier {
    constructor() {
        this.listeners = new Set();
    }
    initialize() {
        electron_1.nativeTheme.on('updated', () => {
            this.notifyListeners();
        });
        electron_1.ipcMain.on('native-theme:init', event => {
            // eslint-disable-next-line no-param-reassign
            event.returnValue = getState();
        });
    }
    addWindow(window) {
        if (this.listeners.has(window)) {
            return;
        }
        this.listeners.add(window);
        window.once('closed', () => {
            this.listeners.delete(window);
        });
    }
    notifyListeners() {
        for (const window of this.listeners) {
            window.webContents.send('native-theme:changed', getState());
        }
    }
}
exports.NativeThemeNotifier = NativeThemeNotifier;
