"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
/* eslint-disable no-console */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeMainHandler = void 0;
const electron_1 = require("electron");
class ChallengeMainHandler {
    constructor() {
        this.handlers = [];
        this.initialize();
    }
    handleCaptcha(captcha) {
        const response = { captcha };
        const { handlers } = this;
        this.handlers = [];
        for (const resolve of handlers) {
            resolve(response);
        }
    }
    async onRequest(event, request) {
        console.log('Received challenge request, waiting for response');
        const data = await new Promise(resolve => {
            this.handlers.push(resolve);
        });
        console.log('Sending challenge response', data);
        const ipcResponse = {
            seq: request.seq,
            data,
        };
        event.sender.send('challenge:response', ipcResponse);
    }
    initialize() {
        electron_1.ipcMain.on('challenge:request', (event, request) => {
            this.onRequest(event, request);
        });
    }
}
exports.ChallengeMainHandler = ChallengeMainHandler;
