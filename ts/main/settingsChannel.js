"use strict";
// Copyright 2017-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingsChannel = void 0;
const electron_1 = require("electron");
const user_config_1 = require("../../app/user_config");
const ephemeral_config_1 = require("../../app/ephemeral_config");
const permissions_1 = require("../../app/permissions");
const assert_1 = require("../util/assert");
const explodePromise_1 = require("../util/explodePromise");
const EPHEMERAL_NAME_MAP = new Map([
    ['spellCheck', 'spell-check'],
    ['systemTraySetting', 'system-tray-setting'],
]);
class SettingsChannel {
    constructor() {
        this.responseQueue = new Map();
        this.responseSeq = 0;
    }
    setMainWindow(mainWindow) {
        this.mainWindow = mainWindow;
    }
    getMainWindow() {
        return this.mainWindow;
    }
    install() {
        this.installSetting('deviceName', { setter: false });
        // ChatColorPicker redux hookups
        this.installCallback('getCustomColors');
        this.installCallback('getConversationsWithCustomColor');
        this.installCallback('resetAllChatColors');
        this.installCallback('resetDefaultChatColor');
        this.installCallback('addCustomColor');
        this.installCallback('editCustomColor');
        this.installCallback('removeCustomColor');
        this.installCallback('removeCustomColorOnConversations');
        this.installCallback('setGlobalDefaultConversationColor');
        this.installCallback('getDefaultConversationColor');
        // Various callbacks
        this.installCallback('getAvailableIODevices');
        this.installCallback('isPrimary');
        this.installCallback('syncRequest');
        this.installCallback('isPhoneNumberSharingEnabled');
        // Getters only. These are set by the primary device
        this.installSetting('blockedCount', { setter: false });
        this.installSetting('linkPreviewSetting', { setter: false });
        this.installSetting('phoneNumberDiscoverabilitySetting', { setter: false });
        this.installSetting('phoneNumberSharingSetting', { setter: false });
        this.installSetting('readReceiptSetting', { setter: false });
        this.installSetting('typingIndicatorSetting', { setter: false });
        this.installSetting('themeSetting');
        this.installSetting('hideMenuBar');
        this.installSetting('systemTraySetting', {
            isEphemeral: true,
        });
        this.installSetting('notificationSetting');
        this.installSetting('notificationDrawAttention');
        this.installSetting('audioNotification');
        this.installSetting('countMutedConversations');
        this.installSetting('spellCheck', {
            isEphemeral: true,
        });
        this.installSetting('autoDownloadUpdate');
        this.installSetting('autoLaunch');
        this.installSetting('alwaysRelayCalls');
        this.installSetting('callRingtoneNotification');
        this.installSetting('callSystemNotification');
        this.installSetting('incomingCallNotification');
        // Media settings
        this.installSetting('preferredAudioInputDevice');
        this.installSetting('preferredAudioOutputDevice');
        this.installSetting('preferredVideoInputDevice');
        this.installSetting('lastSyncTime');
        this.installSetting('universalExpireTimer');
        this.installSetting('zoomFactor');
        (0, permissions_1.installPermissionsHandler)({ session: electron_1.session, userConfig: user_config_1.userConfig });
        // These ones are different because its single source of truth is userConfig,
        // not IndexedDB
        electron_1.ipcMain.handle('settings:get:mediaPermissions', () => {
            return user_config_1.userConfig.get('mediaPermissions') || false;
        });
        electron_1.ipcMain.handle('settings:get:mediaCameraPermissions', () => {
            return user_config_1.userConfig.get('mediaCameraPermissions') || false;
        });
        electron_1.ipcMain.handle('settings:set:mediaPermissions', (_event, value) => {
            user_config_1.userConfig.set('mediaPermissions', value);
            // We reinstall permissions handler to ensure that a revoked permission takes effect
            (0, permissions_1.installPermissionsHandler)({ session: electron_1.session, userConfig: user_config_1.userConfig });
        });
        electron_1.ipcMain.handle('settings:set:mediaCameraPermissions', (_event, value) => {
            user_config_1.userConfig.set('mediaCameraPermissions', value);
            // We reinstall permissions handler to ensure that a revoked permission takes effect
            (0, permissions_1.installPermissionsHandler)({ session: electron_1.session, userConfig: user_config_1.userConfig });
        });
        electron_1.ipcMain.on('settings:response', (_event, seq, error, value) => {
            const entry = this.responseQueue.get(seq);
            this.responseQueue.delete(seq);
            if (!entry) {
                return;
            }
            const { resolve, reject } = entry;
            if (error) {
                reject(error);
            }
            else {
                resolve(value);
            }
        });
    }
    waitForResponse() {
        const seq = this.responseSeq;
        // eslint-disable-next-line no-bitwise
        this.responseSeq = (this.responseSeq + 1) & 0x7fffffff;
        const { promise, resolve, reject } = (0, explodePromise_1.explodePromise)();
        this.responseQueue.set(seq, { resolve, reject });
        return { seq, promise };
    }
    getSettingFromMainWindow(name) {
        const { mainWindow } = this;
        if (!mainWindow || !mainWindow.webContents) {
            throw new Error('No main window');
        }
        const { seq, promise } = this.waitForResponse();
        mainWindow.webContents.send(`settings:get:${name}`, { seq });
        return promise;
    }
    setSettingInMainWindow(name, value) {
        const { mainWindow } = this;
        if (!mainWindow || !mainWindow.webContents) {
            throw new Error('No main window');
        }
        const { seq, promise } = this.waitForResponse();
        mainWindow.webContents.send(`settings:set:${name}`, { seq, value });
        return promise;
    }
    invokeCallbackInMainWindow(name, args) {
        const { mainWindow } = this;
        if (!mainWindow || !mainWindow.webContents) {
            throw new Error('Main window not found');
        }
        const { seq, promise } = this.waitForResponse();
        mainWindow.webContents.send(`settings:call:${name}`, { seq, args });
        return promise;
    }
    installCallback(name) {
        electron_1.ipcMain.handle(`settings:call:${name}`, async (_event, args) => {
            return this.invokeCallbackInMainWindow(name, args);
        });
    }
    installSetting(name, { getter = true, setter = true, isEphemeral = false, } = {}) {
        if (getter) {
            electron_1.ipcMain.handle(`settings:get:${name}`, async () => {
                return this.getSettingFromMainWindow(name);
            });
        }
        if (!setter) {
            return;
        }
        electron_1.ipcMain.handle(`settings:set:${name}`, (_event, value) => {
            if (isEphemeral) {
                const ephemeralName = EPHEMERAL_NAME_MAP.get(name);
                (0, assert_1.strictAssert)(ephemeralName !== undefined, `${name} is not an ephemeral setting`);
                ephemeral_config_1.ephemeralConfig.set(ephemeralName, value);
            }
            return this.setSettingInMainWindow(name, value);
        });
    }
}
exports.SettingsChannel = SettingsChannel;
