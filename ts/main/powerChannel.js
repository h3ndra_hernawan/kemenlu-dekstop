"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.PowerChannel = void 0;
const electron_1 = require("electron");
class PowerChannel {
    static initialize({ send }) {
        if (PowerChannel.isInitialized) {
            throw new Error('PowerChannel already initialized');
        }
        PowerChannel.isInitialized = true;
        electron_1.powerMonitor.on('suspend', () => {
            send('power-channel:suspend');
        });
        electron_1.powerMonitor.on('resume', () => {
            send('power-channel:resume');
        });
    }
}
exports.PowerChannel = PowerChannel;
PowerChannel.isInitialized = false;
