"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewlyCreatedGroupInvitedContactsDialog = void 0;
const react_1 = __importDefault(require("react"));
const Intl_1 = require("./Intl");
const ContactName_1 = require("./conversation/ContactName");
const GroupDialog_1 = require("./GroupDialog");
const openLinkInWebBrowser_1 = require("../util/openLinkInWebBrowser");
const NewlyCreatedGroupInvitedContactsDialog = ({ contacts, i18n, onClose }) => {
    let title;
    let body;
    if (contacts.length === 1) {
        const contact = contacts[0];
        title = i18n('NewlyCreatedGroupInvitedContactsDialog--title--one');
        body = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(GroupDialog_1.GroupDialog.Paragraph, null,
                react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "NewlyCreatedGroupInvitedContactsDialog--body--user-paragraph--one", components: [react_1.default.createElement(ContactName_1.ContactName, { title: contact.title })] })),
            react_1.default.createElement(GroupDialog_1.GroupDialog.Paragraph, null, i18n('NewlyCreatedGroupInvitedContactsDialog--body--info-paragraph'))));
    }
    else {
        title = i18n('NewlyCreatedGroupInvitedContactsDialog--title--many', [
            contacts.length.toString(),
        ]);
        body = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(GroupDialog_1.GroupDialog.Paragraph, null, i18n('NewlyCreatedGroupInvitedContactsDialog--body--user-paragraph--many')),
            react_1.default.createElement(GroupDialog_1.GroupDialog.Paragraph, null, i18n('NewlyCreatedGroupInvitedContactsDialog--body--info-paragraph')),
            react_1.default.createElement(GroupDialog_1.GroupDialog.Contacts, { contacts: contacts, i18n: i18n })));
    }
    return (react_1.default.createElement(GroupDialog_1.GroupDialog, { i18n: i18n, onClickPrimaryButton: onClose, primaryButtonText: i18n('Confirmation--confirm'), secondaryButtonText: i18n('NewlyCreatedGroupInvitedContactsDialog--body--learn-more'), onClickSecondaryButton: () => {
            (0, openLinkInWebBrowser_1.openLinkInWebBrowser)('https://support.signal.org/hc/articles/360007319331-Group-chats');
        }, onClose: onClose, title: title }, body));
};
exports.NewlyCreatedGroupInvitedContactsDialog = NewlyCreatedGroupInvitedContactsDialog;
