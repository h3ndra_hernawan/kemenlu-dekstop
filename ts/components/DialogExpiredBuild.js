"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DialogExpiredBuild = void 0;
const react_1 = __importDefault(require("react"));
const LeftPaneDialog_1 = require("./LeftPaneDialog");
const openLinkInWebBrowser_1 = require("../util/openLinkInWebBrowser");
const DialogExpiredBuild = ({ containerWidthBreakpoint, hasExpired, i18n, }) => {
    if (!hasExpired) {
        return null;
    }
    return (react_1.default.createElement(LeftPaneDialog_1.LeftPaneDialog, { containerWidthBreakpoint: containerWidthBreakpoint, type: "error", onClick: () => {
            (0, openLinkInWebBrowser_1.openLinkInWebBrowser)('https://signal.org/download/');
        }, clickLabel: i18n('upgrade'), hasAction: true },
        i18n('expiredWarning'),
        ' '));
};
exports.DialogExpiredBuild = DialogExpiredBuild;
