"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const react_2 = require("@storybook/react");
const DisappearingTimeDialog_1 = require("./DisappearingTimeDialog");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const expireTimers_1 = require("../test-both/util/expireTimers");
const story = (0, react_2.storiesOf)('Components/DisappearingTimeDialog', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
expireTimers_1.EXPIRE_TIMERS.forEach(({ value, label }) => {
    story.add(`Initial value: ${label}`, () => {
        return (react_1.default.createElement(DisappearingTimeDialog_1.DisappearingTimeDialog, { i18n: i18n, initialValue: value, onSubmit: (0, addon_actions_1.action)('onSubmit'), onClose: (0, addon_actions_1.action)('onClose') }));
    });
});
