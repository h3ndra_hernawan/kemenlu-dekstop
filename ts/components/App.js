"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const react_1 = __importStar(require("react"));
const web_1 = require("@react-spring/web");
const classnames_1 = __importDefault(require("classnames"));
const app_1 = require("../state/ducks/app");
const Inbox_1 = require("./Inbox");
const Install_1 = require("./Install");
const StandaloneRegistration_1 = require("./StandaloneRegistration");
const Util_1 = require("../types/Util");
const usePageVisibility_1 = require("../hooks/usePageVisibility");
const useReducedMotion_1 = require("../hooks/useReducedMotion");
const App = ({ appView, cancelMessagesPendingConversationVerification, conversationsStoppingMessageSendBecauseOfVerification, hasInitialLoadCompleted, i18n, isCustomizingPreferredReactions, numberOfMessagesPendingBecauseOfVerification, renderCallManager, renderCustomizingPreferredReactionsModal, renderGlobalModalContainer, renderSafetyNumber, theme, verifyConversationsStoppingMessageSend, }) => {
    let contents;
    if (appView === app_1.AppViewType.Installer) {
        contents = react_1.default.createElement(Install_1.Install, null);
    }
    else if (appView === app_1.AppViewType.Standalone) {
        contents = react_1.default.createElement(StandaloneRegistration_1.StandaloneRegistration, null);
    }
    else if (appView === app_1.AppViewType.Inbox) {
        contents = (react_1.default.createElement(Inbox_1.Inbox, { cancelMessagesPendingConversationVerification: cancelMessagesPendingConversationVerification, conversationsStoppingMessageSendBecauseOfVerification: conversationsStoppingMessageSendBecauseOfVerification, hasInitialLoadCompleted: hasInitialLoadCompleted, i18n: i18n, isCustomizingPreferredReactions: isCustomizingPreferredReactions, numberOfMessagesPendingBecauseOfVerification: numberOfMessagesPendingBecauseOfVerification, renderCustomizingPreferredReactionsModal: renderCustomizingPreferredReactionsModal, renderSafetyNumber: renderSafetyNumber, verifyConversationsStoppingMessageSend: verifyConversationsStoppingMessageSend }));
    }
    // This are here so that themes are properly applied to anything that is
    // created in a portal and exists outside of the <App /> container.
    (0, react_1.useEffect)(() => {
        document.body.classList.remove('light-theme');
        document.body.classList.remove('dark-theme');
        if (theme === Util_1.ThemeType.dark) {
            document.body.classList.add('dark-theme');
        }
        if (theme === Util_1.ThemeType.light) {
            document.body.classList.add('light-theme');
        }
    }, [theme]);
    const isPageVisible = (0, usePageVisibility_1.usePageVisibility)();
    (0, react_1.useEffect)(() => {
        document.body.classList.toggle('page-is-visible', isPageVisible);
    }, [isPageVisible]);
    // A11y settings for react-spring
    const prefersReducedMotion = (0, useReducedMotion_1.useReducedMotion)();
    (0, react_1.useEffect)(() => {
        web_1.Globals.assign({
            skipAnimation: prefersReducedMotion,
        });
    }, [prefersReducedMotion]);
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)({
            App: true,
            'light-theme': theme === Util_1.ThemeType.light,
            'dark-theme': theme === Util_1.ThemeType.dark,
        }) },
        renderGlobalModalContainer(),
        renderCallManager(),
        contents));
};
exports.App = App;
