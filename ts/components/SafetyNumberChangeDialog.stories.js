"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const react_1 = require("@storybook/react");
const SafetyNumberChangeDialog_1 = require("./SafetyNumberChangeDialog");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const contactWithAllData = (0, getDefaultConversation_1.getDefaultConversation)({
    id: 'abc',
    avatarPath: undefined,
    profileName: '-*Smartest Dude*-',
    title: 'Rick Sanchez',
    name: 'Rick Sanchez',
    phoneNumber: '(305) 123-4567',
});
const contactWithJustProfile = (0, getDefaultConversation_1.getDefaultConversation)({
    id: 'def',
    avatarPath: undefined,
    title: '-*Smartest Dude*-',
    profileName: '-*Smartest Dude*-',
    name: undefined,
    phoneNumber: '(305) 123-4567',
});
const contactWithJustNumber = (0, getDefaultConversation_1.getDefaultConversation)({
    id: 'xyz',
    avatarPath: undefined,
    profileName: undefined,
    name: undefined,
    title: '(305) 123-4567',
    phoneNumber: '(305) 123-4567',
});
const contactWithNothing = (0, getDefaultConversation_1.getDefaultConversation)({
    id: 'some-guid',
    avatarPath: undefined,
    profileName: undefined,
    name: undefined,
    phoneNumber: undefined,
    title: 'Unknown contact',
});
(0, react_1.storiesOf)('Components/SafetyNumberChangeDialog', module)
    .add('Single Contact Dialog', () => {
    return (React.createElement(SafetyNumberChangeDialog_1.SafetyNumberChangeDialog, { contacts: [contactWithAllData], i18n: i18n, onCancel: (0, addon_actions_1.action)('cancel'), onConfirm: (0, addon_actions_1.action)('confirm'), renderSafetyNumber: () => {
            (0, addon_actions_1.action)('renderSafetyNumber');
            return React.createElement("div", null, "This is a mock Safety Number View");
        } }));
})
    .add('Different Confirmation Text', () => {
    return (React.createElement(SafetyNumberChangeDialog_1.SafetyNumberChangeDialog, { confirmText: "You are awesome", contacts: [contactWithAllData], i18n: i18n, onCancel: (0, addon_actions_1.action)('cancel'), onConfirm: (0, addon_actions_1.action)('confirm'), renderSafetyNumber: () => {
            (0, addon_actions_1.action)('renderSafetyNumber');
            return React.createElement("div", null, "This is a mock Safety Number View");
        } }));
})
    .add('Multi Contact Dialog', () => {
    return (React.createElement(SafetyNumberChangeDialog_1.SafetyNumberChangeDialog, { contacts: [
            contactWithAllData,
            contactWithJustProfile,
            contactWithJustNumber,
            contactWithNothing,
        ], i18n: i18n, onCancel: (0, addon_actions_1.action)('cancel'), onConfirm: (0, addon_actions_1.action)('confirm'), renderSafetyNumber: () => {
            (0, addon_actions_1.action)('renderSafetyNumber');
            return React.createElement("div", null, "This is a mock Safety Number View");
        } }));
})
    .add('Scroll Dialog', () => {
    return (React.createElement(SafetyNumberChangeDialog_1.SafetyNumberChangeDialog, { contacts: [
            contactWithAllData,
            contactWithJustProfile,
            contactWithJustNumber,
            contactWithNothing,
            contactWithAllData,
            contactWithAllData,
            contactWithAllData,
            contactWithAllData,
            contactWithAllData,
            contactWithAllData,
        ], i18n: i18n, onCancel: (0, addon_actions_1.action)('cancel'), onConfirm: (0, addon_actions_1.action)('confirm'), renderSafetyNumber: () => {
            (0, addon_actions_1.action)('renderSafetyNumber');
            return React.createElement("div", null, "This is a mock Safety Number View");
        } }));
});
