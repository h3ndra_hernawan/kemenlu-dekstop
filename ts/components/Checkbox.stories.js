"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const react_2 = require("@storybook/react");
const Checkbox_1 = require("./Checkbox");
const createProps = () => ({
    checked: false,
    label: 'Check Me!',
    name: 'check-me',
    onChange: (0, addon_actions_1.action)('onChange'),
});
const story = (0, react_2.storiesOf)('Components/Checkbox', module);
story.add('Normal', () => react_1.default.createElement(Checkbox_1.Checkbox, Object.assign({}, createProps())));
story.add('Checked', () => react_1.default.createElement(Checkbox_1.Checkbox, Object.assign({}, createProps(), { checked: true })));
story.add('Description', () => (react_1.default.createElement(Checkbox_1.Checkbox, Object.assign({}, createProps(), { description: "This is a checkbox" }))));
story.add('Disabled', () => react_1.default.createElement(Checkbox_1.Checkbox, Object.assign({}, createProps(), { disabled: true })));
