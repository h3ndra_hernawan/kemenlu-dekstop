"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactPills = void 0;
const react_1 = __importStar(require("react"));
const usePrevious_1 = require("../hooks/usePrevious");
const scrollToBottom_1 = require("../util/scrollToBottom");
const ContactPills = ({ children }) => {
    const elRef = (0, react_1.useRef)(null);
    const childCount = react_1.Children.count(children);
    const previousChildCount = (0, usePrevious_1.usePrevious)(0, childCount);
    (0, react_1.useEffect)(() => {
        const hasAddedNewChild = childCount > previousChildCount;
        const el = elRef.current;
        if (hasAddedNewChild && el) {
            (0, scrollToBottom_1.scrollToBottom)(el);
        }
    }, [childCount, previousChildCount]);
    return (react_1.default.createElement("div", { className: "module-ContactPills", ref: elRef }, children));
};
exports.ContactPills = ContactPills;
