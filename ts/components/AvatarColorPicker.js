"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvatarColorPicker = void 0;
const react_1 = __importDefault(require("react"));
const Colors_1 = require("../types/Colors");
const BetterAvatarBubble_1 = require("./BetterAvatarBubble");
const AvatarColorPicker = ({ i18n, onColorSelected, selectedColor, }) => {
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: "AvatarEditor__avatar-selector-title" }, i18n('AvatarColorPicker--choose')),
        react_1.default.createElement("div", { className: "AvatarEditor__avatars" }, Colors_1.AvatarColors.map(color => (react_1.default.createElement(BetterAvatarBubble_1.BetterAvatarBubble, { color: color, i18n: i18n, isSelected: selectedColor === color, key: color, onSelect: () => {
                onColorSelected(color);
            } }))))));
};
exports.AvatarColorPicker = AvatarColorPicker;
