"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const react_2 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const GroupCallOverflowArea_1 = require("./GroupCallOverflowArea");
const setupI18n_1 = require("../util/setupI18n");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const fakeGetGroupCallVideoFrameSource_1 = require("../test-both/helpers/fakeGetGroupCallVideoFrameSource");
const constants_1 = require("../calling/constants");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const MAX_PARTICIPANTS = 32;
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const allRemoteParticipants = (0, lodash_1.times)(MAX_PARTICIPANTS).map(index => (Object.assign({ demuxId: index, hasRemoteAudio: index % 3 !== 0, hasRemoteVideo: index % 4 !== 0, presenting: false, sharingScreen: false, videoAspectRatio: 1.3 }, (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
    isBlocked: index === 10 || index === MAX_PARTICIPANTS - 1,
    title: `Participant ${index + 1}`,
}))));
const story = (0, react_2.storiesOf)('Components/GroupCallOverflowArea', module);
const defaultProps = {
    getFrameBuffer: (0, lodash_1.memoize)(() => new ArrayBuffer(constants_1.FRAME_BUFFER_SIZE)),
    getGroupCallVideoFrameSource: fakeGetGroupCallVideoFrameSource_1.fakeGetGroupCallVideoFrameSource,
    i18n,
};
// This component is usually rendered on a call screen.
const Container = ({ children }) => (react_1.default.createElement("div", { style: {
        background: 'black',
        display: 'inline-flex',
        height: '80vh',
    } }, children));
story.add('No overflowed participants', () => (react_1.default.createElement(Container, null,
    react_1.default.createElement(GroupCallOverflowArea_1.GroupCallOverflowArea, Object.assign({}, defaultProps, { overflowedParticipants: [] })))));
story.add('One overflowed participant', () => (react_1.default.createElement(Container, null,
    react_1.default.createElement(GroupCallOverflowArea_1.GroupCallOverflowArea, Object.assign({}, defaultProps, { overflowedParticipants: allRemoteParticipants.slice(0, 1) })))));
story.add('Three overflowed participants', () => (react_1.default.createElement(Container, null,
    react_1.default.createElement(GroupCallOverflowArea_1.GroupCallOverflowArea, Object.assign({}, defaultProps, { overflowedParticipants: allRemoteParticipants.slice(0, 3) })))));
story.add('Many overflowed participants', () => (react_1.default.createElement(Container, null,
    react_1.default.createElement(GroupCallOverflowArea_1.GroupCallOverflowArea, Object.assign({}, defaultProps, { overflowedParticipants: allRemoteParticipants.slice(0, (0, addon_knobs_1.number)('Participant count', MAX_PARTICIPANTS, {
            range: true,
            min: 0,
            max: MAX_PARTICIPANTS,
            step: 1,
        })) })))));
