"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_2 = require("@storybook/react");
const Countdown_1 = require("./Countdown");
const defaultDuration = 10 * 1000;
const createProps = (overrideProps = {}) => ({
    duration: (0, addon_knobs_1.number)('duration', overrideProps.duration || defaultDuration),
    expiresAt: (0, addon_knobs_1.date)('expiresAt', overrideProps.expiresAt
        ? new Date(overrideProps.expiresAt)
        : new Date(Date.now() + defaultDuration)),
    onComplete: (0, addon_actions_1.action)('onComplete'),
});
const story = (0, react_2.storiesOf)('Components/Countdown', module);
story.add('Just Started', () => {
    const props = createProps();
    return react_1.default.createElement(Countdown_1.Countdown, Object.assign({}, props));
});
story.add('In Progress', () => {
    const props = createProps({
        duration: 3 * defaultDuration,
        expiresAt: Date.now() + defaultDuration,
    });
    return react_1.default.createElement(Countdown_1.Countdown, Object.assign({}, props));
});
story.add('Done', () => {
    const props = createProps({
        expiresAt: Date.now() - defaultDuration,
    });
    return react_1.default.createElement(Countdown_1.Countdown, Object.assign({}, props));
});
