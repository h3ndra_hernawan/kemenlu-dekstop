"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Inbox = void 0;
const react_1 = __importStar(require("react"));
const SafetyNumberChangeDialog_1 = require("./SafetyNumberChangeDialog");
const Inbox = ({ cancelMessagesPendingConversationVerification, conversationsStoppingMessageSendBecauseOfVerification, hasInitialLoadCompleted, i18n, isCustomizingPreferredReactions, numberOfMessagesPendingBecauseOfVerification, renderCustomizingPreferredReactionsModal, renderSafetyNumber, verifyConversationsStoppingMessageSend, }) => {
    const hostRef = (0, react_1.useRef)(null);
    const viewRef = (0, react_1.useRef)(undefined);
    (0, react_1.useEffect)(() => {
        const viewOptions = {
            el: hostRef.current,
            initialLoadComplete: false,
            window,
        };
        const view = new window.Whisper.InboxView(viewOptions);
        viewRef.current = view;
        return () => {
            var _a;
            // [`Backbone.View.prototype.remove`][0] removes the DOM element and stops listening
            //   to event listeners. Because React will do the first, we only want to do the
            //   second.
            // [0]: https://github.com/jashkenas/backbone/blob/153dc41616a1f2663e4a86b705fefd412ecb4a7a/backbone.js#L1336-L1342
            (_a = viewRef.current) === null || _a === void 0 ? void 0 : _a.stopListening();
            viewRef.current = undefined;
        };
    }, []);
    (0, react_1.useEffect)(() => {
        if (hasInitialLoadCompleted && viewRef.current && viewRef.current.onEmpty) {
            viewRef.current.onEmpty();
        }
    }, [hasInitialLoadCompleted, viewRef]);
    let activeModal;
    if (conversationsStoppingMessageSendBecauseOfVerification.length) {
        const confirmText = numberOfMessagesPendingBecauseOfVerification === 1
            ? i18n('safetyNumberChangeDialog__pending-messages--1')
            : i18n('safetyNumberChangeDialog__pending-messages--many', [
                numberOfMessagesPendingBecauseOfVerification.toString(),
            ]);
        activeModal = (react_1.default.createElement(SafetyNumberChangeDialog_1.SafetyNumberChangeDialog, { confirmText: confirmText, contacts: conversationsStoppingMessageSendBecauseOfVerification, i18n: i18n, onCancel: cancelMessagesPendingConversationVerification, onConfirm: verifyConversationsStoppingMessageSend, renderSafetyNumber: renderSafetyNumber }));
    }
    if (!activeModal && isCustomizingPreferredReactions) {
        activeModal = renderCustomizingPreferredReactionsModal();
    }
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: "Inbox", ref: hostRef }),
        activeModal));
};
exports.Inbox = Inbox;
