"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const Colors_1 = require("../types/Colors");
const Avatar_1 = require("../types/Avatar");
const BetterAvatar_1 = require("./BetterAvatar");
const createAvatarData_1 = require("../util/createAvatarData");
const setupI18n_1 = require("../util/setupI18n");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    avatarData: overrideProps.avatarData ||
        (0, createAvatarData_1.createAvatarData)({ color: Colors_1.AvatarColors[0], text: 'OOO' }),
    i18n,
    isSelected: Boolean(overrideProps.isSelected),
    onClick: (0, addon_actions_1.action)('onClick'),
    onDelete: (0, addon_actions_1.action)('onDelete'),
    size: 80,
});
const story = (0, react_2.storiesOf)('Components/BetterAvatar', module);
story.add('Text', () => (react_1.default.createElement(BetterAvatar_1.BetterAvatar, Object.assign({}, createProps({
    avatarData: (0, createAvatarData_1.createAvatarData)({
        color: Colors_1.AvatarColors[0],
        text: 'AH',
    }),
})))));
story.add('Personal Icon', () => (react_1.default.createElement(BetterAvatar_1.BetterAvatar, Object.assign({}, createProps({
    avatarData: (0, createAvatarData_1.createAvatarData)({
        color: Colors_1.AvatarColors[1],
        icon: Avatar_1.PersonalAvatarIcons[1],
    }),
})))));
story.add('Group Icon', () => (react_1.default.createElement(BetterAvatar_1.BetterAvatar, Object.assign({}, createProps({
    avatarData: (0, createAvatarData_1.createAvatarData)({
        color: Colors_1.AvatarColors[1],
        icon: Avatar_1.GroupAvatarIcons[1],
    }),
})))));
