"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const AvatarIconEditor_1 = require("./AvatarIconEditor");
const Avatar_1 = require("../types/Avatar");
const Colors_1 = require("../types/Colors");
const createAvatarData_1 = require("../util/createAvatarData");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    avatarData: overrideProps.avatarData || (0, createAvatarData_1.createAvatarData)({}),
    i18n,
    onClose: (0, addon_actions_1.action)('onClose'),
});
const story = (0, react_2.storiesOf)('Components/AvatarIconEditor', module);
story.add('Personal Icon', () => (react_1.default.createElement(AvatarIconEditor_1.AvatarIconEditor, Object.assign({}, createProps({
    avatarData: (0, createAvatarData_1.createAvatarData)({
        color: Colors_1.AvatarColors[3],
        icon: Avatar_1.PersonalAvatarIcons[0],
    }),
})))));
story.add('Group Icon', () => (react_1.default.createElement(AvatarIconEditor_1.AvatarIconEditor, Object.assign({}, createProps({
    avatarData: (0, createAvatarData_1.createAvatarData)({
        color: Colors_1.AvatarColors[8],
        icon: Avatar_1.GroupAvatarIcons[0],
    }),
})))));
