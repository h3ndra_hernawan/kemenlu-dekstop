"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModalWindow = exports.Modal = void 0;
const react_1 = __importStar(require("react"));
const react_measure_1 = __importDefault(require("react-measure"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const web_1 = require("@react-spring/web");
const ModalHost_1 = require("./ModalHost");
const getClassNamesFor_1 = require("../util/getClassNamesFor");
const useAnimated_1 = require("../hooks/useAnimated");
const useHasWrapped_1 = require("../hooks/useHasWrapped");
const useRefMerger_1 = require("../hooks/useRefMerger");
const BASE_CLASS_NAME = 'module-Modal';
function Modal({ children, hasStickyButtons, hasXButton, i18n, moduleClassName, noMouseClose, onClose = lodash_1.noop, title, theme, }) {
    const { close, modalStyles, overlayStyles } = (0, useAnimated_1.useAnimated)(onClose, {
        getFrom: () => ({ opacity: 0, transform: 'translateY(48px)' }),
        getTo: isOpen => isOpen
            ? { opacity: 1, transform: 'translateY(0px)' }
            : { opacity: 0, transform: 'translateY(48px)' },
    });
    return (react_1.default.createElement(ModalHost_1.ModalHost, { noMouseClose: noMouseClose, onClose: close, overlayStyles: overlayStyles, theme: theme },
        react_1.default.createElement(web_1.animated.div, { style: modalStyles },
            react_1.default.createElement(ModalWindow, { hasStickyButtons: hasStickyButtons, hasXButton: hasXButton, i18n: i18n, moduleClassName: moduleClassName, onClose: close, title: title }, children))));
}
exports.Modal = Modal;
function ModalWindow({ children, hasStickyButtons, hasXButton, i18n, moduleClassName, onClose = lodash_1.noop, title, }) {
    const modalRef = (0, react_1.useRef)(null);
    const refMerger = (0, useRefMerger_1.useRefMerger)();
    const bodyRef = (0, react_1.useRef)(null);
    const [scrolled, setScrolled] = (0, react_1.useState)(false);
    const [hasOverflow, setHasOverflow] = (0, react_1.useState)(false);
    const hasHeader = Boolean(hasXButton || title);
    const getClassName = (0, getClassNamesFor_1.getClassNamesFor)(BASE_CLASS_NAME, moduleClassName);
    function handleResize({ scroll }) {
        const modalNode = modalRef === null || modalRef === void 0 ? void 0 : modalRef.current;
        if (!modalNode) {
            return;
        }
        if (scroll) {
            setHasOverflow(scroll.height > modalNode.clientHeight);
        }
    }
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: (0, classnames_1.default)(getClassName(''), getClassName(hasHeader ? '--has-header' : '--no-header'), hasStickyButtons && getClassName('--sticky-buttons')), ref: modalRef, onClick: event => {
                event.stopPropagation();
            } },
            hasHeader && (react_1.default.createElement("div", { className: getClassName('__header') },
                hasXButton && (react_1.default.createElement("button", { "aria-label": i18n('close'), type: "button", className: getClassName('__close-button'), tabIndex: 0, onClick: onClose })),
                title && (react_1.default.createElement("h1", { className: (0, classnames_1.default)(getClassName('__title'), hasXButton ? getClassName('__title--with-x-button') : null) }, title)))),
            react_1.default.createElement(react_measure_1.default, { scroll: true, onResize: handleResize }, ({ measureRef }) => (react_1.default.createElement("div", { className: (0, classnames_1.default)(getClassName('__body'), scrolled ? getClassName('__body--scrolled') : null, hasOverflow || scrolled
                    ? getClassName('__body--overflow')
                    : null), onScroll: () => {
                    var _a;
                    const scrollTop = ((_a = bodyRef.current) === null || _a === void 0 ? void 0 : _a.scrollTop) || 0;
                    setScrolled(scrollTop > 2);
                }, ref: refMerger(measureRef, bodyRef) }, children))))));
}
exports.ModalWindow = ModalWindow;
Modal.ButtonFooter = function ButtonFooter({ children, moduleClassName, }) {
    const [ref, hasWrapped] = (0, useHasWrapped_1.useHasWrapped)();
    const className = (0, getClassNamesFor_1.getClassNamesFor)(BASE_CLASS_NAME, moduleClassName)('__button-footer');
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(className, hasWrapped ? `${className}--one-button-per-line` : undefined), ref: ref }, children));
};
