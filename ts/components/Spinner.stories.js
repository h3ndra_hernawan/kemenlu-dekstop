"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const Spinner_1 = require("./Spinner");
const story = (0, react_1.storiesOf)('Components/Spinner', module);
const createProps = (overrideProps = {}) => ({
    size: (0, addon_knobs_1.text)('size', overrideProps.size || ''),
    svgSize: (0, addon_knobs_1.select)('svgSize', Spinner_1.SpinnerSvgSizes.reduce((m, s) => (Object.assign(Object.assign({}, m), { [s]: s })), {}), overrideProps.svgSize || 'normal'),
    direction: (0, addon_knobs_1.select)('direction', Spinner_1.SpinnerDirections.reduce((d, s) => (Object.assign(Object.assign({}, d), { [s]: s })), {}), overrideProps.direction),
});
story.add('Normal', () => {
    const props = createProps();
    return React.createElement(Spinner_1.Spinner, Object.assign({}, props));
});
story.add('SVG Sizes', () => {
    const props = createProps();
    return Spinner_1.SpinnerSvgSizes.map(svgSize => (React.createElement(Spinner_1.Spinner, Object.assign({ key: svgSize }, props, { svgSize: svgSize }))));
});
story.add('Directions', () => {
    const props = createProps();
    return Spinner_1.SpinnerDirections.map(direction => (React.createElement(Spinner_1.Spinner, Object.assign({ key: direction }, props, { direction: direction }))));
});
