"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DialogUpdate = void 0;
const react_1 = __importDefault(require("react"));
const filesize_1 = __importDefault(require("filesize"));
const version_1 = require("../util/version");
const Dialogs_1 = require("../types/Dialogs");
const Intl_1 = require("./Intl");
const LeftPaneDialog_1 = require("./LeftPaneDialog");
const PRODUCTION_DOWNLOAD_URL = 'https://signal.org/download/';
const BETA_DOWNLOAD_URL = 'https://support.signal.org/beta';
const DialogUpdate = ({ containerWidthBreakpoint, dialogType, didSnooze, dismissDialog, downloadSize, downloadedSize, hasNetworkDialog, i18n, snoozeUpdate, startUpdate, version, currentVersion, }) => {
    if (hasNetworkDialog) {
        return null;
    }
    if (dialogType === Dialogs_1.DialogType.None) {
        return null;
    }
    if (didSnooze) {
        return null;
    }
    if (dialogType === Dialogs_1.DialogType.Cannot_Update) {
        const url = (0, version_1.isBeta)(currentVersion)
            ? BETA_DOWNLOAD_URL
            : PRODUCTION_DOWNLOAD_URL;
        return (react_1.default.createElement(LeftPaneDialog_1.LeftPaneDialog, { containerWidthBreakpoint: containerWidthBreakpoint, type: "warning", title: i18n('cannotUpdate') },
            react_1.default.createElement("span", null,
                react_1.default.createElement(Intl_1.Intl, { components: [
                        react_1.default.createElement("a", { key: "signal-download", href: url, rel: "noreferrer", target: "_blank" }, url),
                    ], i18n: i18n, id: "cannotUpdateDetail" }))));
    }
    if (dialogType === Dialogs_1.DialogType.MacOS_Read_Only) {
        return (react_1.default.createElement(LeftPaneDialog_1.LeftPaneDialog, { containerWidthBreakpoint: containerWidthBreakpoint, type: "warning", title: i18n('cannotUpdate'), hasXButton: true, closeLabel: i18n('close'), onClose: dismissDialog },
            react_1.default.createElement("span", null,
                react_1.default.createElement(Intl_1.Intl, { components: {
                        app: react_1.default.createElement("strong", { key: "app" }, "Signal.app"),
                        folder: react_1.default.createElement("strong", { key: "folder" }, "/Applications"),
                    }, i18n: i18n, id: "readOnlyVolume" }))));
    }
    let title = i18n('autoUpdateNewVersionTitle');
    if (downloadSize &&
        (dialogType === Dialogs_1.DialogType.DownloadReady ||
            dialogType === Dialogs_1.DialogType.Downloading)) {
        title += ` (${(0, filesize_1.default)(downloadSize, { round: 0 })})`;
    }
    const versionTitle = version
        ? i18n('DialogUpdate--version-available', [version])
        : undefined;
    if (dialogType === Dialogs_1.DialogType.Downloading) {
        const width = Math.ceil(((downloadedSize || 1) / (downloadSize || 1)) * 100);
        return (react_1.default.createElement(LeftPaneDialog_1.LeftPaneDialog, { containerWidthBreakpoint: containerWidthBreakpoint, icon: "update", title: title, hoverText: versionTitle },
            react_1.default.createElement("div", { className: "LeftPaneDialog__progress--container" },
                react_1.default.createElement("div", { className: "LeftPaneDialog__progress--bar", style: { width: `${width}%` } }))));
    }
    let clickLabel;
    if (dialogType === Dialogs_1.DialogType.DownloadReady) {
        clickLabel = i18n('downloadNewVersionMessage');
    }
    else {
        clickLabel = i18n('autoUpdateNewVersionMessage');
    }
    return (react_1.default.createElement(LeftPaneDialog_1.LeftPaneDialog, { containerWidthBreakpoint: containerWidthBreakpoint, icon: "update", title: title, hoverText: versionTitle, hasAction: true, onClick: startUpdate, clickLabel: clickLabel, hasXButton: true, onClose: snoozeUpdate, closeLabel: i18n('autoUpdateIgnoreButtonLabel') }));
};
exports.DialogUpdate = DialogUpdate;
