"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaQualitySelector = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_dom_1 = require("react-dom");
const classnames_1 = __importDefault(require("classnames"));
const react_popper_1 = require("react-popper");
const MediaQualitySelector = ({ i18n, isHighQuality, onSelectQuality, }) => {
    const [menuShowing, setMenuShowing] = (0, react_1.useState)(false);
    const [popperRoot, setPopperRoot] = (0, react_1.useState)(null);
    const [focusedOption, setFocusedOption] = (0, react_1.useState)(undefined);
    // We use regular MouseEvent below, and this one uses React.MouseEvent
    const handleClick = (ev) => {
        setMenuShowing(true);
        ev.stopPropagation();
        ev.preventDefault();
    };
    const handleKeyDown = (ev) => {
        if (!popperRoot) {
            if (ev.key === 'Enter') {
                setFocusedOption(isHighQuality ? 1 : 0);
            }
            return;
        }
        if (ev.key === 'ArrowDown' || ev.key === 'ArrowUp') {
            setFocusedOption(oldFocusedOption => (oldFocusedOption === 1 ? 0 : 1));
            ev.stopPropagation();
            ev.preventDefault();
        }
        if (ev.key === 'Enter') {
            onSelectQuality(Boolean(focusedOption));
            setMenuShowing(false);
            ev.stopPropagation();
            ev.preventDefault();
        }
    };
    const handleClose = (0, react_1.useCallback)(() => {
        setMenuShowing(false);
        setFocusedOption(undefined);
    }, [setMenuShowing]);
    (0, react_1.useEffect)(() => {
        if (menuShowing) {
            const root = document.createElement('div');
            setPopperRoot(root);
            document.body.appendChild(root);
            const handleOutsideClick = (event) => {
                if (!root.contains(event.target)) {
                    handleClose();
                    event.stopPropagation();
                    event.preventDefault();
                }
            };
            document.addEventListener('click', handleOutsideClick);
            return () => {
                document.body.removeChild(root);
                document.removeEventListener('click', handleOutsideClick);
                setPopperRoot(null);
            };
        }
        return lodash_1.noop;
    }, [menuShowing, setPopperRoot, handleClose]);
    return (react_1.default.createElement(react_popper_1.Manager, null,
        react_1.default.createElement(react_popper_1.Reference, null, ({ ref }) => (react_1.default.createElement("button", { "aria-label": i18n('MediaQualitySelector--button'), className: (0, classnames_1.default)({
                MediaQualitySelector__button: true,
                'MediaQualitySelector__button--hq': isHighQuality,
                'MediaQualitySelector__button--active': menuShowing,
            }), onClick: handleClick, onKeyDown: handleKeyDown, ref: ref, type: "button" }))),
        menuShowing && popperRoot
            ? (0, react_dom_1.createPortal)(react_1.default.createElement(react_popper_1.Popper, { placement: "top-start", strategy: "fixed" }, ({ ref, style, placement }) => (react_1.default.createElement("div", { className: "MediaQualitySelector__popper", "data-placement": placement, ref: ref, style: style },
                react_1.default.createElement("div", { className: "MediaQualitySelector__title" }, i18n('MediaQualitySelector--title')),
                react_1.default.createElement("button", { "aria-label": i18n('MediaQualitySelector--standard-quality-title'), className: (0, classnames_1.default)({
                        MediaQualitySelector__option: true,
                        'MediaQualitySelector__option--focused': focusedOption === 0,
                    }), type: "button", onClick: () => {
                        onSelectQuality(false);
                        setMenuShowing(false);
                    } },
                    react_1.default.createElement("div", { className: (0, classnames_1.default)({
                            'MediaQualitySelector__option--checkmark': true,
                            'MediaQualitySelector__option--selected': !isHighQuality,
                        }) }),
                    react_1.default.createElement("div", null,
                        react_1.default.createElement("div", { className: "MediaQualitySelector__option--title" }, i18n('MediaQualitySelector--standard-quality-title')),
                        react_1.default.createElement("div", { className: "MediaQualitySelector__option--description" }, i18n('MediaQualitySelector--standard-quality-description')))),
                react_1.default.createElement("button", { "aria-label": i18n('MediaQualitySelector--high-quality-title'), className: (0, classnames_1.default)({
                        MediaQualitySelector__option: true,
                        'MediaQualitySelector__option--focused': focusedOption === 1,
                    }), type: "button", onClick: () => {
                        onSelectQuality(true);
                        setMenuShowing(false);
                    } },
                    react_1.default.createElement("div", { className: (0, classnames_1.default)({
                            'MediaQualitySelector__option--checkmark': true,
                            'MediaQualitySelector__option--selected': isHighQuality,
                        }) }),
                    react_1.default.createElement("div", null,
                        react_1.default.createElement("div", { className: "MediaQualitySelector__option--title" }, i18n('MediaQualitySelector--high-quality-title')),
                        react_1.default.createElement("div", { className: "MediaQualitySelector__option--description" }, i18n('MediaQualitySelector--high-quality-description'))))))), popperRoot)
            : null));
};
exports.MediaQualitySelector = MediaQualitySelector;
