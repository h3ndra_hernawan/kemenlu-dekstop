"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionsPopup = void 0;
const react_1 = __importDefault(require("react"));
const Button_1 = require("./Button");
const useEscapeHandling_1 = require("../hooks/useEscapeHandling");
function focusRef(el) {
    if (el) {
        el.focus();
    }
}
const PermissionsPopup = ({ i18n, message, onAccept, onClose, }) => {
    (0, useEscapeHandling_1.useEscapeHandling)(onClose);
    return (react_1.default.createElement("div", { className: "PermissionsPopup" },
        react_1.default.createElement("div", { className: "PermissionsPopup__body" }, message),
        react_1.default.createElement("div", { className: "PermissionsPopup__buttons" },
            react_1.default.createElement(Button_1.Button, { onClick: onClose, ref: focusRef, variant: Button_1.ButtonVariant.Secondary }, i18n('confirmation-dialog--Cancel')),
            react_1.default.createElement(Button_1.Button, { onClick: onAccept, ref: focusRef, variant: Button_1.ButtonVariant.Primary }, i18n('allowAccess')))));
};
exports.PermissionsPopup = PermissionsPopup;
