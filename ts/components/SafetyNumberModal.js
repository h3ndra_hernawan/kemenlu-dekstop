"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SafetyNumberModal = void 0;
const react_1 = __importDefault(require("react"));
const Modal_1 = require("./Modal");
const SafetyNumberViewer_1 = require("./SafetyNumberViewer");
const SafetyNumberModal = (_a) => {
    var { i18n, toggleSafetyNumberModal } = _a, safetyNumberViewerProps = __rest(_a, ["i18n", "toggleSafetyNumberModal"]);
    return (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, i18n: i18n, moduleClassName: "module-SafetyNumberViewer__modal", onClose: () => toggleSafetyNumberModal(), title: i18n('SafetyNumberModal__title') },
        react_1.default.createElement(SafetyNumberViewer_1.SafetyNumberViewer, Object.assign({ i18n: i18n }, safetyNumberViewerProps))));
};
exports.SafetyNumberModal = SafetyNumberModal;
