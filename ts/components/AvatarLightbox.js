"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvatarLightbox = void 0;
const react_1 = __importDefault(require("react"));
const AvatarPreview_1 = require("./AvatarPreview");
const Lightbox_1 = require("./Lightbox");
const AvatarLightbox = ({ avatarColor, avatarPath, conversationTitle, i18n, isGroup, onClose, }) => {
    return (react_1.default.createElement(Lightbox_1.Lightbox, { close: onClose, i18n: i18n, media: [] },
        react_1.default.createElement(AvatarPreview_1.AvatarPreview, { avatarColor: avatarColor, avatarPath: avatarPath, conversationTitle: conversationTitle, i18n: i18n, isGroup: isGroup, style: {
                fontSize: '16em',
                height: '2em',
                maxHeight: 512,
                maxWidth: 512,
                width: '2em',
            } })));
};
exports.AvatarLightbox = AvatarLightbox;
