"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const ContactPills_1 = require("./ContactPills");
const ContactPill_1 = require("./ContactPill");
const Fixtures_1 = require("../storybook/Fixtures");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/Contact Pills', module);
const contacts = (0, lodash_1.times)(50, index => (0, getDefaultConversation_1.getDefaultConversation)({
    id: `contact-${index}`,
    name: `Contact ${index}`,
    phoneNumber: '(202) 555-0001',
    profileName: `C${index}`,
    title: `Contact ${index}`,
}));
const contactPillProps = (overrideProps) => (Object.assign(Object.assign({}, (overrideProps !== null && overrideProps !== void 0 ? overrideProps : (0, getDefaultConversation_1.getDefaultConversation)({
    avatarPath: Fixtures_1.gifUrl,
    firstName: 'John',
    id: 'abc123',
    isMe: false,
    name: 'John Bon Bon Jovi',
    phoneNumber: '(202) 555-0001',
    profileName: 'JohnB',
    title: 'John Bon Bon Jovi',
}))), { i18n, onClickRemove: (0, addon_actions_1.action)('onClickRemove') }));
story.add('Empty list', () => react_1.default.createElement(ContactPills_1.ContactPills, null));
story.add('One contact', () => (react_1.default.createElement(ContactPills_1.ContactPills, null,
    react_1.default.createElement(ContactPill_1.ContactPill, Object.assign({}, contactPillProps())))));
story.add('Three contacts', () => (react_1.default.createElement(ContactPills_1.ContactPills, null,
    react_1.default.createElement(ContactPill_1.ContactPill, Object.assign({}, contactPillProps(contacts[0]))),
    react_1.default.createElement(ContactPill_1.ContactPill, Object.assign({}, contactPillProps(contacts[1]))),
    react_1.default.createElement(ContactPill_1.ContactPill, Object.assign({}, contactPillProps(contacts[2]))))));
story.add('Four contacts, one with a long name', () => (react_1.default.createElement(ContactPills_1.ContactPills, null,
    react_1.default.createElement(ContactPill_1.ContactPill, Object.assign({}, contactPillProps(contacts[0]))),
    react_1.default.createElement(ContactPill_1.ContactPill, Object.assign({}, contactPillProps(Object.assign(Object.assign({}, contacts[1]), { title: 'Pablo Diego José Francisco de Paula Juan Nepomuceno María de los Remedios Cipriano de la Santísima Trinidad Ruiz y Picasso' })))),
    react_1.default.createElement(ContactPill_1.ContactPill, Object.assign({}, contactPillProps(contacts[2]))),
    react_1.default.createElement(ContactPill_1.ContactPill, Object.assign({}, contactPillProps(contacts[3]))))));
story.add('Fifty contacts', () => (react_1.default.createElement(ContactPills_1.ContactPills, null, contacts.map(contact => (react_1.default.createElement(ContactPill_1.ContactPill, Object.assign({ key: contact.id }, contactPillProps(contact))))))));
