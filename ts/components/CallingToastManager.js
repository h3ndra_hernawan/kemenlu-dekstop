"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallingToastManager = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Calling_1 = require("../types/Calling");
function getReconnectingToast({ activeCall, i18n }) {
    if (activeCall.callMode === Calling_1.CallMode.Group &&
        activeCall.connectionState === Calling_1.GroupCallConnectionState.Reconnecting) {
        return {
            message: i18n('callReconnecting'),
            type: 'static',
        };
    }
    return undefined;
}
const ME = Symbol('me');
function getCurrentPresenter(activeCall) {
    if (activeCall.presentingSource) {
        return ME;
    }
    if (activeCall.callMode === Calling_1.CallMode.Direct) {
        const isOtherPersonPresenting = activeCall.remoteParticipants.some(participant => participant.presenting);
        return isOtherPersonPresenting ? activeCall.conversation : undefined;
    }
    if (activeCall.callMode === Calling_1.CallMode.Group) {
        return activeCall.remoteParticipants.find(participant => participant.presenting);
    }
    return undefined;
}
function useScreenSharingToast({ activeCall, i18n }) {
    const [result, setResult] = (0, react_1.useState)(undefined);
    const [previousPresenter, setPreviousPresenter] = (0, react_1.useState)(undefined);
    const previousPresenterId = previousPresenter === null || previousPresenter === void 0 ? void 0 : previousPresenter.id;
    const previousPresenterTitle = previousPresenter === null || previousPresenter === void 0 ? void 0 : previousPresenter.title;
    (0, react_1.useEffect)(() => {
        const currentPresenter = getCurrentPresenter(activeCall);
        if (!currentPresenter && previousPresenterId) {
            if (previousPresenterId === ME) {
                setResult({
                    type: 'dismissable',
                    message: i18n('calling__presenting--you-stopped'),
                });
            }
            else if (previousPresenterTitle) {
                setResult({
                    type: 'dismissable',
                    message: i18n('calling__presenting--person-stopped', [
                        previousPresenterTitle,
                    ]),
                });
            }
        }
    }, [activeCall, i18n, previousPresenterId, previousPresenterTitle]);
    (0, react_1.useEffect)(() => {
        const currentPresenter = getCurrentPresenter(activeCall);
        if (currentPresenter === ME) {
            setPreviousPresenter({
                id: ME,
            });
        }
        else if (!currentPresenter) {
            setPreviousPresenter(undefined);
        }
        else {
            const { id, title } = currentPresenter;
            setPreviousPresenter({ id, title });
        }
    }, [activeCall]);
    return result;
}
const DEFAULT_DELAY = 5000;
// In the future, this component should show toasts when users join or leave. See
//   DESKTOP-902.
const CallingToastManager = props => {
    const reconnectingToast = getReconnectingToast(props);
    const screenSharingToast = useScreenSharingToast(props);
    let toast;
    if (reconnectingToast) {
        toast = reconnectingToast;
    }
    else if (screenSharingToast) {
        toast = screenSharingToast;
    }
    const [toastMessage, setToastMessage] = (0, react_1.useState)('');
    const timeoutRef = (0, react_1.useRef)(null);
    const dismissToast = (0, react_1.useCallback)(() => {
        if (timeoutRef) {
            setToastMessage('');
        }
    }, [setToastMessage, timeoutRef]);
    (0, react_1.useEffect)(() => {
        if (toast) {
            if (toast.type === 'dismissable') {
                if (timeoutRef && timeoutRef.current) {
                    clearTimeout(timeoutRef.current);
                }
                timeoutRef.current = setTimeout(dismissToast, DEFAULT_DELAY);
            }
            setToastMessage(toast.message);
        }
        return () => {
            if (timeoutRef && timeoutRef.current) {
                clearTimeout(timeoutRef.current);
            }
        };
    }, [dismissToast, setToastMessage, timeoutRef, toast]);
    const isVisible = Boolean(toastMessage);
    return (react_1.default.createElement("button", { className: (0, classnames_1.default)('module-ongoing-call__toast', {
            'module-ongoing-call__toast--hidden': !isVisible,
        }), type: "button", onClick: dismissToast }, toastMessage));
};
exports.CallingToastManager = CallingToastManager;
