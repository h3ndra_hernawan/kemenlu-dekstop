"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const Colors_1 = require("../types/Colors");
const CallingLobby_1 = require("./CallingLobby");
const setupI18n_1 = require("../util/setupI18n");
const UUID_1 = require("../types/UUID");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const camera = {
    deviceId: 'dfbe6effe70b0611ba0fdc2a9ea3f39f6cb110e6687948f7e5f016c111b7329c',
    groupId: '63ee218d2446869e40adfc958ff98263e51f74382b0143328ee4826f20a76f47',
    kind: 'videoinput',
    label: 'FaceTime HD Camera (Built-in) (9fba:bced)',
    toJSON() {
        return '';
    },
};
const createProps = (overrideProps = {}) => {
    const isGroupCall = (0, addon_knobs_1.boolean)('isGroupCall', overrideProps.isGroupCall || false);
    const conversation = isGroupCall
        ? (0, getDefaultConversation_1.getDefaultConversation)({
            title: 'Tahoe Trip',
            type: 'group',
        })
        : (0, getDefaultConversation_1.getDefaultConversation)();
    return {
        availableCameras: overrideProps.availableCameras || [camera],
        conversation,
        groupMembers: overrideProps.groupMembers ||
            (isGroupCall ? (0, lodash_1.times)(3, () => (0, getDefaultConversation_1.getDefaultConversation)()) : undefined),
        hasLocalAudio: (0, addon_knobs_1.boolean)('hasLocalAudio', overrideProps.hasLocalAudio || false),
        hasLocalVideo: (0, addon_knobs_1.boolean)('hasLocalVideo', overrideProps.hasLocalVideo || false),
        i18n,
        isGroupCall,
        isGroupCallOutboundRingEnabled: true,
        isCallFull: (0, addon_knobs_1.boolean)('isCallFull', overrideProps.isCallFull || false),
        me: overrideProps.me || {
            color: Colors_1.AvatarColors[0],
            id: UUID_1.UUID.generate().toString(),
            uuid: UUID_1.UUID.generate().toString(),
        },
        onCallCanceled: (0, addon_actions_1.action)('on-call-canceled'),
        onJoinCall: (0, addon_actions_1.action)('on-join-call'),
        outgoingRing: (0, addon_knobs_1.boolean)('outgoingRing', Boolean(overrideProps.outgoingRing)),
        peekedParticipants: overrideProps.peekedParticipants || [],
        setLocalAudio: (0, addon_actions_1.action)('set-local-audio'),
        setLocalPreview: (0, addon_actions_1.action)('set-local-preview'),
        setLocalVideo: (0, addon_actions_1.action)('set-local-video'),
        setOutgoingRing: (0, addon_actions_1.action)('set-outgoing-ring'),
        showParticipantsList: (0, addon_knobs_1.boolean)('showParticipantsList', Boolean(overrideProps.showParticipantsList)),
        toggleParticipants: (0, addon_actions_1.action)('toggle-participants'),
        toggleSettings: (0, addon_actions_1.action)('toggle-settings'),
    };
};
const fakePeekedParticipant = (conversationProps) => (0, getDefaultConversation_1.getDefaultConversationWithUuid)(Object.assign({}, conversationProps));
const story = (0, react_1.storiesOf)('Components/CallingLobby', module);
story.add('Default', () => {
    const props = createProps();
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('No Camera, no avatar', () => {
    const props = createProps({
        availableCameras: [],
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('No Camera, local avatar', () => {
    const props = createProps({
        availableCameras: [],
        me: {
            avatarPath: '/fixtures/kitten-4-112-112.jpg',
            color: Colors_1.AvatarColors[0],
            id: UUID_1.UUID.generate().toString(),
            uuid: UUID_1.UUID.generate().toString(),
        },
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('Local Video', () => {
    const props = createProps({
        hasLocalVideo: true,
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('Local Video', () => {
    const props = createProps({
        hasLocalVideo: true,
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('Group Call - 0 peeked participants', () => {
    const props = createProps({ isGroupCall: true, peekedParticipants: [] });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('Group Call - 1 peeked participant', () => {
    const props = createProps({
        isGroupCall: true,
        peekedParticipants: [{ title: 'Sam' }].map(fakePeekedParticipant),
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('Group Call - 1 peeked participant (self)', () => {
    const uuid = UUID_1.UUID.generate().toString();
    const props = createProps({
        isGroupCall: true,
        me: {
            id: UUID_1.UUID.generate().toString(),
            uuid,
        },
        peekedParticipants: [fakePeekedParticipant({ title: 'Ash', uuid })],
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('Group Call - 4 peeked participants', () => {
    const props = createProps({
        isGroupCall: true,
        peekedParticipants: ['Sam', 'Cayce', 'April', 'Logan', 'Carl'].map(title => fakePeekedParticipant({ title })),
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('Group Call - 4 peeked participants (participants list)', () => {
    const props = createProps({
        isGroupCall: true,
        peekedParticipants: ['Sam', 'Cayce', 'April', 'Logan', 'Carl'].map(title => fakePeekedParticipant({ title })),
        showParticipantsList: true,
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('Group Call - call full', () => {
    const props = createProps({
        isGroupCall: true,
        isCallFull: true,
        peekedParticipants: ['Sam', 'Cayce'].map(title => fakePeekedParticipant({ title })),
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
story.add('Group Call - 0 peeked participants, big group', () => {
    const props = createProps({
        isGroupCall: true,
        groupMembers: (0, lodash_1.times)(100, () => (0, getDefaultConversation_1.getDefaultConversation)()),
    });
    return React.createElement(CallingLobby_1.CallingLobby, Object.assign({}, props));
});
