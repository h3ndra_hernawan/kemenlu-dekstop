"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallingLobbyJoinButton = exports.CallingLobbyJoinButtonVariant = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const Button_1 = require("./Button");
const Spinner_1 = require("./Spinner");
const PADDING_HORIZONTAL = 48;
const PADDING_VERTICAL = 12;
var CallingLobbyJoinButtonVariant;
(function (CallingLobbyJoinButtonVariant) {
    CallingLobbyJoinButtonVariant["CallIsFull"] = "CallIsFull";
    CallingLobbyJoinButtonVariant["Join"] = "Join";
    CallingLobbyJoinButtonVariant["Loading"] = "Loading";
    CallingLobbyJoinButtonVariant["Start"] = "Start";
})(CallingLobbyJoinButtonVariant = exports.CallingLobbyJoinButtonVariant || (exports.CallingLobbyJoinButtonVariant = {}));
/**
 * This component is a little weird. Why not just render a button with some children?
 *
 * The contents of this component can change but we don't want its size to change, so we
 * render all the variants invisibly, compute the maximum size, and then render the
 * "final" button with those dimensions.
 *
 * For example, we might initially render "Join call" and then render a spinner when you
 * click the button. The button shouldn't resize in that situation.
 */
const CallingLobbyJoinButton = ({ disabled, i18n, onClick, variant }) => {
    const [width, setWidth] = (0, react_1.useState)();
    const [height, setHeight] = (0, react_1.useState)();
    const childrenByVariant = {
        [CallingLobbyJoinButtonVariant.CallIsFull]: i18n('calling__call-is-full'),
        [CallingLobbyJoinButtonVariant.Loading]: react_1.default.createElement(Spinner_1.Spinner, { svgSize: "small" }),
        [CallingLobbyJoinButtonVariant.Join]: i18n('calling__join'),
        [CallingLobbyJoinButtonVariant.Start]: i18n('calling__start'),
    };
    return (react_1.default.createElement(react_1.default.Fragment, null,
        Boolean(width && height) && (react_1.default.createElement(Button_1.Button, { className: "module-CallingLobbyJoinButton", disabled: disabled, onClick: onClick, style: { width, height }, tabIndex: 0, variant: Button_1.ButtonVariant.Calling }, childrenByVariant[variant])),
        react_1.default.createElement("div", { style: {
                visibility: 'hidden',
                position: 'fixed',
                left: -9999,
                top: -9999,
            } }, Object.values(CallingLobbyJoinButtonVariant).map(candidateVariant => (react_1.default.createElement(Button_1.Button, { key: candidateVariant, className: "module-CallingLobbyJoinButton", variant: Button_1.ButtonVariant.Calling, onClick: lodash_1.noop, ref: (button) => {
                if (!button) {
                    return;
                }
                const { width: variantWidth, height: variantHeight } = button.getBoundingClientRect();
                // We could set the padding in CSS, but we don't do that in case some other
                //   styling causes a re-render of the button but not of the component. This
                //   is easiest to reproduce in Storybook, where the font hasn't loaded yet;
                //   we compute the size, then the font makes the text a bit larger, and
                //   there's a layout issue.
                setWidth((previousWidth = 0) => Math.ceil(Math.max(previousWidth, variantWidth + PADDING_HORIZONTAL)));
                setHeight((previousHeight = 0) => Math.ceil(Math.max(previousHeight, variantHeight + PADDING_VERTICAL)));
            } }, childrenByVariant[candidateVariant]))))));
};
exports.CallingLobbyJoinButton = CallingLobbyJoinButton;
