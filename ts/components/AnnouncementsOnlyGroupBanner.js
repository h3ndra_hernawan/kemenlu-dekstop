"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AnnouncementsOnlyGroupBanner = void 0;
const react_1 = __importStar(require("react"));
const Intl_1 = require("./Intl");
const Modal_1 = require("./Modal");
const ConversationListItem_1 = require("./conversationList/ConversationListItem");
const AnnouncementsOnlyGroupBanner = ({ groupAdmins, i18n, openConversation, theme, }) => {
    const [isShowingAdmins, setIsShowingAdmins] = (0, react_1.useState)(false);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        isShowingAdmins && (react_1.default.createElement(Modal_1.Modal, { i18n: i18n, onClose: () => setIsShowingAdmins(false), title: i18n('AnnouncementsOnlyGroupBanner--modal') }, groupAdmins.map(admin => (react_1.default.createElement(ConversationListItem_1.ConversationListItem, Object.assign({}, admin, { i18n: i18n, onClick: () => {
                openConversation(admin.id);
            }, draftPreview: "", lastMessage: undefined, lastUpdated: undefined, theme: theme })))))),
        react_1.default.createElement("div", { className: "AnnouncementsOnlyGroupBanner__banner" },
            react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "AnnouncementsOnlyGroupBanner--announcements-only", components: [
                    react_1.default.createElement("button", { className: "AnnouncementsOnlyGroupBanner__banner--admins", type: "button", onClick: () => setIsShowingAdmins(true) }, i18n('AnnouncementsOnlyGroupBanner--admins')),
                ] }))));
};
exports.AnnouncementsOnlyGroupBanner = AnnouncementsOnlyGroupBanner;
