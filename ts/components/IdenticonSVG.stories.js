"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const IdenticonSVG_1 = require("./IdenticonSVG");
const Colors_1 = require("../types/Colors");
const story = (0, react_2.storiesOf)('Components/IdenticonSVG', module);
Colors_1.AvatarColorMap.forEach((value, key) => story.add(key, () => (react_1.default.createElement(IdenticonSVG_1.IdenticonSVG, { backgroundColor: value.bg, content: "HI", foregroundColor: value.fg }))));
