"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatColorPicker = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const react_contextmenu_1 = require("react-contextmenu");
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const CustomColorEditor_1 = require("./CustomColorEditor");
const Modal_1 = require("./Modal");
const Colors_1 = require("../types/Colors");
const SampleMessageBubbles_1 = require("./SampleMessageBubbles");
const PanelRow_1 = require("./conversation/conversation-details/PanelRow");
const getCustomColorStyle_1 = require("../util/getCustomColorStyle");
const useRestoreFocus_1 = require("../hooks/useRestoreFocus");
const ChatColorPicker = ({ addCustomColor, colorSelected, conversationId, customColors = {}, editCustomColor, getConversationsWithCustomColor, i18n, isGlobal = false, removeCustomColor, removeCustomColorOnConversations, resetAllChatColors, resetDefaultChatColor, selectedColor = Colors_1.ConversationColors[0], selectedCustomColor, setGlobalDefaultConversationColor, }) => {
    const [confirmResetAll, setConfirmResetAll] = (0, react_1.useState)(false);
    const [confirmResetWhat, setConfirmResetWhat] = (0, react_1.useState)(false);
    const [customColorToEdit, setCustomColorToEdit] = (0, react_1.useState)(undefined);
    const [focusRef] = (0, useRestoreFocus_1.useDelayedRestoreFocus)();
    const onSelectColor = (conversationColor, customColorData) => {
        if (conversationId) {
            colorSelected({
                conversationId,
                conversationColor,
                customColorData,
            });
        }
        else {
            setGlobalDefaultConversationColor(conversationColor, customColorData);
        }
    };
    const renderCustomColorEditorWrapper = () => (react_1.default.createElement(CustomColorEditorWrapper, { customColorToEdit: customColorToEdit, i18n: i18n, onClose: () => setCustomColorToEdit(undefined), onSave: (color) => {
            if (customColorToEdit === null || customColorToEdit === void 0 ? void 0 : customColorToEdit.id) {
                editCustomColor(customColorToEdit.id, color);
                onSelectColor('custom', {
                    id: customColorToEdit.id,
                    value: color,
                });
            }
            else {
                addCustomColor(color, conversationId);
            }
        } }));
    return (react_1.default.createElement("div", { className: "ChatColorPicker__container" },
        customColorToEdit ? renderCustomColorEditorWrapper() : null,
        confirmResetWhat ? (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                {
                    action: resetDefaultChatColor,
                    style: 'affirmative',
                    text: i18n('ChatColorPicker__confirm-reset-default'),
                },
                {
                    action: () => {
                        resetDefaultChatColor();
                        resetAllChatColors();
                    },
                    style: 'affirmative',
                    text: i18n('ChatColorPicker__resetAll'),
                },
            ], i18n: i18n, onClose: () => {
                setConfirmResetWhat(false);
            }, title: i18n('ChatColorPicker__resetDefault') }, i18n('ChatColorPicker__confirm-reset-message'))) : null,
        confirmResetAll ? (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                {
                    action: resetAllChatColors,
                    style: 'affirmative',
                    text: i18n('ChatColorPicker__confirm-reset'),
                },
            ], i18n: i18n, onClose: () => {
                setConfirmResetAll(false);
            }, title: i18n('ChatColorPicker__resetAll') }, i18n('ChatColorPicker__confirm-reset-message'))) : null,
        react_1.default.createElement(SampleMessageBubbles_1.SampleMessageBubbles, { backgroundStyle: (0, getCustomColorStyle_1.getCustomColorStyle)(selectedCustomColor.value), color: selectedColor, i18n: i18n }),
        react_1.default.createElement("hr", null),
        react_1.default.createElement("div", { className: "ChatColorPicker__bubbles" },
            Colors_1.ConversationColors.map((color, i) => (react_1.default.createElement("div", { "aria-label": color, className: (0, classnames_1.default)(`ChatColorPicker__bubble ChatColorPicker__bubble--${color}`, {
                    'ChatColorPicker__bubble--selected': color === selectedColor,
                }), key: color, onClick: () => onSelectColor(color), onKeyDown: (ev) => {
                    if (ev.key === 'Enter') {
                        onSelectColor(color);
                    }
                }, role: "button", tabIndex: 0, ref: i === 0 ? focusRef : undefined }))),
            Object.keys(customColors).map(colorId => {
                const colorValues = customColors[colorId];
                return (react_1.default.createElement(CustomColorBubble, { color: colorValues, colorId: colorId, getConversationsWithCustomColor: getConversationsWithCustomColor, key: colorId, i18n: i18n, isSelected: colorId === selectedCustomColor.id, onChoose: () => {
                        onSelectColor('custom', {
                            id: colorId,
                            value: colorValues,
                        });
                    }, onDelete: () => {
                        removeCustomColor(colorId);
                        removeCustomColorOnConversations(colorId);
                    }, onDupe: () => {
                        addCustomColor(colorValues, conversationId);
                    }, onEdit: () => {
                        setCustomColorToEdit({ id: colorId, value: colorValues });
                    } }));
            }),
            react_1.default.createElement("div", { "aria-label": i18n('ChatColorPicker__custom-color--label'), className: "ChatColorPicker__bubble ChatColorPicker__bubble--custom", onClick: () => setCustomColorToEdit({ id: undefined, value: undefined }), onKeyDown: (ev) => {
                    if (ev.key === 'Enter') {
                        setCustomColorToEdit({ id: undefined, value: undefined });
                    }
                }, role: "button", tabIndex: 0 },
                react_1.default.createElement("i", { className: "ChatColorPicker__add-icon" }))),
        react_1.default.createElement("hr", null),
        conversationId ? (react_1.default.createElement(PanelRow_1.PanelRow, { label: i18n('ChatColorPicker__reset'), onClick: () => {
                colorSelected({ conversationId });
            } })) : null,
        react_1.default.createElement(PanelRow_1.PanelRow, { label: i18n('ChatColorPicker__resetAll'), onClick: () => {
                if (isGlobal) {
                    setConfirmResetWhat(true);
                }
                else {
                    setConfirmResetAll(true);
                }
            } })));
};
exports.ChatColorPicker = ChatColorPicker;
const CustomColorBubble = ({ color, colorId, getConversationsWithCustomColor, i18n, isSelected, onDelete, onDupe, onEdit, onChoose, }) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const menuRef = (0, react_1.useRef)(null);
    const [confirmDeleteCount, setConfirmDeleteCount] = (0, react_1.useState)(undefined);
    const handleClick = (ev) => {
        if (!isSelected) {
            onChoose();
            return;
        }
        if (menuRef && menuRef.current) {
            menuRef.current.handleContextClick(ev);
        }
    };
    const bubble = (react_1.default.createElement("div", { "aria-label": colorId, className: (0, classnames_1.default)({
            ChatColorPicker__bubble: true,
            'ChatColorPicker__bubble--custom-selected': isSelected,
            'ChatColorPicker__bubble--selected': isSelected,
        }), onClick: handleClick, onKeyDown: (ev) => {
            if (ev.key === 'Enter') {
                handleClick(ev);
            }
        }, role: "button", tabIndex: 0, style: Object.assign({}, (0, getCustomColorStyle_1.getCustomColorStyle)(color)) }));
    return (react_1.default.createElement(react_1.default.Fragment, null,
        confirmDeleteCount ? (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                {
                    action: onDelete,
                    style: 'negative',
                    text: i18n('ChatColorPicker__context--delete'),
                },
            ], i18n: i18n, onClose: () => {
                setConfirmDeleteCount(undefined);
            }, title: i18n('ChatColorPicker__delete--title') }, i18n('ChatColorPicker__delete--message', [
            String(confirmDeleteCount),
        ]))) : null,
        isSelected ? (react_1.default.createElement(react_contextmenu_1.ContextMenuTrigger, { id: colorId, ref: menuRef }, bubble)) : (bubble),
        react_1.default.createElement(react_contextmenu_1.ContextMenu, { id: colorId },
            react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                    className: 'ChatColorPicker__context--edit',
                }, onClick: (event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    onEdit();
                } }, i18n('ChatColorPicker__context--edit')),
            react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                    className: 'ChatColorPicker__context--duplicate',
                }, onClick: (event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    onDupe();
                } }, i18n('ChatColorPicker__context--duplicate')),
            react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                    className: 'ChatColorPicker__context--delete',
                }, onClick: async (event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    const conversations = await getConversationsWithCustomColor(colorId);
                    if (!conversations.length) {
                        onDelete();
                    }
                    else {
                        setConfirmDeleteCount(conversations.length);
                    }
                } }, i18n('ChatColorPicker__context--delete')))));
};
const CustomColorEditorWrapper = ({ customColorToEdit, i18n, onClose, onSave, }) => {
    const editor = (react_1.default.createElement(CustomColorEditor_1.CustomColorEditor, { customColor: customColorToEdit === null || customColorToEdit === void 0 ? void 0 : customColorToEdit.value, i18n: i18n, onClose: onClose, onSave: onSave }));
    return (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, i18n: i18n, moduleClassName: "ChatColorPicker__modal", noMouseClose: true, onClose: onClose, title: i18n('CustomColorEditor__title') }, editor));
};
