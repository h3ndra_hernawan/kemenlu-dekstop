"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.WhatsNewModal = void 0;
const react_1 = __importDefault(require("react"));
const moment_1 = __importDefault(require("moment"));
const Modal_1 = require("./Modal");
const Intl_1 = require("./Intl");
const Emojify_1 = require("./conversation/Emojify");
const renderText = ({ key, text }) => (react_1.default.createElement(Emojify_1.Emojify, { key: key, text: text }));
const releaseNotes = {
    date: new Date(((_a = window.getBuildCreation) === null || _a === void 0 ? void 0 : _a.call(window)) || Date.now()),
    version: window.getVersion(),
    features: [1].map(n => ({
        key: `WhatsNew__v5.25--${n}`,
        components: undefined,
    })),
};
const WhatsNewModal = ({ i18n, hideWhatsNewModal, }) => {
    let contentNode;
    if (releaseNotes.features.length === 1) {
        const { key, components } = releaseNotes.features[0];
        contentNode = (react_1.default.createElement("p", null,
            react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: key, renderText: renderText, components: components })));
    }
    else {
        contentNode = (react_1.default.createElement("ul", null, releaseNotes.features.map(({ key, components }) => (react_1.default.createElement("li", { key: key },
            react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: key, renderText: renderText, components: components }))))));
    }
    return (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, i18n: i18n, onClose: hideWhatsNewModal, title: i18n('WhatsNew__modal-title') },
        react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("span", null,
                (0, moment_1.default)(releaseNotes.date).format('LL'),
                " \u00B7",
                ' ',
                releaseNotes.version),
            contentNode)));
};
exports.WhatsNewModal = WhatsNewModal;
