"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GlobalAudioProvider = exports.GlobalAudioContext = exports.computePeaks = void 0;
const React = __importStar(require("react"));
const p_queue_1 = __importDefault(require("p-queue"));
const lru_cache_1 = __importDefault(require("lru-cache"));
const log = __importStar(require("../logging/log"));
const MAX_WAVEFORM_COUNT = 1000;
const MAX_PARALLEL_COMPUTE = 8;
const MAX_AUDIO_DURATION = 15 * 60; // 15 minutes
// This context's value is effectively global. This is not ideal but is necessary because
//   the app has multiple React roots. In the future, we should use a single React root
//   and instantiate these inside of `GlobalAudioProvider`. (We may wish to keep
//   `audioContext` global, however, as the browser limits the number that can be
//   created.)
const audioContext = new AudioContext();
audioContext.suspend();
const waveformCache = new lru_cache_1.default({
    max: MAX_WAVEFORM_COUNT,
});
const inProgressMap = new Map();
const computeQueue = new p_queue_1.default({
    concurrency: MAX_PARALLEL_COMPUTE,
});
async function getAudioDuration(url, buffer) {
    const blob = new Blob([buffer]);
    const blobURL = URL.createObjectURL(blob);
    const audio = new Audio();
    audio.muted = true;
    audio.src = blobURL;
    await new Promise((resolve, reject) => {
        audio.addEventListener('loadedmetadata', () => {
            resolve();
        });
        audio.addEventListener('error', event => {
            const error = new Error(`Failed to load audio from: ${url} due to error: ${event.type}`);
            reject(error);
        });
    });
    if (Number.isNaN(audio.duration)) {
        throw new Error(`Invalid audio duration for: ${url}`);
    }
    return audio.duration;
}
/**
 * Load audio from `url`, decode PCM data, and compute RMS peaks for displaying
 * the waveform.
 *
 * The results are cached in the `waveformCache` which is shared across
 * messages in the conversation and provided by GlobalAudioContext.
 *
 * The computation happens off the renderer thread by AudioContext, but it is
 * still quite expensive, so we cache it in the `waveformCache` LRU cache.
 */
async function doComputePeaks(url, barCount) {
    const existing = waveformCache.get(url);
    if (existing) {
        log.info('GlobalAudioContext: waveform cache hit', url);
        return Promise.resolve(existing);
    }
    log.info('GlobalAudioContext: waveform cache miss', url);
    // Load and decode `url` into a raw PCM
    const response = await fetch(url);
    const raw = await response.arrayBuffer();
    const duration = await getAudioDuration(url, raw);
    const peaks = new Array(barCount).fill(0);
    if (duration > MAX_AUDIO_DURATION) {
        log.info(`GlobalAudioContext: audio ${url} duration ${duration}s is too long`);
        const emptyResult = { peaks, duration };
        waveformCache.set(url, emptyResult);
        return emptyResult;
    }
    const data = await audioContext.decodeAudioData(raw);
    // Compute RMS peaks
    const norms = new Array(barCount).fill(0);
    const samplesPerPeak = data.length / peaks.length;
    for (let channelNum = 0; channelNum < data.numberOfChannels; channelNum += 1) {
        const channel = data.getChannelData(channelNum);
        for (let sample = 0; sample < channel.length; sample += 1) {
            const i = Math.floor(sample / samplesPerPeak);
            peaks[i] += channel[sample] ** 2;
            norms[i] += 1;
        }
    }
    // Average
    let max = 1e-23;
    for (let i = 0; i < peaks.length; i += 1) {
        peaks[i] = Math.sqrt(peaks[i] / Math.max(1, norms[i]));
        max = Math.max(max, peaks[i]);
    }
    // Normalize
    for (let i = 0; i < peaks.length; i += 1) {
        peaks[i] /= max;
    }
    const result = { peaks, duration };
    waveformCache.set(url, result);
    return result;
}
async function computePeaks(url, barCount) {
    const computeKey = `${url}:${barCount}`;
    const pending = inProgressMap.get(computeKey);
    if (pending) {
        log.info('GlobalAudioContext: already computing peaks for', computeKey);
        return pending;
    }
    log.info('GlobalAudioContext: queue computing peaks for', computeKey);
    const promise = computeQueue.add(() => doComputePeaks(url, barCount));
    inProgressMap.set(computeKey, promise);
    try {
        return await promise;
    }
    finally {
        inProgressMap.delete(computeKey);
    }
}
exports.computePeaks = computePeaks;
const globalContents = {
    audio: new Audio(),
    computePeaks,
};
exports.GlobalAudioContext = React.createContext(globalContents);
/**
 * A global context that holds Audio, AudioContext, LRU instances that are used
 * inside the conversation by ts/components/conversation/MessageAudio.tsx
 */
const GlobalAudioProvider = ({ conversationId, isPaused, children, }) => {
    // When moving between conversations - stop audio
    React.useEffect(() => {
        return () => {
            globalContents.audio.pause();
        };
    }, [conversationId]);
    // Pause when requested by parent
    React.useEffect(() => {
        if (isPaused) {
            globalContents.audio.pause();
        }
    }, [isPaused]);
    return (React.createElement(exports.GlobalAudioContext.Provider, { value: globalContents }, children));
};
exports.GlobalAudioProvider = GlobalAudioProvider;
