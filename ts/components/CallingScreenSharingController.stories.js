"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const CallingScreenSharingController_1 = require("./CallingScreenSharingController");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    i18n,
    onCloseController: (0, addon_actions_1.action)('on-close-controller'),
    onStopSharing: (0, addon_actions_1.action)('on-stop-sharing'),
    presentedSourceName: overrideProps.presentedSourceName || 'Application',
});
const story = (0, react_2.storiesOf)('Components/CallingScreenSharingController', module);
story.add('Controller', () => {
    return react_1.default.createElement(CallingScreenSharingController_1.CallingScreenSharingController, Object.assign({}, createProps()));
});
story.add('Really long app name', () => {
    return (react_1.default.createElement(CallingScreenSharingController_1.CallingScreenSharingController, Object.assign({}, createProps({
        presentedSourceName: 'A really long application name that is super long',
    }))));
});
