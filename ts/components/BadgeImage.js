"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadgeImage = void 0;
const react_1 = __importDefault(require("react"));
const Spinner_1 = require("./Spinner");
const getBadgeImageFileLocalPath_1 = require("../badges/getBadgeImageFileLocalPath");
const BadgeImageTheme_1 = require("../badges/BadgeImageTheme");
function BadgeImage({ badge, size, }) {
    const { name } = badge;
    const imagePath = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, size, BadgeImageTheme_1.BadgeImageTheme.Transparent);
    if (!imagePath) {
        return (react_1.default.createElement(Spinner_1.Spinner, { ariaLabel: name, moduleClassName: "BadgeImage BadgeImage__loading", size: `${size}px`, svgSize: "normal" }));
    }
    return (react_1.default.createElement("img", { alt: name, className: "BadgeImage", src: imagePath, style: {
            width: size,
            height: size,
        } }));
}
exports.BadgeImage = BadgeImage;
