"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadgeCarouselIndex = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const assert_1 = require("../util/assert");
function BadgeCarouselIndex({ currentIndex, totalCount, }) {
    (0, assert_1.strictAssert)(totalCount >= 1, 'Expected 1 or more items');
    (0, assert_1.strictAssert)(currentIndex < totalCount, 'Expected current index to be in range');
    if (totalCount < 2) {
        return null;
    }
    return (react_1.default.createElement("div", { "aria-hidden": true, className: "BadgeCarouselIndex" }, (0, lodash_1.times)(totalCount, index => (react_1.default.createElement("div", { key: index, className: (0, classnames_1.default)('BadgeCarouselIndex__dot', currentIndex === index && 'BadgeCarouselIndex__dot--selected') })))));
}
exports.BadgeCarouselIndex = BadgeCarouselIndex;
