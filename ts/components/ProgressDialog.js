"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProgressDialog = void 0;
const React = __importStar(require("react"));
const Spinner_1 = require("./Spinner");
// TODO: This should use <Modal>. See DESKTOP-1038.
exports.ProgressDialog = React.memo(({ i18n }) => {
    return (React.createElement("div", { className: "module-progress-dialog" },
        React.createElement("div", { className: "module-progress-dialog__spinner" },
            React.createElement(Spinner_1.Spinner, { svgSize: "normal", size: "39px", direction: "on-progress-dialog" })),
        React.createElement("div", { className: "module-progress-dialog__text" }, i18n('updating'))));
});
