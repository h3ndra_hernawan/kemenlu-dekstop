"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallingScreenSharingController = void 0;
const react_1 = __importDefault(require("react"));
const Button_1 = require("./Button");
const CallingScreenSharingController = ({ i18n, onCloseController, onStopSharing, presentedSourceName, }) => {
    return (react_1.default.createElement("div", { className: "module-CallingScreenSharingController" },
        react_1.default.createElement("div", { className: "module-CallingScreenSharingController__text" }, i18n('calling__presenting--info', [presentedSourceName])),
        react_1.default.createElement("div", { className: "module-CallingScreenSharingController__buttons" },
            react_1.default.createElement(Button_1.Button, { className: "module-CallingScreenSharingController__button", onClick: onStopSharing, variant: Button_1.ButtonVariant.Destructive }, i18n('calling__presenting--stop')),
            react_1.default.createElement("button", { "aria-label": i18n('close'), className: "module-CallingScreenSharingController__close", onClick: onCloseController, type: "button" }))));
};
exports.CallingScreenSharingController = CallingScreenSharingController;
