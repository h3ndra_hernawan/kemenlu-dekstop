"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallingLobby = void 0;
const react_1 = __importDefault(require("react"));
const focus_trap_react_1 = __importDefault(require("focus-trap-react"));
const classnames_1 = __importDefault(require("classnames"));
const CallingButton_1 = require("./CallingButton");
const Tooltip_1 = require("./Tooltip");
const CallBackgroundBlur_1 = require("./CallBackgroundBlur");
const CallingHeader_1 = require("./CallingHeader");
const CallingPreCallInfo_1 = require("./CallingPreCallInfo");
const CallingLobbyJoinButton_1 = require("./CallingLobbyJoinButton");
const useIsOnline_1 = require("../hooks/useIsOnline");
const KeyboardLayout = __importStar(require("../services/keyboardLayout"));
const isConversationTooBigToRing_1 = require("../conversations/isConversationTooBigToRing");
const CallingLobby = ({ availableCameras, conversation, groupMembers, hasLocalAudio, hasLocalVideo, i18n, isGroupCall = false, isGroupCallOutboundRingEnabled, isCallFull = false, me, onCallCanceled, onJoinCall, peekedParticipants, setLocalAudio, setLocalPreview, setLocalVideo, setOutgoingRing, showParticipantsList, toggleParticipants, toggleSettings, outgoingRing, }) => {
    const localVideoRef = react_1.default.useRef(null);
    const shouldShowLocalVideo = hasLocalVideo && availableCameras.length > 0;
    const toggleAudio = react_1.default.useCallback(() => {
        setLocalAudio({ enabled: !hasLocalAudio });
    }, [hasLocalAudio, setLocalAudio]);
    const toggleVideo = react_1.default.useCallback(() => {
        setLocalVideo({ enabled: !hasLocalVideo });
    }, [hasLocalVideo, setLocalVideo]);
    const toggleOutgoingRing = react_1.default.useCallback(() => {
        setOutgoingRing(!outgoingRing);
    }, [outgoingRing, setOutgoingRing]);
    react_1.default.useEffect(() => {
        setLocalPreview({ element: localVideoRef });
        return () => {
            setLocalPreview({ element: undefined });
        };
    }, [setLocalPreview]);
    react_1.default.useEffect(() => {
        function handleKeyDown(event) {
            let eventHandled = false;
            const key = KeyboardLayout.lookup(event);
            if (event.shiftKey && (key === 'V' || key === 'v')) {
                toggleVideo();
                eventHandled = true;
            }
            else if (event.shiftKey && (key === 'M' || key === 'm')) {
                toggleAudio();
                eventHandled = true;
            }
            if (eventHandled) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
        document.addEventListener('keydown', handleKeyDown);
        return () => {
            document.removeEventListener('keydown', handleKeyDown);
        };
    }, [toggleVideo, toggleAudio]);
    const isOnline = (0, useIsOnline_1.useIsOnline)();
    const [isCallConnecting, setIsCallConnecting] = react_1.default.useState(false);
    // eslint-disable-next-line no-nested-ternary
    const videoButtonType = hasLocalVideo
        ? CallingButton_1.CallingButtonType.VIDEO_ON
        : availableCameras.length === 0
            ? CallingButton_1.CallingButtonType.VIDEO_DISABLED
            : CallingButton_1.CallingButtonType.VIDEO_OFF;
    const audioButtonType = hasLocalAudio
        ? CallingButton_1.CallingButtonType.AUDIO_ON
        : CallingButton_1.CallingButtonType.AUDIO_OFF;
    const isGroupTooLargeToRing = (0, isConversationTooBigToRing_1.isConversationTooBigToRing)(conversation);
    const isRingButtonVisible = isGroupCall &&
        isGroupCallOutboundRingEnabled &&
        peekedParticipants.length === 0 &&
        (groupMembers || []).length > 1;
    let preCallInfoRingMode;
    if (isGroupCall) {
        preCallInfoRingMode =
            outgoingRing && !isGroupTooLargeToRing
                ? CallingPreCallInfo_1.RingMode.WillRing
                : CallingPreCallInfo_1.RingMode.WillNotRing;
    }
    else {
        preCallInfoRingMode = CallingPreCallInfo_1.RingMode.WillRing;
    }
    let ringButtonType;
    if (isRingButtonVisible) {
        if (isGroupTooLargeToRing) {
            ringButtonType = CallingButton_1.CallingButtonType.RING_DISABLED;
        }
        else if (outgoingRing) {
            ringButtonType = CallingButton_1.CallingButtonType.RING_ON;
        }
        else {
            ringButtonType = CallingButton_1.CallingButtonType.RING_OFF;
        }
    }
    else {
        ringButtonType = CallingButton_1.CallingButtonType.RING_DISABLED;
    }
    const canJoin = !isCallFull && !isCallConnecting && isOnline;
    let callingLobbyJoinButtonVariant;
    if (isCallFull) {
        callingLobbyJoinButtonVariant = CallingLobbyJoinButton_1.CallingLobbyJoinButtonVariant.CallIsFull;
    }
    else if (isCallConnecting) {
        callingLobbyJoinButtonVariant = CallingLobbyJoinButton_1.CallingLobbyJoinButtonVariant.Loading;
    }
    else if (peekedParticipants.length) {
        callingLobbyJoinButtonVariant = CallingLobbyJoinButton_1.CallingLobbyJoinButtonVariant.Join;
    }
    else {
        callingLobbyJoinButtonVariant = CallingLobbyJoinButton_1.CallingLobbyJoinButtonVariant.Start;
    }
    return (react_1.default.createElement(focus_trap_react_1.default, null,
        react_1.default.createElement("div", { className: "module-calling__container" },
            shouldShowLocalVideo ? (react_1.default.createElement("video", { className: "module-CallingLobby__local-preview module-CallingLobby__local-preview--camera-is-on", ref: localVideoRef, autoPlay: true })) : (react_1.default.createElement(CallBackgroundBlur_1.CallBackgroundBlur, { className: "module-CallingLobby__local-preview module-CallingLobby__local-preview--camera-is-off", avatarPath: me.avatarPath, color: me.color })),
            react_1.default.createElement(CallingHeader_1.CallingHeader, { i18n: i18n, isGroupCall: isGroupCall, participantCount: peekedParticipants.length, showParticipantsList: showParticipantsList, toggleParticipants: toggleParticipants, toggleSettings: toggleSettings, onCancel: onCallCanceled }),
            react_1.default.createElement(CallingPreCallInfo_1.CallingPreCallInfo, { conversation: conversation, groupMembers: groupMembers, i18n: i18n, isCallFull: isCallFull, me: me, peekedParticipants: peekedParticipants, ringMode: preCallInfoRingMode }),
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-CallingLobby__camera-is-off', `module-CallingLobby__camera-is-off--${shouldShowLocalVideo ? 'invisible' : 'visible'}`) }, i18n('calling__your-video-is-off')),
            react_1.default.createElement("div", { className: "module-calling__buttons module-calling__buttons--inline" },
                react_1.default.createElement(CallingButton_1.CallingButton, { buttonType: videoButtonType, i18n: i18n, onClick: toggleVideo, tooltipDirection: Tooltip_1.TooltipPlacement.Top }),
                react_1.default.createElement(CallingButton_1.CallingButton, { buttonType: audioButtonType, i18n: i18n, onClick: toggleAudio, tooltipDirection: Tooltip_1.TooltipPlacement.Top }),
                react_1.default.createElement(CallingButton_1.CallingButton, { buttonType: ringButtonType, i18n: i18n, isVisible: isRingButtonVisible, onClick: toggleOutgoingRing, tooltipDirection: Tooltip_1.TooltipPlacement.Top })),
            react_1.default.createElement(CallingLobbyJoinButton_1.CallingLobbyJoinButton, { disabled: !canJoin, i18n: i18n, onClick: () => {
                    setIsCallConnecting(true);
                    onJoinCall();
                }, variant: callingLobbyJoinButtonVariant }))));
};
exports.CallingLobby = CallingLobby;
