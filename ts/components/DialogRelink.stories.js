"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const DialogRelink_1 = require("./DialogRelink");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const _util_1 = require("./_util");
const FakeLeftPaneContainer_1 = require("../test-both/helpers/FakeLeftPaneContainer");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const defaultProps = {
    containerWidthBreakpoint: _util_1.WidthBreakpoint.Wide,
    i18n,
    isRegistrationDone: true,
    relinkDevice: (0, addon_actions_1.action)('relink-device'),
};
const permutations = [
    {
        title: 'Unlinked (wide container)',
        props: {
            containerWidthBreakpoint: _util_1.WidthBreakpoint.Wide,
            isRegistrationDone: false,
        },
    },
    {
        title: 'Unlinked (narrow container)',
        props: {
            containerWidthBreakpoint: _util_1.WidthBreakpoint.Narrow,
            isRegistrationDone: false,
        },
    },
];
(0, react_1.storiesOf)('Components/DialogRelink', module)
    .add('Knobs Playground', () => {
    const isRegistrationDone = (0, addon_knobs_1.boolean)('isRegistrationDone', false);
    return (React.createElement(DialogRelink_1.DialogRelink, Object.assign({}, defaultProps, { isRegistrationDone: isRegistrationDone })));
})
    .add('Iterations', () => {
    return permutations.map(({ props, title }) => (React.createElement(React.Fragment, null,
        React.createElement("h3", null, title),
        React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: props.containerWidthBreakpoint },
            React.createElement(DialogRelink_1.DialogRelink, Object.assign({}, defaultProps, props))))));
});
