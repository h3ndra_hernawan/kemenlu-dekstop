"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const LeftPane_1 = require("./LeftPane");
const CaptchaDialog_1 = require("./CaptchaDialog");
const MessageSearchResult_1 = require("./conversationList/MessageSearchResult");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const StorybookThemeContext_1 = require("../../.storybook/StorybookThemeContext");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/LeftPane', module);
const defaultConversations = [
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'fred-convo',
        title: 'Fred Willard',
    }),
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'marc-convo',
        isSelected: true,
        title: 'Marc Barraca',
    }),
];
const defaultGroups = [
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'biking-group',
        title: 'Mtn Biking Arizona 🚵☀️⛰',
        type: 'group',
        sharedGroupNames: [],
    }),
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'dance-group',
        title: 'Are we dancers? 💃',
        type: 'group',
        sharedGroupNames: [],
    }),
];
const defaultArchivedConversations = [
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'michelle-archive-convo',
        title: 'Michelle Mercure',
        isArchived: true,
    }),
];
const pinnedConversations = [
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'philly-convo',
        isPinned: true,
        title: 'Philip Glass',
    }),
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'robbo-convo',
        isPinned: true,
        title: 'Robert Moog',
    }),
];
const defaultModeSpecificProps = {
    mode: LeftPane_1.LeftPaneMode.Inbox,
    pinnedConversations,
    conversations: defaultConversations,
    archivedConversations: defaultArchivedConversations,
    isAboutToSearchInAConversation: false,
    startSearchCounter: 0,
};
const emptySearchResultsGroup = { isLoading: false, results: [] };
const useProps = (overrideProps = {}) => (Object.assign({ badgesById: {}, cantAddContactToGroup: (0, addon_actions_1.action)('cantAddContactToGroup'), canResizeLeftPane: true, clearGroupCreationError: (0, addon_actions_1.action)('clearGroupCreationError'), clearSearch: (0, addon_actions_1.action)('clearSearch'), closeCantAddContactToGroupModal: (0, addon_actions_1.action)('closeCantAddContactToGroupModal'), closeMaximumGroupSizeModal: (0, addon_actions_1.action)('closeMaximumGroupSizeModal'), closeRecommendedGroupSizeModal: (0, addon_actions_1.action)('closeRecommendedGroupSizeModal'), composeDeleteAvatarFromDisk: (0, addon_actions_1.action)('composeDeleteAvatarFromDisk'), composeReplaceAvatar: (0, addon_actions_1.action)('composeReplaceAvatar'), composeSaveAvatarToDisk: (0, addon_actions_1.action)('composeSaveAvatarToDisk'), createGroup: (0, addon_actions_1.action)('createGroup'), i18n, modeSpecificProps: defaultModeSpecificProps, preferredWidthFromStorage: 320, openConversationInternal: (0, addon_actions_1.action)('openConversationInternal'), regionCode: 'US', challengeStatus: (0, addon_knobs_1.select)('challengeStatus', ['idle', 'required', 'pending'], 'idle'), setChallengeStatus: (0, addon_actions_1.action)('setChallengeStatus'), renderExpiredBuildDialog: () => React.createElement("div", null), renderMainHeader: () => React.createElement("div", null), renderMessageSearchResult: (id) => (React.createElement(MessageSearchResult_1.MessageSearchResult, { body: "Lorem ipsum wow", bodyRanges: [], conversationId: "marc-convo", from: defaultConversations[0], i18n: i18n, id: id, openConversationInternal: (0, addon_actions_1.action)('openConversationInternal'), sentAt: 1587358800000, snippet: "Lorem <<left>>ipsum<<right>> wow", to: defaultConversations[1] })), renderNetworkStatus: () => React.createElement("div", null), renderRelinkDialog: () => React.createElement("div", null), renderUpdateDialog: () => React.createElement("div", null), renderCaptchaDialog: () => (React.createElement(CaptchaDialog_1.CaptchaDialog, { i18n: i18n, isPending: overrideProps.challengeStatus === 'pending', onContinue: (0, addon_actions_1.action)('onCaptchaContinue'), onSkip: (0, addon_actions_1.action)('onCaptchaSkip') })), selectedConversationId: undefined, selectedMessageId: undefined, savePreferredLeftPaneWidth: (0, addon_actions_1.action)('savePreferredLeftPaneWidth'), searchInConversation: (0, addon_actions_1.action)('searchInConversation'), setComposeSearchTerm: (0, addon_actions_1.action)('setComposeSearchTerm'), setComposeGroupAvatar: (0, addon_actions_1.action)('setComposeGroupAvatar'), setComposeGroupName: (0, addon_actions_1.action)('setComposeGroupName'), setComposeGroupExpireTimer: (0, addon_actions_1.action)('setComposeGroupExpireTimer'), showArchivedConversations: (0, addon_actions_1.action)('showArchivedConversations'), showInbox: (0, addon_actions_1.action)('showInbox'), startComposing: (0, addon_actions_1.action)('startComposing'), showChooseGroupMembers: (0, addon_actions_1.action)('showChooseGroupMembers'), startNewConversationFromPhoneNumber: (0, addon_actions_1.action)('startNewConversationFromPhoneNumber'), startNewConversationFromUsername: (0, addon_actions_1.action)('startNewConversationFromUsername'), startSearch: (0, addon_actions_1.action)('startSearch'), startSettingGroupMetadata: (0, addon_actions_1.action)('startSettingGroupMetadata'), theme: React.useContext(StorybookThemeContext_1.StorybookThemeContext), toggleComposeEditingAvatar: (0, addon_actions_1.action)('toggleComposeEditingAvatar'), toggleConversationInChooseMembers: (0, addon_actions_1.action)('toggleConversationInChooseMembers'), updateSearchTerm: (0, addon_actions_1.action)('updateSearchTerm') }, overrideProps));
// Inbox stories
story.add('Inbox: no conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Inbox,
        pinnedConversations: [],
        conversations: [],
        archivedConversations: [],
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    },
})))));
story.add('Inbox: only pinned conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Inbox,
        pinnedConversations,
        conversations: [],
        archivedConversations: [],
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    },
})))));
story.add('Inbox: only non-pinned conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Inbox,
        pinnedConversations: [],
        conversations: defaultConversations,
        archivedConversations: [],
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    },
})))));
story.add('Inbox: only archived conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Inbox,
        pinnedConversations: [],
        conversations: [],
        archivedConversations: defaultArchivedConversations,
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    },
})))));
story.add('Inbox: pinned and archived conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Inbox,
        pinnedConversations,
        conversations: [],
        archivedConversations: defaultArchivedConversations,
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    },
})))));
story.add('Inbox: non-pinned and archived conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Inbox,
        pinnedConversations: [],
        conversations: defaultConversations,
        archivedConversations: defaultArchivedConversations,
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    },
})))));
story.add('Inbox: pinned and non-pinned conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Inbox,
        pinnedConversations,
        conversations: defaultConversations,
        archivedConversations: [],
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    },
})))));
story.add('Inbox: pinned, non-pinned, and archived conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps()))));
// Search stories
story.add('Search: no results when searching everywhere', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Search,
        conversationResults: emptySearchResultsGroup,
        contactResults: emptySearchResultsGroup,
        messageResults: emptySearchResultsGroup,
        searchTerm: 'foo bar',
        primarySendsSms: false,
    },
})))));
story.add('Search: no results when searching everywhere (SMS)', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Search,
        conversationResults: emptySearchResultsGroup,
        contactResults: emptySearchResultsGroup,
        messageResults: emptySearchResultsGroup,
        searchTerm: 'foo bar',
        primarySendsSms: true,
    },
})))));
story.add('Search: no results when searching in a conversation', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Search,
        conversationResults: emptySearchResultsGroup,
        contactResults: emptySearchResultsGroup,
        messageResults: emptySearchResultsGroup,
        searchConversationName: 'Bing Bong',
        searchTerm: 'foo bar',
        primarySendsSms: false,
    },
})))));
story.add('Search: all results loading', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Search,
        conversationResults: { isLoading: true },
        contactResults: { isLoading: true },
        messageResults: { isLoading: true },
        searchTerm: 'foo bar',
        primarySendsSms: false,
    },
})))));
story.add('Search: some results loading', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Search,
        conversationResults: {
            isLoading: false,
            results: defaultConversations,
        },
        contactResults: { isLoading: true },
        messageResults: { isLoading: true },
        searchTerm: 'foo bar',
        primarySendsSms: false,
    },
})))));
story.add('Search: has conversations and contacts, but not messages', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Search,
        conversationResults: {
            isLoading: false,
            results: defaultConversations,
        },
        contactResults: { isLoading: false, results: defaultConversations },
        messageResults: { isLoading: false, results: [] },
        searchTerm: 'foo bar',
        primarySendsSms: false,
    },
})))));
story.add('Search: all results', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Search,
        conversationResults: {
            isLoading: false,
            results: defaultConversations,
        },
        contactResults: { isLoading: false, results: defaultConversations },
        messageResults: {
            isLoading: false,
            results: [
                { id: 'msg1', conversationId: 'foo' },
                { id: 'msg2', conversationId: 'bar' },
            ],
        },
        searchTerm: 'foo bar',
        primarySendsSms: false,
    },
})))));
// Archived stories
story.add('Archive: no archived conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Archive,
        archivedConversations: [],
        searchConversation: undefined,
        searchTerm: '',
    },
})))));
story.add('Archive: archived conversations', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Archive,
        archivedConversations: defaultConversations,
        searchConversation: undefined,
        searchTerm: '',
    },
})))));
story.add('Archive: searching a conversation', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Archive,
        archivedConversations: defaultConversations,
        searchConversation: defaultConversations[0],
        searchTerm: 'foo bar',
        conversationResults: { isLoading: true },
        contactResults: { isLoading: true },
        messageResults: { isLoading: true },
        primarySendsSms: false,
    },
})))));
// Compose stories
story.add('Compose: no results', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: [],
        composeGroups: [],
        isUsernamesEnabled: true,
        isFetchingUsername: false,
        regionCode: 'US',
        searchTerm: '',
    },
})))));
story.add('Compose: some contacts, no search term', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: defaultConversations,
        composeGroups: [],
        isUsernamesEnabled: true,
        isFetchingUsername: false,
        regionCode: 'US',
        searchTerm: '',
    },
})))));
story.add('Compose: some contacts, with a search term', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: defaultConversations,
        composeGroups: [],
        isUsernamesEnabled: true,
        isFetchingUsername: false,
        regionCode: 'US',
        searchTerm: 'ar',
    },
})))));
story.add('Compose: some groups, no search term', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: [],
        composeGroups: defaultGroups,
        isUsernamesEnabled: true,
        isFetchingUsername: false,
        regionCode: 'US',
        searchTerm: '',
    },
})))));
story.add('Compose: some groups, with search term', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: [],
        composeGroups: defaultGroups,
        isUsernamesEnabled: true,
        isFetchingUsername: false,
        regionCode: 'US',
        searchTerm: 'ar',
    },
})))));
story.add('Compose: search is valid username', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: [],
        composeGroups: [],
        isUsernamesEnabled: true,
        isFetchingUsername: false,
        regionCode: 'US',
        searchTerm: 'someone',
    },
})))));
story.add('Compose: search is valid username, fetching username', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: [],
        composeGroups: [],
        isUsernamesEnabled: true,
        isFetchingUsername: true,
        regionCode: 'US',
        searchTerm: 'someone',
    },
})))));
story.add('Compose: search is valid username, but flag is not enabled', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: [],
        composeGroups: [],
        isUsernamesEnabled: false,
        isFetchingUsername: false,
        regionCode: 'US',
        searchTerm: 'someone',
    },
})))));
story.add('Compose: all kinds of results, no search term', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: defaultConversations,
        composeGroups: defaultGroups,
        isUsernamesEnabled: true,
        isFetchingUsername: false,
        regionCode: 'US',
        searchTerm: '',
    },
})))));
story.add('Compose: all kinds of results, with a search term', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Compose,
        composeContacts: defaultConversations,
        composeGroups: defaultGroups,
        isUsernamesEnabled: true,
        isFetchingUsername: false,
        regionCode: 'US',
        searchTerm: 'someone',
    },
})))));
// Captcha flow
story.add('Captcha dialog: required', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Inbox,
        pinnedConversations,
        conversations: defaultConversations,
        archivedConversations: [],
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    },
    challengeStatus: 'required',
})))));
story.add('Captcha dialog: pending', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.Inbox,
        pinnedConversations,
        conversations: defaultConversations,
        archivedConversations: [],
        isAboutToSearchInAConversation: false,
        startSearchCounter: 0,
    },
    challengeStatus: 'pending',
})))));
// Set group metadata
story.add('Group Metadata: No Timer', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.SetGroupMetadata,
        groupAvatar: undefined,
        groupName: 'Group 1',
        groupExpireTimer: 0,
        hasError: false,
        isCreating: false,
        isEditingAvatar: false,
        selectedContacts: defaultConversations,
        userAvatarData: [],
    },
})))));
story.add('Group Metadata: Regular Timer', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.SetGroupMetadata,
        groupAvatar: undefined,
        groupName: 'Group 1',
        groupExpireTimer: 24 * 3600,
        hasError: false,
        isCreating: false,
        isEditingAvatar: false,
        selectedContacts: defaultConversations,
        userAvatarData: [],
    },
})))));
story.add('Group Metadata: Custom Timer', () => (React.createElement(LeftPane_1.LeftPane, Object.assign({}, useProps({
    modeSpecificProps: {
        mode: LeftPane_1.LeftPaneMode.SetGroupMetadata,
        groupAvatar: undefined,
        groupName: 'Group 1',
        groupExpireTimer: 7 * 3600,
        hasError: false,
        isCreating: false,
        isEditingAvatar: false,
        selectedContacts: defaultConversations,
        userAvatarData: [],
    },
})))));
