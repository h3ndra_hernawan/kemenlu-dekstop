"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const Tooltip_1 = require("./Tooltip");
const theme_1 = require("../util/theme");
const createProps = (overrideProps = {}) => ({
    content: overrideProps.content || 'Hello World',
    direction: (0, addon_knobs_1.select)('direction', Tooltip_1.TooltipPlacement, overrideProps.direction),
    sticky: overrideProps.sticky,
    theme: overrideProps.theme,
});
const story = (0, react_1.storiesOf)('Components/Tooltip', module);
const Trigger = (React.createElement("span", { style: {
        display: 'inline-block',
        marginTop: 200,
        marginBottom: 200,
        marginLeft: 200,
        marginRight: 200,
    } }, "Trigger"));
story.add('Top', () => (React.createElement(Tooltip_1.Tooltip, Object.assign({}, createProps({
    direction: Tooltip_1.TooltipPlacement.Top,
})), Trigger)));
story.add('Right', () => (React.createElement(Tooltip_1.Tooltip, Object.assign({}, createProps({
    direction: Tooltip_1.TooltipPlacement.Right,
})), Trigger)));
story.add('Bottom', () => (React.createElement(Tooltip_1.Tooltip, Object.assign({}, createProps({
    direction: Tooltip_1.TooltipPlacement.Bottom,
})), Trigger)));
story.add('Left', () => (React.createElement(Tooltip_1.Tooltip, Object.assign({}, createProps({
    direction: Tooltip_1.TooltipPlacement.Left,
})), Trigger)));
story.add('Sticky', () => (React.createElement(Tooltip_1.Tooltip, Object.assign({}, createProps({
    sticky: true,
})), Trigger)));
story.add('With Applied Popper Modifiers', () => {
    return (React.createElement(Tooltip_1.Tooltip, Object.assign({}, createProps({
        direction: Tooltip_1.TooltipPlacement.Bottom,
    }), { popperModifiers: [
            {
                name: 'offset',
                options: {
                    offset: [80, 80],
                },
            },
        ] }), Trigger));
});
story.add('Dark Theme', () => (React.createElement(Tooltip_1.Tooltip, Object.assign({}, createProps({
    sticky: true,
    theme: theme_1.Theme.Dark,
})), Trigger)));
