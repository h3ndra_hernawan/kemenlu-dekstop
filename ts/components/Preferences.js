"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Preferences = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const classnames_1 = __importDefault(require("classnames"));
const Button_1 = require("./Button");
const ChatColorPicker_1 = require("./ChatColorPicker");
const Checkbox_1 = require("./Checkbox");
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const DisappearingTimeDialog_1 = require("./DisappearingTimeDialog");
const phoneNumberDiscoverability_1 = require("../util/phoneNumberDiscoverability");
const phoneNumberSharingMode_1 = require("../util/phoneNumberSharingMode");
const Select_1 = require("./Select");
const Spinner_1 = require("./Spinner");
const getCustomColorStyle_1 = require("../util/getCustomColorStyle");
const expirationTimer_1 = require("../util/expirationTimer");
const useEscapeHandling_1 = require("../hooks/useEscapeHandling");
var Page;
(function (Page) {
    // Accessible through left nav
    Page["General"] = "General";
    Page["Appearance"] = "Appearance";
    Page["Chats"] = "Chats";
    Page["Calls"] = "Calls";
    Page["Notifications"] = "Notifications";
    Page["Privacy"] = "Privacy";
    // Sub pages
    Page["ChatColor"] = "ChatColor";
})(Page || (Page = {}));
const DEFAULT_ZOOM_FACTORS = [
    {
        text: '75%',
        value: 0.75,
    },
    {
        text: '100%',
        value: 1,
    },
    {
        text: '125%',
        value: 1.25,
    },
    {
        text: '150%',
        value: 1.5,
    },
    {
        text: '200%',
        value: 2,
    },
];
const Preferences = ({ addCustomColor, availableCameras, availableMicrophones, availableSpeakers, blockedCount, closeSettings, customColors, defaultConversationColor, deviceName = '', doDeleteAllData, doneRendering, editCustomColor, getConversationsWithCustomColor, hasAudioNotifications, hasAutoDownloadUpdate, hasAutoLaunch, hasCallNotifications, hasCallRingtoneNotification, hasCountMutedConversations, hasHideMenuBar, hasIncomingCallNotifications, hasLinkPreviews, hasMediaCameraPermissions, hasMediaPermissions, hasMinimizeToAndStartInSystemTray, hasMinimizeToSystemTray, hasNotificationAttention, hasNotifications, hasReadReceipts, hasRelayCalls, hasSpellCheck, hasTypingIndicators, i18n, initialSpellCheckSetting, isAudioNotificationsSupported, isAutoDownloadUpdatesSupported, isAutoLaunchSupported, isHideMenuBarSupported, isPhoneNumberSharingSupported, isNotificationAttentionSupported, isSyncSupported, isSystemTraySupported, lastSyncTime, makeSyncRequest, notificationContent, onAudioNotificationsChange, onAutoDownloadUpdateChange, onAutoLaunchChange, onCallNotificationsChange, onCallRingtoneNotificationChange, onCountMutedConversationsChange, onHideMenuBarChange, onIncomingCallNotificationsChange, onLastSyncTimeChange, onMediaCameraPermissionsChange, onMediaPermissionsChange, onMinimizeToAndStartInSystemTrayChange, onMinimizeToSystemTrayChange, onNotificationAttentionChange, onNotificationContentChange, onNotificationsChange, onRelayCallsChange, onSelectedCameraChange, onSelectedMicrophoneChange, onSelectedSpeakerChange, onSpellCheckChange, onThemeChange, onUniversalExpireTimerChange, onZoomFactorChange, removeCustomColor, removeCustomColorOnConversations, resetAllChatColors, resetDefaultChatColor, selectedCamera, selectedMicrophone, selectedSpeaker, setGlobalDefaultConversationColor, themeSetting, universalExpireTimer = 0, whoCanFindMe, whoCanSeeMe, zoomFactor, }) => {
    var _a;
    const [confirmDelete, setConfirmDelete] = (0, react_1.useState)(false);
    const [page, setPage] = (0, react_1.useState)(Page.General);
    const [showSyncFailed, setShowSyncFailed] = (0, react_1.useState)(false);
    const [nowSyncing, setNowSyncing] = (0, react_1.useState)(false);
    const [showDisappearingTimerDialog, setShowDisappearingTimerDialog] = (0, react_1.useState)(false);
    (0, react_1.useEffect)(() => {
        doneRendering();
    }, [doneRendering]);
    (0, useEscapeHandling_1.useEscapeHandling)(closeSettings);
    const onZoomSelectChange = (0, react_1.useCallback)((value) => {
        const number = parseFloat(value);
        onZoomFactorChange(number);
    }, [onZoomFactorChange]);
    const onAudioInputSelectChange = (0, react_1.useCallback)((value) => {
        if (value === 'undefined') {
            onSelectedMicrophoneChange(undefined);
        }
        else {
            onSelectedMicrophoneChange(availableMicrophones[parseInt(value, 10)]);
        }
    }, [onSelectedMicrophoneChange, availableMicrophones]);
    const onAudioOutputSelectChange = (0, react_1.useCallback)((value) => {
        if (value === 'undefined') {
            onSelectedSpeakerChange(undefined);
        }
        else {
            onSelectedSpeakerChange(availableSpeakers[parseInt(value, 10)]);
        }
    }, [onSelectedSpeakerChange, availableSpeakers]);
    let settings;
    if (page === Page.General) {
        settings = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "Preferences__title" },
                react_1.default.createElement("div", { className: "Preferences__title--header" }, i18n('Preferences__button--general'))),
            react_1.default.createElement(SettingsRow, null,
                react_1.default.createElement(Control, { left: i18n('Preferences--device-name'), right: deviceName })),
            react_1.default.createElement(SettingsRow, { title: i18n('Preferences--system') },
                isAutoLaunchSupported && (react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasAutoLaunch, label: i18n('autoLaunchDescription'), moduleClassName: "Preferences__checkbox", name: "autoLaunch", onChange: onAutoLaunchChange })),
                isHideMenuBarSupported && (react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasHideMenuBar, label: i18n('hideMenuBar'), moduleClassName: "Preferences__checkbox", name: "hideMenuBar", onChange: onHideMenuBarChange })),
                isSystemTraySupported && (react_1.default.createElement(react_1.default.Fragment, null,
                    react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasMinimizeToSystemTray, label: i18n('SystemTraySetting__minimize-to-system-tray'), moduleClassName: "Preferences__checkbox", name: "system-tray-setting-minimize-to-system-tray", onChange: onMinimizeToSystemTrayChange }),
                    react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasMinimizeToAndStartInSystemTray, disabled: !hasMinimizeToSystemTray, label: i18n('SystemTraySetting__minimize-to-and-start-in-system-tray'), moduleClassName: "Preferences__checkbox", name: "system-tray-setting-minimize-to-and-start-in-system-tray", onChange: onMinimizeToAndStartInSystemTrayChange })))),
            react_1.default.createElement(SettingsRow, { title: i18n('permissions') },
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasMediaPermissions, label: i18n('mediaPermissionsDescription'), moduleClassName: "Preferences__checkbox", name: "mediaPermissions", onChange: onMediaPermissionsChange }),
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasMediaCameraPermissions, label: i18n('mediaCameraPermissionsDescription'), moduleClassName: "Preferences__checkbox", name: "mediaCameraPermissions", onChange: onMediaCameraPermissionsChange })),
            isAutoDownloadUpdatesSupported && (react_1.default.createElement(SettingsRow, { title: i18n('Preferences--updates') },
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasAutoDownloadUpdate, label: i18n('Preferences__download-update'), moduleClassName: "Preferences__checkbox", name: "autoDownloadUpdate", onChange: onAutoDownloadUpdateChange })))));
    }
    else if (page === Page.Appearance) {
        let zoomFactors = DEFAULT_ZOOM_FACTORS;
        if (!zoomFactors.some(({ value }) => value === zoomFactor)) {
            zoomFactors = [
                ...zoomFactors,
                {
                    text: `${Math.round(zoomFactor * 100)}%`,
                    value: zoomFactor,
                },
            ].sort((a, b) => a.value - b.value);
        }
        settings = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "Preferences__title" },
                react_1.default.createElement("div", { className: "Preferences__title--header" }, i18n('Preferences__button--appearance'))),
            react_1.default.createElement(SettingsRow, null,
                react_1.default.createElement(Control, { left: i18n('Preferences--theme'), right: react_1.default.createElement(Select_1.Select, { onChange: onThemeChange, options: [
                            {
                                text: i18n('themeSystem'),
                                value: 'system',
                            },
                            {
                                text: i18n('themeLight'),
                                value: 'light',
                            },
                            {
                                text: i18n('themeDark'),
                                value: 'dark',
                            },
                        ], value: themeSetting }) }),
                react_1.default.createElement(Control, { left: i18n('showChatColorEditor'), onClick: () => {
                        setPage(Page.ChatColor);
                    }, right: react_1.default.createElement("div", { className: `ConversationDetails__chat-color ConversationDetails__chat-color--${defaultConversationColor.color}`, style: Object.assign({}, (0, getCustomColorStyle_1.getCustomColorStyle)((_a = defaultConversationColor.customColorData) === null || _a === void 0 ? void 0 : _a.value)) }) }),
                react_1.default.createElement(Control, { left: i18n('Preferences--zoom'), right: react_1.default.createElement(Select_1.Select, { onChange: onZoomSelectChange, options: zoomFactors, value: zoomFactor }) }))));
    }
    else if (page === Page.Chats) {
        let spellCheckDirtyText;
        if (initialSpellCheckSetting !== hasSpellCheck) {
            spellCheckDirtyText = hasSpellCheck
                ? i18n('spellCheckWillBeEnabled')
                : i18n('spellCheckWillBeDisabled');
        }
        const lastSyncDate = new Date(lastSyncTime || 0);
        settings = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "Preferences__title" },
                react_1.default.createElement("div", { className: "Preferences__title--header" }, i18n('Preferences__button--chats'))),
            react_1.default.createElement(SettingsRow, { title: i18n('Preferences__button--chats') },
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasSpellCheck, description: spellCheckDirtyText, label: i18n('spellCheckDescription'), moduleClassName: "Preferences__checkbox", name: "spellcheck", onChange: onSpellCheckChange }),
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasLinkPreviews, description: i18n('Preferences__link-previews--description'), disabled: true, label: i18n('Preferences__link-previews--title'), moduleClassName: "Preferences__checkbox", name: "linkPreviews", onChange: lodash_1.noop })),
            isSyncSupported && (react_1.default.createElement(SettingsRow, null,
                react_1.default.createElement(Control, { left: react_1.default.createElement(react_1.default.Fragment, null,
                        react_1.default.createElement("div", null, i18n('sync')),
                        react_1.default.createElement("div", { className: "Preferences__description" },
                            i18n('syncExplanation'),
                            ' ',
                            i18n('Preferences--lastSynced', {
                                date: lastSyncDate.toLocaleDateString(),
                                time: lastSyncDate.toLocaleTimeString(),
                            })),
                        showSyncFailed && (react_1.default.createElement("div", { className: "Preferences__description Preferences__description--error" }, i18n('syncFailed')))), right: react_1.default.createElement("div", { className: "Preferences__right-button" },
                        react_1.default.createElement(Button_1.Button, { disabled: nowSyncing, onClick: async () => {
                                setShowSyncFailed(false);
                                setNowSyncing(true);
                                try {
                                    await makeSyncRequest();
                                    onLastSyncTimeChange(Date.now());
                                }
                                catch (err) {
                                    setShowSyncFailed(true);
                                }
                                finally {
                                    setNowSyncing(false);
                                }
                            }, variant: Button_1.ButtonVariant.SecondaryAffirmative }, nowSyncing ? react_1.default.createElement(Spinner_1.Spinner, { svgSize: "small" }) : i18n('syncNow'))) })))));
    }
    else if (page === Page.Calls) {
        settings = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "Preferences__title" },
                react_1.default.createElement("div", { className: "Preferences__title--header" }, i18n('Preferences__button--calls'))),
            react_1.default.createElement(SettingsRow, { title: i18n('calling') },
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasIncomingCallNotifications, label: i18n('incomingCallNotificationDescription'), moduleClassName: "Preferences__checkbox", name: "incomingCallNotification", onChange: onIncomingCallNotificationsChange }),
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasCallRingtoneNotification, label: i18n('callRingtoneNotificationDescription'), moduleClassName: "Preferences__checkbox", name: "callRingtoneNotification", onChange: onCallRingtoneNotificationChange })),
            react_1.default.createElement(SettingsRow, { title: i18n('Preferences__devices') },
                react_1.default.createElement(Control, { left: react_1.default.createElement(react_1.default.Fragment, null,
                        react_1.default.createElement("label", { className: "Preferences__select-title", htmlFor: "video" }, i18n('callingDeviceSelection__label--video')),
                        react_1.default.createElement(Select_1.Select, { disabled: !availableCameras.length, moduleClassName: "Preferences__select", name: "video", onChange: onSelectedCameraChange, options: availableCameras.length
                                ? availableCameras.map(device => ({
                                    text: localizeDefault(i18n, device.label),
                                    value: device.deviceId,
                                }))
                                : [
                                    {
                                        text: i18n('callingDeviceSelection__select--no-device'),
                                        value: 'undefined',
                                    },
                                ], value: selectedCamera })), right: react_1.default.createElement("div", null) }),
                react_1.default.createElement(Control, { left: react_1.default.createElement(react_1.default.Fragment, null,
                        react_1.default.createElement("label", { className: "Preferences__select-title", htmlFor: "audio-input" }, i18n('callingDeviceSelection__label--audio-input')),
                        react_1.default.createElement(Select_1.Select, { disabled: !availableMicrophones.length, moduleClassName: "Preferences__select", name: "audio-input", onChange: onAudioInputSelectChange, options: availableMicrophones.length
                                ? availableMicrophones.map(device => ({
                                    text: localizeDefault(i18n, device.name),
                                    value: device.index,
                                }))
                                : [
                                    {
                                        text: i18n('callingDeviceSelection__select--no-device'),
                                        value: 'undefined',
                                    },
                                ], value: selectedMicrophone === null || selectedMicrophone === void 0 ? void 0 : selectedMicrophone.index })), right: react_1.default.createElement("div", null) }),
                react_1.default.createElement(Control, { left: react_1.default.createElement(react_1.default.Fragment, null,
                        react_1.default.createElement("label", { className: "Preferences__select-title", htmlFor: "audio-output" }, i18n('callingDeviceSelection__label--audio-output')),
                        react_1.default.createElement(Select_1.Select, { disabled: !availableSpeakers.length, moduleClassName: "Preferences__select", name: "audio-output", onChange: onAudioOutputSelectChange, options: availableSpeakers.length
                                ? availableSpeakers.map(device => ({
                                    text: localizeDefault(i18n, device.name),
                                    value: device.index,
                                }))
                                : [
                                    {
                                        text: i18n('callingDeviceSelection__select--no-device'),
                                        value: 'undefined',
                                    },
                                ], value: selectedSpeaker === null || selectedSpeaker === void 0 ? void 0 : selectedSpeaker.index })), right: react_1.default.createElement("div", null) })),
            react_1.default.createElement(SettingsRow, { title: i18n('Preferences--advanced') },
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasRelayCalls, description: i18n('alwaysRelayCallsDetail'), label: i18n('alwaysRelayCallsDescription'), moduleClassName: "Preferences__checkbox", name: "relayCalls", onChange: onRelayCallsChange }))));
    }
    else if (page === Page.Notifications) {
        settings = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "Preferences__title" },
                react_1.default.createElement("div", { className: "Preferences__title--header" }, i18n('Preferences__button--notifications'))),
            react_1.default.createElement(SettingsRow, null,
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasNotifications, label: i18n('Preferences__enable-notifications'), moduleClassName: "Preferences__checkbox", name: "notifications", onChange: onNotificationsChange }),
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasCallNotifications, label: i18n('callSystemNotificationDescription'), moduleClassName: "Preferences__checkbox", name: "callSystemNotification", onChange: onCallNotificationsChange }),
                isNotificationAttentionSupported && (react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasNotificationAttention, label: i18n('notificationDrawAttention'), moduleClassName: "Preferences__checkbox", name: "notificationDrawAttention", onChange: onNotificationAttentionChange })),
                isAudioNotificationsSupported && (react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasAudioNotifications, label: i18n('audioNotificationDescription'), moduleClassName: "Preferences__checkbox", name: "audioNotification", onChange: onAudioNotificationsChange })),
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasCountMutedConversations, label: i18n('countMutedConversationsDescription'), moduleClassName: "Preferences__checkbox", name: "countMutedConversations", onChange: onCountMutedConversationsChange })),
            react_1.default.createElement(SettingsRow, null,
                react_1.default.createElement(Control, { left: i18n('Preferences--notification-content'), right: react_1.default.createElement(Select_1.Select, { disabled: !hasNotifications, onChange: onNotificationContentChange, options: [
                            {
                                text: i18n('nameAndMessage'),
                                value: 'message',
                            },
                            {
                                text: i18n('nameOnly'),
                                value: 'name',
                            },
                            {
                                text: i18n('noNameOrMessage'),
                                value: 'count',
                            },
                        ], value: notificationContent }) }))));
    }
    else if (page === Page.Privacy) {
        const isCustomDisappearingMessageValue = !expirationTimer_1.DEFAULT_DURATIONS_SET.has(universalExpireTimer);
        settings = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "Preferences__title" },
                react_1.default.createElement("div", { className: "Preferences__title--header" }, i18n('Preferences__button--privacy'))),
            react_1.default.createElement(SettingsRow, null,
                react_1.default.createElement(Control, { left: i18n('Preferences--blocked'), right: blockedCount === 1
                        ? i18n('Preferences--blocked-count-singular', [
                            String(blockedCount),
                        ])
                        : i18n('Preferences--blocked-count-plural', [
                            String(blockedCount || 0),
                        ]) })),
            isPhoneNumberSharingSupported ? (react_1.default.createElement(SettingsRow, { title: i18n('Preferences__who-can--title') },
                react_1.default.createElement(Control, { left: i18n('Preferences--see-me'), right: react_1.default.createElement(Select_1.Select, { disabled: true, onChange: lodash_1.noop, options: [
                            {
                                text: i18n('Preferences__who-can--everybody'),
                                value: phoneNumberSharingMode_1.PhoneNumberSharingMode.Everybody,
                            },
                            {
                                text: i18n('Preferences__who-can--contacts'),
                                value: phoneNumberSharingMode_1.PhoneNumberSharingMode.ContactsOnly,
                            },
                            {
                                text: i18n('Preferences__who-can--nobody'),
                                value: phoneNumberSharingMode_1.PhoneNumberSharingMode.Nobody,
                            },
                        ], value: whoCanSeeMe }) }),
                react_1.default.createElement(Control, { left: i18n('Preferences--find-me'), right: react_1.default.createElement(Select_1.Select, { disabled: true, onChange: lodash_1.noop, options: [
                            {
                                text: i18n('Preferences__who-can--everybody'),
                                value: phoneNumberDiscoverability_1.PhoneNumberDiscoverability.Discoverable,
                            },
                            {
                                text: i18n('Preferences__who-can--nobody'),
                                value: phoneNumberDiscoverability_1.PhoneNumberDiscoverability.NotDiscoverable,
                            },
                        ], value: whoCanFindMe }) }),
                react_1.default.createElement("div", { className: "Preferences__padding" },
                    react_1.default.createElement("div", { className: "Preferences__description" }, i18n('Preferences__privacy--description'))))) : null,
            react_1.default.createElement(SettingsRow, { title: i18n('Preferences--messaging') },
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasReadReceipts, disabled: true, label: i18n('Preferences--read-receipts'), moduleClassName: "Preferences__checkbox", name: "readReceipts", onChange: lodash_1.noop }),
                react_1.default.createElement(Checkbox_1.Checkbox, { checked: hasTypingIndicators, disabled: true, label: i18n('Preferences--typing-indicators'), moduleClassName: "Preferences__checkbox", name: "typingIndicators", onChange: lodash_1.noop }),
                react_1.default.createElement("div", { className: "Preferences__padding" },
                    react_1.default.createElement("div", { className: "Preferences__description" }, i18n('Preferences__privacy--description')))),
            showDisappearingTimerDialog && (react_1.default.createElement(DisappearingTimeDialog_1.DisappearingTimeDialog, { i18n: i18n, initialValue: universalExpireTimer, onClose: () => setShowDisappearingTimerDialog(false), onSubmit: onUniversalExpireTimerChange })),
            react_1.default.createElement(SettingsRow, { title: i18n('disappearingMessages') },
                react_1.default.createElement(Control, { left: react_1.default.createElement(react_1.default.Fragment, null,
                        react_1.default.createElement("div", null, i18n('settings__DisappearingMessages__timer__label')),
                        react_1.default.createElement("div", { className: "Preferences__description" }, i18n('settings__DisappearingMessages__footer'))), right: react_1.default.createElement(Select_1.Select, { onChange: value => {
                            if (value === String(universalExpireTimer) ||
                                value === '-1') {
                                setShowDisappearingTimerDialog(true);
                                return;
                            }
                            onUniversalExpireTimerChange(parseInt(value, 10));
                        }, options: expirationTimer_1.DEFAULT_DURATIONS_IN_SECONDS.map(seconds => {
                            const text = (0, expirationTimer_1.format)(i18n, seconds, {
                                capitalizeOff: true,
                            });
                            return {
                                value: seconds,
                                text,
                            };
                        }).concat([
                            {
                                value: isCustomDisappearingMessageValue
                                    ? universalExpireTimer
                                    : -1,
                                text: isCustomDisappearingMessageValue
                                    ? (0, expirationTimer_1.format)(i18n, universalExpireTimer)
                                    : i18n('selectedCustomDisappearingTimeOption'),
                            },
                        ]), value: universalExpireTimer }) })),
            react_1.default.createElement(SettingsRow, null,
                react_1.default.createElement(Control, { left: react_1.default.createElement(react_1.default.Fragment, null,
                        react_1.default.createElement("div", null, i18n('clearDataHeader')),
                        react_1.default.createElement("div", { className: "Preferences__description" }, i18n('clearDataExplanation'))), right: react_1.default.createElement("div", { className: "Preferences__right-button" },
                        react_1.default.createElement(Button_1.Button, { onClick: () => setConfirmDelete(true), variant: Button_1.ButtonVariant.SecondaryDestructive }, i18n('clearDataButton'))) })),
            confirmDelete ? (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                    {
                        action: doDeleteAllData,
                        style: 'negative',
                        text: i18n('clearDataButton'),
                    },
                ], i18n: i18n, onClose: () => {
                    setConfirmDelete(false);
                }, title: i18n('deleteAllDataHeader') }, i18n('deleteAllDataBody'))) : null));
    }
    else if (page === Page.ChatColor) {
        settings = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "Preferences__title" },
                react_1.default.createElement("button", { "aria-label": i18n('goBack'), className: "Preferences__back-icon", onClick: () => setPage(Page.Appearance), type: "button" }),
                react_1.default.createElement("div", { className: "Preferences__title--header" }, i18n('ChatColorPicker__menu-title'))),
            react_1.default.createElement(ChatColorPicker_1.ChatColorPicker, { customColors: customColors, getConversationsWithCustomColor: getConversationsWithCustomColor, i18n: i18n, isGlobal: true, selectedColor: defaultConversationColor.color, selectedCustomColor: defaultConversationColor.customColorData || {}, 
                // actions
                addCustomColor: addCustomColor, colorSelected: lodash_1.noop, editCustomColor: editCustomColor, removeCustomColor: removeCustomColor, removeCustomColorOnConversations: removeCustomColorOnConversations, resetAllChatColors: resetAllChatColors, resetDefaultChatColor: resetDefaultChatColor, setGlobalDefaultConversationColor: setGlobalDefaultConversationColor })));
    }
    return (react_1.default.createElement("div", { className: "Preferences" },
        react_1.default.createElement("div", { className: "Preferences__page-selector" },
            react_1.default.createElement("button", { type: "button", className: (0, classnames_1.default)({
                    Preferences__button: true,
                    'Preferences__button--general': true,
                    'Preferences__button--selected': page === Page.General,
                }), onClick: () => setPage(Page.General) }, i18n('Preferences__button--general')),
            react_1.default.createElement("button", { type: "button", className: (0, classnames_1.default)({
                    Preferences__button: true,
                    'Preferences__button--appearance': true,
                    'Preferences__button--selected': page === Page.Appearance || page === Page.ChatColor,
                }), onClick: () => setPage(Page.Appearance) }, i18n('Preferences__button--appearance')),
            react_1.default.createElement("button", { type: "button", className: (0, classnames_1.default)({
                    Preferences__button: true,
                    'Preferences__button--chats': true,
                    'Preferences__button--selected': page === Page.Chats,
                }), onClick: () => setPage(Page.Chats) }, i18n('Preferences__button--chats')),
            react_1.default.createElement("button", { type: "button", className: (0, classnames_1.default)({
                    Preferences__button: true,
                    'Preferences__button--calls': true,
                    'Preferences__button--selected': page === Page.Calls,
                }), onClick: () => setPage(Page.Calls) }, i18n('Preferences__button--calls')),
            react_1.default.createElement("button", { type: "button", className: (0, classnames_1.default)({
                    Preferences__button: true,
                    'Preferences__button--notifications': true,
                    'Preferences__button--selected': page === Page.Notifications,
                }), onClick: () => setPage(Page.Notifications) }, i18n('Preferences__button--notifications')),
            react_1.default.createElement("button", { type: "button", className: (0, classnames_1.default)({
                    Preferences__button: true,
                    'Preferences__button--privacy': true,
                    'Preferences__button--selected': page === Page.Privacy,
                }), onClick: () => setPage(Page.Privacy) }, i18n('Preferences__button--privacy'))),
        react_1.default.createElement("div", { className: "Preferences__settings-pane" }, settings)));
};
exports.Preferences = Preferences;
const SettingsRow = ({ children, title, }) => {
    return (react_1.default.createElement("div", { className: "Preferences__settings-row" },
        title && react_1.default.createElement("h3", { className: "Preferences__padding" }, title),
        children));
};
const Control = ({ left, onClick, right, }) => {
    const content = (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: "Preferences__control--key" }, left),
        react_1.default.createElement("div", { className: "Preferences__control--value" }, right)));
    if (onClick) {
        return (react_1.default.createElement("button", { className: "Preferences__control Preferences__control--clickable", type: "button", onClick: onClick }, content));
    }
    return react_1.default.createElement("div", { className: "Preferences__control" }, content);
};
function localizeDefault(i18n, deviceLabel) {
    return deviceLabel.toLowerCase().startsWith('default')
        ? deviceLabel.replace(/default/i, i18n('callingDeviceSelection__select--default'))
        : deviceLabel;
}
