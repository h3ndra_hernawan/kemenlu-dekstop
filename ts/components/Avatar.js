"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports._getBadgePlacement = exports.Avatar = exports.AvatarSize = exports.AvatarBlur = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const Spinner_1 = require("./Spinner");
const getInitials_1 = require("../util/getInitials");
const Util_1 = require("../types/Util");
const log = __importStar(require("../logging/log"));
const assert_1 = require("../util/assert");
const shouldBlurAvatar_1 = require("../util/shouldBlurAvatar");
const getBadgeImageFileLocalPath_1 = require("../badges/getBadgeImageFileLocalPath");
const isBadgeVisible_1 = require("../badges/isBadgeVisible");
const BadgeImageTheme_1 = require("../badges/BadgeImageTheme");
const shouldShowBadges_1 = require("../badges/shouldShowBadges");
var AvatarBlur;
(function (AvatarBlur) {
    AvatarBlur[AvatarBlur["NoBlur"] = 0] = "NoBlur";
    AvatarBlur[AvatarBlur["BlurPicture"] = 1] = "BlurPicture";
    AvatarBlur[AvatarBlur["BlurPictureWithClickToView"] = 2] = "BlurPictureWithClickToView";
})(AvatarBlur = exports.AvatarBlur || (exports.AvatarBlur = {}));
var AvatarSize;
(function (AvatarSize) {
    AvatarSize[AvatarSize["SIXTEEN"] = 16] = "SIXTEEN";
    AvatarSize[AvatarSize["TWENTY_EIGHT"] = 28] = "TWENTY_EIGHT";
    AvatarSize[AvatarSize["THIRTY_TWO"] = 32] = "THIRTY_TWO";
    AvatarSize[AvatarSize["THIRTY_SIX"] = 36] = "THIRTY_SIX";
    AvatarSize[AvatarSize["FORTY_EIGHT"] = 48] = "FORTY_EIGHT";
    AvatarSize[AvatarSize["FIFTY_TWO"] = 52] = "FIFTY_TWO";
    AvatarSize[AvatarSize["EIGHTY"] = 80] = "EIGHTY";
    AvatarSize[AvatarSize["NINETY_SIX"] = 96] = "NINETY_SIX";
    AvatarSize[AvatarSize["ONE_HUNDRED_TWELVE"] = 112] = "ONE_HUNDRED_TWELVE";
})(AvatarSize = exports.AvatarSize || (exports.AvatarSize = {}));
const BADGE_PLACEMENT_BY_SIZE = new Map([
    [28, { size: 16, bottom: -4, right: -2 }],
    [32, { size: 16, bottom: -4, right: -2 }],
    [36, { size: 16, bottom: -3, right: 0 }],
    [40, { size: 24, bottom: -6, right: -4 }],
    [48, { size: 24, bottom: -6, right: -4 }],
    [56, { size: 24, bottom: -6, right: 0 }],
    [80, { size: 36, bottom: -8, right: 0 }],
    [88, { size: 36, bottom: -4, right: 3 }],
]);
const getDefaultBlur = (...args) => (0, shouldBlurAvatar_1.shouldBlurAvatar)(...args) ? AvatarBlur.BlurPicture : AvatarBlur.NoBlur;
const Avatar = ({ acceptedMessageRequest, avatarPath, badge, className, color = 'A200', conversationType, i18n, isMe, innerRef, loading, noteToSelf, onClick, sharedGroupNames, size, theme, title, unblurredAvatarPath, searchResult, blur = getDefaultBlur({
    acceptedMessageRequest,
    avatarPath,
    isMe,
    sharedGroupNames,
    unblurredAvatarPath,
}), }) => {
    const [imageBroken, setImageBroken] = (0, react_1.useState)(false);
    (0, react_1.useEffect)(() => {
        setImageBroken(false);
    }, [avatarPath]);
    (0, react_1.useEffect)(() => {
        if (!avatarPath) {
            return lodash_1.noop;
        }
        const image = new Image();
        image.src = avatarPath;
        image.onerror = () => {
            log.warn('Avatar: Image failed to load; failing over to placeholder');
            setImageBroken(true);
        };
        return () => {
            image.onerror = lodash_1.noop;
        };
    }, [avatarPath]);
    const initials = (0, getInitials_1.getInitials)(title);
    const hasImage = !noteToSelf && avatarPath && !imageBroken;
    const shouldUseInitials = !hasImage && conversationType === 'direct' && Boolean(initials);
    let contentsChildren;
    if (loading) {
        const svgSize = size < 40 ? 'small' : 'normal';
        contentsChildren = (react_1.default.createElement("div", { className: "module-Avatar__spinner-container" },
            react_1.default.createElement(Spinner_1.Spinner, { size: `${size - 8}px`, svgSize: svgSize, direction: "on-avatar" })));
    }
    else if (hasImage) {
        (0, assert_1.assert)(avatarPath, 'avatarPath should be defined here');
        (0, assert_1.assert)(blur !== AvatarBlur.BlurPictureWithClickToView || size >= 100, 'Rendering "click to view" for a small avatar. This may not render correctly');
        const isBlurred = blur === AvatarBlur.BlurPicture ||
            blur === AvatarBlur.BlurPictureWithClickToView;
        contentsChildren = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "module-Avatar__image", style: Object.assign({ backgroundImage: `url('${encodeURI(avatarPath)}')` }, (isBlurred ? { filter: `blur(${Math.ceil(size / 2)}px)` } : {})) }),
            blur === AvatarBlur.BlurPictureWithClickToView && (react_1.default.createElement("div", { className: "module-Avatar__click-to-view" }, i18n('view')))));
    }
    else if (searchResult) {
        contentsChildren = (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-Avatar__icon', 'module-Avatar__icon--search-result') }));
    }
    else if (noteToSelf) {
        contentsChildren = (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-Avatar__icon', 'module-Avatar__icon--note-to-self') }));
    }
    else if (shouldUseInitials) {
        contentsChildren = (react_1.default.createElement("div", { "aria-hidden": "true", className: "module-Avatar__label", style: { fontSize: Math.ceil(size * 0.45) } }, initials));
    }
    else {
        contentsChildren = (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-Avatar__icon', `module-Avatar__icon--${conversationType}`) }));
    }
    let contents;
    const contentsClassName = (0, classnames_1.default)('module-Avatar__contents', `module-Avatar__contents--${color}`);
    if (onClick) {
        contents = (react_1.default.createElement("button", { className: contentsClassName, type: "button", onClick: onClick }, contentsChildren));
    }
    else {
        contents = react_1.default.createElement("div", { className: contentsClassName }, contentsChildren);
    }
    let badgeNode;
    if (badge &&
        theme &&
        !noteToSelf &&
        (0, isBadgeVisible_1.isBadgeVisible)(badge) &&
        (0, shouldShowBadges_1.shouldShowBadges)()) {
        const badgePlacement = _getBadgePlacement(size);
        const badgeTheme = theme === Util_1.ThemeType.light ? BadgeImageTheme_1.BadgeImageTheme.Light : BadgeImageTheme_1.BadgeImageTheme.Dark;
        const badgeImagePath = (0, getBadgeImageFileLocalPath_1.getBadgeImageFileLocalPath)(badge, badgePlacement.size, badgeTheme);
        if (badgeImagePath) {
            badgeNode = (react_1.default.createElement("img", { alt: badge.name, className: "module-Avatar__badge", src: badgeImagePath, style: {
                    width: badgePlacement.size,
                    height: badgePlacement.size,
                    bottom: badgePlacement.bottom,
                    right: badgePlacement.right,
                } }));
        }
    }
    else if (badge && !theme) {
        log.error('<Avatar> requires a theme if a badge is provided');
    }
    return (react_1.default.createElement("div", { "aria-label": i18n('contactAvatarAlt', [title]), className: (0, classnames_1.default)('module-Avatar', hasImage ? 'module-Avatar--with-image' : 'module-Avatar--no-image', className), style: {
            minWidth: size,
            width: size,
            height: size,
        }, ref: innerRef },
        contents,
        badgeNode));
};
exports.Avatar = Avatar;
// This is only exported for testing.
function _getBadgePlacement(avatarSize) {
    return (BADGE_PLACEMENT_BY_SIZE.get(avatarSize) || {
        size: Math.ceil(avatarSize * 0.425),
        bottom: 0,
        right: 0,
    });
}
exports._getBadgePlacement = _getBadgePlacement;
