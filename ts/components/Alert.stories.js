"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const Alert_1 = require("./Alert");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/Alert', module);
const defaultProps = {
    i18n,
    onClose: (0, addon_actions_1.action)('onClose'),
};
const LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.';
story.add('Title and body are strings', () => (react_1.default.createElement(Alert_1.Alert, Object.assign({}, defaultProps, { title: "Hello world", body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus." }))));
story.add('Body is a ReactNode', () => (react_1.default.createElement(Alert_1.Alert, Object.assign({}, defaultProps, { title: "Hello world", body: react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("span", { style: { color: 'red' } }, "Hello"),
        ' ',
        react_1.default.createElement("span", { style: { color: 'green', fontWeight: 'bold' } }, "world"),
        "!") }))));
story.add('Long body (without title)', () => (react_1.default.createElement(Alert_1.Alert, Object.assign({}, defaultProps, { body: react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("p", null, LOREM_IPSUM),
        react_1.default.createElement("p", null, LOREM_IPSUM),
        react_1.default.createElement("p", null, LOREM_IPSUM),
        react_1.default.createElement("p", null, LOREM_IPSUM)) }))));
story.add('Long body (with title)', () => (react_1.default.createElement(Alert_1.Alert, Object.assign({}, defaultProps, { title: "Hello world", body: react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("p", null, LOREM_IPSUM),
        react_1.default.createElement("p", null, LOREM_IPSUM),
        react_1.default.createElement("p", null, LOREM_IPSUM),
        react_1.default.createElement("p", null, LOREM_IPSUM)) }))));
