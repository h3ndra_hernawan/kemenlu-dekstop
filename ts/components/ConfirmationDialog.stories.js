"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
(0, react_1.storiesOf)('Components/ConfirmationDialog', module)
    .add('ConfirmationDialog', () => {
    return (React.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onClose: (0, addon_actions_1.action)('onClose'), title: (0, addon_knobs_1.text)('Title', 'Foo bar banana baz?'), actions: [
            {
                text: 'Negate',
                style: 'negative',
                action: (0, addon_actions_1.action)('negative'),
            },
            {
                text: 'Affirm',
                style: 'affirmative',
                action: (0, addon_actions_1.action)('affirmative'),
            },
        ] }, (0, addon_knobs_1.text)('Child text', 'asdf blip')));
})
    .add('Custom cancel text', () => {
    return (React.createElement(ConfirmationDialog_1.ConfirmationDialog, { cancelText: "Nah", i18n: i18n, onClose: (0, addon_actions_1.action)('onClose'), title: (0, addon_knobs_1.text)('Title', 'Foo bar banana baz?'), actions: [
            {
                text: 'Maybe',
                style: 'affirmative',
                action: (0, addon_actions_1.action)('affirmative'),
            },
        ] }, (0, addon_knobs_1.text)('Child text', 'asdf blip')));
});
