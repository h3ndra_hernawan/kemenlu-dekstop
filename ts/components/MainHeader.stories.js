"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const MainHeader_1 = require("./MainHeader");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const Util_1 = require("../types/Util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/MainHeader', module);
const requiredText = (name, value) => (0, addon_knobs_1.text)(name, value || '');
const optionalText = (name, value) => (0, addon_knobs_1.text)(name, value || '') || undefined;
const createProps = (overrideProps = {}) => ({
    searchTerm: requiredText('searchTerm', overrideProps.searchTerm),
    searchConversation: overrideProps.searchConversation,
    selectedConversation: undefined,
    startSearchCounter: 0,
    theme: Util_1.ThemeType.light,
    phoneNumber: optionalText('phoneNumber', overrideProps.phoneNumber),
    title: requiredText('title', overrideProps.title),
    name: optionalText('name', overrideProps.name),
    avatarPath: optionalText('avatarPath', overrideProps.avatarPath),
    hasPendingUpdate: Boolean(overrideProps.hasPendingUpdate),
    i18n,
    updateSearchTerm: (0, addon_actions_1.action)('updateSearchTerm'),
    clearConversationSearch: (0, addon_actions_1.action)('clearConversationSearch'),
    clearSearch: (0, addon_actions_1.action)('clearSearch'),
    startUpdate: (0, addon_actions_1.action)('startUpdate'),
    showArchivedConversations: (0, addon_actions_1.action)('showArchivedConversations'),
    startComposing: (0, addon_actions_1.action)('startComposing'),
    toggleProfileEditor: (0, addon_actions_1.action)('toggleProfileEditor'),
});
story.add('Basic', () => {
    const props = createProps({});
    return React.createElement(MainHeader_1.MainHeader, Object.assign({}, props));
});
story.add('Name', () => {
    const props = createProps({
        name: 'John Smith',
        title: 'John Smith',
    });
    return React.createElement(MainHeader_1.MainHeader, Object.assign({}, props));
});
story.add('Phone Number', () => {
    const props = createProps({
        name: 'John Smith',
        phoneNumber: '+15553004000',
    });
    return React.createElement(MainHeader_1.MainHeader, Object.assign({}, props));
});
story.add('Search Term', () => {
    const props = createProps({
        name: 'John Smith',
        searchTerm: 'Hewwo?',
        title: 'John Smith',
    });
    return React.createElement(MainHeader_1.MainHeader, Object.assign({}, props));
});
story.add('Searching Conversation', () => {
    const props = createProps({
        name: 'John Smith',
        searchConversation: (0, getDefaultConversation_1.getDefaultConversation)(),
    });
    return React.createElement(MainHeader_1.MainHeader, Object.assign({}, props));
});
story.add('Searching Conversation with Term', () => {
    const props = createProps({
        name: 'John Smith',
        searchTerm: 'address',
        searchConversation: (0, getDefaultConversation_1.getDefaultConversation)(),
    });
    return React.createElement(MainHeader_1.MainHeader, Object.assign({}, props));
});
story.add('Update Available', () => {
    const props = createProps({ hasPendingUpdate: true });
    return React.createElement(MainHeader_1.MainHeader, Object.assign({}, props));
});
