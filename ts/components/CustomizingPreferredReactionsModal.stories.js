"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const CustomizingPreferredReactionsModal_1 = require("./CustomizingPreferredReactionsModal");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/CustomizingPreferredReactionsModal', module);
const defaultProps = {
    cancelCustomizePreferredReactionsModal: (0, addon_actions_1.action)('cancelCustomizePreferredReactionsModal'),
    deselectDraftEmoji: (0, addon_actions_1.action)('deselectDraftEmoji'),
    draftPreferredReactions: ['✨', '❇️', '🎇', '🦈', '💖', '🅿️'],
    hadSaveError: false,
    i18n,
    isSaving: false,
    onSetSkinTone: (0, addon_actions_1.action)('onSetSkinTone'),
    originalPreferredReactions: ['❤️', '👍', '👎', '😂', '😮', '😢'],
    recentEmojis: ['cake'],
    replaceSelectedDraftEmoji: (0, addon_actions_1.action)('replaceSelectedDraftEmoji'),
    resetDraftEmoji: (0, addon_actions_1.action)('resetDraftEmoji'),
    savePreferredReactions: (0, addon_actions_1.action)('savePreferredReactions'),
    selectDraftEmojiToBeReplaced: (0, addon_actions_1.action)('selectDraftEmojiToBeReplaced'),
    selectedDraftEmojiIndex: undefined,
    skinTone: 4,
};
story.add('Default', () => (react_1.default.createElement(CustomizingPreferredReactionsModal_1.CustomizingPreferredReactionsModal, Object.assign({}, defaultProps))));
story.add('Draft emoji selected', () => (react_1.default.createElement(CustomizingPreferredReactionsModal_1.CustomizingPreferredReactionsModal, Object.assign({}, defaultProps, { selectedDraftEmojiIndex: 4 }))));
story.add('Saving', () => (react_1.default.createElement(CustomizingPreferredReactionsModal_1.CustomizingPreferredReactionsModal, Object.assign({}, defaultProps, { isSaving: true }))));
story.add('Had error', () => (react_1.default.createElement(CustomizingPreferredReactionsModal_1.CustomizingPreferredReactionsModal, Object.assign({}, defaultProps, { hadSaveError: true }))));
