"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallNeedPermissionScreen = void 0;
const react_1 = __importStar(require("react"));
const Colors_1 = require("../types/Colors");
const Avatar_1 = require("./Avatar");
const Intl_1 = require("./Intl");
const ContactName_1 = require("./conversation/ContactName");
const AUTO_CLOSE_MS = 10000;
const CallNeedPermissionScreen = ({ conversation, i18n, close, }) => {
    const title = conversation.title || i18n('unknownContact');
    const autoCloseAtRef = (0, react_1.useRef)(Date.now() + AUTO_CLOSE_MS);
    (0, react_1.useEffect)(() => {
        const timeout = setTimeout(close, autoCloseAtRef.current - Date.now());
        return clearTimeout.bind(null, timeout);
    }, [autoCloseAtRef, close]);
    return (react_1.default.createElement("div", { className: "module-call-need-permission-screen" },
        react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: conversation.acceptedMessageRequest, avatarPath: conversation.avatarPath, color: conversation.color || Colors_1.AvatarColors[0], noteToSelf: false, conversationType: "direct", i18n: i18n, isMe: conversation.isMe, name: conversation.name, phoneNumber: conversation.phoneNumber, profileName: conversation.profileName, title: conversation.title, sharedGroupNames: conversation.sharedGroupNames, size: 112 }),
        react_1.default.createElement("p", { className: "module-call-need-permission-screen__text" },
            react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "callNeedPermission", components: [react_1.default.createElement(ContactName_1.ContactName, { title: title })] })),
        react_1.default.createElement("button", { type: "button", className: "module-call-need-permission-screen__button", onClick: () => {
                close();
            } }, i18n('close'))));
};
exports.CallNeedPermissionScreen = CallNeedPermissionScreen;
