"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const react_2 = require("@storybook/react");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const CallingPreCallInfo_1 = require("./CallingPreCallInfo");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const getDefaultGroupConversation = () => (0, getDefaultConversation_1.getDefaultConversation)({
    name: 'Tahoe Trip',
    phoneNumber: undefined,
    profileName: undefined,
    title: 'Tahoe Trip',
    type: 'group',
});
const otherMembers = (0, lodash_1.times)(6, () => (0, getDefaultConversation_1.getDefaultConversation)());
const story = (0, react_2.storiesOf)('Components/CallingPreCallInfo', module);
story.add('Direct conversation', () => (react_1.default.createElement(CallingPreCallInfo_1.CallingPreCallInfo, { conversation: (0, getDefaultConversation_1.getDefaultConversation)(), i18n: i18n, me: (0, getDefaultConversation_1.getDefaultConversation)(), ringMode: CallingPreCallInfo_1.RingMode.WillRing })));
(0, lodash_1.times)(5, numberOfOtherPeople => {
    [true, false].forEach(willRing => {
        story.add(`Group conversation, group has ${numberOfOtherPeople} other member${numberOfOtherPeople === 1 ? '' : 's'}, will ${willRing ? 'ring' : 'notify'}`, () => (react_1.default.createElement(CallingPreCallInfo_1.CallingPreCallInfo, { conversation: getDefaultGroupConversation(), groupMembers: otherMembers.slice(0, numberOfOtherPeople), i18n: i18n, me: (0, getDefaultConversation_1.getDefaultConversation)(), peekedParticipants: [], ringMode: willRing ? CallingPreCallInfo_1.RingMode.WillRing : CallingPreCallInfo_1.RingMode.WillNotRing })));
    });
});
(0, lodash_1.range)(1, 5).forEach(numberOfOtherPeople => {
    story.add(`Group conversation, ${numberOfOtherPeople} peeked participant${numberOfOtherPeople === 1 ? '' : 's'}`, () => (react_1.default.createElement(CallingPreCallInfo_1.CallingPreCallInfo, { conversation: getDefaultGroupConversation(), groupMembers: otherMembers, i18n: i18n, me: (0, getDefaultConversation_1.getDefaultConversation)(), peekedParticipants: otherMembers.slice(0, numberOfOtherPeople), ringMode: CallingPreCallInfo_1.RingMode.WillRing })));
});
story.add('Group conversation, you on an other device', () => {
    const me = (0, getDefaultConversation_1.getDefaultConversation)();
    return (react_1.default.createElement(CallingPreCallInfo_1.CallingPreCallInfo, { conversation: getDefaultGroupConversation(), groupMembers: otherMembers, i18n: i18n, me: me, peekedParticipants: [me], ringMode: CallingPreCallInfo_1.RingMode.WillRing }));
});
story.add('Group conversation, call is full', () => (react_1.default.createElement(CallingPreCallInfo_1.CallingPreCallInfo, { conversation: getDefaultGroupConversation(), groupMembers: otherMembers, i18n: i18n, isCallFull: true, me: (0, getDefaultConversation_1.getDefaultConversation)(), peekedParticipants: otherMembers, ringMode: CallingPreCallInfo_1.RingMode.WillRing })));
