"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupDialog = void 0;
const react_1 = __importDefault(require("react"));
const ModalHost_1 = require("./ModalHost");
const Button_1 = require("./Button");
const Avatar_1 = require("./Avatar");
const ContactName_1 = require("./conversation/ContactName");
// TODO: This should use <Modal>. See DESKTOP-1038.
function GroupDialog(props) {
    const { children, i18n, onClickPrimaryButton, onClose, primaryButtonText, title, } = props;
    let secondaryButton;
    if ('secondaryButtonText' in props) {
        const { onClickSecondaryButton, secondaryButtonText } = props;
        secondaryButton = (react_1.default.createElement(Button_1.Button, { onClick: onClickSecondaryButton, variant: Button_1.ButtonVariant.Secondary }, secondaryButtonText));
    }
    return (react_1.default.createElement(ModalHost_1.ModalHost, { onClose: onClose },
        react_1.default.createElement("div", { className: "module-GroupDialog" },
            react_1.default.createElement("button", { "aria-label": i18n('close'), type: "button", className: "module-GroupDialog__close-button", onClick: () => {
                    onClose();
                } }),
            react_1.default.createElement("h1", { className: "module-GroupDialog__title" }, title),
            react_1.default.createElement("div", { className: "module-GroupDialog__body" }, children),
            react_1.default.createElement("div", { className: "module-GroupDialog__button-container" },
                secondaryButton,
                react_1.default.createElement(Button_1.Button, { onClick: onClickPrimaryButton, ref: focusRef, variant: Button_1.ButtonVariant.Primary }, primaryButtonText)))));
}
exports.GroupDialog = GroupDialog;
GroupDialog.Paragraph = ({ children, }) => (react_1.default.createElement("p", { className: "module-GroupDialog__paragraph" }, children));
GroupDialog.Contacts = ({ contacts, i18n }) => (react_1.default.createElement("ul", { className: "module-GroupDialog__contacts" }, contacts.map(contact => (react_1.default.createElement("li", { key: contact.id, className: "module-GroupDialog__contacts__contact" },
    react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: contact.acceptedMessageRequest, avatarPath: contact.avatarPath, color: contact.color, conversationType: contact.type, isMe: contact.isMe, noteToSelf: contact.isMe, title: contact.title, unblurredAvatarPath: contact.unblurredAvatarPath, sharedGroupNames: contact.sharedGroupNames, size: Avatar_1.AvatarSize.TWENTY_EIGHT, i18n: i18n }),
    react_1.default.createElement(ContactName_1.ContactName, { module: "module-GroupDialog__contacts__contact__name", title: contact.title }))))));
function focusRef(el) {
    if (el) {
        el.focus();
    }
}
