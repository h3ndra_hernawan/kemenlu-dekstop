"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const Button_1 = require("./Button");
const story = (0, react_2.storiesOf)('Components/Button', module);
story.add('Kitchen sink', () => (react_1.default.createElement(react_1.default.Fragment, null, Object.values(Button_1.ButtonVariant).map(variant => (react_1.default.createElement(react_1.default.Fragment, { key: variant }, [Button_1.ButtonSize.Large, Button_1.ButtonSize.Medium, Button_1.ButtonSize.Small].map(size => (react_1.default.createElement(react_1.default.Fragment, { key: size },
    react_1.default.createElement("p", null,
        react_1.default.createElement(Button_1.Button, { onClick: (0, addon_actions_1.action)('onClick'), size: size, variant: variant }, variant)),
    react_1.default.createElement("p", null,
        react_1.default.createElement(Button_1.Button, { disabled: true, onClick: (0, addon_actions_1.action)('onClick'), size: size, variant: variant }, variant)))))))))));
story.add('aria-label', () => (react_1.default.createElement(Button_1.Button, { "aria-label": "hello", className: "module-ForwardMessageModal__header--back", onClick: (0, addon_actions_1.action)('onClick') })));
story.add('Custom styles', () => (react_1.default.createElement(Button_1.Button, { onClick: (0, addon_actions_1.action)('onClick'), style: { transform: 'rotate(5deg)' } }, "Hello world")));
