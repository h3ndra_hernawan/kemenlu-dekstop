"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const Colors_1 = require("../types/Colors");
const CallingPip_1 = require("./CallingPip");
const Calling_1 = require("../types/Calling");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const fakeGetGroupCallVideoFrameSource_1 = require("../test-both/helpers/fakeGetGroupCallVideoFrameSource");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const conversation = (0, getDefaultConversation_1.getDefaultConversation)({
    id: '3051234567',
    avatarPath: undefined,
    color: Colors_1.AvatarColors[0],
    title: 'Rick Sanchez',
    name: 'Rick Sanchez',
    phoneNumber: '3051234567',
    profileName: 'Rick Sanchez',
});
const getCommonActiveCallData = () => ({
    conversation,
    hasLocalAudio: (0, addon_knobs_1.boolean)('hasLocalAudio', true),
    hasLocalVideo: (0, addon_knobs_1.boolean)('hasLocalVideo', false),
    isInSpeakerView: (0, addon_knobs_1.boolean)('isInSpeakerView', false),
    joinedAt: Date.now(),
    outgoingRing: true,
    pip: true,
    settingsDialogOpen: false,
    showParticipantsList: false,
});
const defaultCall = Object.assign(Object.assign({}, getCommonActiveCallData()), { callMode: Calling_1.CallMode.Direct, callState: Calling_1.CallState.Accepted, peekedParticipants: [], remoteParticipants: [
        { hasRemoteVideo: true, presenting: false, title: 'Arsene' },
    ] });
const createProps = (overrideProps = {}) => ({
    activeCall: overrideProps.activeCall || defaultCall,
    getGroupCallVideoFrameSource: fakeGetGroupCallVideoFrameSource_1.fakeGetGroupCallVideoFrameSource,
    hangUp: (0, addon_actions_1.action)('hang-up'),
    hasLocalVideo: (0, addon_knobs_1.boolean)('hasLocalVideo', overrideProps.hasLocalVideo || false),
    i18n,
    setGroupCallVideoRequest: (0, addon_actions_1.action)('set-group-call-video-request'),
    setLocalPreview: (0, addon_actions_1.action)('set-local-preview'),
    setRendererCanvas: (0, addon_actions_1.action)('set-renderer-canvas'),
    togglePip: (0, addon_actions_1.action)('toggle-pip'),
    toggleSpeakerView: (0, addon_actions_1.action)('toggleSpeakerView'),
});
const story = (0, react_1.storiesOf)('Components/CallingPip', module);
story.add('Default', () => {
    const props = createProps({});
    return React.createElement(CallingPip_1.CallingPip, Object.assign({}, props));
});
story.add('Contact (with avatar and no video)', () => {
    const props = createProps({
        activeCall: Object.assign(Object.assign({}, defaultCall), { conversation: Object.assign(Object.assign({}, conversation), { avatarPath: 'https://www.fillmurray.com/64/64' }), remoteParticipants: [
                { hasRemoteVideo: false, presenting: false, title: 'Julian' },
            ] }),
    });
    return React.createElement(CallingPip_1.CallingPip, Object.assign({}, props));
});
story.add('Contact (no color)', () => {
    const props = createProps({
        activeCall: Object.assign(Object.assign({}, defaultCall), { conversation: Object.assign(Object.assign({}, conversation), { color: undefined }) }),
    });
    return React.createElement(CallingPip_1.CallingPip, Object.assign({}, props));
});
story.add('Group Call', () => {
    const props = createProps({
        activeCall: Object.assign(Object.assign({}, getCommonActiveCallData()), { callMode: Calling_1.CallMode.Group, connectionState: Calling_1.GroupCallConnectionState.Connected, conversationsWithSafetyNumberChanges: [], groupMembers: (0, lodash_1.times)(3, () => (0, getDefaultConversation_1.getDefaultConversation)()), joinState: Calling_1.GroupCallJoinState.Joined, maxDevices: 5, deviceCount: 0, peekedParticipants: [], remoteParticipants: [] }),
    });
    return React.createElement(CallingPip_1.CallingPip, Object.assign({}, props));
});
