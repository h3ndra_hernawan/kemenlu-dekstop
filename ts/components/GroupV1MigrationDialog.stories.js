"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const lodash_1 = require("lodash");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const GroupV1MigrationDialog_1 = require("./GroupV1MigrationDialog");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const contact1 = (0, getDefaultConversation_1.getDefaultConversation)({
    title: 'Alice',
    phoneNumber: '+1 (300) 555-0000',
    id: 'guid-1',
});
const contact2 = (0, getDefaultConversation_1.getDefaultConversation)({
    title: 'Bob',
    phoneNumber: '+1 (300) 555-0001',
    id: 'guid-2',
});
const contact3 = (0, getDefaultConversation_1.getDefaultConversation)({
    title: 'Chet',
    phoneNumber: '+1 (300) 555-0002',
    id: 'guid-3',
});
function booleanOr(value, defaultValue) {
    return (0, lodash_1.isBoolean)(value) ? value : defaultValue;
}
const createProps = (overrideProps = {}) => ({
    areWeInvited: (0, addon_knobs_1.boolean)('areWeInvited', booleanOr(overrideProps.areWeInvited, false)),
    droppedMembers: overrideProps.droppedMembers || [contact3, contact1],
    hasMigrated: (0, addon_knobs_1.boolean)('hasMigrated', booleanOr(overrideProps.hasMigrated, false)),
    i18n,
    invitedMembers: overrideProps.invitedMembers || [contact2],
    migrate: (0, addon_actions_1.action)('migrate'),
    onClose: (0, addon_actions_1.action)('onClose'),
});
const stories = (0, react_1.storiesOf)('Components/GroupV1MigrationDialog', module);
stories.add('Not yet migrated, basic', () => {
    return React.createElement(GroupV1MigrationDialog_1.GroupV1MigrationDialog, Object.assign({}, createProps()));
});
stories.add('Migrated, basic', () => {
    return (React.createElement(GroupV1MigrationDialog_1.GroupV1MigrationDialog, Object.assign({}, createProps({
        hasMigrated: true,
    }))));
});
stories.add('Migrated, you are invited', () => {
    return (React.createElement(GroupV1MigrationDialog_1.GroupV1MigrationDialog, Object.assign({}, createProps({
        hasMigrated: true,
        areWeInvited: true,
    }))));
});
stories.add('Not yet migrated, multiple dropped and invited members', () => {
    return (React.createElement(GroupV1MigrationDialog_1.GroupV1MigrationDialog, Object.assign({}, createProps({
        droppedMembers: [contact3, contact1, contact2],
        invitedMembers: [contact2, contact3, contact1],
    }))));
});
stories.add('Not yet migrated, no members', () => {
    return (React.createElement(GroupV1MigrationDialog_1.GroupV1MigrationDialog, Object.assign({}, createProps({
        droppedMembers: [],
        invitedMembers: [],
    }))));
});
stories.add('Not yet migrated, just dropped member', () => {
    return (React.createElement(GroupV1MigrationDialog_1.GroupV1MigrationDialog, Object.assign({}, createProps({
        invitedMembers: [],
    }))));
});
