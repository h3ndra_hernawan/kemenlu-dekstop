"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallingButton = exports.CallingButtonType = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const uuid_1 = require("uuid");
const Tooltip_1 = require("./Tooltip");
const theme_1 = require("../util/theme");
var CallingButtonType;
(function (CallingButtonType) {
    CallingButtonType["AUDIO_DISABLED"] = "AUDIO_DISABLED";
    CallingButtonType["AUDIO_OFF"] = "AUDIO_OFF";
    CallingButtonType["AUDIO_ON"] = "AUDIO_ON";
    CallingButtonType["HANG_UP"] = "HANG_UP";
    CallingButtonType["PRESENTING_DISABLED"] = "PRESENTING_DISABLED";
    CallingButtonType["PRESENTING_OFF"] = "PRESENTING_OFF";
    CallingButtonType["PRESENTING_ON"] = "PRESENTING_ON";
    CallingButtonType["RING_DISABLED"] = "RING_DISABLED";
    CallingButtonType["RING_OFF"] = "RING_OFF";
    CallingButtonType["RING_ON"] = "RING_ON";
    CallingButtonType["VIDEO_DISABLED"] = "VIDEO_DISABLED";
    CallingButtonType["VIDEO_OFF"] = "VIDEO_OFF";
    CallingButtonType["VIDEO_ON"] = "VIDEO_ON";
})(CallingButtonType = exports.CallingButtonType || (exports.CallingButtonType = {}));
const CallingButton = ({ buttonType, i18n, isVisible = true, onClick, onMouseEnter, onMouseLeave, tooltipDirection, }) => {
    const uniqueButtonId = (0, react_1.useMemo)(() => (0, uuid_1.v4)(), []);
    let classNameSuffix = '';
    let tooltipContent = '';
    let label = '';
    let disabled = false;
    if (buttonType === CallingButtonType.AUDIO_DISABLED) {
        classNameSuffix = 'audio--disabled';
        tooltipContent = i18n('calling__button--audio-disabled');
        label = i18n('calling__button--audio__label');
        disabled = true;
    }
    else if (buttonType === CallingButtonType.AUDIO_OFF) {
        classNameSuffix = 'audio--off';
        tooltipContent = i18n('calling__button--audio-on');
        label = i18n('calling__button--audio__label');
    }
    else if (buttonType === CallingButtonType.AUDIO_ON) {
        classNameSuffix = 'audio--on';
        tooltipContent = i18n('calling__button--audio-off');
        label = i18n('calling__button--audio__label');
    }
    else if (buttonType === CallingButtonType.VIDEO_DISABLED) {
        classNameSuffix = 'video--disabled';
        tooltipContent = i18n('calling__button--video-disabled');
        disabled = true;
        label = i18n('calling__button--video__label');
    }
    else if (buttonType === CallingButtonType.VIDEO_OFF) {
        classNameSuffix = 'video--off';
        tooltipContent = i18n('calling__button--video-on');
        label = i18n('calling__button--video__label');
    }
    else if (buttonType === CallingButtonType.VIDEO_ON) {
        classNameSuffix = 'video--on';
        tooltipContent = i18n('calling__button--video-off');
        label = i18n('calling__button--video__label');
    }
    else if (buttonType === CallingButtonType.HANG_UP) {
        classNameSuffix = 'hangup';
        tooltipContent = i18n('calling__hangup');
        label = i18n('calling__hangup');
    }
    else if (buttonType === CallingButtonType.RING_DISABLED) {
        classNameSuffix = 'ring--disabled';
        disabled = true;
        tooltipContent = i18n('calling__button--ring__disabled-because-group-is-too-large');
        label = i18n('calling__button--ring__label');
    }
    else if (buttonType === CallingButtonType.RING_OFF) {
        classNameSuffix = 'ring--off';
        tooltipContent = i18n('calling__button--ring__on');
        label = i18n('calling__button--ring__label');
    }
    else if (buttonType === CallingButtonType.RING_ON) {
        classNameSuffix = 'ring--on';
        tooltipContent = i18n('calling__button--ring__off');
        label = i18n('calling__button--ring__label');
    }
    else if (buttonType === CallingButtonType.PRESENTING_DISABLED) {
        classNameSuffix = 'presenting--disabled';
        tooltipContent = i18n('calling__button--presenting-disabled');
        disabled = true;
        label = i18n('calling__button--presenting__label');
    }
    else if (buttonType === CallingButtonType.PRESENTING_ON) {
        classNameSuffix = 'presenting--on';
        tooltipContent = i18n('calling__button--presenting-off');
        label = i18n('calling__button--presenting__label');
    }
    else if (buttonType === CallingButtonType.PRESENTING_OFF) {
        classNameSuffix = 'presenting--off';
        tooltipContent = i18n('calling__button--presenting-on');
        label = i18n('calling__button--presenting__label');
    }
    const className = (0, classnames_1.default)('module-calling-button__icon', `module-calling-button__icon--${classNameSuffix}`);
    return (react_1.default.createElement(Tooltip_1.Tooltip, { content: tooltipContent, direction: tooltipDirection, theme: theme_1.Theme.Dark },
        react_1.default.createElement("div", { className: (0, classnames_1.default)('module-calling-button__container', !isVisible && 'module-calling-button__container--hidden') },
            react_1.default.createElement("button", { "aria-label": tooltipContent, className: className, disabled: disabled, id: uniqueButtonId, onClick: onClick, onMouseEnter: onMouseEnter, onMouseLeave: onMouseLeave, type: "button" },
                react_1.default.createElement("div", null)),
            react_1.default.createElement("label", { className: "module-calling-button__label", htmlFor: uniqueButtonId }, label))));
};
exports.CallingButton = CallingButton;
