"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompositionArea = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const classnames_1 = __importDefault(require("classnames"));
const Spinner_1 = require("./Spinner");
const EmojiButton_1 = require("./emoji/EmojiButton");
const StickerButton_1 = require("./stickers/StickerButton");
const CompositionInput_1 = require("./CompositionInput");
const MessageRequestActions_1 = require("./conversation/MessageRequestActions");
const GroupV1DisabledActions_1 = require("./conversation/GroupV1DisabledActions");
const GroupV2PendingApprovalActions_1 = require("./conversation/GroupV2PendingApprovalActions");
const AnnouncementsOnlyGroupBanner_1 = require("./AnnouncementsOnlyGroupBanner");
const AttachmentList_1 = require("./conversation/AttachmentList");
const Attachment_1 = require("../types/Attachment");
const AudioCapture_1 = require("./conversation/AudioCapture");
const CompositionUpload_1 = require("./CompositionUpload");
const MandatoryProfileSharingActions_1 = require("./conversation/MandatoryProfileSharingActions");
const MediaQualitySelector_1 = require("./MediaQualitySelector");
const Quote_1 = require("./conversation/Quote");
const StagedLinkPreview_1 = require("./conversation/StagedLinkPreview");
const lib_1 = require("./stickers/lib");
const useKeyboardShortcuts_1 = require("../hooks/useKeyboardShortcuts");
const CompositionArea = ({ 
// Base props
addAttachment, addPendingAttachment, conversationId, i18n, onSendMessage, processAttachments, removeAttachment, theme, 
// AttachmentList
draftAttachments, onClearAttachments, 
// AudioCapture
cancelRecording, completeRecording, errorDialogAudioRecorderType, errorRecording, recordingState, startRecording, 
// StagedLinkPreview
linkPreviewLoading, linkPreviewResult, onCloseLinkPreview, 
// Quote
quotedMessageProps, onClickQuotedMessage, setQuotedMessage, 
// MediaQualitySelector
onSelectMediaQuality, shouldSendHighQualityAttachments, 
// CompositionInput
compositionApi, onEditorStateChange, onTextTooLong, draftText, draftBodyRanges, clearQuotedMessage, getQuotedMessage, scrollToBottom, sortedGroupMembers, 
// EmojiButton
onPickEmoji, onSetSkinTone, recentEmojis, skinTone, 
// StickerButton
knownPacks, receivedPacks, installedPack, installedPacks, blessedPacks, recentStickers, clearInstalledStickerPack, onClickAddPack, onPickSticker, clearShowIntroduction, showPickerHint, clearShowPickerHint, 
// Message Requests
acceptedMessageRequest, areWePending, areWePendingApproval, conversationType, groupVersion, isBlocked, isMissingMandatoryProfileSharing, left, messageRequestsEnabled, onAccept, onBlock, onBlockAndReportSpam, onDelete, onUnblock, title, 
// GroupV1 Disabled Actions
isGroupV1AndDisabled, onStartGroupMigration, 
// GroupV2
announcementsOnly, areWeAdmin, groupAdmins, onCancelJoinRequest, openConversation, 
// SMS-only contacts
isSMSOnly, isFetchingUUID, }) => {
    const [disabled, setDisabled] = (0, react_1.useState)(false);
    const [dirty, setDirty] = (0, react_1.useState)(false);
    const [large, setLarge] = (0, react_1.useState)(false);
    const inputApiRef = (0, react_1.useRef)();
    const fileInputRef = (0, react_1.useRef)(null);
    const handleForceSend = (0, react_1.useCallback)(() => {
        setLarge(false);
        if (inputApiRef.current) {
            inputApiRef.current.submit();
        }
    }, [inputApiRef, setLarge]);
    const handleSubmit = (0, react_1.useCallback)((message, mentions, timestamp) => {
        setLarge(false);
        onSendMessage({
            draftAttachments,
            mentions,
            message,
            timestamp,
        });
    }, [draftAttachments, onSendMessage, setLarge]);
    const launchAttachmentPicker = (0, react_1.useCallback)(() => {
        const fileInput = fileInputRef.current;
        if (fileInput) {
            // Setting the value to empty so that onChange always fires in case
            // you add multiple photos.
            fileInput.value = '';
            fileInput.click();
        }
    }, []);
    const attachFileShortcut = (0, useKeyboardShortcuts_1.useAttachFileShortcut)(launchAttachmentPicker);
    (0, useKeyboardShortcuts_1.useKeyboardShortcuts)(attachFileShortcut);
    const focusInput = (0, react_1.useCallback)(() => {
        if (inputApiRef.current) {
            inputApiRef.current.focus();
        }
    }, [inputApiRef]);
    const withStickers = (0, lib_1.countStickers)({
        knownPacks,
        blessedPacks,
        installedPacks,
        receivedPacks,
    }) > 0;
    if (compositionApi) {
        // Using a React.MutableRefObject, so we need to reassign this prop.
        // eslint-disable-next-line no-param-reassign
        compositionApi.current = {
            isDirty: () => dirty,
            focusInput,
            setDisabled,
            reset: () => {
                if (inputApiRef.current) {
                    inputApiRef.current.reset();
                }
            },
            resetEmojiResults: () => {
                if (inputApiRef.current) {
                    inputApiRef.current.resetEmojiResults();
                }
            },
        };
    }
    const insertEmoji = (0, react_1.useCallback)((e) => {
        if (inputApiRef.current) {
            inputApiRef.current.insertEmoji(e);
            onPickEmoji(e);
        }
    }, [inputApiRef, onPickEmoji]);
    const handleToggleLarge = (0, react_1.useCallback)(() => {
        setLarge(l => !l);
    }, [setLarge]);
    const shouldShowMicrophone = !large && !draftAttachments.length && !draftText;
    const showMediaQualitySelector = draftAttachments.some(Attachment_1.isImageAttachment);
    const leftHandSideButtonsFragment = (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: "CompositionArea__button-cell" },
            react_1.default.createElement(EmojiButton_1.EmojiButton, { i18n: i18n, doSend: handleForceSend, onPickEmoji: insertEmoji, onClose: focusInput, recentEmojis: recentEmojis, skinTone: skinTone, onSetSkinTone: onSetSkinTone })),
        showMediaQualitySelector ? (react_1.default.createElement("div", { className: "CompositionArea__button-cell" },
            react_1.default.createElement(MediaQualitySelector_1.MediaQualitySelector, { i18n: i18n, isHighQuality: shouldSendHighQualityAttachments, onSelectQuality: onSelectMediaQuality }))) : null));
    const micButtonFragment = shouldShowMicrophone ? (react_1.default.createElement(AudioCapture_1.AudioCapture, { cancelRecording: cancelRecording, completeRecording: completeRecording, conversationId: conversationId, draftAttachments: draftAttachments, errorDialogAudioRecorderType: errorDialogAudioRecorderType, errorRecording: errorRecording, i18n: i18n, recordingState: recordingState, onSendAudioRecording: (voiceNoteAttachment) => {
            onSendMessage({ voiceNoteAttachment });
        }, startRecording: startRecording })) : null;
    const attButton = (react_1.default.createElement("div", { className: "CompositionArea__button-cell" },
        react_1.default.createElement("button", { type: "button", className: "CompositionArea__attach-file", onClick: launchAttachmentPicker, "aria-label": i18n('CompositionArea--attach-file') })));
    const sendButtonFragment = (react_1.default.createElement("div", { className: (0, classnames_1.default)('CompositionArea__button-cell', large ? 'CompositionArea__button-cell--large-right' : null) },
        react_1.default.createElement("button", { type: "button", className: "CompositionArea__send-button", onClick: handleForceSend, "aria-label": i18n('sendMessageToContact') })));
    const stickerButtonPlacement = large ? 'top-start' : 'top-end';
    const stickerButtonFragment = withStickers ? (react_1.default.createElement("div", { className: "CompositionArea__button-cell" },
        react_1.default.createElement(StickerButton_1.StickerButton, { i18n: i18n, knownPacks: knownPacks, receivedPacks: receivedPacks, installedPack: installedPack, installedPacks: installedPacks, blessedPacks: blessedPacks, recentStickers: recentStickers, clearInstalledStickerPack: clearInstalledStickerPack, onClickAddPack: onClickAddPack, onPickSticker: onPickSticker, clearShowIntroduction: clearShowIntroduction, showPickerHint: showPickerHint, clearShowPickerHint: clearShowPickerHint, position: stickerButtonPlacement }))) : null;
    // Listen for cmd/ctrl-shift-x to toggle large composition mode
    (0, react_1.useEffect)(() => {
        const handler = (e) => {
            const { key, shiftKey, ctrlKey, metaKey } = e;
            // When using the ctrl key, `key` is `'X'`. When using the cmd key, `key` is `'x'`
            const xKey = key === 'x' || key === 'X';
            const commandKey = (0, lodash_1.get)(window, 'platform') === 'darwin' && metaKey;
            const controlKey = (0, lodash_1.get)(window, 'platform') !== 'darwin' && ctrlKey;
            const commandOrCtrl = commandKey || controlKey;
            // cmd/ctrl-shift-x
            if (xKey && shiftKey && commandOrCtrl) {
                e.preventDefault();
                setLarge(x => !x);
            }
        };
        document.addEventListener('keydown', handler);
        return () => {
            document.removeEventListener('keydown', handler);
        };
    }, [setLarge]);
    if (isBlocked ||
        areWePending ||
        (messageRequestsEnabled && !acceptedMessageRequest)) {
        return (react_1.default.createElement(MessageRequestActions_1.MessageRequestActions, { i18n: i18n, conversationType: conversationType, isBlocked: isBlocked, onBlock: onBlock, onBlockAndReportSpam: onBlockAndReportSpam, onUnblock: onUnblock, onDelete: onDelete, onAccept: onAccept, title: title }));
    }
    if (conversationType === 'direct' && isSMSOnly) {
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)([
                'CompositionArea',
                'CompositionArea--sms-only',
                isFetchingUUID ? 'CompositionArea--pending' : null,
            ]) }, isFetchingUUID ? (react_1.default.createElement(Spinner_1.Spinner, { ariaLabel: i18n('CompositionArea--sms-only__spinner-label'), role: "presentation", moduleClassName: "module-image-spinner", svgSize: "small" })) : (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("h2", { className: "CompositionArea--sms-only__title" }, i18n('CompositionArea--sms-only__title')),
            react_1.default.createElement("p", { className: "CompositionArea--sms-only__body" }, i18n('CompositionArea--sms-only__body'))))));
    }
    // If no message request, but we haven't shared profile yet, we show profile-sharing UI
    if (!left &&
        (conversationType === 'direct' ||
            (conversationType === 'group' && groupVersion === 1)) &&
        isMissingMandatoryProfileSharing) {
        return (react_1.default.createElement(MandatoryProfileSharingActions_1.MandatoryProfileSharingActions, { i18n: i18n, conversationType: conversationType, onBlock: onBlock, onBlockAndReportSpam: onBlockAndReportSpam, onDelete: onDelete, onAccept: onAccept, title: title }));
    }
    // If this is a V1 group, now disabled entirely, we show UI to help them upgrade
    if (!left && isGroupV1AndDisabled) {
        return (react_1.default.createElement(GroupV1DisabledActions_1.GroupV1DisabledActions, { i18n: i18n, onStartGroupMigration: onStartGroupMigration }));
    }
    if (areWePendingApproval) {
        return (react_1.default.createElement(GroupV2PendingApprovalActions_1.GroupV2PendingApprovalActions, { i18n: i18n, onCancelJoinRequest: onCancelJoinRequest }));
    }
    if (announcementsOnly && !areWeAdmin) {
        return (react_1.default.createElement(AnnouncementsOnlyGroupBanner_1.AnnouncementsOnlyGroupBanner, { groupAdmins: groupAdmins, i18n: i18n, openConversation: openConversation, theme: theme }));
    }
    return (react_1.default.createElement("div", { className: "CompositionArea" },
        react_1.default.createElement("div", { className: "CompositionArea__toggle-large" },
            react_1.default.createElement("button", { type: "button", className: (0, classnames_1.default)('CompositionArea__toggle-large__button', large ? 'CompositionArea__toggle-large__button--large-active' : null), 
                // This prevents the user from tabbing here
                tabIndex: -1, onClick: handleToggleLarge, "aria-label": i18n('CompositionArea--expand') })),
        react_1.default.createElement("div", { className: (0, classnames_1.default)('CompositionArea__row', 'CompositionArea__row--column') },
            quotedMessageProps && (react_1.default.createElement("div", { className: "quote-wrapper" },
                react_1.default.createElement(Quote_1.Quote, Object.assign({}, quotedMessageProps, { i18n: i18n, onClick: onClickQuotedMessage, onClose: () => {
                        // This one is for redux...
                        setQuotedMessage(undefined);
                        // and this is for conversation_view.
                        clearQuotedMessage();
                    } })))),
            linkPreviewLoading && (react_1.default.createElement("div", { className: "preview-wrapper" },
                react_1.default.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, (linkPreviewResult || {}), { i18n: i18n, onClose: onCloseLinkPreview })))),
            draftAttachments.length ? (react_1.default.createElement("div", { className: "CompositionArea__attachment-list" },
                react_1.default.createElement(AttachmentList_1.AttachmentList, { attachments: draftAttachments, i18n: i18n, onAddAttachment: launchAttachmentPicker, onClose: onClearAttachments, onCloseAttachment: attachment => {
                        if (attachment.path) {
                            removeAttachment(conversationId, attachment.path);
                        }
                    } }))) : null),
        react_1.default.createElement("div", { className: (0, classnames_1.default)('CompositionArea__row', large ? 'CompositionArea__row--padded' : null) },
            !large ? leftHandSideButtonsFragment : null,
            react_1.default.createElement("div", { className: "CompositionArea__input" },
                react_1.default.createElement(CompositionInput_1.CompositionInput, { i18n: i18n, conversationId: conversationId, clearQuotedMessage: clearQuotedMessage, disabled: disabled, draftBodyRanges: draftBodyRanges, draftText: draftText, getQuotedMessage: getQuotedMessage, inputApi: inputApiRef, large: large, onDirtyChange: setDirty, onEditorStateChange: onEditorStateChange, onPickEmoji: onPickEmoji, onSubmit: handleSubmit, onTextTooLong: onTextTooLong, scrollToBottom: scrollToBottom, skinTone: skinTone, sortedGroupMembers: sortedGroupMembers })),
            !large ? (react_1.default.createElement(react_1.default.Fragment, null,
                stickerButtonFragment,
                !dirty ? micButtonFragment : null,
                attButton)) : null),
        large ? (react_1.default.createElement("div", { className: (0, classnames_1.default)('CompositionArea__row', 'CompositionArea__row--control-row') },
            leftHandSideButtonsFragment,
            stickerButtonFragment,
            attButton,
            !dirty ? micButtonFragment : null,
            dirty || !shouldShowMicrophone ? sendButtonFragment : null)) : null,
        react_1.default.createElement(CompositionUpload_1.CompositionUpload, { addAttachment: addAttachment, addPendingAttachment: addPendingAttachment, conversationId: conversationId, draftAttachments: draftAttachments, i18n: i18n, processAttachments: processAttachments, removeAttachment: removeAttachment, ref: fileInputRef })));
};
exports.CompositionArea = CompositionArea;
