"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const Button_1 = require("./Button");
const Modal_1 = require("./Modal");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/Modal', module);
const onClose = (0, addon_actions_1.action)('onClose');
const LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.';
story.add('Bare bones, short', () => react_1.default.createElement(Modal_1.Modal, { i18n: i18n }, "Hello world!"));
story.add('Bare bones, long', () => (react_1.default.createElement(Modal_1.Modal, { i18n: i18n },
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM))));
story.add('Bare bones, long, with button', () => (react_1.default.createElement(Modal_1.Modal, { i18n: i18n },
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay")))));
story.add('Title, X button, body, and button footer', () => (react_1.default.createElement(Modal_1.Modal, { i18n: i18n, title: "Hello world", onClose: onClose, hasXButton: true },
    LOREM_IPSUM,
    react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay")))));
story.add('Lots of buttons in the footer', () => (react_1.default.createElement(Modal_1.Modal, { i18n: i18n, onClose: onClose },
    "Hello world!",
    react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "This is a button with a fairly large amount of text"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "This is a button with a fairly large amount of text"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay")))));
story.add('Long body with title', () => (react_1.default.createElement(Modal_1.Modal, { i18n: i18n, title: "Hello world", onClose: onClose },
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM))));
story.add('Long body with title and button', () => (react_1.default.createElement(Modal_1.Modal, { i18n: i18n, title: "Hello world", onClose: onClose },
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay")))));
story.add('Long body with long title and X button', () => (react_1.default.createElement(Modal_1.Modal, { i18n: i18n, title: LOREM_IPSUM.slice(0, 104), hasXButton: true, onClose: onClose },
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM))));
story.add('With sticky buttons long body', () => (react_1.default.createElement(Modal_1.Modal, { hasStickyButtons: true, hasXButton: true, i18n: i18n, onClose: onClose },
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay")))));
story.add('With sticky buttons short body', () => (react_1.default.createElement(Modal_1.Modal, { hasStickyButtons: true, hasXButton: true, i18n: i18n, onClose: onClose },
    react_1.default.createElement("p", null, LOREM_IPSUM.slice(0, 140)),
    react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay")))));
story.add('Sticky footer, Lots of buttons', () => (react_1.default.createElement(Modal_1.Modal, { hasStickyButtons: true, i18n: i18n, onClose: onClose, title: "OK" },
    react_1.default.createElement("p", null, LOREM_IPSUM),
    react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "This is a button with a fairly large amount of text"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "This is a button with a fairly large amount of text"),
        react_1.default.createElement(Button_1.Button, { onClick: lodash_1.noop }, "Okay")))));
