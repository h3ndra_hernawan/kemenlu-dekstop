"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClassyProvider = exports.PopperRootContext = void 0;
const React = __importStar(require("react"));
const makeApi = (classes) => ({
    createRoot: () => {
        const div = document.createElement('div');
        if (classes) {
            classes.forEach(theme => {
                div.classList.add(theme);
            });
        }
        document.body.appendChild(div);
        return div;
    },
    removeRoot: (root) => {
        document.body.removeChild(root);
    },
});
exports.PopperRootContext = React.createContext(makeApi());
const ClassyProvider = ({ classes, children, }) => {
    const api = React.useMemo(() => makeApi(classes), [classes]);
    return (React.createElement(exports.PopperRootContext.Provider, { value: api }, children));
};
exports.ClassyProvider = ClassyProvider;
