"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const Avatar_1 = require("./Avatar");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const Colors_1 = require("../types/Colors");
const StorybookThemeContext_1 = require("../../.storybook/StorybookThemeContext");
const getFakeBadge_1 = require("../test-both/helpers/getFakeBadge");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Avatar', module);
const colorMap = Colors_1.AvatarColors.reduce((m, color) => (Object.assign(Object.assign({}, m), { [color]: color })), {});
const conversationTypeMap = {
    direct: 'direct',
    group: 'group',
};
const createProps = (overrideProps = {}) => ({
    acceptedMessageRequest: (0, lodash_1.isBoolean)(overrideProps.acceptedMessageRequest)
        ? overrideProps.acceptedMessageRequest
        : true,
    avatarPath: (0, addon_knobs_1.text)('avatarPath', overrideProps.avatarPath || ''),
    badge: overrideProps.badge,
    blur: overrideProps.blur,
    color: (0, addon_knobs_1.select)('color', colorMap, overrideProps.color || Colors_1.AvatarColors[0]),
    conversationType: (0, addon_knobs_1.select)('conversationType', conversationTypeMap, overrideProps.conversationType || 'direct'),
    i18n,
    isMe: false,
    loading: (0, addon_knobs_1.boolean)('loading', overrideProps.loading || false),
    name: (0, addon_knobs_1.text)('name', overrideProps.name || ''),
    noteToSelf: (0, addon_knobs_1.boolean)('noteToSelf', overrideProps.noteToSelf || false),
    onClick: (0, addon_actions_1.action)('onClick'),
    phoneNumber: (0, addon_knobs_1.text)('phoneNumber', overrideProps.phoneNumber || ''),
    searchResult: (0, addon_knobs_1.boolean)('searchResult', typeof overrideProps.searchResult === 'boolean'
        ? overrideProps.searchResult
        : false),
    sharedGroupNames: [],
    size: 80,
    title: overrideProps.title || '',
    theme: overrideProps.theme,
});
const sizes = [112, 96, 80, 52, 32, 28];
story.add('Avatar', () => {
    const props = createProps({
        avatarPath: '/fixtures/giphy-GVNvOUpeYmI7e.gif',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('With badge', () => {
    const Wrapper = () => {
        const theme = React.useContext(StorybookThemeContext_1.StorybookThemeContext);
        const props = createProps({
            avatarPath: '/fixtures/kitten-3-64-64.jpg',
            badge: (0, getFakeBadge_1.getFakeBadge)(),
            theme,
        });
        return (React.createElement(React.Fragment, null, sizes.map(size => (React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size }))))));
    };
    return React.createElement(Wrapper, null);
});
story.add('Wide image', () => {
    const props = createProps({
        avatarPath: '/fixtures/wide.jpg',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('One-word Name', () => {
    const props = createProps({
        title: 'John',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Two-word Name', () => {
    const props = createProps({
        title: 'John Smith',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Wide initials', () => {
    const props = createProps({
        title: 'Walter White',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Three-word name', () => {
    const props = createProps({
        title: 'Walter H. White',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Note to Self', () => {
    const props = createProps({
        noteToSelf: true,
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Contact Icon', () => {
    const props = createProps();
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Group Icon', () => {
    const props = createProps({
        conversationType: 'group',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Search Icon', () => {
    const props = createProps({
        searchResult: true,
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Colors', () => {
    const props = createProps();
    return Colors_1.AvatarColors.map(color => (React.createElement(Avatar_1.Avatar, Object.assign({ key: color }, props, { color: color }))));
});
story.add('Broken Color', () => {
    const props = createProps({
        color: 'nope',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Broken Avatar', () => {
    const props = createProps({
        avatarPath: 'badimage.png',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Broken Avatar for Group', () => {
    const props = createProps({
        avatarPath: 'badimage.png',
        conversationType: 'group',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Loading', () => {
    const props = createProps({
        loading: true,
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Blurred based on props', () => {
    const props = createProps({
        acceptedMessageRequest: false,
        avatarPath: '/fixtures/kitten-3-64-64.jpg',
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Force-blurred', () => {
    const props = createProps({
        avatarPath: '/fixtures/kitten-3-64-64.jpg',
        blur: Avatar_1.AvatarBlur.BlurPicture,
    });
    return sizes.map(size => React.createElement(Avatar_1.Avatar, Object.assign({ key: size }, props, { size: size })));
});
story.add('Blurred with "click to view"', () => {
    const props = createProps({
        avatarPath: '/fixtures/kitten-3-64-64.jpg',
        blur: Avatar_1.AvatarBlur.BlurPictureWithClickToView,
    });
    return React.createElement(Avatar_1.Avatar, Object.assign({}, props, { size: 112 }));
});
