"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const CallingButton_1 = require("./CallingButton");
const Tooltip_1 = require("./Tooltip");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    buttonType: overrideProps.buttonType ||
        (0, addon_knobs_1.select)('buttonType', CallingButton_1.CallingButtonType, CallingButton_1.CallingButtonType.HANG_UP),
    i18n,
    onClick: (0, addon_actions_1.action)('on-click'),
    onMouseEnter: (0, addon_actions_1.action)('on-mouse-enter'),
    onMouseLeave: (0, addon_actions_1.action)('on-mouse-leave'),
    tooltipDirection: (0, addon_knobs_1.select)('tooltipDirection', Tooltip_1.TooltipPlacement, overrideProps.tooltipDirection || Tooltip_1.TooltipPlacement.Bottom),
});
const story = (0, react_1.storiesOf)('Components/CallingButton', module);
story.add('Kitchen Sink', () => {
    return (React.createElement(React.Fragment, null, Object.keys(CallingButton_1.CallingButtonType).map(buttonType => (React.createElement(CallingButton_1.CallingButton, Object.assign({ key: buttonType }, createProps({ buttonType: buttonType })))))));
});
story.add('Audio On', () => {
    const props = createProps({
        buttonType: CallingButton_1.CallingButtonType.AUDIO_ON,
    });
    return React.createElement(CallingButton_1.CallingButton, Object.assign({}, props));
});
story.add('Audio Off', () => {
    const props = createProps({
        buttonType: CallingButton_1.CallingButtonType.AUDIO_OFF,
    });
    return React.createElement(CallingButton_1.CallingButton, Object.assign({}, props));
});
story.add('Audio Disabled', () => {
    const props = createProps({
        buttonType: CallingButton_1.CallingButtonType.AUDIO_DISABLED,
    });
    return React.createElement(CallingButton_1.CallingButton, Object.assign({}, props));
});
story.add('Video On', () => {
    const props = createProps({
        buttonType: CallingButton_1.CallingButtonType.VIDEO_ON,
    });
    return React.createElement(CallingButton_1.CallingButton, Object.assign({}, props));
});
story.add('Video Off', () => {
    const props = createProps({
        buttonType: CallingButton_1.CallingButtonType.VIDEO_OFF,
    });
    return React.createElement(CallingButton_1.CallingButton, Object.assign({}, props));
});
story.add('Video Disabled', () => {
    const props = createProps({
        buttonType: CallingButton_1.CallingButtonType.VIDEO_DISABLED,
    });
    return React.createElement(CallingButton_1.CallingButton, Object.assign({}, props));
});
story.add('Tooltip right', () => {
    const props = createProps({
        tooltipDirection: Tooltip_1.TooltipPlacement.Right,
    });
    return React.createElement(CallingButton_1.CallingButton, Object.assign({}, props));
});
story.add('Presenting On', () => {
    const props = createProps({
        buttonType: CallingButton_1.CallingButtonType.PRESENTING_ON,
    });
    return React.createElement(CallingButton_1.CallingButton, Object.assign({}, props));
});
story.add('Presenting Off', () => {
    const props = createProps({
        buttonType: CallingButton_1.CallingButtonType.PRESENTING_OFF,
    });
    return React.createElement(CallingButton_1.CallingButton, Object.assign({}, props));
});
