"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProgressModal = void 0;
const React = __importStar(require("react"));
const react_dom_1 = require("react-dom");
const ProgressDialog_1 = require("./ProgressDialog");
exports.ProgressModal = React.memo(({ i18n }) => {
    const [root, setRoot] = React.useState(null);
    // Note: We explicitly don't register for user interaction here, since this dialog
    //   cannot be dismissed.
    React.useEffect(() => {
        const div = document.createElement('div');
        document.body.appendChild(div);
        setRoot(div);
        return () => {
            document.body.removeChild(div);
            setRoot(null);
        };
    }, []);
    return root
        ? (0, react_dom_1.createPortal)(React.createElement("div", { role: "presentation", className: "module-progress-dialog__overlay" },
            React.createElement(ProgressDialog_1.ProgressDialog, { i18n: i18n })), root)
        : null;
});
