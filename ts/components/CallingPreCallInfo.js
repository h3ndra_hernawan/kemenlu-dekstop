"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallingPreCallInfo = exports.RingMode = void 0;
const react_1 = __importDefault(require("react"));
const Avatar_1 = require("./Avatar");
const Emojify_1 = require("./conversation/Emojify");
const callingGetParticipantName_1 = require("../util/callingGetParticipantName");
const missingCaseError_1 = require("../util/missingCaseError");
var RingMode;
(function (RingMode) {
    RingMode[RingMode["WillNotRing"] = 0] = "WillNotRing";
    RingMode[RingMode["WillRing"] = 1] = "WillRing";
    RingMode[RingMode["IsRinging"] = 2] = "IsRinging";
})(RingMode = exports.RingMode || (exports.RingMode = {}));
const CallingPreCallInfo = ({ conversation, groupMembers = [], i18n, isCallFull = false, me, peekedParticipants = [], ringMode, }) => {
    let subtitle;
    if (ringMode === RingMode.IsRinging) {
        subtitle = i18n('outgoingCallRinging');
    }
    else if (isCallFull) {
        subtitle = i18n('calling__call-is-full');
    }
    else if (peekedParticipants.length) {
        // It should be rare to see yourself in this list, but it's possible if (1) you rejoin
        //   quickly, causing the server to return stale state (2) you have joined on another
        //   device.
        let hasYou = false;
        const participantNames = peekedParticipants.map(participant => {
            if (participant.uuid === me.uuid) {
                hasYou = true;
                return i18n('you');
            }
            return (0, callingGetParticipantName_1.getParticipantName)(participant);
        });
        switch (participantNames.length) {
            case 1:
                subtitle = hasYou
                    ? i18n('calling__pre-call-info--another-device-in-call')
                    : i18n('calling__pre-call-info--1-person-in-call', participantNames);
                break;
            case 2:
                subtitle = i18n('calling__pre-call-info--2-people-in-call', {
                    first: participantNames[0],
                    second: participantNames[1],
                });
                break;
            case 3:
                subtitle = i18n('calling__pre-call-info--3-people-in-call', {
                    first: participantNames[0],
                    second: participantNames[1],
                    third: participantNames[2],
                });
                break;
            default:
                subtitle = i18n('calling__pre-call-info--many-people-in-call', {
                    first: participantNames[0],
                    second: participantNames[1],
                    others: String(participantNames.length - 2),
                });
                break;
        }
    }
    else {
        let memberNames;
        switch (conversation.type) {
            case 'direct':
                memberNames = [(0, callingGetParticipantName_1.getParticipantName)(conversation)];
                break;
            case 'group':
                memberNames = groupMembers
                    .filter(member => member.id !== me.id)
                    .map(callingGetParticipantName_1.getParticipantName);
                break;
            default:
                throw (0, missingCaseError_1.missingCaseError)(conversation.type);
        }
        const ring = ringMode === RingMode.WillRing;
        switch (memberNames.length) {
            case 0:
                subtitle = i18n('calling__pre-call-info--empty-group');
                break;
            case 1: {
                const i18nValues = [memberNames[0]];
                subtitle = ring
                    ? i18n('calling__pre-call-info--will-ring-1', i18nValues)
                    : i18n('calling__pre-call-info--will-notify-1', i18nValues);
                break;
            }
            case 2: {
                const i18nValues = {
                    first: memberNames[0],
                    second: memberNames[1],
                };
                subtitle = ring
                    ? i18n('calling__pre-call-info--will-ring-2', i18nValues)
                    : i18n('calling__pre-call-info--will-notify-2', i18nValues);
                break;
            }
            case 3: {
                const i18nValues = {
                    first: memberNames[0],
                    second: memberNames[1],
                    third: memberNames[2],
                };
                subtitle = ring
                    ? i18n('calling__pre-call-info--will-ring-3', i18nValues)
                    : i18n('calling__pre-call-info--will-notify-3', i18nValues);
                break;
            }
            default: {
                const i18nValues = {
                    first: memberNames[0],
                    second: memberNames[1],
                    others: String(memberNames.length - 2),
                };
                subtitle = ring
                    ? i18n('calling__pre-call-info--will-ring-many', i18nValues)
                    : i18n('calling__pre-call-info--will-notify-many', i18nValues);
                break;
            }
        }
    }
    return (react_1.default.createElement("div", { className: "module-CallingPreCallInfo" },
        react_1.default.createElement(Avatar_1.Avatar, { avatarPath: conversation.avatarPath, color: conversation.color, acceptedMessageRequest: conversation.acceptedMessageRequest, conversationType: conversation.type, isMe: conversation.isMe, name: conversation.name, noteToSelf: false, phoneNumber: conversation.phoneNumber, profileName: conversation.profileName, sharedGroupNames: conversation.sharedGroupNames, size: Avatar_1.AvatarSize.ONE_HUNDRED_TWELVE, title: conversation.title, unblurredAvatarPath: conversation.unblurredAvatarPath, i18n: i18n }),
        react_1.default.createElement("div", { className: "module-CallingPreCallInfo__title" },
            react_1.default.createElement(Emojify_1.Emojify, { text: conversation.title })),
        react_1.default.createElement("div", { className: "module-CallingPreCallInfo__subtitle" }, subtitle)));
};
exports.CallingPreCallInfo = CallingPreCallInfo;
