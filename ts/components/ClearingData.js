"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClearingData = void 0;
const react_1 = __importStar(require("react"));
function ClearingData({ deleteAllData, i18n }) {
    (0, react_1.useEffect)(() => {
        deleteAllData();
    }, [deleteAllData]);
    return (react_1.default.createElement("div", { className: "full-screen-flow overlay" },
        react_1.default.createElement("div", { className: "step" },
            react_1.default.createElement("div", { className: "inner" },
                react_1.default.createElement("div", { className: "step-body" },
                    react_1.default.createElement("span", { className: "banner-icon delete" }),
                    react_1.default.createElement("div", { className: "header" }, i18n('deleteAllDataProgress'))),
                react_1.default.createElement("div", { className: "progress" },
                    react_1.default.createElement("div", { className: "bar-container" },
                        react_1.default.createElement("div", { className: "bar progress-bar progress-bar-striped active" })))))));
}
exports.ClearingData = ClearingData;
