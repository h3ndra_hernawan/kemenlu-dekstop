"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Checkbox = void 0;
const react_1 = __importStar(require("react"));
const uuid_1 = require("uuid");
const getClassNamesFor_1 = require("../util/getClassNamesFor");
const Checkbox = ({ checked, description, disabled, isRadio, label, moduleClassName, name, onChange, }) => {
    const getClassName = (0, getClassNamesFor_1.getClassNamesFor)('Checkbox', moduleClassName);
    const id = (0, react_1.useMemo)(() => `${name}::${(0, uuid_1.v4)()}`, [name]);
    return (react_1.default.createElement("div", { className: getClassName('') },
        react_1.default.createElement("div", { className: getClassName('__container') },
            react_1.default.createElement("div", { className: getClassName('__checkbox') },
                react_1.default.createElement("input", { checked: Boolean(checked), disabled: disabled, id: id, name: name, onChange: ev => onChange(ev.target.checked), type: isRadio ? 'radio' : 'checkbox' })),
            react_1.default.createElement("div", null,
                react_1.default.createElement("label", { htmlFor: id }, label),
                react_1.default.createElement("div", { className: getClassName('__description') }, description)))));
};
exports.Checkbox = Checkbox;
