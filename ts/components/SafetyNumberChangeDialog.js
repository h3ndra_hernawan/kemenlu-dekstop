"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SafetyNumberChangeDialog = void 0;
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const Avatar_1 = require("./Avatar");
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const InContactsIcon_1 = require("./InContactsIcon");
const Modal_1 = require("./Modal");
const isInSystemContacts_1 = require("../util/isInSystemContacts");
const SafetyNumberChangeDialog = ({ confirmText, contacts, i18n, onCancel, onConfirm, renderSafetyNumber, }) => {
    const [selectedContact, setSelectedContact] = React.useState(undefined);
    const cancelButtonRef = React.createRef();
    React.useEffect(() => {
        if (cancelButtonRef && cancelButtonRef.current) {
            cancelButtonRef.current.focus();
        }
    }, [cancelButtonRef, contacts]);
    const onClose = selectedContact
        ? () => {
            setSelectedContact(undefined);
        }
        : onCancel;
    if (selectedContact) {
        return (React.createElement(Modal_1.Modal, { i18n: i18n, onClose: onClose }, renderSafetyNumber({ contactID: selectedContact.id, onClose })));
    }
    return (React.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
            {
                action: onConfirm,
                text: confirmText || i18n('sendMessageToContact'),
                style: 'affirmative',
            },
        ], i18n: i18n, onCancel: onClose, onClose: lodash_1.noop, title: i18n('safetyNumberChanges') },
        React.createElement("div", { className: "module-SafetyNumberChangeDialog__message" }, i18n('changedVerificationWarning')),
        React.createElement("ul", { className: "module-SafetyNumberChangeDialog__contacts" }, contacts.map((contact) => {
            const shouldShowNumber = Boolean(contact.name || contact.profileName);
            return (React.createElement("li", { className: "module-SafetyNumberChangeDialog__contact", key: contact.id },
                React.createElement(Avatar_1.Avatar, { acceptedMessageRequest: contact.acceptedMessageRequest, avatarPath: contact.avatarPath, color: contact.color, conversationType: "direct", i18n: i18n, isMe: contact.isMe, name: contact.name, phoneNumber: contact.phoneNumber, profileName: contact.profileName, title: contact.title, sharedGroupNames: contact.sharedGroupNames, size: 52, unblurredAvatarPath: contact.unblurredAvatarPath }),
                React.createElement("div", { className: "module-SafetyNumberChangeDialog__contact--wrapper" },
                    React.createElement("div", { className: "module-SafetyNumberChangeDialog__contact--name" },
                        contact.title,
                        (0, isInSystemContacts_1.isInSystemContacts)(contact) ? (React.createElement("span", null,
                            ' ',
                            React.createElement(InContactsIcon_1.InContactsIcon, { i18n: i18n }))) : null),
                    shouldShowNumber ? (React.createElement("div", { className: "module-SafetyNumberChangeDialog__contact--number" }, contact.phoneNumber)) : null),
                React.createElement("button", { className: "module-SafetyNumberChangeDialog__contact--view", onClick: () => {
                        setSelectedContact(contact);
                    }, tabIndex: 0, type: "button" }, i18n('view'))));
        }))));
};
exports.SafetyNumberChangeDialog = SafetyNumberChangeDialog;
