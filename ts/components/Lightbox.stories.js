"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const Lightbox_1 = require("./Lightbox");
const setupI18n_1 = require("../util/setupI18n");
const MIME_1 = require("../types/MIME");
const fakeAttachment_1 = require("../test-both/helpers/fakeAttachment");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Lightbox', module);
function createMediaItem(overrideProps) {
    return Object.assign({ attachment: (0, fakeAttachment_1.fakeAttachment)({
            caption: overrideProps.caption || '',
            contentType: MIME_1.IMAGE_JPEG,
            fileName: overrideProps.objectURL,
            url: overrideProps.objectURL,
        }), contentType: MIME_1.IMAGE_JPEG, index: 0, message: {
            attachments: [],
            conversationId: '1234',
            id: 'image-msg',
            received_at: 0,
            received_at_ms: Date.now(),
            sent_at: Date.now(),
        }, objectURL: '' }, overrideProps);
}
const createProps = (overrideProps = {}) => ({
    close: (0, addon_actions_1.action)('close'),
    i18n,
    isViewOnce: Boolean(overrideProps.isViewOnce),
    media: overrideProps.media || [],
    onSave: (0, addon_actions_1.action)('onSave'),
    selectedIndex: (0, addon_knobs_1.number)('selectedIndex', overrideProps.selectedIndex || 0),
});
story.add('Multimedia', () => {
    const props = createProps({
        media: [
            {
                attachment: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_JPEG,
                    fileName: 'tina-rolf-269345-unsplash.jpg',
                    url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                    caption: 'Still from The Lighthouse, starring Robert Pattinson and Willem Defoe.',
                }),
                contentType: MIME_1.IMAGE_JPEG,
                index: 0,
                message: {
                    attachments: [],
                    conversationId: '1234',
                    id: 'image-msg',
                    received_at: 1,
                    received_at_ms: Date.now(),
                    sent_at: Date.now(),
                },
                objectURL: '/fixtures/tina-rolf-269345-unsplash.jpg',
            },
            {
                attachment: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.VIDEO_MP4,
                    fileName: 'pixabay-Soap-Bubble-7141.mp4',
                    url: '/fixtures/pixabay-Soap-Bubble-7141.mp4',
                }),
                contentType: MIME_1.VIDEO_MP4,
                index: 1,
                message: {
                    attachments: [],
                    conversationId: '1234',
                    id: 'video-msg',
                    received_at: 2,
                    received_at_ms: Date.now(),
                    sent_at: Date.now(),
                },
                objectURL: '/fixtures/pixabay-Soap-Bubble-7141.mp4',
            },
            createMediaItem({
                contentType: MIME_1.IMAGE_JPEG,
                index: 2,
                thumbnailObjectUrl: '/fixtures/kitten-1-64-64.jpg',
                objectURL: '/fixtures/kitten-1-64-64.jpg',
            }),
            createMediaItem({
                contentType: MIME_1.IMAGE_JPEG,
                index: 3,
                thumbnailObjectUrl: '/fixtures/kitten-2-64-64.jpg',
                objectURL: '/fixtures/kitten-2-64-64.jpg',
            }),
        ],
    });
    return React.createElement(Lightbox_1.Lightbox, Object.assign({}, props));
});
story.add('Missing Media', () => {
    const props = createProps({
        media: [
            {
                attachment: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_JPEG,
                    fileName: 'tina-rolf-269345-unsplash.jpg',
                    url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                }),
                contentType: MIME_1.IMAGE_JPEG,
                index: 0,
                message: {
                    attachments: [],
                    conversationId: '1234',
                    id: 'image-msg',
                    received_at: 3,
                    received_at_ms: Date.now(),
                    sent_at: Date.now(),
                },
                objectURL: undefined,
            },
        ],
    });
    return React.createElement(Lightbox_1.Lightbox, Object.assign({}, props));
});
story.add('Single Image', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({
    media: [
        createMediaItem({
            objectURL: '/fixtures/tina-rolf-269345-unsplash.jpg',
        }),
    ],
})))));
story.add('Image with Caption (normal image)', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({
    media: [
        createMediaItem({
            caption: 'This lighthouse is really cool because there are lots of rocks and there is a tower that has a light and the light is really bright because it shines so much. The day was super duper cloudy and stormy and you can see all the waves hitting against the rocks. Wait? What is that weird red hose line thingy running all the way to the tower? Those rocks look slippery! I bet that water is really cold. I am cold now, can I get a sweater? I wonder where this place is, probably somewhere cold like Coldsgar, Frozenville.',
            objectURL: '/fixtures/tina-rolf-269345-unsplash.jpg',
        }),
    ],
})))));
story.add('Image with Caption (all-white image)', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({
    media: [
        createMediaItem({
            caption: 'This is the user-provided caption. It should be visible on light backgrounds.',
            objectURL: '/fixtures/2000x2000-white.png',
        }),
    ],
})))));
story.add('Single Video', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({
    media: [
        createMediaItem({
            contentType: MIME_1.VIDEO_MP4,
            objectURL: '/fixtures/pixabay-Soap-Bubble-7141.mp4',
        }),
    ],
})))));
story.add('Single Video w/caption', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({
    media: [
        createMediaItem({
            caption: 'This is the user-provided caption. It can get long and wrap onto multiple lines.',
            contentType: MIME_1.VIDEO_MP4,
            objectURL: '/fixtures/pixabay-Soap-Bubble-7141.mp4',
        }),
    ],
})))));
story.add('Unsupported Image Type', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({
    media: [
        createMediaItem({
            contentType: (0, MIME_1.stringToMIMEType)('image/tiff'),
            objectURL: 'unsupported-image.tiff',
        }),
    ],
})))));
story.add('Unsupported Video Type', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({
    media: [
        createMediaItem({
            contentType: MIME_1.VIDEO_QUICKTIME,
            objectURL: 'unsupported-video.mov',
        }),
    ],
})))));
story.add('Unsupported Content', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({
    media: [
        createMediaItem({
            contentType: MIME_1.AUDIO_MP3,
            objectURL: '/fixtures/incompetech-com-Agnus-Dei-X.mp3',
        }),
    ],
})))));
story.add('Custom children', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({}), { media: [] }),
    React.createElement("div", { style: {
            color: 'white',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        } }, "I am middle child"))));
story.add('Forwarding', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({}), { onForward: (0, addon_actions_1.action)('onForward') }))));
story.add('Conversation Header', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({}), { getConversation: () => ({
        acceptedMessageRequest: true,
        avatarPath: '/fixtures/kitten-1-64-64.jpg',
        badges: [],
        id: '1234',
        isMe: false,
        name: 'Test',
        profileName: 'Test',
        sharedGroupNames: [],
        title: 'Test',
        type: 'direct',
    }), media: [
        createMediaItem({
            contentType: MIME_1.VIDEO_MP4,
            objectURL: '/fixtures/pixabay-Soap-Bubble-7141.mp4',
        }),
    ] }))));
story.add('View Once Video', () => (React.createElement(Lightbox_1.Lightbox, Object.assign({}, createProps({
    isViewOnce: true,
    media: [
        createMediaItem({
            contentType: MIME_1.VIDEO_MP4,
            objectURL: '/fixtures/pixabay-Soap-Bubble-7141.mp4',
        }),
    ],
}), { isViewOnce: true }))));
