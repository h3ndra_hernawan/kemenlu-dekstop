"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const Calling_1 = require("../types/Calling");
const Colors_1 = require("../types/Colors");
const CallScreen_1 = require("./CallScreen");
const setupI18n_1 = require("../util/setupI18n");
const missingCaseError_1 = require("../util/missingCaseError");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const fakeGetGroupCallVideoFrameSource_1 = require("../test-both/helpers/fakeGetGroupCallVideoFrameSource");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const MAX_PARTICIPANTS = 32;
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const conversation = (0, getDefaultConversation_1.getDefaultConversation)({
    id: '3051234567',
    avatarPath: undefined,
    color: Colors_1.AvatarColors[0],
    title: 'Rick Sanchez',
    name: 'Rick Sanchez',
    phoneNumber: '3051234567',
    profileName: 'Rick Sanchez',
});
const createActiveDirectCallProp = (overrideProps) => ({
    callMode: Calling_1.CallMode.Direct,
    conversation,
    callState: (0, addon_knobs_1.select)('callState', Calling_1.CallState, overrideProps.callState || Calling_1.CallState.Accepted),
    peekedParticipants: [],
    remoteParticipants: [
        {
            hasRemoteVideo: (0, addon_knobs_1.boolean)('hasRemoteVideo', Boolean(overrideProps.hasRemoteVideo)),
            presenting: false,
            title: 'test',
        },
    ],
});
const createActiveGroupCallProp = (overrideProps) => ({
    callMode: Calling_1.CallMode.Group,
    connectionState: overrideProps.connectionState || Calling_1.GroupCallConnectionState.Connected,
    conversationsWithSafetyNumberChanges: [],
    joinState: Calling_1.GroupCallJoinState.Joined,
    maxDevices: 5,
    deviceCount: (overrideProps.remoteParticipants || []).length,
    groupMembers: overrideProps.remoteParticipants || [],
    // Because remote participants are a superset, we can use them in place of peeked
    //   participants.
    peekedParticipants: overrideProps.peekedParticipants || overrideProps.remoteParticipants || [],
    remoteParticipants: overrideProps.remoteParticipants || [],
});
const createActiveCallProp = (overrideProps) => {
    const baseResult = {
        joinedAt: Date.now(),
        conversation,
        hasLocalAudio: (0, addon_knobs_1.boolean)('hasLocalAudio', overrideProps.hasLocalAudio || false),
        hasLocalVideo: (0, addon_knobs_1.boolean)('hasLocalVideo', overrideProps.hasLocalVideo || false),
        isInSpeakerView: (0, addon_knobs_1.boolean)('isInSpeakerView', overrideProps.isInSpeakerView || false),
        outgoingRing: true,
        pip: false,
        settingsDialogOpen: false,
        showParticipantsList: false,
    };
    switch (overrideProps.callMode) {
        case Calling_1.CallMode.Direct:
            return Object.assign(Object.assign({}, baseResult), createActiveDirectCallProp(overrideProps));
        case Calling_1.CallMode.Group:
            return Object.assign(Object.assign({}, baseResult), createActiveGroupCallProp(overrideProps));
        default:
            throw (0, missingCaseError_1.missingCaseError)(overrideProps);
    }
};
const createProps = (overrideProps = {
    callMode: Calling_1.CallMode.Direct,
}) => ({
    activeCall: createActiveCallProp(overrideProps),
    getGroupCallVideoFrameSource: fakeGetGroupCallVideoFrameSource_1.fakeGetGroupCallVideoFrameSource,
    getPresentingSources: (0, addon_actions_1.action)('get-presenting-sources'),
    hangUp: (0, addon_actions_1.action)('hang-up'),
    i18n,
    me: {
        color: Colors_1.AvatarColors[1],
        id: '6146087e-f7ef-457e-9a8d-47df1fdd6b25',
        name: 'Morty Smith',
        profileName: 'Morty Smith',
        title: 'Morty Smith',
        uuid: '3c134598-eecb-42ab-9ad3-2b0873f771b2',
    },
    openSystemPreferencesAction: (0, addon_actions_1.action)('open-system-preferences-action'),
    setGroupCallVideoRequest: (0, addon_actions_1.action)('set-group-call-video-request'),
    setLocalAudio: (0, addon_actions_1.action)('set-local-audio'),
    setLocalPreview: (0, addon_actions_1.action)('set-local-preview'),
    setLocalVideo: (0, addon_actions_1.action)('set-local-video'),
    setPresenting: (0, addon_actions_1.action)('toggle-presenting'),
    setRendererCanvas: (0, addon_actions_1.action)('set-renderer-canvas'),
    stickyControls: (0, addon_knobs_1.boolean)('stickyControls', false),
    toggleParticipants: (0, addon_actions_1.action)('toggle-participants'),
    togglePip: (0, addon_actions_1.action)('toggle-pip'),
    toggleScreenRecordingPermissionsDialog: (0, addon_actions_1.action)('toggle-screen-recording-permissions-dialog'),
    toggleSettings: (0, addon_actions_1.action)('toggle-settings'),
    toggleSpeakerView: (0, addon_actions_1.action)('toggle-speaker-view'),
});
const story = (0, react_1.storiesOf)('Components/CallScreen', module);
story.add('Default', () => {
    return React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps()));
});
story.add('Pre-Ring', () => {
    return (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
        callMode: Calling_1.CallMode.Direct,
        callState: Calling_1.CallState.Prering,
    }))));
});
story.add('Ringing', () => {
    return (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
        callMode: Calling_1.CallMode.Direct,
        callState: Calling_1.CallState.Ringing,
    }))));
});
story.add('Reconnecting', () => {
    return (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
        callMode: Calling_1.CallMode.Direct,
        callState: Calling_1.CallState.Reconnecting,
    }))));
});
story.add('Ended', () => {
    return (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
        callMode: Calling_1.CallMode.Direct,
        callState: Calling_1.CallState.Ended,
    }))));
});
story.add('hasLocalAudio', () => {
    return (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
        callMode: Calling_1.CallMode.Direct,
        hasLocalAudio: true,
    }))));
});
story.add('hasLocalVideo', () => {
    return (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
        callMode: Calling_1.CallMode.Direct,
        hasLocalVideo: true,
    }))));
});
story.add('hasRemoteVideo', () => {
    return (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
        callMode: Calling_1.CallMode.Direct,
        hasRemoteVideo: true,
    }))));
});
story.add('Group call - 1', () => (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
    callMode: Calling_1.CallMode.Group,
    remoteParticipants: [
        Object.assign({ demuxId: 0, hasRemoteAudio: true, hasRemoteVideo: true, presenting: false, sharingScreen: false, videoAspectRatio: 1.3 }, (0, getDefaultConversation_1.getDefaultConversation)({
            isBlocked: false,
            uuid: '72fa60e5-25fb-472d-8a56-e56867c57dda',
            title: 'Tyler',
        })),
    ],
})))));
// We generate these upfront so that the list is stable when you move the slider.
const allRemoteParticipants = (0, lodash_1.times)(MAX_PARTICIPANTS).map(index => (Object.assign({ demuxId: index, hasRemoteAudio: index % 3 !== 0, hasRemoteVideo: index % 4 !== 0, presenting: false, sharingScreen: false, videoAspectRatio: 1.3 }, (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
    isBlocked: index === 10 || index === MAX_PARTICIPANTS - 1,
    title: `Participant ${index + 1}`,
}))));
story.add('Group call - Many', () => {
    return (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
        callMode: Calling_1.CallMode.Group,
        remoteParticipants: allRemoteParticipants.slice(0, (0, addon_knobs_1.number)('Participant count', 3, {
            range: true,
            min: 0,
            max: MAX_PARTICIPANTS,
            step: 1,
        })),
    }))));
});
story.add('Group call - reconnecting', () => (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
    callMode: Calling_1.CallMode.Group,
    connectionState: Calling_1.GroupCallConnectionState.Reconnecting,
    remoteParticipants: [
        Object.assign({ demuxId: 0, hasRemoteAudio: true, hasRemoteVideo: true, presenting: false, sharingScreen: false, videoAspectRatio: 1.3 }, (0, getDefaultConversation_1.getDefaultConversation)({
            isBlocked: false,
            title: 'Tyler',
            uuid: '33871c64-0c22-45ce-8aa4-0ec237ac4a31',
        })),
    ],
})))));
story.add('Group call - 0', () => (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
    callMode: Calling_1.CallMode.Group,
    remoteParticipants: [],
})))));
story.add('Group call - someone is sharing screen', () => (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
    callMode: Calling_1.CallMode.Group,
    remoteParticipants: allRemoteParticipants
        .slice(0, 5)
        .map((participant, index) => (Object.assign(Object.assign({}, participant), { presenting: index === 1, sharingScreen: index === 1 }))),
})))));
story.add("Group call - someone is sharing screen and you're reconnecting", () => (React.createElement(CallScreen_1.CallScreen, Object.assign({}, createProps({
    callMode: Calling_1.CallMode.Group,
    connectionState: Calling_1.GroupCallConnectionState.Reconnecting,
    remoteParticipants: allRemoteParticipants
        .slice(0, 5)
        .map((participant, index) => (Object.assign(Object.assign({}, participant), { presenting: index === 1, sharingScreen: index === 1 }))),
})))));
