"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvatarTextEditor = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const grapheme = __importStar(require("../util/grapheme"));
const AvatarColorPicker_1 = require("./AvatarColorPicker");
const Colors_1 = require("../types/Colors");
const AvatarModalButtons_1 = require("./AvatarModalButtons");
const BetterAvatarBubble_1 = require("./BetterAvatarBubble");
const avatarDataToBytes_1 = require("../util/avatarDataToBytes");
const createAvatarData_1 = require("../util/createAvatarData");
const avatarTextSizeCalculator_1 = require("../util/avatarTextSizeCalculator");
const BUBBLE_SIZE = 120;
const MAX_LENGTH = 3;
const AvatarTextEditor = ({ avatarData, i18n, onCancel, onDone, }) => {
    const initialText = (0, react_1.useMemo)(() => (avatarData === null || avatarData === void 0 ? void 0 : avatarData.text) || '', [avatarData]);
    const initialColor = (0, react_1.useMemo)(() => (avatarData === null || avatarData === void 0 ? void 0 : avatarData.color) || Colors_1.AvatarColors[0], [avatarData]);
    const [inputText, setInputText] = (0, react_1.useState)(initialText);
    const [fontSize, setFontSize] = (0, react_1.useState)((0, avatarTextSizeCalculator_1.getFontSizes)(BUBBLE_SIZE).text);
    const [selectedColor, setSelectedColor] = (0, react_1.useState)(initialColor);
    const inputRef = (0, react_1.useRef)(null);
    const focusInput = (0, react_1.useCallback)(() => {
        const inputEl = inputRef === null || inputRef === void 0 ? void 0 : inputRef.current;
        if (inputEl) {
            inputEl.focus();
        }
    }, []);
    const handleChange = (0, react_1.useCallback)((ev) => {
        const { value } = ev.target;
        if (grapheme.count(value) <= MAX_LENGTH) {
            setInputText(ev.target.value);
        }
    }, [setInputText]);
    const handlePaste = (0, react_1.useCallback)((ev) => {
        const inputEl = ev.currentTarget;
        const selectionStart = inputEl.selectionStart || 0;
        const selectionEnd = inputEl.selectionEnd || inputEl.selectionStart || 0;
        const textBeforeSelection = inputText.slice(0, selectionStart);
        const textAfterSelection = inputText.slice(selectionEnd);
        const pastedText = ev.clipboardData.getData('Text');
        const newGraphemeCount = grapheme.count(textBeforeSelection) +
            grapheme.count(pastedText) +
            grapheme.count(textAfterSelection);
        if (newGraphemeCount > MAX_LENGTH) {
            ev.preventDefault();
        }
    }, [inputText]);
    const onDoneRef = (0, react_1.useRef)(onDone);
    // Make sure we keep onDoneRef up to date
    (0, react_1.useEffect)(() => {
        onDoneRef.current = onDone;
    }, [onDone]);
    const handleDone = (0, react_1.useCallback)(async () => {
        const newAvatarData = (0, createAvatarData_1.createAvatarData)({
            color: selectedColor,
            text: inputText,
        });
        const buffer = await (0, avatarDataToBytes_1.avatarDataToBytes)(newAvatarData);
        onDoneRef.current(buffer, newAvatarData);
    }, [inputText, selectedColor]);
    // In case the component unmounts before we're able to create the avatar data
    // we set the done handler to a no-op.
    (0, react_1.useEffect)(() => {
        return () => {
            onDoneRef.current = lodash_1.noop;
        };
    }, []);
    const measureElRef = (0, react_1.useRef)(null);
    (0, react_1.useEffect)(() => {
        const measureEl = measureElRef.current;
        if (!measureEl) {
            return;
        }
        const nextFontSize = (0, avatarTextSizeCalculator_1.getFittedFontSize)(BUBBLE_SIZE, inputText, candidateFontSize => {
            measureEl.style.fontSize = `${candidateFontSize}px`;
            const { width, height } = measureEl.getBoundingClientRect();
            return { height, width };
        });
        setFontSize(nextFontSize);
    }, [inputText]);
    (0, react_1.useEffect)(() => {
        focusInput();
    }, [focusInput]);
    const hasChanges = initialText !== inputText || selectedColor !== initialColor;
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: "AvatarEditor__preview" },
            react_1.default.createElement(BetterAvatarBubble_1.BetterAvatarBubble, { color: selectedColor, i18n: i18n, onSelect: focusInput, style: {
                    height: BUBBLE_SIZE,
                    width: BUBBLE_SIZE,
                } },
                react_1.default.createElement("input", { className: "AvatarTextEditor__input", onChange: handleChange, onPaste: handlePaste, ref: inputRef, style: { fontSize }, type: "text", value: inputText }))),
        react_1.default.createElement("hr", { className: "AvatarEditor__divider" }),
        react_1.default.createElement(AvatarColorPicker_1.AvatarColorPicker, { i18n: i18n, onColorSelected: color => {
                setSelectedColor(color);
                focusInput();
            }, selectedColor: selectedColor }),
        react_1.default.createElement(AvatarModalButtons_1.AvatarModalButtons, { hasChanges: hasChanges, i18n: i18n, onCancel: onCancel, onSave: handleDone }),
        react_1.default.createElement("div", { className: "AvatarTextEditor__measure", ref: measureElRef }, inputText)));
};
exports.AvatarTextEditor = AvatarTextEditor;
