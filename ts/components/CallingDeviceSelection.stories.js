"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const react_1 = require("@storybook/react");
const CallingDeviceSelection_1 = require("./CallingDeviceSelection");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const audioDevice = {
    name: '',
    index: 0,
    uniqueId: '',
    i18nKey: undefined,
};
const createProps = ({ availableMicrophones = [], availableSpeakers = [], selectedMicrophone = audioDevice, selectedSpeaker = audioDevice, availableCameras = [], selectedCamera = '', } = {}) => ({
    availableCameras,
    availableMicrophones,
    availableSpeakers,
    changeIODevice: (0, addon_actions_1.action)('change-io-device'),
    i18n,
    selectedCamera,
    selectedMicrophone,
    selectedSpeaker,
    toggleSettings: (0, addon_actions_1.action)('toggle-settings'),
});
const stories = (0, react_1.storiesOf)('Components/CallingDeviceSelection', module);
stories.add('Default', () => {
    return React.createElement(CallingDeviceSelection_1.CallingDeviceSelection, Object.assign({}, createProps()));
});
stories.add('Some Devices', () => {
    const availableSpeakers = [
        {
            name: 'Default',
            index: 0,
            uniqueId: 'Default',
            i18nKey: 'default_communication_device',
        },
        {
            name: "Natalie's Airpods (Bluetooth)",
            index: 1,
            uniqueId: 'aa',
        },
        {
            name: 'UE Boom (Bluetooth)',
            index: 2,
            uniqueId: 'bb',
        },
    ];
    const selectedSpeaker = availableSpeakers[0];
    const props = createProps({
        availableSpeakers,
        selectedSpeaker,
    });
    return React.createElement(CallingDeviceSelection_1.CallingDeviceSelection, Object.assign({}, props));
});
stories.add('Default Devices', () => {
    const availableSpeakers = [
        {
            name: 'default (Headphones)',
            index: 0,
            uniqueId: 'Default',
            i18nKey: 'default_communication_device',
        },
    ];
    const selectedSpeaker = availableSpeakers[0];
    const availableMicrophones = [
        {
            name: 'DefAuLt (Headphones)',
            index: 0,
            uniqueId: 'Default',
            i18nKey: 'default_communication_device',
        },
    ];
    const selectedMicrophone = availableMicrophones[0];
    const props = createProps({
        availableMicrophones,
        availableSpeakers,
        selectedMicrophone,
        selectedSpeaker,
    });
    return React.createElement(CallingDeviceSelection_1.CallingDeviceSelection, Object.assign({}, props));
});
stories.add('All Devices', () => {
    const availableSpeakers = [
        {
            name: 'Default',
            index: 0,
            uniqueId: 'Default',
            i18nKey: 'default_communication_device',
        },
        {
            name: "Natalie's Airpods (Bluetooth)",
            index: 1,
            uniqueId: 'aa',
        },
        {
            name: 'UE Boom (Bluetooth)',
            index: 2,
            uniqueId: 'bb',
        },
    ];
    const selectedSpeaker = availableSpeakers[0];
    const availableMicrophones = [
        {
            name: 'Default',
            index: 0,
            uniqueId: 'Default',
            i18nKey: 'default_communication_device',
        },
        {
            name: "Natalie's Airpods (Bluetooth)",
            index: 1,
            uniqueId: 'aa',
        },
    ];
    const selectedMicrophone = availableMicrophones[0];
    const availableCameras = [
        {
            deviceId: 'dfbe6effe70b0611ba0fdc2a9ea3f39f6cb110e6687948f7e5f016c111b7329c',
            groupId: '63ee218d2446869e40adfc958ff98263e51f74382b0143328ee4826f20a76f47',
            kind: 'videoinput',
            label: 'FaceTime HD Camera (Built-in) (9fba:bced)',
            toJSON() {
                return '';
            },
        },
        {
            deviceId: 'e2db196a31d50ff9b135299dc0beea67f65b1a25a06d8a4ce76976751bb7a08d',
            groupId: '218ba7f00d7b1239cca15b9116769e5e7d30cc01104ebf84d667643661e0ecf9',
            kind: 'videoinput',
            label: 'Logitech Webcam (4e72:9058)',
            toJSON() {
                return '';
            },
        },
    ];
    const selectedCamera = 'dfbe6effe70b0611ba0fdc2a9ea3f39f6cb110e6687948f7e5f016c111b7329c';
    const props = createProps({
        availableCameras,
        availableMicrophones,
        availableSpeakers,
        selectedCamera,
        selectedMicrophone,
        selectedSpeaker,
    });
    return React.createElement(CallingDeviceSelection_1.CallingDeviceSelection, Object.assign({}, props));
});
