"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BetterAvatarBubble = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const BetterAvatarBubble = ({ children, color, i18n, isSelected, onDelete, onSelect, style, }) => {
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)({
            BetterAvatarBubble: true,
            'BetterAvatarBubble--selected': isSelected,
        }, color && `BetterAvatarBubble--${color}`), onKeyDown: ev => {
            if (ev.key === 'Enter') {
                onSelect();
            }
        }, onClick: onSelect, role: "button", style: style, tabIndex: 0 },
        onDelete && (react_1.default.createElement("button", { "aria-label": i18n('delete'), className: "BetterAvatarBubble__delete", onClick: onDelete, tabIndex: -1, type: "button" })),
        children));
};
exports.BetterAvatarBubble = BetterAvatarBubble;
