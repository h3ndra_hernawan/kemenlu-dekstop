"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SharedGroupNames = void 0;
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const Emojify_1 = require("./conversation/Emojify");
const Intl_1 = require("./Intl");
const SharedGroupNames = ({ i18n, nameClassName, sharedGroupNames, }) => {
    const firstThreeGroups = (0, lodash_1.take)(sharedGroupNames, 3).map((group, i) => (
    // We cannot guarantee uniqueness of group names
    // eslint-disable-next-line react/no-array-index-key
    react_1.default.createElement("strong", { key: i, className: nameClassName },
        react_1.default.createElement(Emojify_1.Emojify, { text: group }))));
    if (sharedGroupNames.length >= 5) {
        const remainingCount = sharedGroupNames.length - 3;
        return (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "member-of-more-than-3-groups--multiple-more", components: {
                group1: firstThreeGroups[0],
                group2: firstThreeGroups[1],
                group3: firstThreeGroups[2],
                remainingCount: remainingCount.toString(),
            } }));
    }
    if (sharedGroupNames.length === 4) {
        return (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "member-of-more-than-3-groups--one-more", components: {
                group1: firstThreeGroups[0],
                group2: firstThreeGroups[1],
                group3: firstThreeGroups[2],
            } }));
    }
    if (firstThreeGroups.length === 3) {
        return (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "member-of-3-groups", components: {
                group1: firstThreeGroups[0],
                group2: firstThreeGroups[1],
                group3: firstThreeGroups[2],
            } }));
    }
    if (firstThreeGroups.length >= 2) {
        return (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "member-of-2-groups", components: {
                group1: firstThreeGroups[0],
                group2: firstThreeGroups[1],
            } }));
    }
    if (firstThreeGroups.length >= 1) {
        return (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "member-of-1-group", components: {
                group: firstThreeGroups[0],
            } }));
    }
    return react_1.default.createElement(react_1.default.Fragment, null, i18n('no-groups-in-common'));
};
exports.SharedGroupNames = SharedGroupNames;
