"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GlobalModalContainer = void 0;
const react_1 = __importDefault(require("react"));
const Button_1 = require("./Button");
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const WhatsNewModal_1 = require("./WhatsNewModal");
const GlobalModalContainer = ({ i18n, 
// ContactModal
contactModalState, renderContactModal, 
// ProfileEditor
isProfileEditorVisible, renderProfileEditor, 
// SafetyNumberModal
safetyNumberModalContactId, renderSafetyNumber, 
// UsernameNotFoundModal
hideUsernameNotFoundModal, usernameNotFoundModalState, 
// WhatsNewModal
hideWhatsNewModal, isWhatsNewVisible, }) => {
    if (safetyNumberModalContactId) {
        return renderSafetyNumber();
    }
    if (usernameNotFoundModalState) {
        return (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { cancelText: i18n('ok'), cancelButtonVariant: Button_1.ButtonVariant.Secondary, i18n: i18n, onClose: hideUsernameNotFoundModal }, i18n('startConversation--username-not-found', {
            atUsername: i18n('at-username', {
                username: usernameNotFoundModalState.username,
            }),
        })));
    }
    if (contactModalState) {
        return renderContactModal();
    }
    if (isProfileEditorVisible) {
        return renderProfileEditor();
    }
    if (isWhatsNewVisible) {
        return react_1.default.createElement(WhatsNewModal_1.WhatsNewModal, { hideWhatsNewModal: hideWhatsNewModal, i18n: i18n });
    }
    return null;
};
exports.GlobalModalContainer = GlobalModalContainer;
