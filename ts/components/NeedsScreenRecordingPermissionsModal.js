"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NeedsScreenRecordingPermissionsModal = void 0;
const react_1 = __importDefault(require("react"));
const theme_1 = require("../util/theme");
const Modal_1 = require("./Modal");
const Button_1 = require("./Button");
function focusRef(el) {
    if (el) {
        el.focus();
    }
}
const NeedsScreenRecordingPermissionsModal = ({ i18n, openSystemPreferencesAction, toggleScreenRecordingPermissionsDialog, }) => {
    return (react_1.default.createElement(Modal_1.Modal, { i18n: i18n, title: i18n('calling__presenting--permission-title'), theme: theme_1.Theme.Dark, onClose: toggleScreenRecordingPermissionsDialog },
        react_1.default.createElement("p", null, i18n('calling__presenting--macos-permission-description')),
        react_1.default.createElement("ol", { style: { paddingLeft: 16 } },
            react_1.default.createElement("li", null, i18n('calling__presenting--permission-instruction-step1')),
            react_1.default.createElement("li", null, i18n('calling__presenting--permission-instruction-step2')),
            react_1.default.createElement("li", null, i18n('calling__presenting--permission-instruction-step3'))),
        react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
            react_1.default.createElement(Button_1.Button, { onClick: toggleScreenRecordingPermissionsDialog, ref: focusRef, variant: Button_1.ButtonVariant.Secondary }, i18n('calling__presenting--permission-cancel')),
            react_1.default.createElement(Button_1.Button, { onClick: () => {
                    openSystemPreferencesAction();
                    toggleScreenRecordingPermissionsDialog();
                }, variant: Button_1.ButtonVariant.Primary }, i18n('calling__presenting--permission-open')))));
};
exports.NeedsScreenRecordingPermissionsModal = NeedsScreenRecordingPermissionsModal;
