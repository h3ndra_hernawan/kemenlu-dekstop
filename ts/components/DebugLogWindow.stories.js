"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const react_2 = require("@storybook/react");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const DebugLogWindow_1 = require("./DebugLogWindow");
const setupI18n_1 = require("../util/setupI18n");
const sleep_1 = require("../util/sleep");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = () => ({
    closeWindow: (0, addon_actions_1.action)('closeWindow'),
    downloadLog: (0, addon_actions_1.action)('downloadLog'),
    i18n,
    fetchLogs: () => {
        (0, addon_actions_1.action)('fetchLogs')();
        return Promise.resolve('Sample logs');
    },
    uploadLogs: async (logs) => {
        (0, addon_actions_1.action)('uploadLogs')(logs);
        await (0, sleep_1.sleep)(5000);
        return 'https://picsum.photos/1800/900';
    },
});
const story = (0, react_2.storiesOf)('Components/DebugLogWindow', module);
story.add('DebugLogWindow', () => react_1.default.createElement(DebugLogWindow_1.DebugLogWindow, Object.assign({}, createProps())));
