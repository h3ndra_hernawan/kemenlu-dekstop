"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Button = exports.ButtonIconType = exports.ButtonVariant = exports.ButtonSize = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const assert_1 = require("../util/assert");
var ButtonSize;
(function (ButtonSize) {
    ButtonSize[ButtonSize["Large"] = 0] = "Large";
    ButtonSize[ButtonSize["Medium"] = 1] = "Medium";
    ButtonSize[ButtonSize["Small"] = 2] = "Small";
})(ButtonSize = exports.ButtonSize || (exports.ButtonSize = {}));
var ButtonVariant;
(function (ButtonVariant) {
    ButtonVariant["Calling"] = "Calling";
    ButtonVariant["Destructive"] = "Destructive";
    ButtonVariant["Details"] = "Details";
    ButtonVariant["Primary"] = "Primary";
    ButtonVariant["Secondary"] = "Secondary";
    ButtonVariant["SecondaryAffirmative"] = "SecondaryAffirmative";
    ButtonVariant["SecondaryDestructive"] = "SecondaryDestructive";
    ButtonVariant["SystemMessage"] = "SystemMessage";
})(ButtonVariant = exports.ButtonVariant || (exports.ButtonVariant = {}));
var ButtonIconType;
(function (ButtonIconType) {
    ButtonIconType["audio"] = "audio";
    ButtonIconType["muted"] = "muted";
    ButtonIconType["photo"] = "photo";
    ButtonIconType["search"] = "search";
    ButtonIconType["text"] = "text";
    ButtonIconType["unmuted"] = "unmuted";
    ButtonIconType["video"] = "video";
})(ButtonIconType = exports.ButtonIconType || (exports.ButtonIconType = {}));
const SIZE_CLASS_NAMES = new Map([
    [ButtonSize.Large, 'module-Button--large'],
    [ButtonSize.Medium, 'module-Button--medium'],
    [ButtonSize.Small, 'module-Button--small'],
]);
const VARIANT_CLASS_NAMES = new Map([
    [ButtonVariant.Primary, 'module-Button--primary'],
    [ButtonVariant.Secondary, 'module-Button--secondary'],
    [
        ButtonVariant.SecondaryAffirmative,
        'module-Button--secondary module-Button--secondary--affirmative',
    ],
    [
        ButtonVariant.SecondaryDestructive,
        'module-Button--secondary module-Button--secondary--destructive',
    ],
    [ButtonVariant.Destructive, 'module-Button--destructive'],
    [ButtonVariant.Calling, 'module-Button--calling'],
    [ButtonVariant.SystemMessage, 'module-Button--system-message'],
    [ButtonVariant.Details, 'module-Button--details'],
]);
exports.Button = react_1.default.forwardRef((props, ref) => {
    const { children, className, disabled = false, icon, style, tabIndex, variant = ButtonVariant.Primary, size = variant === ButtonVariant.Details
        ? ButtonSize.Small
        : ButtonSize.Medium, } = props;
    const ariaLabel = props['aria-label'];
    let onClick;
    let type;
    if ('onClick' in props) {
        ({ onClick } = props);
        type = 'button';
    }
    else {
        onClick = undefined;
        ({ type } = props);
    }
    const sizeClassName = SIZE_CLASS_NAMES.get(size);
    (0, assert_1.assert)(sizeClassName, '<Button> size not found');
    const variantClassName = VARIANT_CLASS_NAMES.get(variant);
    (0, assert_1.assert)(variantClassName, '<Button> variant not found');
    return (react_1.default.createElement("button", { "aria-label": ariaLabel, className: (0, classnames_1.default)('module-Button', sizeClassName, variantClassName, icon && `module-Button--icon--${icon}`, className), disabled: disabled, onClick: onClick, ref: ref, style: style, tabIndex: tabIndex, 
        // The `type` should either be "button" or "submit", which is effectively static.
        // eslint-disable-next-line react/button-has-type
        type: type }, children));
});
