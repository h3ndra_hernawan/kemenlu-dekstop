"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPaneDialog = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Tooltip_1 = require("./Tooltip");
const _util_1 = require("./_util");
const BASE_CLASS_NAME = 'LeftPaneDialog';
const TOOLTIP_CLASS_NAME = `${BASE_CLASS_NAME}__tooltip`;
const LeftPaneDialog = ({ icon = 'warning', type, onClick, clickLabel, title, subtitle, children, hoverText, hasAction, containerWidthBreakpoint, hasXButton, onClose, closeLabel, }) => {
    const onClickWrap = (e) => {
        e.preventDefault();
        e.stopPropagation();
        onClick === null || onClick === void 0 ? void 0 : onClick();
    };
    const onKeyDownWrap = (e) => {
        if (e.key !== 'Enter' && e.key !== ' ') {
            return;
        }
        e.preventDefault();
        e.stopPropagation();
        onClick === null || onClick === void 0 ? void 0 : onClick();
    };
    const onCloseWrap = (e) => {
        e.preventDefault();
        e.stopPropagation();
        onClose === null || onClose === void 0 ? void 0 : onClose();
    };
    const iconClassName = typeof icon === 'string'
        ? (0, classnames_1.default)([
            `${BASE_CLASS_NAME}__icon`,
            `${BASE_CLASS_NAME}__icon--${icon}`,
        ])
        : undefined;
    let action;
    if (hasAction) {
        action = (react_1.default.createElement("button", { title: clickLabel, "aria-label": clickLabel, className: `${BASE_CLASS_NAME}__action-text`, onClick: onClickWrap, tabIndex: 0, type: "button" }, clickLabel));
    }
    let xButton;
    if (hasXButton) {
        xButton = (react_1.default.createElement("div", { className: `${BASE_CLASS_NAME}__container-close` },
            react_1.default.createElement("button", { title: closeLabel, "aria-label": closeLabel, className: `${BASE_CLASS_NAME}__close-button`, onClick: onCloseWrap, tabIndex: 0, type: "button" })));
    }
    const className = (0, classnames_1.default)([
        BASE_CLASS_NAME,
        type === undefined ? undefined : `${BASE_CLASS_NAME}--${type}`,
        onClick === undefined ? undefined : `${BASE_CLASS_NAME}--clickable`,
    ]);
    const message = (react_1.default.createElement(react_1.default.Fragment, null,
        title === undefined ? undefined : react_1.default.createElement("h3", null, title),
        subtitle === undefined ? undefined : react_1.default.createElement("div", null, subtitle),
        children,
        action));
    const content = (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: `${BASE_CLASS_NAME}__container` },
            typeof icon === 'string' ? react_1.default.createElement("div", { className: iconClassName }) : icon,
            react_1.default.createElement("div", { className: `${BASE_CLASS_NAME}__message` }, message)),
        xButton));
    let dialogNode;
    if (onClick) {
        dialogNode = (react_1.default.createElement("div", { className: className, role: "button", onClick: onClickWrap, onKeyDown: onKeyDownWrap, "aria-label": clickLabel, title: hoverText, tabIndex: 0 }, content));
    }
    else {
        dialogNode = (react_1.default.createElement("div", { className: className, title: hoverText }, content));
    }
    if (containerWidthBreakpoint === _util_1.WidthBreakpoint.Narrow) {
        return (react_1.default.createElement(Tooltip_1.Tooltip, { content: message, direction: Tooltip_1.TooltipPlacement.Right, className: (0, classnames_1.default)(TOOLTIP_CLASS_NAME, type && `${TOOLTIP_CLASS_NAME}--${type}`) }, dialogNode));
    }
    return dialogNode;
};
exports.LeftPaneDialog = LeftPaneDialog;
