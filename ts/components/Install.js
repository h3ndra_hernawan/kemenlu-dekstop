"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Install = void 0;
const react_1 = __importDefault(require("react"));
const BackboneHost_1 = require("./BackboneHost");
const Install = () => {
    return (react_1.default.createElement(BackboneHost_1.BackboneHost, { className: "full-screen-flow", View: window.Whisper.InstallView }));
};
exports.Install = Install;
