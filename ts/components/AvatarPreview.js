"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvatarPreview = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const log = __importStar(require("../logging/log"));
const Spinner_1 = require("./Spinner");
const Colors_1 = require("../types/Colors");
const getInitials_1 = require("../util/getInitials");
const imagePathToBytes_1 = require("../util/imagePathToBytes");
var ImageStatus;
(function (ImageStatus) {
    ImageStatus["Nothing"] = "nothing";
    ImageStatus["Loading"] = "loading";
    ImageStatus["HasImage"] = "has-image";
})(ImageStatus || (ImageStatus = {}));
const AvatarPreview = ({ avatarColor = Colors_1.AvatarColors[0], avatarPath, avatarValue, conversationTitle, i18n, isEditable, isGroup, onAvatarLoaded, onClear, onClick, style = {}, }) => {
    const startingAvatarPathRef = (0, react_1.useRef)(avatarValue ? undefined : avatarPath);
    const [avatarPreview, setAvatarPreview] = (0, react_1.useState)();
    // Loads the initial avatarPath if one is provided, but only if we're in editable mode.
    //   If we're not editable, we assume that we either have an avatarPath or we show a
    //   default avatar.
    (0, react_1.useEffect)(() => {
        if (!isEditable) {
            return;
        }
        const startingAvatarPath = startingAvatarPathRef.current;
        if (!startingAvatarPath) {
            return lodash_1.noop;
        }
        let shouldCancel = false;
        (async () => {
            try {
                const buffer = await (0, imagePathToBytes_1.imagePathToBytes)(startingAvatarPath);
                if (shouldCancel) {
                    return;
                }
                setAvatarPreview(buffer);
                if (onAvatarLoaded) {
                    onAvatarLoaded(buffer);
                }
            }
            catch (err) {
                if (shouldCancel) {
                    return;
                }
                log.warn(`Failed to convert image URL to array buffer. Error message: ${err && err.message}`);
            }
        })();
        return () => {
            shouldCancel = true;
        };
    }, [onAvatarLoaded, isEditable]);
    // Ensures that when avatarValue changes we generate new URLs
    (0, react_1.useEffect)(() => {
        if (avatarValue) {
            setAvatarPreview(avatarValue);
        }
        else {
            setAvatarPreview(undefined);
        }
    }, [avatarValue]);
    // Creates the object URL to render the Uint8Array image
    const [objectUrl, setObjectUrl] = (0, react_1.useState)();
    (0, react_1.useEffect)(() => {
        if (!avatarPreview) {
            setObjectUrl(undefined);
            return lodash_1.noop;
        }
        const url = URL.createObjectURL(new Blob([avatarPreview]));
        setObjectUrl(url);
        return () => {
            URL.revokeObjectURL(url);
        };
    }, [avatarPreview]);
    let imageStatus;
    let encodedPath;
    if (avatarValue && !objectUrl) {
        imageStatus = ImageStatus.Loading;
    }
    else if (objectUrl) {
        encodedPath = objectUrl;
        imageStatus = ImageStatus.HasImage;
    }
    else if (avatarPath) {
        encodedPath = encodeURI(avatarPath);
        imageStatus = ImageStatus.HasImage;
    }
    else {
        imageStatus = ImageStatus.Nothing;
    }
    const isLoading = imageStatus === ImageStatus.Loading;
    const clickProps = onClick
        ? {
            role: 'button',
            onClick,
            tabIndex: 0,
            onKeyDown: (event) => {
                if (event.key === 'Enter' || event.key === ' ') {
                    onClick();
                }
            },
        }
        : {};
    const componentStyle = Object.assign({}, style);
    if (onClick) {
        componentStyle.cursor = 'pointer';
    }
    if (imageStatus === ImageStatus.Nothing) {
        return (react_1.default.createElement("div", { className: "AvatarPreview" },
            react_1.default.createElement("div", Object.assign({ className: `AvatarPreview__avatar BetterAvatarBubble--${avatarColor}` }, clickProps, { style: componentStyle }),
                isGroup ? (react_1.default.createElement("div", { className: `BetterAvatarBubble--${avatarColor}--icon AvatarPreview__group` })) : ((0, getInitials_1.getInitials)(conversationTitle)),
                isEditable && react_1.default.createElement("div", { className: "AvatarPreview__upload" }))));
    }
    return (react_1.default.createElement("div", { className: "AvatarPreview" },
        react_1.default.createElement("div", Object.assign({ className: `AvatarPreview__avatar AvatarPreview__avatar--${imageStatus}` }, clickProps, { style: imageStatus === ImageStatus.HasImage && encodedPath
                ? Object.assign(Object.assign({}, componentStyle), { backgroundImage: `url('${encodedPath}')` }) : componentStyle }),
            isLoading && (react_1.default.createElement(Spinner_1.Spinner, { size: "70px", svgSize: "normal", direction: "on-avatar" })),
            imageStatus === ImageStatus.HasImage && onClear && (react_1.default.createElement("button", { "aria-label": i18n('delete'), className: "AvatarPreview__clear", onClick: onClear, tabIndex: -1, type: "button" })),
            isEditable && react_1.default.createElement("div", { className: "AvatarPreview__upload" }))));
};
exports.AvatarPreview = AvatarPreview;
