"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const CallingParticipantsList_1 = require("./CallingParticipantsList");
const Colors_1 = require("../types/Colors");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
function createParticipant(participantProps) {
    return Object.assign({ demuxId: 2, hasRemoteAudio: Boolean(participantProps.hasRemoteAudio), hasRemoteVideo: Boolean(participantProps.hasRemoteVideo), presenting: Boolean(participantProps.presenting), sharingScreen: Boolean(participantProps.sharingScreen), videoAspectRatio: 1.3 }, (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
        avatarPath: participantProps.avatarPath,
        color: (0, lodash_1.sample)(Colors_1.AvatarColors),
        isBlocked: Boolean(participantProps.isBlocked),
        name: participantProps.name,
        profileName: participantProps.title,
        title: String(participantProps.title),
    }));
}
const createProps = (overrideProps = {}) => ({
    i18n,
    onClose: (0, addon_actions_1.action)('on-close'),
    ourUuid: 'cf085e6a-e70b-41ec-a310-c198248af13f',
    participants: overrideProps.participants || [],
});
const story = (0, react_1.storiesOf)('Components/CallingParticipantsList', module);
story.add('No one', () => {
    const props = createProps();
    return React.createElement(CallingParticipantsList_1.CallingParticipantsList, Object.assign({}, props));
});
story.add('Solo Call', () => {
    const props = createProps({
        participants: [
            createParticipant({
                title: 'Bardock',
            }),
        ],
    });
    return React.createElement(CallingParticipantsList_1.CallingParticipantsList, Object.assign({}, props));
});
story.add('Many Participants', () => {
    const props = createProps({
        participants: [
            createParticipant({
                title: 'Son Goku',
            }),
            createParticipant({
                hasRemoteAudio: true,
                presenting: true,
                name: 'Rage Trunks',
                title: 'Rage Trunks',
            }),
            createParticipant({
                hasRemoteAudio: true,
                title: 'Prince Vegeta',
            }),
            createParticipant({
                hasRemoteAudio: true,
                hasRemoteVideo: true,
                name: 'Goku Black',
                title: 'Goku Black',
            }),
            createParticipant({
                title: 'Supreme Kai Zamasu',
            }),
        ],
    });
    return React.createElement(CallingParticipantsList_1.CallingParticipantsList, Object.assign({}, props));
});
story.add('Overflow', () => {
    const props = createProps({
        participants: Array(50)
            .fill(null)
            .map(() => createParticipant({ title: 'Kirby' })),
    });
    return React.createElement(CallingParticipantsList_1.CallingParticipantsList, Object.assign({}, props));
});
