"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getConversationListWidthBreakpoint = exports.WidthBreakpoint = exports.cleanId = void 0;
function cleanId(id) {
    return id.replace(/[^\u0020-\u007e\u00a0-\u00ff]/g, '_');
}
exports.cleanId = cleanId;
var WidthBreakpoint;
(function (WidthBreakpoint) {
    WidthBreakpoint["Wide"] = "wide";
    WidthBreakpoint["Medium"] = "medium";
    WidthBreakpoint["Narrow"] = "narrow";
})(WidthBreakpoint = exports.WidthBreakpoint || (exports.WidthBreakpoint = {}));
const getConversationListWidthBreakpoint = (width) => width >= 150 ? WidthBreakpoint.Wide : WidthBreakpoint.Narrow;
exports.getConversationListWidthBreakpoint = getConversationListWidthBreakpoint;
