"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SafetyNumberViewer = void 0;
const react_1 = __importDefault(require("react"));
const Button_1 = require("./Button");
const Intl_1 = require("./Intl");
const SafetyNumberViewer = ({ contact, generateSafetyNumber, i18n, onClose, safetyNumber, toggleVerified, verificationDisabled, }) => {
    react_1.default.useEffect(() => {
        if (!contact) {
            return;
        }
        generateSafetyNumber(contact);
    }, [contact, generateSafetyNumber, safetyNumber]);
    if (!contact) {
        return null;
    }
    if (!contact.phoneNumber) {
        return (react_1.default.createElement("div", { className: "module-SafetyNumberViewer" }, i18n('cannotGenerateSafetyNumber')));
    }
    const showNumber = Boolean(contact.name || contact.profileName);
    const numberFragment = showNumber && contact.phoneNumber ? ` · ${contact.phoneNumber}` : '';
    const name = `${contact.title}${numberFragment}`;
    const boldName = (react_1.default.createElement("span", { className: "module-SafetyNumberViewer__bold-name" }, name));
    const { isVerified } = contact;
    const verifiedStatusKey = isVerified ? 'isVerified' : 'isNotVerified';
    const verifyButtonText = isVerified ? i18n('unverify') : i18n('verify');
    return (react_1.default.createElement("div", { className: "module-SafetyNumberViewer" },
        onClose && (react_1.default.createElement("div", { className: "module-SafetyNumberViewer__close-button" },
            react_1.default.createElement("button", { onClick: onClose, tabIndex: 0, type: "button" },
                react_1.default.createElement("span", null)))),
        react_1.default.createElement("div", { className: "module-SafetyNumberViewer__number" }, safetyNumber || getPlaceholder()),
        react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "verifyHelp", components: [boldName] }),
        react_1.default.createElement("div", { className: "module-SafetyNumberViewer__verification-status" },
            isVerified ? (react_1.default.createElement("span", { className: "module-SafetyNumberViewer__icon--verified" })) : (react_1.default.createElement("span", { className: "module-SafetyNumberViewer__icon--shield" })),
            react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: verifiedStatusKey, components: [boldName] })),
        react_1.default.createElement("div", { className: "module-SafetyNumberViewer__button" },
            react_1.default.createElement(Button_1.Button, { disabled: verificationDisabled, onClick: () => {
                    toggleVerified(contact);
                }, variant: Button_1.ButtonVariant.Secondary }, verifyButtonText))));
};
exports.SafetyNumberViewer = SafetyNumberViewer;
function getPlaceholder() {
    return Array.from(Array(12))
        .map(() => 'XXXXX')
        .join(' ');
}
