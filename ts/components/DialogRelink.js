"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DialogRelink = void 0;
const react_1 = __importDefault(require("react"));
const LeftPaneDialog_1 = require("./LeftPaneDialog");
const DialogRelink = ({ containerWidthBreakpoint, i18n, isRegistrationDone, relinkDevice, }) => {
    if (isRegistrationDone) {
        return null;
    }
    return (react_1.default.createElement(LeftPaneDialog_1.LeftPaneDialog, { containerWidthBreakpoint: containerWidthBreakpoint, type: "warning", icon: "relink", clickLabel: i18n('unlinkedWarning'), onClick: relinkDevice, title: i18n('unlinked'), hasAction: true }));
};
exports.DialogRelink = DialogRelink;
