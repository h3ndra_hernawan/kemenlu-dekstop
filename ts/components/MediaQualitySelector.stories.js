"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const MediaQualitySelector_1 = require("./MediaQualitySelector");
const setupI18n_1 = require("../util/setupI18n");
const story = (0, react_2.storiesOf)('Components/MediaQualitySelector', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    i18n,
    isHighQuality: (0, addon_knobs_1.boolean)('isHighQuality', Boolean(overrideProps.isHighQuality)),
    onSelectQuality: (0, addon_actions_1.action)('onSelectQuality'),
});
story.add('Standard Quality', () => (react_1.default.createElement(MediaQualitySelector_1.MediaQualitySelector, Object.assign({}, createProps()))));
story.add('High Quality', () => (react_1.default.createElement(MediaQualitySelector_1.MediaQualitySelector, Object.assign({}, createProps({
    isHighQuality: true,
})))));
