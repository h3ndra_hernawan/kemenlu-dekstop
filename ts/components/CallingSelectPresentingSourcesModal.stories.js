"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const CallingSelectPresentingSourcesModal_1 = require("./CallingSelectPresentingSourcesModal");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = () => ({
    i18n,
    presentingSourcesAvailable: [
        {
            id: 'screen',
            name: 'Entire Screen',
            isScreen: true,
            thumbnail: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P/1PwAF8AL1sEVIPAAAAABJRU5ErkJggg==',
        },
        {
            id: 'window:123',
            name: 'Bozirro Airhorse',
            isScreen: false,
            thumbnail: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z1D4HwAF5wJxzsNOIAAAAABJRU5ErkJggg==',
        },
        {
            id: 'window:456',
            name: 'Discoverer',
            isScreen: false,
            thumbnail: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8HwHwAFHQIIj4yLtgAAAABJRU5ErkJggg==',
        },
        {
            id: 'window:789',
            name: 'Signal Beta',
            isScreen: false,
            thumbnail: '',
        },
        {
            id: 'window:xyz',
            name: 'Window that has a really long name and overflows',
            isScreen: false,
            thumbnail: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+O/wHwAEhgJAyqFnAgAAAABJRU5ErkJggg==',
        },
    ],
    setPresenting: (0, addon_actions_1.action)('set-presenting'),
});
const story = (0, react_2.storiesOf)('Components/CallingSelectPresentingSourcesModal', module);
story.add('Modal', () => {
    return react_1.default.createElement(CallingSelectPresentingSourcesModal_1.CallingSelectPresentingSourcesModal, Object.assign({}, createProps()));
});
