"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const AvatarPopup_1 = require("./AvatarPopup");
const Colors_1 = require("../types/Colors");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const colorMap = Colors_1.AvatarColors.reduce((m, color) => (Object.assign(Object.assign({}, m), { [color]: color })), {});
const conversationTypeMap = {
    direct: 'direct',
    group: 'group',
};
const createProps = (overrideProps = {}) => ({
    acceptedMessageRequest: true,
    avatarPath: (0, addon_knobs_1.text)('avatarPath', overrideProps.avatarPath || ''),
    color: (0, addon_knobs_1.select)('color', colorMap, overrideProps.color || Colors_1.AvatarColors[0]),
    conversationType: (0, addon_knobs_1.select)('conversationType', conversationTypeMap, overrideProps.conversationType || 'direct'),
    hasPendingUpdate: Boolean(overrideProps.hasPendingUpdate),
    i18n,
    isMe: true,
    name: (0, addon_knobs_1.text)('name', overrideProps.name || ''),
    noteToSelf: (0, addon_knobs_1.boolean)('noteToSelf', overrideProps.noteToSelf || false),
    onEditProfile: (0, addon_actions_1.action)('onEditProfile'),
    onViewArchive: (0, addon_actions_1.action)('onViewArchive'),
    onViewPreferences: (0, addon_actions_1.action)('onViewPreferences'),
    phoneNumber: (0, addon_knobs_1.text)('phoneNumber', overrideProps.phoneNumber || ''),
    profileName: (0, addon_knobs_1.text)('profileName', overrideProps.profileName || ''),
    sharedGroupNames: [],
    size: 80,
    startUpdate: (0, addon_actions_1.action)('startUpdate'),
    style: {},
    title: (0, addon_knobs_1.text)('title', overrideProps.title || ''),
});
const stories = (0, react_1.storiesOf)('Components/Avatar Popup', module);
stories.add('Avatar Only', () => {
    const props = createProps();
    return React.createElement(AvatarPopup_1.AvatarPopup, Object.assign({}, props));
});
stories.add('Title', () => {
    const props = createProps({
        title: 'My Great Title',
    });
    return React.createElement(AvatarPopup_1.AvatarPopup, Object.assign({}, props));
});
stories.add('Profile Name', () => {
    const props = createProps({
        profileName: 'Sam Neill',
    });
    return React.createElement(AvatarPopup_1.AvatarPopup, Object.assign({}, props));
});
stories.add('Phone Number', () => {
    const props = createProps({
        profileName: 'Sam Neill',
        phoneNumber: '(555) 867-5309',
    });
    return React.createElement(AvatarPopup_1.AvatarPopup, Object.assign({}, props));
});
stories.add('Update Available', () => {
    const props = createProps({
        hasPendingUpdate: true,
    });
    return React.createElement(AvatarPopup_1.AvatarPopup, Object.assign({}, props));
});
