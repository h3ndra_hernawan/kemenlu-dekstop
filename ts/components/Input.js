"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Input = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const grapheme = __importStar(require("../util/grapheme"));
const getClassNamesFor_1 = require("../util/getClassNamesFor");
const refMerger_1 = require("../util/refMerger");
const Bytes_1 = require("../Bytes");
/**
 * Some inputs must have fewer than maxLengthCount glyphs. Ideally, we'd use the
 * `maxLength` property on inputs, but that doesn't account for glyphs that are more than
 * one UTF-16 code units. For example: `'💩💩'.length === 4`.
 *
 * This component effectively implements a "max grapheme length" on an input.
 *
 * At a high level, this component handles two methods of input:
 *
 * - `onChange`. *Before* the value is changed (in `onKeyDown`), we save the value and the
 *   cursor position. Then, in `onChange`, we see if the new value is too long. If it is,
 *   we revert the value and selection. Otherwise, we fire `onChangeValue`.
 *
 * - `onPaste`. If you're pasting something that will fit, we fall back to normal browser
 *   behavior, which calls `onChange`. If you're pasting something that won't fit, it's a
 *   noop.
 */
exports.Input = (0, react_1.forwardRef)(({ countLength = grapheme.count, countBytes = Bytes_1.byteLength, disabled, expandable, hasClearButton, i18n, icon, maxLengthCount = 0, maxByteCount = 0, moduleClassName, onChange, onEnter, placeholder, value = '', whenToShowRemainingCount = Infinity, }, ref) => {
    const innerRef = (0, react_1.useRef)(null);
    const valueOnKeydownRef = (0, react_1.useRef)(value);
    const selectionStartOnKeydownRef = (0, react_1.useRef)(value.length);
    const [isLarge, setIsLarge] = (0, react_1.useState)(false);
    const maybeSetLarge = (0, react_1.useCallback)(() => {
        if (!expandable) {
            return;
        }
        const inputEl = innerRef.current;
        if (!inputEl) {
            return;
        }
        if (inputEl.scrollHeight > inputEl.clientHeight ||
            inputEl.scrollWidth > inputEl.clientWidth) {
            setIsLarge(true);
        }
    }, [expandable]);
    const handleKeyDown = (0, react_1.useCallback)(event => {
        if (onEnter && event.key === 'Enter') {
            onEnter();
        }
        const inputEl = innerRef.current;
        if (!inputEl) {
            return;
        }
        valueOnKeydownRef.current = inputEl.value;
        selectionStartOnKeydownRef.current = inputEl.selectionStart || 0;
    }, [onEnter]);
    const handleChange = (0, react_1.useCallback)(() => {
        const inputEl = innerRef.current;
        if (!inputEl) {
            return;
        }
        const newValue = inputEl.value;
        const newLengthCount = maxLengthCount ? countLength(newValue) : 0;
        const newByteCount = maxByteCount ? countBytes(newValue) : 0;
        if (newLengthCount <= maxLengthCount && newByteCount <= maxByteCount) {
            onChange(newValue);
        }
        else {
            inputEl.value = valueOnKeydownRef.current;
            inputEl.selectionStart = selectionStartOnKeydownRef.current;
            inputEl.selectionEnd = selectionStartOnKeydownRef.current;
        }
        maybeSetLarge();
    }, [
        countLength,
        countBytes,
        maxLengthCount,
        maxByteCount,
        maybeSetLarge,
        onChange,
    ]);
    const handlePaste = (0, react_1.useCallback)((event) => {
        const inputEl = innerRef.current;
        if (!inputEl || !maxLengthCount || !maxByteCount) {
            return;
        }
        const selectionStart = inputEl.selectionStart || 0;
        const selectionEnd = inputEl.selectionEnd || inputEl.selectionStart || 0;
        const textBeforeSelection = value.slice(0, selectionStart);
        const textAfterSelection = value.slice(selectionEnd);
        const pastedText = event.clipboardData.getData('Text');
        const newLengthCount = countLength(textBeforeSelection) +
            countLength(pastedText) +
            countLength(textAfterSelection);
        const newByteCount = countBytes(textBeforeSelection) +
            countBytes(pastedText) +
            countBytes(textAfterSelection);
        if (newLengthCount > maxLengthCount || newByteCount > maxByteCount) {
            event.preventDefault();
        }
        maybeSetLarge();
    }, [
        countLength,
        countBytes,
        maxLengthCount,
        maxByteCount,
        maybeSetLarge,
        value,
    ]);
    (0, react_1.useEffect)(() => {
        maybeSetLarge();
    }, [maybeSetLarge]);
    const lengthCount = maxLengthCount ? countLength(value) : -1;
    const getClassName = (0, getClassNamesFor_1.getClassNamesFor)('Input', moduleClassName);
    const inputProps = {
        className: (0, classnames_1.default)(getClassName('__input'), icon && getClassName('__input--with-icon'), isLarge && getClassName('__input--large')),
        disabled: Boolean(disabled),
        onChange: handleChange,
        onKeyDown: handleKeyDown,
        onPaste: handlePaste,
        placeholder,
        ref: (0, refMerger_1.refMerger)(ref, innerRef),
        type: 'text',
        value,
    };
    const clearButtonElement = hasClearButton && value ? (react_1.default.createElement("button", { tabIndex: -1, className: getClassName('__clear-icon'), onClick: () => onChange(''), type: "button", "aria-label": i18n('cancel') })) : null;
    const lengthCountElement = lengthCount >= whenToShowRemainingCount && (react_1.default.createElement("div", { className: getClassName('__remaining-count') }, maxLengthCount - lengthCount));
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(getClassName('__container'), disabled && getClassName('__container--disabled')) },
        icon ? react_1.default.createElement("div", { className: getClassName('__icon') }, icon) : null,
        expandable ? react_1.default.createElement("textarea", Object.assign({}, inputProps)) : react_1.default.createElement("input", Object.assign({}, inputProps)),
        isLarge ? (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: getClassName('__controls') }, clearButtonElement),
            react_1.default.createElement("div", { className: getClassName('__remaining-count--large') }, lengthCountElement))) : (react_1.default.createElement("div", { className: getClassName('__controls') },
            lengthCountElement,
            clearButtonElement))));
});
