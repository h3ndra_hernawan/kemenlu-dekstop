"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfirmDiscardDialog = void 0;
const react_1 = __importDefault(require("react"));
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const ConfirmDiscardDialog = ({ i18n, onClose, onDiscard, }) => {
    return (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
            {
                action: onDiscard,
                text: i18n('discard'),
                style: 'negative',
            },
        ], i18n: i18n, onClose: onClose }, i18n('ConfirmDiscardDialog--discard')));
};
exports.ConfirmDiscardDialog = ConfirmDiscardDialog;
