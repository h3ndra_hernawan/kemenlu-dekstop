"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ToastTapToViewExpiredOutgoing = void 0;
const react_1 = __importDefault(require("react"));
const Toast_1 = require("./Toast");
const ToastTapToViewExpiredOutgoing = ({ i18n, onClose, }) => {
    return (react_1.default.createElement(Toast_1.Toast, { onClose: onClose }, i18n('Message--tap-to-view--outgoing--expired-toast')));
};
exports.ToastTapToViewExpiredOutgoing = ToastTapToViewExpiredOutgoing;
