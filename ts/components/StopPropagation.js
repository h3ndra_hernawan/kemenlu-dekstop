"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StopPropagation = void 0;
const react_1 = __importDefault(require("react"));
// Whenever you don't want click or key events to propagate into their parent container
const StopPropagation = ({ children, className, }) => (
// eslint-disable-next-line max-len
// eslint-disable-next-line jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events
react_1.default.createElement("div", { className: className, onClick: ev => ev.stopPropagation(), onKeyDown: ev => ev.stopPropagation() }, children));
exports.StopPropagation = StopPropagation;
