"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DirectCallRemoteParticipant = void 0;
const react_1 = __importStar(require("react"));
const Colors_1 = require("../types/Colors");
const Avatar_1 = require("./Avatar");
const DirectCallRemoteParticipant = ({ conversation, hasRemoteVideo, i18n, setRendererCanvas, }) => {
    const remoteVideoRef = (0, react_1.useRef)(null);
    (0, react_1.useEffect)(() => {
        setRendererCanvas({ element: remoteVideoRef });
        return () => {
            setRendererCanvas({ element: undefined });
        };
    }, [setRendererCanvas]);
    return hasRemoteVideo ? (react_1.default.createElement("canvas", { className: "module-ongoing-call__remote-video-enabled", ref: remoteVideoRef })) : (renderAvatar(i18n, conversation));
};
exports.DirectCallRemoteParticipant = DirectCallRemoteParticipant;
function renderAvatar(i18n, { acceptedMessageRequest, avatarPath, color, isMe, name, phoneNumber, profileName, sharedGroupNames, title, }) {
    return (react_1.default.createElement("div", { className: "module-ongoing-call__remote-video-disabled" },
        react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, color: color || Colors_1.AvatarColors[0], noteToSelf: false, conversationType: "direct", i18n: i18n, isMe: isMe, name: name, phoneNumber: phoneNumber, profileName: profileName, title: title, sharedGroupNames: sharedGroupNames, size: 112 })));
}
