"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Spinner = exports.SpinnerDirections = exports.SpinnerSvgSizes = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const getClassNamesFor_1 = require("../util/getClassNamesFor");
exports.SpinnerSvgSizes = ['small', 'normal'];
exports.SpinnerDirections = [
    'outgoing',
    'incoming',
    'on-background',
    'on-captcha',
    'on-progress-dialog',
    'on-avatar',
];
const Spinner = ({ ariaLabel, direction, moduleClassName, role, size, svgSize, }) => {
    const getClassName = (0, getClassNamesFor_1.getClassNamesFor)('module-spinner', moduleClassName);
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(getClassName('__container'), getClassName(`__container--${svgSize}`), getClassName(direction && `__container--${direction}`), getClassName(direction && `__container--${svgSize}-${direction}`)), role: role, "aria-label": ariaLabel, style: {
            height: size,
            width: size,
        } },
        react_1.default.createElement("div", { className: (0, classnames_1.default)(getClassName('__circle'), getClassName(`__circle--${svgSize}`), getClassName(direction && `__circle--${direction}`), getClassName(direction && `__circle--${svgSize}-${direction}`)) }),
        react_1.default.createElement("div", { className: (0, classnames_1.default)(getClassName('__arc'), getClassName(`__arc--${svgSize}`), getClassName(direction && `__arc--${direction}`), getClassName(direction && `__arc--${svgSize}-${direction}`)) })));
};
exports.Spinner = Spinner;
