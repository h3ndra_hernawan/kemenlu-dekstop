"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupV2JoinDialog = void 0;
const React = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Avatar_1 = require("./Avatar");
const Spinner_1 = require("./Spinner");
const Button_1 = require("./Button");
const GroupDescription_1 = require("./conversation/GroupDescription");
function focusRef(el) {
    if (el) {
        el.focus();
    }
}
exports.GroupV2JoinDialog = React.memo((props) => {
    const [isWorking, setIsWorking] = React.useState(false);
    const [isJoining, setIsJoining] = React.useState(false);
    const { approvalRequired, avatar, groupDescription, i18n, join, memberCount, onClose, title, } = props;
    const joinString = approvalRequired
        ? i18n('GroupV2--join--request-to-join-button')
        : i18n('GroupV2--join--join-button');
    const memberString = memberCount === 1
        ? i18n('GroupV2--join--member-count--single')
        : i18n('GroupV2--join--member-count--multiple', {
            count: memberCount.toString(),
        });
    const wrappedJoin = React.useCallback(() => {
        setIsWorking(true);
        setIsJoining(true);
        join();
    }, [join, setIsJoining, setIsWorking]);
    const wrappedClose = React.useCallback(() => {
        setIsWorking(true);
        onClose();
    }, [onClose, setIsWorking]);
    return (React.createElement("div", { className: "module-group-v2-join-dialog" },
        React.createElement("button", { "aria-label": i18n('close'), type: "button", disabled: isWorking, className: "module-group-v2-join-dialog__close-button", onClick: wrappedClose }),
        React.createElement("div", { className: "module-group-v2-join-dialog__avatar" },
            React.createElement(Avatar_1.Avatar, { acceptedMessageRequest: false, avatarPath: avatar ? avatar.url : undefined, blur: Avatar_1.AvatarBlur.NoBlur, loading: avatar && !avatar.url, conversationType: "group", title: title, isMe: false, sharedGroupNames: [], size: 80, i18n: i18n })),
        React.createElement("div", { className: "module-group-v2-join-dialog__title" }, title),
        React.createElement("div", { className: "module-group-v2-join-dialog__metadata" }, i18n('GroupV2--join--group-metadata', [memberString])),
        groupDescription && (React.createElement("div", { className: "module-group-v2-join-dialog__description" },
            React.createElement(GroupDescription_1.GroupDescription, { i18n: i18n, title: title, text: groupDescription }))),
        approvalRequired ? (React.createElement("div", { className: "module-group-v2-join-dialog__prompt--approval" }, i18n('GroupV2--join--prompt-with-approval'))) : (React.createElement("div", { className: "module-group-v2-join-dialog__prompt" }, i18n('GroupV2--join--prompt'))),
        React.createElement("div", { className: "module-group-v2-join-dialog__buttons" },
            React.createElement(Button_1.Button, { className: (0, classnames_1.default)('module-group-v2-join-dialog__button', 'module-group-v2-join-dialog__button--secondary'), disabled: isWorking, onClick: wrappedClose, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
            React.createElement(Button_1.Button, { className: "module-group-v2-join-dialog__button", disabled: isWorking, ref: focusRef, onClick: wrappedJoin, variant: Button_1.ButtonVariant.Primary }, isJoining ? (React.createElement(Spinner_1.Spinner, { size: "20px", svgSize: "small", direction: "on-avatar" })) : (joinString)))));
});
