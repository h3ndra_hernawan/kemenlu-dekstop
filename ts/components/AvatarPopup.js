"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvatarPopup = void 0;
const React = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Avatar_1 = require("./Avatar");
const useRestoreFocus_1 = require("../hooks/useRestoreFocus");
const AvatarPopup = (props) => {
    const { hasPendingUpdate, i18n, name, onEditProfile, onViewArchive, onViewPreferences, phoneNumber, profileName, startUpdate, style, title, } = props;
    const shouldShowNumber = Boolean(name || profileName);
    // Note: mechanisms to dismiss this view are all in its host, MainHeader
    // Focus first button after initial render, restore focus on teardown
    const [focusRef] = (0, useRestoreFocus_1.useRestoreFocus)();
    return (React.createElement("div", { style: style, className: "module-avatar-popup" },
        React.createElement("button", { className: "module-avatar-popup__profile", onClick: onEditProfile, ref: focusRef, type: "button" },
            React.createElement(Avatar_1.Avatar, Object.assign({}, props, { size: 52 })),
            React.createElement("div", { className: "module-avatar-popup__profile__text" },
                React.createElement("div", { className: "module-avatar-popup__profile__name" }, profileName || title),
                shouldShowNumber ? (React.createElement("div", { className: "module-avatar-popup__profile__number" }, phoneNumber)) : null)),
        React.createElement("hr", { className: "module-avatar-popup__divider" }),
        React.createElement("button", { type: "button", className: "module-avatar-popup__item", onClick: onViewPreferences },
            React.createElement("div", { className: (0, classnames_1.default)('module-avatar-popup__item__icon', 'module-avatar-popup__item__icon-settings') }),
            React.createElement("div", { className: "module-avatar-popup__item__text" }, i18n('mainMenuSettings'))),
        React.createElement("button", { type: "button", className: "module-avatar-popup__item", onClick: onViewArchive },
            React.createElement("div", { className: (0, classnames_1.default)('module-avatar-popup__item__icon', 'module-avatar-popup__item__icon-archive') }),
            React.createElement("div", { className: "module-avatar-popup__item__text" }, i18n('avatarMenuViewArchive'))),
        hasPendingUpdate && (React.createElement("button", { type: "button", className: "module-avatar-popup__item", onClick: startUpdate },
            React.createElement("div", { className: (0, classnames_1.default)('module-avatar-popup__item__icon', 'module-avatar-popup__item__icon--update') }),
            React.createElement("div", { className: "module-avatar-popup__item__text" }, i18n('avatarMenuUpdateAvailable')),
            React.createElement("div", { className: "module-avatar-popup__item--badge" })))));
};
exports.AvatarPopup = AvatarPopup;
