"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tooltip = exports.TooltipPlacement = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const react_popper_1 = require("react-popper");
const theme_1 = require("../util/theme");
const refMerger_1 = require("../util/refMerger");
const popperUtil_1 = require("../util/popperUtil");
// React doesn't reliably fire `onMouseLeave` or `onMouseOut` events if wrapping a
//   disabled button. This uses native browser events to avoid that.
//
// See <https://lecstor.com/react-disabled-button-onmouseleave/>.
const TooltipEventWrapper = react_1.default.forwardRef(({ onHoverChanged, children }, ref) => {
    const wrapperRef = react_1.default.useRef(null);
    const on = react_1.default.useCallback(() => {
        onHoverChanged(true);
    }, [onHoverChanged]);
    const off = react_1.default.useCallback(() => {
        onHoverChanged(false);
    }, [onHoverChanged]);
    const onFocus = react_1.default.useCallback(() => {
        if (window.getInteractionMode() === 'keyboard') {
            on();
        }
    }, [on]);
    react_1.default.useEffect(() => {
        const wrapperEl = wrapperRef.current;
        if (!wrapperEl) {
            return lodash_1.noop;
        }
        wrapperEl.addEventListener('mouseenter', on);
        wrapperEl.addEventListener('mouseleave', off);
        return () => {
            wrapperEl.removeEventListener('mouseenter', on);
            wrapperEl.removeEventListener('mouseleave', off);
        };
    }, [on, off]);
    return (react_1.default.createElement("span", { onFocus: onFocus, onBlur: off, ref: (0, refMerger_1.refMerger)(ref, wrapperRef) }, children));
});
var TooltipPlacement;
(function (TooltipPlacement) {
    TooltipPlacement["Top"] = "top";
    TooltipPlacement["Right"] = "right";
    TooltipPlacement["Bottom"] = "bottom";
    TooltipPlacement["Left"] = "left";
})(TooltipPlacement = exports.TooltipPlacement || (exports.TooltipPlacement = {}));
const Tooltip = ({ children, className, content, direction, sticky, theme, popperModifiers = [], }) => {
    const [isHovering, setIsHovering] = react_1.default.useState(false);
    const showTooltip = isHovering || Boolean(sticky);
    const tooltipThemeClassName = theme
        ? `module-tooltip--${(0, theme_1.themeClassName)(theme)}`
        : undefined;
    return (react_1.default.createElement(react_popper_1.Manager, null,
        react_1.default.createElement(react_popper_1.Reference, null, ({ ref }) => (react_1.default.createElement(TooltipEventWrapper, { ref: ref, onHoverChanged: setIsHovering }, children))),
        react_1.default.createElement(react_popper_1.Popper, { placement: direction, modifiers: [(0, popperUtil_1.offsetDistanceModifier)(12), ...popperModifiers] }, ({ arrowProps, placement, ref, style }) => showTooltip && (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-tooltip', tooltipThemeClassName, className), ref: ref, style: style, "data-placement": placement },
            content,
            react_1.default.createElement("div", { className: "module-tooltip-arrow", ref: arrowProps.ref, style: arrowProps.style }))))));
};
exports.Tooltip = Tooltip;
