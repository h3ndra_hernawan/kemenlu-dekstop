"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GradientDial = exports.KnobType = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
var KnobType;
(function (KnobType) {
    KnobType["start"] = "start";
    KnobType["end"] = "end";
})(KnobType = exports.KnobType || (exports.KnobType = {}));
// Converts from degrees to radians.
function toRadians(degrees) {
    return (degrees * Math.PI) / 180;
}
// Converts from radians to degrees.
function toDegrees(radians) {
    return (radians * 180) / Math.PI;
}
function getKnobCoordinates(degrees, rect) {
    const center = {
        x: rect.width / 2,
        y: rect.height / 2,
    };
    const alpha = toDegrees(Math.atan(rect.height / rect.width));
    const beta = (360.0 - alpha * 4) / 4;
    if (degrees < alpha) {
        // Right top
        const a = center.x;
        const b = a * Math.tan(toRadians(degrees));
        return {
            start: {
                left: rect.width,
                top: center.y - b,
            },
            end: {
                left: 0,
                top: center.y + b,
            },
        };
    }
    if (degrees < 90) {
        // Top right
        const phi = 90 - degrees;
        const a = center.y;
        const b = a * Math.tan(toRadians(phi));
        return {
            start: {
                left: center.x + b,
                top: 0,
            },
            end: {
                left: center.x - b,
                top: rect.height,
            },
        };
    }
    if (degrees < 90 + beta) {
        // Top left
        const phi = degrees - 90;
        const a = center.y;
        const b = a * Math.tan(toRadians(phi));
        return {
            start: {
                left: center.x - b,
                top: 0,
            },
            end: {
                left: center.x + b,
                top: rect.height,
            },
        };
    }
    if (degrees < 180) {
        // left top
        const phi = 180 - degrees;
        const a = center.x;
        const b = a * Math.tan(toRadians(phi));
        return {
            start: {
                left: 0,
                top: center.y - b,
            },
            end: {
                left: rect.width,
                top: center.y + b,
            },
        };
    }
    if (degrees < 180 + alpha) {
        // left bottom
        const phi = degrees - 180;
        const a = center.x;
        const b = a * Math.tan(toRadians(phi));
        return {
            start: {
                left: 0,
                top: center.y + b,
            },
            end: {
                left: rect.width,
                top: center.y - b,
            },
        };
    }
    if (degrees < 270) {
        // bottom left
        const phi = 270 - degrees;
        const a = center.y;
        const b = a * Math.tan(toRadians(phi));
        return {
            start: {
                left: center.x - b,
                top: rect.height,
            },
            end: {
                left: center.x + b,
                top: 0,
            },
        };
    }
    if (degrees < 270 + beta) {
        // bottom right
        const phi = degrees - 270;
        const a = center.y;
        const b = a * Math.tan(toRadians(phi));
        return {
            start: {
                left: center.x + b,
                top: rect.height,
            },
            end: {
                left: center.x - b,
                top: 0,
            },
        };
    }
    // right bottom
    const phi = 360 - degrees;
    const a = center.x;
    const b = a * Math.tan(toRadians(phi));
    return {
        start: {
            left: rect.width,
            top: center.y + b,
        },
        end: {
            left: 0,
            top: center.y - b,
        },
    };
}
const GradientDial = ({ deg = 180, knob1Style, knob2Style, onChange, onClick, selectedKnob, }) => {
    const containerRef = (0, react_1.useRef)(null);
    const [knobDim, setKnobDim] = (0, react_1.useState)({});
    const handleMouseMove = (ev) => {
        if (!containerRef || !containerRef.current) {
            return;
        }
        const rect = containerRef.current.getBoundingClientRect();
        const center = {
            x: rect.width / 2,
            y: rect.height / 2,
        };
        const a = {
            x: ev.clientX - (rect.x + center.x),
            y: ev.clientY - (rect.y + center.y),
        };
        const b = { x: center.x, y: 0 };
        const dot = a.x * b.x + a.y * b.y;
        const det = a.x * b.y - a.y * b.x;
        const offset = selectedKnob === KnobType.end ? 180 : 0;
        const degrees = (toDegrees(Math.atan2(det, dot)) + 360 + offset) % 360;
        onChange(degrees);
        ev.preventDefault();
        ev.stopPropagation();
    };
    const handleMouseUp = () => {
        document.removeEventListener('mouseup', handleMouseUp);
        document.removeEventListener('mousemove', handleMouseMove);
    };
    // We want to use React.MouseEvent here because above we
    // use the regular MouseEvent
    const handleMouseDown = (ev) => {
        ev.preventDefault();
        ev.stopPropagation();
        document.addEventListener('mousemove', handleMouseMove);
        document.addEventListener('mouseup', handleMouseUp);
    };
    const handleKeyDown = (ev) => {
        let add = 1;
        if (ev.key === 'ArrowDown' || ev.key === 'ArrowLeft') {
            add = 1;
        }
        if (ev.key === 'ArrowRight' || ev.key === 'ArrowUp') {
            add = -1;
        }
        onChange(Math.min(360, Math.max(0, deg + add)));
    };
    (0, react_1.useEffect)(() => {
        if (!containerRef || !containerRef.current) {
            return;
        }
        const containerRect = containerRef.current.getBoundingClientRect();
        setKnobDim(getKnobCoordinates(deg, containerRect));
    }, [containerRef, deg]);
    return (react_1.default.createElement("div", { className: "GradientDial__container", ref: containerRef },
        knobDim.start && (react_1.default.createElement("div", { "aria-label": "0", className: (0, classnames_1.default)('GradientDial__knob', {
                'GradientDial__knob--selected': selectedKnob === KnobType.start,
            }), onKeyDown: handleKeyDown, onMouseDown: ev => {
                if (selectedKnob === KnobType.start) {
                    handleMouseDown(ev);
                }
            }, onClick: () => {
                onClick(KnobType.start);
            }, role: "button", style: Object.assign(Object.assign({}, knob1Style), knobDim.start), tabIndex: 0 })),
        knobDim.end && (react_1.default.createElement("div", { "aria-label": "1", className: (0, classnames_1.default)('GradientDial__knob', {
                'GradientDial__knob--selected': selectedKnob === KnobType.end,
            }), onKeyDown: handleKeyDown, onMouseDown: ev => {
                if (selectedKnob === KnobType.end) {
                    handleMouseDown(ev);
                }
            }, onClick: () => {
                onClick(KnobType.end);
            }, role: "button", style: Object.assign(Object.assign({}, knob2Style), knobDim.end), tabIndex: 0 })),
        knobDim.start && knobDim.end && (react_1.default.createElement("div", { className: "GradientDial__bar--container" },
            react_1.default.createElement("div", { className: "GradientDial__bar--node", style: {
                    transform: `translate(-50%, -50%) rotate(${90 - deg}deg)`,
                } })))));
};
exports.GradientDial = GradientDial;
