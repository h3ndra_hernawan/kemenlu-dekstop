"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const StickerPreviewModal_1 = require("./StickerPreviewModal");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const Fixtures_1 = require("../../storybook/Fixtures");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
(0, react_1.storiesOf)('Components/Stickers/StickerPreviewModal', module).add('Full', () => {
    const title = (0, addon_knobs_1.text)('title', 'Foo');
    const author = (0, addon_knobs_1.text)('author', 'Foo McBarrington');
    const abeSticker = {
        id: -1,
        emoji: '🎩',
        url: Fixtures_1.squareStickerUrl,
        packId: 'abe',
    };
    const wideSticker = {
        id: -2,
        emoji: '🤯',
        url: Fixtures_1.landscapeGreenUrl,
        packId: 'wide',
    };
    const tallSticker = {
        id: -3,
        emoji: '🔥',
        url: Fixtures_1.portraitTealUrl,
        packId: 'tall',
    };
    const pack = {
        id: 'foo',
        key: 'foo',
        lastUsed: Date.now(),
        cover: abeSticker,
        title,
        isBlessed: true,
        author,
        status: 'downloaded',
        stickerCount: 101,
        stickers: [
            wideSticker,
            tallSticker,
            ...Array(101)
                .fill(0)
                .map((_n, id) => (Object.assign(Object.assign({}, abeSticker), { id }))),
        ],
    };
    return (React.createElement(StickerPreviewModal_1.StickerPreviewModal, { onClose: (0, addon_actions_1.action)('onClose'), installStickerPack: (0, addon_actions_1.action)('installStickerPack'), uninstallStickerPack: (0, addon_actions_1.action)('uninstallStickerPack'), downloadStickerPack: (0, addon_actions_1.action)('downloadStickerPack'), i18n: i18n, pack: pack }));
});
