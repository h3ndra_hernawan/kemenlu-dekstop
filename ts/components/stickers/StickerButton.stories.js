"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const StickerButton_1 = require("./StickerButton");
const StickerPicker_stories_1 = require("./StickerPicker.stories");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Stickers/StickerButton', module);
story.addDecorator(storyFn => (React.createElement("div", { style: {
        height: '500px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    } }, storyFn())));
const receivedPacks = [
    (0, StickerPicker_stories_1.createPack)({ id: 'received-pack-1', status: 'downloaded' }, StickerPicker_stories_1.sticker1),
    (0, StickerPicker_stories_1.createPack)({ id: 'received-pack-2', status: 'downloaded' }, StickerPicker_stories_1.sticker2),
];
const installedPacks = [
    (0, StickerPicker_stories_1.createPack)({ id: 'installed-pack-1', status: 'installed' }, StickerPicker_stories_1.sticker1),
    (0, StickerPicker_stories_1.createPack)({ id: 'installed-pack-2', status: 'installed' }, StickerPicker_stories_1.sticker2),
];
const blessedPacks = [
    (0, StickerPicker_stories_1.createPack)({ id: 'blessed-pack-1', status: 'downloaded', isBlessed: true }, StickerPicker_stories_1.sticker1),
    (0, StickerPicker_stories_1.createPack)({ id: 'blessed-pack-2', status: 'downloaded', isBlessed: true }, StickerPicker_stories_1.sticker2),
];
const knownPacks = [
    (0, StickerPicker_stories_1.createPack)({ id: 'known-pack-1', status: 'known' }, StickerPicker_stories_1.sticker1),
    (0, StickerPicker_stories_1.createPack)({ id: 'known-pack-2', status: 'known' }, StickerPicker_stories_1.sticker2),
];
const createProps = (overrideProps = {}) => ({
    blessedPacks: overrideProps.blessedPacks || [],
    clearInstalledStickerPack: (0, addon_actions_1.action)('clearInstalledStickerPack'),
    clearShowIntroduction: (0, addon_actions_1.action)('clearShowIntroduction'),
    clearShowPickerHint: (0, addon_actions_1.action)('clearShowPickerHint'),
    i18n,
    installedPack: overrideProps.installedPack,
    installedPacks: overrideProps.installedPacks || [],
    knownPacks: overrideProps.knownPacks || [],
    onClickAddPack: (0, addon_actions_1.action)('onClickAddPack'),
    onPickSticker: (0, addon_actions_1.action)('onPickSticker'),
    receivedPacks: overrideProps.receivedPacks || [],
    recentStickers: [],
    showIntroduction: (0, addon_knobs_1.boolean)('showIntroduction', overrideProps.showIntroduction || false),
    showPickerHint: (0, addon_knobs_1.boolean)('showPickerHint', false),
});
story.add('Only Installed', () => {
    const props = createProps({ installedPacks });
    return React.createElement(StickerButton_1.StickerButton, Object.assign({}, props));
});
story.add('Only Received', () => {
    const props = createProps({ receivedPacks });
    return React.createElement(StickerButton_1.StickerButton, Object.assign({}, props));
});
story.add('Only Known', () => {
    const props = createProps({ knownPacks });
    return React.createElement(StickerButton_1.StickerButton, Object.assign({}, props));
});
story.add('Only Blessed', () => {
    const props = createProps({ blessedPacks });
    return React.createElement(StickerButton_1.StickerButton, Object.assign({}, props));
});
story.add('No Packs', () => {
    const props = createProps();
    return React.createElement(StickerButton_1.StickerButton, Object.assign({}, props));
});
story.add('Installed Pack Tooltip', () => {
    const props = createProps({
        installedPacks,
        installedPack: installedPacks[0],
    });
    return React.createElement(StickerButton_1.StickerButton, Object.assign({}, props));
});
story.add('Installed Pack Tooltip (Wide)', () => {
    const installedPack = (0, StickerPicker_stories_1.createPack)({ id: 'installed-pack-wide' }, StickerPicker_stories_1.wideSticker);
    const props = createProps({
        installedPacks: [installedPack],
        installedPack,
    });
    return React.createElement(StickerButton_1.StickerButton, Object.assign({}, props));
});
story.add('Installed Pack Tooltip (Tall)', () => {
    const installedPack = (0, StickerPicker_stories_1.createPack)({ id: 'installed-pack-tall' }, StickerPicker_stories_1.tallSticker);
    const props = createProps({
        installedPacks: [installedPack],
        installedPack,
    });
    return React.createElement(StickerButton_1.StickerButton, Object.assign({}, props));
});
story.add('New Install Tooltip', () => {
    const props = createProps({
        installedPacks,
        showIntroduction: true,
    });
    return React.createElement(StickerButton_1.StickerButton, Object.assign({}, props));
});
