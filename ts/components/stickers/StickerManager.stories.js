"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const StickerManager_1 = require("./StickerManager");
const StickerPicker_stories_1 = require("./StickerPicker.stories");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Stickers/StickerManager', module);
const receivedPacks = [
    (0, StickerPicker_stories_1.createPack)({ id: 'received-pack-1', status: 'downloaded' }, StickerPicker_stories_1.sticker1),
    (0, StickerPicker_stories_1.createPack)({ id: 'received-pack-2', status: 'downloaded' }, StickerPicker_stories_1.sticker2),
];
const installedPacks = [
    (0, StickerPicker_stories_1.createPack)({ id: 'installed-pack-1', status: 'installed' }, StickerPicker_stories_1.sticker1),
    (0, StickerPicker_stories_1.createPack)({ id: 'installed-pack-2', status: 'installed' }, StickerPicker_stories_1.sticker2),
];
const blessedPacks = [
    (0, StickerPicker_stories_1.createPack)({ id: 'blessed-pack-1', status: 'downloaded', isBlessed: true }, StickerPicker_stories_1.sticker1),
    (0, StickerPicker_stories_1.createPack)({ id: 'blessed-pack-2', status: 'downloaded', isBlessed: true }, StickerPicker_stories_1.sticker2),
];
const knownPacks = [
    (0, StickerPicker_stories_1.createPack)({ id: 'known-pack-1', status: 'known' }, StickerPicker_stories_1.sticker1),
    (0, StickerPicker_stories_1.createPack)({ id: 'known-pack-2', status: 'known' }, StickerPicker_stories_1.sticker2),
];
const createProps = (overrideProps = {}) => ({
    blessedPacks: overrideProps.blessedPacks || [],
    downloadStickerPack: (0, addon_actions_1.action)('downloadStickerPack'),
    i18n,
    installStickerPack: (0, addon_actions_1.action)('installStickerPack'),
    installedPacks: overrideProps.installedPacks || [],
    knownPacks: overrideProps.knownPacks || [],
    receivedPacks: overrideProps.receivedPacks || [],
    uninstallStickerPack: (0, addon_actions_1.action)('uninstallStickerPack'),
});
story.add('Full', () => {
    const props = createProps({ installedPacks, receivedPacks, blessedPacks });
    return React.createElement(StickerManager_1.StickerManager, Object.assign({}, props));
});
story.add('Installed Packs', () => {
    const props = createProps({ installedPacks });
    return React.createElement(StickerManager_1.StickerManager, Object.assign({}, props));
});
story.add('Received Packs', () => {
    const props = createProps({ receivedPacks });
    return React.createElement(StickerManager_1.StickerManager, Object.assign({}, props));
});
story.add('Installed + Known Packs', () => {
    const props = createProps({ installedPacks, knownPacks });
    return React.createElement(StickerManager_1.StickerManager, Object.assign({}, props));
});
story.add('Empty', () => {
    const props = createProps();
    return React.createElement(StickerManager_1.StickerManager, Object.assign({}, props));
});
