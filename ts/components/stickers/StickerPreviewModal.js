"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StickerPreviewModal = void 0;
const React = __importStar(require("react"));
const react_dom_1 = require("react-dom");
const lodash_1 = require("lodash");
const classnames_1 = __importDefault(require("classnames"));
const StickerPackInstallButton_1 = require("./StickerPackInstallButton");
const ConfirmationDialog_1 = require("../ConfirmationDialog");
const Spinner_1 = require("../Spinner");
const useRestoreFocus_1 = require("../../hooks/useRestoreFocus");
function renderBody({ pack, i18n }) {
    if (pack && pack.status === 'error') {
        return (React.createElement("div", { className: "module-sticker-manager__preview-modal__container__error" }, i18n('stickers--StickerPreview--Error')));
    }
    if (!pack || pack.stickerCount === 0 || !(0, lodash_1.isNumber)(pack.stickerCount)) {
        return React.createElement(Spinner_1.Spinner, { svgSize: "normal" });
    }
    return (React.createElement("div", { className: "module-sticker-manager__preview-modal__container__sticker-grid" },
        pack.stickers.map(({ id, url }) => (React.createElement("div", { key: id, className: "module-sticker-manager__preview-modal__container__sticker-grid__cell" },
            React.createElement("img", { className: "module-sticker-manager__preview-modal__container__sticker-grid__cell__image", src: url, alt: pack.title })))),
        (0, lodash_1.range)(pack.stickerCount - pack.stickers.length).map(i => (React.createElement("div", { key: `placeholder-${i}`, className: (0, classnames_1.default)('module-sticker-manager__preview-modal__container__sticker-grid__cell', 'module-sticker-manager__preview-modal__container__sticker-grid__cell--placeholder') })))));
}
exports.StickerPreviewModal = React.memo((props) => {
    const { onClose, pack, i18n, downloadStickerPack, installStickerPack, uninstallStickerPack, } = props;
    const [root, setRoot] = React.useState(null);
    const [confirmingUninstall, setConfirmingUninstall] = React.useState(false);
    // Restore focus on teardown
    const [focusRef] = (0, useRestoreFocus_1.useRestoreFocus)();
    React.useEffect(() => {
        const div = document.createElement('div');
        document.body.appendChild(div);
        setRoot(div);
        return () => {
            document.body.removeChild(div);
        };
    }, []);
    React.useEffect(() => {
        if (pack && pack.status === 'known') {
            downloadStickerPack(pack.id, pack.key);
        }
        if (pack &&
            pack.status === 'error' &&
            (pack.attemptedStatus === 'downloaded' ||
                pack.attemptedStatus === 'installed')) {
            downloadStickerPack(pack.id, pack.key, {
                finalStatus: pack.attemptedStatus,
            });
        }
        // We only want to attempt downloads on initial load
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const isInstalled = Boolean(pack && pack.status === 'installed');
    const handleToggleInstall = React.useCallback(() => {
        if (!pack) {
            return;
        }
        if (isInstalled) {
            setConfirmingUninstall(true);
        }
        else if (pack.status === 'ephemeral') {
            downloadStickerPack(pack.id, pack.key, { finalStatus: 'installed' });
            onClose();
        }
        else {
            installStickerPack(pack.id, pack.key);
            onClose();
        }
    }, [
        downloadStickerPack,
        installStickerPack,
        isInstalled,
        onClose,
        pack,
        setConfirmingUninstall,
    ]);
    const handleUninstall = React.useCallback(() => {
        if (!pack) {
            return;
        }
        uninstallStickerPack(pack.id, pack.key);
        setConfirmingUninstall(false);
        // onClose is called by <ConfirmationDialog />
    }, [uninstallStickerPack, setConfirmingUninstall, pack]);
    React.useEffect(() => {
        const handler = ({ key }) => {
            if (key === 'Escape') {
                onClose();
            }
        };
        document.addEventListener('keydown', handler);
        return () => {
            document.removeEventListener('keydown', handler);
        };
    }, [onClose]);
    const handleClickToClose = React.useCallback((e) => {
        if (e.target === e.currentTarget) {
            onClose();
        }
    }, [onClose]);
    return root
        ? (0, react_dom_1.createPortal)(
        // Not really a button. Just a background which can be clicked to close modal
        // eslint-disable-next-line max-len
        // eslint-disable-next-line jsx-a11y/interactive-supports-focus, jsx-a11y/click-events-have-key-events
        React.createElement("div", { role: "button", className: "module-sticker-manager__preview-modal__overlay", onClick: handleClickToClose }, confirmingUninstall ? (React.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onClose: onClose, actions: [
                {
                    style: 'negative',
                    text: i18n('stickers--StickerManager--Uninstall'),
                    action: handleUninstall,
                },
            ] }, i18n('stickers--StickerManager--UninstallWarning'))) : (React.createElement("div", { className: "module-sticker-manager__preview-modal__container" },
            React.createElement("header", { className: "module-sticker-manager__preview-modal__container__header" },
                React.createElement("h2", { className: "module-sticker-manager__preview-modal__container__header__text" }, i18n('stickers--StickerPreview--Title')),
                React.createElement("button", { type: "button", onClick: onClose, className: "module-sticker-manager__preview-modal__container__header__close-button", "aria-label": i18n('close') })),
            renderBody(props),
            pack && pack.status !== 'error' ? (React.createElement("div", { className: "module-sticker-manager__preview-modal__container__meta-overlay" },
                React.createElement("div", { className: "module-sticker-manager__preview-modal__container__meta-overlay__info" },
                    React.createElement("h3", { className: "module-sticker-manager__preview-modal__container__meta-overlay__info__title" },
                        pack.title,
                        pack.isBlessed ? (React.createElement("span", { className: "module-sticker-manager__preview-modal__container__meta-overlay__info__blessed-icon" })) : null),
                    React.createElement("h4", { className: "module-sticker-manager__preview-modal__container__meta-overlay__info__author" }, pack.author)),
                React.createElement("div", { className: "module-sticker-manager__preview-modal__container__meta-overlay__install" }, pack.status === 'pending' ? (React.createElement(Spinner_1.Spinner, { svgSize: "small", size: "14px" })) : (React.createElement(StickerPackInstallButton_1.StickerPackInstallButton, { ref: focusRef, installed: isInstalled, i18n: i18n, onClick: handleToggleInstall, blue: true }))))) : null))), root)
        : null;
});
