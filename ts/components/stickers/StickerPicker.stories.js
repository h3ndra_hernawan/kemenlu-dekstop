"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createPack = exports.tallSticker = exports.wideSticker = exports.abeSticker = exports.sticker3 = exports.sticker2 = exports.sticker1 = void 0;
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const StickerPicker_1 = require("./StickerPicker");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Stickers/StickerPicker', module);
exports.sticker1 = {
    id: 1,
    url: '/fixtures/kitten-1-64-64.jpg',
    packId: 'foo',
    emoji: '',
};
exports.sticker2 = {
    id: 2,
    url: '/fixtures/kitten-2-64-64.jpg',
    packId: 'bar',
    emoji: '',
};
exports.sticker3 = {
    id: 3,
    url: '/fixtures/kitten-3-64-64.jpg',
    packId: 'baz',
    emoji: '',
};
exports.abeSticker = {
    id: 4,
    url: '/fixtures/512x515-thumbs-up-lincoln.webp',
    packId: 'abe',
    emoji: '',
};
exports.wideSticker = {
    id: 5,
    url: '/fixtures/1000x50-green.jpeg',
    packId: 'wide',
    emoji: '',
};
exports.tallSticker = {
    id: 6,
    url: '/fixtures/50x1000-teal.jpeg',
    packId: 'tall',
    emoji: '',
};
const choosableStickers = [exports.sticker1, exports.sticker2, exports.sticker3, exports.abeSticker];
const createPack = (props, sticker) => (Object.assign({ id: '', title: props.id ? `${props.id} title` : 'title', key: '', author: '', isBlessed: false, lastUsed: 0, status: 'known', cover: sticker, stickerCount: 101, stickers: sticker
        ? Array(101)
            .fill(0)
            .map((_, id) => (Object.assign(Object.assign({}, sticker), { id })))
        : [] }, props));
exports.createPack = createPack;
const packs = [
    (0, exports.createPack)({ id: 'tall' }, exports.tallSticker),
    (0, exports.createPack)({ id: 'wide' }, exports.wideSticker),
    ...Array(20)
        .fill(0)
        .map((_, n) => (0, exports.createPack)({ id: `pack-${n}` }, (0, lodash_1.sample)(choosableStickers))),
];
const recentStickers = [
    exports.abeSticker,
    exports.sticker1,
    exports.sticker2,
    exports.sticker3,
    exports.tallSticker,
    exports.wideSticker,
    Object.assign(Object.assign({}, exports.sticker2), { id: 9999 }),
];
const createProps = (overrideProps = {}) => ({
    i18n,
    onClickAddPack: (0, addon_actions_1.action)('onClickAddPack'),
    onClose: (0, addon_actions_1.action)('onClose'),
    onPickSticker: (0, addon_actions_1.action)('onPickSticker'),
    packs: overrideProps.packs || [],
    recentStickers: overrideProps.recentStickers || [],
    showPickerHint: (0, addon_knobs_1.boolean)('showPickerHint', overrideProps.showPickerHint || false),
});
story.add('Full', () => {
    const props = createProps({ packs, recentStickers });
    return React.createElement(StickerPicker_1.StickerPicker, Object.assign({}, props));
});
story.add('Picker Hint', () => {
    const props = createProps({ packs, recentStickers, showPickerHint: true });
    return React.createElement(StickerPicker_1.StickerPicker, Object.assign({}, props));
});
story.add('No Recent Stickers', () => {
    const props = createProps({ packs });
    return React.createElement(StickerPicker_1.StickerPicker, Object.assign({}, props));
});
story.add('Empty', () => {
    const props = createProps();
    return React.createElement(StickerPicker_1.StickerPicker, Object.assign({}, props));
});
story.add('Pending Download', () => {
    const pack = (0, exports.createPack)({ status: 'pending', stickers: [exports.abeSticker] }, exports.abeSticker);
    const props = createProps({ packs: [pack] });
    return React.createElement(StickerPicker_1.StickerPicker, Object.assign({}, props));
});
story.add('Error', () => {
    const pack = (0, exports.createPack)({ status: 'error', stickers: [exports.abeSticker] }, exports.abeSticker);
    const props = createProps({ packs: [pack] });
    return React.createElement(StickerPicker_1.StickerPicker, Object.assign({}, props));
});
story.add('No Cover', () => {
    const pack = (0, exports.createPack)({ status: 'error', stickers: [exports.abeSticker] });
    const props = createProps({ packs: [pack] });
    return React.createElement(StickerPicker_1.StickerPicker, Object.assign({}, props));
});
