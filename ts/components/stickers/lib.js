"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.countStickers = void 0;
// This function exists to force stickers to be counted consistently wherever
// they are counted (TypeScript ensures that all data is named and provided)
function countStickers(o) {
    return (o.knownPacks.length +
        o.blessedPacks.length +
        o.installedPacks.length +
        o.receivedPacks.length);
}
exports.countStickers = countStickers;
