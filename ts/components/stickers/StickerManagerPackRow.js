"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StickerManagerPackRow = void 0;
const React = __importStar(require("react"));
const StickerPackInstallButton_1 = require("./StickerPackInstallButton");
const ConfirmationDialog_1 = require("../ConfirmationDialog");
exports.StickerManagerPackRow = React.memo(({ installStickerPack, uninstallStickerPack, onClickPreview, pack, i18n, }) => {
    const { id, key, isBlessed } = pack;
    const [uninstalling, setUninstalling] = React.useState(false);
    const clearUninstalling = React.useCallback(() => {
        setUninstalling(false);
    }, [setUninstalling]);
    const handleInstall = React.useCallback((e) => {
        e.stopPropagation();
        if (installStickerPack) {
            installStickerPack(id, key);
        }
    }, [id, installStickerPack, key]);
    const handleUninstall = React.useCallback((e) => {
        e.stopPropagation();
        if (isBlessed && uninstallStickerPack) {
            uninstallStickerPack(id, key);
        }
        else {
            setUninstalling(true);
        }
    }, [id, isBlessed, key, setUninstalling, uninstallStickerPack]);
    const handleConfirmUninstall = React.useCallback(() => {
        clearUninstalling();
        if (uninstallStickerPack) {
            uninstallStickerPack(id, key);
        }
    }, [id, key, clearUninstalling, uninstallStickerPack]);
    const handleKeyDown = React.useCallback((event) => {
        if (onClickPreview &&
            (event.key === 'Enter' || event.key === 'Space')) {
            event.stopPropagation();
            event.preventDefault();
            onClickPreview(pack);
        }
    }, [onClickPreview, pack]);
    const handleClickPreview = React.useCallback((event) => {
        if (onClickPreview) {
            event.stopPropagation();
            event.preventDefault();
            onClickPreview(pack);
        }
    }, [onClickPreview, pack]);
    return (React.createElement(React.Fragment, null,
        uninstalling ? (React.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onClose: clearUninstalling, actions: [
                {
                    style: 'negative',
                    text: i18n('stickers--StickerManager--Uninstall'),
                    action: handleConfirmUninstall,
                },
            ] }, i18n('stickers--StickerManager--UninstallWarning'))) : null,
        React.createElement("div", { tabIndex: 0, 
            // This can't be a button because we have buttons as descendants
            role: "button", onKeyDown: handleKeyDown, onClick: handleClickPreview, className: "module-sticker-manager__pack-row" },
            pack.cover ? (React.createElement("img", { src: pack.cover.url, alt: pack.title, className: "module-sticker-manager__pack-row__cover" })) : (React.createElement("div", { className: "module-sticker-manager__pack-row__cover-placeholder" })),
            React.createElement("div", { className: "module-sticker-manager__pack-row__meta" },
                React.createElement("div", { className: "module-sticker-manager__pack-row__meta__title" },
                    pack.title,
                    pack.isBlessed ? (React.createElement("span", { className: "module-sticker-manager__pack-row__meta__blessed-icon" })) : null),
                React.createElement("div", { className: "module-sticker-manager__pack-row__meta__author" }, pack.author)),
            React.createElement("div", { className: "module-sticker-manager__pack-row__controls" }, pack.status === 'installed' ? (React.createElement(StickerPackInstallButton_1.StickerPackInstallButton, { installed: true, i18n: i18n, onClick: handleUninstall })) : (React.createElement(StickerPackInstallButton_1.StickerPackInstallButton, { installed: false, i18n: i18n, onClick: handleInstall }))))));
});
