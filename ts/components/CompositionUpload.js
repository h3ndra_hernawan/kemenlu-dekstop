"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompositionUpload = void 0;
const react_1 = __importStar(require("react"));
const AttachmentToastType_1 = require("../types/AttachmentToastType");
const ToastCannotMixImageAndNonImageAttachments_1 = require("./ToastCannotMixImageAndNonImageAttachments");
const ToastDangerousFileType_1 = require("./ToastDangerousFileType");
const ToastFileSize_1 = require("./ToastFileSize");
const ToastMaxAttachments_1 = require("./ToastMaxAttachments");
const ToastOneNonImageAtATime_1 = require("./ToastOneNonImageAtATime");
const ToastUnableToLoadAttachment_1 = require("./ToastUnableToLoadAttachment");
exports.CompositionUpload = (0, react_1.forwardRef)(({ addAttachment, addPendingAttachment, conversationId, draftAttachments, i18n, processAttachments, removeAttachment, }, ref) => {
    const [toastType, setToastType] = (0, react_1.useState)();
    const onFileInputChange = async (event) => {
        const files = event.target.files || [];
        await processAttachments({
            addAttachment,
            addPendingAttachment,
            conversationId,
            files: Array.from(files),
            draftAttachments,
            onShowToast: setToastType,
            removeAttachment,
        });
    };
    function closeToast() {
        setToastType(undefined);
    }
    let toast;
    if (toastType === AttachmentToastType_1.AttachmentToastType.ToastFileSize) {
        toast = (react_1.default.createElement(ToastFileSize_1.ToastFileSize, { i18n: i18n, limit: 100, onClose: closeToast, units: "MB" }));
    }
    else if (toastType === AttachmentToastType_1.AttachmentToastType.ToastDangerousFileType) {
        toast = react_1.default.createElement(ToastDangerousFileType_1.ToastDangerousFileType, { i18n: i18n, onClose: closeToast });
    }
    else if (toastType === AttachmentToastType_1.AttachmentToastType.ToastMaxAttachments) {
        toast = react_1.default.createElement(ToastMaxAttachments_1.ToastMaxAttachments, { i18n: i18n, onClose: closeToast });
    }
    else if (toastType === AttachmentToastType_1.AttachmentToastType.ToastOneNonImageAtATime) {
        toast = react_1.default.createElement(ToastOneNonImageAtATime_1.ToastOneNonImageAtATime, { i18n: i18n, onClose: closeToast });
    }
    else if (toastType ===
        AttachmentToastType_1.AttachmentToastType.ToastCannotMixImageAndNonImageAttachments) {
        toast = (react_1.default.createElement(ToastCannotMixImageAndNonImageAttachments_1.ToastCannotMixImageAndNonImageAttachments, { i18n: i18n, onClose: closeToast }));
    }
    else if (toastType === AttachmentToastType_1.AttachmentToastType.ToastUnableToLoadAttachment) {
        toast = react_1.default.createElement(ToastUnableToLoadAttachment_1.ToastUnableToLoadAttachment, { i18n: i18n, onClose: closeToast });
    }
    return (react_1.default.createElement(react_1.default.Fragment, null,
        toast,
        react_1.default.createElement("input", { hidden: true, multiple: true, onChange: onFileInputChange, ref: ref, type: "file" })));
});
