"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IdenticonSVG = void 0;
const react_1 = __importDefault(require("react"));
function IdenticonSVG({ backgroundColor, content, foregroundColor, }) {
    return (react_1.default.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: "100", height: "100" },
        react_1.default.createElement("circle", { cx: "50", cy: "50", r: "40", fill: backgroundColor }),
        react_1.default.createElement("text", { baselineShift: "-8px", fill: foregroundColor, fontFamily: "sans-serif", fontSize: "24px", textAnchor: "middle", x: "50", y: "50" }, content)));
}
exports.IdenticonSVG = IdenticonSVG;
