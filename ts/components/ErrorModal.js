"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorModal = void 0;
const React = __importStar(require("react"));
const Modal_1 = require("./Modal");
const Button_1 = require("./Button");
function focusRef(el) {
    if (el) {
        el.focus();
    }
}
const ErrorModal = (props) => {
    const { buttonText, description, i18n, onClose, title } = props;
    return (React.createElement(Modal_1.Modal, { i18n: i18n, onClose: onClose, title: title || i18n('ErrorModal--title') },
        React.createElement(React.Fragment, null,
            React.createElement("div", { className: "module-error-modal__description" }, description || i18n('ErrorModal--description')),
            React.createElement(Modal_1.Modal.ButtonFooter, null,
                React.createElement(Button_1.Button, { onClick: onClose, ref: focusRef, variant: Button_1.ButtonVariant.Secondary }, buttonText || i18n('Confirmation--confirm'))))));
};
exports.ErrorModal = ErrorModal;
