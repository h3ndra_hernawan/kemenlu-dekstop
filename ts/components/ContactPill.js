"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactPill = void 0;
const react_1 = __importDefault(require("react"));
const ContactName_1 = require("./conversation/ContactName");
const Avatar_1 = require("./Avatar");
const ContactPill = ({ acceptedMessageRequest, avatarPath, color, firstName, i18n, isMe, id, name, phoneNumber, profileName, sharedGroupNames, title, unblurredAvatarPath, onClickRemove, }) => {
    const removeLabel = i18n('ContactPill--remove');
    return (react_1.default.createElement("div", { className: "module-ContactPill" },
        react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, color: color, noteToSelf: false, conversationType: "direct", i18n: i18n, isMe: isMe, name: name, phoneNumber: phoneNumber, profileName: profileName, title: title, sharedGroupNames: sharedGroupNames, size: Avatar_1.AvatarSize.TWENTY_EIGHT, unblurredAvatarPath: unblurredAvatarPath }),
        react_1.default.createElement(ContactName_1.ContactName, { firstName: firstName, module: "module-ContactPill__contact-name", preferFirstName: true, title: title }),
        react_1.default.createElement("button", { "aria-label": removeLabel, className: "module-ContactPill__remove", onClick: () => {
                onClickRemove(id);
            }, title: removeLabel, type: "button" })));
};
exports.ContactPill = ContactPill;
