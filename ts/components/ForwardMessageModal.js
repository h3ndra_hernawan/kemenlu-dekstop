"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ForwardMessageModal = void 0;
const react_1 = __importStar(require("react"));
const react_measure_1 = __importDefault(require("react-measure"));
const lodash_1 = require("lodash");
const web_1 = require("@react-spring/web");
const classnames_1 = __importDefault(require("classnames"));
const AttachmentList_1 = require("./conversation/AttachmentList");
const Button_1 = require("./Button");
const CompositionInput_1 = require("./CompositionInput");
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const ContactCheckbox_1 = require("./conversationList/ContactCheckbox");
const ConversationList_1 = require("./ConversationList");
const EmojiButton_1 = require("./emoji/EmojiButton");
const ModalHost_1 = require("./ModalHost");
const SearchInput_1 = require("./SearchInput");
const StagedLinkPreview_1 = require("./conversation/StagedLinkPreview");
const assert_1 = require("../util/assert");
const filterAndSortConversations_1 = require("../util/filterAndSortConversations");
const useAnimated_1 = require("../hooks/useAnimated");
const MAX_FORWARD = 5;
const ForwardMessageModal = ({ attachments, candidateConversations, conversationId, doForwardMessage, i18n, isSticker, linkPreview, messageBody, onClose, onEditorStateChange, onPickEmoji, onSetSkinTone, onTextTooLong, recentEmojis, removeLinkPreview, skinTone, theme, }) => {
    const inputRef = (0, react_1.useRef)(null);
    const inputApiRef = react_1.default.useRef();
    const [selectedContacts, setSelectedContacts] = (0, react_1.useState)([]);
    const [searchTerm, setSearchTerm] = (0, react_1.useState)('');
    const [filteredConversations, setFilteredConversations] = (0, react_1.useState)((0, filterAndSortConversations_1.filterAndSortConversationsByRecent)(candidateConversations, ''));
    const [attachmentsToForward, setAttachmentsToForward] = (0, react_1.useState)(attachments || []);
    const [isEditingMessage, setIsEditingMessage] = (0, react_1.useState)(false);
    const [messageBodyText, setMessageBodyText] = (0, react_1.useState)(messageBody || '');
    const [cannotMessage, setCannotMessage] = (0, react_1.useState)(false);
    const isMessageEditable = !isSticker;
    const hasSelectedMaximumNumberOfContacts = selectedContacts.length >= MAX_FORWARD;
    const selectedConversationIdsSet = (0, react_1.useMemo)(() => new Set(selectedContacts.map(contact => contact.id)), [selectedContacts]);
    const focusTextEditInput = react_1.default.useCallback(() => {
        if (inputApiRef.current) {
            inputApiRef.current.focus();
        }
    }, [inputApiRef]);
    const insertEmoji = react_1.default.useCallback((e) => {
        if (inputApiRef.current) {
            inputApiRef.current.insertEmoji(e);
            onPickEmoji(e);
        }
    }, [inputApiRef, onPickEmoji]);
    const hasContactsSelected = Boolean(selectedContacts.length);
    const canForwardMessage = hasContactsSelected &&
        (Boolean(messageBodyText) ||
            isSticker ||
            (attachmentsToForward && attachmentsToForward.length));
    const forwardMessage = react_1.default.useCallback(() => {
        if (!canForwardMessage) {
            return;
        }
        doForwardMessage(selectedContacts.map(contact => contact.id), messageBodyText, attachmentsToForward, linkPreview);
    }, [
        attachmentsToForward,
        canForwardMessage,
        doForwardMessage,
        linkPreview,
        messageBodyText,
        selectedContacts,
    ]);
    const normalizedSearchTerm = searchTerm.trim();
    (0, react_1.useEffect)(() => {
        const timeout = setTimeout(() => {
            setFilteredConversations((0, filterAndSortConversations_1.filterAndSortConversationsByRecent)(candidateConversations, normalizedSearchTerm));
        }, 200);
        return () => {
            clearTimeout(timeout);
        };
    }, [candidateConversations, normalizedSearchTerm, setFilteredConversations]);
    const contactLookup = (0, react_1.useMemo)(() => {
        const map = new Map();
        candidateConversations.forEach(contact => {
            map.set(contact.id, contact);
        });
        return map;
    }, [candidateConversations]);
    const toggleSelectedConversation = (0, react_1.useCallback)((selectedConversationId) => {
        let removeContact = false;
        const nextSelectedContacts = selectedContacts.filter(contact => {
            if (contact.id === selectedConversationId) {
                removeContact = true;
                return false;
            }
            return true;
        });
        if (removeContact) {
            setSelectedContacts(nextSelectedContacts);
            return;
        }
        const selectedContact = contactLookup.get(selectedConversationId);
        if (selectedContact) {
            if (selectedContact.announcementsOnly && !selectedContact.areWeAdmin) {
                setCannotMessage(true);
            }
            else {
                setSelectedContacts([...nextSelectedContacts, selectedContact]);
            }
        }
    }, [contactLookup, selectedContacts, setSelectedContacts]);
    const { close, modalStyles, overlayStyles } = (0, useAnimated_1.useAnimated)(onClose, {
        getFrom: () => ({ opacity: 0, transform: 'translateY(48px)' }),
        getTo: isOpen => isOpen
            ? { opacity: 1, transform: 'translateY(0px)' }
            : {
                opacity: 0,
                transform: 'translateY(48px)',
            },
    });
    const handleBackOrClose = (0, react_1.useCallback)(() => {
        if (isEditingMessage) {
            setIsEditingMessage(false);
        }
        else {
            close();
        }
    }, [isEditingMessage, close, setIsEditingMessage]);
    const rowCount = filteredConversations.length;
    const getRow = (index) => {
        const contact = filteredConversations[index];
        if (!contact) {
            return undefined;
        }
        const isSelected = selectedConversationIdsSet.has(contact.id);
        let disabledReason;
        if (hasSelectedMaximumNumberOfContacts && !isSelected) {
            disabledReason = ContactCheckbox_1.ContactCheckboxDisabledReason.MaximumContactsSelected;
        }
        return {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact,
            isChecked: isSelected,
            disabledReason,
        };
    };
    (0, react_1.useEffect)(() => {
        const timeout = setTimeout(() => {
            var _a;
            (_a = inputRef.current) === null || _a === void 0 ? void 0 : _a.focus();
        }, 100);
        return () => {
            clearTimeout(timeout);
        };
    }, []);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        cannotMessage && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { cancelText: i18n('Confirmation--confirm'), i18n: i18n, onClose: () => setCannotMessage(false) }, i18n('GroupV2--cannot-send'))),
        react_1.default.createElement(ModalHost_1.ModalHost, { onEscape: handleBackOrClose, onClose: close, overlayStyles: overlayStyles },
            react_1.default.createElement(web_1.animated.div, { className: "module-ForwardMessageModal", style: modalStyles },
                react_1.default.createElement("div", { className: (0, classnames_1.default)('module-ForwardMessageModal__header', {
                        'module-ForwardMessageModal__header--edit': isEditingMessage,
                    }) },
                    isEditingMessage ? (react_1.default.createElement("button", { "aria-label": i18n('back'), className: "module-ForwardMessageModal__header--back", onClick: () => setIsEditingMessage(false), type: "button" }, "\u00A0")) : (react_1.default.createElement("button", { "aria-label": i18n('close'), className: "module-ForwardMessageModal__header--close", onClick: close, type: "button" })),
                    react_1.default.createElement("h1", null, i18n('forwardMessage'))),
                isEditingMessage ? (react_1.default.createElement("div", { className: "module-ForwardMessageModal__main-body" },
                    linkPreview ? (react_1.default.createElement("div", { className: "module-ForwardMessageModal--link-preview" },
                        react_1.default.createElement(StagedLinkPreview_1.StagedLinkPreview, { date: linkPreview.date || null, description: linkPreview.description || '', domain: linkPreview.url, i18n: i18n, image: linkPreview.image, onClose: () => removeLinkPreview(), title: linkPreview.title }))) : null,
                    attachmentsToForward && attachmentsToForward.length ? (react_1.default.createElement(AttachmentList_1.AttachmentList, { attachments: attachmentsToForward, i18n: i18n, onCloseAttachment: (attachment) => {
                            const newAttachments = attachmentsToForward.filter(currentAttachment => currentAttachment !== attachment);
                            setAttachmentsToForward(newAttachments);
                        } })) : null,
                    react_1.default.createElement("div", { className: "module-ForwardMessageModal__text-edit-area" },
                        react_1.default.createElement(CompositionInput_1.CompositionInput, { conversationId: conversationId, clearQuotedMessage: shouldNeverBeCalled, draftText: messageBodyText, getQuotedMessage: lodash_1.noop, i18n: i18n, inputApi: inputApiRef, large: true, moduleClassName: "module-ForwardMessageModal__input", scrollToBottom: lodash_1.noop, onEditorStateChange: (messageText, bodyRanges, caretLocation) => {
                                setMessageBodyText(messageText);
                                onEditorStateChange(messageText, bodyRanges, caretLocation);
                            }, onPickEmoji: onPickEmoji, onSubmit: forwardMessage, onTextTooLong: onTextTooLong }),
                        react_1.default.createElement("div", { className: "module-ForwardMessageModal__emoji" },
                            react_1.default.createElement(EmojiButton_1.EmojiButton, { i18n: i18n, onClose: focusTextEditInput, onPickEmoji: insertEmoji, onSetSkinTone: onSetSkinTone, recentEmojis: recentEmojis, skinTone: skinTone }))))) : (react_1.default.createElement("div", { className: "module-ForwardMessageModal__main-body" },
                    react_1.default.createElement(SearchInput_1.SearchInput, { disabled: candidateConversations.length === 0, placeholder: i18n('contactSearchPlaceholder'), onChange: event => {
                            setSearchTerm(event.target.value);
                        }, ref: inputRef, value: searchTerm }),
                    candidateConversations.length ? (react_1.default.createElement(react_measure_1.default, { bounds: true }, ({ contentRect, measureRef }) => {
                        // We disable this ESLint rule because we're capturing a bubbled
                        // keydown event. See [this note in the jsx-a11y docs][0].
                        //
                        // [0]: https://github.com/jsx-eslint/eslint-plugin-jsx-a11y/blob/c275964f52c35775208bd00cb612c6f82e42e34f/docs/rules/no-static-element-interactions.md#case-the-event-handler-is-only-being-used-to-capture-bubbled-events
                        /* eslint-disable jsx-a11y/no-static-element-interactions */
                        return (react_1.default.createElement("div", { className: "module-ForwardMessageModal__list-wrapper", ref: measureRef },
                            react_1.default.createElement(ConversationList_1.ConversationList, { dimensions: contentRect.bounds, getRow: getRow, i18n: i18n, onClickArchiveButton: shouldNeverBeCalled, onClickContactCheckbox: (selectedConversationId, disabledReason) => {
                                    if (disabledReason !==
                                        ContactCheckbox_1.ContactCheckboxDisabledReason.MaximumContactsSelected) {
                                        toggleSelectedConversation(selectedConversationId);
                                    }
                                }, onSelectConversation: shouldNeverBeCalled, renderMessageSearchResult: () => {
                                    shouldNeverBeCalled();
                                    return react_1.default.createElement("div", null);
                                }, rowCount: rowCount, shouldRecomputeRowHeights: false, showChooseGroupMembers: shouldNeverBeCalled, startNewConversationFromPhoneNumber: shouldNeverBeCalled, startNewConversationFromUsername: shouldNeverBeCalled, theme: theme })));
                        /* eslint-enable jsx-a11y/no-static-element-interactions */
                    })) : (react_1.default.createElement("div", { className: "module-ForwardMessageModal__no-candidate-contacts" }, i18n('noContactsFound'))))),
                react_1.default.createElement("div", { className: "module-ForwardMessageModal__footer" },
                    react_1.default.createElement("div", null, Boolean(selectedContacts.length) &&
                        selectedContacts.map(contact => contact.title).join(', ')),
                    react_1.default.createElement("div", null, isEditingMessage || !isMessageEditable ? (react_1.default.createElement(Button_1.Button, { "aria-label": i18n('ForwardMessageModal--continue'), className: "module-ForwardMessageModal__send-button module-ForwardMessageModal__send-button--forward", disabled: !canForwardMessage, onClick: forwardMessage })) : (react_1.default.createElement(Button_1.Button, { "aria-label": i18n('forwardMessage'), className: "module-ForwardMessageModal__send-button module-ForwardMessageModal__send-button--continue", disabled: !hasContactsSelected, onClick: () => setIsEditingMessage(true) }))))))));
};
exports.ForwardMessageModal = ForwardMessageModal;
function shouldNeverBeCalled(..._args) {
    (0, assert_1.assert)(false, 'This should never be called. Doing nothing');
}
