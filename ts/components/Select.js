"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Select = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
exports.Select = react_1.default.forwardRef(({ disabled, moduleClassName, name, onChange, options, value }, ref) => {
    const onSelectChange = (event) => {
        onChange(event.target.value);
    };
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(['module-select', moduleClassName]) },
        react_1.default.createElement("select", { disabled: disabled, name: name, value: value, onChange: onSelectChange, ref: ref }, options.map(({ disabled: optionDisabled, text, value: optionValue }) => {
            return (react_1.default.createElement("option", { disabled: optionDisabled, value: optionValue, key: optionValue, "aria-label": text }, text));
        }))));
});
