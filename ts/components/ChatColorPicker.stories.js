"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const ChatColorPicker_1 = require("./ChatColorPicker");
const Colors_1 = require("../types/Colors");
const setupI18n_1 = require("../util/setupI18n");
const story = (0, react_2.storiesOf)('Components/ChatColorPicker', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const SAMPLE_CUSTOM_COLOR = {
    deg: 90,
    end: { hue: 197, saturation: 100 },
    start: { hue: 315, saturation: 78 },
};
const createProps = () => ({
    addCustomColor: (0, addon_actions_1.action)('addCustomColor'),
    colorSelected: (0, addon_actions_1.action)('colorSelected'),
    editCustomColor: (0, addon_actions_1.action)('editCustomColor'),
    getConversationsWithCustomColor: (_) => Promise.resolve([]),
    i18n,
    removeCustomColor: (0, addon_actions_1.action)('removeCustomColor'),
    removeCustomColorOnConversations: (0, addon_actions_1.action)('removeCustomColorOnConversations'),
    resetAllChatColors: (0, addon_actions_1.action)('resetAllChatColors'),
    resetDefaultChatColor: (0, addon_actions_1.action)('resetDefaultChatColor'),
    selectedColor: (0, addon_knobs_1.select)('selectedColor', Colors_1.ConversationColors, 'basil'),
    selectedCustomColor: {},
    setGlobalDefaultConversationColor: (0, addon_actions_1.action)('setGlobalDefaultConversationColor'),
});
story.add('Default', () => react_1.default.createElement(ChatColorPicker_1.ChatColorPicker, Object.assign({}, createProps())));
const CUSTOM_COLORS = {
    abc: {
        start: { hue: 32, saturation: 100 },
    },
    def: {
        deg: 90,
        start: { hue: 180, saturation: 100 },
        end: { hue: 0, saturation: 100 },
    },
    ghi: SAMPLE_CUSTOM_COLOR,
    jkl: {
        deg: 90,
        start: { hue: 161, saturation: 52 },
        end: { hue: 153, saturation: 89 },
    },
};
story.add('Custom Colors', () => (react_1.default.createElement(ChatColorPicker_1.ChatColorPicker, Object.assign({}, createProps(), { customColors: CUSTOM_COLORS, selectedColor: "custom", selectedCustomColor: {
        id: 'ghi',
        value: SAMPLE_CUSTOM_COLOR,
    } }))));
