"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const Colors_1 = require("../types/Colors");
const AvatarEditor_1 = require("./AvatarEditor");
const Avatar_1 = require("../types/Avatar");
const createAvatarData_1 = require("../util/createAvatarData");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    avatarColor: overrideProps.avatarColor || Colors_1.AvatarColors[9],
    avatarPath: overrideProps.avatarPath,
    conversationId: '123',
    conversationTitle: overrideProps.conversationTitle || 'Default Title',
    deleteAvatarFromDisk: (0, addon_actions_1.action)('deleteAvatarFromDisk'),
    i18n,
    isGroup: Boolean(overrideProps.isGroup),
    onCancel: (0, addon_actions_1.action)('onCancel'),
    onSave: (0, addon_actions_1.action)('onSave'),
    replaceAvatar: (0, addon_actions_1.action)('replaceAvatar'),
    saveAvatarToDisk: (0, addon_actions_1.action)('saveAvatarToDisk'),
    userAvatarData: overrideProps.userAvatarData || [
        (0, createAvatarData_1.createAvatarData)({
            imagePath: '/fixtures/kitten-3-64-64.jpg',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A110',
            text: 'YA',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A120',
            text: 'OK',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A130',
            text: 'F',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A140',
            text: '🏄💣',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A150',
            text: '😇🙃😆',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A160',
            text: '🦊F💦',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A170',
            text: 'J',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A180',
            text: 'ZAP',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A190',
            text: '🍍P',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A200',
            text: '🌵',
        }),
        (0, createAvatarData_1.createAvatarData)({
            color: 'A210',
            text: 'NAP',
        }),
    ],
});
const story = (0, react_2.storiesOf)('Components/AvatarEditor', module);
story.add('No Avatar (group)', () => (react_1.default.createElement(AvatarEditor_1.AvatarEditor, Object.assign({}, createProps({ isGroup: true, userAvatarData: (0, Avatar_1.getDefaultAvatars)(true) })))));
story.add('No Avatar (me)', () => (react_1.default.createElement(AvatarEditor_1.AvatarEditor, Object.assign({}, createProps({ userAvatarData: (0, Avatar_1.getDefaultAvatars)() })))));
story.add('Has Avatar', () => (react_1.default.createElement(AvatarEditor_1.AvatarEditor, Object.assign({}, createProps({
    avatarPath: '/fixtures/kitten-3-64-64.jpg',
})))));
