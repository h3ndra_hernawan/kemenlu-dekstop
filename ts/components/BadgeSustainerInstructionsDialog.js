"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadgeSustainerInstructionsDialog = void 0;
const react_1 = __importDefault(require("react"));
const Modal_1 = require("./Modal");
function BadgeSustainerInstructionsDialog({ i18n, onClose, }) {
    return (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, moduleClassName: "BadgeSustainerInstructionsDialog", i18n: i18n, onClose: onClose },
        react_1.default.createElement("h1", { className: "BadgeSustainerInstructionsDialog__header" }, i18n('BadgeSustainerInstructions__header')),
        react_1.default.createElement("h2", { className: "BadgeSustainerInstructionsDialog__subheader" }, i18n('BadgeSustainerInstructions__subheader')),
        react_1.default.createElement("ol", { className: "BadgeSustainerInstructionsDialog__instructions" },
            react_1.default.createElement("li", null, i18n('BadgeSustainerInstructions__instructions__1')),
            react_1.default.createElement("li", null, i18n('BadgeSustainerInstructions__instructions__2')),
            react_1.default.createElement("li", null, i18n('BadgeSustainerInstructions__instructions__3')))));
}
exports.BadgeSustainerInstructionsDialog = BadgeSustainerInstructionsDialog;
