"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DisappearingTimerSelect = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const expirationTimer = __importStar(require("../util/expirationTimer"));
const DisappearingTimeDialog_1 = require("./DisappearingTimeDialog");
const Select_1 = require("./Select");
const CSS_MODULE = 'module-disappearing-timer-select';
const DisappearingTimerSelect = (props) => {
    const { i18n, value = 0, onChange } = props;
    const [isModalOpen, setIsModalOpen] = (0, react_1.useState)(false);
    let expirationTimerOptions = expirationTimer.DEFAULT_DURATIONS_IN_SECONDS.map(seconds => {
        const text = expirationTimer.format(i18n, seconds, {
            capitalizeOff: true,
        });
        return {
            value: seconds,
            text,
        };
    });
    const isCustomTimeSelected = !expirationTimer.DEFAULT_DURATIONS_SET.has(value);
    const onSelectChange = (newValue) => {
        const intValue = parseInt(newValue, 10);
        if (intValue === -1) {
            setIsModalOpen(true);
        }
        else {
            onChange(intValue);
        }
    };
    // Custom time...
    expirationTimerOptions = [
        ...expirationTimerOptions,
        {
            value: -1,
            text: i18n(isCustomTimeSelected
                ? 'selectedCustomDisappearingTimeOption'
                : 'customDisappearingTimeOption'),
        },
    ];
    let modalNode = null;
    if (isModalOpen) {
        modalNode = (react_1.default.createElement(DisappearingTimeDialog_1.DisappearingTimeDialog, { i18n: i18n, initialValue: value, onSubmit: newValue => {
                setIsModalOpen(false);
                onChange(newValue);
            }, onClose: () => setIsModalOpen(false) }));
    }
    let info;
    if (isCustomTimeSelected) {
        info = (react_1.default.createElement("div", { className: `${CSS_MODULE}__info` }, expirationTimer.format(i18n, value)));
    }
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(CSS_MODULE, isCustomTimeSelected ? `${CSS_MODULE}--custom-time` : false) },
        react_1.default.createElement(Select_1.Select, { onChange: onSelectChange, value: isCustomTimeSelected ? -1 : value, options: expirationTimerOptions }),
        info,
        modalNode));
};
exports.DisappearingTimerSelect = DisappearingTimerSelect;
