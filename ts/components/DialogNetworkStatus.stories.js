"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const DialogNetworkStatus_1 = require("./DialogNetworkStatus");
const SocketStatus_1 = require("../types/SocketStatus");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const _util_1 = require("./_util");
const FakeLeftPaneContainer_1 = require("../test-both/helpers/FakeLeftPaneContainer");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const defaultProps = {
    containerWidthBreakpoint: _util_1.WidthBreakpoint.Wide,
    hasNetworkDialog: true,
    i18n,
    isOnline: true,
    socketStatus: SocketStatus_1.SocketStatus.CONNECTING,
    manualReconnect: (0, addon_actions_1.action)('manual-reconnect'),
    withinConnectingGracePeriod: false,
    challengeStatus: 'idle',
};
const story = (0, react_1.storiesOf)('Components/DialogNetworkStatus', module);
story.add('Knobs Playground', () => {
    const containerWidthBreakpoint = (0, addon_knobs_1.select)('containerWidthBreakpoint', _util_1.WidthBreakpoint, _util_1.WidthBreakpoint.Wide);
    const hasNetworkDialog = (0, addon_knobs_1.boolean)('hasNetworkDialog', true);
    const isOnline = (0, addon_knobs_1.boolean)('isOnline', true);
    const socketStatus = (0, addon_knobs_1.select)('socketStatus', {
        CONNECTING: SocketStatus_1.SocketStatus.CONNECTING,
        OPEN: SocketStatus_1.SocketStatus.OPEN,
        CLOSING: SocketStatus_1.SocketStatus.CLOSING,
        CLOSED: SocketStatus_1.SocketStatus.CLOSED,
    }, SocketStatus_1.SocketStatus.CONNECTING);
    return (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogNetworkStatus_1.DialogNetworkStatus, Object.assign({}, defaultProps, { containerWidthBreakpoint: containerWidthBreakpoint, hasNetworkDialog: hasNetworkDialog, isOnline: isOnline, socketStatus: socketStatus }))));
});
[
    ['wide', _util_1.WidthBreakpoint.Wide],
    ['narrow', _util_1.WidthBreakpoint.Narrow],
].forEach(([name, containerWidthBreakpoint]) => {
    const defaultPropsForBreakpoint = Object.assign(Object.assign({}, defaultProps), { containerWidthBreakpoint });
    story.add(`Connecting (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogNetworkStatus_1.DialogNetworkStatus, Object.assign({}, defaultPropsForBreakpoint, { socketStatus: SocketStatus_1.SocketStatus.CONNECTING })))));
    story.add(`Closing (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogNetworkStatus_1.DialogNetworkStatus, Object.assign({}, defaultPropsForBreakpoint, { socketStatus: SocketStatus_1.SocketStatus.CLOSING })))));
    story.add(`Closed (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogNetworkStatus_1.DialogNetworkStatus, Object.assign({}, defaultPropsForBreakpoint, { socketStatus: SocketStatus_1.SocketStatus.CLOSED })))));
    story.add(`Offline (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogNetworkStatus_1.DialogNetworkStatus, Object.assign({}, defaultPropsForBreakpoint, { isOnline: false })))));
});
