"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageSearchResult = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const MessageBodyHighlight_1 = require("./MessageBodyHighlight");
const ContactName_1 = require("../conversation/ContactName");
const assert_1 = require("../../util/assert");
const BaseConversationListItem_1 = require("./BaseConversationListItem");
const renderPerson = (i18n, person) => person.isMe ? i18n('you') : react_1.default.createElement(ContactName_1.ContactName, { title: person.title });
// This function exists because bodyRanges tells us the character position
// where the at-mention starts at according to the full body text. The snippet
// we get back is a portion of the text and we don't know where it starts. This
// function will find the relevant bodyRanges that apply to the snippet and
// then update the proper start position of each body range.
function getFilteredBodyRanges(snippet, body, bodyRanges) {
    if (!bodyRanges.length) {
        return [];
    }
    // Find where the snippet starts in the full text
    const stripped = snippet
        .replace(/<<left>>/g, '')
        .replace(/<<right>>/g, '')
        .replace(/^.../, '')
        .replace(/...$/, '');
    const rx = new RegExp((0, lodash_1.escapeRegExp)(stripped));
    const match = rx.exec(body);
    (0, assert_1.assert)(Boolean(match), `No match found for "${snippet}" inside "${body}"`);
    const delta = match ? match.index + snippet.length : 0;
    // Filters out the @mentions that are present inside the snippet
    const filteredBodyRanges = bodyRanges.filter(bodyRange => {
        return bodyRange.start < delta;
    });
    const snippetBodyRanges = [];
    const MENTIONS_REGEX = /\uFFFC/g;
    let bodyRangeMatch = MENTIONS_REGEX.exec(snippet);
    let i = 0;
    // Find the start position within the snippet so these can later be
    // encoded and rendered correctly.
    while (bodyRangeMatch) {
        const bodyRange = filteredBodyRanges[i];
        if (bodyRange) {
            snippetBodyRanges.push(Object.assign(Object.assign({}, bodyRange), { start: bodyRangeMatch.index }));
        }
        else {
            (0, assert_1.assert)(false, `Body range does not exist? Count: ${i}, Length: ${filteredBodyRanges.length}`);
        }
        bodyRangeMatch = MENTIONS_REGEX.exec(snippet);
        i += 1;
    }
    return snippetBodyRanges;
}
exports.MessageSearchResult = react_1.default.memo(function MessageSearchResult({ body, bodyRanges, conversationId, from, i18n, id, openConversationInternal, sentAt, snippet, to, }) {
    const onClickItem = (0, react_1.useCallback)(() => {
        openConversationInternal({ conversationId, messageId: id });
    }, [openConversationInternal, conversationId, id]);
    if (!from || !to) {
        return react_1.default.createElement("div", null);
    }
    const isNoteToSelf = from.isMe && to.isMe;
    let headerName;
    if (isNoteToSelf) {
        headerName = i18n('noteToSelf');
    }
    else {
        // This isn't perfect because (1) it doesn't work with RTL languages (2)
        //   capitalization may be incorrect for some languages, like English.
        headerName = (react_1.default.createElement("span", null,
            renderPerson(i18n, from),
            " ",
            i18n('toJoiner'),
            " ",
            renderPerson(i18n, to)));
    }
    const snippetBodyRanges = getFilteredBodyRanges(snippet, body, bodyRanges);
    const messageText = (react_1.default.createElement(MessageBodyHighlight_1.MessageBodyHighlight, { text: snippet, bodyRanges: snippetBodyRanges, i18n: i18n }));
    return (react_1.default.createElement(BaseConversationListItem_1.BaseConversationListItem, { acceptedMessageRequest: from.acceptedMessageRequest, avatarPath: from.avatarPath, color: from.color, conversationType: "direct", headerDate: sentAt, headerName: headerName, i18n: i18n, id: id, isNoteToSelf: isNoteToSelf, isMe: from.isMe, isSelected: false, messageText: messageText, name: from.name, onClick: onClickItem, phoneNumber: from.phoneNumber, profileName: from.profileName, sharedGroupNames: from.sharedGroupNames, title: from.title, unblurredAvatarPath: from.unblurredAvatarPath }));
});
