"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationListItem = exports.MessageStatuses = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const BaseConversationListItem_1 = require("./BaseConversationListItem");
const MessageBody_1 = require("../conversation/MessageBody");
const ContactName_1 = require("../conversation/ContactName");
const TypingAnimation_1 = require("../conversation/TypingAnimation");
const MESSAGE_STATUS_ICON_CLASS_NAME = `${BaseConversationListItem_1.MESSAGE_TEXT_CLASS_NAME}__status-icon`;
exports.MessageStatuses = [
    'sending',
    'sent',
    'delivered',
    'read',
    'paused',
    'error',
    'partial-sent',
];
exports.ConversationListItem = react_1.default.memo(function ConversationListItem({ acceptedMessageRequest, avatarPath, badge, color, draftPreview, i18n, id, isMe, isSelected, lastMessage, lastUpdated, markedUnread, muteExpiresAt, name, onClick, phoneNumber, profileName, sharedGroupNames, shouldShowDraft, theme, title, type, typingContactId, unblurredAvatarPath, unreadCount, }) {
    const isMuted = Boolean(muteExpiresAt && Date.now() < muteExpiresAt);
    const headerName = (react_1.default.createElement(react_1.default.Fragment, null,
        isMe ? (react_1.default.createElement("span", { className: BaseConversationListItem_1.HEADER_CONTACT_NAME_CLASS_NAME }, i18n('noteToSelf'))) : (react_1.default.createElement(ContactName_1.ContactName, { module: BaseConversationListItem_1.HEADER_CONTACT_NAME_CLASS_NAME, title: title })),
        isMuted && react_1.default.createElement("div", { className: `${BaseConversationListItem_1.HEADER_NAME_CLASS_NAME}__mute-icon` })));
    let messageText = null;
    let messageStatusIcon = null;
    if (!acceptedMessageRequest) {
        messageText = (react_1.default.createElement("span", { className: `${BaseConversationListItem_1.MESSAGE_TEXT_CLASS_NAME}__message-request` }, i18n('ConversationListItem--message-request')));
    }
    else if (typingContactId) {
        messageText = react_1.default.createElement(TypingAnimation_1.TypingAnimation, { i18n: i18n });
    }
    else if (shouldShowDraft && draftPreview) {
        messageText = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("span", { className: `${BaseConversationListItem_1.MESSAGE_TEXT_CLASS_NAME}__draft-prefix` }, i18n('ConversationListItem--draft-prefix')),
            react_1.default.createElement(MessageBody_1.MessageBody, { text: truncateMessageText(draftPreview), disableJumbomoji: true, disableLinks: true, i18n: i18n })));
    }
    else if (lastMessage === null || lastMessage === void 0 ? void 0 : lastMessage.deletedForEveryone) {
        messageText = (react_1.default.createElement("span", { className: `${BaseConversationListItem_1.MESSAGE_TEXT_CLASS_NAME}__deleted-for-everyone` }, i18n('message--deletedForEveryone')));
    }
    else if (lastMessage) {
        messageText = (react_1.default.createElement(MessageBody_1.MessageBody, { text: truncateMessageText(lastMessage.text), disableJumbomoji: true, disableLinks: true, i18n: i18n }));
        if (lastMessage.status) {
            messageStatusIcon = (react_1.default.createElement("div", { className: (0, classnames_1.default)(MESSAGE_STATUS_ICON_CLASS_NAME, `${MESSAGE_STATUS_ICON_CLASS_NAME}--${lastMessage.status}`) }));
        }
    }
    const onClickItem = (0, react_1.useCallback)(() => onClick(id), [onClick, id]);
    return (react_1.default.createElement(BaseConversationListItem_1.BaseConversationListItem, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, badge: badge, color: color, conversationType: type, headerDate: lastUpdated, headerName: headerName, i18n: i18n, id: id, isMe: isMe, isSelected: Boolean(isSelected), markedUnread: markedUnread, messageStatusIcon: messageStatusIcon, messageText: messageText, messageTextIsAlwaysFullSize: true, name: name, onClick: onClickItem, phoneNumber: phoneNumber, profileName: profileName, sharedGroupNames: sharedGroupNames, theme: theme, title: title, unreadCount: unreadCount, unblurredAvatarPath: unblurredAvatarPath }));
});
// This takes `unknown` because, sometimes, values from the database don't match our
//   types. In the long term, we should fix that. In the short term, this smooths over the
//   problem.
function truncateMessageText(text) {
    if (typeof text !== 'string') {
        return '';
    }
    return text.replace(/(?:\r?\n)+/g, ' ');
}
