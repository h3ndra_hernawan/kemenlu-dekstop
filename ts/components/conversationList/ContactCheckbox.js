"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactCheckbox = exports.ContactCheckboxDisabledReason = void 0;
const react_1 = __importDefault(require("react"));
const BaseConversationListItem_1 = require("./BaseConversationListItem");
const ContactName_1 = require("../conversation/ContactName");
const About_1 = require("../conversation/About");
var ContactCheckboxDisabledReason;
(function (ContactCheckboxDisabledReason) {
    // We start the enum at 1 because the default starting value of 0 is falsy.
    ContactCheckboxDisabledReason[ContactCheckboxDisabledReason["AlreadyAdded"] = 1] = "AlreadyAdded";
    ContactCheckboxDisabledReason[ContactCheckboxDisabledReason["MaximumContactsSelected"] = 2] = "MaximumContactsSelected";
    ContactCheckboxDisabledReason[ContactCheckboxDisabledReason["NotCapable"] = 3] = "NotCapable";
})(ContactCheckboxDisabledReason = exports.ContactCheckboxDisabledReason || (exports.ContactCheckboxDisabledReason = {}));
exports.ContactCheckbox = react_1.default.memo(function ContactCheckbox({ about, acceptedMessageRequest, avatarPath, color, disabledReason, i18n, id, isChecked, isMe, name, onClick, phoneNumber, profileName, sharedGroupNames, title, type, unblurredAvatarPath, }) {
    const disabled = Boolean(disabledReason);
    const headerName = isMe ? (react_1.default.createElement("span", { className: BaseConversationListItem_1.HEADER_CONTACT_NAME_CLASS_NAME }, i18n('noteToSelf'))) : (react_1.default.createElement(ContactName_1.ContactName, { module: BaseConversationListItem_1.HEADER_CONTACT_NAME_CLASS_NAME, title: title }));
    let messageText;
    if (disabledReason === ContactCheckboxDisabledReason.AlreadyAdded) {
        messageText = i18n('alreadyAMember');
    }
    else if (about) {
        messageText = react_1.default.createElement(About_1.About, { className: "", text: about });
    }
    else {
        messageText = null;
    }
    const onClickItem = () => {
        onClick(id, disabledReason);
    };
    return (react_1.default.createElement(BaseConversationListItem_1.BaseConversationListItem, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, checked: isChecked, color: color, conversationType: type, disabled: disabled, headerName: headerName, i18n: i18n, id: id, isMe: isMe, isSelected: false, messageText: messageText, name: name, onClick: onClickItem, phoneNumber: phoneNumber, profileName: profileName, sharedGroupNames: sharedGroupNames, title: title, unblurredAvatarPath: unblurredAvatarPath }));
});
