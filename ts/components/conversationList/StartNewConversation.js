"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StartNewConversation = void 0;
const react_1 = __importStar(require("react"));
const BaseConversationListItem_1 = require("./BaseConversationListItem");
const Colors_1 = require("../../types/Colors");
const TEXT_CLASS_NAME = `${BaseConversationListItem_1.MESSAGE_TEXT_CLASS_NAME}__start-new-conversation`;
exports.StartNewConversation = react_1.default.memo(function StartNewConversation({ i18n, onClick, phoneNumber }) {
    const messageText = (react_1.default.createElement("div", { className: TEXT_CLASS_NAME }, i18n('startConversation')));
    const boundOnClick = (0, react_1.useCallback)(() => {
        onClick(phoneNumber);
    }, [onClick, phoneNumber]);
    return (react_1.default.createElement(BaseConversationListItem_1.BaseConversationListItem, { acceptedMessageRequest: false, color: Colors_1.AvatarColors[0], conversationType: "direct", headerName: phoneNumber, i18n: i18n, isMe: false, isSelected: false, messageText: messageText, onClick: boundOnClick, phoneNumber: phoneNumber, sharedGroupNames: [], title: phoneNumber }));
});
