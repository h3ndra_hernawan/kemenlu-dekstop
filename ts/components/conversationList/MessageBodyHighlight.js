"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageBodyHighlight = void 0;
const react_1 = __importDefault(require("react"));
const BaseConversationListItem_1 = require("./BaseConversationListItem");
const AtMentionify_1 = require("../conversation/AtMentionify");
const MessageBody_1 = require("../conversation/MessageBody");
const Emojify_1 = require("../conversation/Emojify");
const AddNewLines_1 = require("../conversation/AddNewLines");
const CLASS_NAME = `${BaseConversationListItem_1.MESSAGE_TEXT_CLASS_NAME}__message-search-result-contents`;
const renderEmoji = ({ text, key, sizeClass, renderNonEmoji, }) => (react_1.default.createElement(Emojify_1.Emojify, { key: key, text: text, sizeClass: sizeClass, renderNonEmoji: renderNonEmoji }));
class MessageBodyHighlight extends react_1.default.Component {
    constructor() {
        super(...arguments);
        this.renderNewLines = ({ text: textWithNewLines, key, }) => {
            const { bodyRanges } = this.props;
            return (react_1.default.createElement(AddNewLines_1.AddNewLines, { key: key, text: textWithNewLines, renderNonNewLine: ({ text, key: innerKey }) => (react_1.default.createElement(AtMentionify_1.AtMentionify, { bodyRanges: bodyRanges, key: innerKey, text: text })) }));
        };
    }
    renderContents() {
        const { bodyRanges, text, i18n } = this.props;
        const results = [];
        const FIND_BEGIN_END = /<<left>>(.+?)<<right>>/g;
        const processedText = AtMentionify_1.AtMentionify.preprocessMentions(text, bodyRanges);
        let match = FIND_BEGIN_END.exec(processedText);
        let last = 0;
        let count = 1;
        if (!match) {
            return (react_1.default.createElement(MessageBody_1.MessageBody, { bodyRanges: bodyRanges, disableJumbomoji: true, disableLinks: true, text: text, i18n: i18n }));
        }
        const sizeClass = '';
        while (match) {
            if (last < match.index) {
                const beforeText = processedText.slice(last, match.index);
                count += 1;
                results.push(renderEmoji({
                    text: beforeText,
                    sizeClass,
                    key: count,
                    i18n,
                    renderNonEmoji: this.renderNewLines,
                }));
            }
            const [, toHighlight] = match;
            count += 2;
            results.push(react_1.default.createElement("span", { className: "MessageBody__highlight", key: count - 1 }, renderEmoji({
                text: toHighlight,
                sizeClass,
                key: count,
                i18n,
                renderNonEmoji: this.renderNewLines,
            })));
            last = FIND_BEGIN_END.lastIndex;
            match = FIND_BEGIN_END.exec(processedText);
        }
        if (last < processedText.length) {
            count += 1;
            results.push(renderEmoji({
                text: processedText.slice(last),
                sizeClass,
                key: count,
                i18n,
                renderNonEmoji: this.renderNewLines,
            }));
        }
        return results;
    }
    render() {
        return react_1.default.createElement("div", { className: CLASS_NAME }, this.renderContents());
    }
}
exports.MessageBodyHighlight = MessageBodyHighlight;
