"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseConversationListItem = exports.MESSAGE_TEXT_CLASS_NAME = exports.DATE_CLASS_NAME = exports.HEADER_CONTACT_NAME_CLASS_NAME = exports.HEADER_NAME_CLASS_NAME = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const uuid_1 = require("uuid");
const Avatar_1 = require("../Avatar");
const Timestamp_1 = require("../conversation/Timestamp");
const isConversationUnread_1 = require("../../util/isConversationUnread");
const _util_1 = require("../_util");
const Spinner_1 = require("../Spinner");
const BASE_CLASS_NAME = 'module-conversation-list__item--contact-or-conversation';
const CONTENT_CLASS_NAME = `${BASE_CLASS_NAME}__content`;
const HEADER_CLASS_NAME = `${CONTENT_CLASS_NAME}__header`;
exports.HEADER_NAME_CLASS_NAME = `${HEADER_CLASS_NAME}__name`;
exports.HEADER_CONTACT_NAME_CLASS_NAME = `${exports.HEADER_NAME_CLASS_NAME}__contact-name`;
exports.DATE_CLASS_NAME = `${HEADER_CLASS_NAME}__date`;
const TIMESTAMP_CLASS_NAME = `${exports.DATE_CLASS_NAME}__timestamp`;
const MESSAGE_CLASS_NAME = `${CONTENT_CLASS_NAME}__message`;
exports.MESSAGE_TEXT_CLASS_NAME = `${MESSAGE_CLASS_NAME}__text`;
const CHECKBOX_CLASS_NAME = `${BASE_CLASS_NAME}__checkbox`;
exports.BaseConversationListItem = react_1.default.memo(function BaseConversationListItem({ acceptedMessageRequest, avatarPath, badge, checked, color, conversationType, disabled, headerDate, headerName, i18n, id, isMe, isNoteToSelf, isUsernameSearchResult, isSelected, markedUnread, messageStatusIcon, messageText, messageTextIsAlwaysFullSize, name, onClick, phoneNumber, profileName, sharedGroupNames, shouldShowSpinner, theme, title, unblurredAvatarPath, unreadCount, }) {
    const identifier = id ? (0, _util_1.cleanId)(id) : undefined;
    const htmlId = (0, react_1.useMemo)(() => (0, uuid_1.v4)(), []);
    const isUnread = (0, isConversationUnread_1.isConversationUnread)({ markedUnread, unreadCount });
    const isAvatarNoteToSelf = (0, lodash_1.isBoolean)(isNoteToSelf)
        ? isNoteToSelf
        : Boolean(isMe);
    const isCheckbox = (0, lodash_1.isBoolean)(checked);
    let actionNode;
    if (shouldShowSpinner) {
        actionNode = (react_1.default.createElement(Spinner_1.Spinner, { size: "20px", svgSize: "small", direction: "on-progress-dialog" }));
    }
    else if (isCheckbox) {
        let ariaLabel;
        if (disabled) {
            ariaLabel = i18n('cannotSelectContact', [title]);
        }
        else if (checked) {
            ariaLabel = i18n('deselectContact', [title]);
        }
        else {
            ariaLabel = i18n('selectContact', [title]);
        }
        actionNode = (react_1.default.createElement("input", { "aria-label": ariaLabel, checked: checked, className: CHECKBOX_CLASS_NAME, disabled: disabled, id: htmlId, onChange: onClick, onKeyDown: event => {
                if (onClick && !disabled && event.key === 'Enter') {
                    onClick();
                }
            }, type: "checkbox" }));
    }
    const contents = (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, badge: badge, color: color, conversationType: conversationType, noteToSelf: isAvatarNoteToSelf, searchResult: isUsernameSearchResult, i18n: i18n, isMe: isMe, name: name, phoneNumber: phoneNumber, profileName: profileName, theme: theme, title: title, sharedGroupNames: sharedGroupNames, size: Avatar_1.AvatarSize.FORTY_EIGHT, unblurredAvatarPath: unblurredAvatarPath }),
        react_1.default.createElement("div", { className: (0, classnames_1.default)(CONTENT_CLASS_NAME, disabled && `${CONTENT_CLASS_NAME}--disabled`) },
            react_1.default.createElement("div", { className: HEADER_CLASS_NAME },
                react_1.default.createElement("div", { className: `${HEADER_CLASS_NAME}__name` }, headerName),
                (0, lodash_1.isNumber)(headerDate) && (react_1.default.createElement("div", { className: exports.DATE_CLASS_NAME },
                    react_1.default.createElement(Timestamp_1.Timestamp, { timestamp: headerDate, extended: false, module: TIMESTAMP_CLASS_NAME, i18n: i18n })))),
            messageText || isUnread ? (react_1.default.createElement("div", { className: MESSAGE_CLASS_NAME },
                Boolean(messageText) && (react_1.default.createElement("div", { dir: "auto", className: (0, classnames_1.default)(exports.MESSAGE_TEXT_CLASS_NAME, messageTextIsAlwaysFullSize &&
                        `${exports.MESSAGE_TEXT_CLASS_NAME}--always-full-size`) }, messageText)),
                messageStatusIcon,
                isUnread && react_1.default.createElement(UnreadIndicator, { count: unreadCount }))) : null),
        actionNode));
    const commonClassNames = (0, classnames_1.default)(BASE_CLASS_NAME, {
        [`${BASE_CLASS_NAME}--is-selected`]: isSelected,
    });
    if (isCheckbox) {
        return (react_1.default.createElement("label", Object.assign({ className: (0, classnames_1.default)(commonClassNames, `${BASE_CLASS_NAME}--is-checkbox`, { [`${BASE_CLASS_NAME}--is-checkbox--disabled`]: disabled }), "data-id": identifier, htmlFor: htmlId }, (disabled ? { onClick } : {})), contents));
    }
    if (onClick) {
        return (react_1.default.createElement("button", { "aria-label": i18n('BaseConversationListItem__aria-label', { title }), className: (0, classnames_1.default)(commonClassNames, `${BASE_CLASS_NAME}--is-button`), "data-id": identifier, disabled: disabled, onClick: onClick, type: "button" }, contents));
    }
    return (react_1.default.createElement("div", { className: commonClassNames, "data-id": identifier }, contents));
});
function UnreadIndicator({ count = 0 }) {
    let classModifier;
    if (count > 99) {
        classModifier = 'many';
    }
    else if (count > 9) {
        classModifier = 'two-digits';
    }
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(`${BASE_CLASS_NAME}__unread-indicator`, classModifier &&
            `${BASE_CLASS_NAME}__unread-indicator--${classModifier}`) }, Boolean(count) && Math.min(count, 99)));
}
