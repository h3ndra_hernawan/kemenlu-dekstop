"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const MessageBodyHighlight_1 = require("./MessageBodyHighlight");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/MessageBodyHighlight', module);
// Storybook types are incorrect
// eslint-disable-next-line @typescript-eslint/no-explicit-any
story.addDecorator(addon_knobs_1.withKnobs({ escapeHTML: false }));
const createProps = (overrideProps = {}) => ({
    bodyRanges: overrideProps.bodyRanges || [],
    i18n,
    text: (0, addon_knobs_1.text)('text', overrideProps.text || ''),
});
story.add('Basic', () => {
    const props = createProps({
        text: 'This is before <<left>>Inside<<right>> This is after.',
    });
    return React.createElement(MessageBodyHighlight_1.MessageBodyHighlight, Object.assign({}, props));
});
story.add('No Replacement', () => {
    const props = createProps({
        text: 'All\nplain\ntext 🔥 http://somewhere.com',
    });
    return React.createElement(MessageBodyHighlight_1.MessageBodyHighlight, Object.assign({}, props));
});
story.add('Two Replacements', () => {
    const props = createProps({
        text: 'Begin <<left>>Inside #1<<right>> This is between the two <<left>>Inside #2<<right>> End.',
    });
    return React.createElement(MessageBodyHighlight_1.MessageBodyHighlight, Object.assign({}, props));
});
story.add('Two Replacements with an @mention', () => {
    const props = createProps({
        bodyRanges: [
            {
                length: 1,
                mentionUuid: '0ca40892-7b1a-11eb-9439-0242ac130002',
                replacementText: 'Jin Sakai',
                start: 33,
            },
        ],
        text: 'Begin <<left>>Inside #1<<right>> \uFFFC This is between the two <<left>>Inside #2<<right>> End.',
    });
    return React.createElement(MessageBodyHighlight_1.MessageBodyHighlight, Object.assign({}, props));
});
story.add('Emoji + Newlines + URLs', () => {
    const props = createProps({
        text: '\nhttp://somewhere.com\n\n🔥 Before -- <<left>>A 🔥 inside<<right>> -- After 🔥',
    });
    return React.createElement(MessageBodyHighlight_1.MessageBodyHighlight, Object.assign({}, props));
});
story.add('No Jumbomoji', () => {
    const props = createProps({
        text: '🔥',
    });
    return React.createElement(MessageBodyHighlight_1.MessageBodyHighlight, Object.assign({}, props));
});
