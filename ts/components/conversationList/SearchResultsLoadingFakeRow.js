"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SearchResultsLoadingFakeRow = void 0;
const react_1 = __importDefault(require("react"));
const SearchResultsLoadingFakeRow = () => (react_1.default.createElement("div", { className: "module-SearchResultsLoadingFakeRow" },
    react_1.default.createElement("div", { className: "module-SearchResultsLoadingFakeRow__avatar" }),
    react_1.default.createElement("div", { className: "module-SearchResultsLoadingFakeRow__content" },
        react_1.default.createElement("div", { className: "module-SearchResultsLoadingFakeRow__content__line" }),
        react_1.default.createElement("div", { className: "module-SearchResultsLoadingFakeRow__content__line" }),
        react_1.default.createElement("div", { className: "module-SearchResultsLoadingFakeRow__content__line" }))));
exports.SearchResultsLoadingFakeRow = SearchResultsLoadingFakeRow;
