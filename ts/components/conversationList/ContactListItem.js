"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactListItem = void 0;
const react_1 = __importDefault(require("react"));
const BaseConversationListItem_1 = require("./BaseConversationListItem");
const ContactName_1 = require("../conversation/ContactName");
const About_1 = require("../conversation/About");
exports.ContactListItem = react_1.default.memo(function ContactListItem({ about, acceptedMessageRequest, avatarPath, color, i18n, id, isMe, name, onClick, phoneNumber, profileName, sharedGroupNames, title, type, unblurredAvatarPath, }) {
    const headerName = isMe ? (react_1.default.createElement("span", { className: BaseConversationListItem_1.HEADER_CONTACT_NAME_CLASS_NAME }, i18n('noteToSelf'))) : (react_1.default.createElement(ContactName_1.ContactName, { module: BaseConversationListItem_1.HEADER_CONTACT_NAME_CLASS_NAME, title: title }));
    const messageText = about && !isMe ? react_1.default.createElement(About_1.About, { className: "", text: about }) : null;
    return (react_1.default.createElement(BaseConversationListItem_1.BaseConversationListItem, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, color: color, conversationType: type, headerName: headerName, i18n: i18n, id: id, isMe: isMe, isSelected: false, messageText: messageText, name: name, onClick: onClick ? () => onClick(id) : undefined, phoneNumber: phoneNumber, profileName: profileName, sharedGroupNames: sharedGroupNames, title: title, unblurredAvatarPath: unblurredAvatarPath }));
});
