"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateNewGroupButton = void 0;
const react_1 = __importDefault(require("react"));
const BaseConversationListItem_1 = require("./BaseConversationListItem");
exports.CreateNewGroupButton = react_1.default.memo(function CreateNewGroupButton({ i18n, onClick }) {
    const title = i18n('createNewGroupButton');
    return (react_1.default.createElement(BaseConversationListItem_1.BaseConversationListItem, { acceptedMessageRequest: false, conversationType: "group", headerName: title, i18n: i18n, isMe: false, isSelected: false, onClick: onClick, sharedGroupNames: [], title: title }));
});
