"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const MessageSearchResult_1 = require("./MessageSearchResult");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/MessageSearchResult', module);
// Storybook types are incorrect
// eslint-disable-next-line @typescript-eslint/no-explicit-any
story.addDecorator(addon_knobs_1.withKnobs({ escapeHTML: false }));
const someone = (0, getDefaultConversation_1.getDefaultConversation)({
    title: 'Some Person',
    name: 'Some Person',
    phoneNumber: '(202) 555-0011',
});
const me = (0, getDefaultConversation_1.getDefaultConversation)({
    title: 'Me',
    name: 'Me',
    isMe: true,
});
const group = (0, getDefaultConversation_1.getDefaultConversation)({
    title: 'Group Chat',
    name: 'Group Chat',
    type: 'group',
});
const createProps = (overrideProps = {}) => ({
    i18n,
    id: '',
    conversationId: '',
    sentAt: Date.now() - 24 * 60 * 1000,
    snippet: (0, addon_knobs_1.text)('snippet', overrideProps.snippet || "What's <<left>>going<<right>> on?"),
    body: (0, addon_knobs_1.text)('body', overrideProps.body || "What's going on?"),
    bodyRanges: overrideProps.bodyRanges || [],
    from: overrideProps.from,
    to: overrideProps.to,
    isSelected: (0, addon_knobs_1.boolean)('isSelected', overrideProps.isSelected || false),
    openConversationInternal: (0, addon_actions_1.action)('openConversationInternal'),
    isSearchingInConversation: (0, addon_knobs_1.boolean)('isSearchingInConversation', overrideProps.isSearchingInConversation || false),
});
story.add('Default', () => {
    const props = createProps({
        from: someone,
        to: me,
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('Selected', () => {
    const props = createProps({
        from: someone,
        to: me,
        isSelected: true,
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('From You', () => {
    const props = createProps({
        from: me,
        to: someone,
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('Searching in Conversation', () => {
    const props = createProps({
        from: me,
        to: someone,
        isSearchingInConversation: true,
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('From You to Yourself', () => {
    const props = createProps({
        from: me,
        to: me,
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('From You to Group', () => {
    const props = createProps({
        from: me,
        to: group,
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('From Someone to Group', () => {
    const props = createProps({
        from: someone,
        to: group,
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('Long Search Result', () => {
    const snippets = [
        'This is a really <<left>>detail<<right>>ed long line which will wrap and only be cut off after it gets to three lines. So maybe this will make it in as well?',
        "Okay, here are the <<left>>detail<<right>>s:\n\n1355 Ridge Way\nCode: 234\n\nI'm excited!",
    ];
    return snippets.map(snippet => {
        const props = createProps({
            from: someone,
            to: me,
            snippet,
        });
        return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({ key: snippet.length }, props));
    });
});
story.add('Empty (should be invalid)', () => {
    const props = createProps();
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('@mention', () => {
    const props = createProps({
        body: 'moss banana twine sound lake zoo brain count vacuum work stairs try power forget hair dry diary years no results \uFFFC elephant sorry umbrella potato igloo kangaroo home Georgia bayonet vector orange forge diary zebra turtle rise front \uFFFC',
        bodyRanges: [
            {
                length: 1,
                mentionUuid: '7d007e95-771d-43ad-9191-eaa86c773cb8',
                replacementText: 'Shoe',
                start: 113,
            },
            {
                length: 1,
                mentionUuid: '7d007e95-771d-43ad-9191-eaa86c773cb8',
                replacementText: 'Shoe',
                start: 237,
            },
        ],
        from: someone,
        to: me,
        snippet: '...forget hair dry diary years no <<left>>results<<right>> \uFFFC <<left>>elephant<<right>> sorry umbrella potato igloo kangaroo home Georgia...',
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('@mention regexp', () => {
    const props = createProps({
        body: '\uFFFC This is a (long) /text/ ^$ that is ... specially **crafted** to (test) our regexp escaping mechanism! Making sure that the code we write works in all sorts of scenarios',
        bodyRanges: [
            {
                length: 1,
                mentionUuid: '7d007e95-771d-43ad-9191-eaa86c773cb8',
                replacementText: 'RegExp',
                start: 0,
            },
        ],
        from: someone,
        to: me,
        snippet: '\uFFFC This is a (long) /text/ ^$ that is ... <<left>>specially<<right>> **crafted** to (test) our regexp escaping mechanism...',
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('@mention no-matches', () => {
    const props = createProps({
        body: '\uFFFC hello',
        bodyRanges: [
            {
                length: 1,
                mentionUuid: '7d007e95-771d-43ad-9191-eaa86c773cb8',
                replacementText: 'Neo',
                start: 0,
            },
        ],
        from: someone,
        to: me,
        snippet: '\uFFFC hello',
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('@mention no-matches', () => {
    const props = createProps({
        body: 'moss banana twine sound lake zoo brain count vacuum work stairs try power forget hair dry diary years no results \uFFFC elephant sorry umbrella potato igloo kangaroo home Georgia bayonet vector orange forge diary zebra turtle rise front \uFFFC',
        bodyRanges: [
            {
                length: 1,
                mentionUuid: '7d007e95-771d-43ad-9191-eaa86c773cb8',
                replacementText: 'Shoe',
                start: 113,
            },
            {
                length: 1,
                mentionUuid: '7d007e95-771d-43ad-9191-eaa86c773cb8',
                replacementText: 'Shoe',
                start: 237,
            },
        ],
        from: someone,
        to: me,
        snippet: '...forget hair dry diary years no results \uFFFC elephant sorry umbrella potato igloo kangaroo home Georgia...',
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
story.add('Double @mention', () => {
    const props = createProps({
        body: 'Hey \uFFFC \uFFFC test',
        bodyRanges: [
            {
                length: 1,
                mentionUuid: '9eb2eb65-992a-4909-a2a5-18c56bd7648f',
                replacementText: 'Alice',
                start: 4,
            },
            {
                length: 1,
                mentionUuid: '755ec61b-1590-48da-b003-3e57b2b54448',
                replacementText: 'Bob',
                start: 6,
            },
        ],
        from: someone,
        to: me,
        snippet: '<<left>>Hey<<right>> \uFFFC \uFFFC <<left>>test<<right>>',
    });
    return React.createElement(MessageSearchResult_1.MessageSearchResult, Object.assign({}, props));
});
