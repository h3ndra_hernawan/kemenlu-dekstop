"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsernameSearchResultListItem = void 0;
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const BaseConversationListItem_1 = require("./BaseConversationListItem");
const UsernameSearchResultListItem = ({ i18n, isFetchingUsername, onClick, username, }) => {
    const usernameText = i18n('at-username', { username });
    const boundOnClick = isFetchingUsername
        ? lodash_1.noop
        : () => {
            onClick(username);
        };
    return (react_1.default.createElement(BaseConversationListItem_1.BaseConversationListItem, { acceptedMessageRequest: false, conversationType: "direct", headerName: usernameText, i18n: i18n, isMe: false, isSelected: false, isUsernameSearchResult: true, shouldShowSpinner: isFetchingUsername, onClick: boundOnClick, sharedGroupNames: [], title: usernameText }));
};
exports.UsernameSearchResultListItem = UsernameSearchResultListItem;
