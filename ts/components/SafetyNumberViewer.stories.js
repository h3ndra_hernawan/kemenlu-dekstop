"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const SafetyNumberViewer_1 = require("./SafetyNumberViewer");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const contactWithAllData = (0, getDefaultConversation_1.getDefaultConversation)({
    title: 'Summer Smith',
    name: 'Summer Smith',
    phoneNumber: '(305) 123-4567',
    isVerified: true,
});
const contactWithJustProfile = (0, getDefaultConversation_1.getDefaultConversation)({
    avatarPath: undefined,
    title: '-*Smartest Dude*-',
    profileName: '-*Smartest Dude*-',
    name: undefined,
    phoneNumber: '(305) 123-4567',
});
const contactWithJustNumber = (0, getDefaultConversation_1.getDefaultConversation)({
    avatarPath: undefined,
    profileName: undefined,
    name: undefined,
    title: '(305) 123-4567',
    phoneNumber: '(305) 123-4567',
});
const contactWithNothing = (0, getDefaultConversation_1.getDefaultConversation)({
    id: 'some-guid',
    avatarPath: undefined,
    profileName: undefined,
    title: 'Unknown contact',
    name: undefined,
    phoneNumber: undefined,
});
const createProps = (overrideProps = {}) => ({
    contact: overrideProps.contact || contactWithAllData,
    generateSafetyNumber: (0, addon_actions_1.action)('generate-safety-number'),
    i18n,
    safetyNumber: (0, addon_knobs_1.text)('safetyNumber', overrideProps.safetyNumber || 'XXX'),
    toggleVerified: (0, addon_actions_1.action)('toggle-verified'),
    verificationDisabled: (0, addon_knobs_1.boolean)('verificationDisabled', overrideProps.verificationDisabled !== undefined
        ? overrideProps.verificationDisabled
        : false),
    onClose: overrideProps.onClose,
});
const story = (0, react_1.storiesOf)('Components/SafetyNumberViewer', module);
story.add('Safety Number', () => {
    return React.createElement(SafetyNumberViewer_1.SafetyNumberViewer, Object.assign({}, createProps({})));
});
story.add('Safety Number (not verified)', () => {
    return (React.createElement(SafetyNumberViewer_1.SafetyNumberViewer, Object.assign({}, createProps({
        contact: Object.assign(Object.assign({}, contactWithAllData), { isVerified: false }),
    }))));
});
story.add('Verification Disabled', () => {
    return (React.createElement(SafetyNumberViewer_1.SafetyNumberViewer, Object.assign({}, createProps({
        verificationDisabled: true,
    }))));
});
story.add('Safety Number (dialog close)', () => {
    return (React.createElement(SafetyNumberViewer_1.SafetyNumberViewer, Object.assign({}, createProps({
        onClose: (0, addon_actions_1.action)('close'),
    }))));
});
story.add('Just Profile and Number', () => {
    return (React.createElement(SafetyNumberViewer_1.SafetyNumberViewer, Object.assign({}, createProps({
        contact: contactWithJustProfile,
    }))));
});
story.add('Just Number', () => {
    return (React.createElement(SafetyNumberViewer_1.SafetyNumberViewer, Object.assign({}, createProps({
        contact: contactWithJustNumber,
    }))));
});
story.add('No Phone Number (cannot verify)', () => {
    return (React.createElement(SafetyNumberViewer_1.SafetyNumberViewer, Object.assign({}, createProps({
        contact: contactWithNothing,
    }))));
});
