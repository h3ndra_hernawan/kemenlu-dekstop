"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IncomingCallBar = void 0;
const react_1 = __importStar(require("react"));
const Avatar_1 = require("./Avatar");
const Tooltip_1 = require("./Tooltip");
const Intl_1 = require("./Intl");
const theme_1 = require("../util/theme");
const callingGetParticipantName_1 = require("../util/callingGetParticipantName");
const ContactName_1 = require("./conversation/ContactName");
const Emojify_1 = require("./conversation/Emojify");
const Colors_1 = require("../types/Colors");
const Calling_1 = require("../types/Calling");
const missingCaseError_1 = require("../util/missingCaseError");
const CallButton = ({ classSuffix, onClick, tabIndex, tooltipContent, }) => {
    return (react_1.default.createElement(Tooltip_1.Tooltip, { content: tooltipContent, theme: theme_1.Theme.Dark },
        react_1.default.createElement("button", { "aria-label": tooltipContent, className: `IncomingCallBar__button IncomingCallBar__button--${classSuffix}`, onClick: onClick, tabIndex: tabIndex, type: "button" },
            react_1.default.createElement("div", null))));
};
const GroupCallMessage = ({ i18n, otherMembersRung, ringer, }) => {
    // As an optimization, we only process the first two names.
    const [first, second] = otherMembersRung
        .slice(0, 2)
        .map(member => react_1.default.createElement(Emojify_1.Emojify, { text: (0, callingGetParticipantName_1.getParticipantName)(member) }));
    const ringerNode = react_1.default.createElement(Emojify_1.Emojify, { text: (0, callingGetParticipantName_1.getParticipantName)(ringer) });
    switch (otherMembersRung.length) {
        case 0:
            return (react_1.default.createElement(Intl_1.Intl, { id: "incomingGroupCall__ringing-you", i18n: i18n, components: { ringer: ringerNode } }));
        case 1:
            return (react_1.default.createElement(Intl_1.Intl, { id: "incomingGroupCall__ringing-1-other", i18n: i18n, components: {
                    ringer: ringerNode,
                    otherMember: first,
                } }));
        case 2:
            return (react_1.default.createElement(Intl_1.Intl, { id: "incomingGroupCall__ringing-2-others", i18n: i18n, components: {
                    ringer: ringerNode,
                    first,
                    second,
                } }));
            break;
        case 3:
            return (react_1.default.createElement(Intl_1.Intl, { id: "incomingGroupCall__ringing-3-others", i18n: i18n, components: {
                    ringer: ringerNode,
                    first,
                    second,
                } }));
            break;
        default:
            return (react_1.default.createElement(Intl_1.Intl, { id: "incomingGroupCall__ringing-many", i18n: i18n, components: {
                    ringer: ringerNode,
                    first,
                    second,
                    remaining: String(otherMembersRung.length - 2),
                } }));
    }
};
const IncomingCallBar = (props) => {
    const { acceptCall, bounceAppIconStart, bounceAppIconStop, conversation, declineCall, i18n, notifyForCall, } = props;
    const { id: conversationId, acceptedMessageRequest, avatarPath, color, isMe, name, phoneNumber, profileName, sharedGroupNames, title, type: conversationType, } = conversation;
    let isVideoCall;
    let headerNode;
    let messageNode;
    switch (props.callMode) {
        case Calling_1.CallMode.Direct:
            ({ isVideoCall } = props);
            headerNode = react_1.default.createElement(ContactName_1.ContactName, { title: title });
            messageNode = i18n(isVideoCall ? 'incomingVideoCall' : 'incomingAudioCall');
            break;
        case Calling_1.CallMode.Group: {
            const { otherMembersRung, ringer } = props;
            isVideoCall = true;
            headerNode = react_1.default.createElement(Emojify_1.Emojify, { text: title });
            messageNode = (react_1.default.createElement(GroupCallMessage, { i18n: i18n, otherMembersRung: otherMembersRung, ringer: ringer }));
            break;
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(props);
    }
    // We don't want to re-notify if the title changes.
    const initialTitleRef = (0, react_1.useRef)(title);
    (0, react_1.useEffect)(() => {
        const initialTitle = initialTitleRef.current;
        notifyForCall(initialTitle, isVideoCall);
    }, [isVideoCall, notifyForCall]);
    (0, react_1.useEffect)(() => {
        bounceAppIconStart();
        return () => {
            bounceAppIconStop();
        };
    }, [bounceAppIconStart, bounceAppIconStop]);
    return (react_1.default.createElement("div", { className: "IncomingCallBar__container" },
        react_1.default.createElement("div", { className: "IncomingCallBar__bar" },
            react_1.default.createElement("div", { className: "IncomingCallBar__conversation" },
                react_1.default.createElement("div", { className: "IncomingCallBar__conversation--avatar" },
                    react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, color: color || Colors_1.AvatarColors[0], noteToSelf: false, conversationType: conversationType, i18n: i18n, isMe: isMe, name: name, phoneNumber: phoneNumber, profileName: profileName, title: title, sharedGroupNames: sharedGroupNames, size: 52 })),
                react_1.default.createElement("div", { className: "IncomingCallBar__conversation--name" },
                    react_1.default.createElement("div", { className: "IncomingCallBar__conversation--name-header" }, headerNode),
                    react_1.default.createElement("div", { dir: "auto", className: "IncomingCallBar__conversation--message-text" }, messageNode))),
            react_1.default.createElement("div", { className: "IncomingCallBar__actions" },
                react_1.default.createElement(CallButton, { classSuffix: "decline", onClick: () => {
                        declineCall({ conversationId });
                    }, tabIndex: 0, tooltipContent: i18n('declineCall') }),
                isVideoCall ? (react_1.default.createElement(react_1.default.Fragment, null,
                    react_1.default.createElement(CallButton, { classSuffix: "accept-video-as-audio", onClick: () => {
                            acceptCall({ conversationId, asVideoCall: false });
                        }, tabIndex: 0, tooltipContent: i18n('acceptCallWithoutVideo') }),
                    react_1.default.createElement(CallButton, { classSuffix: "accept-video", onClick: () => {
                            acceptCall({ conversationId, asVideoCall: true });
                        }, tabIndex: 0, tooltipContent: i18n('acceptCall') }))) : (react_1.default.createElement(CallButton, { classSuffix: "accept-audio", onClick: () => {
                        acceptCall({ conversationId, asVideoCall: false });
                    }, tabIndex: 0, tooltipContent: i18n('acceptCall') }))))));
};
exports.IncomingCallBar = IncomingCallBar;
