"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tabs = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const assert_1 = require("../util/assert");
const getClassNamesFor_1 = require("../util/getClassNamesFor");
const Tabs = ({ children, initialSelectedTab, moduleClassName, onTabChange, tabs, }) => {
    (0, assert_1.assert)(tabs.length, 'Tabs needs more than 1 tab present');
    const [selectedTab, setSelectedTab] = (0, react_1.useState)(initialSelectedTab || tabs[0].id);
    const getClassName = (0, getClassNamesFor_1.getClassNamesFor)('Tabs', moduleClassName);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: getClassName('') }, tabs.map(({ id, label }) => (react_1.default.createElement("div", { className: (0, classnames_1.default)(getClassName('__tab'), selectedTab === id && getClassName('__tab--selected')), key: id, onClick: () => {
                setSelectedTab(id);
                onTabChange === null || onTabChange === void 0 ? void 0 : onTabChange(id);
            }, onKeyUp: (e) => {
                if (e.target === e.currentTarget && e.keyCode === 13) {
                    setSelectedTab(id);
                    e.preventDefault();
                    e.stopPropagation();
                }
            }, role: "tab", tabIndex: 0 }, label)))),
        children({ selectedTab })));
};
exports.Tabs = Tabs;
