"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const react_2 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const ProfileEditor_1 = require("./ProfileEditor");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const getRandomColor_1 = require("../test-both/helpers/getRandomColor");
const conversationsEnums_1 = require("../state/ducks/conversationsEnums");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const stories = (0, react_2.storiesOf)('Components/ProfileEditor', module);
const createProps = (overrideProps = {}) => ({
    aboutEmoji: overrideProps.aboutEmoji,
    aboutText: (0, addon_knobs_1.text)('about', overrideProps.aboutText || ''),
    avatarPath: overrideProps.avatarPath,
    clearUsernameSave: (0, addon_actions_1.action)('clearUsernameSave'),
    conversationId: '123',
    color: overrideProps.color || (0, getRandomColor_1.getRandomColor)(),
    deleteAvatarFromDisk: (0, addon_actions_1.action)('deleteAvatarFromDisk'),
    familyName: overrideProps.familyName,
    firstName: (0, addon_knobs_1.text)('firstName', overrideProps.firstName || (0, getDefaultConversation_1.getFirstName)()),
    i18n,
    isUsernameFlagEnabled: (0, addon_knobs_1.boolean)('isUsernameFlagEnabled', overrideProps.isUsernameFlagEnabled !== undefined
        ? overrideProps.isUsernameFlagEnabled
        : false),
    onEditStateChanged: (0, addon_actions_1.action)('onEditStateChanged'),
    onProfileChanged: (0, addon_actions_1.action)('onProfileChanged'),
    onSetSkinTone: overrideProps.onSetSkinTone || (0, addon_actions_1.action)('onSetSkinTone'),
    recentEmojis: [],
    replaceAvatar: (0, addon_actions_1.action)('replaceAvatar'),
    saveAvatarToDisk: (0, addon_actions_1.action)('saveAvatarToDisk'),
    saveUsername: (0, addon_actions_1.action)('saveUsername'),
    skinTone: overrideProps.skinTone || 0,
    userAvatarData: [],
    username: overrideProps.username,
    usernameSaveState: (0, addon_knobs_1.select)('usernameSaveState', Object.values(conversationsEnums_1.UsernameSaveState), overrideProps.usernameSaveState || conversationsEnums_1.UsernameSaveState.None),
});
stories.add('Full Set', () => {
    const [skinTone, setSkinTone] = (0, react_1.useState)(0);
    return (react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, createProps({
        aboutEmoji: '🙏',
        aboutText: 'Live. Laugh. Love',
        avatarPath: '/fixtures/kitten-3-64-64.jpg',
        onSetSkinTone: setSkinTone,
        familyName: (0, getDefaultConversation_1.getLastName)(),
        skinTone,
    }))));
});
stories.add('with Full Name', () => (react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, createProps({
    familyName: (0, getDefaultConversation_1.getLastName)(),
})))));
stories.add('with Custom About', () => (react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, createProps({
    aboutEmoji: '🙏',
    aboutText: 'Live. Laugh. Love',
})))));
stories.add('with Username flag enabled', () => (react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, createProps({
    isUsernameFlagEnabled: true,
})))));
stories.add('with Username flag enabled and username', () => (react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, createProps({
    isUsernameFlagEnabled: true,
    username: 'unicorn55',
})))));
stories.add('Username editing, saving', () => (react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, createProps({
    isUsernameFlagEnabled: true,
    usernameSaveState: conversationsEnums_1.UsernameSaveState.Saving,
    username: 'unicorn55',
})))));
stories.add('Username editing, username taken', () => (react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, createProps({
    isUsernameFlagEnabled: true,
    usernameSaveState: conversationsEnums_1.UsernameSaveState.UsernameTakenError,
    username: 'unicorn55',
})))));
stories.add('Username editing, username malformed', () => (react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, createProps({
    isUsernameFlagEnabled: true,
    usernameSaveState: conversationsEnums_1.UsernameSaveState.UsernameMalformedError,
    username: 'unicorn55',
})))));
stories.add('Username editing, general error', () => (react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, createProps({
    isUsernameFlagEnabled: true,
    usernameSaveState: conversationsEnums_1.UsernameSaveState.GeneralError,
    username: 'unicorn55',
})))));
