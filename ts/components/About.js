"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.About = void 0;
const react_1 = __importDefault(require("react"));
const useEscapeHandling_1 = require("../hooks/useEscapeHandling");
const About = ({ closeAbout, i18n, environment, version, }) => {
    (0, useEscapeHandling_1.useEscapeHandling)(closeAbout);
    return (react_1.default.createElement("div", { className: "About" },
        react_1.default.createElement("div", { className: "module-splash-screen" },
            react_1.default.createElement("div", { className: "module-splash-screen__logo module-img--150" }),
            react_1.default.createElement("div", { className: "version" }, version),
            react_1.default.createElement("div", { className: "environment" }, environment),
            react_1.default.createElement("div", null,
                react_1.default.createElement("a", { href: "https://signal.org" }, "signal.org")),
            react_1.default.createElement("br", null),
            react_1.default.createElement("div", null,
                react_1.default.createElement("a", { className: "acknowledgments", href: "https://github.com/signalapp/Signal-Desktop/blob/development/ACKNOWLEDGMENTS.md" }, i18n('softwareAcknowledgments'))),
            react_1.default.createElement("div", null,
                react_1.default.createElement("a", { className: "privacy", href: "https://signal.org/legal" }, i18n('privacyPolicy'))))));
};
exports.About = About;
