"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const AvatarTextEditor_1 = require("./AvatarTextEditor");
const Colors_1 = require("../types/Colors");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    avatarData: overrideProps.avatarData,
    i18n,
    onCancel: (0, addon_actions_1.action)('onCancel'),
    onDone: (0, addon_actions_1.action)('onDone'),
});
const story = (0, react_2.storiesOf)('Components/AvatarTextEditor', module);
story.add('Empty', () => react_1.default.createElement(AvatarTextEditor_1.AvatarTextEditor, Object.assign({}, createProps())));
story.add('with Data', () => (react_1.default.createElement(AvatarTextEditor_1.AvatarTextEditor, Object.assign({}, createProps({
    avatarData: {
        id: '123',
        color: Colors_1.AvatarColors[6],
        text: 'SUP',
    },
})))));
story.add('with wide characters', () => (react_1.default.createElement(AvatarTextEditor_1.AvatarTextEditor, Object.assign({}, createProps({
    avatarData: {
        id: '123',
        color: Colors_1.AvatarColors[6],
        text: '‱௸𒈙',
    },
})))));
