"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPaneComposeHelper = void 0;
const react_1 = __importDefault(require("react"));
const LeftPaneHelper_1 = require("./LeftPaneHelper");
const ConversationList_1 = require("../ConversationList");
const SearchInput_1 = require("../SearchInput");
const libphonenumberInstance_1 = require("../../util/libphonenumberInstance");
const assert_1 = require("../../util/assert");
const missingCaseError_1 = require("../../util/missingCaseError");
const Username_1 = require("../../types/Username");
var TopButton;
(function (TopButton) {
    TopButton[TopButton["None"] = 0] = "None";
    TopButton[TopButton["CreateNewGroup"] = 1] = "CreateNewGroup";
    TopButton[TopButton["StartNewConversation"] = 2] = "StartNewConversation";
})(TopButton || (TopButton = {}));
class LeftPaneComposeHelper extends LeftPaneHelper_1.LeftPaneHelper {
    constructor({ composeContacts, composeGroups, regionCode, searchTerm, isUsernamesEnabled, isFetchingUsername, }) {
        super();
        this.composeContacts = composeContacts;
        this.composeGroups = composeGroups;
        this.searchTerm = searchTerm;
        this.phoneNumber = parsePhoneNumber(searchTerm, regionCode);
        this.isFetchingUsername = isFetchingUsername;
        this.isUsernamesEnabled = isUsernamesEnabled;
    }
    getHeaderContents({ i18n, showInbox, }) {
        return (react_1.default.createElement("div", { className: "module-left-pane__header__contents" },
            react_1.default.createElement("button", { onClick: this.getBackAction({ showInbox }), className: "module-left-pane__header__contents__back-button", title: i18n('backToInbox'), "aria-label": i18n('backToInbox'), type: "button" }),
            react_1.default.createElement("div", { className: "module-left-pane__header__contents__text" }, i18n('newConversation'))));
    }
    getBackAction({ showInbox }) {
        return showInbox;
    }
    getPreRowsNode({ i18n, onChangeComposeSearchTerm, }) {
        return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(SearchInput_1.SearchInput, { moduleClassName: "module-left-pane__compose-search-form", onChange: onChangeComposeSearchTerm, placeholder: i18n('contactSearchPlaceholder'), ref: focusRef, value: this.searchTerm }),
            this.getRowCount() ? null : (react_1.default.createElement("div", { className: "module-left-pane__compose-no-contacts" }, i18n('noConversationsFound')))));
    }
    getRowCount() {
        let result = this.composeContacts.length + this.composeGroups.length;
        if (this.hasTopButton()) {
            result += 1;
        }
        if (this.hasContactsHeader()) {
            result += 1;
        }
        if (this.hasGroupsHeader()) {
            result += 1;
        }
        if (this.getUsernameFromSearch()) {
            result += 2;
        }
        return result;
    }
    getRow(actualRowIndex) {
        let virtualRowIndex = actualRowIndex;
        if (this.hasTopButton()) {
            if (virtualRowIndex === 0) {
                const topButton = this.getTopButton();
                switch (topButton) {
                    case TopButton.None:
                        break;
                    case TopButton.StartNewConversation:
                        (0, assert_1.assert)(this.phoneNumber, 'LeftPaneComposeHelper: we should have a phone number if the top button is "Start new conversation"');
                        return {
                            type: ConversationList_1.RowType.StartNewConversation,
                            phoneNumber: libphonenumberInstance_1.instance.format(this.phoneNumber, libphonenumberInstance_1.PhoneNumberFormat.E164),
                        };
                    case TopButton.CreateNewGroup:
                        return { type: ConversationList_1.RowType.CreateNewGroup };
                    default:
                        throw (0, missingCaseError_1.missingCaseError)(topButton);
                }
            }
            virtualRowIndex -= 1;
        }
        if (this.hasContactsHeader()) {
            if (virtualRowIndex === 0) {
                return {
                    type: ConversationList_1.RowType.Header,
                    i18nKey: 'contactsHeader',
                };
            }
            virtualRowIndex -= 1;
            const contact = this.composeContacts[virtualRowIndex];
            if (contact) {
                return {
                    type: ConversationList_1.RowType.Contact,
                    contact,
                };
            }
            virtualRowIndex -= this.composeContacts.length;
        }
        if (this.hasGroupsHeader()) {
            if (virtualRowIndex === 0) {
                return {
                    type: ConversationList_1.RowType.Header,
                    i18nKey: 'groupsHeader',
                };
            }
            virtualRowIndex -= 1;
            const group = this.composeGroups[virtualRowIndex];
            if (group) {
                return {
                    type: ConversationList_1.RowType.Conversation,
                    conversation: group,
                };
            }
            virtualRowIndex -= this.composeGroups.length;
        }
        const username = this.getUsernameFromSearch();
        if (username) {
            if (virtualRowIndex === 0) {
                return {
                    type: ConversationList_1.RowType.Header,
                    i18nKey: 'findByUsernameHeader',
                };
            }
            virtualRowIndex -= 1;
            if (virtualRowIndex === 0) {
                return {
                    type: ConversationList_1.RowType.UsernameSearchResult,
                    username,
                    isFetchingUsername: this.isFetchingUsername,
                };
                virtualRowIndex -= 1;
            }
        }
        return undefined;
    }
    // This is deliberately unimplemented because these keyboard shortcuts shouldn't work in
    //   the composer. The same is true for the "in direction" function below.
    getConversationAndMessageAtIndex(..._args) {
        return undefined;
    }
    getConversationAndMessageInDirection(..._args) {
        return undefined;
    }
    shouldRecomputeRowHeights(exProps) {
        const prev = new LeftPaneComposeHelper(exProps);
        const currHeaderIndices = this.getHeaderIndices();
        const prevHeaderIndices = prev.getHeaderIndices();
        return (currHeaderIndices.top !== prevHeaderIndices.top ||
            currHeaderIndices.contact !== prevHeaderIndices.contact ||
            currHeaderIndices.group !== prevHeaderIndices.group ||
            currHeaderIndices.username !== prevHeaderIndices.username);
    }
    getTopButton() {
        if (this.phoneNumber) {
            return TopButton.StartNewConversation;
        }
        if (this.searchTerm) {
            return TopButton.None;
        }
        return TopButton.CreateNewGroup;
    }
    hasTopButton() {
        return this.getTopButton() !== TopButton.None;
    }
    hasContactsHeader() {
        return Boolean(this.composeContacts.length);
    }
    hasGroupsHeader() {
        return Boolean(this.composeGroups.length);
    }
    getUsernameFromSearch() {
        if (!this.isUsernamesEnabled) {
            return undefined;
        }
        if (this.phoneNumber) {
            return undefined;
        }
        if (this.searchTerm) {
            return (0, Username_1.getUsernameFromSearch)(this.searchTerm);
        }
        return undefined;
    }
    getHeaderIndices() {
        let top;
        let contact;
        let group;
        let username;
        let rowCount = 0;
        if (this.hasTopButton()) {
            top = 0;
            rowCount += 1;
        }
        if (this.hasContactsHeader()) {
            contact = rowCount;
            rowCount += this.composeContacts.length;
        }
        if (this.hasGroupsHeader()) {
            group = rowCount;
            rowCount += this.composeContacts.length;
        }
        if (this.getUsernameFromSearch()) {
            username = rowCount;
        }
        return {
            top,
            contact,
            group,
            username,
        };
    }
}
exports.LeftPaneComposeHelper = LeftPaneComposeHelper;
function focusRef(el) {
    if (el) {
        el.focus();
    }
}
function parsePhoneNumber(str, regionCode) {
    let result;
    try {
        result = libphonenumberInstance_1.instance.parse(str, regionCode);
    }
    catch (err) {
        return undefined;
    }
    if (!libphonenumberInstance_1.instance.isValidNumber(result)) {
        return undefined;
    }
    return result;
}
