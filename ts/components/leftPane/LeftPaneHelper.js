"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPaneHelper = exports.FindDirection = void 0;
var FindDirection;
(function (FindDirection) {
    FindDirection[FindDirection["Up"] = 0] = "Up";
    FindDirection[FindDirection["Down"] = 1] = "Down";
})(FindDirection = exports.FindDirection || (exports.FindDirection = {}));
class LeftPaneHelper {
    getHeaderContents(_) {
        return null;
    }
    getBackAction(_) {
        return undefined;
    }
    getPreRowsNode(_) {
        return null;
    }
    getFooterContents(_) {
        return null;
    }
    getRowIndexToScrollTo(_selectedConversationId) {
        return undefined;
    }
    isScrollable() {
        return true;
    }
    requiresFullWidth() {
        return true;
    }
    onKeyDown(_event, _options) {
        return undefined;
    }
}
exports.LeftPaneHelper = LeftPaneHelper;
