"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPaneArchiveHelper = void 0;
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const LeftPaneHelper_1 = require("./LeftPaneHelper");
const getConversationInDirection_1 = require("./getConversationInDirection");
const ConversationList_1 = require("../ConversationList");
const LeftPaneSearchInput_1 = require("../LeftPaneSearchInput");
const LeftPaneSearchHelper_1 = require("./LeftPaneSearchHelper");
class LeftPaneArchiveHelper extends LeftPaneHelper_1.LeftPaneHelper {
    constructor(props) {
        super();
        this.archivedConversations = props.archivedConversations;
        this.searchConversation = props.searchConversation;
        this.searchTerm = props.searchTerm;
        if ('conversationResults' in props) {
            this.searchHelper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper(props);
        }
    }
    getHeaderContents({ clearSearch, i18n, showInbox, updateSearchTerm, }) {
        return (react_1.default.createElement("div", { className: "module-left-pane__header__contents" },
            react_1.default.createElement("button", { onClick: this.getBackAction({ showInbox }), className: "module-left-pane__header__contents__back-button", title: i18n('backToInbox'), "aria-label": i18n('backToInbox'), type: "button" }),
            react_1.default.createElement("div", { className: "module-left-pane__header__contents__text" }, this.searchConversation ? (react_1.default.createElement(LeftPaneSearchInput_1.LeftPaneSearchInput, { i18n: i18n, onChangeValue: newValue => {
                    updateSearchTerm(newValue);
                }, onClear: () => {
                    clearSearch();
                }, ref: el => {
                    el === null || el === void 0 ? void 0 : el.focus();
                }, searchConversation: this.searchConversation, value: this.searchTerm })) : (i18n('archivedConversations')))));
    }
    getBackAction({ showInbox }) {
        return showInbox;
    }
    getPreRowsNode({ i18n, }) {
        if (this.searchHelper) {
            return this.searchHelper.getPreRowsNode({ i18n });
        }
        return (react_1.default.createElement("div", { className: "module-left-pane__archive-helper-text" }, i18n('archiveHelperText')));
    }
    getRowCount() {
        var _a, _b;
        return ((_b = (_a = this.searchHelper) === null || _a === void 0 ? void 0 : _a.getRowCount()) !== null && _b !== void 0 ? _b : this.archivedConversations.length);
    }
    getRow(rowIndex) {
        if (this.searchHelper) {
            return this.searchHelper.getRow(rowIndex);
        }
        const conversation = this.archivedConversations[rowIndex];
        return conversation
            ? {
                type: ConversationList_1.RowType.Conversation,
                conversation,
            }
            : undefined;
    }
    getRowIndexToScrollTo(selectedConversationId) {
        if (this.searchHelper) {
            return this.searchHelper.getRowIndexToScrollTo(selectedConversationId);
        }
        if (!selectedConversationId) {
            return undefined;
        }
        const result = this.archivedConversations.findIndex(conversation => conversation.id === selectedConversationId);
        return result === -1 ? undefined : result;
    }
    getConversationAndMessageAtIndex(conversationIndex) {
        const { archivedConversations, searchHelper } = this;
        if (searchHelper) {
            return searchHelper.getConversationAndMessageAtIndex(conversationIndex);
        }
        const conversation = archivedConversations[conversationIndex] || (0, lodash_1.last)(archivedConversations);
        return conversation ? { conversationId: conversation.id } : undefined;
    }
    getConversationAndMessageInDirection(toFind, selectedConversationId, selectedMessageId) {
        if (this.searchHelper) {
            return this.searchHelper.getConversationAndMessageInDirection(toFind, selectedConversationId, selectedMessageId);
        }
        return (0, getConversationInDirection_1.getConversationInDirection)(this.archivedConversations, toFind, selectedConversationId);
    }
    shouldRecomputeRowHeights(old) {
        const hasSearchingChanged = 'conversationResults' in old !== Boolean(this.searchHelper);
        if (hasSearchingChanged) {
            return true;
        }
        if ('conversationResults' in old && this.searchHelper) {
            return this.searchHelper.shouldRecomputeRowHeights(old);
        }
        return false;
    }
    onKeyDown(event, { searchInConversation, selectedConversationId, }) {
        if (!selectedConversationId) {
            return;
        }
        const { ctrlKey, metaKey, shiftKey, key } = event;
        const commandKey = window.platform === 'darwin' && metaKey;
        const controlKey = window.platform !== 'darwin' && ctrlKey;
        const commandOrCtrl = commandKey || controlKey;
        const commandAndCtrl = commandKey && ctrlKey;
        if (commandOrCtrl &&
            !commandAndCtrl &&
            shiftKey &&
            key.toLowerCase() === 'f' &&
            this.archivedConversations.some(({ id }) => id === selectedConversationId)) {
            searchInConversation(selectedConversationId);
            event.preventDefault();
            event.stopPropagation();
        }
    }
}
exports.LeftPaneArchiveHelper = LeftPaneArchiveHelper;
