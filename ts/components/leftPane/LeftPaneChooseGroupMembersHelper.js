"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPaneChooseGroupMembersHelper = void 0;
const react_1 = __importDefault(require("react"));
const LeftPaneHelper_1 = require("./LeftPaneHelper");
const ConversationList_1 = require("../ConversationList");
const ContactCheckbox_1 = require("../conversationList/ContactCheckbox");
const ContactPills_1 = require("../ContactPills");
const ContactPill_1 = require("../ContactPill");
const SearchInput_1 = require("../SearchInput");
const AddGroupMemberErrorDialog_1 = require("../AddGroupMemberErrorDialog");
const Button_1 = require("../Button");
const limits_1 = require("../../groups/limits");
class LeftPaneChooseGroupMembersHelper extends LeftPaneHelper_1.LeftPaneHelper {
    constructor({ candidateContacts, cantAddContactForModal, isShowingMaximumGroupSizeModal, isShowingRecommendedGroupSizeModal, searchTerm, selectedContacts, }) {
        super();
        this.candidateContacts = candidateContacts;
        this.cantAddContactForModal = cantAddContactForModal;
        this.isShowingMaximumGroupSizeModal = isShowingMaximumGroupSizeModal;
        this.isShowingRecommendedGroupSizeModal =
            isShowingRecommendedGroupSizeModal;
        this.searchTerm = searchTerm;
        this.selectedContacts = selectedContacts;
        this.selectedConversationIdsSet = new Set(selectedContacts.map(contact => contact.id));
    }
    getHeaderContents({ i18n, startComposing, }) {
        const backButtonLabel = i18n('chooseGroupMembers__back-button');
        return (react_1.default.createElement("div", { className: "module-left-pane__header__contents" },
            react_1.default.createElement("button", { "aria-label": backButtonLabel, className: "module-left-pane__header__contents__back-button", onClick: this.getBackAction({ startComposing }), title: backButtonLabel, type: "button" }),
            react_1.default.createElement("div", { className: "module-left-pane__header__contents__text" }, i18n('chooseGroupMembers__title'))));
    }
    getBackAction({ startComposing, }) {
        return startComposing;
    }
    getPreRowsNode({ closeCantAddContactToGroupModal, closeMaximumGroupSizeModal, closeRecommendedGroupSizeModal, i18n, onChangeComposeSearchTerm, removeSelectedContact, }) {
        let modalNode;
        if (this.isShowingMaximumGroupSizeModal) {
            modalNode = (react_1.default.createElement(AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialog, { i18n: i18n, maximumNumberOfContacts: this.getMaximumNumberOfContacts(), mode: AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialogMode.MaximumGroupSize, onClose: closeMaximumGroupSizeModal }));
        }
        else if (this.isShowingRecommendedGroupSizeModal) {
            modalNode = (react_1.default.createElement(AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialog, { i18n: i18n, recommendedMaximumNumberOfContacts: this.getRecommendedMaximumNumberOfContacts(), mode: AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialogMode.RecommendedMaximumGroupSize, onClose: closeRecommendedGroupSizeModal }));
        }
        else if (this.cantAddContactForModal) {
            modalNode = (react_1.default.createElement(AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialog, { i18n: i18n, contact: this.cantAddContactForModal, mode: AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialogMode.CantAddContact, onClose: closeCantAddContactToGroupModal }));
        }
        return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(SearchInput_1.SearchInput, { moduleClassName: "module-left-pane__compose-search-form", onChange: onChangeComposeSearchTerm, placeholder: i18n('contactSearchPlaceholder'), ref: focusRef, value: this.searchTerm }),
            Boolean(this.selectedContacts.length) && (react_1.default.createElement(ContactPills_1.ContactPills, null, this.selectedContacts.map(contact => (react_1.default.createElement(ContactPill_1.ContactPill, { key: contact.id, acceptedMessageRequest: contact.acceptedMessageRequest, avatarPath: contact.avatarPath, color: contact.color, firstName: contact.firstName, i18n: i18n, id: contact.id, isMe: contact.isMe, name: contact.name, phoneNumber: contact.phoneNumber, profileName: contact.profileName, sharedGroupNames: contact.sharedGroupNames, title: contact.title, onClickRemove: removeSelectedContact }))))),
            this.getRowCount() ? null : (react_1.default.createElement("div", { className: "module-left-pane__compose-no-contacts" }, i18n('noContactsFound'))),
            modalNode));
    }
    getFooterContents({ i18n, startSettingGroupMetadata, }) {
        return (react_1.default.createElement(Button_1.Button, { disabled: this.hasExceededMaximumNumberOfContacts(), onClick: startSettingGroupMetadata }, this.selectedContacts.length
            ? i18n('chooseGroupMembers__next')
            : i18n('chooseGroupMembers__skip')));
    }
    getRowCount() {
        if (!this.candidateContacts.length) {
            return 0;
        }
        return this.candidateContacts.length + 2;
    }
    getRow(rowIndex) {
        if (!this.candidateContacts.length) {
            return undefined;
        }
        if (rowIndex === 0) {
            return {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'contactsHeader',
            };
        }
        // This puts a blank row for the footer.
        if (rowIndex === this.candidateContacts.length + 1) {
            return { type: ConversationList_1.RowType.Blank };
        }
        const contact = this.candidateContacts[rowIndex - 1];
        if (!contact) {
            return undefined;
        }
        const isChecked = this.selectedConversationIdsSet.has(contact.id);
        let disabledReason;
        if (!isChecked) {
            if (this.hasSelectedMaximumNumberOfContacts()) {
                disabledReason = ContactCheckbox_1.ContactCheckboxDisabledReason.MaximumContactsSelected;
            }
            else if (!contact.isGroupV2Capable) {
                disabledReason = ContactCheckbox_1.ContactCheckboxDisabledReason.NotCapable;
            }
        }
        return {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact,
            isChecked,
            disabledReason,
        };
    }
    // This is deliberately unimplemented because these keyboard shortcuts shouldn't work in
    //   the composer. The same is true for the "in direction" function below.
    getConversationAndMessageAtIndex(..._args) {
        return undefined;
    }
    getConversationAndMessageInDirection(..._args) {
        return undefined;
    }
    shouldRecomputeRowHeights(_old) {
        return false;
    }
    hasSelectedMaximumNumberOfContacts() {
        return this.selectedContacts.length >= this.getMaximumNumberOfContacts();
    }
    hasExceededMaximumNumberOfContacts() {
        // It should be impossible to reach this state. This is here as a failsafe.
        return this.selectedContacts.length > this.getMaximumNumberOfContacts();
    }
    getRecommendedMaximumNumberOfContacts() {
        return (0, limits_1.getGroupSizeRecommendedLimit)(151) - 1;
    }
    getMaximumNumberOfContacts() {
        return (0, limits_1.getGroupSizeHardLimit)(1001) - 1;
    }
}
exports.LeftPaneChooseGroupMembersHelper = LeftPaneChooseGroupMembersHelper;
function focusRef(el) {
    if (el) {
        el.focus();
    }
}
