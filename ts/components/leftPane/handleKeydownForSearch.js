"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleKeydownForSearch = void 0;
function handleKeydownForSearch(event, { searchInConversation, selectedConversationId, startSearch, }) {
    const { ctrlKey, metaKey, shiftKey, key } = event;
    const commandKey = window.platform === 'darwin' && metaKey;
    const controlKey = window.platform !== 'darwin' && ctrlKey;
    const commandOrCtrl = commandKey || controlKey;
    const commandAndCtrl = commandKey && ctrlKey;
    if (commandOrCtrl && !commandAndCtrl && key.toLowerCase() === 'f') {
        if (!shiftKey) {
            startSearch();
            event.preventDefault();
            event.stopPropagation();
        }
        else if (selectedConversationId) {
            searchInConversation(selectedConversationId);
            event.preventDefault();
            event.stopPropagation();
        }
    }
}
exports.handleKeydownForSearch = handleKeydownForSearch;
