"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPaneSetGroupMetadataHelper = void 0;
const react_1 = __importDefault(require("react"));
const LeftPaneHelper_1 = require("./LeftPaneHelper");
const ConversationList_1 = require("../ConversationList");
const DisappearingTimerSelect_1 = require("../DisappearingTimerSelect");
const Alert_1 = require("../Alert");
const AvatarEditor_1 = require("../AvatarEditor");
const AvatarPreview_1 = require("../AvatarPreview");
const Spinner_1 = require("../Spinner");
const Button_1 = require("../Button");
const Modal_1 = require("../Modal");
const GroupTitleInput_1 = require("../GroupTitleInput");
const Colors_1 = require("../../types/Colors");
class LeftPaneSetGroupMetadataHelper extends LeftPaneHelper_1.LeftPaneHelper {
    constructor({ groupAvatar, groupName, groupExpireTimer, hasError, isCreating, isEditingAvatar, selectedContacts, userAvatarData, }) {
        super();
        this.groupAvatar = groupAvatar;
        this.groupName = groupName;
        this.groupExpireTimer = groupExpireTimer;
        this.hasError = hasError;
        this.isCreating = isCreating;
        this.isEditingAvatar = isEditingAvatar;
        this.selectedContacts = selectedContacts;
        this.userAvatarData = userAvatarData;
    }
    getHeaderContents({ i18n, showChooseGroupMembers, }) {
        const backButtonLabel = i18n('setGroupMetadata__back-button');
        return (react_1.default.createElement("div", { className: "module-left-pane__header__contents" },
            react_1.default.createElement("button", { "aria-label": backButtonLabel, className: "module-left-pane__header__contents__back-button", disabled: this.isCreating, onClick: this.getBackAction({ showChooseGroupMembers }), title: backButtonLabel, type: "button" }),
            react_1.default.createElement("div", { className: "module-left-pane__header__contents__text" }, i18n('setGroupMetadata__title'))));
    }
    getBackAction({ showChooseGroupMembers, }) {
        return this.isCreating ? undefined : showChooseGroupMembers;
    }
    getPreRowsNode({ clearGroupCreationError, composeDeleteAvatarFromDisk, composeReplaceAvatar, composeSaveAvatarToDisk, createGroup, i18n, setComposeGroupAvatar, setComposeGroupExpireTimer, setComposeGroupName, toggleComposeEditingAvatar, }) {
        const [avatarColor] = Colors_1.AvatarColors;
        const disabled = this.isCreating;
        return (react_1.default.createElement("form", { className: "module-left-pane__header__form", onSubmit: event => {
                event.preventDefault();
                event.stopPropagation();
                if (!this.canCreateGroup()) {
                    return;
                }
                createGroup();
            } },
            this.isEditingAvatar && (react_1.default.createElement(Modal_1.Modal, { hasStickyButtons: true, hasXButton: true, i18n: i18n, onClose: toggleComposeEditingAvatar, title: i18n('LeftPaneSetGroupMetadataHelper__avatar-modal-title') },
                react_1.default.createElement(AvatarEditor_1.AvatarEditor, { avatarColor: avatarColor, avatarValue: this.groupAvatar, deleteAvatarFromDisk: composeDeleteAvatarFromDisk, i18n: i18n, isGroup: true, onCancel: toggleComposeEditingAvatar, onSave: newAvatar => {
                        setComposeGroupAvatar(newAvatar);
                        toggleComposeEditingAvatar();
                    }, userAvatarData: this.userAvatarData, replaceAvatar: composeReplaceAvatar, saveAvatarToDisk: composeSaveAvatarToDisk }))),
            react_1.default.createElement(AvatarPreview_1.AvatarPreview, { avatarColor: avatarColor, avatarValue: this.groupAvatar, i18n: i18n, isEditable: true, isGroup: true, onClick: toggleComposeEditingAvatar, style: {
                    height: 96,
                    margin: 0,
                    width: 96,
                } }),
            react_1.default.createElement("div", { className: "module-GroupInput--container" },
                react_1.default.createElement(GroupTitleInput_1.GroupTitleInput, { disabled: disabled, i18n: i18n, onChangeValue: setComposeGroupName, ref: focusRef, value: this.groupName })),
            react_1.default.createElement("section", { className: "module-left-pane__header__form__expire-timer" },
                react_1.default.createElement("div", { className: "module-left-pane__header__form__expire-timer__label" }, i18n('disappearingMessages')),
                react_1.default.createElement(DisappearingTimerSelect_1.DisappearingTimerSelect, { i18n: i18n, value: this.groupExpireTimer, onChange: setComposeGroupExpireTimer })),
            this.hasError && (react_1.default.createElement(Alert_1.Alert, { body: i18n('setGroupMetadata__error-message'), i18n: i18n, onClose: clearGroupCreationError }))));
    }
    getFooterContents({ createGroup, i18n, }) {
        return (react_1.default.createElement(Button_1.Button, { disabled: !this.canCreateGroup(), onClick: createGroup }, this.isCreating ? (react_1.default.createElement(Spinner_1.Spinner, { size: "20px", svgSize: "small", direction: "on-avatar" })) : (i18n('setGroupMetadata__create-group'))));
    }
    getRowCount() {
        if (!this.selectedContacts.length) {
            return 0;
        }
        return this.selectedContacts.length + 2;
    }
    getRow(rowIndex) {
        if (!this.selectedContacts.length) {
            return undefined;
        }
        if (rowIndex === 0) {
            return {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'setGroupMetadata__members-header',
            };
        }
        // This puts a blank row for the footer.
        if (rowIndex === this.selectedContacts.length + 1) {
            return { type: ConversationList_1.RowType.Blank };
        }
        const contact = this.selectedContacts[rowIndex - 1];
        return contact
            ? {
                type: ConversationList_1.RowType.Contact,
                contact,
                isClickable: false,
            }
            : undefined;
    }
    // This is deliberately unimplemented because these keyboard shortcuts shouldn't work in
    //   the composer. The same is true for the "in direction" function below.
    getConversationAndMessageAtIndex(..._args) {
        return undefined;
    }
    getConversationAndMessageInDirection(..._args) {
        return undefined;
    }
    shouldRecomputeRowHeights(_old) {
        return false;
    }
    canCreateGroup() {
        return !this.isCreating && Boolean(this.groupName.trim());
    }
}
exports.LeftPaneSetGroupMetadataHelper = LeftPaneSetGroupMetadataHelper;
function focusRef(el) {
    if (el) {
        el.focus();
    }
}
