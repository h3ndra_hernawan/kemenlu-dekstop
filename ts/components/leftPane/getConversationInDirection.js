"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.getConversationInDirection = void 0;
const lodash_1 = require("lodash");
const isConversationUnread_1 = require("../../util/isConversationUnread");
const LeftPaneHelper_1 = require("./LeftPaneHelper");
/**
 * This will look up or down in an array of conversations for the next one to select.
 * Refer to the tests for the intended behavior.
 */
const getConversationInDirection = (conversations, toFind, selectedConversationId) => {
    // As an optimization, we don't need to search if no conversation is selected.
    const selectedConversationIndex = selectedConversationId
        ? conversations.findIndex(({ id }) => id === selectedConversationId)
        : -1;
    let conversation;
    if (selectedConversationIndex < 0) {
        if (toFind.unreadOnly) {
            conversation =
                toFind.direction === LeftPaneHelper_1.FindDirection.Up
                    ? (0, lodash_1.findLast)(conversations, isConversationUnread_1.isConversationUnread)
                    : (0, lodash_1.find)(conversations, isConversationUnread_1.isConversationUnread);
        }
        else {
            conversation =
                toFind.direction === LeftPaneHelper_1.FindDirection.Up
                    ? (0, lodash_1.last)(conversations)
                    : (0, lodash_1.first)(conversations);
        }
    }
    else if (toFind.unreadOnly) {
        conversation =
            toFind.direction === LeftPaneHelper_1.FindDirection.Up
                ? (0, lodash_1.findLast)(conversations.slice(0, selectedConversationIndex), isConversationUnread_1.isConversationUnread)
                : (0, lodash_1.find)(conversations.slice(selectedConversationIndex + 1), isConversationUnread_1.isConversationUnread);
    }
    else {
        const newIndex = selectedConversationIndex +
            (toFind.direction === LeftPaneHelper_1.FindDirection.Up ? -1 : 1);
        if (newIndex < 0) {
            conversation = (0, lodash_1.last)(conversations);
        }
        else if (newIndex >= conversations.length) {
            conversation = (0, lodash_1.first)(conversations);
        }
        else {
            conversation = conversations[newIndex];
        }
    }
    return conversation ? { conversationId: conversation.id } : undefined;
};
exports.getConversationInDirection = getConversationInDirection;
