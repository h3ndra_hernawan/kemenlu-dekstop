"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPaneInboxHelper = void 0;
const lodash_1 = require("lodash");
const react_1 = __importDefault(require("react"));
const Intl_1 = require("../Intl");
const LeftPaneHelper_1 = require("./LeftPaneHelper");
const getConversationInDirection_1 = require("./getConversationInDirection");
const ConversationList_1 = require("../ConversationList");
const handleKeydownForSearch_1 = require("./handleKeydownForSearch");
class LeftPaneInboxHelper extends LeftPaneHelper_1.LeftPaneHelper {
    constructor({ conversations, archivedConversations, pinnedConversations, isAboutToSearchInAConversation, startSearchCounter, }) {
        super();
        this.conversations = conversations;
        this.archivedConversations = archivedConversations;
        this.pinnedConversations = pinnedConversations;
        this.isAboutToSearchInAConversation = isAboutToSearchInAConversation;
        this.startSearchCounter = startSearchCounter;
    }
    getRowCount() {
        const headerCount = this.hasPinnedAndNonpinned() ? 2 : 0;
        const buttonCount = this.archivedConversations.length ? 1 : 0;
        return (headerCount +
            this.pinnedConversations.length +
            this.conversations.length +
            buttonCount);
    }
    getPreRowsNode({ i18n, }) {
        if (this.getRowCount() === 0) {
            return (react_1.default.createElement("div", { className: "module-left-pane__empty" },
                react_1.default.createElement("div", null,
                    react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "emptyInboxMessage", components: [
                            react_1.default.createElement("span", null,
                                react_1.default.createElement("strong", null, i18n('composeIcon')),
                                react_1.default.createElement("span", { className: "module-left-pane__empty--composer_icon" },
                                    react_1.default.createElement("i", { className: "module-left-pane__empty--composer_icon--icon" }))),
                        ] }))));
        }
        return null;
    }
    getRow(rowIndex) {
        const { conversations, archivedConversations, pinnedConversations } = this;
        const archivedConversationsCount = archivedConversations.length;
        if (this.hasPinnedAndNonpinned()) {
            switch (rowIndex) {
                case 0:
                    return {
                        type: ConversationList_1.RowType.Header,
                        i18nKey: 'LeftPane--pinned',
                    };
                case pinnedConversations.length + 1:
                    return {
                        type: ConversationList_1.RowType.Header,
                        i18nKey: 'LeftPane--chats',
                    };
                case pinnedConversations.length + conversations.length + 2:
                    if (archivedConversationsCount) {
                        return {
                            type: ConversationList_1.RowType.ArchiveButton,
                            archivedConversationsCount,
                        };
                    }
                    return undefined;
                default: {
                    const pinnedConversation = pinnedConversations[rowIndex - 1];
                    if (pinnedConversation) {
                        return {
                            type: ConversationList_1.RowType.Conversation,
                            conversation: pinnedConversation,
                        };
                    }
                    const conversation = conversations[rowIndex - pinnedConversations.length - 2];
                    return conversation
                        ? {
                            type: ConversationList_1.RowType.Conversation,
                            conversation,
                        }
                        : undefined;
                }
            }
        }
        const onlyConversations = pinnedConversations.length
            ? pinnedConversations
            : conversations;
        if (rowIndex < onlyConversations.length) {
            const conversation = onlyConversations[rowIndex];
            return conversation
                ? {
                    type: ConversationList_1.RowType.Conversation,
                    conversation,
                }
                : undefined;
        }
        if (rowIndex === onlyConversations.length && archivedConversationsCount) {
            return {
                type: ConversationList_1.RowType.ArchiveButton,
                archivedConversationsCount,
            };
        }
        return undefined;
    }
    getRowIndexToScrollTo(selectedConversationId) {
        if (!selectedConversationId) {
            return undefined;
        }
        const isConversationSelected = (conversation) => conversation.id === selectedConversationId;
        const hasHeaders = this.hasPinnedAndNonpinned();
        const pinnedConversationIndex = this.pinnedConversations.findIndex(isConversationSelected);
        if (pinnedConversationIndex !== -1) {
            const headerOffset = hasHeaders ? 1 : 0;
            return pinnedConversationIndex + headerOffset;
        }
        const conversationIndex = this.conversations.findIndex(isConversationSelected);
        if (conversationIndex !== -1) {
            const pinnedOffset = this.pinnedConversations.length;
            const headerOffset = hasHeaders ? 2 : 0;
            return conversationIndex + pinnedOffset + headerOffset;
        }
        return undefined;
    }
    requiresFullWidth() {
        const hasNoConversations = !this.conversations.length &&
            !this.pinnedConversations.length &&
            !this.archivedConversations.length;
        return (hasNoConversations ||
            this.isAboutToSearchInAConversation ||
            Boolean(this.startSearchCounter));
    }
    shouldRecomputeRowHeights(old) {
        return old.pinnedConversations.length !== this.pinnedConversations.length;
    }
    getConversationAndMessageAtIndex(conversationIndex) {
        const { conversations, pinnedConversations } = this;
        const conversation = pinnedConversations[conversationIndex] ||
            conversations[conversationIndex - pinnedConversations.length] ||
            (0, lodash_1.last)(conversations) ||
            (0, lodash_1.last)(pinnedConversations);
        return conversation ? { conversationId: conversation.id } : undefined;
    }
    getConversationAndMessageInDirection(toFind, selectedConversationId, _selectedMessageId) {
        return (0, getConversationInDirection_1.getConversationInDirection)([...this.pinnedConversations, ...this.conversations], toFind, selectedConversationId);
    }
    onKeyDown(event, options) {
        (0, handleKeydownForSearch_1.handleKeydownForSearch)(event, options);
    }
    hasPinnedAndNonpinned() {
        return Boolean(this.pinnedConversations.length && this.conversations.length);
    }
}
exports.LeftPaneInboxHelper = LeftPaneInboxHelper;
