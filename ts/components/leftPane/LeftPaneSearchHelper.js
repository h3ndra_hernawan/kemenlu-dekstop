"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPaneSearchHelper = void 0;
const react_1 = __importDefault(require("react"));
const LeftPaneHelper_1 = require("./LeftPaneHelper");
const ConversationList_1 = require("../ConversationList");
const handleKeydownForSearch_1 = require("./handleKeydownForSearch");
const Intl_1 = require("../Intl");
const Emojify_1 = require("../conversation/Emojify");
const assert_1 = require("../../util/assert");
// The "correct" thing to do is to measure the size of the left pane and render enough
//   search results for the container height. But (1) that's slow (2) the list is
//   virtualized (3) 99 rows is over 7500px tall, taller than most monitors (4) it's fine
//   if, in some extremely tall window, we have some empty space. So we just hard-code a
//   fairly big number.
const SEARCH_RESULTS_FAKE_ROW_COUNT = 99;
const searchResultKeys = ['conversationResults', 'contactResults', 'messageResults'];
class LeftPaneSearchHelper extends LeftPaneHelper_1.LeftPaneHelper {
    constructor({ conversationResults, contactResults, messageResults, searchConversationName, primarySendsSms, searchTerm, }) {
        super();
        this.conversationResults = conversationResults;
        this.contactResults = contactResults;
        this.messageResults = messageResults;
        this.searchConversationName = searchConversationName;
        this.primarySendsSms = primarySendsSms;
        this.searchTerm = searchTerm;
    }
    getPreRowsNode({ i18n, }) {
        const mightHaveSearchResults = this.allResults().some(searchResult => searchResult.isLoading || searchResult.results.length);
        if (mightHaveSearchResults) {
            return null;
        }
        const { searchConversationName, primarySendsSms, searchTerm } = this;
        let noResults;
        if (searchConversationName) {
            noResults = (react_1.default.createElement(Intl_1.Intl, { id: "noSearchResultsInConversation", i18n: i18n, components: {
                    searchTerm,
                    conversationName: (react_1.default.createElement(Emojify_1.Emojify, { key: "item-1", text: searchConversationName })),
                } }));
        }
        else {
            noResults = (react_1.default.createElement(react_1.default.Fragment, null,
                react_1.default.createElement("div", null, i18n('noSearchResults', [searchTerm])),
                primarySendsSms && (react_1.default.createElement("div", { className: "module-left-pane__no-search-results__sms-only" }, i18n('noSearchResults--sms-only')))));
        }
        return !searchConversationName || searchTerm ? (react_1.default.createElement("div", { 
            // We need this for Ctrl-T shortcut cycling through parts of app
            tabIndex: -1, className: "module-left-pane__no-search-results", key: searchTerm }, noResults)) : null;
    }
    getRowCount() {
        if (this.isLoading()) {
            // 1 for the header.
            return 1 + SEARCH_RESULTS_FAKE_ROW_COUNT;
        }
        return this.allResults().reduce((result, searchResults) => result + getRowCountForLoadedSearchResults(searchResults), 0);
    }
    // This is currently unimplemented. See DESKTOP-1170.
    getRowIndexToScrollTo(_selectedConversationId) {
        return undefined;
    }
    getRow(rowIndex) {
        const { conversationResults, contactResults, messageResults } = this;
        if (this.isLoading()) {
            if (rowIndex === 0) {
                return { type: ConversationList_1.RowType.SearchResultsLoadingFakeHeader };
            }
            if (rowIndex + 1 <= SEARCH_RESULTS_FAKE_ROW_COUNT) {
                return { type: ConversationList_1.RowType.SearchResultsLoadingFakeRow };
            }
            return undefined;
        }
        const conversationRowCount = getRowCountForLoadedSearchResults(conversationResults);
        const contactRowCount = getRowCountForLoadedSearchResults(contactResults);
        const messageRowCount = getRowCountForLoadedSearchResults(messageResults);
        if (rowIndex < conversationRowCount) {
            if (rowIndex === 0) {
                return {
                    type: ConversationList_1.RowType.Header,
                    i18nKey: 'conversationsHeader',
                };
            }
            (0, assert_1.assert)(!conversationResults.isLoading, "We shouldn't get here with conversation results still loading");
            const conversation = conversationResults.results[rowIndex - 1];
            return conversation
                ? {
                    type: ConversationList_1.RowType.Conversation,
                    conversation,
                }
                : undefined;
        }
        if (rowIndex < conversationRowCount + contactRowCount) {
            const localIndex = rowIndex - conversationRowCount;
            if (localIndex === 0) {
                return {
                    type: ConversationList_1.RowType.Header,
                    i18nKey: 'contactsHeader',
                };
            }
            (0, assert_1.assert)(!contactResults.isLoading, "We shouldn't get here with contact results still loading");
            const conversation = contactResults.results[localIndex - 1];
            return conversation
                ? {
                    type: ConversationList_1.RowType.Conversation,
                    conversation,
                }
                : undefined;
        }
        if (rowIndex >= conversationRowCount + contactRowCount + messageRowCount) {
            return undefined;
        }
        const localIndex = rowIndex - conversationRowCount - contactRowCount;
        if (localIndex === 0) {
            return {
                type: ConversationList_1.RowType.Header,
                i18nKey: 'messagesHeader',
            };
        }
        (0, assert_1.assert)(!messageResults.isLoading, "We shouldn't get here with message results still loading");
        const message = messageResults.results[localIndex - 1];
        return message
            ? {
                type: ConversationList_1.RowType.MessageSearchResult,
                messageId: message.id,
            }
            : undefined;
    }
    isScrollable() {
        return !this.isLoading();
    }
    shouldRecomputeRowHeights(old) {
        const oldIsLoading = new LeftPaneSearchHelper(old).isLoading();
        const newIsLoading = this.isLoading();
        if (oldIsLoading && newIsLoading) {
            return false;
        }
        if (oldIsLoading !== newIsLoading) {
            return true;
        }
        return searchResultKeys.some(key => getRowCountForLoadedSearchResults(old[key]) !==
            getRowCountForLoadedSearchResults(this[key]));
    }
    // This is currently unimplemented. See DESKTOP-1170.
    getConversationAndMessageAtIndex(_conversationIndex) {
        return undefined;
    }
    // This is currently unimplemented. See DESKTOP-1170.
    getConversationAndMessageInDirection(_toFind, _selectedConversationId, _selectedMessageId) {
        return undefined;
    }
    onKeyDown(event, options) {
        (0, handleKeydownForSearch_1.handleKeydownForSearch)(event, options);
    }
    allResults() {
        return [this.conversationResults, this.contactResults, this.messageResults];
    }
    isLoading() {
        return this.allResults().some(results => results.isLoading);
    }
}
exports.LeftPaneSearchHelper = LeftPaneSearchHelper;
function getRowCountForLoadedSearchResults(searchResults) {
    // It's possible to call this helper with invalid results (e.g., ones that are loading).
    //   We could change the parameter of this function, but that adds a bunch of redundant
    //   checks that are, in the author's opinion, less clear.
    if (searchResults.isLoading) {
        (0, assert_1.assert)(false, 'getRowCountForLoadedSearchResults: Expected this to be called with loaded search results. Returning 0');
        return 0;
    }
    const resultRows = searchResults.results.length;
    const hasHeader = Boolean(resultRows);
    return (hasHeader ? 1 : 0) + resultRows;
}
