"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallBackgroundBlur = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const CallBackgroundBlur = ({ avatarPath, children, className, color, }) => {
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-calling__background', {
            [`module-background-color__${color || 'default'}`]: !avatarPath,
        }, className) },
        avatarPath && (react_1.default.createElement("div", { className: "module-calling__background--blur", style: {
                backgroundImage: `url('${encodeURI(avatarPath)}')`,
            } })),
        children));
};
exports.CallBackgroundBlur = CallBackgroundBlur;
