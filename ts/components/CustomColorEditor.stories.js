"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const CustomColorEditor_1 = require("./CustomColorEditor");
const setupI18n_1 = require("../util/setupI18n");
const story = (0, react_2.storiesOf)('Components/CustomColorEditor', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = () => ({
    i18n,
    onClose: (0, addon_actions_1.action)('onClose'),
    onSave: (0, addon_actions_1.action)('onSave'),
});
story.add('Default', () => react_1.default.createElement(CustomColorEditor_1.CustomColorEditor, Object.assign({}, createProps())));
