"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationList = exports.RowType = void 0;
const react_1 = __importStar(require("react"));
const react_virtualized_1 = require("react-virtualized");
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const getOwn_1 = require("../util/getOwn");
const missingCaseError_1 = require("../util/missingCaseError");
const assert_1 = require("../util/assert");
const Util_1 = require("../types/Util");
const _util_1 = require("./_util");
const ConversationListItem_1 = require("./conversationList/ConversationListItem");
const ContactListItem_1 = require("./conversationList/ContactListItem");
const ContactCheckbox_1 = require("./conversationList/ContactCheckbox");
const CreateNewGroupButton_1 = require("./conversationList/CreateNewGroupButton");
const StartNewConversation_1 = require("./conversationList/StartNewConversation");
const SearchResultsLoadingFakeHeader_1 = require("./conversationList/SearchResultsLoadingFakeHeader");
const SearchResultsLoadingFakeRow_1 = require("./conversationList/SearchResultsLoadingFakeRow");
const UsernameSearchResultListItem_1 = require("./conversationList/UsernameSearchResultListItem");
var RowType;
(function (RowType) {
    RowType[RowType["ArchiveButton"] = 0] = "ArchiveButton";
    RowType[RowType["Blank"] = 1] = "Blank";
    RowType[RowType["Contact"] = 2] = "Contact";
    RowType[RowType["ContactCheckbox"] = 3] = "ContactCheckbox";
    RowType[RowType["Conversation"] = 4] = "Conversation";
    RowType[RowType["CreateNewGroup"] = 5] = "CreateNewGroup";
    RowType[RowType["Header"] = 6] = "Header";
    RowType[RowType["MessageSearchResult"] = 7] = "MessageSearchResult";
    RowType[RowType["SearchResultsLoadingFakeHeader"] = 8] = "SearchResultsLoadingFakeHeader";
    RowType[RowType["SearchResultsLoadingFakeRow"] = 9] = "SearchResultsLoadingFakeRow";
    RowType[RowType["StartNewConversation"] = 10] = "StartNewConversation";
    RowType[RowType["UsernameSearchResult"] = 11] = "UsernameSearchResult";
})(RowType = exports.RowType || (exports.RowType = {}));
const NORMAL_ROW_HEIGHT = 76;
const HEADER_ROW_HEIGHT = 40;
const ConversationList = ({ badgesById, dimensions, getRow, i18n, onClickArchiveButton, onClickContactCheckbox, onSelectConversation, renderMessageSearchResult, rowCount, scrollBehavior = Util_1.ScrollBehavior.Default, scrollToRowIndex, scrollable = true, shouldRecomputeRowHeights, showChooseGroupMembers, startNewConversationFromPhoneNumber, startNewConversationFromUsername, theme, }) => {
    const listRef = (0, react_1.useRef)(null);
    (0, react_1.useEffect)(() => {
        const list = listRef.current;
        if (shouldRecomputeRowHeights && list) {
            list.recomputeRowHeights();
        }
    });
    const calculateRowHeight = (0, react_1.useCallback)(({ index }) => {
        const row = getRow(index);
        if (!row) {
            (0, assert_1.assert)(false, `Expected a row at index ${index}`);
            return NORMAL_ROW_HEIGHT;
        }
        switch (row.type) {
            case RowType.Header:
            case RowType.SearchResultsLoadingFakeHeader:
                return HEADER_ROW_HEIGHT;
            default:
                return NORMAL_ROW_HEIGHT;
        }
    }, [getRow]);
    const renderRow = (0, react_1.useCallback)(({ key, index, style }) => {
        const row = getRow(index);
        if (!row) {
            (0, assert_1.assert)(false, `Expected a row at index ${index}`);
            return react_1.default.createElement("div", { key: key, style: style });
        }
        let result;
        switch (row.type) {
            case RowType.ArchiveButton:
                result = (react_1.default.createElement("button", { "aria-label": i18n('archivedConversations'), className: "module-conversation-list__item--archive-button", onClick: onClickArchiveButton, type: "button" },
                    react_1.default.createElement("div", { className: "module-conversation-list__item--archive-button__icon" }),
                    react_1.default.createElement("span", { className: "module-conversation-list__item--archive-button__text" }, i18n('archivedConversations')),
                    react_1.default.createElement("span", { className: "module-conversation-list__item--archive-button__archived-count" }, row.archivedConversationsCount)));
                break;
            case RowType.Blank:
                result = react_1.default.createElement(react_1.default.Fragment, null);
                break;
            case RowType.Contact: {
                const { isClickable = true } = row;
                result = (react_1.default.createElement(ContactListItem_1.ContactListItem, Object.assign({}, row.contact, { onClick: isClickable ? onSelectConversation : undefined, i18n: i18n })));
                break;
            }
            case RowType.ContactCheckbox:
                result = (react_1.default.createElement(ContactCheckbox_1.ContactCheckbox, Object.assign({}, row.contact, { isChecked: row.isChecked, disabledReason: row.disabledReason, onClick: onClickContactCheckbox, i18n: i18n })));
                break;
            case RowType.Conversation: {
                const itemProps = (0, lodash_1.pick)(row.conversation, [
                    'acceptedMessageRequest',
                    'avatarPath',
                    'badges',
                    'color',
                    'draftPreview',
                    'id',
                    'isMe',
                    'isSelected',
                    'lastMessage',
                    'lastUpdated',
                    'markedUnread',
                    'muteExpiresAt',
                    'name',
                    'phoneNumber',
                    'profileName',
                    'sharedGroupNames',
                    'shouldShowDraft',
                    'title',
                    'type',
                    'typingContactId',
                    'unblurredAvatarPath',
                    'unreadCount',
                ]);
                const { badges, title, unreadCount, lastMessage } = itemProps;
                let badge;
                if (badgesById && badges[0]) {
                    badge = (0, getOwn_1.getOwn)(badgesById, badges[0].id);
                }
                result = (react_1.default.createElement("div", { "aria-label": i18n('ConversationList__aria-label', {
                        lastMessage: (0, lodash_1.get)(lastMessage, 'text') ||
                            i18n('ConversationList__last-message-undefined'),
                        title,
                        unreadCount: String(unreadCount),
                    }) },
                    react_1.default.createElement(ConversationListItem_1.ConversationListItem, Object.assign({}, itemProps, { key: key, badge: badge, onClick: onSelectConversation, i18n: i18n, theme: theme }))));
                break;
            }
            case RowType.CreateNewGroup:
                result = (react_1.default.createElement(CreateNewGroupButton_1.CreateNewGroupButton, { i18n: i18n, onClick: showChooseGroupMembers }));
                break;
            case RowType.Header:
                result = (react_1.default.createElement("div", { className: "module-conversation-list__item--header", "aria-label": i18n(row.i18nKey) }, i18n(row.i18nKey)));
                break;
            case RowType.MessageSearchResult:
                result = react_1.default.createElement(react_1.default.Fragment, null, renderMessageSearchResult(row.messageId));
                break;
            case RowType.SearchResultsLoadingFakeHeader:
                result = react_1.default.createElement(SearchResultsLoadingFakeHeader_1.SearchResultsLoadingFakeHeader, null);
                break;
            case RowType.SearchResultsLoadingFakeRow:
                result = react_1.default.createElement(SearchResultsLoadingFakeRow_1.SearchResultsLoadingFakeRow, null);
                break;
            case RowType.StartNewConversation:
                result = (react_1.default.createElement(StartNewConversation_1.StartNewConversation, { i18n: i18n, phoneNumber: row.phoneNumber, onClick: startNewConversationFromPhoneNumber }));
                break;
            case RowType.UsernameSearchResult:
                result = (react_1.default.createElement(UsernameSearchResultListItem_1.UsernameSearchResultListItem, { i18n: i18n, username: row.username, isFetchingUsername: row.isFetchingUsername, onClick: startNewConversationFromUsername }));
                break;
            default:
                throw (0, missingCaseError_1.missingCaseError)(row);
        }
        return (react_1.default.createElement("span", { "aria-rowindex": index + 1, role: "row", style: style, key: key },
            react_1.default.createElement("span", { role: "gridcell", "aria-colindex": 1 }, result)));
    }, [
        badgesById,
        getRow,
        i18n,
        onClickArchiveButton,
        onClickContactCheckbox,
        onSelectConversation,
        renderMessageSearchResult,
        showChooseGroupMembers,
        startNewConversationFromPhoneNumber,
        startNewConversationFromUsername,
        theme,
    ]);
    // Though `width` and `height` are required properties, we want to be careful in case
    //   the caller sends bogus data. Notably, react-measure's types seem to be inaccurate.
    const { width = 0, height = 0 } = dimensions || {};
    if (!width || !height) {
        return null;
    }
    const widthBreakpoint = (0, _util_1.getConversationListWidthBreakpoint)(width);
    return (react_1.default.createElement(react_virtualized_1.List, { className: (0, classnames_1.default)('module-conversation-list', `module-conversation-list--scroll-behavior-${scrollBehavior}`, `module-conversation-list--width-${widthBreakpoint}`), height: height, ref: listRef, rowCount: rowCount, rowHeight: calculateRowHeight, rowRenderer: renderRow, scrollToIndex: scrollToRowIndex, style: {
            // See `<Timeline>` for an explanation of this `any` cast.
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            overflowY: scrollable ? 'overlay' : 'hidden',
        }, tabIndex: -1, width: width }));
};
exports.ConversationList = ConversationList;
