"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_2 = require("@storybook/react");
const CaptchaDialog_1 = require("./CaptchaDialog");
const Button_1 = require("./Button");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const story = (0, react_2.storiesOf)('Components/CaptchaDialog', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
story.add('CaptchaDialog', () => {
    const [isSkipped, setIsSkipped] = (0, react_1.useState)(false);
    if (isSkipped) {
        return react_1.default.createElement(Button_1.Button, { onClick: () => setIsSkipped(false) }, "Show again");
    }
    return (react_1.default.createElement(CaptchaDialog_1.CaptchaDialog, { i18n: i18n, isPending: (0, addon_knobs_1.boolean)('isPending', false), onContinue: (0, addon_actions_1.action)('onContinue'), onSkip: () => setIsSkipped(true) }));
});
