"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const addon_actions_1 = require("@storybook/addon-actions");
const react_2 = require("@storybook/react");
const AvatarPreview_1 = require("./AvatarPreview");
const Colors_1 = require("../types/Colors");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const TEST_IMAGE = new Uint8Array((0, lodash_1.chunk)('89504e470d0a1a0a0000000d4948445200000008000000080103000000fec12cc800000006504c5445ff00ff00ff000c82e9800000001849444154085b633061a8638863a867f8c720c760c12000001a4302f4d81dd9870000000049454e44ae426082', 2).map(bytePair => parseInt(bytePair.join(''), 16)));
const createProps = (overrideProps = {}) => ({
    avatarColor: overrideProps.avatarColor,
    avatarPath: overrideProps.avatarPath,
    avatarValue: overrideProps.avatarValue,
    conversationTitle: overrideProps.conversationTitle,
    i18n,
    isEditable: Boolean(overrideProps.isEditable),
    isGroup: Boolean(overrideProps.isGroup),
    onAvatarLoaded: (0, addon_actions_1.action)('onAvatarLoaded'),
    onClear: (0, addon_actions_1.action)('onClear'),
    onClick: (0, addon_actions_1.action)('onClick'),
    style: overrideProps.style,
});
const story = (0, react_2.storiesOf)('Components/AvatarPreview', module);
story.add('No state (personal)', () => (react_1.default.createElement(AvatarPreview_1.AvatarPreview, Object.assign({}, createProps({
    avatarColor: Colors_1.AvatarColors[0],
    conversationTitle: 'Just Testing',
})))));
story.add('No state (group)', () => (react_1.default.createElement(AvatarPreview_1.AvatarPreview, Object.assign({}, createProps({
    avatarColor: Colors_1.AvatarColors[1],
    isGroup: true,
})))));
story.add('No state (group) + upload me', () => (react_1.default.createElement(AvatarPreview_1.AvatarPreview, Object.assign({}, createProps({
    avatarColor: Colors_1.AvatarColors[1],
    isEditable: true,
    isGroup: true,
})))));
story.add('value', () => (react_1.default.createElement(AvatarPreview_1.AvatarPreview, Object.assign({}, createProps({ avatarValue: TEST_IMAGE })))));
story.add('path', () => (react_1.default.createElement(AvatarPreview_1.AvatarPreview, Object.assign({}, createProps({ avatarPath: '/fixtures/kitten-3-64-64.jpg' })))));
story.add('value & path', () => (react_1.default.createElement(AvatarPreview_1.AvatarPreview, Object.assign({}, createProps({
    avatarPath: '/fixtures/kitten-3-64-64.jpg',
    avatarValue: TEST_IMAGE,
})))));
story.add('style', () => (react_1.default.createElement(AvatarPreview_1.AvatarPreview, Object.assign({}, createProps({
    avatarValue: TEST_IMAGE,
    style: { height: 100, width: 100 },
})))));
