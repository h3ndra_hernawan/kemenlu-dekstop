"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SampleMessageBubbles = void 0;
const react_1 = __importDefault(require("react"));
const formatRelativeTime_1 = require("../util/formatRelativeTime");
const A_FEW_DAYS_AGO = 60 * 60 * 24 * 5 * 1000;
const SampleMessage = ({ color = 'ultramarine', direction, i18n, text, timestamp, status, style, }) => (react_1.default.createElement("div", { className: `module-message module-message--${direction}` },
    react_1.default.createElement("div", { className: "module-message__container-outer" },
        react_1.default.createElement("div", { className: `module-message__container module-message__container--${direction} module-message__container--${direction}-${color}`, style: style },
            react_1.default.createElement("div", { dir: "auto", className: `module-message__text module-message__text--${direction}` },
                react_1.default.createElement("span", null, text)),
            react_1.default.createElement("div", { className: `module-message__metadata module-message__metadata--${direction}` },
                react_1.default.createElement("span", { className: `module-message__metadata__date module-message__metadata__date--${direction}` }, (0, formatRelativeTime_1.formatRelativeTime)(timestamp, { extended: true, i18n })),
                direction === 'outgoing' && (react_1.default.createElement("div", { className: `module-message__metadata__status-icon module-message__metadata__status-icon--${status}` })))))));
const SampleMessageBubbles = ({ backgroundStyle = {}, color, i18n, includeAnotherBubble = false, }) => {
    const firstBubbleStyle = includeAnotherBubble ? backgroundStyle : undefined;
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(SampleMessage, { color: color, direction: includeAnotherBubble ? 'outgoing' : 'incoming', i18n: i18n, text: i18n('ChatColorPicker__sampleBubble1'), timestamp: Date.now() - A_FEW_DAYS_AGO, status: "read", style: firstBubbleStyle }),
        react_1.default.createElement("br", null),
        includeAnotherBubble ? (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("br", { style: { clear: 'both' } }),
            react_1.default.createElement("br", null),
            react_1.default.createElement(SampleMessage, { direction: "incoming", i18n: i18n, text: i18n('ChatColorPicker__sampleBubble2'), timestamp: Date.now() - A_FEW_DAYS_AGO / 2, status: "read" }),
            react_1.default.createElement("br", null),
            react_1.default.createElement("br", null))) : null,
        react_1.default.createElement(SampleMessage, { color: color, direction: "outgoing", i18n: i18n, text: i18n('ChatColorPicker__sampleBubble3'), timestamp: Date.now(), status: "delivered", style: backgroundStyle }),
        react_1.default.createElement("br", { style: { clear: 'both' } })));
};
exports.SampleMessageBubbles = SampleMessageBubbles;
