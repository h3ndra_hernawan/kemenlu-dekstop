"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Slider = void 0;
const react_1 = __importStar(require("react"));
const getClassNamesFor_1 = require("../util/getClassNamesFor");
const Slider = ({ containerStyle = {}, label, handleStyle = {}, moduleClassName, onChange, value, }) => {
    const diff = (0, react_1.useRef)(0);
    const handleRef = (0, react_1.useRef)(null);
    const sliderRef = (0, react_1.useRef)(null);
    const getClassName = (0, getClassNamesFor_1.getClassNamesFor)('Slider', moduleClassName);
    const handleValueChange = (ev) => {
        if (!sliderRef || !sliderRef.current) {
            return;
        }
        let x = ev.clientX -
            diff.current -
            sliderRef.current.getBoundingClientRect().left;
        const max = sliderRef.current.offsetWidth;
        x = Math.min(max, Math.max(0, x));
        const nextValue = (100 * x) / max;
        onChange(nextValue);
        ev.preventDefault();
        ev.stopPropagation();
    };
    const handleMouseUp = () => {
        document.removeEventListener('mouseup', handleMouseUp);
        document.removeEventListener('mousemove', handleValueChange);
    };
    // We want to use React.MouseEvent here because above we
    // use the regular MouseEvent
    const handleMouseDown = (ev) => {
        if (!handleRef || !handleRef.current) {
            return;
        }
        diff.current = ev.clientX - handleRef.current.getBoundingClientRect().left;
        document.addEventListener('mousemove', handleValueChange);
        document.addEventListener('mouseup', handleMouseUp);
    };
    const handleKeyDown = (ev) => {
        let preventDefault = false;
        if (ev.key === 'ArrowRight') {
            const nextValue = value + 1;
            onChange(Math.min(nextValue, 100));
            preventDefault = true;
        }
        if (ev.key === 'ArrowLeft') {
            const nextValue = value - 1;
            onChange(Math.max(0, nextValue));
            preventDefault = true;
        }
        if (ev.key === 'Home') {
            onChange(0);
            preventDefault = true;
        }
        if (ev.key === 'End') {
            onChange(100);
            preventDefault = true;
        }
        if (preventDefault) {
            ev.preventDefault();
            ev.stopPropagation();
        }
    };
    return (react_1.default.createElement("div", { "aria-label": label, className: getClassName(''), onClick: handleValueChange, onKeyDown: handleKeyDown, ref: sliderRef, role: "button", style: containerStyle, tabIndex: 0 },
        react_1.default.createElement("div", { "aria-label": label, "aria-valuenow": value, className: getClassName('__handle'), onMouseDown: handleMouseDown, ref: handleRef, role: "slider", style: Object.assign(Object.assign({}, handleStyle), { left: `${value}%` }), tabIndex: -1 })));
};
exports.Slider = Slider;
