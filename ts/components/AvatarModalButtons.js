"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvatarModalButtons = void 0;
const react_1 = __importStar(require("react"));
const Button_1 = require("./Button");
const ConfirmDiscardDialog_1 = require("./ConfirmDiscardDialog");
const Modal_1 = require("./Modal");
const AvatarModalButtons = ({ hasChanges, i18n, onCancel, onSave, }) => {
    const [confirmDiscardAction, setConfirmDiscardAction] = (0, react_1.useState)(undefined);
    return (react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
        react_1.default.createElement(Button_1.Button, { onClick: () => {
                if (hasChanges) {
                    setConfirmDiscardAction(() => onCancel);
                }
                else {
                    onCancel();
                }
            }, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
        react_1.default.createElement(Button_1.Button, { disabled: !hasChanges, onClick: onSave }, i18n('save')),
        confirmDiscardAction && (react_1.default.createElement(ConfirmDiscardDialog_1.ConfirmDiscardDialog, { i18n: i18n, onDiscard: confirmDiscardAction, onClose: () => setConfirmDiscardAction(undefined) }))));
};
exports.AvatarModalButtons = AvatarModalButtons;
