"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const ConversationList_1 = require("./ConversationList");
const MessageSearchResult_1 = require("./conversationList/MessageSearchResult");
const ConversationListItem_1 = require("./conversationList/ConversationListItem");
const ContactCheckbox_1 = require("./conversationList/ContactCheckbox");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const StorybookThemeContext_1 = require("../../.storybook/StorybookThemeContext");
const UUID_1 = require("../types/UUID");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/ConversationList', module);
const defaultConversations = [
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'fred-convo',
        title: 'Fred Willard',
    }),
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'marc-convo',
        isSelected: true,
        unreadCount: 12,
        title: 'Marc Barraca',
        lastMessage: {
            deletedForEveryone: false,
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.',
        },
    }),
    (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'long-name-convo',
        title: 'Pablo Diego José Francisco de Paula Juan Nepomuceno María de los Remedios Cipriano de la Santísima Trinidad Ruiz y Picasso',
    }),
    (0, getDefaultConversation_1.getDefaultConversation)(),
];
const Wrapper = ({ rows, scrollable, }) => {
    const theme = (0, react_1.useContext)(StorybookThemeContext_1.StorybookThemeContext);
    return (react_1.default.createElement(ConversationList_1.ConversationList, { dimensions: {
            width: 300,
            height: 350,
        }, rowCount: rows.length, getRow: (index) => rows[index], shouldRecomputeRowHeights: false, i18n: i18n, onSelectConversation: (0, addon_actions_1.action)('onSelectConversation'), onClickArchiveButton: (0, addon_actions_1.action)('onClickArchiveButton'), onClickContactCheckbox: (0, addon_actions_1.action)('onClickContactCheckbox'), renderMessageSearchResult: (id) => (react_1.default.createElement(MessageSearchResult_1.MessageSearchResult, { body: "Lorem ipsum wow", bodyRanges: [], conversationId: "marc-convo", from: defaultConversations[0], i18n: i18n, id: id, openConversationInternal: (0, addon_actions_1.action)('openConversationInternal'), sentAt: 1587358800000, snippet: "Lorem <<left>>ipsum<<right>> wow", to: defaultConversations[1] })), scrollable: scrollable, showChooseGroupMembers: (0, addon_actions_1.action)('showChooseGroupMembers'), startNewConversationFromPhoneNumber: (0, addon_actions_1.action)('startNewConversationFromPhoneNumber'), startNewConversationFromUsername: (0, addon_actions_1.action)('startNewConversationFromUsername'), theme: theme }));
};
story.add('Archive button', () => (react_1.default.createElement(Wrapper, { rows: [{ type: ConversationList_1.RowType.ArchiveButton, archivedConversationsCount: 123 }] })));
story.add('Contact: note to self', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.Contact,
            contact: Object.assign(Object.assign({}, defaultConversations[0]), { isMe: true, about: '🤠 should be ignored' }),
        },
    ] })));
story.add('Contact: direct', () => (react_1.default.createElement(Wrapper, { rows: [{ type: ConversationList_1.RowType.Contact, contact: defaultConversations[0] }] })));
story.add('Contact: direct with short about', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.Contact,
            contact: Object.assign(Object.assign({}, defaultConversations[0]), { about: '🤠 yee haw' }),
        },
    ] })));
story.add('Contact: direct with long about', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.Contact,
            contact: Object.assign(Object.assign({}, defaultConversations[0]), { about: '🤠 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue.' }),
        },
    ] })));
story.add('Contact: group', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.Contact,
            contact: Object.assign(Object.assign({}, defaultConversations[0]), { type: 'group' }),
        },
    ] })));
story.add('Contact checkboxes', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact: defaultConversations[0],
            isChecked: true,
        },
        {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact: defaultConversations[1],
            isChecked: false,
        },
        {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact: Object.assign(Object.assign({}, defaultConversations[2]), { about: '😃 Hola' }),
            isChecked: true,
        },
    ] })));
story.add('Contact checkboxes: disabled', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact: defaultConversations[0],
            isChecked: false,
            disabledReason: ContactCheckbox_1.ContactCheckboxDisabledReason.MaximumContactsSelected,
        },
        {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact: defaultConversations[1],
            isChecked: false,
            disabledReason: ContactCheckbox_1.ContactCheckboxDisabledReason.NotCapable,
        },
        {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact: defaultConversations[2],
            isChecked: true,
            disabledReason: ContactCheckbox_1.ContactCheckboxDisabledReason.MaximumContactsSelected,
        },
        {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact: defaultConversations[3],
            isChecked: true,
            disabledReason: ContactCheckbox_1.ContactCheckboxDisabledReason.AlreadyAdded,
        },
    ] })));
{
    const createConversation = (overrideProps = {}) => (Object.assign(Object.assign({}, overrideProps), { acceptedMessageRequest: (0, addon_knobs_1.boolean)('acceptedMessageRequest', overrideProps.acceptedMessageRequest !== undefined
            ? overrideProps.acceptedMessageRequest
            : true), badges: [], isMe: (0, addon_knobs_1.boolean)('isMe', overrideProps.isMe || false), avatarPath: (0, addon_knobs_1.text)('avatarPath', overrideProps.avatarPath || ''), id: overrideProps.id || '', isSelected: (0, addon_knobs_1.boolean)('isSelected', overrideProps.isSelected || false), title: (0, addon_knobs_1.text)('title', overrideProps.title || 'Some Person'), name: overrideProps.name || 'Some Person', type: overrideProps.type || 'direct', markedUnread: (0, addon_knobs_1.boolean)('markedUnread', overrideProps.markedUnread || false), lastMessage: overrideProps.lastMessage || {
            text: (0, addon_knobs_1.text)('lastMessage.text', 'Hi there!'),
            status: (0, addon_knobs_1.select)('status', ConversationListItem_1.MessageStatuses.reduce((m, s) => (Object.assign(Object.assign({}, m), { [s]: s })), {}), 'read'),
            deletedForEveryone: false,
        }, lastUpdated: (0, addon_knobs_1.date)('lastUpdated', new Date(overrideProps.lastUpdated || Date.now() - 5 * 60 * 1000)), sharedGroupNames: [] }));
    const renderConversation = (overrideProps = {}) => (react_1.default.createElement(Wrapper, { rows: [
            {
                type: ConversationList_1.RowType.Conversation,
                conversation: createConversation(overrideProps),
            },
        ] }));
    story.add('Conversation: name', () => renderConversation());
    story.add('Conversation: name and avatar', () => renderConversation({
        avatarPath: '/fixtures/kitten-1-64-64.jpg',
    }));
    story.add('Conversation: with yourself', () => renderConversation({
        lastMessage: {
            text: 'Just a second',
            status: 'read',
            deletedForEveryone: false,
        },
        name: 'Myself',
        title: 'Myself',
        isMe: true,
    }));
    story.add('Conversations: Message Statuses', () => (react_1.default.createElement(Wrapper, { rows: ConversationListItem_1.MessageStatuses.map(status => ({
            type: ConversationList_1.RowType.Conversation,
            conversation: createConversation({
                lastMessage: { text: status, status, deletedForEveryone: false },
            }),
        })) })));
    story.add('Conversation: Typing Status', () => renderConversation({
        typingContactId: UUID_1.UUID.generate().toString(),
    }));
    story.add('Conversation: With draft', () => renderConversation({
        shouldShowDraft: true,
        draftPreview: "I'm in the middle of typing this...",
    }));
    story.add('Conversation: Deleted for everyone', () => renderConversation({
        lastMessage: { deletedForEveryone: true },
    }));
    story.add('Conversation: Message Request', () => renderConversation({
        acceptedMessageRequest: false,
        lastMessage: {
            text: 'A Message',
            status: 'delivered',
            deletedForEveryone: false,
        },
    }));
    story.add('Conversations: unread count', () => (react_1.default.createElement(Wrapper, { rows: [4, 10, 34, 250].map(unreadCount => ({
            type: ConversationList_1.RowType.Conversation,
            conversation: createConversation({
                lastMessage: {
                    text: 'Hey there!',
                    status: 'delivered',
                    deletedForEveryone: false,
                },
                unreadCount,
            }),
        })) })));
    story.add('Conversation: marked unread', () => renderConversation({ markedUnread: true }));
    story.add('Conversation: Selected', () => renderConversation({
        lastMessage: {
            text: 'Hey there!',
            status: 'read',
            deletedForEveryone: false,
        },
        isSelected: true,
    }));
    story.add('Conversation: Emoji in Message', () => renderConversation({
        lastMessage: {
            text: '🔥',
            status: 'read',
            deletedForEveryone: false,
        },
    }));
    story.add('Conversation: Link in Message', () => renderConversation({
        lastMessage: {
            text: 'Download at http://signal.org',
            status: 'read',
            deletedForEveryone: false,
        },
    }));
    story.add('Conversation: long name', () => {
        const name = 'Long contact name. Esquire. The third. And stuff. And more! And more!';
        return renderConversation({
            name,
            title: name,
        });
    });
    story.add('Conversation: Long Message', () => {
        const messages = [
            "Long line. This is a really really really long line. Really really long. Because that's just how it is",
            `Many lines. This is a many-line message.
Line 2 is really exciting but it shouldn't be seen.
Line three is even better.
Line 4, well.`,
        ];
        return (react_1.default.createElement(Wrapper, { rows: messages.map(messageText => ({
                type: ConversationList_1.RowType.Conversation,
                conversation: createConversation({
                    lastMessage: {
                        text: messageText,
                        status: 'read',
                        deletedForEveryone: false,
                    },
                }),
            })) }));
    });
    story.add('Conversations: Various Times', () => {
        const pairs = [
            [Date.now() - 5 * 60 * 60 * 1000, 'Five hours ago'],
            [Date.now() - 24 * 60 * 60 * 1000, 'One day ago'],
            [Date.now() - 7 * 24 * 60 * 60 * 1000, 'One week ago'],
            [Date.now() - 365 * 24 * 60 * 60 * 1000, 'One year ago'],
        ];
        return (react_1.default.createElement(Wrapper, { rows: pairs.map(([lastUpdated, messageText]) => ({
                type: ConversationList_1.RowType.Conversation,
                conversation: createConversation({
                    lastUpdated,
                    lastMessage: {
                        text: messageText,
                        status: 'read',
                        deletedForEveryone: false,
                    },
                }),
            })) }));
    });
    story.add('Conversation: Missing Date', () => {
        const row = {
            type: ConversationList_1.RowType.Conversation,
            conversation: (0, lodash_1.omit)(createConversation(), 'lastUpdated'),
        };
        return react_1.default.createElement(Wrapper, { rows: [row] });
    });
    story.add('Conversation: Missing Message', () => {
        const row = {
            type: ConversationList_1.RowType.Conversation,
            conversation: (0, lodash_1.omit)(createConversation(), 'lastMessage'),
        };
        return react_1.default.createElement(Wrapper, { rows: [row] });
    });
    story.add('Conversation: Missing Text', () => renderConversation({
        lastMessage: {
            text: '',
            status: 'sent',
            deletedForEveryone: false,
        },
    }));
    story.add('Conversation: Muted Conversation', () => renderConversation({
        muteExpiresAt: Date.now() + 1000 * 60 * 60,
    }));
    story.add('Conversation: At Mention', () => renderConversation({
        title: 'The Rebellion',
        type: 'group',
        lastMessage: {
            text: '@Leia Organa I know',
            status: 'read',
            deletedForEveryone: false,
        },
    }));
}
story.add('Headers', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.Header,
            i18nKey: 'conversationsHeader',
        },
        {
            type: ConversationList_1.RowType.Header,
            i18nKey: 'messagesHeader',
        },
        {
            type: ConversationList_1.RowType.Header,
            i18nKey: 'findByUsernameHeader',
        },
    ] })));
story.add('Start new conversation', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.StartNewConversation,
            phoneNumber: '+12345559876',
        },
    ] })));
story.add('Find by username', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.Header,
            i18nKey: 'findByUsernameHeader',
        },
        {
            type: ConversationList_1.RowType.UsernameSearchResult,
            username: 'jowerty',
            isFetchingUsername: false,
        },
        {
            type: ConversationList_1.RowType.UsernameSearchResult,
            username: 'jowerty',
            isFetchingUsername: true,
        },
    ] })));
story.add('Search results loading skeleton', () => (react_1.default.createElement(Wrapper, { scrollable: false, rows: [
        { type: ConversationList_1.RowType.SearchResultsLoadingFakeHeader },
        ...(0, lodash_1.times)(99, () => ({
            type: ConversationList_1.RowType.SearchResultsLoadingFakeRow,
        })),
    ] })));
story.add('Kitchen sink', () => (react_1.default.createElement(Wrapper, { rows: [
        {
            type: ConversationList_1.RowType.StartNewConversation,
            phoneNumber: '+12345559876',
        },
        {
            type: ConversationList_1.RowType.Header,
            i18nKey: 'contactsHeader',
        },
        {
            type: ConversationList_1.RowType.Contact,
            contact: defaultConversations[0],
        },
        {
            type: ConversationList_1.RowType.Header,
            i18nKey: 'messagesHeader',
        },
        {
            type: ConversationList_1.RowType.Conversation,
            conversation: defaultConversations[1],
        },
        {
            type: ConversationList_1.RowType.MessageSearchResult,
            messageId: '123',
        },
        {
            type: ConversationList_1.RowType.Header,
            i18nKey: 'findByUsernameHeader',
        },
        {
            type: ConversationList_1.RowType.UsernameSearchResult,
            username: 'jowerty',
            isFetchingUsername: false,
        },
        {
            type: ConversationList_1.RowType.ArchiveButton,
            archivedConversationsCount: 123,
        },
    ] })));
