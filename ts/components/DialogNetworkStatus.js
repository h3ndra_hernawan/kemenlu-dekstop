"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DialogNetworkStatus = void 0;
const react_1 = __importStar(require("react"));
const LeftPaneDialog_1 = require("./LeftPaneDialog");
const Spinner_1 = require("./Spinner");
const SocketStatus_1 = require("../types/SocketStatus");
const FIVE_SECONDS = 5 * 1000;
const DialogNetworkStatus = ({ containerWidthBreakpoint, hasNetworkDialog, i18n, isOnline, socketStatus, manualReconnect, }) => {
    const [isConnecting, setIsConnecting] = react_1.default.useState(socketStatus === SocketStatus_1.SocketStatus.CONNECTING);
    (0, react_1.useEffect)(() => {
        if (!hasNetworkDialog) {
            return () => null;
        }
        let timeout;
        if (isConnecting) {
            timeout = setTimeout(() => {
                setIsConnecting(false);
            }, FIVE_SECONDS);
        }
        return () => {
            if (timeout) {
                clearTimeout(timeout);
            }
        };
    }, [hasNetworkDialog, isConnecting, setIsConnecting]);
    const reconnect = () => {
        setIsConnecting(true);
        manualReconnect();
    };
    if (!hasNetworkDialog) {
        return null;
    }
    if (isConnecting) {
        const spinner = (react_1.default.createElement("div", { className: "LeftPaneDialog__spinner-container" },
            react_1.default.createElement(Spinner_1.Spinner, { direction: "on-avatar", moduleClassName: "LeftPaneDialog__spinner", size: "22px", svgSize: "small" })));
        return (react_1.default.createElement(LeftPaneDialog_1.LeftPaneDialog, { containerWidthBreakpoint: containerWidthBreakpoint, type: "warning", icon: spinner, title: i18n('connecting'), subtitle: i18n('connectingHangOn') }));
    }
    return (react_1.default.createElement(LeftPaneDialog_1.LeftPaneDialog, { containerWidthBreakpoint: containerWidthBreakpoint, type: "warning", icon: "network", title: isOnline ? i18n('disconnected') : i18n('offline'), subtitle: i18n('checkNetworkConnection'), hasAction: true, clickLabel: i18n('connect'), onClick: reconnect }));
};
exports.DialogNetworkStatus = DialogNetworkStatus;
