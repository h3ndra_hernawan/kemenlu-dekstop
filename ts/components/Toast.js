"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Toast = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const react_dom_1 = require("react-dom");
const useRestoreFocus_1 = require("../hooks/useRestoreFocus");
exports.Toast = (0, react_1.memo)(({ autoDismissDisabled = false, children, className, disableCloseOnClick = false, onClose, style, timeout = 8000, toastAction, }) => {
    const [root, setRoot] = react_1.default.useState(null);
    const [focusRef] = (0, useRestoreFocus_1.useRestoreFocus)();
    (0, react_1.useEffect)(() => {
        const div = document.createElement('div');
        document.body.appendChild(div);
        setRoot(div);
        return () => {
            document.body.removeChild(div);
            setRoot(null);
        };
    }, []);
    (0, react_1.useEffect)(() => {
        if (!root || autoDismissDisabled) {
            return;
        }
        const timeoutId = setTimeout(onClose, timeout);
        return () => {
            if (timeoutId) {
                clearTimeout(timeoutId);
            }
        };
    }, [autoDismissDisabled, onClose, root, timeout]);
    return root
        ? (0, react_dom_1.createPortal)(react_1.default.createElement("div", { "aria-live": "assertive", className: (0, classnames_1.default)('Toast', className), onClick: () => {
                if (!disableCloseOnClick) {
                    onClose();
                }
            }, onKeyDown: (ev) => {
                if (ev.key === 'Enter' || ev.key === ' ') {
                    if (!disableCloseOnClick) {
                        onClose();
                    }
                }
            }, role: "button", tabIndex: 0, style: style },
            react_1.default.createElement("div", { className: "Toast__content" }, children),
            toastAction && (react_1.default.createElement("div", { className: "Toast__button", onClick: (ev) => {
                    ev.stopPropagation();
                    ev.preventDefault();
                    toastAction.onClick();
                    onClose();
                }, onKeyDown: (ev) => {
                    if (ev.key === 'Enter' || ev.key === ' ') {
                        ev.stopPropagation();
                        ev.preventDefault();
                        toastAction.onClick();
                        onClose();
                    }
                }, ref: focusRef, role: "button", tabIndex: 0 }, toastAction.label))), root)
        : null;
});
