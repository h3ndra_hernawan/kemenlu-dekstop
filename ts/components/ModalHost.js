"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModalHost = void 0;
const react_1 = __importStar(require("react"));
const react_dom_1 = require("react-dom");
const focus_trap_react_1 = __importDefault(require("focus-trap-react"));
const web_1 = require("@react-spring/web");
const theme_1 = require("../util/theme");
const useEscapeHandling_1 = require("../hooks/useEscapeHandling");
exports.ModalHost = react_1.default.memo(({ children, noMouseClose, onClose, onEscape, theme, overlayStyles, }) => {
    const [root, setRoot] = react_1.default.useState(null);
    const [isMouseDown, setIsMouseDown] = react_1.default.useState(false);
    (0, react_1.useEffect)(() => {
        const div = document.createElement('div');
        document.body.appendChild(div);
        setRoot(div);
        return () => {
            document.body.removeChild(div);
            setRoot(null);
        };
    }, []);
    (0, useEscapeHandling_1.useEscapeHandling)(onEscape || onClose);
    // This makes it easier to write dialogs to be hosted here; they won't have to worry
    //   as much about preventing propagation of mouse events.
    const handleMouseDown = react_1.default.useCallback((e) => {
        if (e.target === e.currentTarget) {
            setIsMouseDown(true);
        }
    }, [setIsMouseDown]);
    const handleMouseUp = react_1.default.useCallback((e) => {
        setIsMouseDown(false);
        if (e.target === e.currentTarget && isMouseDown) {
            onClose();
        }
    }, [onClose, isMouseDown, setIsMouseDown]);
    return root
        ? (0, react_dom_1.createPortal)(react_1.default.createElement(focus_trap_react_1.default, { focusTrapOptions: {
                // This is alright because the overlay covers the entire screen
                allowOutsideClick: false,
            } },
            react_1.default.createElement("div", { className: theme ? (0, theme_1.themeClassName)(theme) : undefined },
                react_1.default.createElement(web_1.animated.div, { role: "presentation", className: "module-modal-host__overlay", onMouseDown: noMouseClose ? undefined : handleMouseDown, onMouseUp: noMouseClose ? undefined : handleMouseUp, style: overlayStyles }),
                react_1.default.createElement("div", { className: "module-modal-host__container" }, children))), root)
        : null;
});
