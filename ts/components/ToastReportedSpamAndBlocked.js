"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ToastReportedSpamAndBlocked = void 0;
const react_1 = __importDefault(require("react"));
const Toast_1 = require("./Toast");
const ToastReportedSpamAndBlocked = ({ i18n, onClose, }) => {
    return (react_1.default.createElement(Toast_1.Toast, { onClose: onClose }, i18n('MessageRequests--block-and-report-spam-success-toast')));
};
exports.ToastReportedSpamAndBlocked = ToastReportedSpamAndBlocked;
