"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfirmationDialog = void 0;
const react_1 = __importStar(require("react"));
const web_1 = require("@react-spring/web");
const Button_1 = require("./Button");
const ModalHost_1 = require("./ModalHost");
const Modal_1 = require("./Modal");
const useAnimated_1 = require("../hooks/useAnimated");
function focusRef(el) {
    if (el) {
        el.focus();
    }
}
function getButtonVariant(buttonStyle) {
    if (buttonStyle === 'affirmative') {
        return Button_1.ButtonVariant.Primary;
    }
    if (buttonStyle === 'negative') {
        return Button_1.ButtonVariant.Destructive;
    }
    return Button_1.ButtonVariant.Secondary;
}
exports.ConfirmationDialog = react_1.default.memo(({ moduleClassName, actions = [], cancelText, children, i18n, onCancel, onClose, theme, title, hasXButton, cancelButtonVariant, }) => {
    const { close, overlayStyles, modalStyles } = (0, useAnimated_1.useAnimated)(onClose, {
        getFrom: () => ({ opacity: 0, transform: 'scale(0.25)' }),
        getTo: isOpen => ({ opacity: isOpen ? 1 : 0, transform: 'scale(1)' }),
    });
    const cancelAndClose = (0, react_1.useCallback)(() => {
        if (onCancel) {
            onCancel();
        }
        close();
    }, [close, onCancel]);
    const handleCancel = (0, react_1.useCallback)((e) => {
        if (e.target === e.currentTarget) {
            cancelAndClose();
        }
    }, [cancelAndClose]);
    const hasActions = Boolean(actions.length);
    return (react_1.default.createElement(ModalHost_1.ModalHost, { onClose: close, theme: theme, overlayStyles: overlayStyles },
        react_1.default.createElement(web_1.animated.div, { style: modalStyles },
            react_1.default.createElement(Modal_1.ModalWindow, { hasXButton: hasXButton, i18n: i18n, moduleClassName: moduleClassName, onClose: cancelAndClose, title: title },
                children,
                react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
                    react_1.default.createElement(Button_1.Button, { onClick: handleCancel, ref: focusRef, variant: cancelButtonVariant ||
                            (hasActions ? Button_1.ButtonVariant.Secondary : Button_1.ButtonVariant.Primary) }, cancelText || i18n('confirmation-dialog--Cancel')),
                    actions.map((action, i) => (react_1.default.createElement(Button_1.Button, { key: action.text, onClick: () => {
                            action.action();
                            close();
                        }, "data-action": i, variant: getButtonVariant(action.style) }, action.text))))))));
});
