"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallManager = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const CallNeedPermissionScreen_1 = require("./CallNeedPermissionScreen");
const CallScreen_1 = require("./CallScreen");
const CallingLobby_1 = require("./CallingLobby");
const CallingParticipantsList_1 = require("./CallingParticipantsList");
const CallingSelectPresentingSourcesModal_1 = require("./CallingSelectPresentingSourcesModal");
const CallingPip_1 = require("./CallingPip");
const IncomingCallBar_1 = require("./IncomingCallBar");
const SafetyNumberChangeDialog_1 = require("./SafetyNumberChangeDialog");
const Calling_1 = require("../types/Calling");
const missingCaseError_1 = require("../util/missingCaseError");
const GROUP_CALL_RING_DURATION = 60 * 1000;
const ActiveCallManager = ({ activeCall, availableCameras, cancelCall, closeNeedPermissionScreen, hangUp, i18n, isGroupCallOutboundRingEnabled, keyChangeOk, getGroupCallVideoFrameSource, getPresentingSources, me, openSystemPreferencesAction, renderDeviceSelection, renderSafetyNumberViewer, setGroupCallVideoRequest, setLocalAudio, setLocalPreview, setLocalVideo, setPresenting, setRendererCanvas, setOutgoingRing, startCall, toggleParticipants, togglePip, toggleScreenRecordingPermissionsDialog, toggleSettings, toggleSpeakerView, }) => {
    const { conversation, hasLocalAudio, hasLocalVideo, joinedAt, peekedParticipants, pip, presentingSourcesAvailable, settingsDialogOpen, showParticipantsList, outgoingRing, } = activeCall;
    const cancelActiveCall = (0, react_1.useCallback)(() => {
        cancelCall({ conversationId: conversation.id });
    }, [cancelCall, conversation.id]);
    const joinActiveCall = (0, react_1.useCallback)(() => {
        startCall({
            callMode: activeCall.callMode,
            conversationId: conversation.id,
            hasLocalAudio,
            hasLocalVideo,
        });
    }, [
        startCall,
        activeCall.callMode,
        conversation.id,
        hasLocalAudio,
        hasLocalVideo,
    ]);
    const getGroupCallVideoFrameSourceForActiveCall = (0, react_1.useCallback)((demuxId) => {
        return getGroupCallVideoFrameSource(conversation.id, demuxId);
    }, [getGroupCallVideoFrameSource, conversation.id]);
    const setGroupCallVideoRequestForConversation = (0, react_1.useCallback)((resolutions) => {
        setGroupCallVideoRequest({
            conversationId: conversation.id,
            resolutions,
        });
    }, [setGroupCallVideoRequest, conversation.id]);
    let isCallFull;
    let showCallLobby;
    let groupMembers;
    switch (activeCall.callMode) {
        case Calling_1.CallMode.Direct: {
            const { callState, callEndedReason } = activeCall;
            const ended = callState === Calling_1.CallState.Ended;
            if (ended &&
                callEndedReason === Calling_1.CallEndedReason.RemoteHangupNeedPermission) {
                return (react_1.default.createElement(CallNeedPermissionScreen_1.CallNeedPermissionScreen, { close: closeNeedPermissionScreen, conversation: conversation, i18n: i18n }));
            }
            showCallLobby = !callState;
            isCallFull = false;
            groupMembers = undefined;
            break;
        }
        case Calling_1.CallMode.Group: {
            showCallLobby = activeCall.joinState === Calling_1.GroupCallJoinState.NotJoined;
            isCallFull = activeCall.deviceCount >= activeCall.maxDevices;
            ({ groupMembers } = activeCall);
            break;
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(activeCall);
    }
    if (showCallLobby) {
        return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(CallingLobby_1.CallingLobby, { availableCameras: availableCameras, conversation: conversation, groupMembers: groupMembers, hasLocalAudio: hasLocalAudio, hasLocalVideo: hasLocalVideo, i18n: i18n, isGroupCall: activeCall.callMode === Calling_1.CallMode.Group, isGroupCallOutboundRingEnabled: isGroupCallOutboundRingEnabled, isCallFull: isCallFull, me: me, onCallCanceled: cancelActiveCall, onJoinCall: joinActiveCall, outgoingRing: outgoingRing, peekedParticipants: peekedParticipants, setLocalPreview: setLocalPreview, setLocalAudio: setLocalAudio, setLocalVideo: setLocalVideo, setOutgoingRing: setOutgoingRing, showParticipantsList: showParticipantsList, toggleParticipants: toggleParticipants, toggleSettings: toggleSettings }),
            settingsDialogOpen && renderDeviceSelection(),
            showParticipantsList && activeCall.callMode === Calling_1.CallMode.Group ? (react_1.default.createElement(CallingParticipantsList_1.CallingParticipantsList, { i18n: i18n, onClose: toggleParticipants, ourUuid: me.uuid, participants: peekedParticipants })) : null));
    }
    if (pip) {
        return (react_1.default.createElement(CallingPip_1.CallingPip, { activeCall: activeCall, getGroupCallVideoFrameSource: getGroupCallVideoFrameSourceForActiveCall, hangUp: hangUp, hasLocalVideo: hasLocalVideo, i18n: i18n, setGroupCallVideoRequest: setGroupCallVideoRequestForConversation, setLocalPreview: setLocalPreview, setRendererCanvas: setRendererCanvas, togglePip: togglePip, toggleSpeakerView: toggleSpeakerView }));
    }
    const groupCallParticipantsForParticipantsList = activeCall.callMode === Calling_1.CallMode.Group
        ? [
            ...activeCall.remoteParticipants.map(participant => (Object.assign(Object.assign({}, participant), { hasRemoteAudio: participant.hasRemoteAudio, hasRemoteVideo: participant.hasRemoteVideo, presenting: participant.presenting }))),
            Object.assign(Object.assign({}, me), { hasRemoteAudio: hasLocalAudio, hasRemoteVideo: hasLocalVideo, presenting: Boolean(activeCall.presentingSource) }),
        ]
        : [];
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(CallScreen_1.CallScreen, { activeCall: activeCall, getPresentingSources: getPresentingSources, getGroupCallVideoFrameSource: getGroupCallVideoFrameSourceForActiveCall, groupMembers: groupMembers, hangUp: hangUp, i18n: i18n, joinedAt: joinedAt, me: me, openSystemPreferencesAction: openSystemPreferencesAction, setGroupCallVideoRequest: setGroupCallVideoRequestForConversation, setLocalPreview: setLocalPreview, setRendererCanvas: setRendererCanvas, setLocalAudio: setLocalAudio, setLocalVideo: setLocalVideo, setPresenting: setPresenting, stickyControls: showParticipantsList, toggleScreenRecordingPermissionsDialog: toggleScreenRecordingPermissionsDialog, toggleParticipants: toggleParticipants, togglePip: togglePip, toggleSettings: toggleSettings, toggleSpeakerView: toggleSpeakerView }),
        presentingSourcesAvailable && presentingSourcesAvailable.length ? (react_1.default.createElement(CallingSelectPresentingSourcesModal_1.CallingSelectPresentingSourcesModal, { i18n: i18n, presentingSourcesAvailable: presentingSourcesAvailable, setPresenting: setPresenting })) : null,
        settingsDialogOpen && renderDeviceSelection(),
        showParticipantsList && activeCall.callMode === Calling_1.CallMode.Group ? (react_1.default.createElement(CallingParticipantsList_1.CallingParticipantsList, { i18n: i18n, onClose: toggleParticipants, ourUuid: me.uuid, participants: groupCallParticipantsForParticipantsList })) : null,
        activeCall.callMode === Calling_1.CallMode.Group &&
            activeCall.conversationsWithSafetyNumberChanges.length ? (react_1.default.createElement(SafetyNumberChangeDialog_1.SafetyNumberChangeDialog, { confirmText: i18n('continueCall'), contacts: activeCall.conversationsWithSafetyNumberChanges, i18n: i18n, onCancel: () => {
                hangUp({ conversationId: activeCall.conversation.id });
            }, onConfirm: () => {
                keyChangeOk({ conversationId: activeCall.conversation.id });
            }, renderSafetyNumber: renderSafetyNumberViewer })) : null));
};
const CallManager = props => {
    const { acceptCall, activeCall, bounceAppIconStart, bounceAppIconStop, declineCall, i18n, incomingCall, notifyForCall, playRingtone, stopRingtone, setIsCallActive, setOutgoingRing, } = props;
    const isCallActive = Boolean(activeCall);
    (0, react_1.useEffect)(() => {
        setIsCallActive(isCallActive);
    }, [isCallActive, setIsCallActive]);
    const shouldRing = getShouldRing(props);
    (0, react_1.useEffect)(() => {
        if (shouldRing) {
            playRingtone();
            return () => {
                stopRingtone();
            };
        }
        stopRingtone();
        return lodash_1.noop;
    }, [shouldRing, playRingtone, stopRingtone]);
    const mightBeRingingOutgoingGroupCall = (activeCall === null || activeCall === void 0 ? void 0 : activeCall.callMode) === Calling_1.CallMode.Group &&
        activeCall.outgoingRing &&
        activeCall.joinState !== Calling_1.GroupCallJoinState.NotJoined;
    (0, react_1.useEffect)(() => {
        if (!mightBeRingingOutgoingGroupCall) {
            return lodash_1.noop;
        }
        const timeout = setTimeout(() => {
            setOutgoingRing(false);
        }, GROUP_CALL_RING_DURATION);
        return () => {
            clearTimeout(timeout);
        };
    }, [mightBeRingingOutgoingGroupCall, setOutgoingRing]);
    if (activeCall) {
        // `props` should logically have an `activeCall` at this point, but TypeScript can't
        //   figure that out, so we pass it in again.
        return react_1.default.createElement(ActiveCallManager, Object.assign({}, props, { activeCall: activeCall }));
    }
    // In the future, we may want to show the incoming call bar when a call is active.
    if (incomingCall) {
        return (react_1.default.createElement(IncomingCallBar_1.IncomingCallBar, Object.assign({ acceptCall: acceptCall, bounceAppIconStart: bounceAppIconStart, bounceAppIconStop: bounceAppIconStop, declineCall: declineCall, i18n: i18n, notifyForCall: notifyForCall }, incomingCall)));
    }
    return null;
};
exports.CallManager = CallManager;
function getShouldRing({ activeCall, incomingCall, }) {
    if (incomingCall) {
        return !activeCall;
    }
    if (!activeCall) {
        return false;
    }
    switch (activeCall.callMode) {
        case Calling_1.CallMode.Direct:
            return (activeCall.callState === Calling_1.CallState.Prering ||
                activeCall.callState === Calling_1.CallState.Ringing);
        case Calling_1.CallMode.Group:
            return (activeCall.outgoingRing &&
                (activeCall.connectionState === Calling_1.GroupCallConnectionState.Connecting ||
                    activeCall.connectionState === Calling_1.GroupCallConnectionState.Connected) &&
                activeCall.joinState !== Calling_1.GroupCallJoinState.NotJoined &&
                !activeCall.remoteParticipants.length &&
                (activeCall.conversation.sortedGroupMembers || []).length >= 2);
        default:
            throw (0, missingCaseError_1.missingCaseError)(activeCall);
    }
}
