"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const DialogUpdate_1 = require("./DialogUpdate");
const Dialogs_1 = require("../types/Dialogs");
const _util_1 = require("./_util");
const FakeLeftPaneContainer_1 = require("../test-both/helpers/FakeLeftPaneContainer");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const defaultProps = {
    containerWidthBreakpoint: _util_1.WidthBreakpoint.Wide,
    dismissDialog: (0, addon_actions_1.action)('dismiss-dialog'),
    downloadSize: 116504357,
    downloadedSize: 61003110,
    hasNetworkDialog: false,
    i18n,
    didSnooze: false,
    showEventsCount: 0,
    snoozeUpdate: (0, addon_actions_1.action)('snooze-update'),
    startUpdate: (0, addon_actions_1.action)('start-update'),
    version: 'v7.7.7',
};
const story = (0, react_1.storiesOf)('Components/DialogUpdate', module);
story.add('Knobs Playground', () => {
    const containerWidthBreakpoint = (0, addon_knobs_1.select)('containerWidthBreakpoint', _util_1.WidthBreakpoint, _util_1.WidthBreakpoint.Wide);
    const dialogType = (0, addon_knobs_1.select)('dialogType', Dialogs_1.DialogType, Dialogs_1.DialogType.Update);
    const hasNetworkDialog = (0, addon_knobs_1.boolean)('hasNetworkDialog', false);
    const didSnooze = (0, addon_knobs_1.boolean)('didSnooze', false);
    return (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogUpdate_1.DialogUpdate, Object.assign({}, defaultProps, { containerWidthBreakpoint: containerWidthBreakpoint, dialogType: dialogType, didSnooze: didSnooze, hasNetworkDialog: hasNetworkDialog, currentVersion: "5.24.0" }))));
});
[
    ['wide', _util_1.WidthBreakpoint.Wide],
    ['narrow', _util_1.WidthBreakpoint.Narrow],
].forEach(([name, containerWidthBreakpoint]) => {
    const defaultPropsForBreakpoint = Object.assign(Object.assign({}, defaultProps), { containerWidthBreakpoint });
    story.add(`Update (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogUpdate_1.DialogUpdate, Object.assign({}, defaultPropsForBreakpoint, { dialogType: Dialogs_1.DialogType.Update, currentVersion: "5.24.0" })))));
    story.add(`Download Ready (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogUpdate_1.DialogUpdate, Object.assign({}, defaultPropsForBreakpoint, { dialogType: Dialogs_1.DialogType.DownloadReady, currentVersion: "5.24.0" })))));
    story.add(`Downloading (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogUpdate_1.DialogUpdate, Object.assign({}, defaultPropsForBreakpoint, { dialogType: Dialogs_1.DialogType.Downloading, currentVersion: "5.24.0" })))));
    story.add(`Cannot Update (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogUpdate_1.DialogUpdate, Object.assign({}, defaultPropsForBreakpoint, { dialogType: Dialogs_1.DialogType.Cannot_Update, currentVersion: "5.24.0" })))));
    story.add(`Cannot Update Beta (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogUpdate_1.DialogUpdate, Object.assign({}, defaultPropsForBreakpoint, { dialogType: Dialogs_1.DialogType.Cannot_Update, currentVersion: "5.24.0-beta.1" })))));
    story.add(`macOS RO Error (${name} container)`, () => (React.createElement(FakeLeftPaneContainer_1.FakeLeftPaneContainer, { containerWidthBreakpoint: containerWidthBreakpoint },
        React.createElement(DialogUpdate_1.DialogUpdate, Object.assign({}, defaultPropsForBreakpoint, { dialogType: Dialogs_1.DialogType.MacOS_Read_Only, currentVersion: "5.24.0" })))));
});
