"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainHeader = void 0;
const react_1 = __importDefault(require("react"));
const react_popper_1 = require("react-popper");
const react_dom_1 = require("react-dom");
const Whisper_1 = require("../shims/Whisper");
const Avatar_1 = require("./Avatar");
const AvatarPopup_1 = require("./AvatarPopup");
const LeftPaneSearchInput_1 = require("./LeftPaneSearchInput");
class MainHeader extends react_1.default.Component {
    constructor(props) {
        super(props);
        this.handleOutsideClick = ({ target }) => {
            const { popperRoot, showingAvatarPopup } = this.state;
            if (showingAvatarPopup &&
                popperRoot &&
                !popperRoot.contains(target)) {
                this.hideAvatarPopup();
            }
        };
        this.showAvatarPopup = () => {
            const popperRoot = document.createElement('div');
            document.body.appendChild(popperRoot);
            this.setState({
                showingAvatarPopup: true,
                popperRoot,
            });
            document.addEventListener('click', this.handleOutsideClick);
        };
        this.hideAvatarPopup = () => {
            const { popperRoot } = this.state;
            document.removeEventListener('click', this.handleOutsideClick);
            this.setState({
                showingAvatarPopup: false,
                popperRoot: null,
            });
            if (popperRoot && document.body.contains(popperRoot)) {
                document.body.removeChild(popperRoot);
            }
        };
        this.updateSearch = (searchTerm) => {
            const { updateSearchTerm, clearConversationSearch, clearSearch, searchConversation, } = this.props;
            if (!searchTerm) {
                if (searchConversation) {
                    clearConversationSearch();
                }
                else {
                    clearSearch();
                }
                return;
            }
            if (updateSearchTerm) {
                updateSearchTerm(searchTerm);
            }
        };
        this.clearSearch = () => {
            const { clearSearch } = this.props;
            clearSearch();
            this.setFocus();
        };
        this.handleInputBlur = () => {
            const { clearSearch, searchConversation, searchTerm } = this.props;
            if (!searchConversation && !searchTerm) {
                clearSearch();
            }
        };
        this.handleGlobalKeyDown = (event) => {
            const { showingAvatarPopup } = this.state;
            const { key } = event;
            if (showingAvatarPopup && key === 'Escape') {
                this.hideAvatarPopup();
            }
        };
        this.setFocus = () => {
            if (this.inputRef.current) {
                this.inputRef.current.focus();
            }
        };
        this.setSelected = () => {
            if (this.inputRef.current) {
                this.inputRef.current.select();
            }
        };
        this.inputRef = react_1.default.createRef();
        this.state = {
            showingAvatarPopup: false,
            popperRoot: null,
        };
    }
    componentDidUpdate(prevProps) {
        var _a;
        const { searchConversation, startSearchCounter } = this.props;
        // When user chooses to search in a given conversation we focus the field for them
        if (searchConversation &&
            searchConversation.id !== ((_a = prevProps.searchConversation) === null || _a === void 0 ? void 0 : _a.id)) {
            this.setFocus();
        }
        // When user chooses to start a new search, we focus the field
        if (startSearchCounter !== prevProps.startSearchCounter) {
            this.setSelected();
        }
    }
    componentDidMount() {
        document.addEventListener('keydown', this.handleGlobalKeyDown);
    }
    componentWillUnmount() {
        const { popperRoot } = this.state;
        document.removeEventListener('click', this.handleOutsideClick);
        document.removeEventListener('keydown', this.handleGlobalKeyDown);
        if (popperRoot && document.body.contains(popperRoot)) {
            document.body.removeChild(popperRoot);
        }
    }
    render() {
        const { avatarPath, badge, color, disabled, hasPendingUpdate, i18n, name, phoneNumber, profileName, searchConversation, searchTerm, showArchivedConversations, startComposing, startUpdate, theme, title, toggleProfileEditor, } = this.props;
        const { showingAvatarPopup, popperRoot } = this.state;
        const isSearching = Boolean(searchConversation || searchTerm.trim().length);
        return (react_1.default.createElement("div", { className: "module-main-header" },
            react_1.default.createElement(react_popper_1.Manager, null,
                react_1.default.createElement(react_popper_1.Reference, null, ({ ref }) => (react_1.default.createElement("div", { className: "module-main-header__avatar--container" },
                    react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: true, avatarPath: avatarPath, badge: badge, className: "module-main-header__avatar", color: color, conversationType: "direct", i18n: i18n, isMe: true, name: name, phoneNumber: phoneNumber, profileName: profileName, theme: theme, title: title, 
                        // `sharedGroupNames` makes no sense for yourself, but
                        // `<Avatar>` needs it to determine blurring.
                        sharedGroupNames: [], size: 28, innerRef: ref, onClick: this.showAvatarPopup }),
                    hasPendingUpdate && (react_1.default.createElement("div", { className: "module-main-header__avatar--badged" }))))),
                showingAvatarPopup && popperRoot
                    ? (0, react_dom_1.createPortal)(react_1.default.createElement(react_popper_1.Popper, { placement: "bottom-end" }, ({ ref, style }) => (react_1.default.createElement(AvatarPopup_1.AvatarPopup, { acceptedMessageRequest: true, badge: badge, innerRef: ref, i18n: i18n, isMe: true, style: Object.assign(Object.assign({}, style), { zIndex: 10 }), color: color, conversationType: "direct", name: name, phoneNumber: phoneNumber, profileName: profileName, theme: theme, title: title, avatarPath: avatarPath, size: 28, hasPendingUpdate: hasPendingUpdate, startUpdate: startUpdate, 
                        // See the comment above about `sharedGroupNames`.
                        sharedGroupNames: [], onEditProfile: () => {
                            toggleProfileEditor();
                            this.hideAvatarPopup();
                        }, onViewPreferences: () => {
                            (0, Whisper_1.showSettings)();
                            this.hideAvatarPopup();
                        }, onViewArchive: () => {
                            showArchivedConversations();
                            this.hideAvatarPopup();
                        } }))), popperRoot)
                    : null),
            react_1.default.createElement(LeftPaneSearchInput_1.LeftPaneSearchInput, { disabled: disabled, i18n: i18n, onBlur: this.handleInputBlur, onChangeValue: this.updateSearch, onClear: this.clearSearch, ref: this.inputRef, searchConversation: searchConversation, value: searchTerm }),
            !isSearching && (react_1.default.createElement("button", { "aria-label": i18n('newConversation'), className: "module-main-header__compose-icon", onClick: startComposing, title: i18n('newConversation'), type: "button" }))));
    }
}
exports.MainHeader = MainHeader;
