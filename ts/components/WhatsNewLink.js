"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WhatsNewLink = void 0;
const react_1 = __importDefault(require("react"));
const Intl_1 = require("./Intl");
const WhatsNewLink = (props) => {
    const { i18n, showWhatsNewModal } = props;
    return (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "whatsNew", components: [
            react_1.default.createElement("button", { className: "WhatsNew", type: "button", onClick: showWhatsNewModal }, i18n('viewReleaseNotes')),
        ] }));
};
exports.WhatsNewLink = WhatsNewLink;
