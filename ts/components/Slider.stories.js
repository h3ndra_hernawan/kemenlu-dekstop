"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const Slider_1 = require("./Slider");
const story = (0, react_2.storiesOf)('Components/Slider', module);
const createProps = () => ({
    label: 'Slider Handle',
    onChange: (0, addon_actions_1.action)('onChange'),
    value: 30,
});
story.add('Default', () => react_1.default.createElement(Slider_1.Slider, Object.assign({}, createProps())));
story.add('Draggable Test', () => {
    function StatefulSliderController(props) {
        const [value, setValue] = (0, react_1.useState)(30);
        return react_1.default.createElement(Slider_1.Slider, Object.assign({}, props, { onChange: setValue, value: value }));
    }
    return react_1.default.createElement(StatefulSliderController, Object.assign({}, createProps()));
});
