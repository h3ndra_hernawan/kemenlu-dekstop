"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvatarIconEditor = void 0;
const react_1 = __importStar(require("react"));
const AvatarColorPicker_1 = require("./AvatarColorPicker");
const AvatarModalButtons_1 = require("./AvatarModalButtons");
const AvatarPreview_1 = require("./AvatarPreview");
const avatarDataToBytes_1 = require("../util/avatarDataToBytes");
const AvatarIconEditor = ({ avatarData: initialAvatarData, i18n, onClose, }) => {
    const [avatarBuffer, setAvatarBuffer] = (0, react_1.useState)();
    const [avatarData, setAvatarData] = (0, react_1.useState)(initialAvatarData);
    const onColorSelected = (0, react_1.useCallback)((color) => {
        setAvatarData(Object.assign(Object.assign({}, avatarData), { color }));
    }, [avatarData]);
    (0, react_1.useEffect)(() => {
        let shouldCancel = false;
        async function loadAvatar() {
            const buffer = await (0, avatarDataToBytes_1.avatarDataToBytes)(avatarData);
            if (!shouldCancel) {
                setAvatarBuffer(buffer);
            }
        }
        loadAvatar();
        return () => {
            shouldCancel = true;
        };
    }, [avatarData, setAvatarBuffer]);
    const hasChanges = avatarData !== initialAvatarData;
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(AvatarPreview_1.AvatarPreview, { avatarColor: avatarData.color, avatarValue: avatarBuffer, conversationTitle: avatarData.text, i18n: i18n }),
        react_1.default.createElement("hr", { className: "AvatarEditor__divider" }),
        react_1.default.createElement(AvatarColorPicker_1.AvatarColorPicker, { i18n: i18n, onColorSelected: onColorSelected, selectedColor: avatarData.color }),
        react_1.default.createElement(AvatarModalButtons_1.AvatarModalButtons, { hasChanges: hasChanges, i18n: i18n, onCancel: onClose, onSave: () => onClose(Object.assign(Object.assign({}, avatarData), { buffer: avatarBuffer })) })));
};
exports.AvatarIconEditor = AvatarIconEditor;
