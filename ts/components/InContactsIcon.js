"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InContactsIcon = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Tooltip_1 = require("./Tooltip");
const InContactsIcon = (props) => {
    const { className, i18n, tooltipContainerRef } = props;
    /* eslint-disable jsx-a11y/no-noninteractive-tabindex */
    return (react_1.default.createElement(Tooltip_1.Tooltip, { content: i18n('contactInAddressBook'), popperModifiers: [
            {
                name: 'preventOverflow',
                options: {
                    boundary: (tooltipContainerRef === null || tooltipContainerRef === void 0 ? void 0 : tooltipContainerRef.current) || undefined,
                },
            },
        ] },
        react_1.default.createElement("span", { "aria-label": i18n('contactInAddressBook'), className: (0, classnames_1.default)('module-in-contacts-icon__icon', className), role: "img", tabIndex: 0 })));
    /* eslint-enable jsx-a11y/no-noninteractive-tabindex */
};
exports.InContactsIcon = InContactsIcon;
