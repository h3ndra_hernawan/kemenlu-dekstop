"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileEditor = exports.EditState = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const log = __importStar(require("../logging/log"));
const Colors_1 = require("../types/Colors");
const AvatarEditor_1 = require("./AvatarEditor");
const AvatarPreview_1 = require("./AvatarPreview");
const Button_1 = require("./Button");
const ConfirmDiscardDialog_1 = require("./ConfirmDiscardDialog");
const Emoji_1 = require("./emoji/Emoji");
const EmojiButton_1 = require("./emoji/EmojiButton");
const Input_1 = require("./Input");
const Intl_1 = require("./Intl");
const Modal_1 = require("./Modal");
const PanelRow_1 = require("./conversation/conversation-details/PanelRow");
const lib_1 = require("./emoji/lib");
const missingCaseError_1 = require("../util/missingCaseError");
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const ConversationDetailsIcon_1 = require("./conversation/conversation-details/ConversationDetailsIcon");
const Spinner_1 = require("./Spinner");
const conversationsEnums_1 = require("../state/ducks/conversationsEnums");
const Username_1 = require("../types/Username");
var EditState;
(function (EditState) {
    EditState["None"] = "None";
    EditState["BetterAvatar"] = "BetterAvatar";
    EditState["ProfileName"] = "ProfileName";
    EditState["Bio"] = "Bio";
    EditState["Username"] = "Username";
})(EditState = exports.EditState || (exports.EditState = {}));
var UsernameEditState;
(function (UsernameEditState) {
    UsernameEditState["Editing"] = "Editing";
    UsernameEditState["ConfirmingDelete"] = "ConfirmingDelete";
    UsernameEditState["ShowingErrorPopup"] = "ShowingErrorPopup";
    UsernameEditState["Saving"] = "Saving";
})(UsernameEditState || (UsernameEditState = {}));
const DEFAULT_BIOS = [
    {
        i18nLabel: 'Bio--speak-freely',
        shortName: 'wave',
    },
    {
        i18nLabel: 'Bio--encrypted',
        shortName: 'zipper_mouth_face',
    },
    {
        i18nLabel: 'Bio--free-to-chat',
        shortName: '+1',
    },
    {
        i18nLabel: 'Bio--coffee-lover',
        shortName: 'coffee',
    },
    {
        i18nLabel: 'Bio--taking-break',
        shortName: 'mobile_phone_off',
    },
];
function getUsernameInvalidKey(username) {
    if (!username) {
        return undefined;
    }
    if (username.length < Username_1.MIN_USERNAME) {
        return {
            key: 'ProfileEditor--username--check-character-min',
            replacements: { min: Username_1.MIN_USERNAME },
        };
    }
    if (!/^[0-9a-z_]+$/.test(username)) {
        return { key: 'ProfileEditor--username--check-characters' };
    }
    if (!/^[a-z_]/.test(username)) {
        return { key: 'ProfileEditor--username--check-starting-character' };
    }
    if (username.length > Username_1.MAX_USERNAME) {
        return {
            key: 'ProfileEditor--username--check-character-max',
            replacements: { max: Username_1.MAX_USERNAME },
        };
    }
    return undefined;
}
function mapSaveStateToEditState({ clearUsernameSave, i18n, setEditState, setUsernameEditState, setUsernameError, usernameSaveState, }) {
    if (usernameSaveState === conversationsEnums_1.UsernameSaveState.None) {
        return;
    }
    if (usernameSaveState === conversationsEnums_1.UsernameSaveState.Saving) {
        setUsernameEditState(UsernameEditState.Saving);
        return;
    }
    clearUsernameSave();
    if (usernameSaveState === conversationsEnums_1.UsernameSaveState.Success) {
        setEditState(EditState.None);
        setUsernameEditState(UsernameEditState.Editing);
        return;
    }
    if (usernameSaveState === conversationsEnums_1.UsernameSaveState.UsernameMalformedError) {
        setUsernameEditState(UsernameEditState.Editing);
        setUsernameError(i18n('ProfileEditor--username--check-characters'));
        return;
    }
    if (usernameSaveState === conversationsEnums_1.UsernameSaveState.UsernameTakenError) {
        setUsernameEditState(UsernameEditState.Editing);
        setUsernameError(i18n('ProfileEditor--username--check-username-taken'));
        return;
    }
    if (usernameSaveState === conversationsEnums_1.UsernameSaveState.GeneralError) {
        setUsernameEditState(UsernameEditState.ShowingErrorPopup);
        return;
    }
    if (usernameSaveState === conversationsEnums_1.UsernameSaveState.DeleteFailed) {
        setUsernameEditState(UsernameEditState.Editing);
        return;
    }
    const state = usernameSaveState;
    log.error(`ProfileEditor: useEffect username didn't handle usernameSaveState '${state})'`);
    setEditState(EditState.None);
}
const ProfileEditor = ({ aboutEmoji, aboutText, avatarPath, clearUsernameSave, color, conversationId, deleteAvatarFromDisk, familyName, firstName, i18n, isUsernameFlagEnabled, onEditStateChanged, onProfileChanged, onSetSkinTone, recentEmojis, replaceAvatar, saveAvatarToDisk, saveUsername, skinTone, userAvatarData, username, usernameSaveState, }) => {
    const focusInputRef = (0, react_1.useRef)(null);
    const [editState, setEditState] = (0, react_1.useState)(EditState.None);
    const [confirmDiscardAction, setConfirmDiscardAction] = (0, react_1.useState)(undefined);
    // This is here to avoid component re-render jitters in the time it takes
    // redux to come back with the correct state
    const [fullName, setFullName] = (0, react_1.useState)({
        familyName,
        firstName,
    });
    const [fullBio, setFullBio] = (0, react_1.useState)({
        aboutEmoji,
        aboutText,
    });
    const [newUsername, setNewUsername] = (0, react_1.useState)(username);
    const [usernameError, setUsernameError] = (0, react_1.useState)();
    const [usernameEditState, setUsernameEditState] = (0, react_1.useState)(UsernameEditState.Editing);
    const [avatarBuffer, setAvatarBuffer] = (0, react_1.useState)(undefined);
    const [stagedProfile, setStagedProfile] = (0, react_1.useState)({
        aboutEmoji,
        aboutText,
        familyName,
        firstName,
    });
    // To make AvatarEditor re-render less often
    const handleBack = (0, react_1.useCallback)(() => {
        setEditState(EditState.None);
        onEditStateChanged(EditState.None);
    }, [setEditState, onEditStateChanged]);
    // To make EmojiButton re-render less often
    const setAboutEmoji = (0, react_1.useCallback)((ev) => {
        const emojiData = (0, lib_1.getEmojiData)(ev.shortName, skinTone);
        setStagedProfile(profileData => (Object.assign(Object.assign({}, profileData), { aboutEmoji: (0, lib_1.unifiedToEmoji)(emojiData.unified) })));
    }, [setStagedProfile, skinTone]);
    // To make AvatarEditor re-render less often
    const handleAvatarChanged = (0, react_1.useCallback)((avatar) => {
        setAvatarBuffer(avatar);
        setEditState(EditState.None);
        onProfileChanged(stagedProfile, avatar);
    }, [onProfileChanged, stagedProfile]);
    const getFullNameText = () => {
        return [fullName.firstName, fullName.familyName].filter(Boolean).join(' ');
    };
    (0, react_1.useEffect)(() => {
        const focusNode = focusInputRef.current;
        if (!focusNode) {
            return;
        }
        focusNode.focus();
        focusNode.setSelectionRange(focusNode.value.length, focusNode.value.length);
    }, [editState]);
    (0, react_1.useEffect)(() => {
        onEditStateChanged(editState);
    }, [editState, onEditStateChanged]);
    // If there's some in-process username save, or just an unacknowledged save
    //   completion/error, we clear it out on mount, and then again on unmount.
    (0, react_1.useEffect)(() => {
        clearUsernameSave();
        return () => {
            clearUsernameSave();
        };
    });
    (0, react_1.useEffect)(() => {
        mapSaveStateToEditState({
            clearUsernameSave,
            i18n,
            setEditState,
            setUsernameEditState,
            setUsernameError,
            usernameSaveState,
        });
    }, [
        clearUsernameSave,
        i18n,
        setEditState,
        setUsernameEditState,
        setUsernameError,
        usernameSaveState,
    ]);
    (0, react_1.useEffect)(() => {
        // Whenever the user makes a change, we'll get rid of the red error text
        setUsernameError(undefined);
        // And then we'll check the validity of that new username
        const timeout = setTimeout(() => {
            const key = getUsernameInvalidKey(newUsername);
            if (key) {
                setUsernameError(i18n(key.key, key.replacements));
            }
        }, 1000);
        return () => {
            clearTimeout(timeout);
        };
    }, [newUsername, i18n, setUsernameError]);
    const isCurrentlySaving = usernameEditState === UsernameEditState.Saving;
    const shouldDisableUsernameSave = Boolean(newUsername === username ||
        !newUsername ||
        usernameError ||
        isCurrentlySaving);
    const checkThenSaveUsername = () => {
        if (isCurrentlySaving) {
            log.error('checkThenSaveUsername: Already saving! Returning early');
            return;
        }
        if (shouldDisableUsernameSave) {
            return;
        }
        const invalidKey = getUsernameInvalidKey(newUsername);
        if (invalidKey) {
            setUsernameError(i18n(invalidKey.key, invalidKey.replacements));
            return;
        }
        setUsernameError(undefined);
        setUsernameEditState(UsernameEditState.Saving);
        saveUsername({ username: newUsername, previousUsername: username });
    };
    const deleteUsername = () => {
        if (isCurrentlySaving) {
            log.error('deleteUsername: Already saving! Returning early');
            return;
        }
        setNewUsername(undefined);
        setUsernameError(undefined);
        setUsernameEditState(UsernameEditState.Saving);
        saveUsername({ username: undefined, previousUsername: username });
    };
    // To make AvatarEditor re-render less often
    const handleAvatarLoaded = (0, react_1.useCallback)(avatar => {
        setAvatarBuffer(avatar);
    }, []);
    let content;
    if (editState === EditState.BetterAvatar) {
        content = (react_1.default.createElement(AvatarEditor_1.AvatarEditor, { avatarColor: color || Colors_1.AvatarColors[0], avatarPath: avatarPath, avatarValue: avatarBuffer, conversationId: conversationId, conversationTitle: getFullNameText(), deleteAvatarFromDisk: deleteAvatarFromDisk, i18n: i18n, onCancel: handleBack, onSave: handleAvatarChanged, userAvatarData: userAvatarData, replaceAvatar: replaceAvatar, saveAvatarToDisk: saveAvatarToDisk }));
    }
    else if (editState === EditState.ProfileName) {
        const shouldDisableSave = !stagedProfile.firstName ||
            (stagedProfile.firstName === fullName.firstName &&
                stagedProfile.familyName === fullName.familyName);
        content = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(Input_1.Input, { i18n: i18n, maxLengthCount: 26, maxByteCount: 128, whenToShowRemainingCount: 0, onChange: newFirstName => {
                    setStagedProfile(profileData => (Object.assign(Object.assign({}, profileData), { firstName: String(newFirstName) })));
                }, placeholder: i18n('ProfileEditor--first-name'), ref: focusInputRef, value: stagedProfile.firstName }),
            react_1.default.createElement(Input_1.Input, { i18n: i18n, maxLengthCount: 26, maxByteCount: 128, whenToShowRemainingCount: 0, onChange: newFamilyName => {
                    setStagedProfile(profileData => (Object.assign(Object.assign({}, profileData), { familyName: newFamilyName })));
                }, placeholder: i18n('ProfileEditor--last-name'), value: stagedProfile.familyName }),
            react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
                react_1.default.createElement(Button_1.Button, { onClick: () => {
                        const handleCancel = () => {
                            handleBack();
                            setStagedProfile(profileData => (Object.assign(Object.assign({}, profileData), { familyName,
                                firstName })));
                        };
                        const hasChanges = stagedProfile.familyName !== fullName.familyName ||
                            stagedProfile.firstName !== fullName.firstName;
                        if (hasChanges) {
                            setConfirmDiscardAction(() => handleCancel);
                        }
                        else {
                            handleCancel();
                        }
                    }, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
                react_1.default.createElement(Button_1.Button, { disabled: shouldDisableSave, onClick: () => {
                        if (!stagedProfile.firstName) {
                            return;
                        }
                        setFullName({
                            firstName: stagedProfile.firstName,
                            familyName: stagedProfile.familyName,
                        });
                        onProfileChanged(stagedProfile, avatarBuffer);
                        handleBack();
                    } }, i18n('save')))));
    }
    else if (editState === EditState.Bio) {
        const shouldDisableSave = stagedProfile.aboutText === fullBio.aboutText &&
            stagedProfile.aboutEmoji === fullBio.aboutEmoji;
        content = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(Input_1.Input, { expandable: true, hasClearButton: true, i18n: i18n, icon: react_1.default.createElement("div", { className: "module-composition-area__button-cell" },
                    react_1.default.createElement(EmojiButton_1.EmojiButton, { closeOnPick: true, emoji: stagedProfile.aboutEmoji, i18n: i18n, onPickEmoji: setAboutEmoji, onSetSkinTone: onSetSkinTone, recentEmojis: recentEmojis, skinTone: skinTone })), maxLengthCount: 140, maxByteCount: 512, moduleClassName: "ProfileEditor__about-input", onChange: value => {
                    if (value) {
                        setStagedProfile(profileData => (Object.assign(Object.assign({}, profileData), { aboutEmoji: stagedProfile.aboutEmoji, aboutText: value.replace(/(\r\n|\n|\r)/gm, '') })));
                    }
                    else {
                        setStagedProfile(profileData => (Object.assign(Object.assign({}, profileData), { aboutEmoji: undefined, aboutText: '' })));
                    }
                }, ref: focusInputRef, placeholder: i18n('ProfileEditor--about-placeholder'), value: stagedProfile.aboutText, whenToShowRemainingCount: 40 }),
            DEFAULT_BIOS.map(defaultBio => (react_1.default.createElement(PanelRow_1.PanelRow, { className: "ProfileEditor__row", key: defaultBio.shortName, icon: react_1.default.createElement("div", { className: "ProfileEditor__icon--container" },
                    react_1.default.createElement(Emoji_1.Emoji, { shortName: defaultBio.shortName, size: 24 })), label: i18n(defaultBio.i18nLabel), onClick: () => {
                    const emojiData = (0, lib_1.getEmojiData)(defaultBio.shortName, skinTone);
                    setStagedProfile(profileData => (Object.assign(Object.assign({}, profileData), { aboutEmoji: (0, lib_1.unifiedToEmoji)(emojiData.unified), aboutText: i18n(defaultBio.i18nLabel) })));
                } }))),
            react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
                react_1.default.createElement(Button_1.Button, { onClick: () => {
                        const handleCancel = () => {
                            handleBack();
                            setStagedProfile(profileData => (Object.assign(Object.assign({}, profileData), fullBio)));
                        };
                        const hasChanges = stagedProfile.aboutText !== fullBio.aboutText ||
                            stagedProfile.aboutEmoji !== fullBio.aboutEmoji;
                        if (hasChanges) {
                            setConfirmDiscardAction(() => handleCancel);
                        }
                        else {
                            handleCancel();
                        }
                    }, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
                react_1.default.createElement(Button_1.Button, { disabled: shouldDisableSave, onClick: () => {
                        setFullBio({
                            aboutEmoji: stagedProfile.aboutEmoji,
                            aboutText: stagedProfile.aboutText,
                        });
                        onProfileChanged(stagedProfile, avatarBuffer);
                        handleBack();
                    } }, i18n('save')))));
    }
    else if (editState === EditState.Username) {
        content = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(Input_1.Input, { i18n: i18n, disabled: isCurrentlySaving, onChange: changedUsername => {
                    setUsernameError(undefined);
                    setNewUsername(changedUsername);
                }, onEnter: checkThenSaveUsername, placeholder: i18n('ProfileEditor--username--placeholder'), ref: focusInputRef, value: newUsername }),
            usernameError && (react_1.default.createElement("div", { className: "ProfileEditor__error" }, usernameError)),
            react_1.default.createElement("div", { className: (0, classnames_1.default)('ProfileEditor__info', !usernameError ? 'ProfileEditor__info--no-error' : undefined) },
                react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "ProfileEditor--username--helper" })),
            react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
                react_1.default.createElement(Button_1.Button, { disabled: isCurrentlySaving, onClick: () => {
                        const handleCancel = () => {
                            handleBack();
                            setNewUsername(username);
                        };
                        const hasChanges = newUsername !== username;
                        if (hasChanges) {
                            setConfirmDiscardAction(() => handleCancel);
                        }
                        else {
                            handleCancel();
                        }
                    }, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
                react_1.default.createElement(Button_1.Button, { disabled: shouldDisableUsernameSave, onClick: checkThenSaveUsername }, isCurrentlySaving ? (react_1.default.createElement(Spinner_1.Spinner, { size: "20px", svgSize: "small", direction: "on-avatar" })) : (i18n('save'))))));
    }
    else if (editState === EditState.None) {
        content = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(AvatarPreview_1.AvatarPreview, { avatarColor: color, avatarPath: avatarPath, avatarValue: avatarBuffer, conversationTitle: getFullNameText(), i18n: i18n, isEditable: true, onAvatarLoaded: handleAvatarLoaded, onClick: () => {
                    setEditState(EditState.BetterAvatar);
                }, style: {
                    height: 80,
                    width: 80,
                } }),
            react_1.default.createElement("hr", { className: "ProfileEditor__divider" }),
            react_1.default.createElement(PanelRow_1.PanelRow, { className: "ProfileEditor__row", icon: react_1.default.createElement("i", { className: "ProfileEditor__icon--container ProfileEditor__icon ProfileEditor__icon--name" }), label: getFullNameText(), onClick: () => {
                    setEditState(EditState.ProfileName);
                } }),
            isUsernameFlagEnabled ? (react_1.default.createElement(PanelRow_1.PanelRow, { className: "ProfileEditor__row", icon: react_1.default.createElement("i", { className: "ProfileEditor__icon--container ProfileEditor__icon ProfileEditor__icon--username" }), label: username || i18n('ProfileEditor--username'), onClick: usernameEditState !== UsernameEditState.Saving
                    ? () => {
                        setNewUsername(username);
                        setEditState(EditState.Username);
                    }
                    : undefined, actions: username ? (react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('ProfileEditor--username--delete-username'), icon: usernameEditState === UsernameEditState.Saving
                        ? ConversationDetailsIcon_1.IconType.spinner
                        : ConversationDetailsIcon_1.IconType.trash, disabled: usernameEditState === UsernameEditState.Saving, fakeButton: true, onClick: () => {
                        setUsernameEditState(UsernameEditState.ConfirmingDelete);
                    } })) : null })) : null,
            react_1.default.createElement(PanelRow_1.PanelRow, { className: "ProfileEditor__row", icon: fullBio.aboutEmoji ? (react_1.default.createElement("div", { className: "ProfileEditor__icon--container" },
                    react_1.default.createElement(Emoji_1.Emoji, { emoji: fullBio.aboutEmoji, size: 24 }))) : (react_1.default.createElement("i", { className: "ProfileEditor__icon--container ProfileEditor__icon ProfileEditor__icon--bio" })), label: fullBio.aboutText || i18n('ProfileEditor--about'), onClick: () => {
                    setEditState(EditState.Bio);
                } }),
            react_1.default.createElement("hr", { className: "ProfileEditor__divider" }),
            react_1.default.createElement("div", { className: "ProfileEditor__info" },
                react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "ProfileEditor--info", components: {
                        learnMore: (react_1.default.createElement("a", { href: "https://support.signal.org/hc/en-us/articles/360007459591", target: "_blank", rel: "noreferrer" }, i18n('ProfileEditor--learnMore'))),
                    } }))));
    }
    else {
        throw (0, missingCaseError_1.missingCaseError)(editState);
    }
    return (react_1.default.createElement(react_1.default.Fragment, null,
        usernameEditState === UsernameEditState.ConfirmingDelete && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onClose: () => setUsernameEditState(UsernameEditState.Editing), actions: [
                {
                    text: i18n('ProfileEditor--username--confirm-delete-button'),
                    style: 'negative',
                    action: () => deleteUsername(),
                },
            ] }, i18n('ProfileEditor--username--confirm-delete-body'))),
        usernameEditState === UsernameEditState.ShowingErrorPopup && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { cancelText: i18n('ok'), cancelButtonVariant: Button_1.ButtonVariant.Secondary, i18n: i18n, onClose: () => setUsernameEditState(UsernameEditState.Editing) }, i18n('ProfileEditor--username--general-error'))),
        confirmDiscardAction && (react_1.default.createElement(ConfirmDiscardDialog_1.ConfirmDiscardDialog, { i18n: i18n, onDiscard: confirmDiscardAction, onClose: () => setConfirmDiscardAction(undefined) })),
        react_1.default.createElement("div", { className: "ProfileEditor" }, content)));
};
exports.ProfileEditor = ProfileEditor;
