"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallingSelectPresentingSourcesModal = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const Button_1 = require("./Button");
const Modal_1 = require("./Modal");
const theme_1 = require("../util/theme");
const Source = ({ onSourceClick, source, sourceToPresent, }) => {
    return (react_1.default.createElement("button", { className: (0, classnames_1.default)({
            'module-CallingSelectPresentingSourcesModal__source': true,
            'module-CallingSelectPresentingSourcesModal__source--selected': (sourceToPresent === null || sourceToPresent === void 0 ? void 0 : sourceToPresent.id) === source.id,
        }), key: source.id, onClick: () => {
            onSourceClick({
                id: source.id,
                name: source.name,
            });
        }, type: "button" },
        react_1.default.createElement("img", { alt: source.name, className: "module-CallingSelectPresentingSourcesModal__name--screenshot", src: source.thumbnail }),
        react_1.default.createElement("div", { className: "module-CallingSelectPresentingSourcesModal__name--container" },
            source.appIcon ? (react_1.default.createElement("img", { alt: source.name, className: "module-CallingSelectPresentingSourcesModal__name--icon", height: 16, src: source.appIcon, width: 16 })) : null,
            react_1.default.createElement("span", { className: "module-CallingSelectPresentingSourcesModal__name--text" }, source.name))));
};
const CallingSelectPresentingSourcesModal = ({ i18n, presentingSourcesAvailable, setPresenting, }) => {
    const [sourceToPresent, setSourceToPresent] = (0, react_1.useState)(undefined);
    if (!presentingSourcesAvailable.length) {
        throw new Error('No sources available for presenting');
    }
    const sources = (0, lodash_1.groupBy)(presentingSourcesAvailable, source => source.isScreen);
    return (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, i18n: i18n, moduleClassName: "module-CallingSelectPresentingSourcesModal", onClose: () => {
            setPresenting();
        }, theme: theme_1.Theme.Dark, title: i18n('calling__SelectPresentingSourcesModal--title') },
        react_1.default.createElement("div", { className: "module-CallingSelectPresentingSourcesModal__title" }, i18n('calling__SelectPresentingSourcesModal--entireScreen')),
        react_1.default.createElement("div", { className: "module-CallingSelectPresentingSourcesModal__sources" }, sources.true.map(source => (react_1.default.createElement(Source, { key: source.id, onSourceClick: selectedSource => setSourceToPresent(selectedSource), source: source, sourceToPresent: sourceToPresent })))),
        react_1.default.createElement("div", { className: "module-CallingSelectPresentingSourcesModal__title" }, i18n('calling__SelectPresentingSourcesModal--window')),
        react_1.default.createElement("div", { className: "module-CallingSelectPresentingSourcesModal__sources" }, sources.false.map(source => (react_1.default.createElement(Source, { key: source.id, onSourceClick: selectedSource => setSourceToPresent(selectedSource), source: source, sourceToPresent: sourceToPresent })))),
        react_1.default.createElement(Modal_1.Modal.ButtonFooter, { moduleClassName: "module-CallingSelectPresentingSourcesModal" },
            react_1.default.createElement(Button_1.Button, { onClick: () => setPresenting(), variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
            react_1.default.createElement(Button_1.Button, { disabled: !sourceToPresent, onClick: () => setPresenting(sourceToPresent) }, i18n('calling__SelectPresentingSourcesModal--confirm')))));
};
exports.CallingSelectPresentingSourcesModal = CallingSelectPresentingSourcesModal;
