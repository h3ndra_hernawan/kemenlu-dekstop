"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmojiButton = void 0;
const React = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const react_popper_1 = require("react-popper");
const react_dom_1 = require("react-dom");
const Emoji_1 = require("./Emoji");
const EmojiPicker_1 = require("./EmojiPicker");
exports.EmojiButton = React.memo(({ closeOnPick, emoji, i18n, doSend, onClose, onPickEmoji, skinTone, onSetSkinTone, recentEmojis, }) => {
    const [open, setOpen] = React.useState(false);
    const [popperRoot, setPopperRoot] = React.useState(null);
    const handleClickButton = React.useCallback(() => {
        if (popperRoot) {
            setOpen(false);
        }
        else {
            setOpen(true);
        }
    }, [popperRoot, setOpen]);
    const handleClose = React.useCallback(() => {
        setOpen(false);
        if (onClose) {
            onClose();
        }
    }, [setOpen, onClose]);
    // Create popper root and handle outside clicks
    React.useEffect(() => {
        if (open) {
            const root = document.createElement('div');
            setPopperRoot(root);
            document.body.appendChild(root);
            const handleOutsideClick = (event) => {
                if (!root.contains(event.target)) {
                    handleClose();
                    event.stopPropagation();
                    event.preventDefault();
                }
            };
            document.addEventListener('click', handleOutsideClick);
            return () => {
                document.body.removeChild(root);
                document.removeEventListener('click', handleOutsideClick);
                setPopperRoot(null);
            };
        }
        return lodash_1.noop;
    }, [open, setOpen, setPopperRoot, handleClose]);
    // Install keyboard shortcut to open emoji picker
    React.useEffect(() => {
        const handleKeydown = (event) => {
            const { ctrlKey, key, metaKey, shiftKey } = event;
            const commandKey = (0, lodash_1.get)(window, 'platform') === 'darwin' && metaKey;
            const controlKey = (0, lodash_1.get)(window, 'platform') !== 'darwin' && ctrlKey;
            const commandOrCtrl = commandKey || controlKey;
            // We don't want to open up if the conversation has any panels open
            const panels = document.querySelectorAll('.conversation .panel');
            if (panels && panels.length > 1) {
                return;
            }
            if (commandOrCtrl && shiftKey && (key === 'j' || key === 'J')) {
                event.stopPropagation();
                event.preventDefault();
                setOpen(!open);
            }
        };
        document.addEventListener('keydown', handleKeydown);
        return () => {
            document.removeEventListener('keydown', handleKeydown);
        };
    }, [open, setOpen]);
    return (React.createElement(react_popper_1.Manager, null,
        React.createElement(react_popper_1.Reference, null, ({ ref }) => (React.createElement("button", { type: "button", ref: ref, onClick: handleClickButton, className: (0, classnames_1.default)({
                'module-emoji-button__button': true,
                'module-emoji-button__button--active': open,
                'module-emoji-button__button--has-emoji': Boolean(emoji),
            }), "aria-label": i18n('EmojiButton__label') }, emoji && React.createElement(Emoji_1.Emoji, { emoji: emoji, size: 24 })))),
        open && popperRoot
            ? (0, react_dom_1.createPortal)(React.createElement(react_popper_1.Popper, { placement: "top-start", strategy: "fixed" }, ({ ref, style }) => (React.createElement(EmojiPicker_1.EmojiPicker, { ref: ref, i18n: i18n, style: style, onPickEmoji: ev => {
                    onPickEmoji(ev);
                    if (closeOnPick) {
                        handleClose();
                    }
                }, doSend: doSend, onClose: handleClose, skinTone: skinTone, onSetSkinTone: onSetSkinTone, recentEmojis: recentEmojis }))), popperRoot)
            : null));
});
