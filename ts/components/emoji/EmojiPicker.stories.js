"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const EmojiPicker_1 = require("./EmojiPicker");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
(0, react_1.storiesOf)('Components/Emoji/EmojiPicker', module)
    .add('Base', () => {
    return (React.createElement(EmojiPicker_1.EmojiPicker, { i18n: i18n, onPickEmoji: (0, addon_actions_1.action)('onPickEmoji'), onSetSkinTone: (0, addon_actions_1.action)('onSetSkinTone'), onClose: (0, addon_actions_1.action)('onClose'), skinTone: 0, recentEmojis: [
            'grinning',
            'grin',
            'joy',
            'rolling_on_the_floor_laughing',
            'smiley',
            'smile',
            'sweat_smile',
            'laughing',
            'wink',
            'blush',
            'yum',
            'sunglasses',
            'heart_eyes',
            'kissing_heart',
            'kissing',
            'kissing_smiling_eyes',
            'kissing_closed_eyes',
            'relaxed',
            'slightly_smiling_face',
            'hugging_face',
            'grinning_face_with_star_eyes',
            'thinking_face',
            'face_with_one_eyebrow_raised',
            'neutral_face',
            'expressionless',
            'no_mouth',
            'face_with_rolling_eyes',
            'smirk',
            'persevere',
            'disappointed_relieved',
            'open_mouth',
            'zipper_mouth_face',
        ] }));
})
    .add('No Recents', () => {
    return (React.createElement(EmojiPicker_1.EmojiPicker, { i18n: i18n, onPickEmoji: (0, addon_actions_1.action)('onPickEmoji'), onSetSkinTone: (0, addon_actions_1.action)('onSetSkinTone'), onClose: (0, addon_actions_1.action)('onClose'), skinTone: 0, recentEmojis: [] }));
})
    .add('With settings button', () => {
    return (React.createElement(EmojiPicker_1.EmojiPicker, { i18n: i18n, onPickEmoji: (0, addon_actions_1.action)('onPickEmoji'), onSetSkinTone: (0, addon_actions_1.action)('onSetSkinTone'), onClickSettings: (0, addon_actions_1.action)('onClickSettings'), onClose: (0, addon_actions_1.action)('onClose'), skinTone: 0, recentEmojis: [] }));
});
