"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const Emoji_1 = require("./Emoji");
const story = (0, react_1.storiesOf)('Components/Emoji/Emoji', module);
const tones = [0, 1, 2, 3, 4, 5];
const createProps = (overrideProps = {}) => ({
    size: (0, addon_knobs_1.select)('size', Emoji_1.EmojiSizes.reduce((m, t) => (Object.assign(Object.assign({}, m), { [t]: t })), {}), overrideProps.size || 48),
    emoji: (0, addon_knobs_1.text)('emoji', overrideProps.emoji || ''),
    shortName: (0, addon_knobs_1.text)('shortName', overrideProps.shortName || ''),
    skinTone: (0, addon_knobs_1.select)('skinTone', tones.reduce((m, t) => (Object.assign(Object.assign({}, m), { [t]: t })), {}), overrideProps.skinTone || 0),
});
story.add('Sizes', () => {
    const props = createProps({
        shortName: 'grinning_face_with_star_eyes',
    });
    return Emoji_1.EmojiSizes.map(size => React.createElement(Emoji_1.Emoji, Object.assign({ key: size }, props, { size: size })));
});
story.add('Skin Tones', () => {
    const props = createProps({
        shortName: 'raised_back_of_hand',
    });
    return tones.map(skinTone => (React.createElement(Emoji_1.Emoji, Object.assign({ key: skinTone }, props, { skinTone: skinTone }))));
});
story.add('From Emoji', () => {
    const props = createProps({
        emoji: '😂',
    });
    return React.createElement(Emoji_1.Emoji, Object.assign({}, props));
});
