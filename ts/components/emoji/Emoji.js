"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Emoji = exports.EmojiSizes = void 0;
const React = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const lib_1 = require("./lib");
exports.EmojiSizes = [16, 18, 20, 24, 28, 32, 48, 64, 66];
// the DOM structure of this Emoji should match the other emoji implementations:
// ts/components/conversation/Emojify.tsx
// ts/quill/emoji/blot.tsx
exports.Emoji = React.memo(React.forwardRef(({ className, emoji, shortName, size = 28, skinTone, style = {}, title, }, ref) => {
    let image = '';
    if (shortName) {
        image = (0, lib_1.getImagePath)(shortName, skinTone);
    }
    else if (emoji) {
        image = (0, lib_1.emojiToImage)(emoji) || '';
    }
    return (React.createElement("span", { ref: ref, className: (0, classnames_1.default)('module-emoji', `module-emoji--${size}px`, className), style: style },
        React.createElement("img", { className: `module-emoji__image--${size}px`, src: image, "aria-label": title !== null && title !== void 0 ? title : emoji, title: title !== null && title !== void 0 ? title : emoji })));
}));
