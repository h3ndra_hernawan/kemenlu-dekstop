"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallScreen = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const classnames_1 = __importDefault(require("classnames"));
const Avatar_1 = require("./Avatar");
const CallingHeader_1 = require("./CallingHeader");
const CallingPreCallInfo_1 = require("./CallingPreCallInfo");
const CallingButton_1 = require("./CallingButton");
const CallBackgroundBlur_1 = require("./CallBackgroundBlur");
const Calling_1 = require("../types/Calling");
const Colors_1 = require("../types/Colors");
const CallingToastManager_1 = require("./CallingToastManager");
const DirectCallRemoteParticipant_1 = require("./DirectCallRemoteParticipant");
const GroupCallRemoteParticipants_1 = require("./GroupCallRemoteParticipants");
const NeedsScreenRecordingPermissionsModal_1 = require("./NeedsScreenRecordingPermissionsModal");
const missingCaseError_1 = require("../util/missingCaseError");
const KeyboardLayout = __importStar(require("../services/keyboardLayout"));
const useActivateSpeakerViewOnPresenting_1 = require("../hooks/useActivateSpeakerViewOnPresenting");
function DirectCallHeaderMessage({ callState, i18n, joinedAt, }) {
    const [acceptedDuration, setAcceptedDuration] = (0, react_1.useState)();
    (0, react_1.useEffect)(() => {
        if (!joinedAt) {
            return lodash_1.noop;
        }
        // It's really jumpy with a value of 500ms.
        const interval = setInterval(() => {
            setAcceptedDuration(Date.now() - joinedAt);
        }, 100);
        return clearInterval.bind(null, interval);
    }, [joinedAt]);
    if (callState === Calling_1.CallState.Reconnecting) {
        return react_1.default.createElement(react_1.default.Fragment, null, i18n('callReconnecting'));
    }
    if (callState === Calling_1.CallState.Accepted && acceptedDuration) {
        return react_1.default.createElement(react_1.default.Fragment, null, i18n('callDuration', [renderDuration(acceptedDuration)]));
    }
    return null;
}
const CallScreen = ({ activeCall, getGroupCallVideoFrameSource, getPresentingSources, groupMembers, hangUp, i18n, joinedAt, me, openSystemPreferencesAction, setGroupCallVideoRequest, setLocalAudio, setLocalVideo, setLocalPreview, setPresenting, setRendererCanvas, stickyControls, toggleParticipants, togglePip, toggleScreenRecordingPermissionsDialog, toggleSettings, toggleSpeakerView, }) => {
    const { conversation, hasLocalAudio, hasLocalVideo, isInSpeakerView, presentingSource, remoteParticipants, showNeedsScreenRecordingPermissionsWarning, showParticipantsList, } = activeCall;
    (0, useActivateSpeakerViewOnPresenting_1.useActivateSpeakerViewOnPresenting)(remoteParticipants, isInSpeakerView, toggleSpeakerView);
    const toggleAudio = (0, react_1.useCallback)(() => {
        setLocalAudio({
            enabled: !hasLocalAudio,
        });
    }, [setLocalAudio, hasLocalAudio]);
    const toggleVideo = (0, react_1.useCallback)(() => {
        setLocalVideo({
            enabled: !hasLocalVideo,
        });
    }, [setLocalVideo, hasLocalVideo]);
    const togglePresenting = (0, react_1.useCallback)(() => {
        if (presentingSource) {
            setPresenting();
        }
        else {
            getPresentingSources();
        }
    }, [getPresentingSources, presentingSource, setPresenting]);
    const [controlsHover, setControlsHover] = (0, react_1.useState)(false);
    const onControlsMouseEnter = (0, react_1.useCallback)(() => {
        setControlsHover(true);
    }, [setControlsHover]);
    const onControlsMouseLeave = (0, react_1.useCallback)(() => {
        setControlsHover(false);
    }, [setControlsHover]);
    const [showControls, setShowControls] = (0, react_1.useState)(true);
    const localVideoRef = (0, react_1.useRef)(null);
    (0, react_1.useEffect)(() => {
        setLocalPreview({ element: localVideoRef });
        return () => {
            setLocalPreview({ element: undefined });
        };
    }, [setLocalPreview, setRendererCanvas]);
    (0, react_1.useEffect)(() => {
        if (!showControls || stickyControls || controlsHover) {
            return lodash_1.noop;
        }
        const timer = setTimeout(() => {
            setShowControls(false);
        }, 5000);
        return clearTimeout.bind(null, timer);
    }, [showControls, stickyControls, controlsHover]);
    (0, react_1.useEffect)(() => {
        const handleKeyDown = (event) => {
            let eventHandled = false;
            const key = KeyboardLayout.lookup(event);
            if (event.shiftKey && (key === 'V' || key === 'v')) {
                toggleVideo();
                eventHandled = true;
            }
            else if (event.shiftKey && (key === 'M' || key === 'm')) {
                toggleAudio();
                eventHandled = true;
            }
            if (eventHandled) {
                event.preventDefault();
                event.stopPropagation();
                setShowControls(true);
            }
        };
        document.addEventListener('keydown', handleKeyDown);
        return () => {
            document.removeEventListener('keydown', handleKeyDown);
        };
    }, [toggleAudio, toggleVideo]);
    const currentPresenter = remoteParticipants.find(participant => participant.presenting);
    const hasRemoteVideo = remoteParticipants.some(remoteParticipant => remoteParticipant.hasRemoteVideo);
    const isSendingVideo = hasLocalVideo || presentingSource;
    let isRinging;
    let hasCallStarted;
    let headerMessage;
    let headerTitle;
    let isConnected;
    let participantCount;
    let remoteParticipantsElement;
    switch (activeCall.callMode) {
        case Calling_1.CallMode.Direct: {
            isRinging =
                activeCall.callState === Calling_1.CallState.Prering ||
                    activeCall.callState === Calling_1.CallState.Ringing;
            hasCallStarted = !isRinging;
            headerMessage = (react_1.default.createElement(DirectCallHeaderMessage, { i18n: i18n, callState: activeCall.callState || Calling_1.CallState.Prering, joinedAt: joinedAt }));
            headerTitle = isRinging ? undefined : conversation.title;
            isConnected = activeCall.callState === Calling_1.CallState.Accepted;
            participantCount = isConnected ? 2 : 0;
            remoteParticipantsElement = hasCallStarted ? (react_1.default.createElement(DirectCallRemoteParticipant_1.DirectCallRemoteParticipant, { conversation: conversation, hasRemoteVideo: hasRemoteVideo, i18n: i18n, setRendererCanvas: setRendererCanvas })) : (react_1.default.createElement("div", { className: "module-ongoing-call__direct-call-ringing-spacer" }));
            break;
        }
        case Calling_1.CallMode.Group:
            isRinging =
                activeCall.outgoingRing && !activeCall.remoteParticipants.length;
            hasCallStarted = activeCall.joinState !== Calling_1.GroupCallJoinState.NotJoined;
            participantCount = activeCall.remoteParticipants.length + 1;
            if (isRinging) {
                headerTitle = undefined;
            }
            else if (currentPresenter) {
                headerTitle = i18n('calling__presenting--person-ongoing', [
                    currentPresenter.title,
                ]);
            }
            else if (!activeCall.remoteParticipants.length) {
                headerTitle = i18n('calling__in-this-call--zero');
            }
            isConnected =
                activeCall.connectionState === Calling_1.GroupCallConnectionState.Connected;
            remoteParticipantsElement = (react_1.default.createElement(GroupCallRemoteParticipants_1.GroupCallRemoteParticipants, { getGroupCallVideoFrameSource: getGroupCallVideoFrameSource, i18n: i18n, isInSpeakerView: isInSpeakerView, remoteParticipants: activeCall.remoteParticipants, setGroupCallVideoRequest: setGroupCallVideoRequest }));
            break;
        default:
            throw (0, missingCaseError_1.missingCaseError)(activeCall);
    }
    let lonelyInGroupNode;
    let localPreviewNode;
    if (activeCall.callMode === Calling_1.CallMode.Group &&
        !activeCall.remoteParticipants.length) {
        lonelyInGroupNode = (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-ongoing-call__local-preview-fullsize', presentingSource &&
                'module-ongoing-call__local-preview-fullsize--presenting') }, isSendingVideo ? (react_1.default.createElement("video", { ref: localVideoRef, autoPlay: true })) : (react_1.default.createElement(CallBackgroundBlur_1.CallBackgroundBlur, { avatarPath: me.avatarPath, color: me.color },
            react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: true, avatarPath: me.avatarPath, color: me.color || Colors_1.AvatarColors[0], noteToSelf: false, conversationType: "direct", i18n: i18n, isMe: true, name: me.name, phoneNumber: me.phoneNumber, profileName: me.profileName, title: me.title, 
                // `sharedGroupNames` makes no sense for yourself, but `<Avatar>` needs it
                //   to determine blurring.
                sharedGroupNames: [], size: 80 }),
            react_1.default.createElement("div", { className: "module-calling__camera-is-off" }, i18n('calling__your-video-is-off'))))));
    }
    else {
        localPreviewNode = isSendingVideo ? (react_1.default.createElement("video", { className: (0, classnames_1.default)('module-ongoing-call__footer__local-preview__video', presentingSource &&
                'module-ongoing-call__footer__local-preview__video--presenting'), ref: localVideoRef, autoPlay: true })) : (react_1.default.createElement(CallBackgroundBlur_1.CallBackgroundBlur, { avatarPath: me.avatarPath, color: me.color },
            react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: true, avatarPath: me.avatarPath, color: me.color || Colors_1.AvatarColors[0], noteToSelf: false, conversationType: "direct", i18n: i18n, isMe: true, name: me.name, phoneNumber: me.phoneNumber, profileName: me.profileName, title: me.title, 
                // See comment above about `sharedGroupNames`.
                sharedGroupNames: [], size: 80 })));
    }
    let videoButtonType;
    if (presentingSource) {
        videoButtonType = CallingButton_1.CallingButtonType.VIDEO_DISABLED;
    }
    else if (hasLocalVideo) {
        videoButtonType = CallingButton_1.CallingButtonType.VIDEO_ON;
    }
    else {
        videoButtonType = CallingButton_1.CallingButtonType.VIDEO_OFF;
    }
    const audioButtonType = hasLocalAudio
        ? CallingButton_1.CallingButtonType.AUDIO_ON
        : CallingButton_1.CallingButtonType.AUDIO_OFF;
    const isAudioOnly = !hasLocalVideo && !hasRemoteVideo;
    const controlsFadeClass = (0, classnames_1.default)({
        'module-ongoing-call__controls--fadeIn': (showControls || isAudioOnly) && !isConnected,
        'module-ongoing-call__controls--fadeOut': !showControls && !isAudioOnly && isConnected,
    });
    const isGroupCall = activeCall.callMode === Calling_1.CallMode.Group;
    let presentingButtonType;
    if (presentingSource) {
        presentingButtonType = CallingButton_1.CallingButtonType.PRESENTING_ON;
    }
    else if (currentPresenter) {
        presentingButtonType = CallingButton_1.CallingButtonType.PRESENTING_DISABLED;
    }
    else {
        presentingButtonType = CallingButton_1.CallingButtonType.PRESENTING_OFF;
    }
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-calling__container', `module-ongoing-call__container--${getCallModeClassSuffix(activeCall.callMode)}`, `module-ongoing-call__container--${hasCallStarted ? 'call-started' : 'call-not-started'}`), onFocus: () => {
            setShowControls(true);
        }, onMouseMove: () => {
            setShowControls(true);
        }, role: "group" },
        showNeedsScreenRecordingPermissionsWarning ? (react_1.default.createElement(NeedsScreenRecordingPermissionsModal_1.NeedsScreenRecordingPermissionsModal, { toggleScreenRecordingPermissionsDialog: toggleScreenRecordingPermissionsDialog, i18n: i18n, openSystemPreferencesAction: openSystemPreferencesAction })) : null,
        react_1.default.createElement(CallingToastManager_1.CallingToastManager, { activeCall: activeCall, i18n: i18n }),
        react_1.default.createElement("div", { className: (0, classnames_1.default)('module-ongoing-call__header', controlsFadeClass) },
            react_1.default.createElement(CallingHeader_1.CallingHeader, { i18n: i18n, isInSpeakerView: isInSpeakerView, isGroupCall: isGroupCall, message: headerMessage, participantCount: participantCount, showParticipantsList: showParticipantsList, title: headerTitle, toggleParticipants: toggleParticipants, togglePip: togglePip, toggleSettings: toggleSettings, toggleSpeakerView: toggleSpeakerView })),
        isRinging && (react_1.default.createElement(CallingPreCallInfo_1.CallingPreCallInfo, { conversation: conversation, groupMembers: groupMembers, i18n: i18n, me: me, ringMode: CallingPreCallInfo_1.RingMode.IsRinging })),
        remoteParticipantsElement,
        lonelyInGroupNode,
        react_1.default.createElement("div", { className: "module-ongoing-call__footer" },
            react_1.default.createElement("div", { className: "module-ongoing-call__footer__local-preview-offset" }),
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-ongoing-call__footer__actions', controlsFadeClass) },
                react_1.default.createElement(CallingButton_1.CallingButton, { buttonType: presentingButtonType, i18n: i18n, onMouseEnter: onControlsMouseEnter, onMouseLeave: onControlsMouseLeave, onClick: togglePresenting }),
                react_1.default.createElement(CallingButton_1.CallingButton, { buttonType: videoButtonType, i18n: i18n, onMouseEnter: onControlsMouseEnter, onMouseLeave: onControlsMouseLeave, onClick: toggleVideo }),
                react_1.default.createElement(CallingButton_1.CallingButton, { buttonType: audioButtonType, i18n: i18n, onMouseEnter: onControlsMouseEnter, onMouseLeave: onControlsMouseLeave, onClick: toggleAudio }),
                react_1.default.createElement(CallingButton_1.CallingButton, { buttonType: CallingButton_1.CallingButtonType.HANG_UP, i18n: i18n, onMouseEnter: onControlsMouseEnter, onMouseLeave: onControlsMouseLeave, onClick: () => {
                        hangUp({ conversationId: conversation.id });
                    } })),
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-ongoing-call__footer__local-preview', {
                    'module-ongoing-call__footer__local-preview--audio-muted': !hasLocalAudio,
                }) }, localPreviewNode))));
};
exports.CallScreen = CallScreen;
function getCallModeClassSuffix(callMode) {
    switch (callMode) {
        case Calling_1.CallMode.Direct:
            return 'direct';
        case Calling_1.CallMode.Group:
            return 'group';
        default:
            throw (0, missingCaseError_1.missingCaseError)(callMode);
    }
}
function renderDuration(ms) {
    const secs = Math.floor((ms / 1000) % 60)
        .toString()
        .padStart(2, '0');
    const mins = Math.floor((ms / 60000) % 60)
        .toString()
        .padStart(2, '0');
    const hours = Math.floor(ms / 3600000);
    if (hours > 0) {
        return `${hours}:${mins}:${secs}`;
    }
    return `${mins}:${secs}`;
}
