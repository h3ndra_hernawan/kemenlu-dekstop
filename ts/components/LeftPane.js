"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPane = exports.LeftPaneMode = void 0;
const react_1 = __importStar(require("react"));
const react_measure_1 = __importDefault(require("react-measure"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const LeftPaneHelper_1 = require("./leftPane/LeftPaneHelper");
const LeftPaneInboxHelper_1 = require("./leftPane/LeftPaneInboxHelper");
const LeftPaneSearchHelper_1 = require("./leftPane/LeftPaneSearchHelper");
const LeftPaneArchiveHelper_1 = require("./leftPane/LeftPaneArchiveHelper");
const LeftPaneComposeHelper_1 = require("./leftPane/LeftPaneComposeHelper");
const LeftPaneChooseGroupMembersHelper_1 = require("./leftPane/LeftPaneChooseGroupMembersHelper");
const LeftPaneSetGroupMetadataHelper_1 = require("./leftPane/LeftPaneSetGroupMetadataHelper");
const OS = __importStar(require("../OS"));
const Util_1 = require("../types/Util");
const usePrevious_1 = require("../hooks/usePrevious");
const missingCaseError_1 = require("../util/missingCaseError");
const assert_1 = require("../util/assert");
const isSorted_1 = require("../util/isSorted");
const _util_1 = require("./_util");
const ConversationList_1 = require("./ConversationList");
const ContactCheckbox_1 = require("./conversationList/ContactCheckbox");
const MIN_WIDTH = 109;
const SNAP_WIDTH = 200;
const MIN_FULL_WIDTH = 280;
const MAX_WIDTH = 380;
(0, assert_1.strictAssert)((0, isSorted_1.isSorted)([MIN_WIDTH, SNAP_WIDTH, MIN_FULL_WIDTH, MAX_WIDTH]), 'Expected widths to be in the right order');
var LeftPaneMode;
(function (LeftPaneMode) {
    LeftPaneMode[LeftPaneMode["Inbox"] = 0] = "Inbox";
    LeftPaneMode[LeftPaneMode["Search"] = 1] = "Search";
    LeftPaneMode[LeftPaneMode["Archive"] = 2] = "Archive";
    LeftPaneMode[LeftPaneMode["Compose"] = 3] = "Compose";
    LeftPaneMode[LeftPaneMode["ChooseGroupMembers"] = 4] = "ChooseGroupMembers";
    LeftPaneMode[LeftPaneMode["SetGroupMetadata"] = 5] = "SetGroupMetadata";
})(LeftPaneMode = exports.LeftPaneMode || (exports.LeftPaneMode = {}));
const LeftPane = ({ badgesById, cantAddContactToGroup, canResizeLeftPane, challengeStatus, clearGroupCreationError, clearSearch, closeCantAddContactToGroupModal, closeMaximumGroupSizeModal, closeRecommendedGroupSizeModal, composeDeleteAvatarFromDisk, composeReplaceAvatar, composeSaveAvatarToDisk, createGroup, i18n, modeSpecificProps, openConversationInternal, preferredWidthFromStorage, renderCaptchaDialog, renderExpiredBuildDialog, renderMainHeader, renderMessageSearchResult, renderNetworkStatus, renderRelinkDialog, renderUpdateDialog, savePreferredLeftPaneWidth, searchInConversation, selectedConversationId, selectedMessageId, setChallengeStatus, setComposeGroupAvatar, setComposeGroupExpireTimer, setComposeGroupName, setComposeSearchTerm, showArchivedConversations, showChooseGroupMembers, showInbox, startComposing, startSearch, startNewConversationFromPhoneNumber, startNewConversationFromUsername, startSettingGroupMetadata, theme, toggleComposeEditingAvatar, toggleConversationInChooseMembers, updateSearchTerm, }) => {
    const [preferredWidth, setPreferredWidth] = (0, react_1.useState)(
    // This clamp is present just in case we get a bogus value from storage.
    (0, lodash_1.clamp)(preferredWidthFromStorage, MIN_WIDTH, MAX_WIDTH));
    const [isResizing, setIsResizing] = (0, react_1.useState)(false);
    const previousModeSpecificProps = (0, usePrevious_1.usePrevious)(modeSpecificProps, modeSpecificProps);
    // The left pane can be in various modes: the inbox, the archive, the composer, etc.
    //   Ideally, this would render subcomponents such as `<LeftPaneInbox>` or
    //   `<LeftPaneArchive>` (and if there's a way to do that cleanly, we should refactor
    //   this).
    //
    // But doing that presents two problems:
    //
    // 1. Different components render the same logical inputs (the main header's search),
    //    but React doesn't know that they're the same, so you can lose focus as you change
    //    modes.
    // 2. These components render virtualized lists, which are somewhat slow to initialize.
    //    Switching between modes can cause noticable hiccups.
    //
    // To get around those problems, we use "helpers" which all correspond to the same
    //   interface.
    //
    // Unfortunately, there's a little bit of repetition here because TypeScript isn't quite
    //   smart enough.
    let helper;
    let shouldRecomputeRowHeights;
    switch (modeSpecificProps.mode) {
        case LeftPaneMode.Inbox: {
            const inboxHelper = new LeftPaneInboxHelper_1.LeftPaneInboxHelper(modeSpecificProps);
            shouldRecomputeRowHeights =
                previousModeSpecificProps.mode === modeSpecificProps.mode
                    ? inboxHelper.shouldRecomputeRowHeights(previousModeSpecificProps)
                    : true;
            helper = inboxHelper;
            break;
        }
        case LeftPaneMode.Search: {
            const searchHelper = new LeftPaneSearchHelper_1.LeftPaneSearchHelper(modeSpecificProps);
            shouldRecomputeRowHeights =
                previousModeSpecificProps.mode === modeSpecificProps.mode
                    ? searchHelper.shouldRecomputeRowHeights(previousModeSpecificProps)
                    : true;
            helper = searchHelper;
            break;
        }
        case LeftPaneMode.Archive: {
            const archiveHelper = new LeftPaneArchiveHelper_1.LeftPaneArchiveHelper(modeSpecificProps);
            shouldRecomputeRowHeights =
                previousModeSpecificProps.mode === modeSpecificProps.mode
                    ? archiveHelper.shouldRecomputeRowHeights(previousModeSpecificProps)
                    : true;
            helper = archiveHelper;
            break;
        }
        case LeftPaneMode.Compose: {
            const composeHelper = new LeftPaneComposeHelper_1.LeftPaneComposeHelper(modeSpecificProps);
            shouldRecomputeRowHeights =
                previousModeSpecificProps.mode === modeSpecificProps.mode
                    ? composeHelper.shouldRecomputeRowHeights(previousModeSpecificProps)
                    : true;
            helper = composeHelper;
            break;
        }
        case LeftPaneMode.ChooseGroupMembers: {
            const chooseGroupMembersHelper = new LeftPaneChooseGroupMembersHelper_1.LeftPaneChooseGroupMembersHelper(modeSpecificProps);
            shouldRecomputeRowHeights =
                previousModeSpecificProps.mode === modeSpecificProps.mode
                    ? chooseGroupMembersHelper.shouldRecomputeRowHeights(previousModeSpecificProps)
                    : true;
            helper = chooseGroupMembersHelper;
            break;
        }
        case LeftPaneMode.SetGroupMetadata: {
            const setGroupMetadataHelper = new LeftPaneSetGroupMetadataHelper_1.LeftPaneSetGroupMetadataHelper(modeSpecificProps);
            shouldRecomputeRowHeights =
                previousModeSpecificProps.mode === modeSpecificProps.mode
                    ? setGroupMetadataHelper.shouldRecomputeRowHeights(previousModeSpecificProps)
                    : true;
            helper = setGroupMetadataHelper;
            break;
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(modeSpecificProps);
    }
    (0, react_1.useEffect)(() => {
        const onKeyDown = (event) => {
            const { ctrlKey, shiftKey, altKey, metaKey, key } = event;
            const commandOrCtrl = OS.isMacOS() ? metaKey : ctrlKey;
            if (event.key === 'Escape') {
                const backAction = helper.getBackAction({
                    showInbox,
                    startComposing,
                    showChooseGroupMembers,
                });
                if (backAction) {
                    event.preventDefault();
                    event.stopPropagation();
                    backAction();
                    return;
                }
            }
            if (commandOrCtrl &&
                !shiftKey &&
                !altKey &&
                (key === 'n' || key === 'N')) {
                startComposing();
                event.preventDefault();
                event.stopPropagation();
                return;
            }
            let conversationToOpen;
            const numericIndex = keyboardKeyToNumericIndex(event.key);
            if (commandOrCtrl && (0, lodash_1.isNumber)(numericIndex)) {
                conversationToOpen =
                    helper.getConversationAndMessageAtIndex(numericIndex);
            }
            else {
                let toFind;
                if ((altKey && !shiftKey && key === 'ArrowUp') ||
                    (commandOrCtrl && shiftKey && key === '[') ||
                    (ctrlKey && shiftKey && key === 'Tab')) {
                    toFind = { direction: LeftPaneHelper_1.FindDirection.Up, unreadOnly: false };
                }
                else if ((altKey && !shiftKey && key === 'ArrowDown') ||
                    (commandOrCtrl && shiftKey && key === ']') ||
                    (ctrlKey && key === 'Tab')) {
                    toFind = { direction: LeftPaneHelper_1.FindDirection.Down, unreadOnly: false };
                }
                else if (altKey && shiftKey && key === 'ArrowUp') {
                    toFind = { direction: LeftPaneHelper_1.FindDirection.Up, unreadOnly: true };
                }
                else if (altKey && shiftKey && key === 'ArrowDown') {
                    toFind = { direction: LeftPaneHelper_1.FindDirection.Down, unreadOnly: true };
                }
                if (toFind) {
                    conversationToOpen = helper.getConversationAndMessageInDirection(toFind, selectedConversationId, selectedMessageId);
                }
            }
            if (conversationToOpen) {
                const { conversationId, messageId } = conversationToOpen;
                openConversationInternal({ conversationId, messageId });
                event.preventDefault();
                event.stopPropagation();
            }
            helper.onKeyDown(event, {
                searchInConversation,
                selectedConversationId,
                startSearch,
            });
        };
        document.addEventListener('keydown', onKeyDown);
        return () => {
            document.removeEventListener('keydown', onKeyDown);
        };
    }, [
        helper,
        openConversationInternal,
        searchInConversation,
        selectedConversationId,
        selectedMessageId,
        showChooseGroupMembers,
        showInbox,
        startComposing,
        startSearch,
    ]);
    const requiresFullWidth = helper.requiresFullWidth();
    (0, react_1.useEffect)(() => {
        if (!isResizing) {
            return lodash_1.noop;
        }
        const onMouseMove = (event) => {
            let width;
            if (requiresFullWidth) {
                width = Math.max(event.clientX, MIN_FULL_WIDTH);
            }
            else if (event.clientX < SNAP_WIDTH) {
                width = MIN_WIDTH;
            }
            else {
                width = (0, lodash_1.clamp)(event.clientX, MIN_FULL_WIDTH, MAX_WIDTH);
            }
            setPreferredWidth(Math.min(width, MAX_WIDTH));
            event.preventDefault();
        };
        const onMouseUp = () => {
            setIsResizing(false);
        };
        document.body.addEventListener('mousemove', onMouseMove);
        document.body.addEventListener('mouseup', onMouseUp);
        return () => {
            document.body.removeEventListener('mousemove', onMouseMove);
            document.body.removeEventListener('mouseup', onMouseUp);
        };
    }, [isResizing, requiresFullWidth]);
    (0, react_1.useEffect)(() => {
        if (!isResizing) {
            return lodash_1.noop;
        }
        document.body.classList.add('is-resizing-left-pane');
        return () => {
            document.body.classList.remove('is-resizing-left-pane');
        };
    }, [isResizing]);
    (0, react_1.useEffect)(() => {
        if (isResizing || preferredWidth === preferredWidthFromStorage) {
            return;
        }
        const timeout = setTimeout(() => {
            savePreferredLeftPaneWidth(preferredWidth);
        }, 1000);
        return () => {
            clearTimeout(timeout);
        };
    }, [
        isResizing,
        preferredWidth,
        preferredWidthFromStorage,
        savePreferredLeftPaneWidth,
    ]);
    const preRowsNode = helper.getPreRowsNode({
        clearGroupCreationError,
        closeCantAddContactToGroupModal,
        closeMaximumGroupSizeModal,
        closeRecommendedGroupSizeModal,
        composeDeleteAvatarFromDisk,
        composeReplaceAvatar,
        composeSaveAvatarToDisk,
        createGroup,
        i18n,
        setComposeGroupAvatar,
        setComposeGroupName,
        setComposeGroupExpireTimer,
        toggleComposeEditingAvatar,
        onChangeComposeSearchTerm: event => {
            setComposeSearchTerm(event.target.value);
        },
        removeSelectedContact: toggleConversationInChooseMembers,
    });
    const footerContents = helper.getFooterContents({
        createGroup,
        i18n,
        startSettingGroupMetadata,
    });
    const getRow = (0, react_1.useMemo)(() => helper.getRow.bind(helper), [helper]);
    const onSelectConversation = (0, react_1.useCallback)((conversationId, messageId) => {
        openConversationInternal({
            conversationId,
            messageId,
            switchToAssociatedView: true,
        });
    }, [openConversationInternal]);
    const previousSelectedConversationId = (0, usePrevious_1.usePrevious)(selectedConversationId, selectedConversationId);
    let width;
    if (requiresFullWidth || preferredWidth >= SNAP_WIDTH) {
        width = Math.max(preferredWidth, MIN_FULL_WIDTH);
    }
    else {
        width = MIN_WIDTH;
    }
    const isScrollable = helper.isScrollable();
    let rowIndexToScrollTo;
    let scrollBehavior;
    if (isScrollable) {
        rowIndexToScrollTo =
            previousSelectedConversationId === selectedConversationId
                ? undefined
                : helper.getRowIndexToScrollTo(selectedConversationId);
        scrollBehavior = Util_1.ScrollBehavior.Default;
    }
    else {
        rowIndexToScrollTo = 0;
        scrollBehavior = Util_1.ScrollBehavior.Hard;
    }
    // We ensure that the listKey differs between some modes (e.g. inbox/archived), ensuring
    //   that AutoSizer properly detects the new size of its slot in the flexbox. The
    //   archive explainer text at the top of the archive view causes problems otherwise.
    //   It also ensures that we scroll to the top when switching views.
    const listKey = preRowsNode ? 1 : 0;
    const widthBreakpoint = (0, _util_1.getConversationListWidthBreakpoint)(width);
    // We disable this lint rule because we're trying to capture bubbled events. See [the
    //   lint rule's docs][0].
    //
    // [0]: https://github.com/jsx-eslint/eslint-plugin-jsx-a11y/blob/645900a0e296ca7053dbf6cd9e12cc85849de2d5/docs/rules/no-static-element-interactions.md#case-the-event-handler-is-only-being-used-to-capture-bubbled-events
    /* eslint-disable jsx-a11y/no-static-element-interactions */
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-left-pane', isResizing && 'module-left-pane--is-resizing', `module-left-pane--width-${widthBreakpoint}`), style: { width } },
        react_1.default.createElement("div", { className: "module-left-pane__header" }, helper.getHeaderContents({
            clearSearch,
            i18n,
            showInbox,
            startComposing,
            showChooseGroupMembers,
            updateSearchTerm,
        }) || renderMainHeader()),
        renderExpiredBuildDialog({ containerWidthBreakpoint: widthBreakpoint }),
        renderRelinkDialog({ containerWidthBreakpoint: widthBreakpoint }),
        renderNetworkStatus({ containerWidthBreakpoint: widthBreakpoint }),
        renderUpdateDialog({ containerWidthBreakpoint: widthBreakpoint }),
        preRowsNode && react_1.default.createElement(react_1.default.Fragment, { key: 0 }, preRowsNode),
        react_1.default.createElement(react_measure_1.default, { bounds: true }, ({ contentRect, measureRef }) => {
            var _a;
            return (react_1.default.createElement("div", { className: "module-left-pane__list--measure", ref: measureRef },
                react_1.default.createElement("div", { className: "module-left-pane__list--wrapper" },
                    react_1.default.createElement("div", { "aria-live": "polite", className: "module-left-pane__list", key: listKey, role: "presentation", tabIndex: -1 },
                        react_1.default.createElement(ConversationList_1.ConversationList, { badgesById: badgesById, dimensions: {
                                width,
                                height: ((_a = contentRect.bounds) === null || _a === void 0 ? void 0 : _a.height) || 0,
                            }, getRow: getRow, i18n: i18n, onClickArchiveButton: showArchivedConversations, onClickContactCheckbox: (conversationId, disabledReason) => {
                                switch (disabledReason) {
                                    case undefined:
                                        toggleConversationInChooseMembers(conversationId);
                                        break;
                                    case ContactCheckbox_1.ContactCheckboxDisabledReason.AlreadyAdded:
                                    case ContactCheckbox_1.ContactCheckboxDisabledReason.MaximumContactsSelected:
                                        // These are no-ops.
                                        break;
                                    case ContactCheckbox_1.ContactCheckboxDisabledReason.NotCapable:
                                        cantAddContactToGroup(conversationId);
                                        break;
                                    default:
                                        throw (0, missingCaseError_1.missingCaseError)(disabledReason);
                                }
                            }, onSelectConversation: onSelectConversation, renderMessageSearchResult: renderMessageSearchResult, rowCount: helper.getRowCount(), scrollBehavior: scrollBehavior, scrollToRowIndex: rowIndexToScrollTo, scrollable: isScrollable, shouldRecomputeRowHeights: shouldRecomputeRowHeights, showChooseGroupMembers: showChooseGroupMembers, startNewConversationFromPhoneNumber: startNewConversationFromPhoneNumber, startNewConversationFromUsername: startNewConversationFromUsername, theme: theme })))));
        }),
        footerContents && (react_1.default.createElement("div", { className: "module-left-pane__footer" }, footerContents)),
        canResizeLeftPane && (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "module-left-pane__resize-grab-area", onMouseDown: () => {
                    setIsResizing(true);
                } }))),
        challengeStatus !== 'idle' &&
            renderCaptchaDialog({
                onSkip() {
                    setChallengeStatus('idle');
                },
            })));
};
exports.LeftPane = LeftPane;
function keyboardKeyToNumericIndex(key) {
    if (key.length !== 1) {
        return undefined;
    }
    const result = parseInt(key, 10) - 1;
    const isValidIndex = Number.isInteger(result) && result >= 0 && result <= 8;
    return isValidIndex ? result : undefined;
}
