"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomizingPreferredReactionsModal = void 0;
const react_1 = __importStar(require("react"));
const react_popper_1 = require("react-popper");
const lodash_1 = require("lodash");
const Modal_1 = require("./Modal");
const Button_1 = require("./Button");
const ReactionPickerPicker_1 = require("./ReactionPickerPicker");
const EmojiPicker_1 = require("./emoji/EmojiPicker");
const constants_1 = require("../reactions/constants");
const lib_1 = require("./emoji/lib");
const popperUtil_1 = require("../util/popperUtil");
function CustomizingPreferredReactionsModal({ cancelCustomizePreferredReactionsModal, deselectDraftEmoji, draftPreferredReactions, hadSaveError, i18n, isSaving, onSetSkinTone, originalPreferredReactions, recentEmojis, replaceSelectedDraftEmoji, resetDraftEmoji, savePreferredReactions, selectDraftEmojiToBeReplaced, selectedDraftEmojiIndex, skinTone, }) {
    const [referenceElement, setReferenceElement] = (0, react_1.useState)(null);
    const [popperElement, setPopperElement] = (0, react_1.useState)(null);
    const emojiPickerPopper = (0, react_popper_1.usePopper)(referenceElement, popperElement, {
        placement: 'bottom',
        modifiers: [
            (0, popperUtil_1.offsetDistanceModifier)(8),
            {
                name: 'preventOverflow',
                options: { altAxis: true },
            },
        ],
    });
    const isSomethingSelected = selectedDraftEmojiIndex !== undefined;
    (0, react_1.useEffect)(() => {
        if (!isSomethingSelected) {
            return lodash_1.noop;
        }
        const onBodyClick = (event) => {
            const { target } = event;
            if (!(target instanceof HTMLElement) || !popperElement) {
                return;
            }
            const isClickOutsidePicker = !popperElement.contains(target);
            if (isClickOutsidePicker) {
                deselectDraftEmoji();
            }
        };
        document.body.addEventListener('click', onBodyClick);
        return () => {
            document.body.removeEventListener('click', onBodyClick);
        };
    }, [isSomethingSelected, popperElement, deselectDraftEmoji]);
    const hasChanged = !(0, lodash_1.isEqual)(originalPreferredReactions, draftPreferredReactions);
    const canReset = !isSaving &&
        !(0, lodash_1.isEqual)(constants_1.DEFAULT_PREFERRED_REACTION_EMOJI_SHORT_NAMES.map(shortName => (0, lib_1.convertShortName)(shortName, skinTone)), draftPreferredReactions);
    const canSave = !isSaving && hasChanged;
    return (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, i18n: i18n, onClose: () => {
            cancelCustomizePreferredReactionsModal();
        }, title: i18n('CustomizingPreferredReactions__title') },
        react_1.default.createElement("div", { className: "module-CustomizingPreferredReactionsModal__small-emoji-picker-wrapper" },
            react_1.default.createElement(ReactionPickerPicker_1.ReactionPickerPicker, { isSomethingSelected: isSomethingSelected, pickerStyle: ReactionPickerPicker_1.ReactionPickerPickerStyle.Menu, ref: setReferenceElement }, draftPreferredReactions.map((emoji, index) => (react_1.default.createElement(ReactionPickerPicker_1.ReactionPickerPickerEmojiButton, { emoji: emoji, 
                // The index is the only thing that uniquely identifies the emoji, because
                //   there can be duplicates in the list.
                // eslint-disable-next-line react/no-array-index-key
                key: index, onClick: () => {
                    selectDraftEmojiToBeReplaced(index);
                }, isSelected: index === selectedDraftEmojiIndex })))),
            hadSaveError
                ? i18n('CustomizingPreferredReactions__had-save-error')
                : i18n('CustomizingPreferredReactions__subtitle')),
        isSomethingSelected && (react_1.default.createElement("div", Object.assign({ ref: setPopperElement, style: emojiPickerPopper.styles.popper }, emojiPickerPopper.attributes.popper),
            react_1.default.createElement(EmojiPicker_1.EmojiPicker, { i18n: i18n, onPickEmoji: pickedEmoji => {
                    const emoji = (0, lib_1.convertShortName)(pickedEmoji.shortName, pickedEmoji.skinTone);
                    replaceSelectedDraftEmoji(emoji);
                }, recentEmojis: recentEmojis, skinTone: skinTone, onSetSkinTone: onSetSkinTone, onClose: () => {
                    deselectDraftEmoji();
                } }))),
        react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
            react_1.default.createElement(Button_1.Button, { disabled: !canReset, onClick: () => {
                    resetDraftEmoji();
                }, variant: Button_1.ButtonVariant.SecondaryAffirmative }, i18n('reset')),
            react_1.default.createElement(Button_1.Button, { disabled: !canSave, onClick: () => {
                    savePreferredReactions();
                } }, i18n('save')))));
}
exports.CustomizingPreferredReactionsModal = CustomizingPreferredReactionsModal;
