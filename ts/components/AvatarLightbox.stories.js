"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const Colors_1 = require("../types/Colors");
const AvatarLightbox_1 = require("./AvatarLightbox");
const setupI18n_1 = require("../util/setupI18n");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    avatarColor: (0, addon_knobs_1.select)('Color', Colors_1.AvatarColors, overrideProps.avatarColor || Colors_1.AvatarColors[0]),
    avatarPath: overrideProps.avatarPath,
    conversationTitle: overrideProps.conversationTitle,
    i18n,
    isGroup: Boolean(overrideProps.isGroup),
    onClose: (0, addon_actions_1.action)('onClose'),
});
const story = (0, react_2.storiesOf)('Components/AvatarLightbox', module);
story.add('Group', () => (react_1.default.createElement(AvatarLightbox_1.AvatarLightbox, Object.assign({}, createProps({
    isGroup: true,
})))));
story.add('Person', () => {
    const conversation = (0, getDefaultConversation_1.getDefaultConversation)();
    return (react_1.default.createElement(AvatarLightbox_1.AvatarLightbox, Object.assign({}, createProps({
        avatarColor: conversation.color,
        conversationTitle: conversation.title,
    }))));
});
story.add('Photo', () => (react_1.default.createElement(AvatarLightbox_1.AvatarLightbox, Object.assign({}, createProps({
    avatarPath: '/fixtures/kitten-1-64-64.jpg',
})))));
