"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CaptchaDialog = void 0;
const react_1 = __importStar(require("react"));
const Button_1 = require("./Button");
const Modal_1 = require("./Modal");
const Spinner_1 = require("./Spinner");
function CaptchaDialog(props) {
    const { i18n, isPending, onSkip, onContinue } = props;
    const [isClosing, setIsClosing] = (0, react_1.useState)(false);
    const buttonRef = (0, react_1.useRef)(null);
    const onCancelClick = (event) => {
        event.preventDefault();
        setIsClosing(false);
    };
    const onSkipClick = (event) => {
        event.preventDefault();
        onSkip();
    };
    if (isClosing && !isPending) {
        return (react_1.default.createElement(Modal_1.Modal, { moduleClassName: "module-Modal", i18n: i18n, title: i18n('CaptchaDialog--can-close__title'), onClose: () => setIsClosing(false) },
            react_1.default.createElement("section", null,
                react_1.default.createElement("p", null, i18n('CaptchaDialog--can-close__body'))),
            react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
                react_1.default.createElement(Button_1.Button, { onClick: onCancelClick, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
                react_1.default.createElement(Button_1.Button, { onClick: onSkipClick, variant: Button_1.ButtonVariant.Destructive }, i18n('CaptchaDialog--can_close__skip-verification')))));
    }
    const onContinueClick = (event) => {
        event.preventDefault();
        onContinue();
    };
    const updateButtonRef = (button) => {
        buttonRef.current = button;
        if (button) {
            button.focus();
        }
    };
    return (react_1.default.createElement(Modal_1.Modal, { moduleClassName: "module-Modal--important", i18n: i18n, title: i18n('CaptchaDialog__title'), hasXButton: true, onClose: () => setIsClosing(true) },
        react_1.default.createElement("section", null,
            react_1.default.createElement("p", null, i18n('CaptchaDialog__first-paragraph')),
            react_1.default.createElement("p", null, i18n('CaptchaDialog__second-paragraph'))),
        react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
            react_1.default.createElement(Button_1.Button, { disabled: isPending, onClick: onContinueClick, ref: updateButtonRef, variant: Button_1.ButtonVariant.Primary }, isPending ? (react_1.default.createElement(Spinner_1.Spinner, { size: "22px", svgSize: "small", direction: "on-captcha" })) : ('Continue')))));
}
exports.CaptchaDialog = CaptchaDialog;
