"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileEditorModal = void 0;
const react_1 = __importStar(require("react"));
const Modal_1 = require("./Modal");
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const ProfileEditor_1 = require("./ProfileEditor");
const ProfileEditorModal = (_a) => {
    var { hasError, i18n, myProfileChanged, onSetSkinTone, toggleProfileEditor, toggleProfileEditorHasError } = _a, restProps = __rest(_a, ["hasError", "i18n", "myProfileChanged", "onSetSkinTone", "toggleProfileEditor", "toggleProfileEditorHasError"]);
    const MODAL_TITLES_BY_EDIT_STATE = {
        [ProfileEditor_1.EditState.BetterAvatar]: i18n('ProfileEditorModal--avatar'),
        [ProfileEditor_1.EditState.Bio]: i18n('ProfileEditorModal--about'),
        [ProfileEditor_1.EditState.None]: i18n('ProfileEditorModal--profile'),
        [ProfileEditor_1.EditState.ProfileName]: i18n('ProfileEditorModal--name'),
        [ProfileEditor_1.EditState.Username]: i18n('ProfileEditorModal--username'),
    };
    const [modalTitle, setModalTitle] = (0, react_1.useState)(MODAL_TITLES_BY_EDIT_STATE[ProfileEditor_1.EditState.None]);
    if (hasError) {
        return (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { cancelText: i18n('Confirmation--confirm'), i18n: i18n, onClose: toggleProfileEditorHasError }, i18n('ProfileEditorModal--error')));
    }
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(Modal_1.Modal, { hasStickyButtons: true, hasXButton: true, i18n: i18n, onClose: toggleProfileEditor, title: modalTitle },
            react_1.default.createElement(ProfileEditor_1.ProfileEditor, Object.assign({}, restProps, { i18n: i18n, onEditStateChanged: editState => {
                    setModalTitle(MODAL_TITLES_BY_EDIT_STATE[editState]);
                }, onProfileChanged: myProfileChanged, onSetSkinTone: onSetSkinTone })))));
};
exports.ProfileEditorModal = ProfileEditorModal;
