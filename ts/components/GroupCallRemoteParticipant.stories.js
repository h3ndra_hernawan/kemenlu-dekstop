"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const GroupCallRemoteParticipant_1 = require("./GroupCallRemoteParticipant");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const constants_1 = require("../calling/constants");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const getFrameBuffer = (0, lodash_1.memoize)(() => new ArrayBuffer(constants_1.FRAME_BUFFER_SIZE));
const createProps = (overrideProps, isBlocked) => (Object.assign({ getFrameBuffer, 
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    getGroupCallVideoFrameSource: lodash_1.noop, i18n, remoteParticipant: Object.assign({ demuxId: 123, hasRemoteAudio: false, hasRemoteVideo: true, presenting: false, sharingScreen: false, videoAspectRatio: 1.3 }, (0, getDefaultConversation_1.getDefaultConversation)({
        isBlocked: Boolean(isBlocked),
        title: 'Pablo Diego José Francisco de Paula Juan Nepomuceno María de los Remedios Cipriano de la Santísima Trinidad Ruiz y Picasso',
        uuid: '992ed3b9-fc9b-47a9-bdb4-e0c7cbb0fda5',
    })) }, overrideProps));
const story = (0, react_1.storiesOf)('Components/GroupCallRemoteParticipant', module);
story.add('Default', () => (React.createElement(GroupCallRemoteParticipant_1.GroupCallRemoteParticipant, Object.assign({}, createProps({
    isInPip: false,
    height: 120,
    left: 0,
    top: 0,
    width: 120,
})))));
story.add('isInPip', () => (React.createElement(GroupCallRemoteParticipant_1.GroupCallRemoteParticipant, Object.assign({}, createProps({
    isInPip: true,
})))));
story.add('Blocked', () => (React.createElement(GroupCallRemoteParticipant_1.GroupCallRemoteParticipant, Object.assign({}, createProps({
    isInPip: false,
    height: 120,
    left: 0,
    top: 0,
    width: 120,
}, true)))));
