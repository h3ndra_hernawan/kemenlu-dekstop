"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupDescriptionText = void 0;
const react_1 = __importDefault(require("react"));
const AddNewLines_1 = require("./conversation/AddNewLines");
const Emojify_1 = require("./conversation/Emojify");
const Linkify_1 = require("./conversation/Linkify");
const renderNonLink = ({ key, text }) => (react_1.default.createElement(Emojify_1.Emojify, { key: key, text: text }));
const renderNonNewLine = ({ key, text }) => (react_1.default.createElement(Linkify_1.Linkify, { key: key, text: text, renderNonLink: renderNonLink }));
const GroupDescriptionText = ({ text, }) => react_1.default.createElement(AddNewLines_1.AddNewLines, { text: text, renderNonNewLine: renderNonNewLine });
exports.GroupDescriptionText = GroupDescriptionText;
