"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const CallManager_1 = require("./CallManager");
const Calling_1 = require("../types/Calling");
const Colors_1 = require("../types/Colors");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const fakeGetGroupCallVideoFrameSource_1 = require("../test-both/helpers/fakeGetGroupCallVideoFrameSource");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const getConversation = () => (0, getDefaultConversation_1.getDefaultConversation)({
    id: '3051234567',
    avatarPath: undefined,
    color: (0, addon_knobs_1.select)('Callee color', Colors_1.AvatarColors, 'ultramarine'),
    title: (0, addon_knobs_1.text)('Callee Title', 'Rick Sanchez'),
    name: (0, addon_knobs_1.text)('Callee Name', 'Rick Sanchez'),
    phoneNumber: '3051234567',
    profileName: 'Rick Sanchez',
    markedUnread: false,
    type: 'direct',
    lastUpdated: Date.now(),
});
const getCommonActiveCallData = () => ({
    conversation: getConversation(),
    joinedAt: Date.now(),
    hasLocalAudio: (0, addon_knobs_1.boolean)('hasLocalAudio', true),
    hasLocalVideo: (0, addon_knobs_1.boolean)('hasLocalVideo', false),
    isInSpeakerView: (0, addon_knobs_1.boolean)('isInSpeakerView', false),
    outgoingRing: (0, addon_knobs_1.boolean)('outgoingRing', true),
    pip: (0, addon_knobs_1.boolean)('pip', false),
    settingsDialogOpen: (0, addon_knobs_1.boolean)('settingsDialogOpen', false),
    showParticipantsList: (0, addon_knobs_1.boolean)('showParticipantsList', false),
});
const createProps = (storyProps = {}) => (Object.assign(Object.assign({}, storyProps), { availableCameras: [], acceptCall: (0, addon_actions_1.action)('accept-call'), bounceAppIconStart: (0, addon_actions_1.action)('bounce-app-icon-start'), bounceAppIconStop: (0, addon_actions_1.action)('bounce-app-icon-stop'), cancelCall: (0, addon_actions_1.action)('cancel-call'), closeNeedPermissionScreen: (0, addon_actions_1.action)('close-need-permission-screen'), declineCall: (0, addon_actions_1.action)('decline-call'), getGroupCallVideoFrameSource: (_, demuxId) => (0, fakeGetGroupCallVideoFrameSource_1.fakeGetGroupCallVideoFrameSource)(demuxId), getPresentingSources: (0, addon_actions_1.action)('get-presenting-sources'), hangUp: (0, addon_actions_1.action)('hang-up'), i18n, isGroupCallOutboundRingEnabled: true, keyChangeOk: (0, addon_actions_1.action)('key-change-ok'), me: Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
        color: (0, addon_knobs_1.select)('Caller color', Colors_1.AvatarColors, 'ultramarine'),
        title: (0, addon_knobs_1.text)('Caller Title', 'Morty Smith'),
    })), { uuid: 'cb0dd0c8-7393-41e9-a0aa-d631c4109541' }), notifyForCall: (0, addon_actions_1.action)('notify-for-call'), openSystemPreferencesAction: (0, addon_actions_1.action)('open-system-preferences-action'), playRingtone: (0, addon_actions_1.action)('play-ringtone'), renderDeviceSelection: () => React.createElement("div", null), renderSafetyNumberViewer: (_) => React.createElement("div", null), setGroupCallVideoRequest: (0, addon_actions_1.action)('set-group-call-video-request'), setIsCallActive: (0, addon_actions_1.action)('set-is-call-active'), setLocalAudio: (0, addon_actions_1.action)('set-local-audio'), setLocalPreview: (0, addon_actions_1.action)('set-local-preview'), setLocalVideo: (0, addon_actions_1.action)('set-local-video'), setPresenting: (0, addon_actions_1.action)('toggle-presenting'), setRendererCanvas: (0, addon_actions_1.action)('set-renderer-canvas'), setOutgoingRing: (0, addon_actions_1.action)('set-outgoing-ring'), startCall: (0, addon_actions_1.action)('start-call'), stopRingtone: (0, addon_actions_1.action)('stop-ringtone'), toggleParticipants: (0, addon_actions_1.action)('toggle-participants'), togglePip: (0, addon_actions_1.action)('toggle-pip'), toggleScreenRecordingPermissionsDialog: (0, addon_actions_1.action)('toggle-screen-recording-permissions-dialog'), toggleSettings: (0, addon_actions_1.action)('toggle-settings'), toggleSpeakerView: (0, addon_actions_1.action)('toggle-speaker-view') }));
const story = (0, react_1.storiesOf)('Components/CallManager', module);
story.add('No Call', () => React.createElement(CallManager_1.CallManager, Object.assign({}, createProps())));
story.add('Ongoing Direct Call', () => (React.createElement(CallManager_1.CallManager, Object.assign({}, createProps({
    activeCall: Object.assign(Object.assign({}, getCommonActiveCallData()), { callMode: Calling_1.CallMode.Direct, callState: Calling_1.CallState.Accepted, peekedParticipants: [], remoteParticipants: [
            { hasRemoteVideo: true, presenting: false, title: 'Remy' },
        ] }),
})))));
story.add('Ongoing Group Call', () => (React.createElement(CallManager_1.CallManager, Object.assign({}, createProps({
    activeCall: Object.assign(Object.assign({}, getCommonActiveCallData()), { callMode: Calling_1.CallMode.Group, connectionState: Calling_1.GroupCallConnectionState.Connected, conversationsWithSafetyNumberChanges: [], deviceCount: 0, joinState: Calling_1.GroupCallJoinState.Joined, maxDevices: 5, groupMembers: [], peekedParticipants: [], remoteParticipants: [] }),
})))));
story.add('Ringing (direct call)', () => (React.createElement(CallManager_1.CallManager, Object.assign({}, createProps({
    incomingCall: {
        callMode: Calling_1.CallMode.Direct,
        conversation: getConversation(),
        isVideoCall: true,
    },
})))));
story.add('Ringing (group call)', () => (React.createElement(CallManager_1.CallManager, Object.assign({}, createProps({
    incomingCall: {
        callMode: Calling_1.CallMode.Group,
        conversation: Object.assign(Object.assign({}, getConversation()), { type: 'group', title: 'Tahoe Trip' }),
        otherMembersRung: [
            { firstName: 'Morty', title: 'Morty Smith' },
            { firstName: 'Summer', title: 'Summer Smith' },
        ],
        ringer: { firstName: 'Rick', title: 'Rick Sanchez' },
    },
})))));
story.add('Call Request Needed', () => (React.createElement(CallManager_1.CallManager, Object.assign({}, createProps({
    activeCall: Object.assign(Object.assign({}, getCommonActiveCallData()), { callEndedReason: Calling_1.CallEndedReason.RemoteHangupNeedPermission, callMode: Calling_1.CallMode.Direct, callState: Calling_1.CallState.Accepted, peekedParticipants: [], remoteParticipants: [
            { hasRemoteVideo: true, presenting: false, title: 'Mike' },
        ] }),
})))));
story.add('Group call - Safety Number Changed', () => (React.createElement(CallManager_1.CallManager, Object.assign({}, createProps({
    activeCall: Object.assign(Object.assign({}, getCommonActiveCallData()), { callMode: Calling_1.CallMode.Group, connectionState: Calling_1.GroupCallConnectionState.Connected, conversationsWithSafetyNumberChanges: [
            Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Aaron',
            })),
        ], deviceCount: 0, joinState: Calling_1.GroupCallJoinState.Joined, maxDevices: 5, groupMembers: [], peekedParticipants: [], remoteParticipants: [] }),
})))));
