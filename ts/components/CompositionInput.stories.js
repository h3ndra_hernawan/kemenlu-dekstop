"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
require("react-quill/dist/quill.core.css");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const CompositionInput_1 = require("./CompositionInput");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/CompositionInput', module);
const createProps = (overrideProps = {}) => ({
    i18n,
    conversationId: 'conversation-id',
    disabled: (0, addon_knobs_1.boolean)('disabled', overrideProps.disabled || false),
    onSubmit: (0, addon_actions_1.action)('onSubmit'),
    onEditorStateChange: (0, addon_actions_1.action)('onEditorStateChange'),
    onTextTooLong: (0, addon_actions_1.action)('onTextTooLong'),
    draftText: overrideProps.draftText || undefined,
    draftBodyRanges: overrideProps.draftBodyRanges || [],
    clearQuotedMessage: (0, addon_actions_1.action)('clearQuotedMessage'),
    getQuotedMessage: (0, addon_actions_1.action)('getQuotedMessage'),
    onPickEmoji: (0, addon_actions_1.action)('onPickEmoji'),
    large: (0, addon_knobs_1.boolean)('large', overrideProps.large || false),
    scrollToBottom: (0, addon_actions_1.action)('scrollToBottom'),
    sortedGroupMembers: overrideProps.sortedGroupMembers || [],
    skinTone: (0, addon_knobs_1.select)('skinTone', {
        skinTone0: 0,
        skinTone1: 1,
        skinTone2: 2,
        skinTone3: 3,
        skinTone4: 4,
        skinTone5: 5,
    }, overrideProps.skinTone || undefined),
});
story.add('Default', () => {
    const props = createProps();
    return React.createElement(CompositionInput_1.CompositionInput, Object.assign({}, props));
});
story.add('Large', () => {
    const props = createProps({
        large: true,
    });
    return React.createElement(CompositionInput_1.CompositionInput, Object.assign({}, props));
});
story.add('Disabled', () => {
    const props = createProps({
        disabled: true,
    });
    return React.createElement(CompositionInput_1.CompositionInput, Object.assign({}, props));
});
story.add('Starting Text', () => {
    const props = createProps({
        draftText: "here's some starting text",
    });
    return React.createElement(CompositionInput_1.CompositionInput, Object.assign({}, props));
});
story.add('Multiline Text', () => {
    const props = createProps({
        draftText: `here's some starting text
and more on another line
and yet another line
and yet another line
and yet another line
and yet another line
and yet another line
and yet another line
and we're done`,
    });
    return React.createElement(CompositionInput_1.CompositionInput, Object.assign({}, props));
});
story.add('Emojis', () => {
    const props = createProps({
        draftText: `⁣😐😐😐😐😐😐😐
😐😐😐😐😐😐😐
😐😐😐😂⁣😐😐😐
😐😐😐😐😐😐😐
😐😐😐😐😐😐😐`,
    });
    return React.createElement(CompositionInput_1.CompositionInput, Object.assign({}, props));
});
story.add('Mentions', () => {
    const props = createProps({
        sortedGroupMembers: [
            (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Kate Beaton',
            }),
            (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Parry Gripp',
            }),
        ],
        draftText: 'send _ a message',
        draftBodyRanges: [
            {
                start: 5,
                length: 1,
                mentionUuid: '0',
                replacementText: 'Kate Beaton',
            },
        ],
    });
    return React.createElement(CompositionInput_1.CompositionInput, Object.assign({}, props));
});
