"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShortcutGuideModal = void 0;
const React = __importStar(require("react"));
const react_dom_1 = require("react-dom");
const ShortcutGuide_1 = require("./ShortcutGuide");
exports.ShortcutGuideModal = React.memo((props) => {
    const { i18n, close, hasInstalledStickers, platform } = props;
    const [root, setRoot] = React.useState(null);
    React.useEffect(() => {
        const div = document.createElement('div');
        document.body.appendChild(div);
        setRoot(div);
        return () => {
            document.body.removeChild(div);
        };
    }, []);
    return root
        ? (0, react_dom_1.createPortal)(React.createElement("div", { className: "module-shortcut-guide-modal" },
            React.createElement("div", { className: "module-shortcut-guide-container" },
                React.createElement(ShortcutGuide_1.ShortcutGuide, { hasInstalledStickers: hasInstalledStickers, platform: platform, close: close, i18n: i18n }))), root)
        : null;
});
