"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DisappearingTimeDialog = void 0;
const react_1 = __importStar(require("react"));
const ConfirmationDialog_1 = require("./ConfirmationDialog");
const Select_1 = require("./Select");
const CSS_MODULE = 'module-disappearing-time-dialog';
const DEFAULT_VALUE = 60;
const UNITS = ['seconds', 'minutes', 'hours', 'days', 'weeks'];
const UNIT_TO_MS = new Map([
    ['seconds', 1],
    ['minutes', 60],
    ['hours', 60 * 60],
    ['days', 24 * 60 * 60],
    ['weeks', 7 * 24 * 60 * 60],
]);
const RANGES = new Map([
    ['seconds', [1, 60]],
    ['minutes', [1, 60]],
    ['hours', [1, 24]],
    ['days', [1, 7]],
    ['weeks', [1, 5]],
]);
function DisappearingTimeDialog(props) {
    const { i18n, theme, initialValue = DEFAULT_VALUE, onSubmit, onClose, } = props;
    let initialUnit = 'seconds';
    let initialUnitValue = 1;
    for (const unit of UNITS) {
        const ms = UNIT_TO_MS.get(unit) || 1;
        if (initialValue < ms) {
            break;
        }
        initialUnit = unit;
        initialUnitValue = Math.floor(initialValue / ms);
    }
    const [unitValue, setUnitValue] = (0, react_1.useState)(initialUnitValue);
    const [unit, setUnit] = (0, react_1.useState)(initialUnit);
    const range = RANGES.get(unit) || [1, 1];
    const values = [];
    for (let i = range[0]; i < range[1]; i += 1) {
        values.push(i);
    }
    return (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { moduleClassName: CSS_MODULE, i18n: i18n, theme: theme, onClose: onClose, title: i18n('DisappearingTimeDialog__title'), hasXButton: true, actions: [
            {
                text: i18n('DisappearingTimeDialog__set'),
                style: 'affirmative',
                action() {
                    onSubmit(unitValue * (UNIT_TO_MS.get(unit) || 1));
                },
            },
        ] },
        react_1.default.createElement("p", null, i18n('DisappearingTimeDialog__body')),
        react_1.default.createElement("section", { className: `${CSS_MODULE}__time-boxes` },
            react_1.default.createElement(Select_1.Select, { moduleClassName: `${CSS_MODULE}__time-boxes__value`, value: unitValue, onChange: newValue => setUnitValue(parseInt(newValue, 10)), options: values.map(value => ({ value, text: value.toString() })) }),
            react_1.default.createElement(Select_1.Select, { moduleClassName: `${CSS_MODULE}__time-boxes__units`, value: unit, onChange: newUnit => {
                    setUnit(newUnit);
                    const ranges = RANGES.get(newUnit);
                    if (!ranges) {
                        return;
                    }
                    const [min, max] = ranges;
                    setUnitValue(Math.max(min, Math.min(max - 1, unitValue)));
                }, options: UNITS.map(unitName => {
                    return {
                        value: unitName,
                        text: i18n(`DisappearingTimeDialog__${unitName}`),
                    };
                }) }))));
}
exports.DisappearingTimeDialog = DisappearingTimeDialog;
