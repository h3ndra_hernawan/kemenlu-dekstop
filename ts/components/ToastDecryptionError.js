"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ToastDecryptionError = void 0;
const react_1 = __importDefault(require("react"));
const Toast_1 = require("./Toast");
const ToastDecryptionError = ({ i18n, onClose, onShowDebugLog, }) => {
    return (react_1.default.createElement(Toast_1.Toast, { autoDismissDisabled: true, className: "decryption-error", onClose: onClose, toastAction: {
            label: i18n('decryptionErrorToastAction'),
            onClick: onShowDebugLog,
        } }, i18n('decryptionErrorToast')));
};
exports.ToastDecryptionError = ToastDecryptionError;
