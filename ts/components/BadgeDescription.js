"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadgeDescription = void 0;
const react_1 = __importDefault(require("react"));
const ContactName_1 = require("./conversation/ContactName");
function BadgeDescription({ firstName, template, title, }) {
    const result = [];
    let lastIndex = 0;
    const matches = template.matchAll(/\{short_name\}/g);
    for (const match of matches) {
        const matchIndex = match.index || 0;
        result.push(template.slice(lastIndex, matchIndex));
        result.push(react_1.default.createElement(ContactName_1.ContactName, { key: matchIndex, firstName: firstName, title: title, preferFirstName: true }));
        lastIndex = matchIndex + 12;
    }
    result.push(template.slice(lastIndex));
    return react_1.default.createElement(react_1.default.Fragment, null, result);
}
exports.BadgeDescription = BadgeDescription;
