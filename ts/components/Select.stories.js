"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const react_2 = require("@storybook/react");
const Select_1 = require("./Select");
const story = (0, react_2.storiesOf)('Components/Select', module);
story.add('Normal', () => {
    const [value, setValue] = (0, react_1.useState)(0);
    const onChange = (0, addon_actions_1.action)('onChange');
    return (react_1.default.createElement(Select_1.Select, { options: [
            { value: 1, text: '1' },
            { value: 2, text: '2' },
            { value: 3, text: '3' },
        ], value: value, onChange: newValue => {
            onChange(newValue);
            setValue(parseInt(newValue, 10));
        } }));
});
story.add('With disabled options', () => (react_1.default.createElement(Select_1.Select, { options: [
        { value: 'a', text: 'Apples' },
        { value: 'b', text: 'Bananas', disabled: true },
        { value: 'c', text: 'Cabbage' },
        { value: 'd', text: 'Durian', disabled: true },
    ], onChange: (0, addon_actions_1.action)('onChange'), value: "c" })));
