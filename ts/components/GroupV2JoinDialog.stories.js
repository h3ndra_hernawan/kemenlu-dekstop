"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const GroupV2JoinDialog_1 = require("./GroupV2JoinDialog");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    memberCount: (0, addon_knobs_1.number)('memberCount', overrideProps.memberCount || 12),
    avatar: overrideProps.avatar,
    title: (0, addon_knobs_1.text)('title', overrideProps.title || 'Random Group!'),
    approvalRequired: (0, addon_knobs_1.boolean)('approvalRequired', overrideProps.approvalRequired || false),
    groupDescription: overrideProps.groupDescription,
    join: (0, addon_actions_1.action)('join'),
    onClose: (0, addon_actions_1.action)('onClose'),
    i18n,
});
const stories = (0, react_1.storiesOf)('Components/GroupV2JoinDialog', module);
stories.add('Basic', () => {
    return React.createElement(GroupV2JoinDialog_1.GroupV2JoinDialog, Object.assign({}, createProps()));
});
stories.add('Approval required', () => {
    return (React.createElement(GroupV2JoinDialog_1.GroupV2JoinDialog, Object.assign({}, createProps({
        approvalRequired: true,
        title: 'Approval required!',
    }))));
});
stories.add('With avatar', () => {
    return (React.createElement(GroupV2JoinDialog_1.GroupV2JoinDialog, Object.assign({}, createProps({
        avatar: {
            url: '/fixtures/giphy-GVNvOUpeYmI7e.gif',
        },
        title: 'Has an avatar!',
    }))));
});
stories.add('With one member', () => {
    return (React.createElement(GroupV2JoinDialog_1.GroupV2JoinDialog, Object.assign({}, createProps({
        memberCount: 1,
        title: 'Just one member!',
    }))));
});
stories.add('Avatar loading state', () => {
    return (React.createElement(GroupV2JoinDialog_1.GroupV2JoinDialog, Object.assign({}, createProps({
        avatar: {
            loading: true,
        },
        title: 'Avatar loading!',
    }))));
});
stories.add('Full', () => {
    return (React.createElement(GroupV2JoinDialog_1.GroupV2JoinDialog, Object.assign({}, createProps({
        avatar: {
            url: '/fixtures/giphy-GVNvOUpeYmI7e.gif',
        },
        memberCount: 16,
        groupDescription: 'Discuss meets, events, training, and recruiting.',
        title: 'Underwater basket weavers (LA)',
    }))));
});
