"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReactionPickerPicker = exports.ReactionPickerPickerMoreButton = exports.ReactionPickerPickerEmojiButton = exports.ReactionPickerPickerStyle = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Emoji_1 = require("./emoji/Emoji");
var ReactionPickerPickerStyle;
(function (ReactionPickerPickerStyle) {
    ReactionPickerPickerStyle[ReactionPickerPickerStyle["Picker"] = 0] = "Picker";
    ReactionPickerPickerStyle[ReactionPickerPickerStyle["Menu"] = 1] = "Menu";
})(ReactionPickerPickerStyle = exports.ReactionPickerPickerStyle || (exports.ReactionPickerPickerStyle = {}));
exports.ReactionPickerPickerEmojiButton = react_1.default.forwardRef(({ emoji, onClick, isSelected, title }, ref) => (react_1.default.createElement("button", { type: "button", ref: ref, tabIndex: 0, className: (0, classnames_1.default)('module-ReactionPickerPicker__button', 'module-ReactionPickerPicker__button--emoji', isSelected && 'module-ReactionPickerPicker__button--selected'), onClick: event => {
        event.stopPropagation();
        onClick();
    } },
    react_1.default.createElement(Emoji_1.Emoji, { size: 48, emoji: emoji, title: title }))));
const ReactionPickerPickerMoreButton = ({ i18n, onClick, }) => (react_1.default.createElement("button", { "aria-label": i18n('Reactions--more'), className: "module-ReactionPickerPicker__button module-ReactionPickerPicker__button--more", onClick: event => {
        event.stopPropagation();
        onClick();
    }, tabIndex: 0, title: i18n('Reactions--more'), type: "button" },
    react_1.default.createElement("div", { className: "module-ReactionPickerPicker__button--more__dot" }),
    react_1.default.createElement("div", { className: "module-ReactionPickerPicker__button--more__dot" }),
    react_1.default.createElement("div", { className: "module-ReactionPickerPicker__button--more__dot" })));
exports.ReactionPickerPickerMoreButton = ReactionPickerPickerMoreButton;
exports.ReactionPickerPicker = (0, react_1.forwardRef)(({ children, isSomethingSelected, pickerStyle, style }, ref) => (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-ReactionPickerPicker', isSomethingSelected && 'module-ReactionPickerPicker--something-selected', {
        'module-ReactionPickerPicker--picker-style': pickerStyle === ReactionPickerPickerStyle.Picker,
        'module-ReactionPickerPicker--menu-style': pickerStyle === ReactionPickerPickerStyle.Menu,
    }), ref: ref, style: style }, children)));
