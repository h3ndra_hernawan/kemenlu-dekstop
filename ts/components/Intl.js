"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Intl = void 0;
const react_1 = __importDefault(require("react"));
const log = __importStar(require("../logging/log"));
class Intl extends react_1.default.Component {
    getComponent(index, placeholderName, key) {
        const { id, components } = this.props;
        if (!components) {
            log.error(`Error: Intl component prop not provided; Metadata: id '${id}', index ${index}, placeholder '${placeholderName}'`);
            return null;
        }
        if (Array.isArray(components)) {
            if (!components || !components.length || components.length <= index) {
                log.error(`Error: Intl missing provided component for id '${id}', index ${index}`);
                return null;
            }
            return react_1.default.createElement(react_1.default.Fragment, { key: key }, components[index]);
        }
        const value = components[placeholderName];
        if (!value) {
            log.error(`Error: Intl missing provided component for id '${id}', placeholder '${placeholderName}'`);
            return null;
        }
        return react_1.default.createElement(react_1.default.Fragment, { key: key }, value);
    }
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    render() {
        const { components, id, i18n, renderText } = this.props;
        const text = i18n(id);
        const results = [];
        const FIND_REPLACEMENTS = /\$([^$]+)\$/g;
        // We have to do this, because renderText is not required in our Props object,
        //   but it is always provided via defaultProps.
        if (!renderText) {
            return null;
        }
        if (Array.isArray(components) && components.length > 1) {
            throw new Error('Array syntax is not supported with more than one placeholder');
        }
        let componentIndex = 0;
        let key = 0;
        let lastTextIndex = 0;
        let match = FIND_REPLACEMENTS.exec(text);
        if (!match) {
            return renderText({ text, key: 0 });
        }
        while (match) {
            if (lastTextIndex < match.index) {
                const textWithNoReplacements = text.slice(lastTextIndex, match.index);
                results.push(renderText({ text: textWithNoReplacements, key }));
                key += 1;
            }
            const placeholderName = match[1];
            results.push(this.getComponent(componentIndex, placeholderName, key));
            componentIndex += 1;
            key += 1;
            lastTextIndex = FIND_REPLACEMENTS.lastIndex;
            match = FIND_REPLACEMENTS.exec(text);
        }
        if (lastTextIndex < text.length) {
            results.push(renderText({ text: text.slice(lastTextIndex), key }));
            key += 1;
        }
        return results;
    }
}
exports.Intl = Intl;
Intl.defaultProps = {
    renderText: ({ text, key }) => (react_1.default.createElement(react_1.default.Fragment, { key: key }, text)),
};
