"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const NewlyCreatedGroupInvitedContactsDialog_1 = require("./NewlyCreatedGroupInvitedContactsDialog");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const conversations = [
    (0, getDefaultConversation_1.getDefaultConversation)({ title: 'Fred Willard' }),
    (0, getDefaultConversation_1.getDefaultConversation)({ title: 'Marc Barraca' }),
];
const story = (0, react_2.storiesOf)('Components/NewlyCreatedGroupInvitedContactsDialog', module);
story.add('One contact', () => (react_1.default.createElement(NewlyCreatedGroupInvitedContactsDialog_1.NewlyCreatedGroupInvitedContactsDialog, { contacts: [conversations[0]], i18n: i18n, onClose: (0, addon_actions_1.action)('onClose') })));
story.add('Two contacts', () => (react_1.default.createElement(NewlyCreatedGroupInvitedContactsDialog_1.NewlyCreatedGroupInvitedContactsDialog, { contacts: conversations, i18n: i18n, onClose: (0, addon_actions_1.action)('onClose') })));
