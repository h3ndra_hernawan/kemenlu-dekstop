"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const react_2 = require("@storybook/react");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const Preferences_1 = require("./Preferences");
const setupI18n_1 = require("../util/setupI18n");
const Colors_1 = require("../types/Colors");
const phoneNumberSharingMode_1 = require("../util/phoneNumberSharingMode");
const phoneNumberDiscoverability_1 = require("../util/phoneNumberDiscoverability");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const availableMicrophones = [
    {
        name: 'DefAuLt (Headphones)',
        index: 0,
        uniqueId: 'Default',
        i18nKey: 'default_communication_device',
    },
];
const availableSpeakers = [
    {
        name: 'Default',
        index: 0,
        uniqueId: 'Default',
        i18nKey: 'default_communication_device',
    },
    {
        name: "Natalie's Airpods (Bluetooth)",
        index: 1,
        uniqueId: 'aa',
    },
    {
        name: 'UE Boom (Bluetooth)',
        index: 2,
        uniqueId: 'bb',
    },
];
const createProps = () => ({
    availableCameras: [
        {
            deviceId: 'dfbe6effe70b0611ba0fdc2a9ea3f39f6cb110e6687948f7e5f016c111b7329c',
            groupId: '63ee218d2446869e40adfc958ff98263e51f74382b0143328ee4826f20a76f47',
            kind: 'videoinput',
            label: 'FaceTime HD Camera (Built-in) (9fba:bced)',
        },
        {
            deviceId: 'e2db196a31d50ff9b135299dc0beea67f65b1a25a06d8a4ce76976751bb7a08d',
            groupId: '218ba7f00d7b1239cca15b9116769e5e7d30cc01104ebf84d667643661e0ecf9',
            kind: 'videoinput',
            label: 'Logitech Webcam (4e72:9058)',
        },
    ],
    availableMicrophones,
    availableSpeakers,
    blockedCount: 0,
    customColors: {},
    defaultConversationColor: Colors_1.DEFAULT_CONVERSATION_COLOR,
    deviceName: 'Work Windows ME',
    hasAudioNotifications: true,
    hasAutoDownloadUpdate: true,
    hasAutoLaunch: true,
    hasCallNotifications: true,
    hasCallRingtoneNotification: false,
    hasCountMutedConversations: false,
    hasHideMenuBar: false,
    hasIncomingCallNotifications: true,
    hasLinkPreviews: true,
    hasMediaCameraPermissions: true,
    hasMediaPermissions: true,
    hasMinimizeToAndStartInSystemTray: true,
    hasMinimizeToSystemTray: true,
    hasNotificationAttention: false,
    hasNotifications: true,
    hasReadReceipts: true,
    hasRelayCalls: false,
    hasSpellCheck: true,
    hasTypingIndicators: true,
    lastSyncTime: Date.now(),
    notificationContent: 'name',
    selectedCamera: 'dfbe6effe70b0611ba0fdc2a9ea3f39f6cb110e6687948f7e5f016c111b7329c',
    selectedMicrophone: availableMicrophones[0],
    selectedSpeaker: availableSpeakers[1],
    themeSetting: 'system',
    universalExpireTimer: 3600,
    whoCanFindMe: phoneNumberDiscoverability_1.PhoneNumberDiscoverability.Discoverable,
    whoCanSeeMe: phoneNumberSharingMode_1.PhoneNumberSharingMode.Everybody,
    zoomFactor: 1,
    addCustomColor: (0, addon_actions_1.action)('addCustomColor'),
    closeSettings: (0, addon_actions_1.action)('closeSettings'),
    doDeleteAllData: (0, addon_actions_1.action)('doDeleteAllData'),
    doneRendering: (0, addon_actions_1.action)('doneRendering'),
    editCustomColor: (0, addon_actions_1.action)('editCustomColor'),
    getConversationsWithCustomColor: () => Promise.resolve([]),
    initialSpellCheckSetting: true,
    makeSyncRequest: () => {
        (0, addon_actions_1.action)('makeSyncRequest');
        return Promise.resolve();
    },
    removeCustomColor: (0, addon_actions_1.action)('removeCustomColor'),
    removeCustomColorOnConversations: (0, addon_actions_1.action)('removeCustomColorOnConversations'),
    resetAllChatColors: (0, addon_actions_1.action)('resetAllChatColors'),
    resetDefaultChatColor: (0, addon_actions_1.action)('resetDefaultChatColor'),
    setGlobalDefaultConversationColor: (0, addon_actions_1.action)('setGlobalDefaultConversationColor'),
    isAudioNotificationsSupported: true,
    isAutoDownloadUpdatesSupported: true,
    isAutoLaunchSupported: true,
    isHideMenuBarSupported: true,
    isNotificationAttentionSupported: true,
    isPhoneNumberSharingSupported: false,
    isSyncSupported: true,
    isSystemTraySupported: true,
    onAudioNotificationsChange: (0, addon_actions_1.action)('onAudioNotificationsChange'),
    onAutoDownloadUpdateChange: (0, addon_actions_1.action)('onAutoDownloadUpdateChange'),
    onAutoLaunchChange: (0, addon_actions_1.action)('onAutoLaunchChange'),
    onCallNotificationsChange: (0, addon_actions_1.action)('onCallNotificationsChange'),
    onCallRingtoneNotificationChange: (0, addon_actions_1.action)('onCallRingtoneNotificationChange'),
    onCountMutedConversationsChange: (0, addon_actions_1.action)('onCountMutedConversationsChange'),
    onHideMenuBarChange: (0, addon_actions_1.action)('onHideMenuBarChange'),
    onIncomingCallNotificationsChange: (0, addon_actions_1.action)('onIncomingCallNotificationsChange'),
    onLastSyncTimeChange: (0, addon_actions_1.action)('onLastSyncTimeChange'),
    onMediaCameraPermissionsChange: (0, addon_actions_1.action)('onMediaCameraPermissionsChange'),
    onMediaPermissionsChange: (0, addon_actions_1.action)('onMediaPermissionsChange'),
    onMinimizeToAndStartInSystemTrayChange: (0, addon_actions_1.action)('onMinimizeToAndStartInSystemTrayChange'),
    onMinimizeToSystemTrayChange: (0, addon_actions_1.action)('onMinimizeToSystemTrayChange'),
    onNotificationAttentionChange: (0, addon_actions_1.action)('onNotificationAttentionChange'),
    onNotificationContentChange: (0, addon_actions_1.action)('onNotificationContentChange'),
    onNotificationsChange: (0, addon_actions_1.action)('onNotificationsChange'),
    onRelayCallsChange: (0, addon_actions_1.action)('onRelayCallsChange'),
    onSelectedCameraChange: (0, addon_actions_1.action)('onSelectedCameraChange'),
    onSelectedMicrophoneChange: (0, addon_actions_1.action)('onSelectedMicrophoneChange'),
    onSelectedSpeakerChange: (0, addon_actions_1.action)('onSelectedSpeakerChange'),
    onSpellCheckChange: (0, addon_actions_1.action)('onSpellCheckChange'),
    onThemeChange: (0, addon_actions_1.action)('onThemeChange'),
    onUniversalExpireTimerChange: (0, addon_actions_1.action)('onUniversalExpireTimerChange'),
    onZoomFactorChange: (0, addon_actions_1.action)('onZoomFactorChange'),
    i18n,
});
const story = (0, react_2.storiesOf)('Components/Preferences', module);
story.add('Preferences', () => react_1.default.createElement(Preferences_1.Preferences, Object.assign({}, createProps())));
story.add('Blocked 1', () => (react_1.default.createElement(Preferences_1.Preferences, Object.assign({}, createProps(), { blockedCount: 1 }))));
story.add('Blocked Many', () => (react_1.default.createElement(Preferences_1.Preferences, Object.assign({}, createProps(), { blockedCount: 55 }))));
story.add('Custom universalExpireTimer', () => (react_1.default.createElement(Preferences_1.Preferences, Object.assign({}, createProps(), { universalExpireTimer: 9000 }))));
