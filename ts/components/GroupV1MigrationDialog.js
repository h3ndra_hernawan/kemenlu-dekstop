"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupV1MigrationDialog = void 0;
const React = __importStar(require("react"));
const GroupDialog_1 = require("./GroupDialog");
const sortByTitle_1 = require("../util/sortByTitle");
exports.GroupV1MigrationDialog = React.memo((props) => {
    const { areWeInvited, droppedMembers, hasMigrated, i18n, invitedMembers, migrate, onClose, } = props;
    const title = hasMigrated
        ? i18n('GroupV1--Migration--info--title')
        : i18n('GroupV1--Migration--migrate--title');
    const keepHistory = hasMigrated
        ? i18n('GroupV1--Migration--info--keep-history')
        : i18n('GroupV1--Migration--migrate--keep-history');
    const migrationKey = hasMigrated ? 'after' : 'before';
    const droppedMembersKey = `GroupV1--Migration--info--removed--${migrationKey}`;
    let primaryButtonText;
    let onClickPrimaryButton;
    let secondaryButtonProps;
    if (hasMigrated) {
        primaryButtonText = i18n('Confirmation--confirm');
        onClickPrimaryButton = onClose;
    }
    else {
        primaryButtonText = i18n('GroupV1--Migration--migrate');
        onClickPrimaryButton = migrate;
        secondaryButtonProps = {
            secondaryButtonText: i18n('cancel'),
            onClickSecondaryButton: onClose,
        };
    }
    return (React.createElement(GroupDialog_1.GroupDialog, Object.assign({ i18n: i18n, onClickPrimaryButton: onClickPrimaryButton, onClose: onClose, primaryButtonText: primaryButtonText, title: title }, secondaryButtonProps),
        React.createElement(GroupDialog_1.GroupDialog.Paragraph, null, i18n('GroupV1--Migration--info--summary')),
        React.createElement(GroupDialog_1.GroupDialog.Paragraph, null, keepHistory),
        areWeInvited ? (React.createElement(GroupDialog_1.GroupDialog.Paragraph, null, i18n('GroupV1--Migration--info--invited--you'))) : (React.createElement(React.Fragment, null,
            renderMembers(invitedMembers, 'GroupV1--Migration--info--invited', i18n),
            renderMembers(droppedMembers, droppedMembersKey, i18n)))));
});
function renderMembers(members, prefix, i18n) {
    if (!members.length) {
        return null;
    }
    const postfix = members.length === 1 ? '--one' : '--many';
    const key = `${prefix}${postfix}`;
    return (React.createElement(React.Fragment, null,
        React.createElement(GroupDialog_1.GroupDialog.Paragraph, null, i18n(key)),
        React.createElement(GroupDialog_1.GroupDialog.Contacts, { contacts: (0, sortByTitle_1.sortByTitle)(members), i18n: i18n })));
}
