"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomColorEditor = void 0;
const react_1 = __importStar(require("react"));
const Button_1 = require("./Button");
const GradientDial_1 = require("./GradientDial");
const SampleMessageBubbles_1 = require("./SampleMessageBubbles");
const Slider_1 = require("./Slider");
const Tabs_1 = require("./Tabs");
const getHSL_1 = require("../util/getHSL");
const getCustomColorStyle_1 = require("../util/getCustomColorStyle");
var TabViews;
(function (TabViews) {
    TabViews["Solid"] = "Solid";
    TabViews["Gradient"] = "Gradient";
})(TabViews || (TabViews = {}));
function getPercentage(value, max) {
    return (100 * value) / max;
}
function getValue(percentage, max) {
    return Math.round((max / 100) * percentage);
}
const MAX_HUE = 360;
const ULTRAMARINE_ISH_VALUES = {
    hue: 220,
    saturation: 84,
};
const ULTRAMARINE_ISH = {
    start: ULTRAMARINE_ISH_VALUES,
    deg: 180,
};
const CustomColorEditor = ({ customColor = ULTRAMARINE_ISH, i18n, onClose, onSave, }) => {
    const [color, setColor] = (0, react_1.useState)(customColor);
    const [selectedColorKnob, setSelectedColorKnob] = (0, react_1.useState)(GradientDial_1.KnobType.start);
    const { hue, saturation } = color[selectedColorKnob] || ULTRAMARINE_ISH_VALUES;
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(Tabs_1.Tabs, { initialSelectedTab: color.end ? TabViews.Gradient : TabViews.Solid, moduleClassName: "CustomColorEditor__tabs", onTabChange: selectedTab => {
                if (selectedTab === TabViews.Gradient && !color.end) {
                    setColor(Object.assign(Object.assign({}, color), { end: ULTRAMARINE_ISH_VALUES }));
                }
                if (selectedTab === TabViews.Solid && color.end) {
                    setColor(Object.assign(Object.assign({}, color), { end: undefined }));
                }
            }, tabs: [
                {
                    id: TabViews.Solid,
                    label: i18n('CustomColorEditor__solid'),
                },
                {
                    id: TabViews.Gradient,
                    label: i18n('CustomColorEditor__gradient'),
                },
            ] }, ({ selectedTab }) => (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "CustomColorEditor__messages" },
                react_1.default.createElement(SampleMessageBubbles_1.SampleMessageBubbles, { backgroundStyle: (0, getCustomColorStyle_1.getCustomColorStyle)(color), color: "custom", i18n: i18n, includeAnotherBubble: true }),
                selectedTab === TabViews.Gradient && (react_1.default.createElement(react_1.default.Fragment, null,
                    react_1.default.createElement(GradientDial_1.GradientDial, { deg: color.deg, knob1Style: { backgroundColor: (0, getHSL_1.getHSL)(color.start) }, knob2Style: {
                            backgroundColor: (0, getHSL_1.getHSL)(color.end || ULTRAMARINE_ISH_VALUES),
                        }, onChange: deg => {
                            setColor(Object.assign(Object.assign({}, color), { deg }));
                        }, onClick: knob => setSelectedColorKnob(knob), selectedKnob: selectedColorKnob })))),
            react_1.default.createElement("div", { className: "CustomColorEditor__slider-container" },
                i18n('CustomColorEditor__hue'),
                react_1.default.createElement(Slider_1.Slider, { handleStyle: {
                        backgroundColor: (0, getHSL_1.getHSL)({
                            hue,
                            saturation: 100,
                        }),
                    }, label: i18n('CustomColorEditor__hue'), moduleClassName: "CustomColorEditor__hue-slider", onChange: (percentage) => {
                        setColor(Object.assign(Object.assign({}, color), { [selectedColorKnob]: Object.assign(Object.assign(Object.assign({}, ULTRAMARINE_ISH_VALUES), color[selectedColorKnob]), { hue: getValue(percentage, MAX_HUE) }) }));
                    }, value: getPercentage(hue, MAX_HUE) })),
            react_1.default.createElement("div", { className: "CustomColorEditor__slider-container" },
                i18n('CustomColorEditor__saturation'),
                react_1.default.createElement(Slider_1.Slider, { containerStyle: (0, getCustomColorStyle_1.getCustomColorStyle)({
                        deg: 180,
                        start: { hue, saturation: 0 },
                        end: { hue, saturation: 100 },
                    }), handleStyle: {
                        backgroundColor: (0, getHSL_1.getHSL)(color[selectedColorKnob] || ULTRAMARINE_ISH_VALUES),
                    }, label: i18n('CustomColorEditor__saturation'), moduleClassName: "CustomColorEditor__saturation-slider", onChange: (value) => {
                        setColor(Object.assign(Object.assign({}, color), { [selectedColorKnob]: Object.assign(Object.assign(Object.assign({}, ULTRAMARINE_ISH_VALUES), color[selectedColorKnob]), { saturation: value }) }));
                    }, value: saturation })),
            react_1.default.createElement("div", { className: "CustomColorEditor__footer" },
                react_1.default.createElement(Button_1.Button, { variant: Button_1.ButtonVariant.Secondary, onClick: onClose }, i18n('cancel')),
                react_1.default.createElement(Button_1.Button, { onClick: () => {
                        onSave(color);
                        onClose();
                    } }, i18n('save'))))))));
};
exports.CustomColorEditor = CustomColorEditor;
