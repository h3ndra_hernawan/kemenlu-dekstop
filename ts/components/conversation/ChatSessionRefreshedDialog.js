"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatSessionRefreshedDialog = void 0;
const React = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Modal_1 = require("../Modal");
const useRestoreFocus_1 = require("../../hooks/useRestoreFocus");
function ChatSessionRefreshedDialog(props) {
    const { i18n, contactSupport, onClose } = props;
    // Focus first button after initial render, restore focus on teardown
    const [focusRef] = (0, useRestoreFocus_1.useRestoreFocus)();
    return (React.createElement(Modal_1.Modal, { hasXButton: false, onClose: onClose, i18n: i18n },
        React.createElement("div", { className: "module-chat-session-refreshed-dialog" },
            React.createElement("div", { className: "module-chat-session-refreshed-dialog__image" },
                React.createElement("img", { src: "images/chat-session-refresh.svg", height: "110", width: "200", alt: "" })),
            React.createElement("div", { className: "module-chat-session-refreshed-dialog__title" }, i18n('ChatRefresh--notification')),
            React.createElement("div", { className: "module-chat-session-refreshed-dialog__description" }, i18n('ChatRefresh--summary')),
            React.createElement("div", { className: "module-chat-session-refreshed-dialog__buttons" },
                React.createElement("button", { type: "button", onClick: contactSupport, className: (0, classnames_1.default)('module-chat-session-refreshed-dialog__button', 'module-chat-session-refreshed-dialog__button--secondary') }, i18n('ChatRefresh--contactSupport')),
                React.createElement("button", { type: "button", onClick: onClose, ref: focusRef, className: "module-chat-session-refreshed-dialog__button" }, i18n('Confirmation--confirm'))))));
}
exports.ChatSessionRefreshedDialog = ChatSessionRefreshedDialog;
