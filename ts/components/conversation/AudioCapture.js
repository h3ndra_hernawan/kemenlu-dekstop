"use strict";
// Copyright 2016-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AudioCapture = void 0;
const react_1 = __importStar(require("react"));
const moment = __importStar(require("moment"));
const lodash_1 = require("lodash");
const ConfirmationDialog_1 = require("../ConfirmationDialog");
const audioRecorder_1 = require("../../state/ducks/audioRecorder");
const ToastVoiceNoteLimit_1 = require("../ToastVoiceNoteLimit");
const ToastVoiceNoteMustBeOnlyAttachment_1 = require("../ToastVoiceNoteMustBeOnlyAttachment");
const useEscapeHandling_1 = require("../../hooks/useEscapeHandling");
const useKeyboardShortcuts_1 = require("../../hooks/useKeyboardShortcuts");
var ToastType;
(function (ToastType) {
    ToastType[ToastType["VoiceNoteLimit"] = 0] = "VoiceNoteLimit";
    ToastType[ToastType["VoiceNoteMustBeOnlyAttachment"] = 1] = "VoiceNoteMustBeOnlyAttachment";
})(ToastType || (ToastType = {}));
const START_DURATION_TEXT = '0:00';
const AudioCapture = ({ cancelRecording, completeRecording, conversationId, draftAttachments, errorDialogAudioRecorderType, errorRecording, i18n, recordingState, onSendAudioRecording, startRecording, }) => {
    const [durationText, setDurationText] = (0, react_1.useState)(START_DURATION_TEXT);
    const [toastType, setToastType] = (0, react_1.useState)();
    // Cancel recording if we switch away from this conversation, unmounting
    (0, react_1.useEffect)(() => {
        return () => {
            cancelRecording();
        };
    }, [cancelRecording]);
    // Stop recording and show confirmation if user switches away from this app
    (0, react_1.useEffect)(() => {
        if (recordingState !== audioRecorder_1.RecordingState.Recording) {
            return;
        }
        const handler = () => {
            errorRecording(audioRecorder_1.ErrorDialogAudioRecorderType.Blur);
        };
        window.addEventListener('blur', handler);
        return () => {
            window.removeEventListener('blur', handler);
        };
    }, [recordingState, completeRecording, errorRecording]);
    const escapeRecording = (0, react_1.useCallback)(() => {
        if (recordingState !== audioRecorder_1.RecordingState.Recording) {
            return;
        }
        cancelRecording();
    }, [cancelRecording, recordingState]);
    (0, useEscapeHandling_1.useEscapeHandling)(escapeRecording);
    const startRecordingShortcut = (0, useKeyboardShortcuts_1.useStartRecordingShortcut)(startRecording);
    (0, useKeyboardShortcuts_1.useKeyboardShortcuts)(startRecordingShortcut);
    const closeToast = (0, react_1.useCallback)(() => {
        setToastType(undefined);
    }, []);
    // Update timestamp regularly, then timeout if recording goes over five minutes
    (0, react_1.useEffect)(() => {
        if (recordingState !== audioRecorder_1.RecordingState.Recording) {
            return;
        }
        setDurationText(START_DURATION_TEXT);
        setToastType(ToastType.VoiceNoteLimit);
        const startTime = Date.now();
        const interval = setInterval(() => {
            const duration = moment.duration(Date.now() - startTime, 'ms');
            const minutes = `${Math.trunc(duration.asMinutes())}`;
            let seconds = `${duration.seconds()}`;
            if (seconds.length < 2) {
                seconds = `0${seconds}`;
            }
            setDurationText(`${minutes}:${seconds}`);
            if (duration >= moment.duration(5, 'minutes')) {
                errorRecording(audioRecorder_1.ErrorDialogAudioRecorderType.Timeout);
            }
        }, 1000);
        return () => {
            clearInterval(interval);
            closeToast();
        };
    }, [
        closeToast,
        completeRecording,
        errorRecording,
        recordingState,
        setDurationText,
    ]);
    const clickCancel = (0, react_1.useCallback)(() => {
        cancelRecording();
    }, [cancelRecording]);
    const clickSend = (0, react_1.useCallback)(() => {
        completeRecording(conversationId, onSendAudioRecording);
    }, [conversationId, completeRecording, onSendAudioRecording]);
    let toastElement;
    if (toastType === ToastType.VoiceNoteLimit) {
        toastElement = react_1.default.createElement(ToastVoiceNoteLimit_1.ToastVoiceNoteLimit, { i18n: i18n, onClose: closeToast });
    }
    else if (toastType === ToastType.VoiceNoteMustBeOnlyAttachment) {
        toastElement = (react_1.default.createElement(ToastVoiceNoteMustBeOnlyAttachment_1.ToastVoiceNoteMustBeOnlyAttachment, { i18n: i18n, onClose: closeToast }));
    }
    let confirmationDialog;
    if (errorDialogAudioRecorderType === audioRecorder_1.ErrorDialogAudioRecorderType.Blur ||
        errorDialogAudioRecorderType === audioRecorder_1.ErrorDialogAudioRecorderType.Timeout) {
        const confirmationDialogText = errorDialogAudioRecorderType === audioRecorder_1.ErrorDialogAudioRecorderType.Blur
            ? i18n('voiceRecordingInterruptedBlur')
            : i18n('voiceRecordingInterruptedMax');
        confirmationDialog = (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onCancel: clickCancel, onClose: lodash_1.noop, cancelText: i18n('discard'), actions: [
                {
                    text: i18n('sendAnyway'),
                    style: 'affirmative',
                    action: clickSend,
                },
            ] }, confirmationDialogText));
    }
    else if (errorDialogAudioRecorderType === audioRecorder_1.ErrorDialogAudioRecorderType.ErrorRecording) {
        confirmationDialog = (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onCancel: clickCancel, onClose: lodash_1.noop, cancelText: i18n('ok'), actions: [] }, i18n('voiceNoteError')));
    }
    if (recordingState === audioRecorder_1.RecordingState.Recording && !confirmationDialog) {
        return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "AudioCapture" },
                react_1.default.createElement("button", { className: "AudioCapture__recorder-button AudioCapture__recorder-button--complete", onClick: clickSend, tabIndex: 0, title: i18n('voiceRecording--complete'), type: "button" },
                    react_1.default.createElement("span", { className: "icon" })),
                react_1.default.createElement("span", { className: "AudioCapture__time" }, durationText),
                react_1.default.createElement("button", { className: "AudioCapture__recorder-button AudioCapture__recorder-button--cancel", onClick: clickCancel, tabIndex: 0, title: i18n('voiceRecording--cancel'), type: "button" },
                    react_1.default.createElement("span", { className: "icon" }))),
            toastElement));
    }
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: "AudioCapture" },
            react_1.default.createElement("button", { "aria-label": i18n('voiceRecording--start'), className: "AudioCapture__microphone", onClick: () => {
                    if (draftAttachments.length) {
                        setToastType(ToastType.VoiceNoteMustBeOnlyAttachment);
                    }
                    else {
                        startRecording();
                    }
                }, title: i18n('voiceRecording--start'), type: "button" }),
            confirmationDialog),
        toastElement));
};
exports.AudioCapture = AudioCapture;
