"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimerNotification = void 0;
const react_1 = __importDefault(require("react"));
const ContactName_1 = require("./ContactName");
const SystemMessage_1 = require("./SystemMessage");
const Intl_1 = require("../Intl");
const expirationTimer = __importStar(require("../../util/expirationTimer"));
const log = __importStar(require("../../logging/log"));
const TimerNotification = props => {
    const { disabled, i18n, title, type } = props;
    let changeKey;
    let timespan;
    if (props.disabled) {
        changeKey = 'disabledDisappearingMessages';
        timespan = ''; // Set to the empty string to satisfy types
    }
    else {
        changeKey = 'theyChangedTheTimer';
        timespan = expirationTimer.format(i18n, props.expireTimer);
    }
    let message;
    switch (type) {
        case 'fromOther':
            message = (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: changeKey, components: {
                    name: react_1.default.createElement(ContactName_1.ContactName, { key: "external-1", title: title }),
                    time: timespan,
                } }));
            break;
        case 'fromMe':
            message = disabled
                ? i18n('youDisabledDisappearingMessages')
                : i18n('youChangedTheTimer', [timespan]);
            break;
        case 'fromSync':
            message = disabled
                ? i18n('disappearingMessagesDisabled')
                : i18n('timerSetOnSync', [timespan]);
            break;
        case 'fromMember':
            message = disabled
                ? i18n('disappearingMessagesDisabledByMember')
                : i18n('timerSetByMember', [timespan]);
            break;
        default:
            log.warn('TimerNotification: unsupported type provided:', type);
            break;
    }
    const icon = disabled ? 'timer-disabled' : 'timer';
    return react_1.default.createElement(SystemMessage_1.SystemMessage, { icon: icon, contents: message });
};
exports.TimerNotification = TimerNotification;
