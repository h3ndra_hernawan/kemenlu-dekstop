"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_2 = require("@storybook/react");
const GroupDescription_1 = require("./GroupDescription");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/Conversation/GroupDescription', module);
const createProps = (overrideProps = {}) => ({
    i18n,
    title: (0, addon_knobs_1.text)('title', overrideProps.title || 'Sample Title'),
    text: (0, addon_knobs_1.text)('text', overrideProps.text || 'Default group description'),
});
story.add('Default', () => react_1.default.createElement(GroupDescription_1.GroupDescription, Object.assign({}, createProps())));
story.add('Long', () => (react_1.default.createElement(GroupDescription_1.GroupDescription, Object.assign({}, createProps({
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed vehicula urna. Ut rhoncus, justo a vestibulum elementum, libero ligula molestie massa, et volutpat nibh ipsum sit amet enim. Vestibulum ac mi enim. Nulla fringilla justo justo, volutpat semper ex convallis quis. Proin posuere, mi at auctor tincidunt, magna turpis mattis nibh, ullamcorper vehicula lectus mauris in mauris. Nullam blandit sapien tortor, quis vehicula quam molestie nec. Nam sagittis dolor in eros dapibus scelerisque. Proin vitae ex sed magna lobortis tincidunt. Aenean dictum laoreet dolor, at suscipit ligula fermentum ac. Nam condimentum turpis quis sollicitudin rhoncus.',
})))));
story.add('With newlines', () => (react_1.default.createElement(GroupDescription_1.GroupDescription, Object.assign({}, createProps({
    text: 'This is long\n\nSo many lines\n\nToo many lines?',
})))));
story.add('With emoji', () => (react_1.default.createElement(GroupDescription_1.GroupDescription, Object.assign({}, createProps({
    text: '🍒🍩🌭',
})))));
story.add('With link', () => (react_1.default.createElement(GroupDescription_1.GroupDescription, Object.assign({}, createProps({
    text: 'I love https://example.com and http://example.com and example.com, but not https://user:bar@example.com',
})))));
story.add('Kitchen sink', () => (react_1.default.createElement(GroupDescription_1.GroupDescription, Object.assign({}, createProps({
    text: '🍒 https://example.com this is a long thing\nhttps://example.com on another line\nhttps://example.com',
})))));
