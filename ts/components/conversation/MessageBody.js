"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageBody = void 0;
const react_1 = __importDefault(require("react"));
const lib_1 = require("../emoji/lib");
const AtMentionify_1 = require("./AtMentionify");
const Emojify_1 = require("./Emojify");
const AddNewLines_1 = require("./AddNewLines");
const Linkify_1 = require("./Linkify");
const renderEmoji = ({ text, key, sizeClass, renderNonEmoji, }) => (react_1.default.createElement(Emojify_1.Emojify, { key: key, text: text, sizeClass: sizeClass, renderNonEmoji: renderNonEmoji }));
/**
 * This component makes it very easy to use all three of our message formatting
 * components: `Emojify`, `Linkify`, and `AddNewLines`. Because each of them is fully
 * configurable with their `renderXXX` props, this component will assemble all three of
 * them for you.
 */
function MessageBody({ bodyRanges, direction, disableJumbomoji, disableLinks, i18n, onIncreaseTextLength, openConversation, text, textPending, }) {
    const hasReadMore = Boolean(onIncreaseTextLength);
    const textWithSuffix = textPending || hasReadMore ? `${text}...` : text;
    const sizeClass = disableJumbomoji ? undefined : (0, lib_1.getSizeClass)(text);
    const processedText = AtMentionify_1.AtMentionify.preprocessMentions(textWithSuffix, bodyRanges);
    const renderNewLines = ({ text: textWithNewLines, key, }) => {
        return (react_1.default.createElement(AddNewLines_1.AddNewLines, { key: key, text: textWithNewLines, renderNonNewLine: ({ text: innerText, key: innerKey }) => (react_1.default.createElement(AtMentionify_1.AtMentionify, { key: innerKey, direction: direction, text: innerText, bodyRanges: bodyRanges, openConversation: openConversation })) }));
    };
    return (react_1.default.createElement("span", null,
        disableLinks ? (renderEmoji({
            i18n,
            text: processedText,
            sizeClass,
            key: 0,
            renderNonEmoji: renderNewLines,
        })) : (react_1.default.createElement(Linkify_1.Linkify, { text: processedText, renderNonLink: ({ key, text: nonLinkText }) => {
                return renderEmoji({
                    i18n,
                    text: nonLinkText,
                    sizeClass,
                    key,
                    renderNonEmoji: renderNewLines,
                });
            } })),
        textPending ? (react_1.default.createElement("span", { className: "MessageBody__highlight" },
            " ",
            i18n('downloading'))) : null,
        onIncreaseTextLength ? (react_1.default.createElement("button", { className: "MessageBody__read-more", onClick: () => {
                onIncreaseTextLength();
            }, onKeyDown: (ev) => {
                if (ev.key === 'Space' || ev.key === 'Enter') {
                    onIncreaseTextLength();
                }
            }, tabIndex: 0, type: "button" },
            ' ',
            i18n('MessageBody--read-more'))) : null));
}
exports.MessageBody = MessageBody;
