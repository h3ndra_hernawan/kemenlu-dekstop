"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GIF = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const react_blurhash_1 = require("react-blurhash");
const Spinner_1 = require("../Spinner");
const Attachment_1 = require("../../types/Attachment");
const log = __importStar(require("../../logging/log"));
const MAX_GIF_REPEAT = 4;
const MAX_GIF_TIME = 8;
const GIF = props => {
    const { attachment, size, tabIndex, i18n, theme, reducedMotion = Boolean(window.Accessibility && window.Accessibility.reducedMotionSetting), onError, showVisualAttachment, kickOffAttachmentDownload, } = props;
    const tapToPlay = reducedMotion;
    const videoRef = (0, react_1.useRef)(null);
    const { height, width } = (0, Attachment_1.getImageDimensions)(attachment, size);
    const [repeatCount, setRepeatCount] = (0, react_1.useState)(0);
    const [playTime, setPlayTime] = (0, react_1.useState)(MAX_GIF_TIME);
    const [currentTime, setCurrentTime] = (0, react_1.useState)(0);
    const [isFocused, setIsFocused] = (0, react_1.useState)(true);
    const [isPlaying, setIsPlaying] = (0, react_1.useState)(!tapToPlay);
    (0, react_1.useEffect)(() => {
        const onFocus = () => setIsFocused(true);
        const onBlur = () => setIsFocused(false);
        window.addEventListener('focus', onFocus);
        window.addEventListener('blur', onBlur);
        return () => {
            window.removeEventListener('focus', onFocus);
            window.removeEventListener('blur', onBlur);
        };
    });
    //
    // Play & Pause video in response to change of `isPlaying` and `repeatCount`.
    //
    (0, react_1.useEffect)(() => {
        const { current: video } = videoRef;
        if (!video) {
            return;
        }
        if (isPlaying) {
            video.play().catch(error => {
                log.info("Failed to match GIF playback to window's state", (error && error.stack) || error);
            });
        }
        else {
            video.pause();
        }
    }, [isPlaying, repeatCount]);
    //
    // Change `isPlaying` in response to focus, play time, and repeat count
    // changes.
    //
    (0, react_1.useEffect)(() => {
        const { current: video } = videoRef;
        if (!video) {
            return;
        }
        let isTapToPlayPaused = false;
        if (tapToPlay) {
            if (playTime + currentTime >= MAX_GIF_TIME ||
                repeatCount >= MAX_GIF_REPEAT) {
                isTapToPlayPaused = true;
            }
        }
        setIsPlaying(isFocused && !isTapToPlayPaused);
    }, [isFocused, playTime, currentTime, repeatCount, tapToPlay]);
    const onTimeUpdate = async (event) => {
        const { currentTime: reportedTime } = event.currentTarget;
        if (!Number.isNaN(reportedTime)) {
            setCurrentTime(reportedTime);
        }
    };
    const onEnded = async (event) => {
        const { currentTarget: video } = event;
        const { duration } = video;
        setRepeatCount(repeatCount + 1);
        if (!Number.isNaN(duration)) {
            video.currentTime = 0;
            setCurrentTime(0);
            setPlayTime(playTime + duration);
        }
    };
    const onOverlayClick = (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (!attachment.url) {
            kickOffAttachmentDownload();
        }
        else if (tapToPlay) {
            setPlayTime(0);
            setCurrentTime(0);
            setRepeatCount(0);
        }
    };
    const onOverlayKeyDown = (event) => {
        if (event.key !== 'Enter' && event.key !== 'Space') {
            return;
        }
        event.preventDefault();
        event.stopPropagation();
        kickOffAttachmentDownload();
    };
    const isPending = Boolean(attachment.pending);
    const isNotDownloaded = (0, Attachment_1.hasNotDownloaded)(attachment) && !isPending;
    let fileSize;
    if (isNotDownloaded && attachment.fileSize) {
        fileSize = (react_1.default.createElement("div", { className: "module-image--gif__filesize" },
            attachment.fileSize,
            " \u00B7 GIF"));
    }
    let gif;
    if (isNotDownloaded || isPending) {
        gif = (react_1.default.createElement(react_blurhash_1.Blurhash, { hash: attachment.blurHash || (0, Attachment_1.defaultBlurHash)(theme), width: width, height: height, style: { display: 'block' } }));
    }
    else {
        gif = (react_1.default.createElement("video", { ref: videoRef, onTimeUpdate: onTimeUpdate, onEnded: onEnded, onError: onError, onClick: (event) => {
                event.preventDefault();
                event.stopPropagation();
                showVisualAttachment();
            }, className: "module-image--gif__video", autoPlay: true, playsInline: true, muted: true, poster: attachment.screenshot && attachment.screenshot.url, height: height, width: width, src: attachment.url }));
    }
    let overlay;
    if ((tapToPlay && !isPlaying) || isNotDownloaded) {
        const className = (0, classnames_1.default)([
            'module-image__border-overlay',
            'module-image__border-overlay--with-click-handler',
            'module-image--soft-corners',
            isNotDownloaded
                ? 'module-image--not-downloaded'
                : 'module-image--tap-to-play',
        ]);
        overlay = (react_1.default.createElement("button", { type: "button", className: className, onClick: onOverlayClick, onKeyDown: onOverlayKeyDown, tabIndex: tabIndex },
            react_1.default.createElement("span", null)));
    }
    let spinner;
    if (isPending) {
        spinner = (react_1.default.createElement("div", { className: "module-image__download-pending--spinner-container" },
            react_1.default.createElement("div", { className: "module-image__download-pending--spinner", title: i18n('loading') },
                react_1.default.createElement(Spinner_1.Spinner, { moduleClassName: "module-image-spinner", svgSize: "small" }))));
    }
    return (react_1.default.createElement("div", { className: "module-image module-image--gif" },
        gif,
        overlay,
        spinner,
        fileSize));
};
exports.GIF = GIF;
