"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const getRandomColor_1 = require("../../test-both/helpers/getRandomColor");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const StorybookThemeContext_1 = require("../../../.storybook/StorybookThemeContext");
const ConversationHeader_1 = require("./ConversationHeader");
const Fixtures_1 = require("../../storybook/Fixtures");
const book = (0, react_2.storiesOf)('Components/Conversation/ConversationHeader', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const commonProps = Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)()), { showBackButton: false, outgoingCallButtonStyle: ConversationHeader_1.OutgoingCallButtonStyle.Both, i18n, onShowConversationDetails: (0, addon_actions_1.action)('onShowConversationDetails'), onSetDisappearingMessages: (0, addon_actions_1.action)('onSetDisappearingMessages'), onDeleteMessages: (0, addon_actions_1.action)('onDeleteMessages'), onSearchInConversation: (0, addon_actions_1.action)('onSearchInConversation'), onSetMuteNotifications: (0, addon_actions_1.action)('onSetMuteNotifications'), onOutgoingAudioCallInConversation: (0, addon_actions_1.action)('onOutgoingAudioCallInConversation'), onOutgoingVideoCallInConversation: (0, addon_actions_1.action)('onOutgoingVideoCallInConversation'), onShowAllMedia: (0, addon_actions_1.action)('onShowAllMedia'), onShowGroupMembers: (0, addon_actions_1.action)('onShowGroupMembers'), onGoBack: (0, addon_actions_1.action)('onGoBack'), onArchive: (0, addon_actions_1.action)('onArchive'), onMarkUnread: (0, addon_actions_1.action)('onMarkUnread'), onMoveToInbox: (0, addon_actions_1.action)('onMoveToInbox'), onSetPin: (0, addon_actions_1.action)('onSetPin') });
const stories = [
    {
        title: '1:1 conversation',
        description: "Note the five items in menu, and the second-level menu with disappearing messages options. Disappearing message set to 'off'.",
        items: [
            {
                title: 'With name and profile, verified',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), isVerified: true, avatarPath: Fixtures_1.gifUrl, title: 'Someone 🔥 Somewhere', name: 'Someone 🔥 Somewhere', phoneNumber: '(202) 555-0001', type: 'direct', id: '1', profileName: '🔥Flames🔥', acceptedMessageRequest: true }),
            },
            {
                title: 'With name, not verified, no avatar',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), isVerified: false, title: 'Someone 🔥 Somewhere', name: 'Someone 🔥 Somewhere', phoneNumber: '(202) 555-0002', type: 'direct', id: '2', acceptedMessageRequest: true }),
            },
            {
                title: 'With name, not verified, descenders',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), isVerified: false, title: 'Joyrey 🔥 Leppey', name: 'Joyrey 🔥 Leppey', phoneNumber: '(202) 555-0002', type: 'direct', id: '3', acceptedMessageRequest: true }),
            },
            {
                title: 'Profile, no name',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), isVerified: false, phoneNumber: '(202) 555-0003', type: 'direct', id: '4', title: '🔥Flames🔥', profileName: '🔥Flames🔥', acceptedMessageRequest: true }),
            },
            {
                title: 'No name, no profile, no color',
                props: Object.assign(Object.assign({}, commonProps), { title: '(202) 555-0011', phoneNumber: '(202) 555-0011', type: 'direct', id: '5', acceptedMessageRequest: true }),
            },
            {
                title: 'With back button',
                props: Object.assign(Object.assign({}, commonProps), { showBackButton: true, color: (0, getRandomColor_1.getRandomColor)(), phoneNumber: '(202) 555-0004', title: '(202) 555-0004', type: 'direct', id: '6', acceptedMessageRequest: true }),
            },
            {
                title: 'Disappearing messages set',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: '(202) 555-0005', phoneNumber: '(202) 555-0005', type: 'direct', id: '7', expireTimer: 10, acceptedMessageRequest: true }),
            },
            {
                title: 'Disappearing messages + verified',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: '(202) 555-0005', phoneNumber: '(202) 555-0005', type: 'direct', id: '8', expireTimer: 300, acceptedMessageRequest: true, isVerified: true, canChangeTimer: true }),
            },
            {
                title: 'Muting Conversation',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: '(202) 555-0006', phoneNumber: '(202) 555-0006', type: 'direct', id: '9', acceptedMessageRequest: true, muteExpiresAt: new Date('3000-10-18T11:11:11Z').valueOf() }),
            },
            {
                title: 'SMS-only conversation',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: '(202) 555-0006', phoneNumber: '(202) 555-0006', type: 'direct', id: '10', acceptedMessageRequest: true, isSMSOnly: true }),
            },
        ],
    },
    {
        title: 'In a group',
        description: "Note that the menu should includes 'Show Members' instead of 'Show Safety Number'",
        items: [
            {
                title: 'Basic',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: 'Typescript support group', name: 'Typescript support group', phoneNumber: '', id: '11', type: 'group', expireTimer: 10, acceptedMessageRequest: true, outgoingCallButtonStyle: ConversationHeader_1.OutgoingCallButtonStyle.JustVideo }),
            },
            {
                title: 'In a group you left - no disappearing messages',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: 'Typescript support group', name: 'Typescript support group', phoneNumber: '', id: '12', type: 'group', left: true, expireTimer: 10, acceptedMessageRequest: true, outgoingCallButtonStyle: ConversationHeader_1.OutgoingCallButtonStyle.JustVideo }),
            },
            {
                title: 'In a group with an active group call',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: 'Typescript support group', name: 'Typescript support group', phoneNumber: '', id: '13', type: 'group', expireTimer: 10, acceptedMessageRequest: true, outgoingCallButtonStyle: ConversationHeader_1.OutgoingCallButtonStyle.Join }),
            },
            {
                title: 'In a forever muted group',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: 'Way too many messages', name: 'Way too many messages', phoneNumber: '', id: '14', type: 'group', expireTimer: 10, acceptedMessageRequest: true, outgoingCallButtonStyle: ConversationHeader_1.OutgoingCallButtonStyle.JustVideo, muteExpiresAt: Infinity }),
            },
        ],
    },
    {
        title: 'Note to Self',
        description: 'No safety number entry.',
        items: [
            {
                title: 'In chat with yourself',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: '(202) 555-0007', phoneNumber: '(202) 555-0007', id: '15', type: 'direct', isMe: true, acceptedMessageRequest: true, outgoingCallButtonStyle: ConversationHeader_1.OutgoingCallButtonStyle.None }),
            },
        ],
    },
    {
        title: 'Unaccepted',
        description: 'No safety number entry.',
        items: [
            {
                title: '1:1 conversation',
                props: Object.assign(Object.assign({}, commonProps), { color: (0, getRandomColor_1.getRandomColor)(), title: '(202) 555-0007', phoneNumber: '(202) 555-0007', id: '16', type: 'direct', isMe: false, acceptedMessageRequest: false, outgoingCallButtonStyle: ConversationHeader_1.OutgoingCallButtonStyle.None }),
            },
        ],
    },
];
stories.forEach(({ title, description, items }) => book.add(title, () => {
    const theme = (0, react_1.useContext)(StorybookThemeContext_1.StorybookThemeContext);
    return items.map(({ title: subtitle, props }, i) => {
        return (react_1.default.createElement("div", { key: i },
            subtitle ? react_1.default.createElement("h3", null, subtitle) : null,
            react_1.default.createElement(ConversationHeader_1.ConversationHeader, Object.assign({}, props, { theme: theme }))));
    });
}, {
    docs: description,
}));
