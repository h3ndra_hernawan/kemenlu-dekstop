"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupDescription = void 0;
const react_1 = __importStar(require("react"));
const Modal_1 = require("../Modal");
const GroupDescriptionText_1 = require("../GroupDescriptionText");
// Emojification can cause the scroll height to be *slightly* larger than the client
//   height, so we add a little wiggle room.
const SHOW_READ_MORE_THRESHOLD = 5;
const GroupDescription = ({ i18n, title, text, }) => {
    const textRef = (0, react_1.useRef)(null);
    const [hasReadMore, setHasReadMore] = (0, react_1.useState)(false);
    const [showFullDescription, setShowFullDescription] = (0, react_1.useState)(false);
    (0, react_1.useEffect)(() => {
        if (!textRef || !textRef.current) {
            return;
        }
        setHasReadMore(textRef.current.scrollHeight - SHOW_READ_MORE_THRESHOLD >
            textRef.current.clientHeight);
    }, [setHasReadMore, text, textRef]);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        showFullDescription && (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, i18n: i18n, onClose: () => setShowFullDescription(false), title: title },
            react_1.default.createElement(GroupDescriptionText_1.GroupDescriptionText, { text: text }))),
        react_1.default.createElement("div", { className: "GroupDescription__text", ref: textRef },
            react_1.default.createElement(GroupDescriptionText_1.GroupDescriptionText, { text: text })),
        hasReadMore && (react_1.default.createElement("button", { className: "GroupDescription__read-more", onClick: ev => {
                ev.preventDefault();
                ev.stopPropagation();
                setShowFullDescription(true);
            }, type: "button" }, i18n('GroupDescription__read-more')))));
};
exports.GroupDescription = GroupDescription;
