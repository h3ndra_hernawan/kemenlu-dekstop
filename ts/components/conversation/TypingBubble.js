"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypingBubble = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const TypingAnimation_1 = require("./TypingAnimation");
const Avatar_1 = require("../Avatar");
function TypingBubble({ acceptedMessageRequest, avatarPath, badge, color, conversationType, i18n, isMe, name, phoneNumber, profileName, sharedGroupNames, theme, title, }) {
    const isGroup = conversationType === 'group';
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message', 'module-message--incoming', isGroup ? 'module-message--group' : null) },
        isGroup && (react_1.default.createElement("div", { className: "module-message__author-avatar-container" },
            react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, badge: badge, color: color, conversationType: "direct", i18n: i18n, isMe: isMe, name: name, phoneNumber: phoneNumber, profileName: profileName, theme: theme, title: title, sharedGroupNames: sharedGroupNames, size: 28 }))),
        react_1.default.createElement("div", { className: "module-message__container-outer" },
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__container', 'module-message__container--incoming') },
                react_1.default.createElement("div", { className: "module-message__typing-container" },
                    react_1.default.createElement(TypingAnimation_1.TypingAnimation, { color: "light", i18n: i18n }))))));
}
exports.TypingBubble = TypingBubble;
