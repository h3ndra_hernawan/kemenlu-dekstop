"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmbeddedContact = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const _contactUtil_1 = require("./_contactUtil");
const EmbeddedContact = (props) => {
    const { contact, i18n, isIncoming, onClick, tabIndex, withContentAbove, withContentBelow, } = props;
    const module = 'embedded-contact';
    const direction = isIncoming ? 'incoming' : 'outgoing';
    return (react_1.default.createElement("button", { type: "button", className: (0, classnames_1.default)('module-embedded-contact', `module-embedded-contact--${direction}`, withContentAbove ? 'module-embedded-contact--with-content-above' : null, withContentBelow ? 'module-embedded-contact--with-content-below' : null), onKeyDown: (event) => {
            if (event.key !== 'Enter' && event.key !== 'Space') {
                return;
            }
            if (onClick) {
                event.stopPropagation();
                event.preventDefault();
                onClick();
            }
        }, onClick: (event) => {
            if (onClick) {
                event.stopPropagation();
                event.preventDefault();
                onClick();
            }
        }, tabIndex: tabIndex },
        (0, _contactUtil_1.renderAvatar)({ contact, i18n, size: 52, direction }),
        react_1.default.createElement("div", { className: "module-embedded-contact__text-container" },
            (0, _contactUtil_1.renderName)({ contact, isIncoming, module }),
            (0, _contactUtil_1.renderContactShorthand)({ contact, isIncoming, module }))));
};
exports.EmbeddedContact = EmbeddedContact;
