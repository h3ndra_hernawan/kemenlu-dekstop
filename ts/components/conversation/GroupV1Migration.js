"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupV1Migration = void 0;
const React = __importStar(require("react"));
const Button_1 = require("../Button");
const SystemMessage_1 = require("./SystemMessage");
const Intl_1 = require("../Intl");
const ContactName_1 = require("./ContactName");
const GroupV1MigrationDialog_1 = require("../GroupV1MigrationDialog");
const log = __importStar(require("../../logging/log"));
function GroupV1Migration(props) {
    const { areWeInvited, droppedMembers, i18n, invitedMembers } = props;
    const [showingDialog, setShowingDialog] = React.useState(false);
    const showDialog = React.useCallback(() => {
        setShowingDialog(true);
    }, [setShowingDialog]);
    const dismissDialog = React.useCallback(() => {
        setShowingDialog(false);
    }, [setShowingDialog]);
    return (React.createElement(React.Fragment, null,
        React.createElement(SystemMessage_1.SystemMessage, { icon: "group", contents: React.createElement(React.Fragment, null,
                React.createElement("p", null, i18n('GroupV1--Migration--was-upgraded')),
                React.createElement("p", null, areWeInvited ? (i18n('GroupV1--Migration--invited--you')) : (React.createElement(React.Fragment, null,
                    renderUsers(invitedMembers, i18n, 'GroupV1--Migration--invited'),
                    renderUsers(droppedMembers, i18n, 'GroupV1--Migration--removed'))))), button: React.createElement(Button_1.Button, { onClick: showDialog, size: Button_1.ButtonSize.Small, variant: Button_1.ButtonVariant.SystemMessage }, i18n('GroupV1--Migration--learn-more')) }),
        showingDialog ? (React.createElement(GroupV1MigrationDialog_1.GroupV1MigrationDialog, { areWeInvited: areWeInvited, droppedMembers: droppedMembers, hasMigrated: true, i18n: i18n, invitedMembers: invitedMembers, migrate: () => log.warn('GroupV1Migration: Modal called migrate()'), onClose: dismissDialog })) : null));
}
exports.GroupV1Migration = GroupV1Migration;
function renderUsers(members, i18n, keyPrefix) {
    if (!members || members.length === 0) {
        return null;
    }
    if (members.length === 1) {
        return (React.createElement("p", null,
            React.createElement(Intl_1.Intl, { i18n: i18n, id: `${keyPrefix}--one`, components: [React.createElement(ContactName_1.ContactName, { title: members[0].title })] })));
    }
    return React.createElement("p", null, i18n(`${keyPrefix}--many`, [members.length.toString()]));
}
