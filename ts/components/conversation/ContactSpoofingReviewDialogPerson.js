"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactSpoofingReviewDialogPerson = void 0;
const react_1 = __importDefault(require("react"));
const assert_1 = require("../../util/assert");
const Avatar_1 = require("../Avatar");
const ContactName_1 = require("./ContactName");
const SharedGroupNames_1 = require("../SharedGroupNames");
const ContactSpoofingReviewDialogPerson = ({ children, conversation, i18n, onClick }) => {
    (0, assert_1.assert)(conversation.type === 'direct', '<ContactSpoofingReviewDialogPerson> expected a direct conversation');
    const contents = (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(Avatar_1.Avatar, Object.assign({}, conversation, { conversationType: conversation.type, size: Avatar_1.AvatarSize.FIFTY_TWO, className: "module-ContactSpoofingReviewDialogPerson__avatar", i18n: i18n })),
        react_1.default.createElement("div", { className: "module-ContactSpoofingReviewDialogPerson__info" },
            react_1.default.createElement(ContactName_1.ContactName, { module: "module-ContactSpoofingReviewDialogPerson__info__contact-name", title: conversation.title }),
            conversation.phoneNumber ? (react_1.default.createElement("div", { className: "module-ContactSpoofingReviewDialogPerson__info__property" }, conversation.phoneNumber)) : null,
            react_1.default.createElement("div", { className: "module-ContactSpoofingReviewDialogPerson__info__property" },
                react_1.default.createElement(SharedGroupNames_1.SharedGroupNames, { i18n: i18n, sharedGroupNames: conversation.sharedGroupNames || [] })),
            children)));
    if (onClick) {
        return (react_1.default.createElement("button", { type: "button", className: "module-ContactSpoofingReviewDialogPerson", onClick: onClick }, contents));
    }
    return (react_1.default.createElement("div", { className: "module-ContactSpoofingReviewDialogPerson" }, contents));
};
exports.ContactSpoofingReviewDialogPerson = ContactSpoofingReviewDialogPerson;
