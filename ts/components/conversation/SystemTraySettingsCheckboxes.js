"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SystemTraySettingsCheckboxes = void 0;
const react_1 = __importStar(require("react"));
const SystemTraySetting_1 = require("../../types/SystemTraySetting");
// This component is rendered by Backbone, so it deviates from idiomatic React a bit. For
//   example, it does not receive its value as a prop.
const SystemTraySettingsCheckboxes = ({ i18n, initialValue, isSystemTraySupported, onChange, }) => {
    const [localValue, setLocalValue] = (0, react_1.useState)((0, SystemTraySetting_1.parseSystemTraySetting)(initialValue));
    if (!isSystemTraySupported) {
        return null;
    }
    const setValue = (value) => {
        setLocalValue(oldValue => {
            if (oldValue !== value) {
                onChange(value);
            }
            return value;
        });
    };
    const setMinimizeToSystemTray = (event) => {
        setValue(event.target.checked
            ? SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray
            : SystemTraySetting_1.SystemTraySetting.DoNotUseSystemTray);
    };
    const setMinimizeToAndStartInSystemTray = (event) => {
        setValue(event.target.checked
            ? SystemTraySetting_1.SystemTraySetting.MinimizeToAndStartInSystemTray
            : SystemTraySetting_1.SystemTraySetting.MinimizeToSystemTray);
    };
    const minimizesToTray = (0, SystemTraySetting_1.shouldMinimizeToSystemTray)(localValue);
    const minimizesToAndStartsInSystemTray = localValue === SystemTraySetting_1.SystemTraySetting.MinimizeToAndStartInSystemTray;
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", null,
            react_1.default.createElement("input", { checked: minimizesToTray, id: "system-tray-setting-minimize-to-system-tray", onChange: setMinimizeToSystemTray, type: "checkbox" }),
            ' ',
            react_1.default.createElement("label", { htmlFor: "system-tray-setting-minimize-to-system-tray" }, i18n('SystemTraySetting__minimize-to-system-tray'))),
        react_1.default.createElement("div", null,
            react_1.default.createElement("input", { checked: minimizesToAndStartsInSystemTray, disabled: !minimizesToTray, id: "system-tray-setting-minimize-to-and-start-in-system-tray", onChange: setMinimizeToAndStartInSystemTray, type: "checkbox" }),
            ' ',
            react_1.default.createElement("label", { htmlFor: "system-tray-setting-minimize-to-and-start-in-system-tray", style: minimizesToTray ? {} : { opacity: 0.75 } }, i18n('SystemTraySetting__minimize-to-and-start-in-system-tray')))));
};
exports.SystemTraySettingsCheckboxes = SystemTraySettingsCheckboxes;
