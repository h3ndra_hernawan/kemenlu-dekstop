"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResetSessionNotification = void 0;
const react_1 = __importDefault(require("react"));
const SystemMessage_1 = require("./SystemMessage");
const ResetSessionNotification = ({ i18n }) => (react_1.default.createElement(SystemMessage_1.SystemMessage, { contents: i18n('sessionEnded'), icon: "session-refresh" }));
exports.ResetSessionNotification = ResetSessionNotification;
