"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const VerificationNotification_1 = require("./VerificationNotification");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/VerificationNotification', module);
const contact = { title: 'Mr. Fire' };
const createProps = (overrideProps = {}) => ({
    i18n,
    type: overrideProps.type || 'markVerified',
    isLocal: (0, addon_knobs_1.boolean)('isLocal', overrideProps.isLocal !== false),
    contact: overrideProps.contact || contact,
});
story.add('Mark as Verified', () => {
    const props = createProps({ type: 'markVerified' });
    return React.createElement(VerificationNotification_1.VerificationNotification, Object.assign({}, props));
});
story.add('Mark as Not Verified', () => {
    const props = createProps({ type: 'markNotVerified' });
    return React.createElement(VerificationNotification_1.VerificationNotification, Object.assign({}, props));
});
story.add('Mark as Verified Remotely', () => {
    const props = createProps({ type: 'markVerified', isLocal: false });
    return React.createElement(VerificationNotification_1.VerificationNotification, Object.assign({}, props));
});
story.add('Mark as Not Verified Remotely', () => {
    const props = createProps({ type: 'markNotVerified', isLocal: false });
    return React.createElement(VerificationNotification_1.VerificationNotification, Object.assign({}, props));
});
story.add('Long name', () => {
    const longName = '🎆🍬🏈'.repeat(50);
    const props = createProps({
        type: 'markVerified',
        contact: { title: longName },
    });
    return React.createElement(VerificationNotification_1.VerificationNotification, Object.assign({}, props));
});
