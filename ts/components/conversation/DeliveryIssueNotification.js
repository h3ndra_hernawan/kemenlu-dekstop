"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryIssueNotification = void 0;
const react_1 = __importStar(require("react"));
const Button_1 = require("../Button");
const SystemMessage_1 = require("./SystemMessage");
const Intl_1 = require("../Intl");
const Emojify_1 = require("./Emojify");
const DeliveryIssueDialog_1 = require("./DeliveryIssueDialog");
function DeliveryIssueNotification(props) {
    const { i18n, inGroup, sender, learnMoreAboutDeliveryIssue } = props;
    const [isDialogOpen, setIsDialogOpen] = (0, react_1.useState)(false);
    const openDialog = (0, react_1.useCallback)(() => {
        setIsDialogOpen(true);
    }, [setIsDialogOpen]);
    const closeDialog = (0, react_1.useCallback)(() => {
        setIsDialogOpen(false);
    }, [setIsDialogOpen]);
    if (!sender) {
        return null;
    }
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(SystemMessage_1.SystemMessage, { contents: react_1.default.createElement(Intl_1.Intl, { id: "DeliveryIssue--notification", components: {
                    sender: react_1.default.createElement(Emojify_1.Emojify, { text: sender.firstName || sender.title }),
                }, i18n: i18n }), icon: "info", button: react_1.default.createElement(Button_1.Button, { onClick: openDialog, size: Button_1.ButtonSize.Small, variant: Button_1.ButtonVariant.SystemMessage }, i18n('DeliveryIssue--learnMore')) }),
        isDialogOpen ? (react_1.default.createElement(DeliveryIssueDialog_1.DeliveryIssueDialog, { i18n: i18n, inGroup: inGroup, learnMoreAboutDeliveryIssue: learnMoreAboutDeliveryIssue, sender: sender, onClose: closeDialog })) : null));
}
exports.DeliveryIssueNotification = DeliveryIssueNotification;
