"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Message = exports.Directions = exports.MessageStatuses = void 0;
const react_1 = __importDefault(require("react"));
const react_dom_1 = __importStar(require("react-dom"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const react_contextmenu_1 = require("react-contextmenu");
const react_popper_1 = require("react-popper");
const MessageReadStatus_1 = require("../../messages/MessageReadStatus");
const Avatar_1 = require("../Avatar");
const Spinner_1 = require("../Spinner");
const MessageBodyReadMore_1 = require("./MessageBodyReadMore");
const MessageMetadata_1 = require("./MessageMetadata");
const ImageGrid_1 = require("./ImageGrid");
const GIF_1 = require("./GIF");
const Image_1 = require("./Image");
const ContactName_1 = require("./ContactName");
const Quote_1 = require("./Quote");
const EmbeddedContact_1 = require("./EmbeddedContact");
const ReactionViewer_1 = require("./ReactionViewer");
const Emoji_1 = require("../emoji/Emoji");
const LinkPreviewDate_1 = require("./LinkPreviewDate");
const shouldUseFullSizeLinkPreviewImage_1 = require("../../linkPreviews/shouldUseFullSizeLinkPreviewImage");
const _util_1 = require("../_util");
const log = __importStar(require("../../logging/log"));
const Attachment_1 = require("../../types/Attachment");
const timer_1 = require("../../util/timer");
const isFileDangerous_1 = require("../../util/isFileDangerous");
const missingCaseError_1 = require("../../util/missingCaseError");
const refMerger_1 = require("../../util/refMerger");
const lib_1 = require("../emoji/lib");
const isEmojiOnlyText_1 = require("../../util/isEmojiOnlyText");
const getCustomColorStyle_1 = require("../../util/getCustomColorStyle");
const popperUtil_1 = require("../../util/popperUtil");
const KeyboardLayout = __importStar(require("../../services/keyboardLayout"));
const StopPropagation_1 = require("../StopPropagation");
const STICKER_SIZE = 200;
const GIF_SIZE = 300;
const SELECTED_TIMEOUT = 1000;
const THREE_HOURS = 3 * 60 * 60 * 1000;
exports.MessageStatuses = [
    'delivered',
    'error',
    'paused',
    'partial-sent',
    'read',
    'sending',
    'sent',
    'viewed',
];
exports.Directions = ['incoming', 'outgoing'];
const EXPIRATION_CHECK_MINIMUM = 2000;
const EXPIRED_DELAY = 600;
class Message extends react_1.default.PureComponent {
    constructor(props) {
        super(props);
        this.focusRef = react_1.default.createRef();
        this.audioButtonRef = react_1.default.createRef();
        this.reactionsContainerRef = react_1.default.createRef();
        this.reactionsContainerRefMerger = (0, refMerger_1.createRefMerger)();
        this.captureMenuTrigger = (triggerRef) => {
            this.menuTriggerRef = triggerRef;
        };
        this.showMenu = (event) => {
            if (this.menuTriggerRef) {
                this.menuTriggerRef.handleContextClick(event);
            }
        };
        this.showContextMenu = (event) => {
            const selection = window.getSelection();
            if (selection && !selection.isCollapsed) {
                return;
            }
            if (event.target instanceof HTMLAnchorElement) {
                return;
            }
            this.showMenu(event);
        };
        this.handleImageError = () => {
            const { id } = this.props;
            log.info(`Message ${id}: Image failed to load; failing over to placeholder`);
            this.setState({
                imageBroken: true,
            });
        };
        this.handleFocus = () => {
            const { interactionMode } = this.props;
            if (interactionMode === 'keyboard') {
                this.setSelected();
            }
        };
        this.setSelected = () => {
            const { id, conversationId, selectMessage } = this.props;
            if (selectMessage) {
                selectMessage(id, conversationId);
            }
        };
        this.setFocus = () => {
            const container = this.focusRef.current;
            if (container && !container.contains(document.activeElement)) {
                container.focus();
            }
        };
        this.toggleReactionViewer = (onlyRemove = false) => {
            this.setState(({ reactionViewerRoot }) => {
                if (reactionViewerRoot) {
                    document.body.removeChild(reactionViewerRoot);
                    document.body.removeEventListener('click', this.handleClickOutsideReactionViewer, true);
                    return { reactionViewerRoot: null };
                }
                if (!onlyRemove) {
                    const root = document.createElement('div');
                    document.body.appendChild(root);
                    document.body.addEventListener('click', this.handleClickOutsideReactionViewer, true);
                    return {
                        reactionViewerRoot: root,
                    };
                }
                return { reactionViewerRoot: null };
            });
        };
        this.toggleReactionPicker = (onlyRemove = false) => {
            this.setState(({ reactionPickerRoot }) => {
                if (reactionPickerRoot) {
                    document.body.removeChild(reactionPickerRoot);
                    document.body.removeEventListener('click', this.handleClickOutsideReactionPicker, true);
                    return { reactionPickerRoot: null };
                }
                if (!onlyRemove) {
                    const root = document.createElement('div');
                    document.body.appendChild(root);
                    document.body.addEventListener('click', this.handleClickOutsideReactionPicker, true);
                    return {
                        reactionPickerRoot: root,
                    };
                }
                return { reactionPickerRoot: null };
            });
        };
        this.handleClickOutsideReactionViewer = (e) => {
            const { reactionViewerRoot } = this.state;
            const { current: reactionsContainer } = this.reactionsContainerRef;
            if (reactionViewerRoot && reactionsContainer) {
                if (!reactionViewerRoot.contains(e.target) &&
                    !reactionsContainer.contains(e.target)) {
                    this.toggleReactionViewer(true);
                }
            }
        };
        this.handleClickOutsideReactionPicker = (e) => {
            const { reactionPickerRoot } = this.state;
            if (reactionPickerRoot) {
                if (!reactionPickerRoot.contains(e.target)) {
                    this.toggleReactionPicker(true);
                }
            }
        };
        this.handleOpen = (event) => {
            const { attachments, contact, displayTapToViewMessage, direction, id, isTapToView, isTapToViewExpired, kickOffAttachmentDownload, openConversation, showContactDetail, showVisualAttachment, showExpiredIncomingTapToViewToast, showExpiredOutgoingTapToViewToast, } = this.props;
            const { imageBroken } = this.state;
            const isAttachmentPending = this.isAttachmentPending();
            if (isTapToView) {
                if (isAttachmentPending) {
                    log.info('<Message> handleOpen: tap-to-view attachment is pending; not showing the lightbox');
                    return;
                }
                if (attachments && (0, Attachment_1.hasNotDownloaded)(attachments[0])) {
                    event.preventDefault();
                    event.stopPropagation();
                    kickOffAttachmentDownload({
                        attachment: attachments[0],
                        messageId: id,
                    });
                    return;
                }
                if (isTapToViewExpired) {
                    const action = direction === 'outgoing'
                        ? showExpiredOutgoingTapToViewToast
                        : showExpiredIncomingTapToViewToast;
                    action();
                }
                else {
                    event.preventDefault();
                    event.stopPropagation();
                    displayTapToViewMessage(id);
                }
                return;
            }
            if (!imageBroken &&
                attachments &&
                attachments.length > 0 &&
                !isAttachmentPending &&
                ((0, Attachment_1.isImage)(attachments) || (0, Attachment_1.isVideo)(attachments)) &&
                (0, Attachment_1.hasNotDownloaded)(attachments[0])) {
                event.preventDefault();
                event.stopPropagation();
                const attachment = attachments[0];
                kickOffAttachmentDownload({ attachment, messageId: id });
                return;
            }
            if (!imageBroken &&
                attachments &&
                attachments.length > 0 &&
                !isAttachmentPending &&
                (0, Attachment_1.canDisplayImage)(attachments) &&
                (((0, Attachment_1.isImage)(attachments) && (0, Attachment_1.hasImage)(attachments)) ||
                    ((0, Attachment_1.isVideo)(attachments) && (0, Attachment_1.hasVideoScreenshot)(attachments)))) {
                event.preventDefault();
                event.stopPropagation();
                const attachment = attachments[0];
                showVisualAttachment({ attachment, messageId: id });
                return;
            }
            if (attachments &&
                attachments.length === 1 &&
                !isAttachmentPending &&
                !(0, Attachment_1.isAudio)(attachments)) {
                event.preventDefault();
                event.stopPropagation();
                this.openGenericAttachment();
                return;
            }
            if (!isAttachmentPending &&
                (0, Attachment_1.isAudio)(attachments) &&
                this.audioButtonRef &&
                this.audioButtonRef.current) {
                event.preventDefault();
                event.stopPropagation();
                this.audioButtonRef.current.click();
            }
            if (contact && contact.firstNumber && contact.isNumberOnSignal) {
                openConversation(contact.firstNumber);
                event.preventDefault();
                event.stopPropagation();
            }
            if (contact) {
                showContactDetail({ contact, signalAccount: contact.firstNumber });
                event.preventDefault();
                event.stopPropagation();
            }
        };
        this.openGenericAttachment = (event) => {
            const { id, attachments, downloadAttachment, timestamp, kickOffAttachmentDownload, } = this.props;
            if (event) {
                event.preventDefault();
                event.stopPropagation();
            }
            if (!attachments || attachments.length !== 1) {
                return;
            }
            const attachment = attachments[0];
            if ((0, Attachment_1.hasNotDownloaded)(attachment)) {
                kickOffAttachmentDownload({
                    attachment,
                    messageId: id,
                });
                return;
            }
            const { fileName } = attachment;
            const isDangerous = (0, isFileDangerous_1.isFileDangerous)(fileName || '');
            downloadAttachment({
                isDangerous,
                attachment,
                timestamp,
            });
        };
        this.handleKeyDown = (event) => {
            // Do not allow reactions to error messages
            const { canReply } = this.props;
            const key = KeyboardLayout.lookup(event.nativeEvent);
            if ((key === 'E' || key === 'e') &&
                (event.metaKey || event.ctrlKey) &&
                event.shiftKey &&
                canReply) {
                this.toggleReactionPicker();
            }
            if (event.key !== 'Enter' && event.key !== 'Space') {
                return;
            }
            this.handleOpen(event);
        };
        this.handleClick = (event) => {
            // We don't want clicks on body text to result in the 'default action' for the message
            const { text } = this.props;
            if (text && text.length > 0) {
                return;
            }
            this.handleOpen(event);
        };
        this.state = {
            expiring: false,
            expired: false,
            imageBroken: false,
            isSelected: props.isSelected,
            prevSelectedCounter: props.isSelectedCounter,
            reactionViewerRoot: null,
            reactionPickerRoot: null,
            canDeleteForEveryone: props.canDeleteForEveryone,
        };
    }
    static getDerivedStateFromProps(props, state) {
        const newState = Object.assign(Object.assign({}, state), { canDeleteForEveryone: props.canDeleteForEveryone && state.canDeleteForEveryone });
        if (!props.isSelected) {
            return Object.assign(Object.assign({}, newState), { isSelected: false, prevSelectedCounter: 0 });
        }
        if (props.isSelected &&
            props.isSelectedCounter !== state.prevSelectedCounter) {
            return Object.assign(Object.assign({}, newState), { isSelected: props.isSelected, prevSelectedCounter: props.isSelectedCounter });
        }
        return newState;
    }
    hasReactions() {
        const { reactions } = this.props;
        return Boolean(reactions && reactions.length);
    }
    componentDidMount() {
        this.startSelectedTimer();
        this.startDeleteForEveryoneTimer();
        const { isSelected } = this.props;
        if (isSelected) {
            this.setFocus();
        }
        const { expirationLength } = this.props;
        if (expirationLength) {
            const increment = (0, timer_1.getIncrement)(expirationLength);
            const checkFrequency = Math.max(EXPIRATION_CHECK_MINIMUM, increment);
            this.checkExpired();
            this.expirationCheckInterval = setInterval(() => {
                this.checkExpired();
            }, checkFrequency);
        }
        const { contact, checkForAccount } = this.props;
        if (contact && contact.firstNumber && !contact.isNumberOnSignal) {
            checkForAccount(contact.firstNumber);
        }
    }
    componentWillUnmount() {
        if (this.selectedTimeout) {
            clearTimeout(this.selectedTimeout);
        }
        if (this.expirationCheckInterval) {
            clearInterval(this.expirationCheckInterval);
        }
        if (this.expiredTimeout) {
            clearTimeout(this.expiredTimeout);
        }
        if (this.deleteForEveryoneTimeout) {
            clearTimeout(this.deleteForEveryoneTimeout);
        }
        this.toggleReactionViewer(true);
        this.toggleReactionPicker(true);
    }
    componentDidUpdate(prevProps) {
        var _a;
        const { canDeleteForEveryone, isSelected, status, timestamp } = this.props;
        this.startSelectedTimer();
        if (!prevProps.isSelected && isSelected) {
            this.setFocus();
        }
        this.checkExpired();
        this.checkForHeightChange(prevProps);
        if (canDeleteForEveryone !== prevProps.canDeleteForEveryone) {
            this.startDeleteForEveryoneTimer();
        }
        if (prevProps.status === 'sending' &&
            (status === 'sent' ||
                status === 'delivered' ||
                status === 'read' ||
                status === 'viewed')) {
            const delta = Date.now() - timestamp;
            (_a = window.CI) === null || _a === void 0 ? void 0 : _a.handleEvent('message:send-complete', {
                timestamp,
                delta,
            });
            log.info(`Message.tsx: Rendered 'send complete' for message ${timestamp}; took ${delta}ms`);
        }
    }
    checkForHeightChange(prevProps) {
        const { contact, onHeightChange } = this.props;
        const willRenderSendMessageButton = Boolean(contact && contact.firstNumber && contact.isNumberOnSignal);
        const { contact: previousContact } = prevProps;
        const previouslyRenderedSendMessageButton = Boolean(previousContact &&
            previousContact.firstNumber &&
            previousContact.isNumberOnSignal);
        if (willRenderSendMessageButton !== previouslyRenderedSendMessageButton) {
            onHeightChange();
        }
    }
    startSelectedTimer() {
        const { clearSelectedMessage, interactionMode } = this.props;
        const { isSelected } = this.state;
        if (interactionMode === 'keyboard' || !isSelected) {
            return;
        }
        if (!this.selectedTimeout) {
            this.selectedTimeout = setTimeout(() => {
                this.selectedTimeout = undefined;
                this.setState({ isSelected: false });
                clearSelectedMessage();
            }, SELECTED_TIMEOUT);
        }
    }
    startDeleteForEveryoneTimer() {
        if (this.deleteForEveryoneTimeout) {
            clearTimeout(this.deleteForEveryoneTimeout);
        }
        const { canDeleteForEveryone } = this.props;
        if (!canDeleteForEveryone) {
            return;
        }
        const { timestamp } = this.props;
        const timeToDeletion = timestamp - Date.now() + THREE_HOURS;
        if (timeToDeletion <= 0) {
            this.setState({ canDeleteForEveryone: false });
        }
        else {
            this.deleteForEveryoneTimeout = setTimeout(() => {
                this.setState({ canDeleteForEveryone: false });
            }, timeToDeletion);
        }
    }
    checkExpired() {
        const now = Date.now();
        const { expirationTimestamp, expirationLength } = this.props;
        if (!expirationTimestamp || !expirationLength) {
            return;
        }
        if (this.expiredTimeout) {
            return;
        }
        if (now >= expirationTimestamp) {
            this.setState({
                expiring: true,
            });
            const setExpired = () => {
                this.setState({
                    expired: true,
                });
            };
            this.expiredTimeout = setTimeout(setExpired, EXPIRED_DELAY);
        }
    }
    areLinksEnabled() {
        const { isMessageRequestAccepted, isBlocked } = this.props;
        return isMessageRequestAccepted && !isBlocked;
    }
    canRenderStickerLikeEmoji() {
        const { text, quote, attachments, previews } = this.props;
        return Boolean(text &&
            (0, isEmojiOnlyText_1.isEmojiOnlyText)(text) &&
            (0, lib_1.getEmojiCount)(text) < 6 &&
            !quote &&
            (!attachments || !attachments.length) &&
            (!previews || !previews.length));
    }
    renderMetadata() {
        const { attachments, collapseMetadata, deletedForEveryone, direction, expirationLength, expirationTimestamp, isSticker, isTapToViewExpired, status, i18n, text, textPending, timestamp, id, showMessageDetail, } = this.props;
        if (collapseMetadata) {
            return null;
        }
        // The message audio component renders its own metadata because it positions the
        //   metadata in line with some of its own.
        if ((0, Attachment_1.isAudio)(attachments) && !text) {
            return null;
        }
        const isStickerLike = isSticker || this.canRenderStickerLikeEmoji();
        return (react_1.default.createElement(MessageMetadata_1.MessageMetadata, { deletedForEveryone: deletedForEveryone, direction: direction, expirationLength: expirationLength, expirationTimestamp: expirationTimestamp, hasText: Boolean(text), i18n: i18n, id: id, isShowingImage: this.isShowingImage(), isSticker: isStickerLike, isTapToViewExpired: isTapToViewExpired, showMessageDetail: showMessageDetail, status: status, textPending: textPending, timestamp: timestamp }));
    }
    renderAuthor() {
        const { author, collapseMetadata, contactNameColor, conversationType, direction, isSticker, isTapToView, isTapToViewExpired, } = this.props;
        if (collapseMetadata) {
            return null;
        }
        if (direction !== 'incoming' ||
            conversationType !== 'group' ||
            !author.title) {
            return null;
        }
        const withTapToViewExpired = isTapToView && isTapToViewExpired;
        const stickerSuffix = isSticker ? '_with_sticker' : '';
        const tapToViewSuffix = withTapToViewExpired
            ? '--with-tap-to-view-expired'
            : '';
        const moduleName = `module-message__author${stickerSuffix}${tapToViewSuffix}`;
        return (react_1.default.createElement("div", { className: moduleName },
            react_1.default.createElement(ContactName_1.ContactName, { contactNameColor: contactNameColor, title: author.title, module: moduleName })));
    }
    renderAttachment() {
        const { attachments, collapseMetadata, conversationType, direction, expirationLength, expirationTimestamp, i18n, id, isSticker, kickOffAttachmentDownload, markAttachmentAsCorrupted, markViewed, quote, readStatus, reducedMotion, renderAudioAttachment, renderingContext, showMessageDetail, showVisualAttachment, status, text, textPending, theme, timestamp, } = this.props;
        const { imageBroken } = this.state;
        if (!attachments || !attachments[0]) {
            return null;
        }
        const firstAttachment = attachments[0];
        // For attachments which aren't full-frame
        const withContentBelow = Boolean(text);
        const withContentAbove = Boolean(quote) ||
            (conversationType === 'group' && direction === 'incoming');
        const displayImage = (0, Attachment_1.canDisplayImage)(attachments);
        if (displayImage && !imageBroken) {
            const prefix = isSticker ? 'sticker' : 'attachment';
            const containerClassName = (0, classnames_1.default)(`module-message__${prefix}-container`, withContentAbove
                ? `module-message__${prefix}-container--with-content-above`
                : null, withContentBelow
                ? 'module-message__attachment-container--with-content-below'
                : null, isSticker && !collapseMetadata
                ? 'module-message__sticker-container--with-content-below'
                : null);
            if ((0, Attachment_1.isGIF)(attachments)) {
                return (react_1.default.createElement("div", { className: containerClassName },
                    react_1.default.createElement(GIF_1.GIF, { attachment: firstAttachment, size: GIF_SIZE, theme: theme, i18n: i18n, tabIndex: 0, reducedMotion: reducedMotion, onError: this.handleImageError, showVisualAttachment: () => {
                            showVisualAttachment({
                                attachment: firstAttachment,
                                messageId: id,
                            });
                        }, kickOffAttachmentDownload: () => {
                            kickOffAttachmentDownload({
                                attachment: firstAttachment,
                                messageId: id,
                            });
                        } })));
            }
            if ((0, Attachment_1.isImage)(attachments) || (0, Attachment_1.isVideo)(attachments)) {
                const bottomOverlay = !isSticker && !collapseMetadata;
                // We only want users to tab into this if there's more than one
                const tabIndex = attachments.length > 1 ? 0 : -1;
                return (react_1.default.createElement("div", { className: containerClassName },
                    react_1.default.createElement(ImageGrid_1.ImageGrid, { attachments: attachments, withContentAbove: isSticker || withContentAbove, withContentBelow: isSticker || withContentBelow, isSticker: isSticker, stickerSize: STICKER_SIZE, bottomOverlay: bottomOverlay, i18n: i18n, theme: theme, onError: this.handleImageError, tabIndex: tabIndex, onClick: attachment => {
                            if ((0, Attachment_1.hasNotDownloaded)(attachment)) {
                                kickOffAttachmentDownload({ attachment, messageId: id });
                            }
                            else {
                                showVisualAttachment({ attachment, messageId: id });
                            }
                        } })));
            }
        }
        if ((0, Attachment_1.isAudio)(attachments)) {
            let played;
            switch (direction) {
                case 'outgoing':
                    played = status === 'viewed';
                    break;
                case 'incoming':
                    played = readStatus === MessageReadStatus_1.ReadStatus.Viewed;
                    break;
                default:
                    log.error((0, missingCaseError_1.missingCaseError)(direction));
                    played = false;
                    break;
            }
            return renderAudioAttachment({
                i18n,
                buttonRef: this.audioButtonRef,
                renderingContext,
                theme,
                attachment: firstAttachment,
                withContentAbove,
                withContentBelow,
                direction,
                expirationLength,
                expirationTimestamp,
                id,
                played,
                showMessageDetail,
                status,
                textPending,
                timestamp,
                kickOffAttachmentDownload() {
                    kickOffAttachmentDownload({
                        attachment: firstAttachment,
                        messageId: id,
                    });
                },
                onCorrupted() {
                    markAttachmentAsCorrupted({
                        attachment: firstAttachment,
                        messageId: id,
                    });
                },
                onFirstPlayed() {
                    markViewed(id);
                },
            });
        }
        const { pending, fileName, fileSize, contentType } = firstAttachment;
        const extension = (0, Attachment_1.getExtensionForDisplay)({ contentType, fileName });
        const isDangerous = (0, isFileDangerous_1.isFileDangerous)(fileName || '');
        return (react_1.default.createElement("button", { type: "button", className: (0, classnames_1.default)('module-message__generic-attachment', withContentBelow
                ? 'module-message__generic-attachment--with-content-below'
                : null, withContentAbove
                ? 'module-message__generic-attachment--with-content-above'
                : null, !firstAttachment.url
                ? 'module-message__generic-attachment--not-active'
                : null), 
            // There's only ever one of these, so we don't want users to tab into it
            tabIndex: -1, onClick: this.openGenericAttachment },
            pending ? (react_1.default.createElement("div", { className: "module-message__generic-attachment__spinner-container" },
                react_1.default.createElement(Spinner_1.Spinner, { svgSize: "small", size: "24px", direction: direction }))) : (react_1.default.createElement("div", { className: "module-message__generic-attachment__icon-container" },
                react_1.default.createElement("div", { className: "module-message__generic-attachment__icon" }, extension ? (react_1.default.createElement("div", { className: "module-message__generic-attachment__icon__extension" }, extension)) : null),
                isDangerous ? (react_1.default.createElement("div", { className: "module-message__generic-attachment__icon-dangerous-container" },
                    react_1.default.createElement("div", { className: "module-message__generic-attachment__icon-dangerous" }))) : null)),
            react_1.default.createElement("div", { className: "module-message__generic-attachment__text" },
                react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__generic-attachment__file-name', `module-message__generic-attachment__file-name--${direction}`) }, fileName),
                react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__generic-attachment__file-size', `module-message__generic-attachment__file-size--${direction}`) }, fileSize))));
    }
    renderPreview() {
        const { id, attachments, conversationType, direction, i18n, openLink, previews, quote, theme, kickOffAttachmentDownload, } = this.props;
        // Attachments take precedence over Link Previews
        if (attachments && attachments.length) {
            return null;
        }
        if (!previews || previews.length < 1) {
            return null;
        }
        const first = previews[0];
        if (!first) {
            return null;
        }
        const withContentAbove = Boolean(quote) ||
            (conversationType === 'group' && direction === 'incoming');
        const previewHasImage = (0, Attachment_1.isImageAttachment)(first.image);
        const isFullSizeImage = (0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(first);
        const linkPreviewDate = first.date || null;
        const isClickable = this.areLinksEnabled();
        const className = (0, classnames_1.default)('module-message__link-preview', `module-message__link-preview--${direction}`, {
            'module-message__link-preview--with-content-above': withContentAbove,
            'module-message__link-preview--nonclickable': !isClickable,
        });
        const onPreviewImageClick = () => {
            if (first.image && (0, Attachment_1.hasNotDownloaded)(first.image)) {
                kickOffAttachmentDownload({
                    attachment: first.image,
                    messageId: id,
                });
                return;
            }
            openLink(first.url);
        };
        const contents = (react_1.default.createElement(react_1.default.Fragment, null,
            first.image && previewHasImage && isFullSizeImage ? (react_1.default.createElement(ImageGrid_1.ImageGrid, { attachments: [first.image], withContentAbove: withContentAbove, withContentBelow: true, onError: this.handleImageError, i18n: i18n, theme: theme, onClick: onPreviewImageClick })) : null,
            react_1.default.createElement("div", { className: "module-message__link-preview__content" },
                first.image && previewHasImage && !isFullSizeImage ? (react_1.default.createElement("div", { className: "module-message__link-preview__icon_container" },
                    react_1.default.createElement(Image_1.Image, { smallCurveTopLeft: !withContentAbove, noBorder: true, noBackground: true, softCorners: true, alt: i18n('previewThumbnail', [first.domain]), height: 72, width: 72, url: first.image.url, attachment: first.image, onError: this.handleImageError, i18n: i18n, onClick: onPreviewImageClick }))) : null,
                react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__link-preview__text', previewHasImage && !isFullSizeImage
                        ? 'module-message__link-preview__text--with-icon'
                        : null) },
                    react_1.default.createElement("div", { className: "module-message__link-preview__title" }, first.title),
                    first.description && (react_1.default.createElement("div", { className: "module-message__link-preview__description" }, (0, lodash_1.unescape)(first.description))),
                    react_1.default.createElement("div", { className: "module-message__link-preview__footer" },
                        react_1.default.createElement("div", { className: "module-message__link-preview__location" }, first.domain),
                        react_1.default.createElement(LinkPreviewDate_1.LinkPreviewDate, { date: linkPreviewDate, className: "module-message__link-preview__date" }))))));
        return isClickable ? (react_1.default.createElement("div", { role: "link", tabIndex: 0, className: className, onKeyDown: (event) => {
                if (event.key === 'Enter' || event.key === 'Space') {
                    event.stopPropagation();
                    event.preventDefault();
                    openLink(first.url);
                }
            }, onClick: (event) => {
                event.stopPropagation();
                event.preventDefault();
                openLink(first.url);
            } }, contents)) : (react_1.default.createElement("div", { className: className }, contents));
    }
    renderQuote() {
        const { conversationColor, customColor, direction, disableScroll, doubleCheckMissingQuoteReference, i18n, id, quote, scrollToQuotedMessage, } = this.props;
        if (!quote) {
            return null;
        }
        const { isViewOnce, referencedMessageNotFound } = quote;
        const clickHandler = disableScroll
            ? undefined
            : () => {
                scrollToQuotedMessage({
                    authorId: quote.authorId,
                    sentAt: quote.sentAt,
                });
            };
        return (react_1.default.createElement(Quote_1.Quote, { i18n: i18n, onClick: clickHandler, text: quote.text, rawAttachment: quote.rawAttachment, isIncoming: direction === 'incoming', authorTitle: quote.authorTitle, bodyRanges: quote.bodyRanges, conversationColor: conversationColor, customColor: customColor, isViewOnce: isViewOnce, referencedMessageNotFound: referencedMessageNotFound, isFromMe: quote.isFromMe, doubleCheckMissingQuoteReference: () => doubleCheckMissingQuoteReference(id) }));
    }
    renderEmbeddedContact() {
        const { collapseMetadata, contact, conversationType, direction, i18n, showContactDetail, text, } = this.props;
        if (!contact) {
            return null;
        }
        const withCaption = Boolean(text);
        const withContentAbove = conversationType === 'group' && direction === 'incoming';
        const withContentBelow = withCaption || !collapseMetadata;
        const otherContent = (contact && contact.firstNumber && contact.isNumberOnSignal) ||
            withCaption;
        const tabIndex = otherContent ? 0 : -1;
        return (react_1.default.createElement(EmbeddedContact_1.EmbeddedContact, { contact: contact, isIncoming: direction === 'incoming', i18n: i18n, onClick: () => {
                showContactDetail({ contact, signalAccount: contact.firstNumber });
            }, withContentAbove: withContentAbove, withContentBelow: withContentBelow, tabIndex: tabIndex }));
    }
    renderSendMessageButton() {
        const { contact, openConversation, i18n } = this.props;
        if (!contact) {
            return null;
        }
        const { firstNumber, isNumberOnSignal } = contact;
        if (!firstNumber || !isNumberOnSignal) {
            return null;
        }
        return (react_1.default.createElement("button", { type: "button", onClick: () => openConversation(firstNumber), className: "module-message__send-message-button" }, i18n('sendMessageToContact')));
    }
    hasAvatar() {
        const { collapseMetadata, conversationType, direction } = this.props;
        return Boolean(!collapseMetadata &&
            conversationType === 'group' &&
            direction !== 'outgoing');
    }
    renderAvatar() {
        const { author, authorBadge, i18n, showContactModal, theme } = this.props;
        if (!this.hasAvatar()) {
            return undefined;
        }
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__author-avatar-container', {
                'module-message__author-avatar-container--with-reactions': this.hasReactions(),
            }) },
            react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: author.acceptedMessageRequest, avatarPath: author.avatarPath, badge: authorBadge, color: author.color, conversationType: "direct", i18n: i18n, isMe: author.isMe, name: author.name, onClick: event => {
                    event.stopPropagation();
                    event.preventDefault();
                    showContactModal(author.id);
                }, phoneNumber: author.phoneNumber, profileName: author.profileName, sharedGroupNames: author.sharedGroupNames, size: 28, theme: theme, title: author.title, unblurredAvatarPath: author.unblurredAvatarPath })));
    }
    renderText() {
        const { bodyRanges, deletedForEveryone, direction, displayLimit, i18n, id, messageExpanded, onHeightChange, openConversation, status, text, textPending, } = this.props;
        // eslint-disable-next-line no-nested-ternary
        const contents = deletedForEveryone
            ? i18n('message--deletedForEveryone')
            : direction === 'incoming' && status === 'error'
                ? i18n('incomingError')
                : text;
        if (!contents) {
            return null;
        }
        return (react_1.default.createElement("div", { dir: "auto", className: (0, classnames_1.default)('module-message__text', `module-message__text--${direction}`, status === 'error' && direction === 'incoming'
                ? 'module-message__text--error'
                : null) },
            react_1.default.createElement(MessageBodyReadMore_1.MessageBodyReadMore, { bodyRanges: bodyRanges, disableLinks: !this.areLinksEnabled(), direction: direction, displayLimit: displayLimit, i18n: i18n, id: id, messageExpanded: messageExpanded, openConversation: openConversation, onHeightChange: onHeightChange, text: contents || '', textPending: textPending })));
    }
    renderError(isCorrectSide) {
        const { status, direction } = this.props;
        if (!isCorrectSide) {
            return null;
        }
        if (status !== 'paused' &&
            status !== 'error' &&
            status !== 'partial-sent') {
            return null;
        }
        return (react_1.default.createElement("div", { className: "module-message__error-container" },
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__error', `module-message__error--${direction}`, `module-message__error--${status}`) })));
    }
    renderMenu(isCorrectSide, triggerId) {
        const { attachments, canDownload, canReply, direction, disableMenu, i18n, id, isSticker, isTapToView, reactToMessage, renderEmojiPicker, renderReactionPicker, replyToMessage, selectedReaction, } = this.props;
        if (!isCorrectSide || disableMenu) {
            return null;
        }
        const { reactionPickerRoot } = this.state;
        const multipleAttachments = attachments && attachments.length > 1;
        const firstAttachment = attachments && attachments[0];
        const downloadButton = !isSticker &&
            !multipleAttachments &&
            !isTapToView &&
            firstAttachment &&
            !firstAttachment.pending ? (
        // This a menu meant for mouse use only
        // eslint-disable-next-line max-len
        // eslint-disable-next-line jsx-a11y/interactive-supports-focus, jsx-a11y/click-events-have-key-events
        react_1.default.createElement("div", { onClick: this.openGenericAttachment, role: "button", "aria-label": i18n('downloadAttachment'), className: (0, classnames_1.default)('module-message__buttons__download', `module-message__buttons__download--${direction}`) })) : null;
        const reactButton = (react_1.default.createElement(react_popper_1.Reference, null, ({ ref: popperRef }) => {
            // Only attach the popper reference to the reaction button if it is
            //   visible (it is hidden when the timeline is narrow)
            const maybePopperRef = this.shouldShowAdditionalMenuButtons()
                ? popperRef
                : undefined;
            return (
            // This a menu meant for mouse use only
            // eslint-disable-next-line max-len
            // eslint-disable-next-line jsx-a11y/interactive-supports-focus, jsx-a11y/click-events-have-key-events
            react_1.default.createElement("div", { ref: maybePopperRef, onClick: (event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    this.toggleReactionPicker();
                }, role: "button", className: "module-message__buttons__react", "aria-label": i18n('reactToMessage') }));
        }));
        const replyButton = (
        // This a menu meant for mouse use only
        // eslint-disable-next-line max-len
        // eslint-disable-next-line jsx-a11y/interactive-supports-focus, jsx-a11y/click-events-have-key-events
        react_1.default.createElement("div", { onClick: (event) => {
                event.stopPropagation();
                event.preventDefault();
                replyToMessage(id);
            }, 
            // This a menu meant for mouse use only
            role: "button", "aria-label": i18n('replyToMessage'), className: (0, classnames_1.default)('module-message__buttons__reply', `module-message__buttons__download--${direction}`) }));
        // This a menu meant for mouse use only
        /* eslint-disable jsx-a11y/interactive-supports-focus */
        /* eslint-disable jsx-a11y/click-events-have-key-events */
        const menuButton = (react_1.default.createElement(react_popper_1.Reference, null, ({ ref: popperRef }) => {
            // Only attach the popper reference to the collapsed menu button if the reaction
            //   button is not visible (it is hidden when the timeline is narrow)
            const maybePopperRef = !this.shouldShowAdditionalMenuButtons()
                ? popperRef
                : undefined;
            return (react_1.default.createElement(StopPropagation_1.StopPropagation, { className: "module-message__buttons__menu--container" },
                react_1.default.createElement(react_contextmenu_1.ContextMenuTrigger, { id: triggerId, 
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    ref: this.captureMenuTrigger },
                    react_1.default.createElement("div", { ref: maybePopperRef, role: "button", onClick: this.showMenu, "aria-label": i18n('messageContextMenuButton'), className: (0, classnames_1.default)('module-message__buttons__menu', `module-message__buttons__download--${direction}`) }))));
        }));
        /* eslint-enable jsx-a11y/interactive-supports-focus */
        /* eslint-enable jsx-a11y/click-events-have-key-events */
        return (react_1.default.createElement(react_popper_1.Manager, null,
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__buttons', `module-message__buttons--${direction}`) },
                this.shouldShowAdditionalMenuButtons() && (react_1.default.createElement(react_1.default.Fragment, null,
                    canReply ? reactButton : null,
                    canDownload ? downloadButton : null,
                    canReply ? replyButton : null)),
                menuButton),
            reactionPickerRoot &&
                (0, react_dom_1.createPortal)(react_1.default.createElement(StopPropagation_1.StopPropagation, null,
                    react_1.default.createElement(react_popper_1.Popper, { placement: "top", modifiers: [
                            (0, popperUtil_1.offsetDistanceModifier)(4),
                            this.popperPreventOverflowModifier(),
                        ] }, ({ ref, style }) => renderReactionPicker({
                        ref,
                        style,
                        selected: selectedReaction,
                        onClose: this.toggleReactionPicker,
                        onPick: emoji => {
                            this.toggleReactionPicker(true);
                            reactToMessage(id, {
                                emoji,
                                remove: emoji === selectedReaction,
                            });
                        },
                        renderEmojiPicker,
                    }))), reactionPickerRoot)));
    }
    renderContextMenu(triggerId) {
        const { attachments, canDownload, canReply, deleteMessage, deleteMessageForEveryone, deletedForEveryone, direction, i18n, id, isSticker, isTapToView, replyToMessage, retrySend, showForwardMessageModal, showMessageDetail, status, } = this.props;
        const canForward = !isTapToView && !deletedForEveryone;
        const { canDeleteForEveryone } = this.state;
        const showRetry = (status === 'paused' ||
            status === 'error' ||
            status === 'partial-sent') &&
            direction === 'outgoing';
        const multipleAttachments = attachments && attachments.length > 1;
        const menu = (react_1.default.createElement(react_contextmenu_1.ContextMenu, { id: triggerId },
            canDownload &&
                !this.shouldShowAdditionalMenuButtons() &&
                !isSticker &&
                !multipleAttachments &&
                !isTapToView &&
                attachments &&
                attachments[0] ? (react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                    className: 'module-message__context--icon module-message__context__download',
                }, onClick: this.openGenericAttachment }, i18n('downloadAttachment'))) : null,
            canReply && !this.shouldShowAdditionalMenuButtons() ? (react_1.default.createElement(react_1.default.Fragment, null,
                react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                        className: 'module-message__context--icon module-message__context__reply',
                    }, onClick: (event) => {
                        event.stopPropagation();
                        event.preventDefault();
                        replyToMessage(id);
                    } }, i18n('replyToMessage')),
                react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                        className: 'module-message__context--icon module-message__context__react',
                    }, onClick: (event) => {
                        event.stopPropagation();
                        event.preventDefault();
                        this.toggleReactionPicker();
                    } }, i18n('reactToMessage')))) : null,
            react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                    className: 'module-message__context--icon module-message__context__more-info',
                }, onClick: (event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    showMessageDetail(id);
                } }, i18n('moreInfo')),
            showRetry ? (react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                    className: 'module-message__context--icon module-message__context__retry-send',
                }, onClick: (event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    retrySend(id);
                } }, i18n('retrySend'))) : null,
            canForward ? (react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                    className: 'module-message__context--icon module-message__context__forward-message',
                }, onClick: (event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    showForwardMessageModal(id);
                } }, i18n('forwardMessage'))) : null,
            react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                    className: 'module-message__context--icon module-message__context__delete-message',
                }, onClick: (event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    deleteMessage(id);
                } }, i18n('deleteMessage')),
            canDeleteForEveryone ? (react_1.default.createElement(react_contextmenu_1.MenuItem, { attributes: {
                    className: 'module-message__context--icon module-message__context__delete-message-for-everyone',
                }, onClick: (event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    deleteMessageForEveryone(id);
                } }, i18n('deleteMessageForEveryone'))) : null));
        return react_dom_1.default.createPortal(menu, document.body);
    }
    shouldShowAdditionalMenuButtons() {
        const { containerWidthBreakpoint } = this.props;
        return containerWidthBreakpoint !== _util_1.WidthBreakpoint.Narrow;
    }
    getWidth() {
        const { attachments, isSticker, previews } = this.props;
        if (attachments && attachments.length) {
            if ((0, Attachment_1.isGIF)(attachments)) {
                // Message container border
                return GIF_SIZE + 2;
            }
            if (isSticker) {
                // Padding is 8px, on both sides, plus two for 1px border
                return STICKER_SIZE + 8 * 2 + 2;
            }
            const dimensions = (0, Attachment_1.getGridDimensions)(attachments);
            if (dimensions) {
                // Add two for 1px border
                return dimensions.width + 2;
            }
        }
        const firstLinkPreview = (previews || [])[0];
        if (firstLinkPreview &&
            firstLinkPreview.image &&
            (0, shouldUseFullSizeLinkPreviewImage_1.shouldUseFullSizeLinkPreviewImage)(firstLinkPreview)) {
            const dimensions = (0, Attachment_1.getImageDimensions)(firstLinkPreview.image);
            if (dimensions) {
                // Add two for 1px border
                return dimensions.width + 2;
            }
        }
        return undefined;
    }
    isShowingImage() {
        const { isTapToView, attachments, previews } = this.props;
        const { imageBroken } = this.state;
        if (imageBroken || isTapToView) {
            return false;
        }
        if (attachments && attachments.length) {
            const displayImage = (0, Attachment_1.canDisplayImage)(attachments);
            return displayImage && ((0, Attachment_1.isImage)(attachments) || (0, Attachment_1.isVideo)(attachments));
        }
        if (previews && previews.length) {
            const first = previews[0];
            const { image } = first;
            return (0, Attachment_1.isImageAttachment)(image);
        }
        return false;
    }
    isAttachmentPending() {
        const { attachments } = this.props;
        if (!attachments || attachments.length < 1) {
            return false;
        }
        const first = attachments[0];
        return Boolean(first.pending);
    }
    renderTapToViewIcon() {
        const { direction, isTapToViewExpired } = this.props;
        const isDownloadPending = this.isAttachmentPending();
        return !isTapToViewExpired && isDownloadPending ? (react_1.default.createElement("div", { className: "module-message__tap-to-view__spinner-container" },
            react_1.default.createElement(Spinner_1.Spinner, { svgSize: "small", size: "20px", direction: direction }))) : (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__tap-to-view__icon', `module-message__tap-to-view__icon--${direction}`, isTapToViewExpired
                ? 'module-message__tap-to-view__icon--expired'
                : null) }));
    }
    renderTapToViewText() {
        const { attachments, direction, i18n, isTapToViewExpired, isTapToViewError, } = this.props;
        const incomingString = isTapToViewExpired
            ? i18n('Message--tap-to-view-expired')
            : i18n(`Message--tap-to-view--incoming${(0, Attachment_1.isVideo)(attachments) ? '-video' : ''}`);
        const outgoingString = i18n('Message--tap-to-view--outgoing');
        const isDownloadPending = this.isAttachmentPending();
        if (isDownloadPending) {
            return;
        }
        // eslint-disable-next-line no-nested-ternary
        return isTapToViewError
            ? i18n('incomingError')
            : direction === 'outgoing'
                ? outgoingString
                : incomingString;
    }
    renderTapToView() {
        const { collapseMetadata, conversationType, direction, isTapToViewExpired, isTapToViewError, } = this.props;
        const withContentBelow = !collapseMetadata;
        const withContentAbove = !collapseMetadata &&
            conversationType === 'group' &&
            direction === 'incoming';
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__tap-to-view', withContentBelow
                ? 'module-message__tap-to-view--with-content-below'
                : null, withContentAbove
                ? 'module-message__tap-to-view--with-content-above'
                : null) },
            isTapToViewError ? null : this.renderTapToViewIcon(),
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__tap-to-view__text', `module-message__tap-to-view__text--${direction}`, isTapToViewExpired
                    ? `module-message__tap-to-view__text--${direction}-expired`
                    : null, isTapToViewError
                    ? `module-message__tap-to-view__text--${direction}-error`
                    : null) }, this.renderTapToViewText())));
    }
    popperPreventOverflowModifier() {
        const { containerElementRef } = this.props;
        return {
            name: 'preventOverflow',
            options: {
                altAxis: true,
                boundary: containerElementRef.current || undefined,
                padding: {
                    bottom: 16,
                    left: 8,
                    right: 8,
                    top: 16,
                },
            },
        };
    }
    renderReactions(outgoing) {
        const { reactions = [], i18n } = this.props;
        if (!this.hasReactions()) {
            return null;
        }
        const reactionsWithEmojiData = reactions.map(reaction => (Object.assign(Object.assign({}, reaction), (0, lib_1.emojiToData)(reaction.emoji))));
        // Group by emoji and order each group by timestamp descending
        const groupedAndSortedReactions = Object.values((0, lodash_1.groupBy)(reactionsWithEmojiData, 'short_name')).map(groupedReactions => (0, lodash_1.orderBy)(groupedReactions, [reaction => reaction.from.isMe, 'timestamp'], ['desc', 'desc']));
        // Order groups by length and subsequently by most recent reaction
        const ordered = (0, lodash_1.orderBy)(groupedAndSortedReactions, ['length', ([{ timestamp }]) => timestamp], ['desc', 'desc']);
        // Take the first three groups for rendering
        const toRender = (0, lodash_1.take)(ordered, 3).map(res => ({
            emoji: res[0].emoji,
            count: res.length,
            isMe: res.some(re => Boolean(re.from.isMe)),
        }));
        const someNotRendered = ordered.length > 3;
        // We only drop two here because the third emoji would be replaced by the
        // more button
        const maybeNotRendered = (0, lodash_1.drop)(ordered, 2);
        const maybeNotRenderedTotal = maybeNotRendered.reduce((sum, res) => sum + res.length, 0);
        const notRenderedIsMe = someNotRendered &&
            maybeNotRendered.some(res => res.some(re => Boolean(re.from.isMe)));
        const { reactionViewerRoot } = this.state;
        const popperPlacement = outgoing ? 'bottom-end' : 'bottom-start';
        return (react_1.default.createElement(react_popper_1.Manager, null,
            react_1.default.createElement(react_popper_1.Reference, null, ({ ref: popperRef }) => (react_1.default.createElement("div", { ref: this.reactionsContainerRefMerger(this.reactionsContainerRef, popperRef), className: (0, classnames_1.default)('module-message__reactions', outgoing
                    ? 'module-message__reactions--outgoing'
                    : 'module-message__reactions--incoming') }, toRender.map((re, i) => {
                const isLast = i === toRender.length - 1;
                const isMore = isLast && someNotRendered;
                const isMoreWithMe = isMore && notRenderedIsMe;
                return (react_1.default.createElement("button", { type: "button", 
                    // eslint-disable-next-line react/no-array-index-key
                    key: `${re.emoji}-${i}`, className: (0, classnames_1.default)('module-message__reactions__reaction', re.count > 1
                        ? 'module-message__reactions__reaction--with-count'
                        : null, outgoing
                        ? 'module-message__reactions__reaction--outgoing'
                        : 'module-message__reactions__reaction--incoming', isMoreWithMe || (re.isMe && !isMoreWithMe)
                        ? 'module-message__reactions__reaction--is-me'
                        : null), onClick: e => {
                        e.stopPropagation();
                        e.preventDefault();
                        this.toggleReactionViewer(false);
                    }, onKeyDown: e => {
                        // Prevent enter key from opening stickers/attachments
                        if (e.key === 'Enter') {
                            e.stopPropagation();
                        }
                    } }, isMore ? (react_1.default.createElement("span", { className: (0, classnames_1.default)('module-message__reactions__reaction__count', 'module-message__reactions__reaction__count--no-emoji', isMoreWithMe
                        ? 'module-message__reactions__reaction__count--is-me'
                        : null) },
                    "+",
                    maybeNotRenderedTotal)) : (react_1.default.createElement(react_1.default.Fragment, null,
                    react_1.default.createElement(Emoji_1.Emoji, { size: 16, emoji: re.emoji }),
                    re.count > 1 ? (react_1.default.createElement("span", { className: (0, classnames_1.default)('module-message__reactions__reaction__count', re.isMe
                            ? 'module-message__reactions__reaction__count--is-me'
                            : null) }, re.count)) : null))));
            })))),
            reactionViewerRoot &&
                (0, react_dom_1.createPortal)(react_1.default.createElement(StopPropagation_1.StopPropagation, null,
                    react_1.default.createElement(react_popper_1.Popper, { placement: popperPlacement, strategy: "fixed", modifiers: [this.popperPreventOverflowModifier()] }, ({ ref, style }) => (react_1.default.createElement(ReactionViewer_1.ReactionViewer, { ref: ref, style: Object.assign(Object.assign({}, style), { zIndex: 2 }), reactions: reactions, i18n: i18n, onClose: this.toggleReactionViewer })))), reactionViewerRoot)));
    }
    renderContents() {
        const { isTapToView, deletedForEveryone } = this.props;
        if (deletedForEveryone) {
            return (react_1.default.createElement(react_1.default.Fragment, null,
                this.renderText(),
                this.renderMetadata()));
        }
        if (isTapToView) {
            return (react_1.default.createElement(react_1.default.Fragment, null,
                this.renderTapToView(),
                this.renderMetadata()));
        }
        return (react_1.default.createElement(react_1.default.Fragment, null,
            this.renderQuote(),
            this.renderAttachment(),
            this.renderPreview(),
            this.renderEmbeddedContact(),
            this.renderText(),
            this.renderMetadata(),
            this.renderSendMessageButton()));
    }
    renderContainer() {
        const { attachments, conversationColor, customColor, deletedForEveryone, direction, isSticker, isTapToView, isTapToViewExpired, isTapToViewError, } = this.props;
        const { isSelected } = this.state;
        const isAttachmentPending = this.isAttachmentPending();
        const width = this.getWidth();
        const isShowingImage = this.isShowingImage();
        const isEmojiOnly = this.canRenderStickerLikeEmoji();
        const isStickerLike = isSticker || isEmojiOnly;
        const containerClassnames = (0, classnames_1.default)('module-message__container', (0, Attachment_1.isGIF)(attachments) ? 'module-message__container--gif' : null, isSelected && !isStickerLike
            ? 'module-message__container--selected'
            : null, isStickerLike ? 'module-message__container--with-sticker' : null, !isStickerLike ? `module-message__container--${direction}` : null, isEmojiOnly ? 'module-message__container--emoji' : null, isTapToView ? 'module-message__container--with-tap-to-view' : null, isTapToView && isTapToViewExpired
            ? 'module-message__container--with-tap-to-view-expired'
            : null, !isStickerLike && direction === 'outgoing'
            ? `module-message__container--outgoing-${conversationColor}`
            : null, isTapToView && isAttachmentPending && !isTapToViewExpired
            ? 'module-message__container--with-tap-to-view-pending'
            : null, isTapToView && isAttachmentPending && !isTapToViewExpired
            ? `module-message__container--${direction}-${conversationColor}-tap-to-view-pending`
            : null, isTapToViewError
            ? 'module-message__container--with-tap-to-view-error'
            : null, this.hasReactions() ? 'module-message__container--with-reactions' : null, deletedForEveryone
            ? 'module-message__container--deleted-for-everyone'
            : null);
        const containerStyles = {
            width: isShowingImage ? width : undefined,
        };
        if (!isStickerLike && direction === 'outgoing') {
            Object.assign(containerStyles, (0, getCustomColorStyle_1.getCustomColorStyle)(customColor));
        }
        return (react_1.default.createElement("div", { className: "module-message__container-outer" },
            react_1.default.createElement("div", { className: containerClassnames, style: containerStyles, onContextMenu: this.showContextMenu },
                this.renderAuthor(),
                this.renderContents()),
            this.renderReactions(direction === 'outgoing')));
    }
    render() {
        const { author, attachments, direction, id, isSticker, timestamp } = this.props;
        const { expired, expiring, imageBroken, isSelected } = this.state;
        // This id is what connects our triple-dot click with our associated pop-up menu.
        //   It needs to be unique.
        const triggerId = String(id || `${author.id}-${timestamp}`);
        if (expired) {
            return null;
        }
        if (isSticker && (imageBroken || !attachments || !attachments.length)) {
            return null;
        }
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message', `module-message--${direction}`, isSelected ? 'module-message--selected' : null, expiring ? 'module-message--expired' : null, this.hasAvatar() ? 'module-message--with-avatar' : null), tabIndex: 0, 
            // We pretend to be a button because we sometimes contain buttons and a button
            //   cannot be within another button
            role: "button", onKeyDown: this.handleKeyDown, onClick: this.handleClick, onFocus: this.handleFocus, ref: this.focusRef },
            this.renderError(direction === 'incoming'),
            this.renderMenu(direction === 'outgoing', triggerId),
            this.renderAvatar(),
            this.renderContainer(),
            this.renderError(direction === 'outgoing'),
            this.renderMenu(direction === 'incoming', triggerId),
            this.renderContextMenu(triggerId)));
    }
}
exports.Message = Message;
