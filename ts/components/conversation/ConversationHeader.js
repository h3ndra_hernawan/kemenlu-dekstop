"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationHeader = exports.OutgoingCallButtonStyle = void 0;
const react_1 = __importDefault(require("react"));
const react_measure_1 = __importDefault(require("react-measure"));
const classnames_1 = __importDefault(require("classnames"));
const react_contextmenu_1 = require("react-contextmenu");
const Emojify_1 = require("./Emojify");
const DisappearingTimeDialog_1 = require("../DisappearingTimeDialog");
const Avatar_1 = require("../Avatar");
const InContactsIcon_1 = require("../InContactsIcon");
const getMuteOptions_1 = require("../../util/getMuteOptions");
const expirationTimer = __importStar(require("../../util/expirationTimer"));
const missingCaseError_1 = require("../../util/missingCaseError");
const isInSystemContacts_1 = require("../../util/isInSystemContacts");
var OutgoingCallButtonStyle;
(function (OutgoingCallButtonStyle) {
    OutgoingCallButtonStyle[OutgoingCallButtonStyle["None"] = 0] = "None";
    OutgoingCallButtonStyle[OutgoingCallButtonStyle["JustVideo"] = 1] = "JustVideo";
    OutgoingCallButtonStyle[OutgoingCallButtonStyle["Both"] = 2] = "Both";
    OutgoingCallButtonStyle[OutgoingCallButtonStyle["Join"] = 3] = "Join";
})(OutgoingCallButtonStyle = exports.OutgoingCallButtonStyle || (exports.OutgoingCallButtonStyle = {}));
var ModalState;
(function (ModalState) {
    ModalState[ModalState["NothingOpen"] = 0] = "NothingOpen";
    ModalState[ModalState["CustomDisappearingTimeout"] = 1] = "CustomDisappearingTimeout";
})(ModalState || (ModalState = {}));
const TIMER_ITEM_CLASS = 'module-ConversationHeader__disappearing-timer__item';
class ConversationHeader extends react_1.default.Component {
    constructor(props) {
        super(props);
        this.state = { isNarrow: false, modalState: ModalState.NothingOpen };
        this.menuTriggerRef = react_1.default.createRef();
        this.headerRef = react_1.default.createRef();
        this.showMenuBound = this.showMenu.bind(this);
    }
    showMenu(event) {
        if (this.menuTriggerRef.current) {
            this.menuTriggerRef.current.handleContextClick(event);
        }
    }
    renderBackButton() {
        const { i18n, onGoBack, showBackButton } = this.props;
        return (react_1.default.createElement("button", { type: "button", onClick: onGoBack, className: (0, classnames_1.default)('module-ConversationHeader__back-icon', showBackButton ? 'module-ConversationHeader__back-icon--show' : null), disabled: !showBackButton, "aria-label": i18n('goBack') }));
    }
    renderHeaderInfoTitle() {
        const { name, title, type, i18n, isMe } = this.props;
        if (isMe) {
            return (react_1.default.createElement("div", { className: "module-ConversationHeader__header__info__title" }, i18n('noteToSelf')));
        }
        return (react_1.default.createElement("div", { className: "module-ConversationHeader__header__info__title" },
            react_1.default.createElement(Emojify_1.Emojify, { text: title }),
            (0, isInSystemContacts_1.isInSystemContacts)({ name, type }) ? (react_1.default.createElement(InContactsIcon_1.InContactsIcon, { className: "module-ConversationHeader__header__info__title__in-contacts-icon", i18n: i18n, tooltipContainerRef: this.headerRef })) : null));
    }
    renderHeaderInfoSubtitle() {
        const expirationNode = this.renderExpirationLength();
        const verifiedNode = this.renderVerifiedIcon();
        if (expirationNode || verifiedNode) {
            return (react_1.default.createElement("div", { className: "module-ConversationHeader__header__info__subtitle" },
                expirationNode,
                verifiedNode));
        }
        return null;
    }
    renderAvatar() {
        const { acceptedMessageRequest, avatarPath, badge, color, i18n, type, isMe, name, phoneNumber, profileName, sharedGroupNames, theme, title, unblurredAvatarPath, } = this.props;
        return (react_1.default.createElement("span", { className: "module-ConversationHeader__header__avatar" },
            react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, badge: badge, color: color, conversationType: type, i18n: i18n, isMe: isMe, noteToSelf: isMe, title: title, name: name, phoneNumber: phoneNumber, profileName: profileName, sharedGroupNames: sharedGroupNames, size: Avatar_1.AvatarSize.THIRTY_TWO, theme: theme, unblurredAvatarPath: unblurredAvatarPath })));
    }
    renderExpirationLength() {
        const { i18n, expireTimer } = this.props;
        if (!expireTimer) {
            return null;
        }
        return (react_1.default.createElement("div", { className: "module-ConversationHeader__header__info__subtitle__expiration" }, expirationTimer.format(i18n, expireTimer)));
    }
    renderVerifiedIcon() {
        const { i18n, isVerified } = this.props;
        if (!isVerified) {
            return null;
        }
        return (react_1.default.createElement("div", { className: "module-ConversationHeader__header__info__subtitle__verified" }, i18n('verified')));
    }
    renderMoreButton(triggerId) {
        const { i18n, showBackButton } = this.props;
        return (react_1.default.createElement(react_contextmenu_1.ContextMenuTrigger, { id: triggerId, ref: this.menuTriggerRef },
            react_1.default.createElement("button", { type: "button", onClick: this.showMenuBound, className: (0, classnames_1.default)('module-ConversationHeader__button', 'module-ConversationHeader__button--more', showBackButton ? null : 'module-ConversationHeader__button--show'), disabled: showBackButton, "aria-label": i18n('moreInfo') })));
    }
    renderSearchButton() {
        const { i18n, onSearchInConversation, showBackButton } = this.props;
        return (react_1.default.createElement("button", { type: "button", onClick: onSearchInConversation, className: (0, classnames_1.default)('module-ConversationHeader__button', 'module-ConversationHeader__button--search', showBackButton ? null : 'module-ConversationHeader__button--show'), disabled: showBackButton, "aria-label": i18n('search') }));
    }
    renderOutgoingCallButtons() {
        const { announcementsOnly, areWeAdmin, i18n, onOutgoingAudioCallInConversation, onOutgoingVideoCallInConversation, outgoingCallButtonStyle, showBackButton, } = this.props;
        const { isNarrow } = this.state;
        const videoButton = (react_1.default.createElement("button", { "aria-label": i18n('makeOutgoingVideoCall'), className: (0, classnames_1.default)('module-ConversationHeader__button', 'module-ConversationHeader__button--video', showBackButton ? null : 'module-ConversationHeader__button--show', !showBackButton && announcementsOnly && !areWeAdmin
                ? 'module-ConversationHeader__button--show-disabled'
                : undefined), disabled: showBackButton, onClick: onOutgoingVideoCallInConversation, type: "button" }));
        switch (outgoingCallButtonStyle) {
            case OutgoingCallButtonStyle.None:
                return null;
            case OutgoingCallButtonStyle.JustVideo:
                return videoButton;
            case OutgoingCallButtonStyle.Both:
                return (react_1.default.createElement(react_1.default.Fragment, null,
                    videoButton,
                    react_1.default.createElement("button", { type: "button", onClick: onOutgoingAudioCallInConversation, className: (0, classnames_1.default)('module-ConversationHeader__button', 'module-ConversationHeader__button--audio', showBackButton
                            ? null
                            : 'module-ConversationHeader__button--show'), disabled: showBackButton, "aria-label": i18n('makeOutgoingCall') })));
            case OutgoingCallButtonStyle.Join:
                return (react_1.default.createElement("button", { "aria-label": i18n('joinOngoingCall'), className: (0, classnames_1.default)('module-ConversationHeader__button', 'module-ConversationHeader__button--join-call', showBackButton ? null : 'module-ConversationHeader__button--show'), disabled: showBackButton, onClick: onOutgoingVideoCallInConversation, type: "button" }, isNarrow ? null : i18n('joinOngoingCall')));
            default:
                throw (0, missingCaseError_1.missingCaseError)(outgoingCallButtonStyle);
        }
    }
    renderMenu(triggerId) {
        const { acceptedMessageRequest, canChangeTimer, expireTimer, groupVersion, i18n, isArchived, isMissingMandatoryProfileSharing, isPinned, left, markedUnread, muteExpiresAt, onArchive, onDeleteMessages, onMarkUnread, onMoveToInbox, onSetDisappearingMessages, onSetMuteNotifications, onSetPin, onShowAllMedia, onShowConversationDetails, onShowGroupMembers, type, } = this.props;
        const muteOptions = (0, getMuteOptions_1.getMuteOptions)(muteExpiresAt, i18n);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const disappearingTitle = i18n('disappearingMessages');
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const muteTitle = i18n('muteNotificationsTitle');
        const isGroup = type === 'group';
        const disableTimerChanges = Boolean(!canChangeTimer ||
            !acceptedMessageRequest ||
            left ||
            isMissingMandatoryProfileSharing);
        const hasGV2AdminEnabled = isGroup && groupVersion === 2;
        const isActiveExpireTimer = (value) => {
            if (!expireTimer) {
                return value === 0;
            }
            // Custom time...
            if (value === -1) {
                return !expirationTimer.DEFAULT_DURATIONS_SET.has(expireTimer);
            }
            return value === expireTimer;
        };
        const expireDurations = [
            ...expirationTimer.DEFAULT_DURATIONS_IN_SECONDS,
            -1,
        ].map((seconds) => {
            let text;
            if (seconds === -1) {
                text = i18n('customDisappearingTimeOption');
            }
            else {
                text = expirationTimer.format(i18n, seconds, {
                    capitalizeOff: true,
                });
            }
            const onDurationClick = () => {
                if (seconds === -1) {
                    this.setState({
                        modalState: ModalState.CustomDisappearingTimeout,
                    });
                }
                else {
                    onSetDisappearingMessages(seconds);
                }
            };
            return (react_1.default.createElement(react_contextmenu_1.MenuItem, { key: seconds, onClick: onDurationClick },
                react_1.default.createElement("div", { className: (0, classnames_1.default)(TIMER_ITEM_CLASS, isActiveExpireTimer(seconds) && `${TIMER_ITEM_CLASS}--active`) }, text)));
        });
        return (react_1.default.createElement(react_contextmenu_1.ContextMenu, { id: triggerId },
            disableTimerChanges ? null : (react_1.default.createElement(react_contextmenu_1.SubMenu, { hoverDelay: 1, title: disappearingTitle }, expireDurations)),
            react_1.default.createElement(react_contextmenu_1.SubMenu, { hoverDelay: 1, title: muteTitle }, muteOptions.map(item => (react_1.default.createElement(react_contextmenu_1.MenuItem, { key: item.name, disabled: item.disabled, onClick: () => {
                    onSetMuteNotifications(item.value);
                } }, item.name)))),
            !isGroup || hasGV2AdminEnabled ? (react_1.default.createElement(react_contextmenu_1.MenuItem, { onClick: onShowConversationDetails }, isGroup
                ? i18n('showConversationDetails')
                : i18n('showConversationDetails--direct'))) : null,
            isGroup && !hasGV2AdminEnabled ? (react_1.default.createElement(react_contextmenu_1.MenuItem, { onClick: onShowGroupMembers }, i18n('showMembers'))) : null,
            react_1.default.createElement(react_contextmenu_1.MenuItem, { onClick: onShowAllMedia }, i18n('viewRecentMedia')),
            react_1.default.createElement(react_contextmenu_1.MenuItem, { divider: true }),
            !markedUnread ? (react_1.default.createElement(react_contextmenu_1.MenuItem, { onClick: onMarkUnread }, i18n('markUnread'))) : null,
            isArchived ? (react_1.default.createElement(react_contextmenu_1.MenuItem, { onClick: onMoveToInbox }, i18n('moveConversationToInbox'))) : (react_1.default.createElement(react_contextmenu_1.MenuItem, { onClick: onArchive }, i18n('archiveConversation'))),
            react_1.default.createElement(react_contextmenu_1.MenuItem, { onClick: onDeleteMessages }, i18n('deleteMessages')),
            isPinned ? (react_1.default.createElement(react_contextmenu_1.MenuItem, { onClick: () => onSetPin(false) }, i18n('unpinConversation'))) : (react_1.default.createElement(react_contextmenu_1.MenuItem, { onClick: () => onSetPin(true) }, i18n('pinConversation')))));
    }
    renderHeader() {
        const { conversationTitle, groupVersion, onShowConversationDetails, type } = this.props;
        if (conversationTitle !== undefined) {
            return (react_1.default.createElement("div", { className: "module-ConversationHeader__header" },
                react_1.default.createElement("div", { className: "module-ConversationHeader__header__info" },
                    react_1.default.createElement("div", { className: "module-ConversationHeader__header__info__title" }, conversationTitle))));
        }
        let onClick;
        switch (type) {
            case 'direct':
                onClick = () => {
                    onShowConversationDetails();
                };
                break;
            case 'group': {
                const hasGV2AdminEnabled = groupVersion === 2;
                onClick = hasGV2AdminEnabled
                    ? () => {
                        onShowConversationDetails();
                    }
                    : undefined;
                break;
            }
            default:
                throw (0, missingCaseError_1.missingCaseError)(type);
        }
        const contents = (react_1.default.createElement(react_1.default.Fragment, null,
            this.renderAvatar(),
            react_1.default.createElement("div", { className: "module-ConversationHeader__header__info" },
                this.renderHeaderInfoTitle(),
                this.renderHeaderInfoSubtitle())));
        if (onClick) {
            return (react_1.default.createElement("button", { type: "button", className: "module-ConversationHeader__header module-ConversationHeader__header--clickable", onClick: onClick }, contents));
        }
        return (react_1.default.createElement("div", { className: "module-ConversationHeader__header", ref: this.headerRef }, contents));
    }
    render() {
        const { id, isSMSOnly, i18n, onSetDisappearingMessages, expireTimer } = this.props;
        const { isNarrow, modalState } = this.state;
        const triggerId = `conversation-${id}`;
        let modalNode;
        if (modalState === ModalState.NothingOpen) {
            modalNode = undefined;
        }
        else if (modalState === ModalState.CustomDisappearingTimeout) {
            modalNode = (react_1.default.createElement(DisappearingTimeDialog_1.DisappearingTimeDialog, { i18n: i18n, initialValue: expireTimer, onSubmit: value => {
                    this.setState({ modalState: ModalState.NothingOpen });
                    onSetDisappearingMessages(value);
                }, onClose: () => this.setState({ modalState: ModalState.NothingOpen }) }));
        }
        else {
            throw (0, missingCaseError_1.missingCaseError)(modalState);
        }
        return (react_1.default.createElement(react_1.default.Fragment, null,
            modalNode,
            react_1.default.createElement(react_measure_1.default, { bounds: true, onResize: ({ bounds }) => {
                    if (!bounds || !bounds.width) {
                        return;
                    }
                    this.setState({ isNarrow: bounds.width < 500 });
                } }, ({ measureRef }) => (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-ConversationHeader', {
                    'module-ConversationHeader--narrow': isNarrow,
                }), ref: measureRef },
                this.renderBackButton(),
                this.renderHeader(),
                !isSMSOnly && this.renderOutgoingCallButtons(),
                this.renderSearchButton(),
                this.renderMoreButton(triggerId),
                this.renderMenu(triggerId))))));
    }
}
exports.ConversationHeader = ConversationHeader;
