"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExpireTimer = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const timer_1 = require("../../util/timer");
class ExpireTimer extends react_1.default.Component {
    constructor(props) {
        super(props);
        this.interval = null;
    }
    componentDidMount() {
        const { expirationLength } = this.props;
        const increment = (0, timer_1.getIncrement)(expirationLength);
        const updateFrequency = Math.max(increment, 500);
        const update = () => {
            this.setState({
                // Used to trigger renders
                // eslint-disable-next-line react/no-unused-state
                lastUpdated: Date.now(),
            });
        };
        this.interval = setInterval(update, updateFrequency);
    }
    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }
    render() {
        const { direction, expirationLength, expirationTimestamp, withImageNoCaption, withSticker, withTapToViewExpired, } = this.props;
        const bucket = (0, timer_1.getTimerBucket)(expirationTimestamp, expirationLength);
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-expire-timer', `module-expire-timer--${bucket}`, direction ? `module-expire-timer--${direction}` : null, withTapToViewExpired
                ? `module-expire-timer--${direction}-with-tap-to-view-expired`
                : null, direction && withImageNoCaption
                ? 'module-expire-timer--with-image-no-caption'
                : null, withSticker ? 'module-expire-timer--with-sticker' : null) }));
    }
}
exports.ExpireTimer = ExpireTimer;
