"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const addon_actions_1 = require("@storybook/addon-actions");
const react_2 = require("@storybook/react");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const ContactSpoofingReviewDialog_1 = require("./ContactSpoofingReviewDialog");
const contactSpoofing_1 = require("../../util/contactSpoofing");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/Conversation/ContactSpoofingReviewDialog', module);
const getCommonProps = () => ({
    i18n,
    onBlock: (0, addon_actions_1.action)('onBlock'),
    onBlockAndReportSpam: (0, addon_actions_1.action)('onBlockAndReportSpam'),
    onClose: (0, addon_actions_1.action)('onClose'),
    onDelete: (0, addon_actions_1.action)('onDelete'),
    onShowContactModal: (0, addon_actions_1.action)('onShowContactModal'),
    onUnblock: (0, addon_actions_1.action)('onUnblock'),
    removeMember: (0, addon_actions_1.action)('removeMember'),
});
story.add('Direct conversations with same title', () => (react_1.default.createElement(ContactSpoofingReviewDialog_1.ContactSpoofingReviewDialog, Object.assign({}, getCommonProps(), { type: contactSpoofing_1.ContactSpoofingType.DirectConversationWithSameTitle, possiblyUnsafeConversation: (0, getDefaultConversation_1.getDefaultConversation)(), safeConversation: (0, getDefaultConversation_1.getDefaultConversation)() }))));
[false, true].forEach(areWeAdmin => {
    story.add(`Group conversation many group members${areWeAdmin ? " (and we're an admin)" : ''}`, () => (react_1.default.createElement(ContactSpoofingReviewDialog_1.ContactSpoofingReviewDialog, Object.assign({}, getCommonProps(), { type: contactSpoofing_1.ContactSpoofingType.MultipleGroupMembersWithSameTitle, areWeAdmin: areWeAdmin, collisionInfoByTitle: {
            Alice: (0, lodash_1.times)(2, () => ({
                oldName: 'Alicia',
                conversation: (0, getDefaultConversation_1.getDefaultConversation)({ title: 'Alice' }),
            })),
            Bob: (0, lodash_1.times)(3, () => ({
                conversation: (0, getDefaultConversation_1.getDefaultConversation)({ title: 'Bob' }),
            })),
            Charlie: (0, lodash_1.times)(5, () => ({
                conversation: (0, getDefaultConversation_1.getDefaultConversation)({ title: 'Charlie' }),
            })),
        } }))));
});
