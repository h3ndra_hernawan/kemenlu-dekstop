"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const Colors_1 = require("../../types/Colors");
const Fixtures_1 = require("../../storybook/Fixtures");
const Message_1 = require("./Message");
const MIME_1 = require("../../types/MIME");
const Quote_1 = require("./Quote");
const MessageReadStatus_1 = require("../../messages/MessageReadStatus");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const _util_1 = require("../_util");
const Util_1 = require("../../types/Util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/Quote', module);
const defaultMessageProps = {
    author: (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'some-id',
        title: 'Person X',
    }),
    authorBadge: undefined,
    canReply: true,
    canDeleteForEveryone: true,
    canDownload: true,
    checkForAccount: (0, addon_actions_1.action)('checkForAccount'),
    clearSelectedMessage: (0, addon_actions_1.action)('default--clearSelectedMessage'),
    containerElementRef: React.createRef(),
    containerWidthBreakpoint: _util_1.WidthBreakpoint.Wide,
    conversationColor: 'crimson',
    conversationId: 'conversationId',
    conversationType: 'direct',
    deleteMessage: (0, addon_actions_1.action)('default--deleteMessage'),
    deleteMessageForEveryone: (0, addon_actions_1.action)('default--deleteMessageForEveryone'),
    direction: 'incoming',
    displayTapToViewMessage: (0, addon_actions_1.action)('default--displayTapToViewMessage'),
    downloadAttachment: (0, addon_actions_1.action)('default--downloadAttachment'),
    doubleCheckMissingQuoteReference: (0, addon_actions_1.action)('default--doubleCheckMissingQuoteReference'),
    i18n,
    id: 'messageId',
    renderingContext: 'storybook',
    interactionMode: 'keyboard',
    isBlocked: false,
    isMessageRequestAccepted: true,
    kickOffAttachmentDownload: (0, addon_actions_1.action)('default--kickOffAttachmentDownload'),
    markAttachmentAsCorrupted: (0, addon_actions_1.action)('default--markAttachmentAsCorrupted'),
    markViewed: (0, addon_actions_1.action)('default--markViewed'),
    messageExpanded: (0, addon_actions_1.action)('dafult--message-expanded'),
    onHeightChange: (0, addon_actions_1.action)('default--onHeightChange'),
    openConversation: (0, addon_actions_1.action)('default--openConversation'),
    openLink: (0, addon_actions_1.action)('default--openLink'),
    previews: [],
    reactToMessage: (0, addon_actions_1.action)('default--reactToMessage'),
    readStatus: MessageReadStatus_1.ReadStatus.Read,
    renderEmojiPicker: () => React.createElement("div", null),
    renderReactionPicker: () => React.createElement("div", null),
    renderAudioAttachment: () => React.createElement("div", null, "*AudioAttachment*"),
    replyToMessage: (0, addon_actions_1.action)('default--replyToMessage'),
    retrySend: (0, addon_actions_1.action)('default--retrySend'),
    scrollToQuotedMessage: (0, addon_actions_1.action)('default--scrollToQuotedMessage'),
    selectMessage: (0, addon_actions_1.action)('default--selectMessage'),
    showContactDetail: (0, addon_actions_1.action)('default--showContactDetail'),
    showContactModal: (0, addon_actions_1.action)('default--showContactModal'),
    showExpiredIncomingTapToViewToast: (0, addon_actions_1.action)('showExpiredIncomingTapToViewToast'),
    showExpiredOutgoingTapToViewToast: (0, addon_actions_1.action)('showExpiredOutgoingTapToViewToast'),
    showForwardMessageModal: (0, addon_actions_1.action)('default--showForwardMessageModal'),
    showMessageDetail: (0, addon_actions_1.action)('default--showMessageDetail'),
    showVisualAttachment: (0, addon_actions_1.action)('default--showVisualAttachment'),
    status: 'sent',
    text: 'This is really interesting.',
    theme: Util_1.ThemeType.light,
    timestamp: Date.now(),
};
const renderInMessage = ({ authorTitle, conversationColor, isFromMe, rawAttachment, isViewOnce, referencedMessageNotFound, text: quoteText, }) => {
    const messageProps = Object.assign(Object.assign({}, defaultMessageProps), { conversationColor, quote: {
            authorId: 'an-author',
            authorTitle,
            conversationColor,
            isFromMe,
            rawAttachment,
            isViewOnce,
            referencedMessageNotFound,
            sentAt: Date.now() - 30 * 1000,
            text: quoteText,
        } });
    return (React.createElement("div", { style: { overflow: 'hidden' } },
        React.createElement(Message_1.Message, Object.assign({}, messageProps)),
        React.createElement("br", null),
        React.createElement(Message_1.Message, Object.assign({}, messageProps, { direction: "outgoing" }))));
};
const createProps = (overrideProps = {}) => ({
    authorTitle: (0, addon_knobs_1.text)('authorTitle', overrideProps.authorTitle || ''),
    conversationColor: overrideProps.conversationColor || 'forest',
    doubleCheckMissingQuoteReference: overrideProps.doubleCheckMissingQuoteReference ||
        (0, addon_actions_1.action)('doubleCheckMissingQuoteReference'),
    i18n,
    isFromMe: (0, addon_knobs_1.boolean)('isFromMe', overrideProps.isFromMe || false),
    isIncoming: (0, addon_knobs_1.boolean)('isIncoming', overrideProps.isIncoming || false),
    onClick: (0, addon_actions_1.action)('onClick'),
    onClose: (0, addon_actions_1.action)('onClose'),
    rawAttachment: overrideProps.rawAttachment || undefined,
    referencedMessageNotFound: (0, addon_knobs_1.boolean)('referencedMessageNotFound', overrideProps.referencedMessageNotFound || false),
    isViewOnce: (0, addon_knobs_1.boolean)('isViewOnce', overrideProps.isViewOnce || false),
    text: (0, addon_knobs_1.text)('text', (0, lodash_1.isString)(overrideProps.text)
        ? overrideProps.text
        : 'A sample message from a pal'),
});
story.add('Outgoing by Another Author', () => {
    const props = createProps({
        authorTitle: 'Terrence Malick',
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Outgoing by Me', () => {
    const props = createProps({
        isFromMe: true,
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Incoming by Another Author', () => {
    const props = createProps({
        authorTitle: 'Terrence Malick',
        isIncoming: true,
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Incoming by Me', () => {
    const props = createProps({
        isFromMe: true,
        isIncoming: true,
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Incoming/Outgoing Colors', () => {
    const props = createProps({});
    return (React.createElement(React.Fragment, null, Colors_1.ConversationColors.map(color => renderInMessage(Object.assign(Object.assign({}, props), { conversationColor: color })))));
});
story.add('Image Only', () => {
    const props = createProps({
        text: '',
        rawAttachment: {
            contentType: MIME_1.IMAGE_PNG,
            fileName: 'sax.png',
            isVoiceMessage: false,
            thumbnail: {
                contentType: MIME_1.IMAGE_PNG,
                objectUrl: Fixtures_1.pngUrl,
            },
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Image Attachment', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.IMAGE_PNG,
            fileName: 'sax.png',
            isVoiceMessage: false,
            thumbnail: {
                contentType: MIME_1.IMAGE_PNG,
                objectUrl: Fixtures_1.pngUrl,
            },
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Image Attachment w/o Thumbnail', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.IMAGE_PNG,
            fileName: 'sax.png',
            isVoiceMessage: false,
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Image Tap-to-View', () => {
    const props = createProps({
        text: '',
        isViewOnce: true,
        rawAttachment: {
            contentType: MIME_1.IMAGE_PNG,
            fileName: 'sax.png',
            isVoiceMessage: false,
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Video Only', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.VIDEO_MP4,
            fileName: 'great-video.mp4',
            isVoiceMessage: false,
            thumbnail: {
                contentType: MIME_1.IMAGE_PNG,
                objectUrl: Fixtures_1.pngUrl,
            },
        },
    });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    props.text = undefined;
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Video Attachment', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.VIDEO_MP4,
            fileName: 'great-video.mp4',
            isVoiceMessage: false,
            thumbnail: {
                contentType: MIME_1.IMAGE_PNG,
                objectUrl: Fixtures_1.pngUrl,
            },
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Video Attachment w/o Thumbnail', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.VIDEO_MP4,
            fileName: 'great-video.mp4',
            isVoiceMessage: false,
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Video Tap-to-View', () => {
    const props = createProps({
        text: '',
        isViewOnce: true,
        rawAttachment: {
            contentType: MIME_1.VIDEO_MP4,
            fileName: 'great-video.mp4',
            isVoiceMessage: false,
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Audio Only', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.AUDIO_MP3,
            fileName: 'great-video.mp3',
            isVoiceMessage: false,
        },
    });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    props.text = undefined;
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Audio Attachment', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.AUDIO_MP3,
            fileName: 'great-video.mp3',
            isVoiceMessage: false,
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Voice Message Only', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.AUDIO_MP3,
            fileName: 'great-video.mp3',
            isVoiceMessage: true,
        },
    });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    props.text = undefined;
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Voice Message Attachment', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.AUDIO_MP3,
            fileName: 'great-video.mp3',
            isVoiceMessage: true,
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Other File Only', () => {
    const props = createProps({
        rawAttachment: {
            contentType: (0, MIME_1.stringToMIMEType)('application/json'),
            fileName: 'great-data.json',
            isVoiceMessage: false,
        },
    });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    props.text = undefined;
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Media Tap-to-View', () => {
    const props = createProps({
        text: '',
        isViewOnce: true,
        rawAttachment: {
            contentType: MIME_1.AUDIO_MP3,
            fileName: 'great-video.mp3',
            isVoiceMessage: false,
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Other File Attachment', () => {
    const props = createProps({
        rawAttachment: {
            contentType: (0, MIME_1.stringToMIMEType)('application/json'),
            fileName: 'great-data.json',
            isVoiceMessage: false,
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Long message attachment (should be hidden)', () => {
    const props = createProps({
        rawAttachment: {
            contentType: MIME_1.LONG_MESSAGE,
            fileName: 'signal-long-message-123.txt',
            isVoiceMessage: false,
        },
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('No Close Button', () => {
    const props = createProps();
    props.onClose = undefined;
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Message Not Found', () => {
    const props = createProps({
        referencedMessageNotFound: true,
    });
    return renderInMessage(props);
});
story.add('Missing Text & Attachment', () => {
    const props = createProps();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    props.text = undefined;
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('@mention + outgoing + another author', () => {
    const props = createProps({
        authorTitle: 'Tony Stark',
        text: '@Captain America Lunch later?',
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('@mention + outgoing + me', () => {
    const props = createProps({
        isFromMe: true,
        text: '@Captain America Lunch later?',
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('@mention + incoming + another author', () => {
    const props = createProps({
        authorTitle: 'Captain America',
        isIncoming: true,
        text: '@Tony Stark sure',
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('@mention + incoming + me', () => {
    const props = createProps({
        isFromMe: true,
        isIncoming: true,
        text: '@Tony Stark sure',
    });
    return React.createElement(Quote_1.Quote, Object.assign({}, props));
});
story.add('Custom Color', () => (React.createElement(React.Fragment, null,
    React.createElement(Quote_1.Quote, Object.assign({}, createProps({ isIncoming: true, text: 'Solid + Gradient' }), { customColor: {
            start: { hue: 82, saturation: 35 },
        } })),
    React.createElement(Quote_1.Quote, Object.assign({}, createProps(), { customColor: {
            deg: 192,
            start: { hue: 304, saturation: 85 },
            end: { hue: 231, saturation: 76 },
        } })))));
