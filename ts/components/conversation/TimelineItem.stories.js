"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const EmojiPicker_1 = require("../emoji/EmojiPicker");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const TimelineItem_1 = require("./TimelineItem");
const UniversalTimerNotification_1 = require("./UniversalTimerNotification");
const Calling_1 = require("../../types/Calling");
const Colors_1 = require("../../types/Colors");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const _util_1 = require("../_util");
const Util_1 = require("../../types/Util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const renderEmojiPicker = ({ onClose, onPickEmoji, ref, }) => (React.createElement(EmojiPicker_1.EmojiPicker, { i18n: (0, setupI18n_1.setupI18n)('en', messages_json_1.default), skinTone: 0, onSetSkinTone: (0, addon_actions_1.action)('EmojiPicker::onSetSkinTone'), ref: ref, onClose: onClose, onPickEmoji: onPickEmoji }));
const renderReactionPicker = () => (React.createElement("div", null));
const renderContact = (conversationId) => (React.createElement(React.Fragment, { key: conversationId }, conversationId));
const renderUniversalTimerNotification = () => (React.createElement(UniversalTimerNotification_1.UniversalTimerNotification, { i18n: i18n, expireTimer: 3600 }));
const getDefaultProps = () => ({
    containerElementRef: React.createRef(),
    containerWidthBreakpoint: _util_1.WidthBreakpoint.Wide,
    conversationId: 'conversation-id',
    id: 'asdf',
    isSelected: false,
    interactionMode: 'keyboard',
    theme: Util_1.ThemeType.light,
    selectMessage: (0, addon_actions_1.action)('selectMessage'),
    reactToMessage: (0, addon_actions_1.action)('reactToMessage'),
    checkForAccount: (0, addon_actions_1.action)('checkForAccount'),
    clearSelectedMessage: (0, addon_actions_1.action)('clearSelectedMessage'),
    contactSupport: (0, addon_actions_1.action)('contactSupport'),
    replyToMessage: (0, addon_actions_1.action)('replyToMessage'),
    retrySend: (0, addon_actions_1.action)('retrySend'),
    deleteMessage: (0, addon_actions_1.action)('deleteMessage'),
    deleteMessageForEveryone: (0, addon_actions_1.action)('deleteMessageForEveryone'),
    kickOffAttachmentDownload: (0, addon_actions_1.action)('kickOffAttachmentDownload'),
    learnMoreAboutDeliveryIssue: (0, addon_actions_1.action)('learnMoreAboutDeliveryIssue'),
    markAttachmentAsCorrupted: (0, addon_actions_1.action)('markAttachmentAsCorrupted'),
    markViewed: (0, addon_actions_1.action)('markViewed'),
    messageExpanded: (0, addon_actions_1.action)('messageExpanded'),
    showMessageDetail: (0, addon_actions_1.action)('showMessageDetail'),
    openConversation: (0, addon_actions_1.action)('openConversation'),
    showContactDetail: (0, addon_actions_1.action)('showContactDetail'),
    showContactModal: (0, addon_actions_1.action)('showContactModal'),
    showForwardMessageModal: (0, addon_actions_1.action)('showForwardMessageModal'),
    showVisualAttachment: (0, addon_actions_1.action)('showVisualAttachment'),
    downloadAttachment: (0, addon_actions_1.action)('downloadAttachment'),
    displayTapToViewMessage: (0, addon_actions_1.action)('displayTapToViewMessage'),
    doubleCheckMissingQuoteReference: (0, addon_actions_1.action)('doubleCheckMissingQuoteReference'),
    showExpiredIncomingTapToViewToast: (0, addon_actions_1.action)('showExpiredIncomingTapToViewToast'),
    showExpiredOutgoingTapToViewToast: (0, addon_actions_1.action)('showExpiredIncomingTapToViewToast'),
    onHeightChange: (0, addon_actions_1.action)('onHeightChange'),
    openLink: (0, addon_actions_1.action)('openLink'),
    scrollToQuotedMessage: (0, addon_actions_1.action)('scrollToQuotedMessage'),
    downloadNewVersion: (0, addon_actions_1.action)('downloadNewVersion'),
    showIdentity: (0, addon_actions_1.action)('showIdentity'),
    messageSizeChanged: (0, addon_actions_1.action)('messageSizeChanged'),
    startCallingLobby: (0, addon_actions_1.action)('startCallingLobby'),
    returnToActiveCall: (0, addon_actions_1.action)('returnToActiveCall'),
    previousItem: undefined,
    nextItem: undefined,
    renderContact,
    renderUniversalTimerNotification,
    renderEmojiPicker,
    renderReactionPicker,
    renderAudioAttachment: () => React.createElement("div", null, "*AudioAttachment*"),
});
(0, react_1.storiesOf)('Components/Conversation/TimelineItem', module)
    .add('Plain Message', () => {
    const item = {
        type: 'message',
        data: {
            id: 'id-1',
            direction: 'incoming',
            timestamp: Date.now(),
            author: {
                phoneNumber: '(202) 555-2001',
                color: Colors_1.AvatarColors[0],
            },
            text: '🔥',
        },
    };
    return React.createElement(TimelineItem_1.TimelineItem, Object.assign({}, getDefaultProps(), { item: item, i18n: i18n }));
})
    .add('Notification', () => {
    const items = [
        {
            type: 'timerNotification',
            data: Object.assign(Object.assign({ phoneNumber: '(202) 555-0000', expireTimer: 60 }, (0, getDefaultConversation_1.getDefaultConversation)()), { type: 'fromOther' }),
        },
        {
            type: 'universalTimerNotification',
            data: null,
        },
        {
            type: 'chatSessionRefreshed',
        },
        {
            type: 'safetyNumberNotification',
            data: {
                isGroup: false,
                contact: (0, getDefaultConversation_1.getDefaultConversation)(),
            },
        },
        {
            type: 'deliveryIssue',
            data: {
                sender: (0, getDefaultConversation_1.getDefaultConversation)(),
            },
        },
        {
            type: 'changeNumberNotification',
            data: {
                sender: (0, getDefaultConversation_1.getDefaultConversation)(),
                timestamp: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // declined incoming audio
                callMode: Calling_1.CallMode.Direct,
                wasDeclined: true,
                wasIncoming: true,
                wasVideoCall: false,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // declined incoming video
                callMode: Calling_1.CallMode.Direct,
                wasDeclined: true,
                wasIncoming: true,
                wasVideoCall: true,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // accepted incoming audio
                callMode: Calling_1.CallMode.Direct,
                acceptedTime: Date.now() - 300,
                wasDeclined: false,
                wasIncoming: true,
                wasVideoCall: false,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // accepted incoming video
                callMode: Calling_1.CallMode.Direct,
                acceptedTime: Date.now() - 400,
                wasDeclined: false,
                wasIncoming: true,
                wasVideoCall: true,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // missed (neither accepted nor declined) incoming audio
                callMode: Calling_1.CallMode.Direct,
                wasDeclined: false,
                wasIncoming: true,
                wasVideoCall: false,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // missed (neither accepted nor declined) incoming video
                callMode: Calling_1.CallMode.Direct,
                wasDeclined: false,
                wasIncoming: true,
                wasVideoCall: true,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // accepted outgoing audio
                callMode: Calling_1.CallMode.Direct,
                acceptedTime: Date.now() - 200,
                wasDeclined: false,
                wasIncoming: false,
                wasVideoCall: false,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // accepted outgoing video
                callMode: Calling_1.CallMode.Direct,
                acceptedTime: Date.now() - 200,
                wasDeclined: false,
                wasIncoming: false,
                wasVideoCall: true,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // declined outgoing audio
                callMode: Calling_1.CallMode.Direct,
                wasDeclined: true,
                wasIncoming: false,
                wasVideoCall: false,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // declined outgoing video
                callMode: Calling_1.CallMode.Direct,
                wasDeclined: true,
                wasIncoming: false,
                wasVideoCall: true,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // unanswered (neither accepted nor declined) outgoing audio
                callMode: Calling_1.CallMode.Direct,
                wasDeclined: false,
                wasIncoming: false,
                wasVideoCall: false,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // unanswered (neither accepted nor declined) outgoing video
                callMode: Calling_1.CallMode.Direct,
                wasDeclined: false,
                wasIncoming: false,
                wasVideoCall: true,
                endedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // ongoing group call
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                creator: {
                    firstName: 'Luigi',
                    isMe: false,
                    title: 'Luigi Mario',
                },
                ended: false,
                deviceCount: 1,
                maxDevices: 16,
                startedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // ongoing group call started by you
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                creator: {
                    firstName: 'Peach',
                    isMe: true,
                    title: 'Princess Peach',
                },
                ended: false,
                deviceCount: 1,
                maxDevices: 16,
                startedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // ongoing group call, creator unknown
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                ended: false,
                deviceCount: 1,
                maxDevices: 16,
                startedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // ongoing and active group call
                callMode: Calling_1.CallMode.Group,
                activeCallConversationId: 'abc123',
                conversationId: 'abc123',
                creator: {
                    firstName: 'Luigi',
                    isMe: false,
                    title: 'Luigi Mario',
                },
                ended: false,
                deviceCount: 1,
                maxDevices: 16,
                startedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // ongoing group call, but you're in another one
                callMode: Calling_1.CallMode.Group,
                activeCallConversationId: 'abc123',
                conversationId: 'xyz987',
                creator: {
                    firstName: 'Luigi',
                    isMe: false,
                    title: 'Luigi Mario',
                },
                ended: false,
                deviceCount: 1,
                maxDevices: 16,
                startedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // ongoing full group call
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                creator: {
                    firstName: 'Luigi',
                    isMe: false,
                    title: 'Luigi Mario',
                },
                ended: false,
                deviceCount: 16,
                maxDevices: 16,
                startedTime: Date.now(),
            },
        },
        {
            type: 'callHistory',
            data: {
                // finished call
                callMode: Calling_1.CallMode.Group,
                conversationId: 'abc123',
                creator: {
                    firstName: 'Luigi',
                    isMe: false,
                    title: 'Luigi Mario',
                },
                ended: true,
                deviceCount: 0,
                maxDevices: 16,
                startedTime: Date.now(),
            },
        },
        {
            type: 'linkNotification',
            data: null,
        },
        {
            type: 'profileChange',
            data: {
                change: {
                    type: 'name',
                    oldName: 'Fred',
                    newName: 'John',
                },
                changedContact: (0, getDefaultConversation_1.getDefaultConversation)(),
            },
        },
        {
            type: 'resetSessionNotification',
            data: null,
        },
        {
            type: 'unsupportedMessage',
            data: {
                canProcessNow: true,
                contact: (0, getDefaultConversation_1.getDefaultConversation)(),
            },
        },
        {
            type: 'unsupportedMessage',
            data: {
                canProcessNow: false,
                contact: (0, getDefaultConversation_1.getDefaultConversation)(),
            },
        },
        {
            type: 'verificationNotification',
            data: {
                type: 'markVerified',
                isLocal: false,
                contact: (0, getDefaultConversation_1.getDefaultConversation)(),
            },
        },
        {
            type: 'verificationNotification',
            data: {
                type: 'markVerified',
                isLocal: true,
                contact: (0, getDefaultConversation_1.getDefaultConversation)(),
            },
        },
        {
            type: 'verificationNotification',
            data: {
                type: 'markNotVerified',
                isLocal: false,
                contact: (0, getDefaultConversation_1.getDefaultConversation)(),
            },
        },
        {
            type: 'verificationNotification',
            data: {
                type: 'markNotVerified',
                isLocal: true,
                contact: (0, getDefaultConversation_1.getDefaultConversation)(),
            },
        },
    ];
    return (React.createElement(React.Fragment, null, items.map((item, index) => (React.createElement(React.Fragment, { key: index },
        React.createElement(TimelineItem_1.TimelineItem, Object.assign({}, getDefaultProps(), { item: item, i18n: i18n })))))));
})
    .add('Unknown Type', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore: intentional
    const item = {
        type: 'random',
        data: {
            somethin: 'somethin',
        },
    };
    return React.createElement(TimelineItem_1.TimelineItem, Object.assign({}, getDefaultProps(), { item: item, i18n: i18n }));
})
    .add('Missing Item', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore: intentional
    const item = null;
    return React.createElement(TimelineItem_1.TimelineItem, Object.assign({}, getDefaultProps(), { item: item, i18n: i18n }));
});
