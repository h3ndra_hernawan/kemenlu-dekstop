"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const Calling_1 = require("../../types/Calling");
const CallingNotification_1 = require("./CallingNotification");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/CallingNotification', module);
const getCommonProps = () => ({
    conversationId: 'fake-conversation-id',
    i18n,
    messageId: 'fake-message-id',
    messageSizeChanged: (0, addon_actions_1.action)('messageSizeChanged'),
    nextItem: undefined,
    returnToActiveCall: (0, addon_actions_1.action)('returnToActiveCall'),
    startCallingLobby: (0, addon_actions_1.action)('startCallingLobby'),
});
[false, true].forEach(wasIncoming => {
    [false, true].forEach(wasVideoCall => {
        [false, true].forEach(wasDeclined => {
            const direction = wasIncoming ? 'incoming' : 'outgoing';
            const type = wasVideoCall ? 'video' : 'audio';
            const acceptance = wasDeclined ? 'declined' : 'accepted';
            const storyName = `Direct call: ${direction} ${type} call, ${acceptance}`;
            story.add(storyName, () => (React.createElement(CallingNotification_1.CallingNotification, Object.assign({}, getCommonProps(), { acceptedTime: wasDeclined ? undefined : 1618894800000, callMode: Calling_1.CallMode.Direct, endedTime: 1618894800000, wasDeclined: wasDeclined, wasIncoming: wasIncoming, wasVideoCall: wasVideoCall }))));
        });
    });
});
story.add('Two incoming direct calls back-to-back', () => {
    const call1 = {
        callMode: Calling_1.CallMode.Direct,
        wasIncoming: true,
        wasVideoCall: true,
        wasDeclined: false,
        acceptedTime: 1618894800000,
        endedTime: 1618894800000,
    };
    const call2 = {
        callMode: Calling_1.CallMode.Direct,
        wasIncoming: true,
        wasVideoCall: false,
        wasDeclined: false,
        endedTime: 1618894800000,
    };
    return (React.createElement(React.Fragment, null,
        React.createElement(CallingNotification_1.CallingNotification, Object.assign({}, getCommonProps(), call1, { nextItem: { type: 'callHistory', data: call2 } })),
        React.createElement(CallingNotification_1.CallingNotification, Object.assign({}, getCommonProps(), call2))));
});
story.add('Two outgoing direct calls back-to-back', () => {
    const call1 = {
        callMode: Calling_1.CallMode.Direct,
        wasIncoming: false,
        wasVideoCall: true,
        wasDeclined: false,
        acceptedTime: 1618894800000,
        endedTime: 1618894800000,
    };
    const call2 = {
        callMode: Calling_1.CallMode.Direct,
        wasIncoming: false,
        wasVideoCall: false,
        wasDeclined: false,
        endedTime: 1618894800000,
    };
    return (React.createElement(React.Fragment, null,
        React.createElement(CallingNotification_1.CallingNotification, Object.assign({}, getCommonProps(), call1, { nextItem: { type: 'callHistory', data: call2 } })),
        React.createElement(CallingNotification_1.CallingNotification, Object.assign({}, getCommonProps(), call2))));
});
[
    undefined,
    { isMe: false, title: 'Alice' },
    { isMe: true, title: 'Alicia' },
].forEach(creator => {
    let startedBy;
    if (!creator) {
        startedBy = 'with unknown creator';
    }
    else if (creator.isMe) {
        startedBy = 'started by you';
    }
    else {
        startedBy = 'started by someone else';
    }
    const storyName = `Group call: active, ${startedBy}`;
    story.add(storyName, () => (React.createElement(CallingNotification_1.CallingNotification, Object.assign({}, getCommonProps(), { callMode: Calling_1.CallMode.Group, creator: creator, deviceCount: 15, ended: false, maxDevices: 16, startedTime: 1618894800000 }))));
});
story.add('Group call: started by someone with a long name', () => {
    const longName = '😤🪐🦆'.repeat(50);
    return (React.createElement(CallingNotification_1.CallingNotification, Object.assign({}, getCommonProps(), { callMode: Calling_1.CallMode.Group, creator: {
            isMe: false,
            title: longName,
        }, deviceCount: 15, ended: false, maxDevices: 16, startedTime: 1618894800000 })));
});
story.add('Group call: active, call full', () => (React.createElement(CallingNotification_1.CallingNotification, Object.assign({}, getCommonProps(), { callMode: Calling_1.CallMode.Group, deviceCount: 16, ended: false, maxDevices: 16, startedTime: 1618894800000 }))));
story.add('Group call: ended', () => (React.createElement(CallingNotification_1.CallingNotification, Object.assign({}, getCommonProps(), { callMode: Calling_1.CallMode.Group, deviceCount: 0, ended: true, maxDevices: 16, startedTime: 1618894800000 }))));
