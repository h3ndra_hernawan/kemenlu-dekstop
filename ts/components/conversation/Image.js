"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Image = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const react_blurhash_1 = require("react-blurhash");
const Spinner_1 = require("../Spinner");
const Attachment_1 = require("../../types/Attachment");
class Image extends react_1.default.Component {
    constructor() {
        super(...arguments);
        this.handleClick = (event) => {
            if (!this.canClick()) {
                event.preventDefault();
                event.stopPropagation();
                return;
            }
            const { onClick, attachment } = this.props;
            if (onClick) {
                event.preventDefault();
                event.stopPropagation();
                onClick(attachment);
            }
        };
        this.handleKeyDown = (event) => {
            if (!this.canClick()) {
                event.preventDefault();
                event.stopPropagation();
                return;
            }
            const { onClick, attachment } = this.props;
            if (onClick && (event.key === 'Enter' || event.key === 'Space')) {
                event.preventDefault();
                event.stopPropagation();
                onClick(attachment);
            }
        };
        this.renderPending = () => {
            const { blurHash, height, i18n, width } = this.props;
            if (blurHash) {
                return (react_1.default.createElement("div", { className: "module-image__download-pending" },
                    react_1.default.createElement(react_blurhash_1.Blurhash, { hash: blurHash, width: width, height: height, style: { display: 'block' } }),
                    react_1.default.createElement("div", { className: "module-image__download-pending--spinner-container" },
                        react_1.default.createElement("div", { className: "module-image__download-pending--spinner", title: i18n('loading') },
                            react_1.default.createElement(Spinner_1.Spinner, { moduleClassName: "module-image-spinner", svgSize: "small" })))));
            }
            return (react_1.default.createElement("div", { className: "module-image__loading-placeholder", style: {
                    height: `${height}px`,
                    width: `${width}px`,
                    lineHeight: `${height}px`,
                    textAlign: 'center',
                }, title: i18n('loading') },
                react_1.default.createElement(Spinner_1.Spinner, { svgSize: "normal" })));
        };
    }
    canClick() {
        const { onClick, attachment } = this.props;
        const { pending } = attachment || { pending: true };
        return Boolean(onClick && !pending);
    }
    render() {
        const { alt, attachment, blurHash, bottomOverlay, className, closeButton, curveBottomLeft, curveBottomRight, curveTopLeft, curveTopRight, darkOverlay, isDownloaded, height = 0, i18n, noBackground, noBorder, onClickClose, onError, overlayText, playIconOverlay, smallCurveTopLeft, softCorners, tabIndex, theme, url, width = 0, cropWidth = 0, cropHeight = 0, } = this.props;
        const { caption, pending } = attachment || { caption: null, pending: true };
        const canClick = this.canClick();
        const imgNotDownloaded = isDownloaded
            ? false
            : (0, Attachment_1.hasNotDownloaded)(attachment);
        const resolvedBlurHash = blurHash || (0, Attachment_1.defaultBlurHash)(theme);
        const overlayClassName = (0, classnames_1.default)('module-image__border-overlay', {
            'module-image__border-overlay--with-border': !noBorder,
            'module-image__border-overlay--with-click-handler': canClick,
            'module-image--curved-top-left': curveTopLeft,
            'module-image--curved-top-right': curveTopRight,
            'module-image--curved-bottom-left': curveBottomLeft,
            'module-image--curved-bottom-right': curveBottomRight,
            'module-image--small-curved-top-left': smallCurveTopLeft,
            'module-image--soft-corners': softCorners,
            'module-image__border-overlay--dark': darkOverlay,
            'module-image--not-downloaded': imgNotDownloaded,
        });
        const overlay = canClick ? (
        // Not sure what this button does.
        // eslint-disable-next-line jsx-a11y/control-has-associated-label
        react_1.default.createElement("button", { type: "button", className: overlayClassName, onClick: this.handleClick, onKeyDown: this.handleKeyDown, tabIndex: tabIndex }, imgNotDownloaded ? react_1.default.createElement("span", null) : null)) : null;
        /* eslint-disable no-nested-ternary */
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-image', className, !noBackground ? 'module-image--with-background' : null, curveBottomLeft ? 'module-image--curved-bottom-left' : null, curveBottomRight ? 'module-image--curved-bottom-right' : null, curveTopLeft ? 'module-image--curved-top-left' : null, curveTopRight ? 'module-image--curved-top-right' : null, smallCurveTopLeft ? 'module-image--small-curved-top-left' : null, softCorners ? 'module-image--soft-corners' : null, cropWidth || cropHeight ? 'module-image--cropped' : null), style: { width: width - cropWidth, height: height - cropHeight } },
            pending ? (this.renderPending()) : url ? (react_1.default.createElement("img", { onError: onError, className: "module-image__image", alt: alt, height: height, width: width, src: url })) : resolvedBlurHash ? (react_1.default.createElement(react_blurhash_1.Blurhash, { hash: resolvedBlurHash, width: width, height: height, style: { display: 'block' } })) : null,
            caption ? (react_1.default.createElement("img", { className: "module-image__caption-icon", src: "images/caption-shadow.svg", alt: i18n('imageCaptionIconAlt') })) : null,
            bottomOverlay ? (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-image__bottom-overlay', curveBottomLeft ? 'module-image--curved-bottom-left' : null, curveBottomRight ? 'module-image--curved-bottom-right' : null) })) : null,
            !pending && !imgNotDownloaded && playIconOverlay ? (react_1.default.createElement("div", { className: "module-image__play-overlay__circle" },
                react_1.default.createElement("div", { className: "module-image__play-overlay__icon" }))) : null,
            overlayText ? (react_1.default.createElement("div", { className: "module-image__text-container", style: { lineHeight: `${height}px` } }, overlayText)) : null,
            overlay,
            closeButton ? (react_1.default.createElement("button", { type: "button", onClick: (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    if (onClickClose) {
                        onClickClose(attachment);
                    }
                }, className: "module-image__close-button", title: i18n('remove-attachment'), "aria-label": i18n('remove-attachment') })) : null));
        /* eslint-enable no-nested-ternary */
    }
}
exports.Image = Image;
