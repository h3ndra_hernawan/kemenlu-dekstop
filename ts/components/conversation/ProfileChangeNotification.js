"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileChangeNotification = void 0;
const react_1 = __importDefault(require("react"));
const SystemMessage_1 = require("./SystemMessage");
const Emojify_1 = require("./Emojify");
const getStringForProfileChange_1 = require("../../util/getStringForProfileChange");
function ProfileChangeNotification(props) {
    const { change, changedContact, i18n } = props;
    const message = (0, getStringForProfileChange_1.getStringForProfileChange)(change, changedContact, i18n);
    return react_1.default.createElement(SystemMessage_1.SystemMessage, { icon: "profile", contents: react_1.default.createElement(Emojify_1.Emojify, { text: message }) });
}
exports.ProfileChangeNotification = ProfileChangeNotification;
