"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const protobuf_1 = require("../../protobuf");
const Colors_1 = require("../../types/Colors");
const EmojiPicker_1 = require("../emoji/EmojiPicker");
const Message_1 = require("./Message");
const MIME_1 = require("../../types/MIME");
const MessageReadStatus_1 = require("../../messages/MessageReadStatus");
const MessageAudio_1 = require("./MessageAudio");
const GlobalAudioContext_1 = require("../GlobalAudioContext");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const Fixtures_1 = require("../../storybook/Fixtures");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const _util_1 = require("../_util");
const fakeAttachment_1 = require("../../test-both/helpers/fakeAttachment");
const getFakeBadge_1 = require("../../test-both/helpers/getFakeBadge");
const Util_1 = require("../../types/Util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
function getJoyReaction() {
    return {
        emoji: '😂',
        from: (0, getDefaultConversation_1.getDefaultConversation)({
            id: '+14155552674',
            phoneNumber: '+14155552674',
            name: 'Amelia Briggs',
            title: 'Amelia',
        }),
        timestamp: Date.now() - 10,
    };
}
const story = (0, react_1.storiesOf)('Components/Conversation/Message', module);
const renderEmojiPicker = ({ onClose, onPickEmoji, ref, }) => (React.createElement(EmojiPicker_1.EmojiPicker, { i18n: (0, setupI18n_1.setupI18n)('en', messages_json_1.default), skinTone: 0, onSetSkinTone: (0, addon_actions_1.action)('EmojiPicker::onSetSkinTone'), ref: ref, onClose: onClose, onPickEmoji: onPickEmoji }));
const renderReactionPicker = () => React.createElement("div", null);
const MessageAudioContainer = props => {
    const [active, setActive] = React.useState({});
    const audio = React.useMemo(() => new Audio(), []);
    return (React.createElement(MessageAudio_1.MessageAudio, Object.assign({}, props, { id: "storybook", renderingContext: "storybook", audio: audio, computePeaks: GlobalAudioContext_1.computePeaks, setActiveAudioID: (id, context) => setActive({ id, context }), onFirstPlayed: (0, addon_actions_1.action)('onFirstPlayed'), activeAudioID: active.id, activeAudioContext: active.context })));
};
const renderAudioAttachment = props => (React.createElement(MessageAudioContainer, Object.assign({}, props)));
const createProps = (overrideProps = {}) => ({
    attachments: overrideProps.attachments,
    author: overrideProps.author || (0, getDefaultConversation_1.getDefaultConversation)(),
    authorBadge: overrideProps.authorBadge,
    reducedMotion: (0, addon_knobs_1.boolean)('reducedMotion', false),
    bodyRanges: overrideProps.bodyRanges,
    canReply: true,
    canDownload: true,
    canDeleteForEveryone: overrideProps.canDeleteForEveryone || false,
    checkForAccount: (0, addon_actions_1.action)('checkForAccount'),
    clearSelectedMessage: (0, addon_actions_1.action)('clearSelectedMessage'),
    collapseMetadata: overrideProps.collapseMetadata,
    containerElementRef: React.createRef(),
    containerWidthBreakpoint: _util_1.WidthBreakpoint.Wide,
    conversationColor: overrideProps.conversationColor ||
        (0, addon_knobs_1.select)('conversationColor', Colors_1.ConversationColors, Colors_1.ConversationColors[0]),
    conversationId: (0, addon_knobs_1.text)('conversationId', overrideProps.conversationId || ''),
    conversationType: overrideProps.conversationType || 'direct',
    deletedForEveryone: overrideProps.deletedForEveryone,
    deleteMessage: (0, addon_actions_1.action)('deleteMessage'),
    deleteMessageForEveryone: (0, addon_actions_1.action)('deleteMessageForEveryone'),
    disableMenu: overrideProps.disableMenu,
    disableScroll: overrideProps.disableScroll,
    direction: overrideProps.direction || 'incoming',
    displayTapToViewMessage: (0, addon_actions_1.action)('displayTapToViewMessage'),
    doubleCheckMissingQuoteReference: (0, addon_actions_1.action)('doubleCheckMissingQuoteReference'),
    downloadAttachment: (0, addon_actions_1.action)('downloadAttachment'),
    expirationLength: (0, addon_knobs_1.number)('expirationLength', overrideProps.expirationLength || 0) ||
        undefined,
    expirationTimestamp: (0, addon_knobs_1.number)('expirationTimestamp', overrideProps.expirationTimestamp || 0) ||
        undefined,
    i18n,
    id: (0, addon_knobs_1.text)('id', overrideProps.id || ''),
    renderingContext: 'storybook',
    interactionMode: overrideProps.interactionMode || 'keyboard',
    isSticker: (0, lodash_1.isBoolean)(overrideProps.isSticker)
        ? overrideProps.isSticker
        : false,
    isBlocked: (0, lodash_1.isBoolean)(overrideProps.isBlocked)
        ? overrideProps.isBlocked
        : false,
    isMessageRequestAccepted: (0, lodash_1.isBoolean)(overrideProps.isMessageRequestAccepted)
        ? overrideProps.isMessageRequestAccepted
        : true,
    isTapToView: overrideProps.isTapToView,
    isTapToViewError: overrideProps.isTapToViewError,
    isTapToViewExpired: overrideProps.isTapToViewExpired,
    kickOffAttachmentDownload: (0, addon_actions_1.action)('kickOffAttachmentDownload'),
    markAttachmentAsCorrupted: (0, addon_actions_1.action)('markAttachmentAsCorrupted'),
    markViewed: (0, addon_actions_1.action)('markViewed'),
    messageExpanded: (0, addon_actions_1.action)('messageExpanded'),
    onHeightChange: (0, addon_actions_1.action)('onHeightChange'),
    openConversation: (0, addon_actions_1.action)('openConversation'),
    openLink: (0, addon_actions_1.action)('openLink'),
    previews: overrideProps.previews || [],
    reactions: overrideProps.reactions,
    reactToMessage: (0, addon_actions_1.action)('reactToMessage'),
    readStatus: overrideProps.readStatus === undefined
        ? MessageReadStatus_1.ReadStatus.Read
        : overrideProps.readStatus,
    renderEmojiPicker,
    renderReactionPicker,
    renderAudioAttachment,
    replyToMessage: (0, addon_actions_1.action)('replyToMessage'),
    retrySend: (0, addon_actions_1.action)('retrySend'),
    scrollToQuotedMessage: (0, addon_actions_1.action)('scrollToQuotedMessage'),
    selectMessage: (0, addon_actions_1.action)('selectMessage'),
    showContactDetail: (0, addon_actions_1.action)('showContactDetail'),
    showContactModal: (0, addon_actions_1.action)('showContactModal'),
    showExpiredIncomingTapToViewToast: (0, addon_actions_1.action)('showExpiredIncomingTapToViewToast'),
    showExpiredOutgoingTapToViewToast: (0, addon_actions_1.action)('showExpiredOutgoingTapToViewToast'),
    showForwardMessageModal: (0, addon_actions_1.action)('showForwardMessageModal'),
    showMessageDetail: (0, addon_actions_1.action)('showMessageDetail'),
    showVisualAttachment: (0, addon_actions_1.action)('showVisualAttachment'),
    status: overrideProps.status || 'sent',
    text: overrideProps.text || (0, addon_knobs_1.text)('text', ''),
    textPending: (0, addon_knobs_1.boolean)('textPending', overrideProps.textPending || false),
    theme: Util_1.ThemeType.light,
    timestamp: (0, addon_knobs_1.number)('timestamp', overrideProps.timestamp || Date.now()),
});
const renderBothDirections = (props) => (React.createElement(React.Fragment, null,
    React.createElement(Message_1.Message, Object.assign({}, props)),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, props, { direction: "outgoing" }))));
story.add('Plain Message', () => {
    const props = createProps({
        text: 'Hello there from a pal! I am sending a long message so that it will wrap a bit, since I like that look.',
    });
    return renderBothDirections(props);
});
story.add('Emoji Messages', () => (React.createElement(React.Fragment, null,
    React.createElement(Message_1.Message, Object.assign({}, createProps({ text: '😀' }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({ text: '😀😀' }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({ text: '😀😀😀' }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({ text: '😀😀😀😀' }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({ text: '😀😀😀😀😀' }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({ text: '😀😀😀😀😀😀😀' }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({
        previews: [
            {
                domain: 'signal.org',
                image: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_PNG,
                    fileName: 'the-sax.png',
                    height: 240,
                    url: Fixtures_1.pngUrl,
                    width: 320,
                }),
                isStickerPack: false,
                title: 'Signal',
                description: 'Say "hello" to a different messaging experience. An unexpected focus on privacy, combined with all of the features you expect.',
                url: 'https://www.signal.org',
                date: new Date(2020, 2, 10).valueOf(),
            },
        ],
        text: '😀',
    }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                fileName: 'tina-rolf-269345-unsplash.jpg',
                contentType: MIME_1.IMAGE_JPEG,
                width: 128,
                height: 128,
            }),
        ],
        text: '😀',
    }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.AUDIO_MP3,
                fileName: 'incompetech-com-Agnus-Dei-X.mp3',
                url: '/fixtures/incompetech-com-Agnus-Dei-X.mp3',
            }),
        ],
        text: '😀',
    }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: (0, MIME_1.stringToMIMEType)('text/plain'),
                fileName: 'my-resume.txt',
                url: 'my-resume.txt',
            }),
        ],
        text: '😀',
    }))),
    React.createElement("br", null),
    React.createElement(Message_1.Message, Object.assign({}, createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.GIF,
                fileName: 'cat-gif.mp4',
                url: '/fixtures/cat-gif.mp4',
                width: 400,
                height: 332,
            }),
        ],
        text: '😀',
    }))))));
story.add('Delivered', () => {
    const props = createProps({
        direction: 'outgoing',
        status: 'delivered',
        text: 'Hello there from a pal! I am sending a long message so that it will wrap a bit, since I like that look.',
    });
    return React.createElement(Message_1.Message, Object.assign({}, props));
});
story.add('Read', () => {
    const props = createProps({
        direction: 'outgoing',
        status: 'read',
        text: 'Hello there from a pal! I am sending a long message so that it will wrap a bit, since I like that look.',
    });
    return React.createElement(Message_1.Message, Object.assign({}, props));
});
story.add('Sending', () => {
    const props = createProps({
        direction: 'outgoing',
        status: 'sending',
        text: 'Hello there from a pal! I am sending a long message so that it will wrap a bit, since I like that look.',
    });
    return React.createElement(Message_1.Message, Object.assign({}, props));
});
story.add('Expiring', () => {
    const props = createProps({
        expirationLength: 30 * 1000,
        expirationTimestamp: Date.now() + 30 * 1000,
        text: 'Hello there from a pal! I am sending a long message so that it will wrap a bit, since I like that look.',
    });
    return renderBothDirections(props);
});
story.add('Pending', () => {
    const props = createProps({
        text: 'Hello there from a pal! I am sending a long message so that it will wrap a bit, since I like that look.',
        textPending: true,
    });
    return renderBothDirections(props);
});
story.add('Collapsed Metadata', () => {
    const props = createProps({
        author: (0, getDefaultConversation_1.getDefaultConversation)({ title: 'Fred Willard' }),
        collapseMetadata: true,
        conversationType: 'group',
        text: 'Hello there from a pal!',
    });
    return renderBothDirections(props);
});
story.add('Recent', () => {
    const props = createProps({
        text: 'Hello there from a pal!',
        timestamp: Date.now() - 30 * 60 * 1000,
    });
    return renderBothDirections(props);
});
story.add('Older', () => {
    const props = createProps({
        text: 'Hello there from a pal!',
        timestamp: Date.now() - 180 * 24 * 60 * 60 * 1000,
    });
    return renderBothDirections(props);
});
story.add('Reactions (wider message)', () => {
    const props = createProps({
        text: 'Hello there from a pal!',
        timestamp: Date.now() - 180 * 24 * 60 * 60 * 1000,
        reactions: [
            {
                emoji: '👍',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    isMe: true,
                    id: '+14155552672',
                    phoneNumber: '+14155552672',
                    name: 'Me',
                    title: 'Me',
                }),
                timestamp: Date.now() - 10,
            },
            {
                emoji: '👍',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552672',
                    phoneNumber: '+14155552672',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now() - 10,
            },
            {
                emoji: '👍',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552673',
                    phoneNumber: '+14155552673',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now() - 10,
            },
            {
                emoji: '😂',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552674',
                    phoneNumber: '+14155552674',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now() - 10,
            },
            {
                emoji: '😡',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552677',
                    phoneNumber: '+14155552677',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now() - 10,
            },
            {
                emoji: '👎',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552678',
                    phoneNumber: '+14155552678',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now() - 10,
            },
            {
                emoji: '❤️',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552679',
                    phoneNumber: '+14155552679',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now() - 10,
            },
        ],
    });
    return renderBothDirections(props);
});
const joyReactions = Array.from({ length: 52 }, () => getJoyReaction());
story.add('Reactions (short message)', () => {
    const props = createProps({
        text: 'h',
        timestamp: Date.now(),
        reactions: [
            ...joyReactions,
            {
                emoji: '👍',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    isMe: true,
                    id: '+14155552672',
                    phoneNumber: '+14155552672',
                    name: 'Me',
                    title: 'Me',
                }),
                timestamp: Date.now(),
            },
            {
                emoji: '👍',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552672',
                    phoneNumber: '+14155552672',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now(),
            },
            {
                emoji: '👍',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552673',
                    phoneNumber: '+14155552673',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now(),
            },
            {
                emoji: '😡',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552677',
                    phoneNumber: '+14155552677',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now(),
            },
            {
                emoji: '👎',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552678',
                    phoneNumber: '+14155552678',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now(),
            },
            {
                emoji: '❤️',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552679',
                    phoneNumber: '+14155552679',
                    name: 'Amelia Briggs',
                    title: 'Amelia',
                }),
                timestamp: Date.now(),
            },
        ],
    });
    return renderBothDirections(props);
});
story.add('Avatar in Group', () => {
    const props = createProps({
        author: (0, getDefaultConversation_1.getDefaultConversation)({ avatarPath: Fixtures_1.pngUrl }),
        conversationType: 'group',
        status: 'sent',
        text: 'Hello it is me, the saxophone.',
    });
    return React.createElement(Message_1.Message, Object.assign({}, props));
});
story.add('Badge in Group', () => {
    const props = createProps({
        authorBadge: (0, getFakeBadge_1.getFakeBadge)(),
        conversationType: 'group',
        status: 'sent',
        text: 'Hello it is me, the saxophone.',
    });
    return React.createElement(Message_1.Message, Object.assign({}, props));
});
story.add('Sticker', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                url: '/fixtures/512x515-thumbs-up-lincoln.webp',
                fileName: '512x515-thumbs-up-lincoln.webp',
                contentType: MIME_1.IMAGE_WEBP,
                width: 128,
                height: 128,
            }),
        ],
        isSticker: true,
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Deleted', () => {
    const props = createProps({
        conversationType: 'group',
        deletedForEveryone: true,
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Deleted with expireTimer', () => {
    const props = createProps({
        timestamp: Date.now() - 60 * 1000,
        conversationType: 'group',
        deletedForEveryone: true,
        expirationLength: 5 * 60 * 1000,
        expirationTimestamp: Date.now() + 3 * 60 * 1000,
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Can delete for everyone', () => {
    const props = createProps({
        status: 'read',
        text: 'I hope you get this.',
        canDeleteForEveryone: true,
    });
    return React.createElement(Message_1.Message, Object.assign({}, props, { direction: "outgoing" }));
});
story.add('Error', () => {
    const props = createProps({
        status: 'error',
        text: 'I hope you get this.',
    });
    return renderBothDirections(props);
});
story.add('Paused', () => {
    const props = createProps({
        status: 'paused',
        text: 'I am up to a challenge',
    });
    return renderBothDirections(props);
});
story.add('Partial Send', () => {
    const props = createProps({
        status: 'partial-sent',
        text: 'I hope you get this.',
    });
    return renderBothDirections(props);
});
story.add('Link Preview', () => {
    const props = createProps({
        previews: [
            {
                domain: 'signal.org',
                image: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_PNG,
                    fileName: 'the-sax.png',
                    height: 240,
                    url: Fixtures_1.pngUrl,
                    width: 320,
                }),
                isStickerPack: false,
                title: 'Signal',
                description: 'Say "hello" to a different messaging experience. An unexpected focus on privacy, combined with all of the features you expect.',
                url: 'https://www.signal.org',
                date: new Date(2020, 2, 10).valueOf(),
            },
        ],
        status: 'sent',
        text: 'Be sure to look at https://www.signal.org',
    });
    return renderBothDirections(props);
});
story.add('Link Preview with Small Image', () => {
    const props = createProps({
        previews: [
            {
                domain: 'signal.org',
                image: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_PNG,
                    fileName: 'the-sax.png',
                    height: 50,
                    url: Fixtures_1.pngUrl,
                    width: 50,
                }),
                isStickerPack: false,
                title: 'Signal',
                description: 'Say "hello" to a different messaging experience. An unexpected focus on privacy, combined with all of the features you expect.',
                url: 'https://www.signal.org',
                date: new Date(2020, 2, 10).valueOf(),
            },
        ],
        status: 'sent',
        text: 'Be sure to look at https://www.signal.org',
    });
    return renderBothDirections(props);
});
story.add('Link Preview without Image', () => {
    const props = createProps({
        previews: [
            {
                domain: 'signal.org',
                isStickerPack: false,
                title: 'Signal',
                description: 'Say "hello" to a different messaging experience. An unexpected focus on privacy, combined with all of the features you expect.',
                url: 'https://www.signal.org',
                date: new Date(2020, 2, 10).valueOf(),
            },
        ],
        status: 'sent',
        text: 'Be sure to look at https://www.signal.org',
    });
    return renderBothDirections(props);
});
story.add('Link Preview with no description', () => {
    const props = createProps({
        previews: [
            {
                domain: 'signal.org',
                isStickerPack: false,
                title: 'Signal',
                url: 'https://www.signal.org',
                date: Date.now(),
            },
        ],
        status: 'sent',
        text: 'Be sure to look at https://www.signal.org',
    });
    return renderBothDirections(props);
});
story.add('Link Preview with long description', () => {
    const props = createProps({
        previews: [
            {
                domain: 'signal.org',
                isStickerPack: false,
                title: 'Signal',
                description: Array(10)
                    .fill('Say "hello" to a different messaging experience. An unexpected focus on privacy, combined with all of the features you expect.')
                    .join(' '),
                url: 'https://www.signal.org',
                date: Date.now(),
            },
        ],
        status: 'sent',
        text: 'Be sure to look at https://www.signal.org',
    });
    return renderBothDirections(props);
});
story.add('Link Preview with small image, long description', () => {
    const props = createProps({
        previews: [
            {
                domain: 'signal.org',
                image: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_PNG,
                    fileName: 'the-sax.png',
                    height: 50,
                    url: Fixtures_1.pngUrl,
                    width: 50,
                }),
                isStickerPack: false,
                title: 'Signal',
                description: Array(10)
                    .fill('Say "hello" to a different messaging experience. An unexpected focus on privacy, combined with all of the features you expect.')
                    .join(' '),
                url: 'https://www.signal.org',
                date: Date.now(),
            },
        ],
        status: 'sent',
        text: 'Be sure to look at https://www.signal.org',
    });
    return renderBothDirections(props);
});
story.add('Link Preview with no date', () => {
    const props = createProps({
        previews: [
            {
                domain: 'signal.org',
                image: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_PNG,
                    fileName: 'the-sax.png',
                    height: 240,
                    url: Fixtures_1.pngUrl,
                    width: 320,
                }),
                isStickerPack: false,
                title: 'Signal',
                description: 'Say "hello" to a different messaging experience. An unexpected focus on privacy, combined with all of the features you expect.',
                url: 'https://www.signal.org',
            },
        ],
        status: 'sent',
        text: 'Be sure to look at https://www.signal.org',
    });
    return renderBothDirections(props);
});
story.add('Link Preview with too new a date', () => {
    const props = createProps({
        previews: [
            {
                domain: 'signal.org',
                image: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_PNG,
                    fileName: 'the-sax.png',
                    height: 240,
                    url: Fixtures_1.pngUrl,
                    width: 320,
                }),
                isStickerPack: false,
                title: 'Signal',
                description: 'Say "hello" to a different messaging experience. An unexpected focus on privacy, combined with all of the features you expect.',
                url: 'https://www.signal.org',
                date: Date.now() + 3000000000,
            },
        ],
        status: 'sent',
        text: 'Be sure to look at https://www.signal.org',
    });
    return renderBothDirections(props);
});
story.add('Image', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                fileName: 'tina-rolf-269345-unsplash.jpg',
                contentType: MIME_1.IMAGE_JPEG,
                width: 128,
                height: 128,
            }),
        ],
        status: 'sent',
    });
    return renderBothDirections(props);
});
for (let i = 2; i <= 5; i += 1) {
    story.add(`Multiple Images x${i}`, () => {
        const props = createProps({
            attachments: [
                (0, fakeAttachment_1.fakeAttachment)({
                    url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                    fileName: 'tina-rolf-269345-unsplash.jpg',
                    contentType: MIME_1.IMAGE_JPEG,
                    width: 128,
                    height: 128,
                }),
                (0, fakeAttachment_1.fakeAttachment)({
                    url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                    fileName: 'tina-rolf-269345-unsplash.jpg',
                    contentType: MIME_1.IMAGE_JPEG,
                    width: 128,
                    height: 128,
                }),
                (0, fakeAttachment_1.fakeAttachment)({
                    url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                    fileName: 'tina-rolf-269345-unsplash.jpg',
                    contentType: MIME_1.IMAGE_JPEG,
                    width: 128,
                    height: 128,
                }),
                (0, fakeAttachment_1.fakeAttachment)({
                    url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                    fileName: 'tina-rolf-269345-unsplash.jpg',
                    contentType: MIME_1.IMAGE_JPEG,
                    width: 128,
                    height: 128,
                }),
                (0, fakeAttachment_1.fakeAttachment)({
                    url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                    fileName: 'tina-rolf-269345-unsplash.jpg',
                    contentType: MIME_1.IMAGE_JPEG,
                    width: 128,
                    height: 128,
                }),
            ].slice(0, i),
            status: 'sent',
        });
        return renderBothDirections(props);
    });
}
story.add('Image with Caption', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                fileName: 'tina-rolf-269345-unsplash.jpg',
                contentType: MIME_1.IMAGE_JPEG,
                width: 128,
                height: 128,
            }),
        ],
        status: 'sent',
        text: 'This is my home.',
    });
    return renderBothDirections(props);
});
story.add('GIF', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.GIF,
                fileName: 'cat-gif.mp4',
                url: '/fixtures/cat-gif.mp4',
                width: 400,
                height: 332,
            }),
        ],
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('GIF in a group', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.GIF,
                fileName: 'cat-gif.mp4',
                url: '/fixtures/cat-gif.mp4',
                width: 400,
                height: 332,
            }),
        ],
        conversationType: 'group',
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Not Downloaded GIF', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.GIF,
                fileName: 'cat-gif.mp4',
                fileSize: '188.61 KB',
                blurHash: 'LDA,FDBnm+I=p{tkIUI;~UkpELV]',
                width: 400,
                height: 332,
            }),
        ],
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Pending GIF', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                pending: true,
                contentType: MIME_1.VIDEO_MP4,
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.GIF,
                fileName: 'cat-gif.mp4',
                fileSize: '188.61 KB',
                blurHash: 'LDA,FDBnm+I=p{tkIUI;~UkpELV]',
                width: 400,
                height: 332,
            }),
        ],
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Audio', () => {
    const Wrapper = () => {
        const [isPlayed, setIsPlayed] = React.useState(false);
        const messageProps = createProps(Object.assign({ attachments: [
                (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.AUDIO_MP3,
                    fileName: 'incompetech-com-Agnus-Dei-X.mp3',
                    url: '/fixtures/incompetech-com-Agnus-Dei-X.mp3',
                }),
            ] }, (isPlayed
            ? {
                status: 'viewed',
                readStatus: MessageReadStatus_1.ReadStatus.Viewed,
            }
            : {
                status: 'read',
                readStatus: MessageReadStatus_1.ReadStatus.Read,
            })));
        return (React.createElement(React.Fragment, null,
            React.createElement("button", { type: "button", onClick: () => {
                    setIsPlayed(old => !old);
                }, style: {
                    display: 'block',
                    marginBottom: '2em',
                } }, "Toggle played"),
            renderBothDirections(messageProps)));
    };
    return React.createElement(Wrapper, null);
});
story.add('Long Audio', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.AUDIO_MP3,
                fileName: 'long-audio.mp3',
                url: '/fixtures/long-audio.mp3',
            }),
        ],
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Audio with Caption', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.AUDIO_MP3,
                fileName: 'incompetech-com-Agnus-Dei-X.mp3',
                url: '/fixtures/incompetech-com-Agnus-Dei-X.mp3',
            }),
        ],
        status: 'sent',
        text: 'This is what I sound like.',
    });
    return renderBothDirections(props);
});
story.add('Audio with Not Downloaded Attachment', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.AUDIO_MP3,
                fileName: 'incompetech-com-Agnus-Dei-X.mp3',
            }),
        ],
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Audio with Pending Attachment', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.AUDIO_MP3,
                fileName: 'incompetech-com-Agnus-Dei-X.mp3',
                pending: true,
            }),
        ],
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Other File Type', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: (0, MIME_1.stringToMIMEType)('text/plain'),
                fileName: 'my-resume.txt',
                url: 'my-resume.txt',
            }),
        ],
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Other File Type with Caption', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: (0, MIME_1.stringToMIMEType)('text/plain'),
                fileName: 'my-resume.txt',
                url: 'my-resume.txt',
            }),
        ],
        status: 'sent',
        text: 'This is what I have done.',
    });
    return renderBothDirections(props);
});
story.add('Other File Type with Long Filename', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: (0, MIME_1.stringToMIMEType)('text/plain'),
                fileName: 'INSERT-APP-NAME_INSERT-APP-APPLE-ID_AppStore_AppsGamesWatch.psd.zip',
                url: 'a2/a2334324darewer4234',
            }),
        ],
        status: 'sent',
        text: 'This is what I have done.',
    });
    return renderBothDirections(props);
});
story.add('TapToView Image', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                fileName: 'tina-rolf-269345-unsplash.jpg',
                contentType: MIME_1.IMAGE_JPEG,
                width: 128,
                height: 128,
            }),
        ],
        isTapToView: true,
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('TapToView Video', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                fileName: 'pixabay-Soap-Bubble-7141.mp4',
                height: 128,
                url: '/fixtures/pixabay-Soap-Bubble-7141.mp4',
                width: 128,
            }),
        ],
        isTapToView: true,
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('TapToView GIF', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                flags: protobuf_1.SignalService.AttachmentPointer.Flags.GIF,
                fileName: 'cat-gif.mp4',
                url: '/fixtures/cat-gif.mp4',
                width: 400,
                height: 332,
            }),
        ],
        isTapToView: true,
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('TapToView Expired', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                fileName: 'tina-rolf-269345-unsplash.jpg',
                contentType: MIME_1.IMAGE_JPEG,
                width: 128,
                height: 128,
            }),
        ],
        isTapToView: true,
        isTapToViewExpired: true,
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('TapToView Error', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                fileName: 'tina-rolf-269345-unsplash.jpg',
                contentType: MIME_1.IMAGE_JPEG,
                width: 128,
                height: 128,
            }),
        ],
        isTapToView: true,
        isTapToViewError: true,
        status: 'sent',
    });
    return React.createElement(Message_1.Message, Object.assign({}, props));
});
story.add('Dangerous File Type', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: (0, MIME_1.stringToMIMEType)('application/vnd.microsoft.portable-executable'),
                fileName: 'terrible.exe',
                url: 'terrible.exe',
            }),
        ],
        status: 'sent',
    });
    return renderBothDirections(props);
});
story.add('Colors', () => {
    return (React.createElement(React.Fragment, null, Colors_1.ConversationColors.map(color => (React.createElement("div", { key: color }, renderBothDirections(createProps({
        conversationColor: color,
        text: `Here is a preview of the chat color: ${color}. The color is visible to only you.`,
    })))))));
});
story.add('@Mentions', () => {
    const props = createProps({
        bodyRanges: [
            {
                start: 0,
                length: 1,
                mentionUuid: 'zap',
                replacementText: 'Zapp Brannigan',
            },
        ],
        text: '\uFFFC This Is It. The Moment We Should Have Trained For.',
    });
    return renderBothDirections(props);
});
story.add('All the context menus', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                fileName: 'tina-rolf-269345-unsplash.jpg',
                contentType: MIME_1.IMAGE_JPEG,
                width: 128,
                height: 128,
            }),
        ],
        status: 'partial-sent',
        canDeleteForEveryone: true,
    });
    return React.createElement(Message_1.Message, Object.assign({}, props, { direction: "outgoing" }));
});
story.add('Not approved, with link preview', () => {
    const props = createProps({
        previews: [
            {
                domain: 'signal.org',
                image: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_PNG,
                    fileName: 'the-sax.png',
                    height: 240,
                    url: Fixtures_1.pngUrl,
                    width: 320,
                }),
                isStickerPack: false,
                title: 'Signal',
                description: 'Say "hello" to a different messaging experience. An unexpected focus on privacy, combined with all of the features you expect.',
                url: 'https://www.signal.org',
                date: new Date(2020, 2, 10).valueOf(),
            },
        ],
        status: 'sent',
        text: 'Be sure to look at https://www.signal.org',
        isMessageRequestAccepted: false,
    });
    return renderBothDirections(props);
});
story.add('Custom Color', () => (React.createElement(React.Fragment, null,
    React.createElement(Message_1.Message, Object.assign({}, createProps({ text: 'Solid.' }), { direction: "outgoing", customColor: {
            start: { hue: 82, saturation: 35 },
        } })),
    React.createElement("br", { style: { clear: 'both' } }),
    React.createElement(Message_1.Message, Object.assign({}, createProps({ text: 'Gradient.' }), { direction: "outgoing", customColor: {
            deg: 192,
            start: { hue: 304, saturation: 85 },
            end: { hue: 231, saturation: 76 },
        } })))));
