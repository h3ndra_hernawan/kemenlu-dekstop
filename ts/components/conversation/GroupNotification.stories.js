"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const GroupNotification_1 = require("./GroupNotification");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const book = (0, react_1.storiesOf)('Components/Conversation', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const longName = '🍷🐓🥶'.repeat(50);
const stories = [
    [
        'Combo',
        [
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'add',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mrs. Ice',
                                phoneNumber: '(202) 555-1001',
                            }),
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                phoneNumber: '(202) 555-1002',
                                title: 'Ms. Earth',
                            }),
                        ],
                    },
                    { type: 'name', newName: 'Fishing Stories' },
                    { type: 'avatar' },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                    isMe: true,
                }),
                changes: [
                    {
                        type: 'add',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mrs. Ice',
                                phoneNumber: '(202) 555-1001',
                            }),
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Ms. Earth',
                                phoneNumber: '(202) 555-1002',
                            }),
                        ],
                    },
                    { type: 'name', newName: 'Fishing Stories' },
                    { type: 'avatar' },
                ],
                i18n,
            },
        ],
    ],
    [
        'Joined group',
        [
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'add',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: '(202) 555-1000',
                                phoneNumber: '(202) 555-1000',
                            }),
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mrs. Ice',
                                phoneNumber: '(202) 555-1001',
                            }),
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Ms. Earth',
                                phoneNumber: '(202) 555-1002',
                            }),
                        ],
                    },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'add',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: '(202) 555-1000',
                                phoneNumber: '(202) 555-1000',
                                isMe: true,
                            }),
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mrs. Ice',
                                phoneNumber: '(202) 555-1001',
                            }),
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Ms. Earth',
                                phoneNumber: '(202) 555-1002',
                            }),
                        ],
                    },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'add',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mr. Fire',
                                phoneNumber: '(202) 555-1000',
                            }),
                        ],
                    },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                    isMe: true,
                }),
                changes: [
                    {
                        type: 'add',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mr. Fire',
                                phoneNumber: '(202) 555-1000',
                            }),
                        ],
                    },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'add',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mr. Fire',
                                phoneNumber: '(202) 555-1000',
                                isMe: true,
                            }),
                        ],
                    },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'add',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mr. Fire',
                                phoneNumber: '(202) 555-1000',
                                isMe: true,
                            }),
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mrs. Ice',
                                phoneNumber: '(202) 555-1001',
                            }),
                        ],
                    },
                ],
                i18n,
            },
        ],
    ],
    [
        'Left group',
        [
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'remove',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mr. Fire',
                                phoneNumber: '(202) 555-1000',
                            }),
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mrs. Ice',
                                phoneNumber: '(202) 555-1001',
                            }),
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Ms. Earth',
                                phoneNumber: '(202) 555-1002',
                            }),
                        ],
                    },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'remove',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Mr. Fire',
                                phoneNumber: '(202) 555-1000',
                            }),
                        ],
                    },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                    isMe: true,
                }),
                changes: [
                    {
                        type: 'remove',
                        contacts: [
                            (0, getDefaultConversation_1.getDefaultConversation)({
                                title: 'Alice',
                                phoneNumber: '(202) 555-1000',
                                isMe: true,
                            }),
                        ],
                    },
                ],
                i18n,
            },
        ],
    ],
    [
        'Title changed',
        [
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'name',
                        newName: 'New Group Name',
                    },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                    isMe: true,
                }),
                changes: [
                    {
                        type: 'name',
                        newName: 'New Group Name',
                    },
                ],
                i18n,
            },
        ],
    ],
    [
        'Avatar changed',
        [
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'avatar',
                        newName: 'New Group Name',
                    },
                ],
                i18n,
            },
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                    isMe: true,
                }),
                changes: [
                    {
                        type: 'avatar',
                        newName: 'New Group Name',
                    },
                ],
                i18n,
            },
        ],
    ],
    [
        'Generic group update',
        [
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: 'Alice',
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'general',
                    },
                ],
                i18n,
            },
        ],
    ],
    [
        'Long name',
        [
            {
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    title: longName,
                    phoneNumber: '(202) 555-1000',
                }),
                changes: [
                    {
                        type: 'general',
                    },
                ],
                i18n,
            },
        ],
    ],
];
book.add('GroupNotification', () => stories.map(([title, propsArray]) => (React.createElement(React.Fragment, null,
    React.createElement("h3", null, title),
    propsArray.map((props, i) => {
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { key: i, className: "module-message-container" },
                React.createElement("div", { className: "module-inline-notification-wrapper" },
                    React.createElement(GroupNotification_1.GroupNotification, Object.assign({}, props))))));
    })))));
