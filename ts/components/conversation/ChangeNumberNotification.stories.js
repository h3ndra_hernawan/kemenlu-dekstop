"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const ChangeNumberNotification_1 = require("./ChangeNumberNotification");
const story = (0, react_1.storiesOf)('Components/Conversation/ChangeNumberNotification', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
story.add('Default', () => (React.createElement(ChangeNumberNotification_1.ChangeNumberNotification, { sender: (0, getDefaultConversation_1.getDefaultConversation)(), timestamp: 1618894800000, i18n: i18n })));
story.add('Long name', () => (React.createElement(ChangeNumberNotification_1.ChangeNumberNotification, { sender: (0, getDefaultConversation_1.getDefaultConversation)({
        firstName: '💅😇🖋'.repeat(50),
    }), timestamp: 1618894800000, i18n: i18n })));
