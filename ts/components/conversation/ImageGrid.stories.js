"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const ImageGrid_1 = require("./ImageGrid");
const MIME_1 = require("../../types/MIME");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const Fixtures_1 = require("../../storybook/Fixtures");
const fakeAttachment_1 = require("../../test-both/helpers/fakeAttachment");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ImageGrid', module);
const createProps = (overrideProps = {}) => ({
    attachments: overrideProps.attachments || [
        (0, fakeAttachment_1.fakeAttachment)({
            contentType: MIME_1.IMAGE_PNG,
            fileName: 'sax.png',
            height: 1200,
            url: Fixtures_1.pngUrl,
            width: 800,
        }),
    ],
    bottomOverlay: (0, addon_knobs_1.boolean)('bottomOverlay', overrideProps.bottomOverlay || false),
    i18n,
    isSticker: (0, addon_knobs_1.boolean)('isSticker', overrideProps.isSticker || false),
    onClick: (0, addon_actions_1.action)('onClick'),
    onError: (0, addon_actions_1.action)('onError'),
    stickerSize: (0, addon_knobs_1.number)('stickerSize', overrideProps.stickerSize || 0),
    tabIndex: (0, addon_knobs_1.number)('tabIndex', overrideProps.tabIndex || 0),
    withContentAbove: (0, addon_knobs_1.boolean)('withContentAbove', overrideProps.withContentAbove || false),
    withContentBelow: (0, addon_knobs_1.boolean)('withContentBelow', overrideProps.withContentBelow || false),
});
story.add('One Image', () => {
    const props = createProps();
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
story.add('Two Images', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                height: 1680,
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                width: 3000,
            }),
        ],
    });
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
story.add('Three Images', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                height: 1680,
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                width: 3000,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
        ],
    });
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
story.add('Four Images', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                height: 1680,
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                width: 3000,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                height: 1680,
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                width: 3000,
            }),
        ],
    });
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
story.add('Five Images', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                height: 1680,
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                width: 3000,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                height: 1680,
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                width: 3000,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
        ],
    });
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
story.add('6+ Images', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                height: 1680,
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                width: 3000,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                height: 1680,
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
                width: 3000,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
        ],
    });
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
story.add('Mixed Content Types', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                fileName: 'pixabay-Soap-Bubble-7141.mp4',
                height: 112,
                screenshot: {
                    height: 112,
                    width: 112,
                    url: '/fixtures/kitten-4-112-112.jpg',
                    contentType: MIME_1.IMAGE_JPEG,
                    path: 'originalpath',
                },
                url: '/fixtures/pixabay-Soap-Bubble-7141.mp4',
                width: 112,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_PNG,
                fileName: 'sax.png',
                height: 1200,
                url: Fixtures_1.pngUrl,
                width: 800,
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: (0, MIME_1.stringToMIMEType)('text/plain'),
                fileName: 'lorem-ipsum.txt',
                url: '/fixtures/lorem-ipsum.txt',
            }),
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.AUDIO_MP3,
                fileName: 'incompetech-com-Agnus-Dei-X.mp3',
                url: '/fixtures/incompetech-com-Agnus-Dei-X.mp3',
            }),
        ],
    });
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
story.add('Sticker', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeAttachment)({
                contentType: MIME_1.IMAGE_WEBP,
                fileName: 'sticker.webp',
                height: 512,
                url: Fixtures_1.squareStickerUrl,
                width: 512,
            }),
        ],
        isSticker: true,
        stickerSize: 128,
    });
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
story.add('Content Above and Below', () => {
    const props = createProps({
        withContentAbove: true,
        withContentBelow: true,
    });
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
story.add('Bottom Overlay', () => {
    const props = createProps({
        bottomOverlay: true,
    });
    return React.createElement(ImageGrid_1.ImageGrid, Object.assign({}, props));
});
