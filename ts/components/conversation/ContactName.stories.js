"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const ContactName_1 = require("./ContactName");
const Colors_1 = require("../../types/Colors");
(0, react_1.storiesOf)('Components/Conversation/ContactName', module)
    .add('First name and title; title preferred', () => (React.createElement(ContactName_1.ContactName, { firstName: "Ignored", title: "Someone \uD83D\uDD25 Somewhere" })))
    .add('First name and title; first name preferred', () => (React.createElement(ContactName_1.ContactName, { firstName: "Someone \uD83D\uDD25 Somewhere", title: "Ignored", preferFirstName: true })))
    .add('Colors', () => {
    return Colors_1.ContactNameColors.map(color => (React.createElement("div", { key: color },
        React.createElement(ContactName_1.ContactName, { title: `Hello ${color}`, contactNameColor: color }))));
});
