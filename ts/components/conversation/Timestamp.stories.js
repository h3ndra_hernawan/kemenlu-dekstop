"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const Timestamp_1 = require("./Timestamp");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/Timestamp', module);
const { now } = Date;
const seconds = (n) => n * 1000;
const minutes = (n) => 60 * seconds(n);
const hours = (n) => 60 * minutes(n);
const days = (n) => 24 * hours(n);
const get1201 = () => {
    const d = new Date();
    d.setHours(0, 1, 0, 0);
    return d.getTime();
};
const getJanuary1201 = () => {
    const d = new Date();
    d.setHours(0, 1, 0, 0);
    d.setMonth(0);
    d.setDate(1);
    return d.getTime();
};
const times = () => [
    ['500ms ago', now() - seconds(0.5)],
    ['30s ago', now() - seconds(30)],
    ['1m ago', now() - minutes(1)],
    ['30m ago', now() - minutes(30)],
    ['45m ago', now() - minutes(45)],
    ['1h ago', now() - hours(1)],
    ['12:01am today', get1201()],
    ['11:59pm yesterday', get1201() - minutes(2)],
    ['24h ago', now() - hours(24)],
    ['2d ago', now() - days(2)],
    ['7d ago', now() - days(7)],
    ['30d ago', now() - days(30)],
    ['January 1st this year, 12:01am ', getJanuary1201()],
    ['December 31st last year, 11:59pm', getJanuary1201() - minutes(2)],
    ['366d ago', now() - days(366)],
];
const createProps = (overrideProps = {}) => ({
    i18n,
    timestamp: overrideProps.timestamp,
    extended: (0, addon_knobs_1.boolean)('extended', overrideProps.extended || false),
    module: (0, addon_knobs_1.text)('module', ''),
    withImageNoCaption: (0, addon_knobs_1.boolean)('withImageNoCaption', false),
    withSticker: (0, addon_knobs_1.boolean)('withSticker', false),
    withTapToViewExpired: (0, addon_knobs_1.boolean)('withTapToViewExpired', false),
    direction: (0, addon_knobs_1.select)('direction', { none: '', incoming: 'incoming', outgoing: 'outgoing' }, '') || undefined,
});
const createTable = (overrideProps = {}) => (React.createElement("table", { cellPadding: 5 },
    React.createElement("tbody", null,
        React.createElement("tr", null,
            React.createElement("th", null, "Description"),
            React.createElement("th", null, "Timestamp")),
        times().map(([description, timestamp]) => (React.createElement("tr", { key: timestamp },
            React.createElement("td", null, description),
            React.createElement("td", null,
                React.createElement(Timestamp_1.Timestamp, Object.assign({ key: timestamp }, createProps(Object.assign(Object.assign({}, overrideProps), { timestamp })))))))))));
story.add('Normal', () => {
    return createTable();
});
story.add('Extended', () => {
    return createTable({ extended: true });
});
story.add('Knobs', () => {
    const props = createProps({
        timestamp: (0, addon_knobs_1.date)('timestamp', new Date()),
    });
    return React.createElement(Timestamp_1.Timestamp, Object.assign({}, props));
});
