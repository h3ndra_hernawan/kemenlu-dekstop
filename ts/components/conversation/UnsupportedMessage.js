"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnsupportedMessage = void 0;
const react_1 = __importDefault(require("react"));
const SystemMessage_1 = require("./SystemMessage");
const Button_1 = require("../Button");
const ContactName_1 = require("./ContactName");
const Intl_1 = require("../Intl");
const UnsupportedMessage = ({ canProcessNow, contact, i18n, downloadNewVersion, }) => {
    const { isMe } = contact;
    const otherStringId = canProcessNow
        ? 'Message--unsupported-message-ask-to-resend'
        : 'Message--unsupported-message';
    const meStringId = canProcessNow
        ? 'Message--from-me-unsupported-message-ask-to-resend'
        : 'Message--from-me-unsupported-message';
    const stringId = isMe ? meStringId : otherStringId;
    const icon = canProcessNow ? 'unsupported--can-process' : 'unsupported';
    return (react_1.default.createElement(SystemMessage_1.SystemMessage, { icon: icon, contents: react_1.default.createElement(Intl_1.Intl, { id: stringId, components: [
                react_1.default.createElement("span", { key: "external-1", className: "module-unsupported-message__contact" },
                    react_1.default.createElement(ContactName_1.ContactName, { title: contact.title, module: "module-unsupported-message__contact" })),
            ], i18n: i18n }), button: canProcessNow ? undefined : (react_1.default.createElement("div", { className: "SystemMessage__line" },
            react_1.default.createElement(Button_1.Button, { onClick: () => {
                    downloadNewVersion();
                }, size: Button_1.ButtonSize.Small, variant: Button_1.ButtonVariant.SystemMessage }, i18n('Message--update-signal')))) }));
};
exports.UnsupportedMessage = UnsupportedMessage;
