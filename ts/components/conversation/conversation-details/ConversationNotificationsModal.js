"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationNotificationsModal = void 0;
const react_1 = __importStar(require("react"));
const getMuteOptions_1 = require("../../../util/getMuteOptions");
const parseIntOrThrow_1 = require("../../../util/parseIntOrThrow");
const Checkbox_1 = require("../../Checkbox");
const Modal_1 = require("../../Modal");
const Button_1 = require("../../Button");
const ConversationNotificationsModal = ({ i18n, muteExpiresAt, onClose, setMuteExpiration, }) => {
    const muteOptions = (0, react_1.useMemo)(() => (0, getMuteOptions_1.getMuteOptions)(muteExpiresAt, i18n).map(({ disabled, name, value }) => ({
        disabled,
        text: name,
        value,
    })), [i18n, muteExpiresAt]);
    const [muteExpirationValue, setMuteExpirationValue] = (0, react_1.useState)(muteExpiresAt);
    const onMuteChange = () => {
        const ms = (0, parseIntOrThrow_1.parseIntOrThrow)(muteExpirationValue, 'NotificationSettings: mute ms was not an integer');
        setMuteExpiration(ms);
        onClose();
    };
    return (react_1.default.createElement(Modal_1.Modal, { hasStickyButtons: true, hasXButton: true, onClose: onClose, i18n: i18n, title: i18n('muteNotificationsTitle') },
        muteOptions
            .filter(x => x.value > 0)
            .map(option => (react_1.default.createElement(Checkbox_1.Checkbox, { checked: muteExpirationValue === option.value, disabled: option.disabled, isRadio: true, label: option.text, moduleClassName: "ConversationDetails__radio", name: "mute", onChange: value => value && setMuteExpirationValue(option.value) }))),
        react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
            react_1.default.createElement(Button_1.Button, { onClick: onClose, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
            react_1.default.createElement(Button_1.Button, { onClick: onMuteChange, variant: Button_1.ButtonVariant.Primary }, i18n('mute')))));
};
exports.ConversationNotificationsModal = ConversationNotificationsModal;
