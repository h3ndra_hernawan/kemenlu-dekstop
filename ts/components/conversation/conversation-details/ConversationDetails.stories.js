"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const lodash_1 = require("lodash");
const setupI18n_1 = require("../../../util/setupI18n");
const errors_1 = require("../../../types/errors");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const ConversationDetails_1 = require("./ConversationDetails");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const Util_1 = require("../../../types/Util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ConversationDetails/ConversationDetails', module);
const conversation = (0, getDefaultConversation_1.getDefaultConversation)({
    id: '',
    lastUpdated: 0,
    title: 'Some Conversation',
    groupDescription: 'Hello World!',
    type: 'group',
    sharedGroupNames: [],
    conversationColor: 'ultramarine',
});
const createProps = (hasGroupLink = false, expireTimer) => ({
    addMembers: async () => {
        (0, addon_actions_1.action)('addMembers');
    },
    canEditGroupInfo: false,
    candidateContactsToAdd: (0, lodash_1.times)(10, () => (0, getDefaultConversation_1.getDefaultConversation)()),
    conversation: expireTimer
        ? Object.assign(Object.assign({}, conversation), { expireTimer }) : conversation,
    hasGroupLink,
    i18n,
    isAdmin: false,
    isGroup: true,
    loadRecentMediaItems: (0, addon_actions_1.action)('loadRecentMediaItems'),
    memberships: (0, lodash_1.times)(32, i => ({
        isAdmin: i === 1,
        member: (0, getDefaultConversation_1.getDefaultConversation)({
            isMe: i === 2,
        }),
    })),
    preferredBadgeByConversation: {},
    pendingApprovalMemberships: (0, lodash_1.times)(8, () => ({
        member: (0, getDefaultConversation_1.getDefaultConversation)(),
    })),
    pendingMemberships: (0, lodash_1.times)(5, () => ({
        metadata: {},
        member: (0, getDefaultConversation_1.getDefaultConversation)(),
    })),
    setDisappearingMessages: (0, addon_actions_1.action)('setDisappearingMessages'),
    showAllMedia: (0, addon_actions_1.action)('showAllMedia'),
    showContactModal: (0, addon_actions_1.action)('showContactModal'),
    showChatColorEditor: (0, addon_actions_1.action)('showChatColorEditor'),
    showGroupLinkManagement: (0, addon_actions_1.action)('showGroupLinkManagement'),
    showGroupV2Permissions: (0, addon_actions_1.action)('showGroupV2Permissions'),
    showConversationNotificationsSettings: (0, addon_actions_1.action)('showConversationNotificationsSettings'),
    showPendingInvites: (0, addon_actions_1.action)('showPendingInvites'),
    showLightboxForMedia: (0, addon_actions_1.action)('showLightboxForMedia'),
    updateGroupAttributes: async () => {
        (0, addon_actions_1.action)('updateGroupAttributes')();
    },
    onBlock: (0, addon_actions_1.action)('onBlock'),
    onLeave: (0, addon_actions_1.action)('onLeave'),
    onUnblock: (0, addon_actions_1.action)('onUnblock'),
    deleteAvatarFromDisk: (0, addon_actions_1.action)('deleteAvatarFromDisk'),
    replaceAvatar: (0, addon_actions_1.action)('replaceAvatar'),
    saveAvatarToDisk: (0, addon_actions_1.action)('saveAvatarToDisk'),
    setMuteExpiration: (0, addon_actions_1.action)('setMuteExpiration'),
    userAvatarData: [],
    toggleSafetyNumberModal: (0, addon_actions_1.action)('toggleSafetyNumberModal'),
    onOutgoingAudioCallInConversation: (0, addon_actions_1.action)('onOutgoingAudioCallInConversation'),
    onOutgoingVideoCallInConversation: (0, addon_actions_1.action)('onOutgoingVideoCallInConversation'),
    searchInConversation: (0, addon_actions_1.action)('searchInConversation'),
    theme: Util_1.ThemeType.light,
});
story.add('Basic', () => {
    const props = createProps();
    return React.createElement(ConversationDetails_1.ConversationDetails, Object.assign({}, props));
});
story.add('as Admin', () => {
    const props = createProps();
    return React.createElement(ConversationDetails_1.ConversationDetails, Object.assign({}, props, { isAdmin: true }));
});
story.add('as last admin', () => {
    const props = createProps();
    return (React.createElement(ConversationDetails_1.ConversationDetails, Object.assign({}, props, { isAdmin: true, memberships: (0, lodash_1.times)(32, i => ({
            isAdmin: i === 2,
            member: (0, getDefaultConversation_1.getDefaultConversation)({
                isMe: i === 2,
            }),
        })) })));
});
story.add('as only admin', () => {
    const props = createProps();
    return (React.createElement(ConversationDetails_1.ConversationDetails, Object.assign({}, props, { isAdmin: true, memberships: [
            {
                isAdmin: true,
                member: (0, getDefaultConversation_1.getDefaultConversation)({
                    isMe: true,
                }),
            },
        ] })));
});
story.add('Group Editable', () => {
    const props = createProps();
    return React.createElement(ConversationDetails_1.ConversationDetails, Object.assign({}, props, { canEditGroupInfo: true }));
});
story.add('Group Editable with custom disappearing timeout', () => {
    const props = createProps(false, 3 * 24 * 60 * 60);
    return React.createElement(ConversationDetails_1.ConversationDetails, Object.assign({}, props, { canEditGroupInfo: true }));
});
story.add('Group Links On', () => {
    const props = createProps(true);
    return React.createElement(ConversationDetails_1.ConversationDetails, Object.assign({}, props, { isAdmin: true }));
});
story.add('Group add with missing capabilities', () => (React.createElement(ConversationDetails_1.ConversationDetails, Object.assign({}, createProps(), { canEditGroupInfo: true, addMembers: async () => {
        throw new errors_1.CapabilityError('stories');
    } }))));
story.add('1:1', () => (React.createElement(ConversationDetails_1.ConversationDetails, Object.assign({}, createProps(), { isGroup: false }))));
