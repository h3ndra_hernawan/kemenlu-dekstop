"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PendingInvites = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = __importDefault(require("lodash"));
const Avatar_1 = require("../../Avatar");
const ConfirmationDialog_1 = require("../../ConfirmationDialog");
const PanelSection_1 = require("./PanelSection");
const PanelRow_1 = require("./PanelRow");
const ConversationDetailsIcon_1 = require("./ConversationDetailsIcon");
var Tab;
(function (Tab) {
    Tab["Requests"] = "Requests";
    Tab["Pending"] = "Pending";
})(Tab || (Tab = {}));
var StageType;
(function (StageType) {
    StageType["APPROVE_REQUEST"] = "APPROVE_REQUEST";
    StageType["DENY_REQUEST"] = "DENY_REQUEST";
    StageType["REVOKE_INVITE"] = "REVOKE_INVITE";
})(StageType || (StageType = {}));
const PendingInvites = ({ approvePendingMembership, conversation, i18n, ourUuid, pendingMemberships, pendingApprovalMemberships, revokePendingMemberships, }) => {
    if (!conversation || !ourUuid) {
        throw new Error('PendingInvites rendered without a conversation or ourUuid');
    }
    const [selectedTab, setSelectedTab] = react_1.default.useState(Tab.Requests);
    const [stagedMemberships, setStagedMemberships] = react_1.default.useState(null);
    return (react_1.default.createElement("div", { className: "conversation-details-panel" },
        react_1.default.createElement("div", { className: "ConversationDetails__tabs" },
            react_1.default.createElement("div", { className: (0, classnames_1.default)({
                    ConversationDetails__tab: true,
                    'ConversationDetails__tab--selected': selectedTab === Tab.Requests,
                }), onClick: () => {
                    setSelectedTab(Tab.Requests);
                }, onKeyUp: (e) => {
                    if (e.target === e.currentTarget && e.keyCode === 13) {
                        setSelectedTab(Tab.Requests);
                    }
                }, role: "tab", tabIndex: 0 }, i18n('PendingInvites--tab-requests', {
                count: String(pendingApprovalMemberships.length),
            })),
            react_1.default.createElement("div", { className: (0, classnames_1.default)({
                    ConversationDetails__tab: true,
                    'ConversationDetails__tab--selected': selectedTab === Tab.Pending,
                }), onClick: () => {
                    setSelectedTab(Tab.Pending);
                }, onKeyUp: (e) => {
                    if (e.target === e.currentTarget && e.keyCode === 13) {
                        setSelectedTab(Tab.Pending);
                    }
                }, role: "tab", tabIndex: 0 }, i18n('PendingInvites--tab-invites', {
                count: String(pendingMemberships.length),
            }))),
        selectedTab === Tab.Requests ? (react_1.default.createElement(MembersPendingAdminApproval, { conversation: conversation, i18n: i18n, memberships: pendingApprovalMemberships, setStagedMemberships: setStagedMemberships })) : null,
        selectedTab === Tab.Pending ? (react_1.default.createElement(MembersPendingProfileKey, { conversation: conversation, i18n: i18n, members: conversation.sortedGroupMembers || [], memberships: pendingMemberships, ourUuid: ourUuid, setStagedMemberships: setStagedMemberships })) : null,
        stagedMemberships && stagedMemberships.length && (react_1.default.createElement(MembershipActionConfirmation, { approvePendingMembership: approvePendingMembership, i18n: i18n, members: conversation.sortedGroupMembers || [], onClose: () => setStagedMemberships(null), ourUuid: ourUuid, revokePendingMemberships: revokePendingMemberships, stagedMemberships: stagedMemberships }))));
};
exports.PendingInvites = PendingInvites;
function MembershipActionConfirmation({ approvePendingMembership, i18n, members, onClose, ourUuid, revokePendingMemberships, stagedMemberships, }) {
    const revokeStagedMemberships = () => {
        if (!stagedMemberships) {
            return;
        }
        revokePendingMemberships(stagedMemberships.map(({ membership }) => membership.member.id));
    };
    const approveStagedMembership = () => {
        if (!stagedMemberships) {
            return;
        }
        approvePendingMembership(stagedMemberships[0].membership.member.id);
    };
    const membershipType = stagedMemberships[0].type;
    const modalAction = membershipType === StageType.APPROVE_REQUEST
        ? approveStagedMembership
        : revokeStagedMemberships;
    let modalActionText = i18n('PendingInvites--revoke');
    if (membershipType === StageType.APPROVE_REQUEST) {
        modalActionText = i18n('PendingRequests--approve');
    }
    else if (membershipType === StageType.DENY_REQUEST) {
        modalActionText = i18n('PendingRequests--deny');
    }
    else if (membershipType === StageType.REVOKE_INVITE) {
        modalActionText = i18n('PendingInvites--revoke');
    }
    return (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
            {
                action: modalAction,
                style: 'affirmative',
                text: modalActionText,
            },
        ], i18n: i18n, onClose: onClose }, getConfirmationMessage({
        i18n,
        members,
        ourUuid,
        stagedMemberships,
    })));
}
function getConfirmationMessage({ i18n, members, ourUuid, stagedMemberships, }) {
    if (!stagedMemberships || !stagedMemberships.length) {
        return '';
    }
    const membershipType = stagedMemberships[0].type;
    const firstMembership = stagedMemberships[0].membership;
    // Requesting a membership since they weren't added by anyone
    if (membershipType === StageType.DENY_REQUEST) {
        return i18n('PendingRequests--deny-for', {
            name: firstMembership.member.title,
        });
    }
    if (membershipType === StageType.APPROVE_REQUEST) {
        return i18n('PendingRequests--approve-for', {
            name: firstMembership.member.title,
        });
    }
    if (membershipType !== StageType.REVOKE_INVITE) {
        throw new Error('getConfirmationMessage: Invalid staging type');
    }
    const firstPendingMembership = firstMembership;
    // Pending invite
    const invitedByUs = firstPendingMembership.metadata.addedByUserId === ourUuid;
    if (invitedByUs) {
        return i18n('PendingInvites--revoke-for', {
            name: firstPendingMembership.member.title,
        });
    }
    const inviter = members.find(({ id }) => id === firstPendingMembership.metadata.addedByUserId);
    if (inviter === undefined) {
        return '';
    }
    const name = inviter.title;
    if (stagedMemberships.length === 1) {
        return i18n('PendingInvites--revoke-from-singular', { name });
    }
    return i18n('PendingInvites--revoke-from-plural', {
        number: stagedMemberships.length.toString(),
        name,
    });
}
function MembersPendingAdminApproval({ conversation, i18n, memberships, setStagedMemberships, }) {
    return (react_1.default.createElement(PanelSection_1.PanelSection, null,
        memberships.map(membership => (react_1.default.createElement(PanelRow_1.PanelRow, { alwaysShowActions: true, key: membership.member.id, icon: react_1.default.createElement(Avatar_1.Avatar, Object.assign({ conversationType: "direct", size: 32, i18n: i18n }, membership.member)), label: membership.member.title, actions: conversation.areWeAdmin ? (react_1.default.createElement(react_1.default.Fragment, null,
                react_1.default.createElement("button", { type: "button", className: "module-button__small ConversationDetails__action-button", onClick: () => {
                        setStagedMemberships([
                            {
                                type: StageType.DENY_REQUEST,
                                membership,
                            },
                        ]);
                    } }, i18n('delete')),
                react_1.default.createElement("button", { type: "button", className: "module-button__small ConversationDetails__action-button", onClick: () => {
                        setStagedMemberships([
                            {
                                type: StageType.APPROVE_REQUEST,
                                membership,
                            },
                        ]);
                    } }, i18n('accept')))) : null }))),
        react_1.default.createElement("div", { className: "ConversationDetails__pending--info" }, i18n('PendingRequests--info', [conversation.title]))));
}
function MembersPendingProfileKey({ conversation, i18n, members, memberships, ourUuid, setStagedMemberships, }) {
    const groupedPendingMemberships = lodash_1.default.groupBy(memberships, membership => membership.metadata.addedByUserId);
    const _a = groupedPendingMemberships, _b = ourUuid, ourPendingMemberships = _a[_b], otherPendingMembershipGroups = __rest(_a, [typeof _b === "symbol" ? _b : _b + ""]);
    const otherPendingMemberships = Object.keys(otherPendingMembershipGroups)
        .map(id => members.find(member => member.id === id))
        .filter((member) => member !== undefined)
        .map(member => ({
        member,
        pendingMemberships: otherPendingMembershipGroups[member.id],
    }));
    return (react_1.default.createElement(PanelSection_1.PanelSection, null,
        ourPendingMemberships && (react_1.default.createElement(PanelSection_1.PanelSection, { title: i18n('PendingInvites--invited-by-you') }, ourPendingMemberships.map(membership => (react_1.default.createElement(PanelRow_1.PanelRow, { key: membership.member.id, icon: react_1.default.createElement(Avatar_1.Avatar, Object.assign({ conversationType: "direct", size: 32, i18n: i18n }, membership.member)), label: membership.member.title, actions: conversation.areWeAdmin ? (react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('PendingInvites--revoke-for-label'), icon: ConversationDetailsIcon_1.IconType.trash, onClick: () => {
                    setStagedMemberships([
                        {
                            type: StageType.REVOKE_INVITE,
                            membership,
                        },
                    ]);
                } })) : null }))))),
        otherPendingMemberships.length > 0 && (react_1.default.createElement(PanelSection_1.PanelSection, { title: i18n('PendingInvites--invited-by-others') }, otherPendingMemberships.map(({ member, pendingMemberships }) => (react_1.default.createElement(PanelRow_1.PanelRow, { key: member.id, icon: react_1.default.createElement(Avatar_1.Avatar, Object.assign({ conversationType: "direct", size: 32, i18n: i18n }, member)), label: member.title, right: i18n('PendingInvites--invited-count', [
                pendingMemberships.length.toString(),
            ]), actions: conversation.areWeAdmin ? (react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('PendingInvites--revoke-for-label'), icon: ConversationDetailsIcon_1.IconType.trash, onClick: () => {
                    setStagedMemberships(pendingMemberships.map(membership => ({
                        type: StageType.REVOKE_INVITE,
                        membership,
                    })));
                } })) : null }))))),
        react_1.default.createElement("div", { className: "ConversationDetails__pending--info" }, i18n('PendingInvites--info'))));
}
