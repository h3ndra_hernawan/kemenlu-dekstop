"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const GroupLinkManagement_1 = require("./GroupLinkManagement");
const protobuf_1 = require("../../../protobuf");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ConversationDetails/GroupLinkManagement', module);
const AccessControlEnum = protobuf_1.SignalService.AccessControl.AccessRequired;
function getConversation(groupLink, accessControlAddFromInviteLink) {
    return (0, getDefaultConversation_1.getDefaultConversation)({
        id: '',
        lastUpdated: 0,
        memberships: Array(32).fill({ member: (0, getDefaultConversation_1.getDefaultConversation)({}) }),
        pendingMemberships: Array(16).fill({ member: (0, getDefaultConversation_1.getDefaultConversation)({}) }),
        title: 'Some Conversation',
        type: 'group',
        sharedGroupNames: [],
        groupLink,
        accessControlAddFromInviteLink: accessControlAddFromInviteLink !== undefined
            ? accessControlAddFromInviteLink
            : AccessControlEnum.UNSATISFIABLE,
    });
}
const createProps = (conversation, isAdmin = false) => ({
    changeHasGroupLink: (0, addon_actions_1.action)('changeHasGroupLink'),
    conversation: conversation || getConversation(),
    copyGroupLink: (0, addon_actions_1.action)('copyGroupLink'),
    generateNewGroupLink: (0, addon_actions_1.action)('generateNewGroupLink'),
    i18n,
    isAdmin,
    setAccessControlAddFromInviteLinkSetting: (0, addon_actions_1.action)('setAccessControlAddFromInviteLinkSetting'),
});
story.add('Off (Admin)', () => {
    const props = createProps(undefined, true);
    return React.createElement(GroupLinkManagement_1.GroupLinkManagement, Object.assign({}, props));
});
story.add('On (Admin)', () => {
    const props = createProps(getConversation('https://signal.group/1', AccessControlEnum.ANY), true);
    return React.createElement(GroupLinkManagement_1.GroupLinkManagement, Object.assign({}, props));
});
story.add('On (Admin + Admin Approval Needed)', () => {
    const props = createProps(getConversation('https://signal.group/1', AccessControlEnum.ADMINISTRATOR), true);
    return React.createElement(GroupLinkManagement_1.GroupLinkManagement, Object.assign({}, props));
});
story.add('On (Non-admin)', () => {
    const props = createProps(getConversation('https://signal.group/1', AccessControlEnum.ANY));
    return React.createElement(GroupLinkManagement_1.GroupLinkManagement, Object.assign({}, props));
});
story.add('Off (Non-admin) - user cannot get here', () => {
    const props = createProps(undefined, false);
    return React.createElement(GroupLinkManagement_1.GroupLinkManagement, Object.assign({}, props));
});
