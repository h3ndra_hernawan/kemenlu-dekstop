"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupLinkManagement = void 0;
const react_1 = __importDefault(require("react"));
const ConversationDetailsIcon_1 = require("./ConversationDetailsIcon");
const protobuf_1 = require("../../../protobuf");
const PanelRow_1 = require("./PanelRow");
const PanelSection_1 = require("./PanelSection");
const Select_1 = require("../../Select");
const useRestoreFocus_1 = require("../../../hooks/useRestoreFocus");
const AccessControlEnum = protobuf_1.SignalService.AccessControl.AccessRequired;
const GroupLinkManagement = ({ changeHasGroupLink, conversation, copyGroupLink, generateNewGroupLink, i18n, isAdmin, setAccessControlAddFromInviteLinkSetting, }) => {
    if (conversation === undefined) {
        throw new Error('GroupLinkManagement rendered without a conversation');
    }
    const [focusRef] = (0, useRestoreFocus_1.useDelayedRestoreFocus)();
    const createEventHandler = (handleEvent) => {
        return (value) => {
            handleEvent(value === 'true');
        };
    };
    const membersNeedAdminApproval = conversation.accessControlAddFromInviteLink ===
        AccessControlEnum.ADMINISTRATOR;
    const hasGroupLink = conversation.groupLink &&
        conversation.accessControlAddFromInviteLink !==
            AccessControlEnum.UNSATISFIABLE;
    const groupLinkInfo = hasGroupLink ? conversation.groupLink : '';
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(PanelSection_1.PanelSection, null,
            react_1.default.createElement(PanelRow_1.PanelRow, { info: groupLinkInfo, label: i18n('ConversationDetails--group-link'), right: isAdmin ? (react_1.default.createElement(Select_1.Select, { onChange: createEventHandler(changeHasGroupLink), options: [
                        {
                            text: i18n('on'),
                            value: 'true',
                        },
                        {
                            text: i18n('off'),
                            value: 'false',
                        },
                    ], ref: focusRef, value: String(Boolean(hasGroupLink)) })) : null })),
        hasGroupLink ? (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(PanelSection_1.PanelSection, null,
                react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('GroupLinkManagement--share'), icon: ConversationDetailsIcon_1.IconType.share }), label: i18n('GroupLinkManagement--share'), ref: !isAdmin ? focusRef : undefined, onClick: () => {
                        if (conversation.groupLink) {
                            copyGroupLink(conversation.groupLink);
                        }
                    } }),
                isAdmin ? (react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('GroupLinkManagement--reset'), icon: ConversationDetailsIcon_1.IconType.reset }), label: i18n('GroupLinkManagement--reset'), onClick: generateNewGroupLink })) : null),
            isAdmin ? (react_1.default.createElement(PanelSection_1.PanelSection, null,
                react_1.default.createElement(PanelRow_1.PanelRow, { info: i18n('GroupLinkManagement--approve-info'), label: i18n('GroupLinkManagement--approve-label'), right: react_1.default.createElement(Select_1.Select, { onChange: createEventHandler(setAccessControlAddFromInviteLinkSetting), options: [
                            {
                                text: i18n('on'),
                                value: 'true',
                            },
                            {
                                text: i18n('off'),
                                value: 'false',
                            },
                        ], value: String(membersNeedAdminApproval) }) }))) : null)) : null));
};
exports.GroupLinkManagement = GroupLinkManagement;
