"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const getFakeBadge_1 = require("../../../test-both/helpers/getFakeBadge");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const StorybookThemeContext_1 = require("../../../../.storybook/StorybookThemeContext");
const ConversationDetailsHeader_1 = require("./ConversationDetailsHeader");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ConversationDetails/ConversationDetailsHeader', module);
const createConversation = () => (0, getDefaultConversation_1.getDefaultConversation)({
    id: '',
    type: 'group',
    lastUpdated: 0,
    title: (0, addon_knobs_1.text)('conversation title', 'Some Conversation'),
    groupDescription: (0, addon_knobs_1.text)('description', 'This is a group description. https://www.signal.org'),
});
const Wrapper = (overrideProps) => {
    const theme = React.useContext(StorybookThemeContext_1.StorybookThemeContext);
    return (React.createElement(ConversationDetailsHeader_1.ConversationDetailsHeader, Object.assign({ conversation: createConversation(), i18n: i18n, canEdit: false, startEditing: (0, addon_actions_1.action)('startEditing'), memberships: new Array((0, addon_knobs_1.number)('conversation members length', 0)), isGroup: true, isMe: false, theme: theme }, overrideProps)));
};
story.add('Basic', () => React.createElement(Wrapper, null));
story.add('Editable', () => React.createElement(Wrapper, { canEdit: true }));
story.add('Basic no-description', () => (React.createElement(Wrapper, { conversation: (0, getDefaultConversation_1.getDefaultConversation)({
        title: 'My Group',
        type: 'group',
    }) })));
story.add('Editable no-description', () => (React.createElement(Wrapper, { conversation: (0, getDefaultConversation_1.getDefaultConversation)({
        title: 'My Group',
        type: 'group',
    }) })));
story.add('1:1', () => React.createElement(Wrapper, { isGroup: false, badges: (0, getFakeBadge_1.getFakeBadges)(3) }));
story.add('Note to self', () => React.createElement(Wrapper, { isMe: true }));
