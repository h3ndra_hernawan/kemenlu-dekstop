"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupV2Permissions = void 0;
const react_1 = __importDefault(require("react"));
const getAccessControlOptions_1 = require("../../../util/getAccessControlOptions");
const protobuf_1 = require("../../../protobuf");
const PanelRow_1 = require("./PanelRow");
const PanelSection_1 = require("./PanelSection");
const Select_1 = require("../../Select");
const GroupV2Permissions = ({ conversation, i18n, setAccessControlAttributesSetting, setAccessControlMembersSetting, setAnnouncementsOnly, }) => {
    if (conversation === undefined) {
        throw new Error('GroupV2Permissions rendered without a conversation');
    }
    const updateAccessControlAttributes = (value) => {
        setAccessControlAttributesSetting(Number(value));
    };
    const updateAccessControlMembers = (value) => {
        setAccessControlMembersSetting(Number(value));
    };
    const AccessControlEnum = protobuf_1.SignalService.AccessControl.AccessRequired;
    const updateAnnouncementsOnly = (value) => {
        setAnnouncementsOnly(Number(value) === AccessControlEnum.ADMINISTRATOR);
    };
    const accessControlOptions = (0, getAccessControlOptions_1.getAccessControlOptions)(i18n);
    const announcementsOnlyValue = String(conversation.announcementsOnly
        ? AccessControlEnum.ADMINISTRATOR
        : AccessControlEnum.MEMBER);
    const showAnnouncementsOnlyPermission = conversation.areWeAdmin &&
        (conversation.announcementsOnly || conversation.announcementsOnlyReady);
    return (react_1.default.createElement(PanelSection_1.PanelSection, null,
        react_1.default.createElement(PanelRow_1.PanelRow, { label: i18n('ConversationDetails--add-members-label'), info: i18n('ConversationDetails--add-members-info'), right: react_1.default.createElement(Select_1.Select, { onChange: updateAccessControlMembers, options: accessControlOptions, value: String(conversation.accessControlMembers) }) }),
        react_1.default.createElement(PanelRow_1.PanelRow, { label: i18n('ConversationDetails--group-info-label'), info: i18n('ConversationDetails--group-info-info'), right: react_1.default.createElement(Select_1.Select, { onChange: updateAccessControlAttributes, options: accessControlOptions, value: String(conversation.accessControlAttributes) }) }),
        showAnnouncementsOnlyPermission && (react_1.default.createElement(PanelRow_1.PanelRow, { label: i18n('ConversationDetails--announcement-label'), info: i18n('ConversationDetails--announcement-info'), right: react_1.default.createElement(Select_1.Select, { onChange: updateAnnouncementsOnly, options: accessControlOptions, value: announcementsOnlyValue }) }))));
};
exports.GroupV2Permissions = GroupV2Permissions;
