"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const ConversationDetailsIcon_1 = require("./ConversationDetailsIcon");
const PanelRow_1 = require("./PanelRow");
const story = (0, react_1.storiesOf)('Components/Conversation/ConversationDetails/PanelRow', module);
const createProps = (overrideProps = {}) => ({
    icon: (0, addon_knobs_1.boolean)('with icon', overrideProps.icon !== undefined) ? (React.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: "timer", icon: ConversationDetailsIcon_1.IconType.timer })) : null,
    label: (0, addon_knobs_1.text)('label', overrideProps.label || ''),
    info: (0, addon_knobs_1.text)('info', overrideProps.info || ''),
    right: (0, addon_knobs_1.text)('right', overrideProps.right || ''),
    actions: (0, addon_knobs_1.boolean)('with action', overrideProps.actions !== undefined) ? (React.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: "trash", icon: ConversationDetailsIcon_1.IconType.trash, onClick: (0, addon_actions_1.action)('action onClick') })) : null,
    onClick: (0, addon_knobs_1.boolean)('clickable', overrideProps.onClick !== undefined)
        ? overrideProps.onClick || (0, addon_actions_1.action)('onClick')
        : undefined,
});
story.add('Basic', () => {
    const props = createProps({
        label: 'this is a panel row',
    });
    return React.createElement(PanelRow_1.PanelRow, Object.assign({}, props));
});
story.add('Simple', () => {
    const props = createProps({
        label: 'this is a panel row',
        icon: 'with icon',
        right: 'side text',
    });
    return React.createElement(PanelRow_1.PanelRow, Object.assign({}, props));
});
story.add('Full', () => {
    const props = createProps({
        label: 'this is a panel row',
        icon: 'with icon',
        info: 'this is some info that exists below the main label',
        right: 'side text',
        actions: 'with action',
    });
    return React.createElement(PanelRow_1.PanelRow, Object.assign({}, props));
});
story.add('Button', () => {
    const props = createProps({
        label: 'this is a panel row',
        icon: 'with icon',
        right: 'side text',
        onClick: (0, addon_actions_1.action)('onClick'),
    });
    return React.createElement(PanelRow_1.PanelRow, Object.assign({}, props));
});
