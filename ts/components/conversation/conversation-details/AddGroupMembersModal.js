"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddGroupMembersModal = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const AddGroupMemberErrorDialog_1 = require("../../AddGroupMemberErrorDialog");
const limits_1 = require("../../../groups/limits");
const toggleSelectedContactForGroupAddition_1 = require("../../../groups/toggleSelectedContactForGroupAddition");
const makeLookup_1 = require("../../../util/makeLookup");
const deconstructLookup_1 = require("../../../util/deconstructLookup");
const missingCaseError_1 = require("../../../util/missingCaseError");
const ChooseGroupMembersModal_1 = require("./AddGroupMembersModal/ChooseGroupMembersModal");
const ConfirmAdditionsModal_1 = require("./AddGroupMembersModal/ConfirmAdditionsModal");
var Stage;
(function (Stage) {
    Stage[Stage["ChoosingContacts"] = 0] = "ChoosingContacts";
    Stage[Stage["ConfirmingAdds"] = 1] = "ConfirmingAdds";
})(Stage || (Stage = {}));
var ActionType;
(function (ActionType) {
    ActionType[ActionType["CloseMaximumGroupSizeModal"] = 0] = "CloseMaximumGroupSizeModal";
    ActionType[ActionType["CloseRecommendedMaximumGroupSizeModal"] = 1] = "CloseRecommendedMaximumGroupSizeModal";
    ActionType[ActionType["ConfirmAdds"] = 2] = "ConfirmAdds";
    ActionType[ActionType["RemoveSelectedContact"] = 3] = "RemoveSelectedContact";
    ActionType[ActionType["ReturnToContactChooser"] = 4] = "ReturnToContactChooser";
    ActionType[ActionType["SetCantAddContactForModal"] = 5] = "SetCantAddContactForModal";
    ActionType[ActionType["ToggleSelectedContact"] = 6] = "ToggleSelectedContact";
    ActionType[ActionType["UpdateSearchTerm"] = 7] = "UpdateSearchTerm";
})(ActionType || (ActionType = {}));
// `<ConversationDetails>` isn't currently hooked up to Redux, but that's not desirable in
//   the long term (see DESKTOP-1260). For now, this component has internal state with a
//   reducer. Hopefully, this will make things easier to port to Redux in the future.
function reducer(state, action) {
    switch (action.type) {
        case ActionType.CloseMaximumGroupSizeModal:
            return Object.assign(Object.assign({}, state), { maximumGroupSizeModalState: toggleSelectedContactForGroupAddition_1.OneTimeModalState.Shown });
        case ActionType.CloseRecommendedMaximumGroupSizeModal:
            return Object.assign(Object.assign({}, state), { recommendedGroupSizeModalState: toggleSelectedContactForGroupAddition_1.OneTimeModalState.Shown });
        case ActionType.ConfirmAdds:
            return Object.assign(Object.assign({}, state), { stage: Stage.ConfirmingAdds });
        case ActionType.ReturnToContactChooser:
            return Object.assign(Object.assign({}, state), { stage: Stage.ChoosingContacts });
        case ActionType.RemoveSelectedContact:
            return Object.assign(Object.assign({}, state), { selectedConversationIds: (0, lodash_1.without)(state.selectedConversationIds, action.conversationId) });
        case ActionType.SetCantAddContactForModal:
            return Object.assign(Object.assign({}, state), { cantAddContactForModal: action.contact });
        case ActionType.ToggleSelectedContact:
            return Object.assign(Object.assign({}, state), (0, toggleSelectedContactForGroupAddition_1.toggleSelectedContactForGroupAddition)(action.conversationId, {
                maxGroupSize: getMaximumNumberOfContacts(),
                maxRecommendedGroupSize: getRecommendedMaximumNumberOfContacts(),
                maximumGroupSizeModalState: state.maximumGroupSizeModalState,
                numberOfContactsAlreadyInGroup: action.numberOfContactsAlreadyInGroup,
                recommendedGroupSizeModalState: state.recommendedGroupSizeModalState,
                selectedConversationIds: state.selectedConversationIds,
            }));
        case ActionType.UpdateSearchTerm:
            return Object.assign(Object.assign({}, state), { searchTerm: action.searchTerm });
        default:
            throw (0, missingCaseError_1.missingCaseError)(action);
    }
}
const AddGroupMembersModal = ({ candidateContacts, clearRequestError, conversationIdsAlreadyInGroup, groupTitle, i18n, onClose, makeRequest, requestState, theme, }) => {
    const maxGroupSize = getMaximumNumberOfContacts();
    const maxRecommendedGroupSize = getRecommendedMaximumNumberOfContacts();
    const numberOfContactsAlreadyInGroup = conversationIdsAlreadyInGroup.size;
    const isGroupAlreadyFull = numberOfContactsAlreadyInGroup >= maxGroupSize;
    const isGroupAlreadyOverRecommendedMaximum = numberOfContactsAlreadyInGroup >= maxRecommendedGroupSize;
    const [{ cantAddContactForModal, maximumGroupSizeModalState, recommendedGroupSizeModalState, searchTerm, selectedConversationIds, stage, }, dispatch,] = (0, react_1.useReducer)(reducer, {
        cantAddContactForModal: undefined,
        maximumGroupSizeModalState: isGroupAlreadyFull
            ? toggleSelectedContactForGroupAddition_1.OneTimeModalState.Showing
            : toggleSelectedContactForGroupAddition_1.OneTimeModalState.NeverShown,
        recommendedGroupSizeModalState: isGroupAlreadyOverRecommendedMaximum
            ? toggleSelectedContactForGroupAddition_1.OneTimeModalState.Shown
            : toggleSelectedContactForGroupAddition_1.OneTimeModalState.NeverShown,
        searchTerm: '',
        selectedConversationIds: [],
        stage: Stage.ChoosingContacts,
    });
    const contactLookup = (0, react_1.useMemo)(() => (0, makeLookup_1.makeLookup)(candidateContacts, 'id'), [candidateContacts]);
    const selectedContacts = (0, deconstructLookup_1.deconstructLookup)(contactLookup, selectedConversationIds);
    if (cantAddContactForModal) {
        return (react_1.default.createElement(AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialog, { contact: cantAddContactForModal, i18n: i18n, mode: AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialogMode.CantAddContact, onClose: () => {
                dispatch({
                    type: ActionType.SetCantAddContactForModal,
                    contact: undefined,
                });
            } }));
    }
    if (maximumGroupSizeModalState === toggleSelectedContactForGroupAddition_1.OneTimeModalState.Showing) {
        return (react_1.default.createElement(AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialog, { i18n: i18n, maximumNumberOfContacts: maxGroupSize, mode: AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialogMode.MaximumGroupSize, onClose: () => {
                dispatch({ type: ActionType.CloseMaximumGroupSizeModal });
            } }));
    }
    if (recommendedGroupSizeModalState === toggleSelectedContactForGroupAddition_1.OneTimeModalState.Showing) {
        return (react_1.default.createElement(AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialog, { i18n: i18n, mode: AddGroupMemberErrorDialog_1.AddGroupMemberErrorDialogMode.RecommendedMaximumGroupSize, onClose: () => {
                dispatch({
                    type: ActionType.CloseRecommendedMaximumGroupSizeModal,
                });
            }, recommendedMaximumNumberOfContacts: maxRecommendedGroupSize }));
    }
    switch (stage) {
        case Stage.ChoosingContacts: {
            // See note above: these will soon become Redux actions.
            const confirmAdds = () => {
                dispatch({ type: ActionType.ConfirmAdds });
            };
            const removeSelectedContact = (conversationId) => {
                dispatch({
                    type: ActionType.RemoveSelectedContact,
                    conversationId,
                });
            };
            const setCantAddContactForModal = (contact) => {
                dispatch({
                    type: ActionType.SetCantAddContactForModal,
                    contact,
                });
            };
            const setSearchTerm = (term) => {
                dispatch({
                    type: ActionType.UpdateSearchTerm,
                    searchTerm: term,
                });
            };
            const toggleSelectedContact = (conversationId) => {
                dispatch({
                    type: ActionType.ToggleSelectedContact,
                    conversationId,
                    numberOfContactsAlreadyInGroup,
                });
            };
            return (react_1.default.createElement(ChooseGroupMembersModal_1.ChooseGroupMembersModal, { candidateContacts: candidateContacts, confirmAdds: confirmAdds, contactLookup: contactLookup, conversationIdsAlreadyInGroup: conversationIdsAlreadyInGroup, i18n: i18n, maxGroupSize: maxGroupSize, onClose: onClose, removeSelectedContact: removeSelectedContact, searchTerm: searchTerm, selectedContacts: selectedContacts, setCantAddContactForModal: setCantAddContactForModal, setSearchTerm: setSearchTerm, theme: theme, toggleSelectedContact: toggleSelectedContact }));
        }
        case Stage.ConfirmingAdds: {
            const onCloseConfirmationDialog = () => {
                dispatch({ type: ActionType.ReturnToContactChooser });
                clearRequestError();
            };
            return (react_1.default.createElement(ConfirmAdditionsModal_1.ConfirmAdditionsModal, { groupTitle: groupTitle, i18n: i18n, makeRequest: () => {
                    makeRequest(selectedConversationIds);
                }, onClose: onCloseConfirmationDialog, requestState: requestState, selectedContacts: selectedContacts }));
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(stage);
    }
};
exports.AddGroupMembersModal = AddGroupMembersModal;
function getRecommendedMaximumNumberOfContacts() {
    return (0, limits_1.getGroupSizeRecommendedLimit)(151);
}
function getMaximumNumberOfContacts() {
    return (0, limits_1.getGroupSizeHardLimit)(1001);
}
