"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const EditConversationAttributesModal_1 = require("./EditConversationAttributesModal");
const util_1 = require("./util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/Conversation/ConversationDetails/EditConversationAttributesModal', module);
const createProps = (overrideProps = {}) => (Object.assign({ avatarPath: undefined, conversationId: '123', i18n, initiallyFocusDescription: false, onClose: (0, addon_actions_1.action)('onClose'), makeRequest: (0, addon_actions_1.action)('onMakeRequest'), requestState: util_1.RequestState.Inactive, title: 'Bing Bong Group', deleteAvatarFromDisk: (0, addon_actions_1.action)('deleteAvatarFromDisk'), replaceAvatar: (0, addon_actions_1.action)('replaceAvatar'), saveAvatarToDisk: (0, addon_actions_1.action)('saveAvatarToDisk'), userAvatarData: [] }, overrideProps));
story.add('No avatar, empty title', () => (react_1.default.createElement(EditConversationAttributesModal_1.EditConversationAttributesModal, Object.assign({}, createProps({ title: '' })))));
story.add('Avatar and title', () => (react_1.default.createElement(EditConversationAttributesModal_1.EditConversationAttributesModal, Object.assign({}, createProps({
    avatarPath: '/fixtures/kitten-3-64-64.jpg',
})))));
story.add('Initially focusing description', () => (react_1.default.createElement(EditConversationAttributesModal_1.EditConversationAttributesModal, Object.assign({}, createProps({ title: 'Has title', initiallyFocusDescription: true })))));
story.add('Request active', () => (react_1.default.createElement(EditConversationAttributesModal_1.EditConversationAttributesModal, Object.assign({}, createProps({ requestState: util_1.RequestState.Active })))));
story.add('Has error', () => (react_1.default.createElement(EditConversationAttributesModal_1.EditConversationAttributesModal, Object.assign({}, createProps({ requestState: util_1.RequestState.InactiveWithError })))));
