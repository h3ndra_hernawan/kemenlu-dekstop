"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationDetailsIcon = exports.IconType = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Spinner_1 = require("../../Spinner");
const util_1 = require("./util");
var IconType;
(function (IconType) {
    IconType["block"] = "block";
    IconType["color"] = "color";
    IconType["down"] = "down";
    IconType["invites"] = "invites";
    IconType["leave"] = "leave";
    IconType["link"] = "link";
    IconType["lock"] = "lock";
    IconType["mention"] = "mention";
    IconType["mute"] = "mute";
    IconType["notifications"] = "notifications";
    IconType["reset"] = "reset";
    IconType["share"] = "share";
    IconType["spinner"] = "spinner";
    IconType["timer"] = "timer";
    IconType["trash"] = "trash";
    IconType["verify"] = "verify";
})(IconType = exports.IconType || (exports.IconType = {}));
const bem = (0, util_1.bemGenerator)('ConversationDetails-icon');
const ConversationDetailsIcon = ({ ariaLabel, disabled, icon, fakeButton, onClick, }) => {
    let content;
    if (icon === IconType.spinner) {
        content = react_1.default.createElement(Spinner_1.Spinner, { svgSize: "small", size: "24" });
    }
    else {
        const iconClassName = bem('icon', icon);
        content = (react_1.default.createElement("div", { className: (0, classnames_1.default)(iconClassName, disabled && `${iconClassName}--disabled`) }));
    }
    // We need this because sometimes this component is inside other buttons
    if (onClick && fakeButton && !disabled) {
        return (react_1.default.createElement("div", { "aria-label": ariaLabel, role: "button", className: bem('button'), tabIndex: 0, onClick: (event) => {
                event.preventDefault();
                event.stopPropagation();
                onClick();
            }, onKeyDown: (event) => {
                if (event.key === 'Enter' || event.key === ' ') {
                    event.preventDefault();
                    event.stopPropagation();
                    onClick();
                }
            } }, content));
    }
    if (onClick) {
        return (react_1.default.createElement("button", { "aria-label": ariaLabel, className: bem('button'), disabled: disabled, type: "button", onClick: (event) => {
                event.preventDefault();
                event.stopPropagation();
                onClick();
            } }, content));
    }
    return content;
};
exports.ConversationDetailsIcon = ConversationDetailsIcon;
