"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PanelRow = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const util_1 = require("./util");
const bem = (0, util_1.bemGenerator)('ConversationDetails-panel-row');
exports.PanelRow = react_1.default.forwardRef(({ alwaysShowActions, className, disabled, icon, label, info, right, actions, onClick, }, ref) => {
    const content = (react_1.default.createElement(react_1.default.Fragment, null,
        icon !== undefined ? react_1.default.createElement("div", { className: bem('icon') }, icon) : null,
        react_1.default.createElement("div", { className: bem('label') },
            react_1.default.createElement("div", null, label),
            info !== undefined ? (react_1.default.createElement("div", { className: bem('info') }, info)) : null),
        right !== undefined ? (react_1.default.createElement("div", { className: bem('right') }, right)) : null,
        actions !== undefined ? (react_1.default.createElement("div", { className: alwaysShowActions ? '' : bem('actions') }, actions)) : null));
    if (onClick) {
        return (react_1.default.createElement("button", { disabled: disabled, type: "button", className: (0, classnames_1.default)(bem('root', 'button'), className), onClick: onClick, ref: ref }, content));
    }
    return react_1.default.createElement("div", { className: (0, classnames_1.default)(bem('root'), className) }, content);
});
