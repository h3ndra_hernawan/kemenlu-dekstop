"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const PanelSection_1 = require("./PanelSection");
const PanelRow_1 = require("./PanelRow");
const story = (0, react_1.storiesOf)('Components/Conversation/ConversationDetails/PanelSection', module);
const createProps = (overrideProps = {}) => ({
    title: (0, addon_knobs_1.text)('label', overrideProps.title || ''),
    centerTitle: (0, addon_knobs_1.boolean)('centerTitle', overrideProps.centerTitle || false),
    actions: (0, addon_knobs_1.boolean)('with action', overrideProps.actions !== undefined) ? (React.createElement("button", { onClick: (0, addon_actions_1.action)('actions onClick'), type: "button" }, "action")) : null,
});
story.add('Basic', () => {
    const props = createProps({
        title: 'panel section header',
    });
    return React.createElement(PanelSection_1.PanelSection, Object.assign({}, props));
});
story.add('Centered', () => {
    const props = createProps({
        title: 'this is a panel row',
        centerTitle: true,
    });
    return React.createElement(PanelSection_1.PanelSection, Object.assign({}, props));
});
story.add('With Actions', () => {
    const props = createProps({
        title: 'this is a panel row',
        actions: (React.createElement("button", { onClick: (0, addon_actions_1.action)('actions onClick'), type: "button" }, "action")),
    });
    return React.createElement(PanelSection_1.PanelSection, Object.assign({}, props));
});
story.add('With Content', () => {
    const props = createProps({
        title: 'this is a panel row',
    });
    return (React.createElement(PanelSection_1.PanelSection, Object.assign({}, props),
        React.createElement(PanelRow_1.PanelRow, { label: "this is panel row one" }),
        React.createElement(PanelRow_1.PanelRow, { label: "this is panel row two" }),
        React.createElement(PanelRow_1.PanelRow, { label: "this is panel row three" }),
        React.createElement(PanelRow_1.PanelRow, { label: "this is panel row four" })));
});
