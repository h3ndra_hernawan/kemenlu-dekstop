"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const ConversationDetailsIcon_1 = require("./ConversationDetailsIcon");
const story = (0, react_1.storiesOf)('Components/Conversation/ConversationDetails/ConversationDetailIcon', module);
const createProps = (overrideProps) => ({
    ariaLabel: overrideProps.ariaLabel || '',
    icon: overrideProps.icon || ConversationDetailsIcon_1.IconType.timer,
    onClick: overrideProps.onClick,
});
story.add('All', () => {
    const icons = Object.values(ConversationDetailsIcon_1.IconType);
    return icons.map(icon => (React.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, Object.assign({}, createProps({ icon })))));
});
story.add('Clickable Icons', () => {
    const icons = [
        ConversationDetailsIcon_1.IconType.timer,
        ConversationDetailsIcon_1.IconType.trash,
        ConversationDetailsIcon_1.IconType.invites,
        ConversationDetailsIcon_1.IconType.block,
        ConversationDetailsIcon_1.IconType.leave,
        ConversationDetailsIcon_1.IconType.down,
    ];
    const onClick = (0, addon_actions_1.action)('onClick');
    return icons.map(icon => (React.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, Object.assign({}, createProps({ icon, onClick })))));
});
