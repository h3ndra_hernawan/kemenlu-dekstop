"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationDetailsMembershipList = void 0;
const react_1 = __importDefault(require("react"));
const getOwn_1 = require("../../../util/getOwn");
const Avatar_1 = require("../../Avatar");
const Emojify_1 = require("../Emojify");
const ConversationDetailsIcon_1 = require("./ConversationDetailsIcon");
const PanelRow_1 = require("./PanelRow");
const PanelSection_1 = require("./PanelSection");
const collator = new Intl.Collator(undefined, { sensitivity: 'base' });
function sortConversationTitles(left, right) {
    const leftTitle = left.member.title;
    const rightTitle = right.member.title;
    return collator.compare(leftTitle, rightTitle);
}
function sortMemberships(memberships) {
    let you;
    const admins = [];
    const nonAdmins = [];
    memberships.forEach(membershipInfo => {
        const { isAdmin, member } = membershipInfo;
        if (member.isMe) {
            you = membershipInfo;
        }
        else if (isAdmin) {
            admins.push(membershipInfo);
        }
        else {
            nonAdmins.push(membershipInfo);
        }
    });
    admins.sort(sortConversationTitles);
    nonAdmins.sort(sortConversationTitles);
    const sortedMemberships = [];
    if (you) {
        sortedMemberships.push(you);
    }
    sortedMemberships.push(...admins);
    sortedMemberships.push(...nonAdmins);
    return sortedMemberships;
}
const ConversationDetailsMembershipList = ({ canAddNewMembers, conversationId, i18n, maxShownMemberCount = 5, memberships, preferredBadgeByConversation, showContactModal, startAddingNewMembers, theme, }) => {
    const [showAllMembers, setShowAllMembers] = react_1.default.useState(false);
    const sortedMemberships = sortMemberships(memberships);
    const shouldHideRestMembers = sortedMemberships.length - maxShownMemberCount > 1;
    const membersToShow = shouldHideRestMembers && !showAllMembers
        ? maxShownMemberCount
        : sortedMemberships.length;
    return (react_1.default.createElement(PanelSection_1.PanelSection, { title: i18n('ConversationDetailsMembershipList--title', [
            sortedMemberships.length.toString(),
        ]) },
        canAddNewMembers && (react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement("div", { className: "ConversationDetails-membership-list__add-members-icon" }), label: i18n('ConversationDetailsMembershipList--add-members'), onClick: () => startAddingNewMembers === null || startAddingNewMembers === void 0 ? void 0 : startAddingNewMembers() })),
        sortedMemberships.slice(0, membersToShow).map(({ isAdmin, member }) => (react_1.default.createElement(PanelRow_1.PanelRow, { key: member.id, onClick: () => showContactModal(member.id, conversationId), icon: react_1.default.createElement(Avatar_1.Avatar, Object.assign({ conversationType: "direct", badge: (0, getOwn_1.getOwn)(preferredBadgeByConversation, member.id), i18n: i18n, size: 32, theme: theme }, member)), label: react_1.default.createElement(Emojify_1.Emojify, { text: member.isMe ? i18n('you') : member.title }), right: isAdmin ? i18n('GroupV2--admin') : '' }))),
        showAllMembers === false && shouldHideRestMembers && (react_1.default.createElement(PanelRow_1.PanelRow, { className: "ConversationDetails-membership-list--show-all", icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('ConversationDetailsMembershipList--show-all'), icon: ConversationDetailsIcon_1.IconType.down }), onClick: () => setShowAllMembers(true), label: i18n('ConversationDetailsMembershipList--show-all') }))));
};
exports.ConversationDetailsMembershipList = ConversationDetailsMembershipList;
