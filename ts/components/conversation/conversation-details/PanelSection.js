"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PanelSection = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const util_1 = require("./util");
const bem = (0, util_1.bemGenerator)('ConversationDetails-panel-section');
const borderlessClass = bem('root', 'borderless');
const PanelSection = ({ actions, borderless, centerTitle, children, title, }) => (react_1.default.createElement("div", { className: (0, classnames_1.default)(bem('root'), borderless ? borderlessClass : null) },
    (title || actions) && (react_1.default.createElement("div", { className: bem('header', { center: centerTitle || false }) },
        title && react_1.default.createElement("div", { className: bem('title') }, title),
        actions && react_1.default.createElement("div", null, actions))),
    react_1.default.createElement("div", null, children)));
exports.PanelSection = PanelSection;
