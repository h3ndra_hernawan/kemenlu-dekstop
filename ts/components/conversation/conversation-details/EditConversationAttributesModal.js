"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EditConversationAttributesModal = void 0;
const react_1 = __importStar(require("react"));
const Modal_1 = require("../../Modal");
const AvatarEditor_1 = require("../../AvatarEditor");
const AvatarPreview_1 = require("../../AvatarPreview");
const Button_1 = require("../../Button");
const Spinner_1 = require("../../Spinner");
const GroupDescriptionInput_1 = require("../../GroupDescriptionInput");
const GroupTitleInput_1 = require("../../GroupTitleInput");
const util_1 = require("./util");
const EditConversationAttributesModal = ({ avatarColor, avatarPath: externalAvatarPath, conversationId, groupDescription: externalGroupDescription = '', i18n, initiallyFocusDescription, makeRequest, onClose, requestState, title: externalTitle, deleteAvatarFromDisk, replaceAvatar, saveAvatarToDisk, userAvatarData, }) => {
    const focusDescriptionRef = (0, react_1.useRef)(initiallyFocusDescription);
    const focusDescription = focusDescriptionRef.current;
    const startingTitleRef = (0, react_1.useRef)(externalTitle);
    const startingAvatarPathRef = (0, react_1.useRef)(externalAvatarPath);
    const [editingAvatar, setEditingAvatar] = (0, react_1.useState)(false);
    const [avatar, setAvatar] = (0, react_1.useState)();
    const [rawTitle, setRawTitle] = (0, react_1.useState)(externalTitle);
    const [rawGroupDescription, setRawGroupDescription] = (0, react_1.useState)(externalGroupDescription);
    const [hasAvatarChanged, setHasAvatarChanged] = (0, react_1.useState)(false);
    const trimmedTitle = rawTitle.trim();
    const trimmedDescription = rawGroupDescription.trim();
    const focusRef = (el) => {
        if (el) {
            el.focus();
            focusDescriptionRef.current = undefined;
        }
    };
    const hasChangedExternally = startingAvatarPathRef.current !== externalAvatarPath ||
        startingTitleRef.current !== externalTitle;
    const hasTitleChanged = trimmedTitle !== externalTitle.trim();
    const hasGroupDescriptionChanged = externalGroupDescription.trim() !== trimmedDescription;
    const isRequestActive = requestState === util_1.RequestState.Active;
    const canSubmit = !isRequestActive &&
        (hasChangedExternally ||
            hasTitleChanged ||
            hasAvatarChanged ||
            hasGroupDescriptionChanged) &&
        trimmedTitle.length > 0;
    const onSubmit = event => {
        event.preventDefault();
        const request = {};
        if (hasAvatarChanged) {
            request.avatar = avatar;
        }
        if (hasTitleChanged) {
            request.title = trimmedTitle;
        }
        if (hasGroupDescriptionChanged) {
            request.description = trimmedDescription;
        }
        makeRequest(request);
    };
    const avatarPathForPreview = hasAvatarChanged
        ? undefined
        : externalAvatarPath;
    let content;
    if (editingAvatar) {
        content = (react_1.default.createElement(AvatarEditor_1.AvatarEditor, { avatarColor: avatarColor, avatarPath: avatarPathForPreview, avatarValue: avatar, conversationId: conversationId, deleteAvatarFromDisk: deleteAvatarFromDisk, i18n: i18n, isGroup: true, onCancel: () => {
                setHasAvatarChanged(false);
                setEditingAvatar(false);
            }, onSave: newAvatar => {
                setAvatar(newAvatar);
                setHasAvatarChanged(true);
                setEditingAvatar(false);
            }, userAvatarData: userAvatarData, replaceAvatar: replaceAvatar, saveAvatarToDisk: saveAvatarToDisk }));
    }
    else {
        content = (react_1.default.createElement("form", { onSubmit: onSubmit, className: "module-EditConversationAttributesModal" },
            react_1.default.createElement(AvatarPreview_1.AvatarPreview, { avatarColor: avatarColor, avatarPath: avatarPathForPreview, avatarValue: avatar, i18n: i18n, isEditable: true, isGroup: true, onClick: () => {
                    setEditingAvatar(true);
                }, style: {
                    height: 96,
                    width: 96,
                } }),
            react_1.default.createElement(GroupTitleInput_1.GroupTitleInput, { disabled: isRequestActive, i18n: i18n, onChangeValue: setRawTitle, ref: focusDescription === false ? focusRef : undefined, value: rawTitle }),
            react_1.default.createElement(GroupDescriptionInput_1.GroupDescriptionInput, { disabled: isRequestActive, i18n: i18n, onChangeValue: setRawGroupDescription, ref: focusDescription === true ? focusRef : undefined, value: rawGroupDescription }),
            react_1.default.createElement("div", { className: "module-EditConversationAttributesModal__description-warning" }, i18n('EditConversationAttributesModal__description-warning')),
            requestState === util_1.RequestState.InactiveWithError && (react_1.default.createElement("div", { className: "module-EditConversationAttributesModal__error-message" }, i18n('updateGroupAttributes__error-message'))),
            react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
                react_1.default.createElement(Button_1.Button, { disabled: isRequestActive, onClick: onClose, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
                react_1.default.createElement(Button_1.Button, { type: "submit", variant: Button_1.ButtonVariant.Primary, disabled: !canSubmit }, isRequestActive ? (react_1.default.createElement(Spinner_1.Spinner, { size: "20px", svgSize: "small", direction: "on-avatar" })) : (i18n('save'))))));
    }
    return (react_1.default.createElement(Modal_1.Modal, { hasStickyButtons: true, hasXButton: true, i18n: i18n, onClose: onClose, title: i18n('updateGroupAttributes__title') }, content));
};
exports.EditConversationAttributesModal = EditConversationAttributesModal;
