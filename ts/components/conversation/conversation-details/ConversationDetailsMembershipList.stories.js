"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const getFakeBadge_1 = require("../../../test-both/helpers/getFakeBadge");
const Util_1 = require("../../../types/Util");
const ConversationDetailsMembershipList_1 = require("./ConversationDetailsMembershipList");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ConversationDetails/ConversationDetailsMembershipList', module);
const createMemberships = (numberOfMemberships = 10) => {
    return Array.from(new Array((0, addon_knobs_1.number)('number of memberships', numberOfMemberships))).map((_, i) => ({
        isAdmin: i % 3 === 0,
        member: (0, getDefaultConversation_1.getDefaultConversation)({
            isMe: i === 2,
        }),
    }));
};
const createProps = (overrideProps) => ({
    canAddNewMembers: (0, lodash_1.isBoolean)(overrideProps.canAddNewMembers)
        ? overrideProps.canAddNewMembers
        : false,
    conversationId: '123',
    i18n,
    memberships: overrideProps.memberships || [],
    preferredBadgeByConversation: overrideProps.preferredBadgeByConversation ||
        (overrideProps.memberships || []).reduce((result, { member }, index) => (index + 1) % 3 === 0
            ? Object.assign(Object.assign({}, result), { [member.id]: (0, getFakeBadge_1.getFakeBadge)({ alternate: index % 2 !== 0 }) }) : result, {}),
    showContactModal: (0, addon_actions_1.action)('showContactModal'),
    startAddingNewMembers: (0, addon_actions_1.action)('startAddingNewMembers'),
    theme: Util_1.ThemeType.light,
});
story.add('Few', () => {
    const memberships = createMemberships(3);
    const props = createProps({ memberships });
    return React.createElement(ConversationDetailsMembershipList_1.ConversationDetailsMembershipList, Object.assign({}, props));
});
story.add('Limit', () => {
    const memberships = createMemberships(5);
    const props = createProps({ memberships });
    return React.createElement(ConversationDetailsMembershipList_1.ConversationDetailsMembershipList, Object.assign({}, props));
});
story.add('Limit +1', () => {
    const memberships = createMemberships(6);
    const props = createProps({ memberships });
    return React.createElement(ConversationDetailsMembershipList_1.ConversationDetailsMembershipList, Object.assign({}, props));
});
story.add('Limit +2', () => {
    const memberships = createMemberships(7);
    const props = createProps({ memberships });
    return React.createElement(ConversationDetailsMembershipList_1.ConversationDetailsMembershipList, Object.assign({}, props));
});
story.add('Many', () => {
    const memberships = createMemberships(100);
    const props = createProps({ memberships });
    return React.createElement(ConversationDetailsMembershipList_1.ConversationDetailsMembershipList, Object.assign({}, props));
});
story.add('None', () => {
    const props = createProps({ memberships: [] });
    return React.createElement(ConversationDetailsMembershipList_1.ConversationDetailsMembershipList, Object.assign({}, props));
});
story.add('Can add new members', () => {
    const memberships = createMemberships(10);
    const props = createProps({ canAddNewMembers: true, memberships });
    return React.createElement(ConversationDetailsMembershipList_1.ConversationDetailsMembershipList, Object.assign({}, props));
});
