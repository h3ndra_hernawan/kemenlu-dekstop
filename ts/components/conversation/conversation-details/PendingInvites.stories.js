"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const UUID_1 = require("../../../types/UUID");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const PendingInvites_1 = require("./PendingInvites");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ConversationDetails/PendingInvites', module);
const sortedGroupMembers = Array.from(Array(32)).map((_, i) => i === 0
    ? (0, getDefaultConversation_1.getDefaultConversation)({ id: 'def456' })
    : (0, getDefaultConversation_1.getDefaultConversation)({}));
const conversation = {
    acceptedMessageRequest: true,
    areWeAdmin: true,
    badges: [],
    id: '',
    lastUpdated: 0,
    markedUnread: false,
    isMe: false,
    sortedGroupMembers,
    title: 'Some Conversation',
    type: 'group',
    sharedGroupNames: [],
};
const OUR_UUID = UUID_1.UUID.generate().toString();
const createProps = () => ({
    approvePendingMembership: (0, addon_actions_1.action)('approvePendingMembership'),
    conversation,
    i18n,
    ourUuid: OUR_UUID,
    pendingApprovalMemberships: (0, lodash_1.times)(5, () => ({
        member: (0, getDefaultConversation_1.getDefaultConversation)(),
    })),
    pendingMemberships: [
        ...(0, lodash_1.times)(4, () => ({
            member: (0, getDefaultConversation_1.getDefaultConversation)(),
            metadata: {
                addedByUserId: OUR_UUID,
            },
        })),
        ...(0, lodash_1.times)(8, () => ({
            member: (0, getDefaultConversation_1.getDefaultConversation)(),
            metadata: {
                addedByUserId: UUID_1.UUID.generate().toString(),
            },
        })),
    ],
    revokePendingMemberships: (0, addon_actions_1.action)('revokePendingMemberships'),
});
story.add('Basic', () => {
    const props = createProps();
    return React.createElement(PendingInvites_1.PendingInvites, Object.assign({}, props));
});
