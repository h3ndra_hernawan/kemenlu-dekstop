"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationDetailsHeader = void 0;
const react_1 = __importStar(require("react"));
const Avatar_1 = require("../../Avatar");
const AvatarLightbox_1 = require("../../AvatarLightbox");
const Emojify_1 = require("../Emojify");
const GroupDescription_1 = require("../GroupDescription");
const About_1 = require("../About");
const util_1 = require("./util");
const BadgeDialog_1 = require("../../BadgeDialog");
const shouldShowBadges_1 = require("../../../badges/shouldShowBadges");
var ConversationDetailsHeaderActiveModal;
(function (ConversationDetailsHeaderActiveModal) {
    ConversationDetailsHeaderActiveModal[ConversationDetailsHeaderActiveModal["ShowingAvatar"] = 0] = "ShowingAvatar";
    ConversationDetailsHeaderActiveModal[ConversationDetailsHeaderActiveModal["ShowingBadges"] = 1] = "ShowingBadges";
})(ConversationDetailsHeaderActiveModal || (ConversationDetailsHeaderActiveModal = {}));
const bem = (0, util_1.bemGenerator)('ConversationDetails-header');
const ConversationDetailsHeader = ({ badges, canEdit, conversation, i18n, isGroup, isMe, memberships, startEditing, theme, }) => {
    const [activeModal, setActiveModal] = (0, react_1.useState)();
    let preferredBadge;
    let subtitle;
    if (isGroup) {
        if (conversation.groupDescription) {
            subtitle = (react_1.default.createElement(GroupDescription_1.GroupDescription, { i18n: i18n, text: conversation.groupDescription, title: conversation.title }));
        }
        else if (canEdit) {
            subtitle = i18n('ConversationDetailsHeader--add-group-description');
        }
        else {
            subtitle = i18n('ConversationDetailsHeader--members', [
                memberships.length.toString(),
            ]);
        }
    }
    else if (!isMe) {
        subtitle = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: bem('subtitle__about') },
                react_1.default.createElement(About_1.About, { text: conversation.about })),
            react_1.default.createElement("div", { className: bem('subtitle__phone-number') }, conversation.phoneNumber)));
        preferredBadge = badges === null || badges === void 0 ? void 0 : badges[0];
    }
    const avatar = (react_1.default.createElement(Avatar_1.Avatar, Object.assign({ badge: preferredBadge, conversationType: conversation.type, i18n: i18n, size: 80 }, conversation, { noteToSelf: isMe, onClick: () => {
            setActiveModal(preferredBadge && (0, shouldShowBadges_1.shouldShowBadges)()
                ? ConversationDetailsHeaderActiveModal.ShowingBadges
                : ConversationDetailsHeaderActiveModal.ShowingAvatar);
        }, sharedGroupNames: [], theme: theme })));
    const contents = (react_1.default.createElement("div", null,
        react_1.default.createElement("div", { className: bem('title') },
            react_1.default.createElement(Emojify_1.Emojify, { text: isMe ? i18n('noteToSelf') : conversation.title }))));
    let modal;
    switch (activeModal) {
        case ConversationDetailsHeaderActiveModal.ShowingAvatar:
            modal = (react_1.default.createElement(AvatarLightbox_1.AvatarLightbox, { avatarColor: conversation.color, avatarPath: conversation.avatarPath, conversationTitle: conversation.title, i18n: i18n, isGroup: isGroup, onClose: () => {
                    setActiveModal(undefined);
                } }));
            break;
        case ConversationDetailsHeaderActiveModal.ShowingBadges:
            modal = (react_1.default.createElement(BadgeDialog_1.BadgeDialog, { badges: badges || [], firstName: conversation.firstName, i18n: i18n, onClose: () => {
                    setActiveModal(undefined);
                }, title: conversation.title }));
            break;
        default:
            modal = null;
            break;
    }
    if (canEdit) {
        return (react_1.default.createElement("div", { className: bem('root') },
            modal,
            avatar,
            react_1.default.createElement("button", { type: "button", onClick: ev => {
                    ev.preventDefault();
                    ev.stopPropagation();
                    startEditing(true);
                }, className: bem('root', 'editable') }, contents),
            react_1.default.createElement("button", { type: "button", onClick: ev => {
                    if (ev.target instanceof HTMLAnchorElement) {
                        return;
                    }
                    ev.preventDefault();
                    ev.stopPropagation();
                    startEditing(false);
                }, className: bem('root', 'editable') },
                react_1.default.createElement("div", { className: bem('subtitle') }, subtitle))));
    }
    return (react_1.default.createElement("div", { className: bem('root') },
        modal,
        avatar,
        contents,
        react_1.default.createElement("div", { className: bem('subtitle') }, subtitle)));
};
exports.ConversationDetailsHeader = ConversationDetailsHeader;
