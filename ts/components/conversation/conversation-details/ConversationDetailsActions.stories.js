"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const ConversationDetailsActions_1 = require("./ConversationDetailsActions");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ConversationDetails/ConversationDetailsActions', module);
const createProps = (overrideProps = {}) => ({
    cannotLeaveBecauseYouAreLastAdmin: (0, lodash_1.isBoolean)(overrideProps.cannotLeaveBecauseYouAreLastAdmin)
        ? overrideProps.cannotLeaveBecauseYouAreLastAdmin
        : false,
    conversationTitle: overrideProps.conversationTitle || '',
    left: (0, lodash_1.isBoolean)(overrideProps.left) ? overrideProps.left : false,
    onBlock: (0, addon_actions_1.action)('onBlock'),
    onLeave: (0, addon_actions_1.action)('onLeave'),
    onUnblock: (0, addon_actions_1.action)('onUnblock'),
    i18n,
    isBlocked: false,
    isGroup: true,
});
story.add('Basic', () => {
    const props = createProps();
    return React.createElement(ConversationDetailsActions_1.ConversationDetailsActions, Object.assign({}, props));
});
story.add('Left the group', () => {
    const props = createProps({ left: true });
    return React.createElement(ConversationDetailsActions_1.ConversationDetailsActions, Object.assign({}, props));
});
story.add('Cannot leave because you are the last admin', () => {
    const props = createProps({ cannotLeaveBecauseYouAreLastAdmin: true });
    return React.createElement(ConversationDetailsActions_1.ConversationDetailsActions, Object.assign({}, props));
});
story.add('1:1', () => (React.createElement(ConversationDetailsActions_1.ConversationDetailsActions, Object.assign({}, createProps(), { isGroup: false }))));
story.add('1:1 Blocked', () => (React.createElement(ConversationDetailsActions_1.ConversationDetailsActions, Object.assign({}, createProps(), { isGroup: false, isBlocked: true }))));
