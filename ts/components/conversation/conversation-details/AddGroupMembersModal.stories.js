"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const sleep_1 = require("../../../util/sleep");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const AddGroupMembersModal_1 = require("./AddGroupMembersModal");
const util_1 = require("./util");
const Util_1 = require("../../../types/Util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/Conversation/ConversationDetails/AddGroupMembersModal', module);
const allCandidateContacts = (0, lodash_1.times)(50, () => (0, getDefaultConversation_1.getDefaultConversation)());
const createProps = (overrideProps = {}) => (Object.assign({ candidateContacts: allCandidateContacts, clearRequestError: (0, addon_actions_1.action)('clearRequestError'), conversationIdsAlreadyInGroup: new Set(), groupTitle: 'Tahoe Trip', i18n, onClose: (0, addon_actions_1.action)('onClose'), makeRequest: async (conversationIds) => {
        (0, addon_actions_1.action)('onMakeRequest')(conversationIds);
    }, requestState: util_1.RequestState.Inactive, theme: Util_1.ThemeType.light }, overrideProps));
story.add('Default', () => react_1.default.createElement(AddGroupMembersModal_1.AddGroupMembersModal, Object.assign({}, createProps())));
story.add('Only 3 contacts', () => (react_1.default.createElement(AddGroupMembersModal_1.AddGroupMembersModal, Object.assign({}, createProps({
    candidateContacts: allCandidateContacts.slice(0, 3),
})))));
story.add('No candidate contacts', () => (react_1.default.createElement(AddGroupMembersModal_1.AddGroupMembersModal, Object.assign({}, createProps({
    candidateContacts: [],
})))));
story.add('Everyone already added', () => (react_1.default.createElement(AddGroupMembersModal_1.AddGroupMembersModal, Object.assign({}, createProps({
    conversationIdsAlreadyInGroup: new Set(allCandidateContacts.map(contact => contact.id)),
})))));
story.add('Request fails after 1 second', () => {
    const Wrapper = () => {
        const [requestState, setRequestState] = (0, react_1.useState)(util_1.RequestState.Inactive);
        return (react_1.default.createElement(AddGroupMembersModal_1.AddGroupMembersModal, Object.assign({}, createProps({
            clearRequestError: () => {
                setRequestState(util_1.RequestState.Inactive);
            },
            makeRequest: async () => {
                setRequestState(util_1.RequestState.Active);
                await (0, sleep_1.sleep)(1000);
                setRequestState(util_1.RequestState.InactiveWithError);
            },
            requestState,
        }))));
    };
    return react_1.default.createElement(Wrapper, null);
});
