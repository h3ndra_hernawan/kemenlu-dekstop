"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationDetailsMediaList = void 0;
const react_1 = __importDefault(require("react"));
const PanelSection_1 = require("./PanelSection");
const util_1 = require("./util");
const MediaGridItem_1 = require("../media-gallery/MediaGridItem");
const MEDIA_ITEM_LIMIT = 6;
const bem = (0, util_1.bemGenerator)('ConversationDetails-media-list');
const ConversationDetailsMediaList = ({ conversation, i18n, loadRecentMediaItems, showAllMedia, showLightboxForMedia, }) => {
    const mediaItems = conversation.recentMediaItems || [];
    const mediaItemsLength = mediaItems.length;
    react_1.default.useEffect(() => {
        loadRecentMediaItems(MEDIA_ITEM_LIMIT);
    }, [loadRecentMediaItems, mediaItemsLength]);
    if (mediaItemsLength === 0) {
        return null;
    }
    return (react_1.default.createElement(PanelSection_1.PanelSection, { actions: react_1.default.createElement("button", { className: bem('show-all'), onClick: showAllMedia, type: "button" }, i18n('ConversationDetailsMediaList--show-all')), title: i18n('ConversationDetailsMediaList--shared-media') },
        react_1.default.createElement("div", { className: bem('root') }, mediaItems.slice(0, MEDIA_ITEM_LIMIT).map(mediaItem => (react_1.default.createElement(MediaGridItem_1.MediaGridItem, { key: `${mediaItem.message.id}-${mediaItem.index}`, mediaItem: mediaItem, i18n: i18n, onClick: () => showLightboxForMedia(mediaItem, mediaItems) }))))));
};
exports.ConversationDetailsMediaList = ConversationDetailsMediaList;
