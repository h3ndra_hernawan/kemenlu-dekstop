"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationDetails = void 0;
const react_1 = __importStar(require("react"));
const Button_1 = require("../../Button");
const assert_1 = require("../../../util/assert");
const getMutedUntilText_1 = require("../../../util/getMutedUntilText");
const errors_1 = require("../../../types/errors");
const missingCaseError_1 = require("../../../util/missingCaseError");
const DisappearingTimerSelect_1 = require("../../DisappearingTimerSelect");
const PanelRow_1 = require("./PanelRow");
const PanelSection_1 = require("./PanelSection");
const AddGroupMembersModal_1 = require("./AddGroupMembersModal");
const ConversationDetailsActions_1 = require("./ConversationDetailsActions");
const ConversationDetailsHeader_1 = require("./ConversationDetailsHeader");
const ConversationDetailsIcon_1 = require("./ConversationDetailsIcon");
const ConversationDetailsMediaList_1 = require("./ConversationDetailsMediaList");
const ConversationDetailsMembershipList_1 = require("./ConversationDetailsMembershipList");
const EditConversationAttributesModal_1 = require("./EditConversationAttributesModal");
const util_1 = require("./util");
const getCustomColorStyle_1 = require("../../../util/getCustomColorStyle");
const ConfirmationDialog_1 = require("../../ConfirmationDialog");
const ConversationNotificationsModal_1 = require("./ConversationNotificationsModal");
const isMuted_1 = require("../../../util/isMuted");
var ModalState;
(function (ModalState) {
    ModalState[ModalState["NothingOpen"] = 0] = "NothingOpen";
    ModalState[ModalState["EditingGroupDescription"] = 1] = "EditingGroupDescription";
    ModalState[ModalState["EditingGroupTitle"] = 2] = "EditingGroupTitle";
    ModalState[ModalState["AddingGroupMembers"] = 3] = "AddingGroupMembers";
    ModalState[ModalState["MuteNotifications"] = 4] = "MuteNotifications";
    ModalState[ModalState["UnmuteNotifications"] = 5] = "UnmuteNotifications";
})(ModalState || (ModalState = {}));
const ConversationDetails = ({ addMembers, badges, canEditGroupInfo, candidateContactsToAdd, conversation, deleteAvatarFromDisk, hasGroupLink, i18n, isAdmin, isGroup, loadRecentMediaItems, memberships, onBlock, onLeave, onOutgoingAudioCallInConversation, onOutgoingVideoCallInConversation, onUnblock, pendingApprovalMemberships, pendingMemberships, preferredBadgeByConversation, replaceAvatar, saveAvatarToDisk, searchInConversation, setDisappearingMessages, setMuteExpiration, showAllMedia, showChatColorEditor, showContactModal, showConversationNotificationsSettings, showGroupLinkManagement, showGroupV2Permissions, showLightboxForMedia, showPendingInvites, theme, toggleSafetyNumberModal, updateGroupAttributes, userAvatarData, }) => {
    const [modalState, setModalState] = (0, react_1.useState)(ModalState.NothingOpen);
    const [editGroupAttributesRequestState, setEditGroupAttributesRequestState] = (0, react_1.useState)(util_1.RequestState.Inactive);
    const [addGroupMembersRequestState, setAddGroupMembersRequestState] = (0, react_1.useState)(util_1.RequestState.Inactive);
    const [membersMissingCapability, setMembersMissingCapability] = (0, react_1.useState)(false);
    if (conversation === undefined) {
        throw new Error('ConversationDetails rendered without a conversation');
    }
    const invitesCount = pendingMemberships.length + pendingApprovalMemberships.length;
    const otherMemberships = memberships.filter(({ member }) => !member.isMe);
    const isJustMe = otherMemberships.length === 0;
    const isAnyoneElseAnAdmin = otherMemberships.some(membership => membership.isAdmin);
    const cannotLeaveBecauseYouAreLastAdmin = isAdmin && !isJustMe && !isAnyoneElseAnAdmin;
    let modalNode;
    switch (modalState) {
        case ModalState.NothingOpen:
            modalNode = undefined;
            break;
        case ModalState.EditingGroupDescription:
        case ModalState.EditingGroupTitle:
            modalNode = (react_1.default.createElement(EditConversationAttributesModal_1.EditConversationAttributesModal, { avatarColor: conversation.color, avatarPath: conversation.avatarPath, conversationId: conversation.id, groupDescription: conversation.groupDescription, i18n: i18n, initiallyFocusDescription: modalState === ModalState.EditingGroupDescription, makeRequest: async (options) => {
                    setEditGroupAttributesRequestState(util_1.RequestState.Active);
                    try {
                        await updateGroupAttributes(options);
                        setModalState(ModalState.NothingOpen);
                        setEditGroupAttributesRequestState(util_1.RequestState.Inactive);
                    }
                    catch (err) {
                        setEditGroupAttributesRequestState(util_1.RequestState.InactiveWithError);
                    }
                }, onClose: () => {
                    setModalState(ModalState.NothingOpen);
                    setEditGroupAttributesRequestState(util_1.RequestState.Inactive);
                }, requestState: editGroupAttributesRequestState, title: conversation.title, deleteAvatarFromDisk: deleteAvatarFromDisk, replaceAvatar: replaceAvatar, saveAvatarToDisk: saveAvatarToDisk, userAvatarData: userAvatarData }));
            break;
        case ModalState.AddingGroupMembers:
            modalNode = (react_1.default.createElement(AddGroupMembersModal_1.AddGroupMembersModal, { candidateContacts: candidateContactsToAdd, clearRequestError: () => {
                    setAddGroupMembersRequestState(oldRequestState => {
                        (0, assert_1.assert)(oldRequestState !== util_1.RequestState.Active, 'Should not be clearing an active request state');
                        return util_1.RequestState.Inactive;
                    });
                }, conversationIdsAlreadyInGroup: new Set(memberships.map(membership => membership.member.id)), groupTitle: conversation.title, i18n: i18n, makeRequest: async (conversationIds) => {
                    setAddGroupMembersRequestState(util_1.RequestState.Active);
                    try {
                        await addMembers(conversationIds);
                        setModalState(ModalState.NothingOpen);
                        setAddGroupMembersRequestState(util_1.RequestState.Inactive);
                    }
                    catch (err) {
                        if (err instanceof errors_1.CapabilityError) {
                            setMembersMissingCapability(true);
                            setAddGroupMembersRequestState(util_1.RequestState.InactiveWithError);
                        }
                        else {
                            setAddGroupMembersRequestState(util_1.RequestState.InactiveWithError);
                        }
                    }
                }, onClose: () => {
                    setModalState(ModalState.NothingOpen);
                    setEditGroupAttributesRequestState(util_1.RequestState.Inactive);
                }, requestState: addGroupMembersRequestState, theme: theme }));
            break;
        case ModalState.MuteNotifications:
            modalNode = (react_1.default.createElement(ConversationNotificationsModal_1.ConversationNotificationsModal, { i18n: i18n, muteExpiresAt: conversation.muteExpiresAt, onClose: () => {
                    setModalState(ModalState.NothingOpen);
                }, setMuteExpiration: setMuteExpiration }));
            break;
        case ModalState.UnmuteNotifications:
            modalNode = (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                    {
                        action: () => setMuteExpiration(0),
                        style: 'affirmative',
                        text: i18n('unmute'),
                    },
                ], hasXButton: true, i18n: i18n, title: i18n('ConversationDetails__unmute--title'), onClose: () => {
                    setModalState(ModalState.NothingOpen);
                } }, (0, getMutedUntilText_1.getMutedUntilText)(Number(conversation.muteExpiresAt), i18n)));
            break;
        default:
            throw (0, missingCaseError_1.missingCaseError)(modalState);
    }
    const isConversationMuted = (0, isMuted_1.isMuted)(conversation.muteExpiresAt);
    return (react_1.default.createElement("div", { className: "conversation-details-panel" },
        membersMissingCapability && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { cancelText: i18n('Confirmation--confirm'), i18n: i18n, onClose: () => setMembersMissingCapability(false) }, i18n('GroupV2--add--missing-capability'))),
        react_1.default.createElement(ConversationDetailsHeader_1.ConversationDetailsHeader, { badges: badges, canEdit: canEditGroupInfo, conversation: conversation, i18n: i18n, isMe: conversation.isMe, isGroup: isGroup, memberships: memberships, startEditing: (isGroupTitle) => {
                setModalState(isGroupTitle
                    ? ModalState.EditingGroupTitle
                    : ModalState.EditingGroupDescription);
            }, theme: theme }),
        react_1.default.createElement("div", { className: "ConversationDetails__header-buttons" },
            !conversation.isMe && (react_1.default.createElement(react_1.default.Fragment, null,
                react_1.default.createElement(Button_1.Button, { icon: Button_1.ButtonIconType.video, onClick: onOutgoingVideoCallInConversation, variant: Button_1.ButtonVariant.Details }, i18n('video')),
                !isGroup && (react_1.default.createElement(Button_1.Button, { icon: Button_1.ButtonIconType.audio, onClick: onOutgoingAudioCallInConversation, variant: Button_1.ButtonVariant.Details }, i18n('audio'))))),
            react_1.default.createElement(Button_1.Button, { icon: isConversationMuted ? Button_1.ButtonIconType.muted : Button_1.ButtonIconType.unmuted, onClick: () => {
                    if (isConversationMuted) {
                        setModalState(ModalState.UnmuteNotifications);
                    }
                    else {
                        setModalState(ModalState.MuteNotifications);
                    }
                }, variant: Button_1.ButtonVariant.Details }, isConversationMuted ? i18n('unmute') : i18n('mute')),
            react_1.default.createElement(Button_1.Button, { icon: Button_1.ButtonIconType.search, onClick: () => {
                    searchInConversation(conversation.id);
                }, variant: Button_1.ButtonVariant.Details }, i18n('search'))),
        react_1.default.createElement(PanelSection_1.PanelSection, null,
            !isGroup || canEditGroupInfo ? (react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('ConversationDetails--disappearing-messages-label'), icon: ConversationDetailsIcon_1.IconType.timer }), info: i18n('ConversationDetails--disappearing-messages-info'), label: i18n('ConversationDetails--disappearing-messages-label'), right: react_1.default.createElement(DisappearingTimerSelect_1.DisappearingTimerSelect, { i18n: i18n, value: conversation.expireTimer || 0, onChange: setDisappearingMessages }) })) : null,
            react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('showChatColorEditor'), icon: ConversationDetailsIcon_1.IconType.color }), label: i18n('showChatColorEditor'), onClick: showChatColorEditor, right: react_1.default.createElement("div", { className: `ConversationDetails__chat-color ConversationDetails__chat-color--${conversation.conversationColor}`, style: Object.assign({}, (0, getCustomColorStyle_1.getCustomColorStyle)(conversation.customColor)) }) }),
            isGroup && (react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('ConversationDetails--notifications'), icon: ConversationDetailsIcon_1.IconType.notifications }), label: i18n('ConversationDetails--notifications'), onClick: showConversationNotificationsSettings, right: conversation.muteExpiresAt
                    ? (0, getMutedUntilText_1.getMutedUntilText)(conversation.muteExpiresAt, i18n)
                    : undefined })),
            !isGroup && !conversation.isMe && (react_1.default.createElement(react_1.default.Fragment, null,
                react_1.default.createElement(PanelRow_1.PanelRow, { onClick: () => toggleSafetyNumberModal(conversation.id), icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('verifyNewNumber'), icon: ConversationDetailsIcon_1.IconType.verify }), label: react_1.default.createElement("div", { className: "ConversationDetails__safety-number" }, i18n('verifyNewNumber')) })))),
        isGroup && (react_1.default.createElement(ConversationDetailsMembershipList_1.ConversationDetailsMembershipList, { canAddNewMembers: canEditGroupInfo, conversationId: conversation.id, i18n: i18n, memberships: memberships, preferredBadgeByConversation: preferredBadgeByConversation, showContactModal: showContactModal, startAddingNewMembers: () => {
                setModalState(ModalState.AddingGroupMembers);
            }, theme: theme })),
        isGroup && (react_1.default.createElement(PanelSection_1.PanelSection, null,
            isAdmin || hasGroupLink ? (react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('ConversationDetails--group-link'), icon: ConversationDetailsIcon_1.IconType.link }), label: i18n('ConversationDetails--group-link'), onClick: showGroupLinkManagement, right: hasGroupLink ? i18n('on') : i18n('off') })) : null,
            react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('ConversationDetails--requests-and-invites'), icon: ConversationDetailsIcon_1.IconType.invites }), label: i18n('ConversationDetails--requests-and-invites'), onClick: showPendingInvites, right: invitesCount }),
            isAdmin ? (react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('permissions'), icon: ConversationDetailsIcon_1.IconType.lock }), label: i18n('permissions'), onClick: showGroupV2Permissions })) : null)),
        react_1.default.createElement(ConversationDetailsMediaList_1.ConversationDetailsMediaList, { conversation: conversation, i18n: i18n, loadRecentMediaItems: loadRecentMediaItems, showAllMedia: showAllMedia, showLightboxForMedia: showLightboxForMedia }),
        !conversation.isMe && (react_1.default.createElement(ConversationDetailsActions_1.ConversationDetailsActions, { cannotLeaveBecauseYouAreLastAdmin: cannotLeaveBecauseYouAreLastAdmin, conversationTitle: conversation.title, i18n: i18n, isBlocked: Boolean(conversation.isBlocked), isGroup: isGroup, left: Boolean(conversation.left), onBlock: onBlock, onLeave: onLeave, onUnblock: onUnblock })),
        modalNode));
};
exports.ConversationDetails = ConversationDetails;
