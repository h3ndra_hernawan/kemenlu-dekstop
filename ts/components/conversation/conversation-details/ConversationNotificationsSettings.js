"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationNotificationsSettings = void 0;
const react_1 = __importStar(require("react"));
const PanelSection_1 = require("./PanelSection");
const PanelRow_1 = require("./PanelRow");
const ConversationDetailsIcon_1 = require("./ConversationDetailsIcon");
const Select_1 = require("../../Select");
const isMuted_1 = require("../../../util/isMuted");
const getMuteOptions_1 = require("../../../util/getMuteOptions");
const parseIntOrThrow_1 = require("../../../util/parseIntOrThrow");
const ConversationNotificationsSettings = ({ conversationType, dontNotifyForMentionsIfMuted, i18n, muteExpiresAt, setMuteExpiration, setDontNotifyForMentionsIfMuted, }) => {
    const muteOptions = (0, react_1.useMemo)(() => [
        ...((0, isMuted_1.isMuted)(muteExpiresAt)
            ? []
            : [
                {
                    disabled: true,
                    text: i18n('notMuted'),
                    value: -1,
                },
            ]),
        ...(0, getMuteOptions_1.getMuteOptions)(muteExpiresAt, i18n).map(({ disabled, name, value }) => ({
            disabled,
            text: name,
            value,
        })),
    ], [i18n, muteExpiresAt]);
    const onMuteChange = (rawValue) => {
        const ms = (0, parseIntOrThrow_1.parseIntOrThrow)(rawValue, 'NotificationSettings: mute ms was not an integer');
        setMuteExpiration(ms);
    };
    const onChangeDontNotifyForMentionsIfMuted = (rawValue) => {
        setDontNotifyForMentionsIfMuted(rawValue === 'yes');
    };
    return (react_1.default.createElement("div", { className: "conversation-details-panel" },
        react_1.default.createElement(PanelSection_1.PanelSection, null,
            react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('muteNotificationsTitle'), icon: ConversationDetailsIcon_1.IconType.mute }), label: i18n('muteNotificationsTitle'), right: react_1.default.createElement(Select_1.Select, { options: muteOptions, onChange: onMuteChange, value: -1 }) }),
            conversationType === 'group' && (react_1.default.createElement(PanelRow_1.PanelRow, { icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('ConversationNotificationsSettings__mentions__label'), icon: ConversationDetailsIcon_1.IconType.mention }), label: i18n('ConversationNotificationsSettings__mentions__label'), info: i18n('ConversationNotificationsSettings__mentions__info'), right: react_1.default.createElement(Select_1.Select, { options: [
                        {
                            text: i18n('ConversationNotificationsSettings__mentions__select__always-notify'),
                            value: 'no',
                        },
                        {
                            text: i18n('ConversationNotificationsSettings__mentions__select__dont-notify-for-mentions-if-muted'),
                            value: 'yes',
                        },
                    ], onChange: onChangeDontNotifyForMentionsIfMuted, value: dontNotifyForMentionsIfMuted ? 'yes' : 'no' }) })))));
};
exports.ConversationNotificationsSettings = ConversationNotificationsSettings;
