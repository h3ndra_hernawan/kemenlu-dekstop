"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationDetailsActions = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const ConfirmationDialog_1 = require("../../ConfirmationDialog");
const Tooltip_1 = require("../../Tooltip");
const PanelRow_1 = require("./PanelRow");
const PanelSection_1 = require("./PanelSection");
const ConversationDetailsIcon_1 = require("./ConversationDetailsIcon");
const ConversationDetailsActions = ({ cannotLeaveBecauseYouAreLastAdmin, conversationTitle, i18n, isBlocked, isGroup, left, onBlock, onLeave, onUnblock, }) => {
    const [confirmLeave, gLeave] = (0, react_1.useState)(false);
    const [confirmGroupBlock, gGroupBlock] = (0, react_1.useState)(false);
    const [confirmDirectBlock, gDirectBlock] = (0, react_1.useState)(false);
    const [confirmDirectUnblock, gDirectUnblock] = (0, react_1.useState)(false);
    let leaveGroupNode;
    if (isGroup && !left) {
        leaveGroupNode = (react_1.default.createElement(PanelRow_1.PanelRow, { disabled: cannotLeaveBecauseYouAreLastAdmin, onClick: () => gLeave(true), icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('ConversationDetailsActions--leave-group'), disabled: cannotLeaveBecauseYouAreLastAdmin, icon: ConversationDetailsIcon_1.IconType.leave }), label: react_1.default.createElement("div", { className: (0, classnames_1.default)('ConversationDetails__leave-group', cannotLeaveBecauseYouAreLastAdmin &&
                    'ConversationDetails__leave-group--disabled') }, i18n('ConversationDetailsActions--leave-group')) }));
        if (cannotLeaveBecauseYouAreLastAdmin) {
            leaveGroupNode = (react_1.default.createElement(Tooltip_1.Tooltip, { content: i18n('ConversationDetailsActions--leave-group-must-choose-new-admin'), direction: Tooltip_1.TooltipPlacement.Top }, leaveGroupNode));
        }
    }
    let blockNode;
    if (isGroup) {
        blockNode = (react_1.default.createElement(PanelRow_1.PanelRow, { disabled: cannotLeaveBecauseYouAreLastAdmin, onClick: () => gGroupBlock(true), icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: i18n('ConversationDetailsActions--block-group'), icon: ConversationDetailsIcon_1.IconType.block }), label: react_1.default.createElement("div", { className: "ConversationDetails__block-group" }, i18n('ConversationDetailsActions--block-group')) }));
    }
    else {
        const label = isBlocked
            ? i18n('MessageRequests--unblock')
            : i18n('MessageRequests--block');
        blockNode = (react_1.default.createElement(PanelRow_1.PanelRow, { onClick: () => (isBlocked ? gDirectUnblock(true) : gDirectBlock(true)), icon: react_1.default.createElement(ConversationDetailsIcon_1.ConversationDetailsIcon, { ariaLabel: label, icon: ConversationDetailsIcon_1.IconType.block }), label: react_1.default.createElement("div", { className: "ConversationDetails__block-group" }, label) }));
    }
    if (cannotLeaveBecauseYouAreLastAdmin) {
        blockNode = (react_1.default.createElement(Tooltip_1.Tooltip, { content: i18n('ConversationDetailsActions--leave-group-must-choose-new-admin'), direction: Tooltip_1.TooltipPlacement.Top }, blockNode));
    }
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(PanelSection_1.PanelSection, null,
            leaveGroupNode,
            blockNode),
        confirmLeave && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                {
                    text: i18n('ConversationDetailsActions--leave-group-modal-confirm'),
                    action: onLeave,
                    style: 'affirmative',
                },
            ], i18n: i18n, onClose: () => gLeave(false), title: i18n('ConversationDetailsActions--leave-group-modal-title') }, i18n('ConversationDetailsActions--leave-group-modal-content'))),
        confirmGroupBlock && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                {
                    text: i18n('ConversationDetailsActions--block-group-modal-confirm'),
                    action: onBlock,
                    style: 'affirmative',
                },
            ], i18n: i18n, onClose: () => gGroupBlock(false), title: i18n('ConversationDetailsActions--block-group-modal-title', [
                conversationTitle,
            ]) }, i18n('ConversationDetailsActions--block-group-modal-content'))),
        confirmDirectBlock && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                {
                    text: i18n('MessageRequests--block'),
                    action: onBlock,
                    style: 'affirmative',
                },
            ], i18n: i18n, onClose: () => gDirectBlock(false), title: i18n('MessageRequests--block-direct-confirm-title', [
                conversationTitle,
            ]) }, i18n('MessageRequests--block-direct-confirm-body'))),
        confirmDirectUnblock && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                {
                    text: i18n('MessageRequests--unblock'),
                    action: onUnblock,
                    style: 'affirmative',
                },
            ], i18n: i18n, onClose: () => gDirectUnblock(false), title: i18n('MessageRequests--unblock-direct-confirm-title', [
                conversationTitle,
            ]) }, i18n('MessageRequests--unblock-direct-confirm-body')))));
};
exports.ConversationDetailsActions = ConversationDetailsActions;
