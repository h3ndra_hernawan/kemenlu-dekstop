"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfirmAdditionsModal = void 0;
const react_1 = __importDefault(require("react"));
const assert_1 = require("../../../../util/assert");
const ModalHost_1 = require("../../../ModalHost");
const Button_1 = require("../../../Button");
const Spinner_1 = require("../../../Spinner");
const util_1 = require("../util");
const Intl_1 = require("../../../Intl");
const Emojify_1 = require("../../Emojify");
const ContactName_1 = require("../../ContactName");
const ConfirmAdditionsModal = ({ groupTitle, i18n, makeRequest, onClose, requestState, selectedContacts, }) => {
    const firstContact = selectedContacts[0];
    (0, assert_1.assert)(firstContact, 'Expected at least one conversation to be selected but none were picked');
    const groupTitleNode = react_1.default.createElement(Emojify_1.Emojify, { text: groupTitle });
    let headerText;
    if (selectedContacts.length === 1) {
        headerText = (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "AddGroupMembersModal--confirm-title--one", components: {
                person: react_1.default.createElement(ContactName_1.ContactName, { title: firstContact.title }),
                group: groupTitleNode,
            } }));
    }
    else {
        headerText = (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "AddGroupMembersModal--confirm-title--many", components: {
                count: selectedContacts.length.toString(),
                group: groupTitleNode,
            } }));
    }
    let buttonContents;
    if (requestState === util_1.RequestState.Active) {
        buttonContents = (react_1.default.createElement(Spinner_1.Spinner, { size: "20px", svgSize: "small", direction: "on-avatar" }));
    }
    else if (selectedContacts.length === 1) {
        buttonContents = i18n('AddGroupMembersModal--confirm-button--one');
    }
    else {
        buttonContents = i18n('AddGroupMembersModal--confirm-button--many');
    }
    return (react_1.default.createElement(ModalHost_1.ModalHost, { onClose: onClose },
        react_1.default.createElement("div", { className: "module-AddGroupMembersModal module-AddGroupMembersModal--confirm-adds" },
            react_1.default.createElement("h1", { className: "module-AddGroupMembersModal__header" }, headerText),
            requestState === util_1.RequestState.InactiveWithError && (react_1.default.createElement("div", { className: "module-AddGroupMembersModal__error-message" }, i18n('updateGroupAttributes__error-message'))),
            react_1.default.createElement("div", { className: "module-AddGroupMembersModal__button-container" },
                react_1.default.createElement(Button_1.Button, { onClick: onClose, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
                react_1.default.createElement(Button_1.Button, { disabled: requestState === util_1.RequestState.Active, onClick: makeRequest, variant: Button_1.ButtonVariant.Primary }, buttonContents)))));
};
exports.ConfirmAdditionsModal = ConfirmAdditionsModal;
