"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChooseGroupMembersModal = void 0;
const react_1 = __importStar(require("react"));
const react_measure_1 = __importDefault(require("react-measure"));
const assert_1 = require("../../../../util/assert");
const getOwn_1 = require("../../../../util/getOwn");
const refMerger_1 = require("../../../../util/refMerger");
const useRestoreFocus_1 = require("../../../../hooks/useRestoreFocus");
const missingCaseError_1 = require("../../../../util/missingCaseError");
const filterAndSortConversations_1 = require("../../../../util/filterAndSortConversations");
const ModalHost_1 = require("../../../ModalHost");
const ContactPills_1 = require("../../../ContactPills");
const ContactPill_1 = require("../../../ContactPill");
const ConversationList_1 = require("../../../ConversationList");
const ContactCheckbox_1 = require("../../../conversationList/ContactCheckbox");
const Button_1 = require("../../../Button");
const SearchInput_1 = require("../../../SearchInput");
// TODO: This should use <Modal>. See DESKTOP-1038.
const ChooseGroupMembersModal = ({ candidateContacts, confirmAdds, contactLookup, conversationIdsAlreadyInGroup, i18n, maxGroupSize, onClose, removeSelectedContact, searchTerm, selectedContacts, setCantAddContactForModal, setSearchTerm, theme, toggleSelectedContact, }) => {
    const [focusRef] = (0, useRestoreFocus_1.useRestoreFocus)();
    const inputRef = (0, react_1.useRef)(null);
    const numberOfContactsAlreadyInGroup = conversationIdsAlreadyInGroup.size;
    const hasSelectedMaximumNumberOfContacts = selectedContacts.length + numberOfContactsAlreadyInGroup >= maxGroupSize;
    const selectedConversationIdsSet = (0, react_1.useMemo)(() => new Set(selectedContacts.map(contact => contact.id)), [selectedContacts]);
    const canContinue = Boolean(selectedContacts.length);
    const [filteredContacts, setFilteredContacts] = (0, react_1.useState)((0, filterAndSortConversations_1.filterAndSortConversationsByTitle)(candidateContacts, ''));
    const normalizedSearchTerm = searchTerm.trim();
    (0, react_1.useEffect)(() => {
        const timeout = setTimeout(() => {
            setFilteredContacts((0, filterAndSortConversations_1.filterAndSortConversationsByTitle)(candidateContacts, normalizedSearchTerm));
        }, 200);
        return () => {
            clearTimeout(timeout);
        };
    }, [candidateContacts, normalizedSearchTerm, setFilteredContacts]);
    const rowCount = filteredContacts.length;
    const getRow = (index) => {
        const contact = filteredContacts[index];
        if (!contact) {
            return undefined;
        }
        const isSelected = selectedConversationIdsSet.has(contact.id);
        const isAlreadyInGroup = conversationIdsAlreadyInGroup.has(contact.id);
        let disabledReason;
        if (isAlreadyInGroup) {
            disabledReason = ContactCheckbox_1.ContactCheckboxDisabledReason.AlreadyAdded;
        }
        else if (hasSelectedMaximumNumberOfContacts && !isSelected) {
            disabledReason = ContactCheckbox_1.ContactCheckboxDisabledReason.MaximumContactsSelected;
        }
        else if (!contact.isGroupV2Capable) {
            disabledReason = ContactCheckbox_1.ContactCheckboxDisabledReason.NotCapable;
        }
        return {
            type: ConversationList_1.RowType.ContactCheckbox,
            contact,
            isChecked: isSelected || isAlreadyInGroup,
            disabledReason,
        };
    };
    return (react_1.default.createElement(ModalHost_1.ModalHost, { onClose: onClose },
        react_1.default.createElement("div", { className: "module-AddGroupMembersModal module-AddGroupMembersModal--choose-members" },
            react_1.default.createElement("button", { "aria-label": i18n('close'), className: "module-AddGroupMembersModal__close-button", type: "button", onClick: () => {
                    onClose();
                } }),
            react_1.default.createElement("h1", { className: "module-AddGroupMembersModal__header" }, i18n('AddGroupMembersModal--title')),
            react_1.default.createElement(SearchInput_1.SearchInput, { disabled: candidateContacts.length === 0, placeholder: i18n('contactSearchPlaceholder'), onChange: event => {
                    setSearchTerm(event.target.value);
                }, onKeyDown: event => {
                    if (canContinue && event.key === 'Enter') {
                        confirmAdds();
                    }
                }, ref: (0, refMerger_1.refMerger)(inputRef, focusRef), value: searchTerm }),
            Boolean(selectedContacts.length) && (react_1.default.createElement(ContactPills_1.ContactPills, null, selectedContacts.map(contact => (react_1.default.createElement(ContactPill_1.ContactPill, { key: contact.id, acceptedMessageRequest: contact.acceptedMessageRequest, avatarPath: contact.avatarPath, color: contact.color, firstName: contact.firstName, i18n: i18n, isMe: contact.isMe, id: contact.id, name: contact.name, phoneNumber: contact.phoneNumber, profileName: contact.profileName, sharedGroupNames: contact.sharedGroupNames, title: contact.title, onClickRemove: () => {
                    removeSelectedContact(contact.id);
                } }))))),
            candidateContacts.length ? (react_1.default.createElement(react_measure_1.default, { bounds: true }, ({ contentRect, measureRef }) => {
                // We disable this ESLint rule because we're capturing a bubbled keydown
                //   event. See [this note in the jsx-a11y docs][0].
                //
                // [0]: https://github.com/jsx-eslint/eslint-plugin-jsx-a11y/blob/c275964f52c35775208bd00cb612c6f82e42e34f/docs/rules/no-static-element-interactions.md#case-the-event-handler-is-only-being-used-to-capture-bubbled-events
                /* eslint-disable jsx-a11y/no-static-element-interactions */
                return (react_1.default.createElement("div", { className: "module-AddGroupMembersModal__list-wrapper", ref: measureRef, onKeyDown: event => {
                        var _a;
                        if (event.key === 'Enter') {
                            (_a = inputRef.current) === null || _a === void 0 ? void 0 : _a.focus();
                        }
                    } },
                    react_1.default.createElement(ConversationList_1.ConversationList, { dimensions: contentRect.bounds, getRow: getRow, i18n: i18n, onClickArchiveButton: shouldNeverBeCalled, onClickContactCheckbox: (conversationId, disabledReason) => {
                            switch (disabledReason) {
                                case undefined:
                                    toggleSelectedContact(conversationId);
                                    break;
                                case ContactCheckbox_1.ContactCheckboxDisabledReason.AlreadyAdded:
                                case ContactCheckbox_1.ContactCheckboxDisabledReason.MaximumContactsSelected:
                                    // These are no-ops.
                                    break;
                                case ContactCheckbox_1.ContactCheckboxDisabledReason.NotCapable: {
                                    const contact = (0, getOwn_1.getOwn)(contactLookup, conversationId);
                                    (0, assert_1.assert)(contact, 'Contact was not in lookup; not showing modal');
                                    setCantAddContactForModal(contact);
                                    break;
                                }
                                default:
                                    throw (0, missingCaseError_1.missingCaseError)(disabledReason);
                            }
                        }, onSelectConversation: shouldNeverBeCalled, renderMessageSearchResult: () => {
                            shouldNeverBeCalled();
                            return react_1.default.createElement("div", null);
                        }, rowCount: rowCount, shouldRecomputeRowHeights: false, showChooseGroupMembers: shouldNeverBeCalled, startNewConversationFromPhoneNumber: shouldNeverBeCalled, startNewConversationFromUsername: shouldNeverBeCalled, theme: theme })));
                /* eslint-enable jsx-a11y/no-static-element-interactions */
            })) : (react_1.default.createElement("div", { className: "module-AddGroupMembersModal__no-candidate-contacts" }, i18n('noContactsFound'))),
            react_1.default.createElement("div", { className: "module-AddGroupMembersModal__button-container" },
                react_1.default.createElement(Button_1.Button, { onClick: onClose, variant: Button_1.ButtonVariant.Secondary }, i18n('cancel')),
                react_1.default.createElement(Button_1.Button, { disabled: !canContinue, onClick: confirmAdds }, i18n('AddGroupMembersModal--continue-to-confirm'))))));
};
exports.ChooseGroupMembersModal = ChooseGroupMembersModal;
function shouldNeverBeCalled(..._args) {
    (0, assert_1.assert)(false, 'This should never be called. Doing nothing');
}
