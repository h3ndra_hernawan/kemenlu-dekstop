"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.bemGenerator = exports.RequestState = void 0;
const classnames_1 = __importDefault(require("classnames"));
var RequestState;
(function (RequestState) {
    RequestState[RequestState["Inactive"] = 0] = "Inactive";
    RequestState[RequestState["InactiveWithError"] = 1] = "InactiveWithError";
    RequestState[RequestState["Active"] = 2] = "Active";
})(RequestState = exports.RequestState || (exports.RequestState = {}));
const bemGenerator = (block) => (element, modifier) => {
    const base = `${block}__${element}`;
    const classes = [base];
    let conditionals = {};
    if (modifier) {
        if (typeof modifier === 'string') {
            classes.push(`${base}--${modifier}`);
        }
        else {
            conditionals = Object.keys(modifier).reduce((acc, key) => (Object.assign(Object.assign({}, acc), { [`${base}--${key}`]: modifier[key] })), {});
        }
    }
    return (0, classnames_1.default)(classes, conditionals);
};
exports.bemGenerator = bemGenerator;
