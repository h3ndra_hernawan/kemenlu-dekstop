"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageAudio = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const lodash_1 = require("lodash");
const assert_1 = require("../../util/assert");
const Attachment_1 = require("../../types/Attachment");
const MessageMetadata_1 = require("./MessageMetadata");
const log = __importStar(require("../../logging/log"));
var State;
(function (State) {
    State["NotDownloaded"] = "NotDownloaded";
    State["Pending"] = "Pending";
    State["Computing"] = "Computing";
    State["Normal"] = "Normal";
})(State || (State = {}));
// Constants
const CSS_BASE = 'module-message__audio-attachment';
const BAR_COUNT = 47;
const BAR_NOT_DOWNLOADED_HEIGHT = 2;
const BAR_MIN_HEIGHT = 4;
const BAR_MAX_HEIGHT = 20;
const REWIND_BAR_COUNT = 2;
// Increments for keyboard audio seek (in seconds)
const SMALL_INCREMENT = 1;
const BIG_INCREMENT = 5;
// Utils
const timeToText = (time) => {
    const hours = Math.floor(time / 3600);
    let minutes = Math.floor((time % 3600) / 60).toString();
    let seconds = Math.floor(time % 60).toString();
    if (hours !== 0 && minutes.length < 2) {
        minutes = `0${minutes}`;
    }
    if (seconds.length < 2) {
        seconds = `0${seconds}`;
    }
    return hours ? `${hours}:${minutes}:${seconds}` : `${minutes}:${seconds}`;
};
const Button = props => {
    const { i18n, buttonRef, mod, label, onClick } = props;
    // Clicking button toggle playback
    const onButtonClick = (event) => {
        event.stopPropagation();
        event.preventDefault();
        onClick();
    };
    // Keyboard playback toggle
    const onButtonKeyDown = (event) => {
        if (event.key !== 'Enter' && event.key !== 'Space') {
            return;
        }
        event.stopPropagation();
        event.preventDefault();
        onClick();
    };
    return (react_1.default.createElement("button", { type: "button", ref: buttonRef, className: (0, classnames_1.default)(`${CSS_BASE}__button`, `${CSS_BASE}__button--${mod}`), onClick: onButtonClick, onKeyDown: onButtonKeyDown, tabIndex: 0, "aria-label": i18n(label) }));
};
/**
 * Display message audio attachment along with its waveform, duration, and
 * toggle Play/Pause button.
 *
 * A global audio player is used for playback and access is managed by the
 * `activeAudioID` and `activeAudioContext` properties. Whenever both
 * `activeAudioID` and `activeAudioContext` are equal to `id` and `context`
 * respectively the instance of the `MessageAudio` assumes the ownership of the
 * `Audio` instance and fully manages it.
 *
 * `context` is required for displaying separate MessageAudio instances in
 * MessageDetails and Message React components.
 */
const MessageAudio = (props) => {
    const { i18n, renderingContext, attachment, withContentAbove, withContentBelow, direction, expirationLength, expirationTimestamp, id, played, showMessageDetail, status, textPending, timestamp, buttonRef, kickOffAttachmentDownload, onCorrupted, onFirstPlayed, audio, computePeaks, activeAudioID, activeAudioContext, setActiveAudioID, } = props;
    (0, assert_1.assert)(audio !== null, 'GlobalAudioContext always provides audio');
    const isActive = activeAudioID === id && activeAudioContext === renderingContext;
    const waveformRef = (0, react_1.useRef)(null);
    const [isPlaying, setIsPlaying] = (0, react_1.useState)(isActive && !(audio.paused || audio.ended));
    const [currentTime, setCurrentTime] = (0, react_1.useState)(isActive ? audio.currentTime : 0);
    // NOTE: Avoid division by zero
    const [duration, setDuration] = (0, react_1.useState)(1e-23);
    const [hasPeaks, setHasPeaks] = (0, react_1.useState)(false);
    const [peaks, setPeaks] = (0, react_1.useState)(new Array(BAR_COUNT).fill(0));
    let state;
    if (attachment.pending) {
        state = State.Pending;
    }
    else if ((0, Attachment_1.hasNotDownloaded)(attachment)) {
        state = State.NotDownloaded;
    }
    else if (!hasPeaks) {
        state = State.Computing;
    }
    else {
        state = State.Normal;
    }
    // This effect loads audio file and computes its RMS peak for displaying the
    // waveform.
    (0, react_1.useEffect)(() => {
        if (state !== State.Computing) {
            return lodash_1.noop;
        }
        log.info('MessageAudio: loading audio and computing waveform');
        let canceled = false;
        (async () => {
            try {
                if (!attachment.url) {
                    throw new Error('Expected attachment url in the MessageAudio with ' +
                        `state: ${state}`);
                }
                const { peaks: newPeaks, duration: newDuration } = await computePeaks(attachment.url, BAR_COUNT);
                if (canceled) {
                    return;
                }
                setPeaks(newPeaks);
                setHasPeaks(true);
                setDuration(Math.max(newDuration, 1e-23));
            }
            catch (err) {
                log.error('MessageAudio: computePeaks error, marking as corrupted', err);
                onCorrupted();
            }
        })();
        return () => {
            canceled = true;
        };
    }, [
        attachment,
        computePeaks,
        setDuration,
        setPeaks,
        setHasPeaks,
        onCorrupted,
        state,
    ]);
    // This effect attaches/detaches event listeners to the global <audio/>
    // instance that we reuse from the GlobalAudioContext.
    //
    // Audio playback changes `audio.currentTime` so we have to propagate this
    // to the waveform UI.
    //
    // When audio ends - we have to change state and reset the position of the
    // waveform.
    (0, react_1.useEffect)(() => {
        // Owner of Audio instance changed
        if (!isActive) {
            log.info('MessageAudio: pausing old owner', id);
            setIsPlaying(false);
            setCurrentTime(0);
            return lodash_1.noop;
        }
        const onTimeUpdate = () => {
            setCurrentTime(audio.currentTime);
            if (audio.currentTime > duration) {
                setDuration(audio.currentTime);
            }
        };
        const onEnded = () => {
            log.info('MessageAudio: ended, changing UI', id);
            setIsPlaying(false);
            setCurrentTime(0);
        };
        const onLoadedMetadata = () => {
            (0, assert_1.assert)(!Number.isNaN(audio.duration), 'Audio should have definite duration on `loadedmetadata` event');
            log.info('MessageAudio: `loadedmetadata` event', id);
            // Sync-up audio's time in case if <audio/> loaded its source after
            // user clicked on waveform
            audio.currentTime = currentTime;
        };
        const onDurationChange = () => {
            log.info('MessageAudio: `durationchange` event', id);
            if (!Number.isNaN(audio.duration)) {
                setDuration(Math.max(audio.duration, 1e-23));
            }
        };
        audio.addEventListener('timeupdate', onTimeUpdate);
        audio.addEventListener('ended', onEnded);
        audio.addEventListener('loadedmetadata', onLoadedMetadata);
        audio.addEventListener('durationchange', onDurationChange);
        return () => {
            audio.removeEventListener('timeupdate', onTimeUpdate);
            audio.removeEventListener('ended', onEnded);
            audio.removeEventListener('loadedmetadata', onLoadedMetadata);
            audio.removeEventListener('durationchange', onDurationChange);
        };
    }, [id, audio, isActive, currentTime, duration]);
    // This effect detects `isPlaying` changes and starts/pauses playback when
    // needed (+keeps waveform position and audio position in sync).
    (0, react_1.useEffect)(() => {
        if (!isActive) {
            return;
        }
        if (isPlaying) {
            if (!audio.paused) {
                return;
            }
            log.info('MessageAudio: resuming playback for', id);
            audio.currentTime = currentTime;
            audio.play().catch(error => {
                log.info('MessageAudio: resume error', id, error.stack || error);
            });
        }
        else {
            log.info('MessageAudio: pausing playback for', id);
            audio.pause();
        }
    }, [id, audio, isActive, isPlaying, currentTime]);
    const toggleIsPlaying = () => {
        setIsPlaying(!isPlaying);
        if (!isActive && !isPlaying) {
            log.info('MessageAudio: changing owner', id);
            setActiveAudioID(id, renderingContext);
            // Pause old audio
            if (!audio.paused) {
                audio.pause();
            }
            if (!attachment.url) {
                throw new Error('Expected attachment url in the MessageAudio with ' +
                    `state: ${state}`);
            }
            audio.src = attachment.url;
        }
    };
    (0, react_1.useEffect)(() => {
        if (!played && isPlaying) {
            onFirstPlayed();
        }
    }, [played, isPlaying, onFirstPlayed]);
    // Clicking waveform moves playback head position and starts playback.
    const onWaveformClick = (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (state !== State.Normal) {
            return;
        }
        if (!isPlaying) {
            toggleIsPlaying();
        }
        if (!waveformRef.current) {
            return;
        }
        const boundingRect = waveformRef.current.getBoundingClientRect();
        let progress = (event.pageX - boundingRect.left) / boundingRect.width;
        if (progress <= REWIND_BAR_COUNT / BAR_COUNT) {
            progress = 0;
        }
        if (isPlaying && !Number.isNaN(audio.duration)) {
            audio.currentTime = audio.duration * progress;
        }
        else {
            setCurrentTime(duration * progress);
        }
    };
    // Keyboard navigation for waveform. Pressing keys moves playback head
    // forward/backwards.
    const onWaveformKeyDown = (event) => {
        let increment;
        if (event.key === 'ArrowRight' || event.key === 'ArrowUp') {
            increment = +SMALL_INCREMENT;
        }
        else if (event.key === 'ArrowLeft' || event.key === 'ArrowDown') {
            increment = -SMALL_INCREMENT;
        }
        else if (event.key === 'PageUp') {
            increment = +BIG_INCREMENT;
        }
        else if (event.key === 'PageDown') {
            increment = -BIG_INCREMENT;
        }
        else {
            // We don't handle other keys
            return;
        }
        event.preventDefault();
        event.stopPropagation();
        // There is no audio to rewind
        if (!isActive) {
            return;
        }
        audio.currentTime = Math.min(Number.isNaN(audio.duration) ? Infinity : audio.duration, Math.max(0, audio.currentTime + increment));
        if (!isPlaying) {
            toggleIsPlaying();
        }
    };
    const peakPosition = peaks.length * (currentTime / duration);
    const waveform = (react_1.default.createElement("div", { ref: waveformRef, className: `${CSS_BASE}__waveform`, onClick: onWaveformClick, onKeyDown: onWaveformKeyDown, tabIndex: 0, role: "slider", "aria-label": i18n('MessageAudio--slider'), "aria-orientation": "horizontal", "aria-valuenow": currentTime, "aria-valuemin": 0, "aria-valuemax": duration, "aria-valuetext": timeToText(currentTime) }, peaks.map((peak, i) => {
        let height = Math.max(BAR_MIN_HEIGHT, BAR_MAX_HEIGHT * peak);
        if (state !== State.Normal) {
            height = BAR_NOT_DOWNLOADED_HEIGHT;
        }
        const highlight = i < peakPosition;
        // Use maximum height for current audio position
        if (highlight && i + 1 >= peakPosition) {
            height = BAR_MAX_HEIGHT;
        }
        const key = i;
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)([
                `${CSS_BASE}__waveform__bar`,
                highlight ? `${CSS_BASE}__waveform__bar--active` : null,
            ]), key: key, style: { height } }));
    })));
    let button;
    if (state === State.Pending || state === State.Computing) {
        // Not really a button, but who cares?
        button = (react_1.default.createElement("div", { className: (0, classnames_1.default)(`${CSS_BASE}__spinner`, `${CSS_BASE}__spinner--pending`), title: i18n('MessageAudio--pending') }));
    }
    else if (state === State.NotDownloaded) {
        button = (react_1.default.createElement(Button, { i18n: i18n, buttonRef: buttonRef, mod: "download", label: "MessageAudio--download", onClick: kickOffAttachmentDownload }));
    }
    else {
        // State.Normal
        button = (react_1.default.createElement(Button, { i18n: i18n, buttonRef: buttonRef, mod: isPlaying ? 'pause' : 'play', label: isPlaying ? 'MessageAudio--pause' : 'MessageAudio--play', onClick: toggleIsPlaying }));
    }
    const countDown = Math.max(0, duration - currentTime);
    const metadata = (react_1.default.createElement("div", { className: `${CSS_BASE}__metadata` },
        !withContentBelow && (react_1.default.createElement(MessageMetadata_1.MessageMetadata, { direction: direction, expirationLength: expirationLength, expirationTimestamp: expirationTimestamp, hasText: withContentBelow, i18n: i18n, id: id, isShowingImage: false, isSticker: false, isTapToViewExpired: false, showMessageDetail: showMessageDetail, status: status, textPending: textPending, timestamp: timestamp })),
        react_1.default.createElement("div", { className: (0, classnames_1.default)(`${CSS_BASE}__countdown`, `${CSS_BASE}__countdown--${played ? 'played' : 'unplayed'}`) }, timeToText(countDown))));
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(CSS_BASE, `${CSS_BASE}--${direction}`, withContentBelow ? `${CSS_BASE}--with-content-below` : null, withContentAbove ? `${CSS_BASE}--with-content-above` : null) },
        react_1.default.createElement("div", { className: `${CSS_BASE}__button-and-waveform` },
            button,
            waveform),
        metadata));
};
exports.MessageAudio = MessageAudio;
