"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AttachmentList = void 0;
const react_1 = __importDefault(require("react"));
const Image_1 = require("./Image");
const StagedGenericAttachment_1 = require("./StagedGenericAttachment");
const StagedPlaceholderAttachment_1 = require("./StagedPlaceholderAttachment");
const Attachment_1 = require("../../types/Attachment");
const IMAGE_WIDTH = 120;
const IMAGE_HEIGHT = 120;
// This is a 1x1 black square.
const BLANK_VIDEO_THUMBNAIL = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQAAAAA3bvkkAAAACklEQVR42mNiAAAABgADm78GJQAAAABJRU5ErkJggg==';
function getUrl(attachment) {
    if (attachment.pending) {
        return undefined;
    }
    return attachment.url;
}
const AttachmentList = ({ attachments, i18n, onAddAttachment, onClickAttachment, onCloseAttachment, onClose, }) => {
    if (!attachments.length) {
        return null;
    }
    const allVisualAttachments = (0, Attachment_1.areAllAttachmentsVisual)(attachments);
    return (react_1.default.createElement("div", { className: "module-attachments" },
        onClose && attachments.length > 1 ? (react_1.default.createElement("div", { className: "module-attachments__header" },
            react_1.default.createElement("button", { type: "button", onClick: onClose, className: "module-attachments__close-button", "aria-label": i18n('close') }))) : null,
        react_1.default.createElement("div", { className: "module-attachments__rail" },
            (attachments || []).map((attachment, index) => {
                const url = getUrl(attachment);
                const key = url || attachment.path || attachment.fileName || index;
                const isImage = (0, Attachment_1.isImageAttachment)(attachment);
                const isVideo = (0, Attachment_1.isVideoAttachment)(attachment);
                const closeAttachment = () => onCloseAttachment(attachment);
                if (isImage || isVideo || attachment.pending) {
                    const isDownloaded = !attachment.pending;
                    const imageUrl = url || (isVideo ? BLANK_VIDEO_THUMBNAIL : undefined);
                    const clickAttachment = onClickAttachment
                        ? () => onClickAttachment(attachment)
                        : undefined;
                    return (react_1.default.createElement(Image_1.Image, { key: key, alt: i18n('stagedImageAttachment', [
                            attachment.fileName || url || index.toString(),
                        ]), className: "module-staged-attachment", i18n: i18n, attachment: attachment, isDownloaded: isDownloaded, softCorners: true, playIconOverlay: isVideo, height: IMAGE_HEIGHT, width: IMAGE_WIDTH, url: imageUrl, closeButton: true, onClick: clickAttachment, onClickClose: closeAttachment, onError: closeAttachment }));
                }
                return (react_1.default.createElement(StagedGenericAttachment_1.StagedGenericAttachment, { key: key, attachment: attachment, i18n: i18n, onClose: closeAttachment }));
            }),
            allVisualAttachments && onAddAttachment ? (react_1.default.createElement(StagedPlaceholderAttachment_1.StagedPlaceholderAttachment, { onClick: onAddAttachment, i18n: i18n })) : null)));
};
exports.AttachmentList = AttachmentList;
