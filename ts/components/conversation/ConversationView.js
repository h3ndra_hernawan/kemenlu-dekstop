"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationView = void 0;
const react_1 = __importDefault(require("react"));
const ConversationView = ({ renderCompositionArea, renderConversationHeader, renderTimeline, }) => {
    return (react_1.default.createElement("div", { className: "ConversationView" },
        react_1.default.createElement("div", { className: "ConversationView__header" }, renderConversationHeader()),
        react_1.default.createElement("div", { className: "ConversationView__pane main panel" },
            react_1.default.createElement("div", { className: "ConversationView__timeline--container" },
                react_1.default.createElement("div", { "aria-live": "polite", className: "ConversationView__timeline" }, renderTimeline())),
            react_1.default.createElement("div", { className: "ConversationView__composition-area" }, renderCompositionArea()))));
};
exports.ConversationView = ConversationView;
