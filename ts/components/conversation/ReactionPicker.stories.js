"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const ReactionPicker_1 = require("./ReactionPicker");
const EmojiPicker_1 = require("../emoji/EmojiPicker");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const preferredReactionEmoji = ['❤️', '👍', '👎', '😂', '😮', '😢'];
const renderEmojiPicker = ({ onClose, onPickEmoji, onSetSkinTone, ref, }) => (React.createElement(EmojiPicker_1.EmojiPicker, { i18n: i18n, skinTone: 0, ref: ref, onClose: onClose, onPickEmoji: onPickEmoji, onSetSkinTone: onSetSkinTone }));
(0, react_1.storiesOf)('Components/Conversation/ReactionPicker', module)
    .add('Base', () => {
    return (React.createElement(ReactionPicker_1.ReactionPicker, { i18n: i18n, onPick: (0, addon_actions_1.action)('onPick'), onSetSkinTone: (0, addon_actions_1.action)('onSetSkinTone'), openCustomizePreferredReactionsModal: (0, addon_actions_1.action)('openCustomizePreferredReactionsModal'), preferredReactionEmoji: preferredReactionEmoji, renderEmojiPicker: renderEmojiPicker }));
})
    .add('Selected Reaction', () => {
    return ['❤️', '👍', '👎', '😂', '😮', '😢', '😡'].map(e => (React.createElement("div", { key: e, style: { height: '100px' } },
        React.createElement(ReactionPicker_1.ReactionPicker, { i18n: i18n, selected: e, onPick: (0, addon_actions_1.action)('onPick'), onSetSkinTone: (0, addon_actions_1.action)('onSetSkinTone'), openCustomizePreferredReactionsModal: (0, addon_actions_1.action)('openCustomizePreferredReactionsModal'), preferredReactionEmoji: preferredReactionEmoji, renderEmojiPicker: renderEmojiPicker }))));
});
