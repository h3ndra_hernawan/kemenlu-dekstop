"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallingNotification = void 0;
const react_1 = __importStar(require("react"));
const react_measure_1 = __importDefault(require("react-measure"));
const lodash_1 = require("lodash");
const SystemMessage_1 = require("./SystemMessage");
const Button_1 = require("../Button");
const Timestamp_1 = require("./Timestamp");
const Calling_1 = require("../../types/Calling");
const callingNotification_1 = require("../../util/callingNotification");
const usePrevious_1 = require("../../hooks/usePrevious");
const missingCaseError_1 = require("../../util/missingCaseError");
const Tooltip_1 = require("../Tooltip");
const log = __importStar(require("../../logging/log"));
exports.CallingNotification = react_1.default.memo(props => {
    const { conversationId, i18n, messageId, messageSizeChanged } = props;
    const [height, setHeight] = (0, react_1.useState)(null);
    const previousHeight = (0, usePrevious_1.usePrevious)(null, height);
    (0, react_1.useEffect)(() => {
        if (height === null) {
            return;
        }
        if (previousHeight !== null && height !== previousHeight) {
            messageSizeChanged(messageId, conversationId);
        }
    }, [height, previousHeight, conversationId, messageId, messageSizeChanged]);
    let timestamp;
    let wasMissed = false;
    switch (props.callMode) {
        case Calling_1.CallMode.Direct:
            timestamp = props.acceptedTime || props.endedTime;
            wasMissed =
                props.wasIncoming && !props.acceptedTime && !props.wasDeclined;
            break;
        case Calling_1.CallMode.Group:
            timestamp = props.startedTime;
            break;
        default:
            log.error(`CallingNotification missing case: ${(0, missingCaseError_1.missingCaseError)(props)}`);
            return null;
    }
    const icon = (0, callingNotification_1.getCallingIcon)(props);
    return (react_1.default.createElement(react_measure_1.default, { bounds: true, onResize: ({ bounds }) => {
            if (!bounds) {
                log.error('We should be measuring the bounds');
                return;
            }
            setHeight(bounds.height);
        } }, ({ measureRef }) => (react_1.default.createElement(SystemMessage_1.SystemMessage, { button: renderCallingNotificationButton(props), contents: react_1.default.createElement(react_1.default.Fragment, null,
            (0, callingNotification_1.getCallingNotificationText)(props, i18n),
            " \u00B7",
            ' ',
            react_1.default.createElement(Timestamp_1.Timestamp, { direction: "outgoing", extended: true, i18n: i18n, timestamp: timestamp, withImageNoCaption: false, withSticker: false, withTapToViewExpired: false })), icon: icon, isError: wasMissed, ref: measureRef }))));
});
function renderCallingNotificationButton(props) {
    const { conversationId, i18n, nextItem, returnToActiveCall, startCallingLobby, } = props;
    if ((nextItem === null || nextItem === void 0 ? void 0 : nextItem.type) === 'callHistory') {
        return null;
    }
    let buttonText;
    let disabledTooltipText;
    let onClick;
    switch (props.callMode) {
        case Calling_1.CallMode.Direct: {
            const { wasIncoming, wasVideoCall } = props;
            buttonText = wasIncoming
                ? i18n('calling__call-back')
                : i18n('calling__call-again');
            onClick = () => {
                startCallingLobby({ conversationId, isVideoCall: wasVideoCall });
            };
            break;
        }
        case Calling_1.CallMode.Group: {
            if (props.ended) {
                return null;
            }
            const { activeCallConversationId, deviceCount, maxDevices } = props;
            if (activeCallConversationId) {
                if (activeCallConversationId === conversationId) {
                    buttonText = i18n('calling__return');
                    onClick = returnToActiveCall;
                }
                else {
                    buttonText = i18n('calling__join');
                    disabledTooltipText = i18n('calling__call-notification__button__in-another-call-tooltip');
                    onClick = lodash_1.noop;
                }
            }
            else if (deviceCount >= maxDevices) {
                buttonText = i18n('calling__call-is-full');
                disabledTooltipText = i18n('calling__call-notification__button__call-full-tooltip', [String(deviceCount)]);
                onClick = lodash_1.noop;
            }
            else {
                buttonText = i18n('calling__join');
                onClick = () => {
                    startCallingLobby({ conversationId, isVideoCall: true });
                };
            }
            break;
        }
        default:
            log.error((0, missingCaseError_1.missingCaseError)(props));
            return null;
    }
    const button = (react_1.default.createElement(Button_1.Button, { disabled: Boolean(disabledTooltipText), onClick: onClick, size: Button_1.ButtonSize.Small, variant: Button_1.ButtonVariant.SystemMessage }, buttonText));
    if (disabledTooltipText) {
        return (react_1.default.createElement(Tooltip_1.Tooltip, { content: disabledTooltipText, direction: Tooltip_1.TooltipPlacement.Top }, button));
    }
    return button;
}
