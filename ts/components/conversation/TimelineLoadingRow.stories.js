"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const TimelineLoadingRow_1 = require("./TimelineLoadingRow");
const story = (0, react_1.storiesOf)('Components/Conversation/TimelineLoadingRow', module);
const createProps = (overrideProps = {}) => ({
    state: (0, addon_knobs_1.select)('state', { idle: 'idle', countdown: 'countdown', loading: 'loading' }, overrideProps.state || 'idle'),
    duration: (0, addon_knobs_1.number)('duration', overrideProps.duration || 0),
    expiresAt: (0, addon_knobs_1.date)('expiresAt', new Date(overrideProps.expiresAt || Date.now())),
    onComplete: (0, addon_actions_1.action)('onComplete'),
});
story.add('Idle', () => {
    const props = createProps();
    return React.createElement(TimelineLoadingRow_1.TimelineLoadingRow, Object.assign({}, props));
});
story.add('Countdown', () => {
    const props = createProps({
        state: 'countdown',
        duration: 40000,
        expiresAt: Date.now() + 20000,
    });
    return React.createElement(TimelineLoadingRow_1.TimelineLoadingRow, Object.assign({}, props));
});
story.add('Loading', () => {
    const props = createProps({ state: 'loading' });
    return React.createElement(TimelineLoadingRow_1.TimelineLoadingRow, Object.assign({}, props));
});
