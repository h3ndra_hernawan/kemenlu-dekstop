"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const Emojify_1 = require("./Emojify");
const story = (0, react_1.storiesOf)('Components/Conversation/Emojify', module);
const createProps = (overrideProps = {}) => ({
    renderNonEmoji: overrideProps.renderNonEmoji,
    sizeClass: overrideProps.sizeClass,
    text: (0, addon_knobs_1.text)('text', overrideProps.text || ''),
});
story.add('Emoji Only', () => {
    const props = createProps({
        text: '😹😹😹',
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Skin Color Modifier', () => {
    const props = createProps({
        text: '👍🏾',
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Jumbo', () => {
    const props = createProps({
        text: '😹😹😹',
        sizeClass: 'max',
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Extra Large', () => {
    const props = createProps({
        text: '😹😹😹',
        sizeClass: 'extra-large',
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Large', () => {
    const props = createProps({
        text: '😹😹😹',
        sizeClass: 'large',
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Medium', () => {
    const props = createProps({
        text: '😹😹😹',
        sizeClass: 'medium',
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Small', () => {
    const props = createProps({
        text: '😹😹😹',
        sizeClass: 'small',
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Plus Text', () => {
    const props = createProps({
        text: 'this 😹 cat 😹 is 😹 so 😹 joyful',
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('All Text, No Emoji', () => {
    const props = createProps({
        text: 'this cat is so joyful',
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Custom Text Render', () => {
    const props = createProps({
        text: 'this 😹 cat 😹 is 😹 so 😹 joyful',
        renderNonEmoji: ({ text: theText, key }) => (React.createElement("div", { key: key, style: { backgroundColor: 'aquamarine' } }, theText)),
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Tens of thousands of emoji', () => {
    const props = createProps({
        text: '💅'.repeat(40000),
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
story.add('Tens of thousands of emoji, interspersed with text', () => {
    const props = createProps({
        text: '💅 hi '.repeat(40000),
    });
    return React.createElement(Emojify_1.Emojify, Object.assign({}, props));
});
