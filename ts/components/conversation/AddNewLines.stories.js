"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const AddNewLines_1 = require("./AddNewLines");
const story = (0, react_1.storiesOf)('Components/Conversation/AddNewLines', module);
const createProps = (overrideProps = {}) => ({
    renderNonNewLine: overrideProps.renderNonNewLine,
    text: (0, addon_knobs_1.text)('text', overrideProps.text || ''),
});
story.add('All newlines', () => {
    const props = createProps({
        text: '\n\n\n',
    });
    return React.createElement(AddNewLines_1.AddNewLines, Object.assign({}, props));
});
story.add('Starting/Ending with Newlines', () => {
    const props = createProps({
        text: '\nSome text\n',
    });
    return React.createElement(AddNewLines_1.AddNewLines, Object.assign({}, props));
});
story.add('Newlines in the Middle', () => {
    const props = createProps({
        text: 'Some\ntext',
    });
    return React.createElement(AddNewLines_1.AddNewLines, Object.assign({}, props));
});
story.add('No Newlines', () => {
    const props = createProps({
        text: 'Some text',
    });
    return React.createElement(AddNewLines_1.AddNewLines, Object.assign({}, props));
});
story.add('Custom Render Function', () => {
    const props = createProps({
        text: 'Some text',
        renderNonNewLine: ({ text: theText, key }) => (React.createElement("div", { key: key, style: { color: 'aquamarine' } }, theText)),
    });
    return React.createElement(AddNewLines_1.AddNewLines, Object.assign({}, props));
});
