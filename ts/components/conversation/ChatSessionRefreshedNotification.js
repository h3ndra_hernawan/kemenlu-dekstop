"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatSessionRefreshedNotification = void 0;
const react_1 = __importStar(require("react"));
const Button_1 = require("../Button");
const SystemMessage_1 = require("./SystemMessage");
const ChatSessionRefreshedDialog_1 = require("./ChatSessionRefreshedDialog");
function ChatSessionRefreshedNotification(props) {
    const { contactSupport, i18n } = props;
    const [isDialogOpen, setIsDialogOpen] = (0, react_1.useState)(false);
    const openDialog = (0, react_1.useCallback)(() => {
        setIsDialogOpen(true);
    }, [setIsDialogOpen]);
    const closeDialog = (0, react_1.useCallback)(() => {
        setIsDialogOpen(false);
    }, [setIsDialogOpen]);
    const wrappedContactSupport = (0, react_1.useCallback)(() => {
        setIsDialogOpen(false);
        contactSupport();
    }, [contactSupport, setIsDialogOpen]);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(SystemMessage_1.SystemMessage, { contents: i18n('ChatRefresh--notification'), button: react_1.default.createElement(Button_1.Button, { onClick: openDialog, size: Button_1.ButtonSize.Small, variant: Button_1.ButtonVariant.SystemMessage }, i18n('ChatRefresh--learnMore')), icon: "session-refresh" }),
        isDialogOpen ? (react_1.default.createElement(ChatSessionRefreshedDialog_1.ChatSessionRefreshedDialog, { onClose: closeDialog, contactSupport: wrappedContactSupport, i18n: i18n })) : null));
}
exports.ChatSessionRefreshedNotification = ChatSessionRefreshedNotification;
