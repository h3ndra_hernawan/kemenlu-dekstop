"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupV2Change = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const Intl_1 = require("../Intl");
const GroupDescriptionText_1 = require("../GroupDescriptionText");
const Button_1 = require("../Button");
const SystemMessage_1 = require("./SystemMessage");
const groupChange_1 = require("../../groupChange");
const Modal_1 = require("../Modal");
function renderStringToIntl(id, i18n, components) {
    return react_1.default.createElement(Intl_1.Intl, { id: id, i18n: i18n, components: components });
}
const changeToIconMap = new Map([
    ['access-attributes', 'group-access'],
    ['access-invite-link', 'group-access'],
    ['access-members', 'group-access'],
    ['admin-approval-add-one', 'group-add'],
    ['admin-approval-remove-one', 'group-decline'],
    ['announcements-only', 'group-access'],
    ['avatar', 'group-avatar'],
    ['description', 'group-edit'],
    ['group-link-add', 'group-access'],
    ['group-link-remove', 'group-access'],
    ['group-link-reset', 'group-access'],
    ['member-add', 'group-add'],
    ['member-add-from-admin-approval', 'group-approved'],
    ['member-add-from-invite', 'group-add'],
    ['member-add-from-link', 'group-add'],
    ['member-privilege', 'group-access'],
    ['member-remove', 'group-remove'],
    ['pending-add-many', 'group-add'],
    ['pending-add-one', 'group-add'],
    ['pending-remove-many', 'group-decline'],
    ['pending-remove-one', 'group-decline'],
    ['title', 'group-edit'],
]);
function getIcon(detail, fromId) {
    const changeType = detail.type;
    let possibleIcon = changeToIconMap.get(changeType);
    const isSameId = fromId === (0, lodash_1.get)(detail, 'uuid', null);
    if (isSameId) {
        if (changeType === 'member-remove') {
            possibleIcon = 'group-leave';
        }
        if (changeType === 'member-add-from-invite') {
            possibleIcon = 'group-approved';
        }
    }
    return possibleIcon || 'group';
}
function GroupV2Detail({ detail, i18n, fromId, onButtonClick, text, }) {
    const icon = getIcon(detail, fromId);
    const newGroupDescription = detail.type === 'description' && (0, lodash_1.get)(detail, 'description');
    return (react_1.default.createElement(SystemMessage_1.SystemMessage, { icon: icon, contents: text, button: newGroupDescription ? (react_1.default.createElement(Button_1.Button, { onClick: () => onButtonClick(newGroupDescription), size: Button_1.ButtonSize.Small, variant: Button_1.ButtonVariant.SystemMessage }, i18n('view'))) : undefined }));
}
function GroupV2Change(props) {
    const { change, groupName, i18n, ourUuid, renderContact } = props;
    const [groupDescription, setGroupDescription] = (0, react_1.useState)();
    return (react_1.default.createElement(react_1.default.Fragment, null,
        (0, groupChange_1.renderChange)(change, {
            i18n,
            ourUuid,
            renderContact,
            renderString: renderStringToIntl,
        }).map((text, index) => (react_1.default.createElement(GroupV2Detail, { detail: change.details[index], fromId: change.from, i18n: i18n, 
            // Difficult to find a unique key for this type
            // eslint-disable-next-line react/no-array-index-key
            key: index, onButtonClick: nextGroupDescription => setGroupDescription(nextGroupDescription), text: text }))),
        groupDescription ? (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, i18n: i18n, title: groupName, onClose: () => setGroupDescription(undefined) },
            react_1.default.createElement(GroupDescriptionText_1.GroupDescriptionText, { text: groupDescription }))) : null));
}
exports.GroupV2Change = GroupV2Change;
