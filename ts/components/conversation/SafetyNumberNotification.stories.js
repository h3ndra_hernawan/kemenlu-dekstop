"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const SafetyNumberNotification_1 = require("./SafetyNumberNotification");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createContact = (props) => ({
    id: '',
    title: (0, addon_knobs_1.text)('contact title', props.title || ''),
});
const createProps = (overrideProps = {}) => ({
    i18n,
    contact: overrideProps.contact || {},
    isGroup: (0, addon_knobs_1.boolean)('isGroup', overrideProps.isGroup || false),
    showIdentity: (0, addon_actions_1.action)('showIdentity'),
});
const stories = (0, react_1.storiesOf)('Components/Conversation/SafetyNumberNotification', module);
stories.add('Group Conversation', () => {
    const props = createProps({
        isGroup: true,
        contact: createContact({
            title: 'Mr. Fire',
        }),
    });
    return React.createElement(SafetyNumberNotification_1.SafetyNumberNotification, Object.assign({}, props));
});
stories.add('Direct Conversation', () => {
    const props = createProps({
        isGroup: false,
        contact: createContact({
            title: 'Mr. Fire',
        }),
    });
    return React.createElement(SafetyNumberNotification_1.SafetyNumberNotification, Object.assign({}, props));
});
stories.add('Long name in group', () => {
    const props = createProps({
        isGroup: true,
        contact: createContact({
            title: '🐈‍⬛🍕🎂'.repeat(50),
        }),
    });
    return React.createElement(SafetyNumberNotification_1.SafetyNumberNotification, Object.assign({}, props));
});
