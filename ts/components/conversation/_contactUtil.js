"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.renderContactShorthand = exports.renderName = exports.renderAvatar = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Avatar_1 = require("../Avatar");
const Spinner_1 = require("../Spinner");
const Colors_1 = require("../../types/Colors");
const EmbeddedContact_1 = require("../../types/EmbeddedContact");
// This file starts with _ to keep it from showing up in the StyleGuide.
function renderAvatar({ contact, i18n, size, direction, }) {
    const { avatar } = contact;
    const avatarPath = avatar && avatar.avatar && avatar.avatar.path;
    const pending = avatar && avatar.avatar && avatar.avatar.pending;
    const title = (0, EmbeddedContact_1.getName)(contact) || '';
    const spinnerSvgSize = size < 50 ? 'small' : 'normal';
    const spinnerSize = size < 50 ? '24px' : undefined;
    if (pending) {
        return (react_1.default.createElement("div", { className: "module-embedded-contact__spinner-container" },
            react_1.default.createElement(Spinner_1.Spinner, { svgSize: spinnerSvgSize, size: spinnerSize, direction: direction })));
    }
    return (react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: false, avatarPath: avatarPath, blur: Avatar_1.AvatarBlur.NoBlur, color: Colors_1.AvatarColors[0], conversationType: "direct", i18n: i18n, isMe: true, title: title, sharedGroupNames: [], size: size }));
}
exports.renderAvatar = renderAvatar;
function renderName({ contact, isIncoming, module, }) {
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(`module-${module}__contact-name`, isIncoming ? `module-${module}__contact-name--incoming` : null) }, (0, EmbeddedContact_1.getName)(contact)));
}
exports.renderName = renderName;
function renderContactShorthand({ contact, isIncoming, module, }) {
    const { number: phoneNumber, email } = contact;
    const firstNumber = phoneNumber && phoneNumber[0] && phoneNumber[0].value;
    const firstEmail = email && email[0] && email[0].value;
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(`module-${module}__contact-method`, isIncoming ? `module-${module}__contact-method--incoming` : null) }, firstNumber || firstEmail));
}
exports.renderContactShorthand = renderContactShorthand;
