"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageBodyReadMore = void 0;
const react_1 = __importStar(require("react"));
const MessageBody_1 = require("./MessageBody");
const usePrevious_1 = require("../../hooks/usePrevious");
const INITIAL_LENGTH = 800;
const INCREMENT_COUNT = 3000;
function graphemeAwareSlice(str, length) {
    if (str.length <= length) {
        return { text: str, hasReadMore: false };
    }
    let text;
    for (const { index } of new Intl.Segmenter().segment(str)) {
        if (!text && index >= length) {
            text = str.slice(0, index);
        }
        if (text && index > length) {
            return {
                text,
                hasReadMore: true,
            };
        }
    }
    return {
        text: str,
        hasReadMore: false,
    };
}
function MessageBodyReadMore({ bodyRanges, direction, disableLinks, displayLimit, i18n, id, messageExpanded, onHeightChange, openConversation, text, textPending, }) {
    const maxLength = displayLimit || INITIAL_LENGTH;
    const previousMaxLength = (0, usePrevious_1.usePrevious)(maxLength, maxLength);
    (0, react_1.useEffect)(() => {
        if (previousMaxLength !== maxLength) {
            onHeightChange();
        }
    }, [maxLength, previousMaxLength, onHeightChange]);
    const { hasReadMore, text: slicedText } = graphemeAwareSlice(text, maxLength);
    const onIncreaseTextLength = hasReadMore
        ? () => {
            messageExpanded(id, maxLength + INCREMENT_COUNT);
        }
        : undefined;
    return (react_1.default.createElement(MessageBody_1.MessageBody, { bodyRanges: bodyRanges, disableLinks: disableLinks, direction: direction, i18n: i18n, onIncreaseTextLength: onIncreaseTextLength, openConversation: openConversation, text: slicedText, textPending: textPending }));
}
exports.MessageBodyReadMore = MessageBodyReadMore;
