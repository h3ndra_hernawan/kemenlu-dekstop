"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupNotification = void 0;
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const ContactName_1 = require("./ContactName");
const SystemMessage_1 = require("./SystemMessage");
const Intl_1 = require("../Intl");
const missingCaseError_1 = require("../../util/missingCaseError");
class GroupNotification extends react_1.default.Component {
    renderChange(change, from) {
        const { contacts, type, newName } = change;
        const { i18n } = this.props;
        const otherPeople = (0, lodash_1.compact)((contacts || []).map(contact => {
            if (contact.isMe) {
                return null;
            }
            return (react_1.default.createElement("span", { key: `external-${contact.id}`, className: "module-group-notification__contact" },
                react_1.default.createElement(ContactName_1.ContactName, { title: contact.title })));
        }));
        const otherPeopleWithCommas = (0, lodash_1.compact)((0, lodash_1.flatten)(otherPeople.map((person, index) => [index > 0 ? ', ' : null, person])));
        const contactsIncludesMe = (contacts || []).length !== otherPeople.length;
        switch (type) {
            case 'name':
                return (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "titleIsNow", components: [newName || ''] }));
            case 'avatar':
                return react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "updatedGroupAvatar" });
            case 'add':
                if (!contacts || !contacts.length) {
                    throw new Error('Group update is missing contacts');
                }
                // eslint-disable-next-line no-case-declarations
                const otherPeopleNotifMsg = otherPeople.length === 1
                    ? 'joinedTheGroup'
                    : 'multipleJoinedTheGroup';
                return (react_1.default.createElement(react_1.default.Fragment, null,
                    otherPeople.length > 0 && (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: otherPeopleNotifMsg, components: [otherPeopleWithCommas] })),
                    contactsIncludesMe && (react_1.default.createElement("div", { className: "module-group-notification__change" },
                        react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "youJoinedTheGroup" })))));
            case 'remove':
                if (from && from.isMe) {
                    return i18n('youLeftTheGroup');
                }
                if (!contacts || !contacts.length) {
                    throw new Error('Group update is missing contacts');
                }
                // eslint-disable-next-line no-case-declarations
                const leftKey = contacts.length > 1 ? 'multipleLeftTheGroup' : 'leftTheGroup';
                return (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: leftKey, components: [otherPeopleWithCommas] }));
            case 'general':
                return;
            default:
                throw (0, missingCaseError_1.missingCaseError)(type);
        }
    }
    render() {
        const { changes: rawChanges, i18n, from } = this.props;
        // This check is just to be extra careful, and can probably be removed.
        const changes = Array.isArray(rawChanges) ? rawChanges : [];
        // Leave messages are always from the person leaving, so we omit the fromLabel if
        //   the change is a 'leave.'
        const firstChange = changes[0];
        const isLeftOnly = changes.length === 1 && (firstChange === null || firstChange === void 0 ? void 0 : firstChange.type) === 'remove';
        const fromLabel = from.isMe ? (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "youUpdatedTheGroup" })) : (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "updatedTheGroup", components: [react_1.default.createElement(ContactName_1.ContactName, { title: from.title })] }));
        let contents;
        if (isLeftOnly) {
            contents = this.renderChange(firstChange, from);
        }
        else {
            contents = (react_1.default.createElement(react_1.default.Fragment, null,
                react_1.default.createElement("p", null, fromLabel),
                changes.map((change, i) => (
                // eslint-disable-next-line react/no-array-index-key
                react_1.default.createElement("p", { key: i, className: "module-group-notification__change" }, this.renderChange(change, from))))));
        }
        return react_1.default.createElement(SystemMessage_1.SystemMessage, { contents: contents, icon: "group" });
    }
}
exports.GroupNotification = GroupNotification;
