"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const MessageDetail_1 = require("./MessageDetail");
const MessageSendState_1 = require("../../messages/MessageSendState");
const MessageReadStatus_1 = require("../../messages/MessageReadStatus");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const getFakeBadge_1 = require("../../test-both/helpers/getFakeBadge");
const Util_1 = require("../../types/Util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/MessageDetail', module);
const defaultMessage = {
    author: (0, getDefaultConversation_1.getDefaultConversation)({
        id: 'some-id',
        title: 'Max',
    }),
    authorBadge: (0, getFakeBadge_1.getFakeBadge)(),
    canReply: true,
    canDeleteForEveryone: true,
    canDownload: true,
    conversationColor: 'crimson',
    conversationId: 'my-convo',
    conversationType: 'direct',
    direction: 'incoming',
    id: 'my-message',
    renderingContext: 'storybook',
    isBlocked: false,
    isMessageRequestAccepted: true,
    previews: [],
    readStatus: MessageReadStatus_1.ReadStatus.Read,
    status: 'sent',
    text: 'A message from Max',
    timestamp: Date.now(),
};
const createProps = (overrideProps = {}) => ({
    contacts: overrideProps.contacts || [
        Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
            title: 'Just Max',
        })), { isOutgoingKeyError: false, isUnidentifiedDelivery: false, status: MessageSendState_1.SendStatus.Delivered }),
    ],
    errors: overrideProps.errors || [],
    message: overrideProps.message || defaultMessage,
    receivedAt: (0, addon_knobs_1.number)('receivedAt', overrideProps.receivedAt || Date.now()),
    sentAt: (0, addon_knobs_1.number)('sentAt', overrideProps.sentAt || Date.now()),
    getPreferredBadge: () => (0, getFakeBadge_1.getFakeBadge)(),
    i18n,
    interactionMode: 'keyboard',
    theme: Util_1.ThemeType.light,
    showSafetyNumber: (0, addon_actions_1.action)('showSafetyNumber'),
    checkForAccount: (0, addon_actions_1.action)('checkForAccount'),
    clearSelectedMessage: (0, addon_actions_1.action)('clearSelectedMessage'),
    displayTapToViewMessage: (0, addon_actions_1.action)('displayTapToViewMessage'),
    doubleCheckMissingQuoteReference: (0, addon_actions_1.action)('doubleCheckMissingQuoteReference'),
    kickOffAttachmentDownload: (0, addon_actions_1.action)('kickOffAttachmentDownload'),
    markAttachmentAsCorrupted: (0, addon_actions_1.action)('markAttachmentAsCorrupted'),
    markViewed: (0, addon_actions_1.action)('markViewed'),
    openConversation: (0, addon_actions_1.action)('openConversation'),
    openLink: (0, addon_actions_1.action)('openLink'),
    reactToMessage: (0, addon_actions_1.action)('reactToMessage'),
    renderAudioAttachment: () => React.createElement("div", null, "*AudioAttachment*"),
    renderEmojiPicker: () => React.createElement("div", null),
    renderReactionPicker: () => React.createElement("div", null),
    replyToMessage: (0, addon_actions_1.action)('replyToMessage'),
    retrySend: (0, addon_actions_1.action)('retrySend'),
    showContactDetail: (0, addon_actions_1.action)('showContactDetail'),
    showContactModal: (0, addon_actions_1.action)('showContactModal'),
    showExpiredIncomingTapToViewToast: (0, addon_actions_1.action)('showExpiredIncomingTapToViewToast'),
    showExpiredOutgoingTapToViewToast: (0, addon_actions_1.action)('showExpiredOutgoingTapToViewToast'),
    showForwardMessageModal: (0, addon_actions_1.action)('showForwardMessageModal'),
    showVisualAttachment: (0, addon_actions_1.action)('showVisualAttachment'),
});
story.add('Delivered Incoming', () => {
    const props = createProps({
        contacts: [
            Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                color: 'forest',
                title: 'Max',
            })), { status: undefined, isOutgoingKeyError: false, isUnidentifiedDelivery: false }),
        ],
    });
    return React.createElement(MessageDetail_1.MessageDetail, Object.assign({}, props));
});
story.add('Delivered Outgoing', () => {
    const props = createProps({
        message: Object.assign(Object.assign({}, defaultMessage), { direction: 'outgoing', text: 'A message to Max' }),
    });
    return React.createElement(MessageDetail_1.MessageDetail, Object.assign({}, props));
});
story.add('Message Statuses', () => {
    const props = createProps({
        contacts: [
            Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Max',
            })), { isOutgoingKeyError: false, isUnidentifiedDelivery: false, status: MessageSendState_1.SendStatus.Sent }),
            Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Sally',
            })), { isOutgoingKeyError: false, isUnidentifiedDelivery: false, status: MessageSendState_1.SendStatus.Pending }),
            Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Terry',
            })), { isOutgoingKeyError: false, isUnidentifiedDelivery: false, status: MessageSendState_1.SendStatus.Failed }),
            Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Theo',
            })), { isOutgoingKeyError: false, isUnidentifiedDelivery: false, status: MessageSendState_1.SendStatus.Delivered }),
            Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Nikki',
            })), { isOutgoingKeyError: false, isUnidentifiedDelivery: false, status: MessageSendState_1.SendStatus.Read }),
        ],
        message: Object.assign(Object.assign({}, defaultMessage), { conversationType: 'group', text: 'A message to you all!' }),
    });
    return React.createElement(MessageDetail_1.MessageDetail, Object.assign({}, props));
});
story.add('Not Delivered', () => {
    const props = createProps({
        message: Object.assign(Object.assign({}, defaultMessage), { direction: 'outgoing', text: 'A message to Max' }),
    });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    props.receivedAt = undefined;
    return React.createElement(MessageDetail_1.MessageDetail, Object.assign({}, props));
});
story.add('No Contacts', () => {
    const props = createProps({
        contacts: [],
        message: Object.assign(Object.assign({}, defaultMessage), { direction: 'outgoing', text: 'Is anybody there?' }),
    });
    return React.createElement(MessageDetail_1.MessageDetail, Object.assign({}, props));
});
story.add('All Errors', () => {
    const props = createProps({
        errors: [
            {
                name: 'Another Error',
                message: 'Wow, that went bad.',
            },
        ],
        message: Object.assign({}, defaultMessage),
        contacts: [
            Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Max',
            })), { isOutgoingKeyError: true, isUnidentifiedDelivery: false, status: MessageSendState_1.SendStatus.Failed }),
            Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Sally',
            })), { errors: [
                    {
                        name: 'Big Error',
                        message: 'Stuff happened, in a bad way.',
                    },
                ], isOutgoingKeyError: false, isUnidentifiedDelivery: true, status: MessageSendState_1.SendStatus.Failed }),
            Object.assign(Object.assign({}, (0, getDefaultConversation_1.getDefaultConversation)({
                title: 'Terry',
            })), { isOutgoingKeyError: true, isUnidentifiedDelivery: true, status: MessageSendState_1.SendStatus.Failed }),
        ],
    });
    return React.createElement(MessageDetail_1.MessageDetail, Object.assign({}, props));
});
