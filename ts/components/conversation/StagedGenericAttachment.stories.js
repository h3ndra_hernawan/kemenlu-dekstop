"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const MIME_1 = require("../../types/MIME");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const StagedGenericAttachment_1 = require("./StagedGenericAttachment");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/StagedGenericAttachment', module);
const createProps = (overrideProps = {}) => ({
    attachment: overrideProps.attachment || {},
    i18n,
    onClose: (0, addon_actions_1.action)('onClose'),
});
const createAttachment = (props = {}) => ({
    contentType: (0, MIME_1.stringToMIMEType)((0, addon_knobs_1.text)('attachment contentType', props.contentType || '')),
    fileName: (0, addon_knobs_1.text)('attachment fileName', props.fileName || ''),
    url: '',
    size: 14243,
});
story.add('Text File', () => {
    const attachment = createAttachment({
        contentType: (0, MIME_1.stringToMIMEType)('text/plain'),
        fileName: 'manifesto.txt',
    });
    const props = createProps({ attachment });
    return React.createElement(StagedGenericAttachment_1.StagedGenericAttachment, Object.assign({}, props));
});
story.add('Long Name', () => {
    const attachment = createAttachment({
        contentType: (0, MIME_1.stringToMIMEType)('text/plain'),
        fileName: 'this-is-my-very-important-manifesto-you-must-read-it.txt',
    });
    const props = createProps({ attachment });
    return React.createElement(StagedGenericAttachment_1.StagedGenericAttachment, Object.assign({}, props));
});
story.add('Long Extension', () => {
    const attachment = createAttachment({
        contentType: (0, MIME_1.stringToMIMEType)('text/plain'),
        fileName: 'manifesto.reallylongtxt',
    });
    const props = createProps({ attachment });
    return React.createElement(StagedGenericAttachment_1.StagedGenericAttachment, Object.assign({}, props));
});
