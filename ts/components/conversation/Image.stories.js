"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const Fixtures_1 = require("../../storybook/Fixtures");
const Image_1 = require("./Image");
const MIME_1 = require("../../types/MIME");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const StorybookThemeContext_1 = require("../../../.storybook/StorybookThemeContext");
const fakeAttachment_1 = require("../../test-both/helpers/fakeAttachment");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/Image', module);
const createProps = (overrideProps = {}) => ({
    alt: (0, addon_knobs_1.text)('alt', overrideProps.alt || ''),
    attachment: overrideProps.attachment ||
        (0, fakeAttachment_1.fakeAttachment)({
            contentType: MIME_1.IMAGE_PNG,
            fileName: 'sax.png',
            url: Fixtures_1.pngUrl,
        }),
    blurHash: (0, addon_knobs_1.text)('blurHash', overrideProps.blurHash || ''),
    bottomOverlay: (0, addon_knobs_1.boolean)('bottomOverlay', overrideProps.bottomOverlay || false),
    closeButton: (0, addon_knobs_1.boolean)('closeButton', overrideProps.closeButton || false),
    curveBottomLeft: (0, addon_knobs_1.boolean)('curveBottomLeft', overrideProps.curveBottomLeft || false),
    curveBottomRight: (0, addon_knobs_1.boolean)('curveBottomRight', overrideProps.curveBottomRight || false),
    curveTopLeft: (0, addon_knobs_1.boolean)('curveTopLeft', overrideProps.curveTopLeft || false),
    curveTopRight: (0, addon_knobs_1.boolean)('curveTopRight', overrideProps.curveTopRight || false),
    darkOverlay: (0, addon_knobs_1.boolean)('darkOverlay', overrideProps.darkOverlay || false),
    height: (0, addon_knobs_1.number)('height', overrideProps.height || 100),
    i18n,
    noBackground: (0, addon_knobs_1.boolean)('noBackground', overrideProps.noBackground || false),
    noBorder: (0, addon_knobs_1.boolean)('noBorder', overrideProps.noBorder || false),
    onClick: (0, addon_actions_1.action)('onClick'),
    onClickClose: (0, addon_actions_1.action)('onClickClose'),
    onError: (0, addon_actions_1.action)('onError'),
    overlayText: (0, addon_knobs_1.text)('overlayText', overrideProps.overlayText || ''),
    playIconOverlay: (0, addon_knobs_1.boolean)('playIconOverlay', overrideProps.playIconOverlay || false),
    smallCurveTopLeft: (0, addon_knobs_1.boolean)('smallCurveTopLeft', overrideProps.smallCurveTopLeft || false),
    softCorners: (0, addon_knobs_1.boolean)('softCorners', overrideProps.softCorners || false),
    tabIndex: (0, addon_knobs_1.number)('tabIndex', overrideProps.tabIndex || 0),
    theme: (0, addon_knobs_1.text)('theme', overrideProps.theme || 'light'),
    url: (0, addon_knobs_1.text)('url', 'url' in overrideProps ? overrideProps.url || null : Fixtures_1.pngUrl),
    width: (0, addon_knobs_1.number)('width', overrideProps.width || 100),
});
story.add('URL with Height/Width', () => {
    const props = createProps();
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('Caption', () => {
    const defaultProps = createProps();
    const props = Object.assign(Object.assign({}, defaultProps), { attachment: Object.assign(Object.assign({}, defaultProps.attachment), { caption: '<Saxophone Pun>' }) });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('Play Icon', () => {
    const props = createProps({
        playIconOverlay: true,
    });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('Close Button', () => {
    const props = createProps({
        closeButton: true,
    });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('No Border or Background', () => {
    const props = createProps({
        attachment: (0, fakeAttachment_1.fakeAttachment)({
            contentType: MIME_1.IMAGE_PNG,
            fileName: 'sax.png',
            url: Fixtures_1.pngUrl,
        }),
        noBackground: true,
        noBorder: true,
        url: Fixtures_1.pngUrl,
    });
    return (React.createElement("div", { style: { backgroundColor: '#999' } },
        React.createElement(Image_1.Image, Object.assign({}, props))));
});
story.add('Pending', () => {
    const props = createProps();
    props.attachment.pending = true;
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('Pending w/blurhash', () => {
    const props = createProps();
    props.attachment.pending = true;
    return (React.createElement(Image_1.Image, Object.assign({}, props, { blurHash: "LDA,FDBnm+I=p{tkIUI;~UkpELV]", width: 300, height: 400 })));
});
story.add('Curved Corners', () => {
    const props = createProps({
        curveBottomLeft: true,
        curveBottomRight: true,
        curveTopLeft: true,
        curveTopRight: true,
    });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('Small Curve Top Left', () => {
    const props = createProps({
        smallCurveTopLeft: true,
    });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('Soft Corners', () => {
    const props = createProps({
        softCorners: true,
    });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('Bottom Overlay', () => {
    const props = createProps({
        bottomOverlay: true,
    });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('Full Overlay with Text', () => {
    const props = createProps({
        darkOverlay: true,
        overlayText: 'Honk!',
    });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('Blurhash', () => {
    const defaultProps = createProps();
    const props = Object.assign(Object.assign({}, defaultProps), { blurHash: 'thisisafakeblurhashthatwasmadeup' });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
story.add('undefined blurHash', () => {
    const Wrapper = () => {
        const theme = React.useContext(StorybookThemeContext_1.StorybookThemeContext);
        const props = createProps({
            blurHash: undefined,
            theme,
            url: undefined,
        });
        return React.createElement(Image_1.Image, Object.assign({}, props));
    };
    return React.createElement(Wrapper, null);
});
story.add('Missing Image', () => {
    const defaultProps = createProps();
    const props = Object.assign(Object.assign({}, defaultProps), { 
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        attachment: undefined });
    return React.createElement(Image_1.Image, Object.assign({}, props));
});
