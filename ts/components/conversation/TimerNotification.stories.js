"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const moment = __importStar(require("moment"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const TimerNotification_1 = require("./TimerNotification");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/TimerNotification', module);
const createProps = (overrideProps = {}) => (Object.assign({ i18n, type: (0, addon_knobs_1.select)('type', {
        fromOther: 'fromOther',
        fromMe: 'fromMe',
        fromSync: 'fromSync',
    }, overrideProps.type || 'fromOther'), title: (0, addon_knobs_1.text)('title', overrideProps.title || '') }, ((0, addon_knobs_1.boolean)('disabled', overrideProps.disabled || false)
    ? {
        disabled: true,
    }
    : {
        disabled: false,
        expireTimer: (0, addon_knobs_1.number)('expireTimer', ('expireTimer' in overrideProps ? overrideProps.expireTimer : 0) || 0),
    })));
story.add('Set By Other', () => {
    const props = createProps({
        expireTimer: moment.duration(1, 'hour').asSeconds(),
        type: 'fromOther',
        title: 'Mr. Fire',
    });
    return (React.createElement(React.Fragment, null,
        React.createElement(TimerNotification_1.TimerNotification, Object.assign({}, props)),
        React.createElement("div", { style: { padding: '1em' } }),
        React.createElement(TimerNotification_1.TimerNotification, Object.assign({}, props, { disabled: true }))));
});
story.add('Set By Other (with a long name)', () => {
    const longName = '🦴🧩📴'.repeat(50);
    const props = createProps({
        expireTimer: moment.duration(1, 'hour').asSeconds(),
        type: 'fromOther',
        title: longName,
    });
    return (React.createElement(React.Fragment, null,
        React.createElement(TimerNotification_1.TimerNotification, Object.assign({}, props)),
        React.createElement("div", { style: { padding: '1em' } }),
        React.createElement(TimerNotification_1.TimerNotification, Object.assign({}, props, { disabled: true }))));
});
story.add('Set By You', () => {
    const props = createProps({
        expireTimer: moment.duration(1, 'hour').asSeconds(),
        type: 'fromMe',
        title: 'Mr. Fire',
    });
    return (React.createElement(React.Fragment, null,
        React.createElement(TimerNotification_1.TimerNotification, Object.assign({}, props)),
        React.createElement("div", { style: { padding: '1em' } }),
        React.createElement(TimerNotification_1.TimerNotification, Object.assign({}, props, { disabled: true }))));
});
story.add('Set By Sync', () => {
    const props = createProps({
        expireTimer: moment.duration(1, 'hour').asSeconds(),
        type: 'fromSync',
        title: 'Mr. Fire',
    });
    return (React.createElement(React.Fragment, null,
        React.createElement(TimerNotification_1.TimerNotification, Object.assign({}, props)),
        React.createElement("div", { style: { padding: '1em' } }),
        React.createElement(TimerNotification_1.TimerNotification, Object.assign({}, props, { disabled: true }))));
});
