"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const moment = __importStar(require("moment"));
const lodash_1 = require("lodash");
const uuid_1 = require("uuid");
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const Timeline_1 = require("./Timeline");
const TimelineItem_1 = require("./TimelineItem");
const StorybookThemeContext_1 = require("../../../.storybook/StorybookThemeContext");
const ConversationHero_1 = require("./ConversationHero");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const getRandomColor_1 = require("../../test-both/helpers/getRandomColor");
const LastSeenIndicator_1 = require("./LastSeenIndicator");
const TimelineLoadingRow_1 = require("./TimelineLoadingRow");
const TypingBubble_1 = require("./TypingBubble");
const contactSpoofing_1 = require("../../util/contactSpoofing");
const MessageReadStatus_1 = require("../../messages/MessageReadStatus");
const Util_1 = require("../../types/Util");
const UUID_1 = require("../../types/UUID");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/Timeline', module);
// eslint-disable-next-line
const noop = () => { };
Object.assign(window, {
    registerForActive: noop,
    unregisterForActive: noop,
});
const items = {
    'id-1': {
        type: 'message',
        data: {
            author: (0, getDefaultConversation_1.getDefaultConversation)({
                phoneNumber: '(202) 555-2001',
            }),
            authorBadge: undefined,
            canDeleteForEveryone: false,
            canDownload: true,
            canReply: true,
            conversationColor: 'forest',
            conversationId: 'conversation-id',
            conversationType: 'group',
            direction: 'incoming',
            id: 'id-1',
            isBlocked: false,
            isMessageRequestAccepted: true,
            previews: [],
            readStatus: MessageReadStatus_1.ReadStatus.Read,
            text: '🔥',
            timestamp: Date.now(),
        },
    },
    'id-2': {
        type: 'message',
        data: {
            author: (0, getDefaultConversation_1.getDefaultConversation)({}),
            authorBadge: undefined,
            canDeleteForEveryone: false,
            canDownload: true,
            canReply: true,
            conversationColor: 'forest',
            conversationId: 'conversation-id',
            conversationType: 'group',
            direction: 'incoming',
            id: 'id-2',
            isBlocked: false,
            isMessageRequestAccepted: true,
            previews: [],
            readStatus: MessageReadStatus_1.ReadStatus.Read,
            text: 'Hello there from the new world! http://somewhere.com',
            timestamp: Date.now(),
        },
    },
    'id-2.5': {
        type: 'unsupportedMessage',
        data: {
            canProcessNow: false,
            contact: {
                id: '061d3783-5736-4145-b1a2-6b6cf1156393',
                isMe: false,
                phoneNumber: '(202) 555-1000',
                profileName: 'Mr. Pig',
                title: 'Mr. Pig',
            },
        },
    },
    'id-3': {
        type: 'message',
        data: {
            author: (0, getDefaultConversation_1.getDefaultConversation)({}),
            authorBadge: undefined,
            canDeleteForEveryone: false,
            canDownload: true,
            canReply: true,
            conversationColor: 'crimson',
            conversationId: 'conversation-id',
            conversationType: 'group',
            direction: 'incoming',
            id: 'id-3',
            isBlocked: false,
            isMessageRequestAccepted: true,
            previews: [],
            readStatus: MessageReadStatus_1.ReadStatus.Read,
            text: 'Hello there from the new world!',
            timestamp: Date.now(),
        },
    },
    'id-4': {
        type: 'timerNotification',
        data: {
            disabled: false,
            expireTimer: moment.duration(2, 'hours').asSeconds(),
            title: "It's Me",
            type: 'fromMe',
        },
    },
    'id-5': {
        type: 'timerNotification',
        data: {
            disabled: false,
            expireTimer: moment.duration(2, 'hours').asSeconds(),
            title: '(202) 555-0000',
            type: 'fromOther',
        },
    },
    'id-6': {
        type: 'safetyNumberNotification',
        data: {
            contact: {
                id: '+1202555000',
                title: 'Mr. Fire',
            },
            isGroup: true,
        },
    },
    'id-7': {
        type: 'verificationNotification',
        data: {
            contact: { title: 'Mrs. Ice' },
            isLocal: true,
            type: 'markVerified',
        },
    },
    'id-8': {
        type: 'groupNotification',
        data: {
            changes: [
                {
                    type: 'name',
                    newName: 'Squirrels and their uses',
                },
                {
                    type: 'add',
                    contacts: [
                        (0, getDefaultConversation_1.getDefaultConversation)({
                            phoneNumber: '(202) 555-0002',
                            title: 'Mr. Fire',
                        }),
                        (0, getDefaultConversation_1.getDefaultConversation)({
                            phoneNumber: '(202) 555-0003',
                            title: 'Ms. Water',
                        }),
                    ],
                },
            ],
            from: (0, getDefaultConversation_1.getDefaultConversation)({
                phoneNumber: '(202) 555-0001',
                title: 'Mrs. Ice',
                isMe: false,
            }),
        },
    },
    'id-9': {
        type: 'resetSessionNotification',
        data: null,
    },
    'id-10': {
        type: 'message',
        data: {
            author: (0, getDefaultConversation_1.getDefaultConversation)({}),
            authorBadge: undefined,
            canDeleteForEveryone: false,
            canDownload: true,
            canReply: true,
            conversationColor: 'plum',
            conversationId: 'conversation-id',
            conversationType: 'group',
            direction: 'outgoing',
            id: 'id-6',
            isBlocked: false,
            isMessageRequestAccepted: true,
            previews: [],
            readStatus: MessageReadStatus_1.ReadStatus.Read,
            status: 'sent',
            text: '🔥',
            timestamp: Date.now(),
        },
    },
    'id-11': {
        type: 'message',
        data: {
            author: (0, getDefaultConversation_1.getDefaultConversation)({}),
            authorBadge: undefined,
            canDeleteForEveryone: false,
            canDownload: true,
            canReply: true,
            conversationColor: 'crimson',
            conversationId: 'conversation-id',
            conversationType: 'group',
            direction: 'outgoing',
            id: 'id-7',
            isBlocked: false,
            isMessageRequestAccepted: true,
            previews: [],
            readStatus: MessageReadStatus_1.ReadStatus.Read,
            status: 'read',
            text: 'Hello there from the new world! http://somewhere.com',
            timestamp: Date.now(),
        },
    },
    'id-12': {
        type: 'message',
        data: {
            author: (0, getDefaultConversation_1.getDefaultConversation)({}),
            authorBadge: undefined,
            canDeleteForEveryone: false,
            canDownload: true,
            canReply: true,
            conversationColor: 'crimson',
            conversationId: 'conversation-id',
            conversationType: 'group',
            direction: 'outgoing',
            id: 'id-8',
            isBlocked: false,
            isMessageRequestAccepted: true,
            previews: [],
            readStatus: MessageReadStatus_1.ReadStatus.Read,
            status: 'sent',
            text: 'Hello there from the new world! 🔥',
            timestamp: Date.now(),
        },
    },
    'id-13': {
        type: 'message',
        data: {
            author: (0, getDefaultConversation_1.getDefaultConversation)({}),
            authorBadge: undefined,
            canDeleteForEveryone: false,
            canDownload: true,
            canReply: true,
            conversationColor: 'crimson',
            conversationId: 'conversation-id',
            conversationType: 'group',
            direction: 'outgoing',
            id: 'id-9',
            isBlocked: false,
            isMessageRequestAccepted: true,
            previews: [],
            readStatus: MessageReadStatus_1.ReadStatus.Read,
            status: 'sent',
            text: 'Hello there from the new world! And this is multiple lines of text. Lines and lines and lines.',
            timestamp: Date.now(),
        },
    },
    'id-14': {
        type: 'message',
        data: {
            author: (0, getDefaultConversation_1.getDefaultConversation)({}),
            authorBadge: undefined,
            canDeleteForEveryone: false,
            canDownload: true,
            canReply: true,
            conversationColor: 'crimson',
            conversationId: 'conversation-id',
            conversationType: 'group',
            direction: 'outgoing',
            id: 'id-10',
            isBlocked: false,
            isMessageRequestAccepted: true,
            previews: [],
            readStatus: MessageReadStatus_1.ReadStatus.Read,
            status: 'read',
            text: 'Hello there from the new world! And this is multiple lines of text. Lines and lines and lines.',
            timestamp: Date.now(),
        },
    },
    'id-15': {
        type: 'linkNotification',
        data: null,
    },
};
const actions = () => ({
    acknowledgeGroupMemberNameCollisions: (0, addon_actions_1.action)('acknowledgeGroupMemberNameCollisions'),
    checkForAccount: (0, addon_actions_1.action)('checkForAccount'),
    clearChangedMessages: (0, addon_actions_1.action)('clearChangedMessages'),
    clearInvitedUuidsForNewlyCreatedGroup: (0, addon_actions_1.action)('clearInvitedUuidsForNewlyCreatedGroup'),
    setLoadCountdownStart: (0, addon_actions_1.action)('setLoadCountdownStart'),
    setIsNearBottom: (0, addon_actions_1.action)('setIsNearBottom'),
    learnMoreAboutDeliveryIssue: (0, addon_actions_1.action)('learnMoreAboutDeliveryIssue'),
    loadAndScroll: (0, addon_actions_1.action)('loadAndScroll'),
    loadOlderMessages: (0, addon_actions_1.action)('loadOlderMessages'),
    loadNewerMessages: (0, addon_actions_1.action)('loadNewerMessages'),
    loadNewestMessages: (0, addon_actions_1.action)('loadNewestMessages'),
    markMessageRead: (0, addon_actions_1.action)('markMessageRead'),
    selectMessage: (0, addon_actions_1.action)('selectMessage'),
    clearSelectedMessage: (0, addon_actions_1.action)('clearSelectedMessage'),
    updateSharedGroups: (0, addon_actions_1.action)('updateSharedGroups'),
    reactToMessage: (0, addon_actions_1.action)('reactToMessage'),
    replyToMessage: (0, addon_actions_1.action)('replyToMessage'),
    retrySend: (0, addon_actions_1.action)('retrySend'),
    deleteMessage: (0, addon_actions_1.action)('deleteMessage'),
    deleteMessageForEveryone: (0, addon_actions_1.action)('deleteMessageForEveryone'),
    showMessageDetail: (0, addon_actions_1.action)('showMessageDetail'),
    openConversation: (0, addon_actions_1.action)('openConversation'),
    showContactDetail: (0, addon_actions_1.action)('showContactDetail'),
    showContactModal: (0, addon_actions_1.action)('showContactModal'),
    kickOffAttachmentDownload: (0, addon_actions_1.action)('kickOffAttachmentDownload'),
    markAttachmentAsCorrupted: (0, addon_actions_1.action)('markAttachmentAsCorrupted'),
    markViewed: (0, addon_actions_1.action)('markViewed'),
    messageExpanded: (0, addon_actions_1.action)('messageExpanded'),
    showVisualAttachment: (0, addon_actions_1.action)('showVisualAttachment'),
    downloadAttachment: (0, addon_actions_1.action)('downloadAttachment'),
    displayTapToViewMessage: (0, addon_actions_1.action)('displayTapToViewMessage'),
    doubleCheckMissingQuoteReference: (0, addon_actions_1.action)('doubleCheckMissingQuoteReference'),
    onHeightChange: (0, addon_actions_1.action)('onHeightChange'),
    openLink: (0, addon_actions_1.action)('openLink'),
    scrollToQuotedMessage: (0, addon_actions_1.action)('scrollToQuotedMessage'),
    showExpiredIncomingTapToViewToast: (0, addon_actions_1.action)('showExpiredIncomingTapToViewToast'),
    showExpiredOutgoingTapToViewToast: (0, addon_actions_1.action)('showExpiredOutgoingTapToViewToast'),
    showForwardMessageModal: (0, addon_actions_1.action)('showForwardMessageModal'),
    showIdentity: (0, addon_actions_1.action)('showIdentity'),
    downloadNewVersion: (0, addon_actions_1.action)('downloadNewVersion'),
    messageSizeChanged: (0, addon_actions_1.action)('messageSizeChanged'),
    startCallingLobby: (0, addon_actions_1.action)('startCallingLobby'),
    returnToActiveCall: (0, addon_actions_1.action)('returnToActiveCall'),
    contactSupport: (0, addon_actions_1.action)('contactSupport'),
    closeContactSpoofingReview: (0, addon_actions_1.action)('closeContactSpoofingReview'),
    reviewGroupMemberNameCollision: (0, addon_actions_1.action)('reviewGroupMemberNameCollision'),
    reviewMessageRequestNameCollision: (0, addon_actions_1.action)('reviewMessageRequestNameCollision'),
    onBlock: (0, addon_actions_1.action)('onBlock'),
    onBlockAndReportSpam: (0, addon_actions_1.action)('onBlockAndReportSpam'),
    onDelete: (0, addon_actions_1.action)('onDelete'),
    onUnblock: (0, addon_actions_1.action)('onUnblock'),
    removeMember: (0, addon_actions_1.action)('removeMember'),
    unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'),
});
const renderItem = ({ messageId, containerElementRef, containerWidthBreakpoint, }) => (React.createElement(TimelineItem_1.TimelineItem, Object.assign({ id: "", isSelected: false, renderEmojiPicker: () => React.createElement("div", null), renderReactionPicker: () => React.createElement("div", null), item: items[messageId], previousItem: undefined, nextItem: undefined, i18n: i18n, interactionMode: "keyboard", theme: Util_1.ThemeType.light, containerElementRef: containerElementRef, containerWidthBreakpoint: containerWidthBreakpoint, conversationId: "", renderContact: () => '*ContactName*', renderUniversalTimerNotification: () => (React.createElement("div", null, "*UniversalTimerNotification*")), renderAudioAttachment: () => React.createElement("div", null, "*AudioAttachment*") }, actions())));
const renderLastSeenIndicator = () => (React.createElement(LastSeenIndicator_1.LastSeenIndicator, { count: 2, i18n: i18n }));
const getAbout = () => (0, addon_knobs_1.text)('about', '👍 Free to chat');
const getTitle = () => (0, addon_knobs_1.text)('name', 'Cayce Bollard');
const getName = () => (0, addon_knobs_1.text)('name', 'Cayce Bollard');
const getProfileName = () => (0, addon_knobs_1.text)('profileName', 'Cayce Bollard (profile)');
const getAvatarPath = () => (0, addon_knobs_1.text)('avatarPath', '/fixtures/kitten-4-112-112.jpg');
const getPhoneNumber = () => (0, addon_knobs_1.text)('phoneNumber', '+1 (808) 555-1234');
const renderHeroRow = () => {
    const Wrapper = () => {
        const theme = React.useContext(StorybookThemeContext_1.StorybookThemeContext);
        return (React.createElement(ConversationHero_1.ConversationHero, { about: getAbout(), acceptedMessageRequest: true, i18n: i18n, isMe: false, title: getTitle(), avatarPath: getAvatarPath(), name: getName(), profileName: getProfileName(), phoneNumber: getPhoneNumber(), conversationType: "direct", onHeightChange: (0, addon_actions_1.action)('onHeightChange in ConversationHero'), sharedGroupNames: ['NYC Rock Climbers', 'Dinner Party'], theme: theme, unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), updateSharedGroups: noop }));
    };
    return React.createElement(Wrapper, null);
};
const renderLoadingRow = () => React.createElement(TimelineLoadingRow_1.TimelineLoadingRow, { state: "loading" });
const renderTypingBubble = () => (React.createElement(TypingBubble_1.TypingBubble, { acceptedMessageRequest: true, badge: undefined, color: (0, getRandomColor_1.getRandomColor)(), conversationType: "direct", phoneNumber: "+18005552222", i18n: i18n, isMe: false, title: "title", theme: Util_1.ThemeType.light, sharedGroupNames: [] }));
const createProps = (overrideProps = {}) => (Object.assign({ i18n, haveNewest: (0, addon_knobs_1.boolean)('haveNewest', overrideProps.haveNewest !== false), haveOldest: (0, addon_knobs_1.boolean)('haveOldest', overrideProps.haveOldest !== false), isIncomingMessageRequest: (0, addon_knobs_1.boolean)('isIncomingMessageRequest', overrideProps.isIncomingMessageRequest === true), isLoadingMessages: (0, addon_knobs_1.boolean)('isLoadingMessages', overrideProps.isLoadingMessages === false), items: overrideProps.items || Object.keys(items), loadCountdownStart: undefined, messageHeightChangeIndex: undefined, resetCounter: 0, scrollToBottomCounter: 0, scrollToIndex: overrideProps.scrollToIndex, scrollToIndexCounter: 0, totalUnread: (0, addon_knobs_1.number)('totalUnread', overrideProps.totalUnread || 0), oldestUnreadIndex: (0, addon_knobs_1.number)('oldestUnreadIndex', overrideProps.oldestUnreadIndex || 0) ||
        undefined, invitedContactsForNewlyCreatedGroup: overrideProps.invitedContactsForNewlyCreatedGroup || [], warning: overrideProps.warning, id: (0, uuid_1.v4)(), isNearBottom: false, renderItem,
    renderLastSeenIndicator,
    renderHeroRow,
    renderLoadingRow,
    renderTypingBubble, typingContactId: overrideProps.typingContactId }, actions()));
story.add('Oldest and Newest', () => {
    const props = createProps();
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('With active message request', () => {
    const props = createProps({
        isIncomingMessageRequest: true,
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('Without Newest Message', () => {
    const props = createProps({
        haveNewest: false,
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('Without newest message, active message request', () => {
    const props = createProps({
        haveOldest: false,
        isIncomingMessageRequest: true,
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('Without Oldest Message', () => {
    const props = createProps({
        haveOldest: false,
        scrollToIndex: -1,
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('Empty (just hero)', () => {
    const props = createProps({
        items: [],
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('Last Seen', () => {
    const props = createProps({
        oldestUnreadIndex: 13,
        totalUnread: 2,
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('Target Index to Top', () => {
    const props = createProps({
        scrollToIndex: 0,
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('Typing Indicator', () => {
    const props = createProps({
        typingContactId: UUID_1.UUID.generate().toString(),
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('With invited contacts for a newly-created group', () => {
    const props = createProps({
        invitedContactsForNewlyCreatedGroup: [
            (0, getDefaultConversation_1.getDefaultConversation)({
                id: 'abc123',
                title: 'John Bon Bon Jovi',
            }),
            (0, getDefaultConversation_1.getDefaultConversation)({
                id: 'def456',
                title: 'Bon John Bon Jovi',
            }),
        ],
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('With "same name in direct conversation" warning', () => {
    const props = createProps({
        warning: {
            type: contactSpoofing_1.ContactSpoofingType.DirectConversationWithSameTitle,
            safeConversation: (0, getDefaultConversation_1.getDefaultConversation)(),
        },
        items: [],
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
story.add('With "same name in group conversation" warning', () => {
    const props = createProps({
        warning: {
            type: contactSpoofing_1.ContactSpoofingType.MultipleGroupMembersWithSameTitle,
            acknowledgedGroupNameCollisions: {},
            groupNameCollisions: {
                Alice: (0, lodash_1.times)(2, () => (0, uuid_1.v4)()),
                Bob: (0, lodash_1.times)(3, () => (0, uuid_1.v4)()),
            },
        },
        items: [],
    });
    return React.createElement(Timeline_1.Timeline, Object.assign({}, props));
});
