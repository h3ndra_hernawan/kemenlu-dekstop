"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryIssueDialog = void 0;
const React = __importStar(require("react"));
const Button_1 = require("../Button");
const Modal_1 = require("../Modal");
const Intl_1 = require("../Intl");
const Emojify_1 = require("./Emojify");
const useRestoreFocus_1 = require("../../hooks/useRestoreFocus");
function DeliveryIssueDialog(props) {
    const { i18n, inGroup, learnMoreAboutDeliveryIssue, sender, onClose } = props;
    const key = inGroup
        ? 'DeliveryIssue--summary--group'
        : 'DeliveryIssue--summary';
    // Focus first button after initial render, restore focus on teardown
    const [focusRef] = (0, useRestoreFocus_1.useRestoreFocus)();
    return (React.createElement(Modal_1.Modal, { hasXButton: false, onClose: onClose, i18n: i18n },
        React.createElement("section", null,
            React.createElement("div", { className: "module-delivery-issue-dialog__image" },
                React.createElement("img", { src: "images/delivery-issue.svg", height: "110", width: "200", alt: "" })),
            React.createElement("div", { className: "module-delivery-issue-dialog__title" }, i18n('DeliveryIssue--title')),
            React.createElement("div", { className: "module-delivery-issue-dialog__description" },
                React.createElement(Intl_1.Intl, { id: key, components: {
                        sender: React.createElement(Emojify_1.Emojify, { text: sender.title }),
                    }, i18n: i18n }))),
        React.createElement(Modal_1.Modal.ButtonFooter, null,
            React.createElement(Button_1.Button, { onClick: learnMoreAboutDeliveryIssue, size: Button_1.ButtonSize.Medium, variant: Button_1.ButtonVariant.Secondary }, i18n('DeliveryIssue--learnMore')),
            React.createElement(Button_1.Button, { onClick: onClose, ref: focusRef, size: Button_1.ButtonSize.Medium, variant: Button_1.ButtonVariant.Primary, className: "module-delivery-issue-dialog__close-button" }, i18n('Confirmation--confirm')))));
}
exports.DeliveryIssueDialog = DeliveryIssueDialog;
