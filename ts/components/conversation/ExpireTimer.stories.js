"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const ExpireTimer_1 = require("./ExpireTimer");
const story = (0, react_1.storiesOf)('Components/Conversation/ExpireTimer', module);
const createProps = (overrideProps = {}) => ({
    direction: overrideProps.direction || 'outgoing',
    expirationLength: (0, addon_knobs_1.number)('expirationLength', overrideProps.expirationLength || 30 * 1000),
    expirationTimestamp: (0, addon_knobs_1.number)('expirationTimestamp', overrideProps.expirationTimestamp || Date.now() + 30 * 1000),
    withImageNoCaption: (0, addon_knobs_1.boolean)('withImageNoCaption', overrideProps.withImageNoCaption || false),
    withSticker: (0, addon_knobs_1.boolean)('withSticker', overrideProps.withSticker || false),
    withTapToViewExpired: (0, addon_knobs_1.boolean)('withTapToViewExpired', overrideProps.withTapToViewExpired || false),
});
story.add('30 seconds', () => {
    const props = createProps();
    return React.createElement(ExpireTimer_1.ExpireTimer, Object.assign({}, props));
});
story.add('2 minutes', () => {
    const twoMinutes = 60 * 1000 * 2;
    const props = createProps({
        expirationTimestamp: Date.now() + twoMinutes,
        expirationLength: twoMinutes,
    });
    return React.createElement(ExpireTimer_1.ExpireTimer, Object.assign({}, props));
});
story.add('In Progress', () => {
    const props = createProps({
        expirationTimestamp: Date.now() + 15 * 1000,
    });
    return React.createElement(ExpireTimer_1.ExpireTimer, Object.assign({}, props));
});
story.add('Expired', () => {
    const props = createProps({
        expirationTimestamp: Date.now() - 30 * 1000,
    });
    return React.createElement(ExpireTimer_1.ExpireTimer, Object.assign({}, props));
});
story.add('Sticker', () => {
    const props = createProps({
        withSticker: true,
    });
    return React.createElement(ExpireTimer_1.ExpireTimer, Object.assign({}, props));
});
story.add('Tap To View Expired', () => {
    const props = createProps({
        withTapToViewExpired: true,
    });
    return React.createElement(ExpireTimer_1.ExpireTimer, Object.assign({}, props));
});
story.add('Image No Caption', () => {
    const props = createProps({
        withImageNoCaption: true,
    });
    return (React.createElement("div", { style: { backgroundColor: 'darkgreen' } },
        React.createElement(ExpireTimer_1.ExpireTimer, Object.assign({}, props))));
});
story.add('Incoming', () => {
    const props = createProps({
        direction: 'incoming',
    });
    return (React.createElement("div", { style: { backgroundColor: 'darkgreen' } },
        React.createElement(ExpireTimer_1.ExpireTimer, Object.assign({}, props))));
});
story.add('Expiration Too Far Out', () => {
    const props = createProps({
        expirationTimestamp: Date.now() + 150 * 1000,
    });
    return React.createElement(ExpireTimer_1.ExpireTimer, Object.assign({}, props));
});
