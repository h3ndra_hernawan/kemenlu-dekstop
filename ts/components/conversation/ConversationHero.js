"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationHero = void 0;
const react_1 = __importStar(require("react"));
const Avatar_1 = require("../Avatar");
const ContactName_1 = require("./ContactName");
const About_1 = require("./About");
const GroupDescription_1 = require("./GroupDescription");
const SharedGroupNames_1 = require("../SharedGroupNames");
const ConfirmationDialog_1 = require("../ConfirmationDialog");
const Button_1 = require("../Button");
const shouldBlurAvatar_1 = require("../../util/shouldBlurAvatar");
const log = __importStar(require("../../logging/log"));
const openLinkInWebBrowser_1 = require("../../util/openLinkInWebBrowser");
const renderMembershipRow = ({ acceptedMessageRequest, conversationType, i18n, isMe, onClickMessageRequestWarning, phoneNumber, sharedGroupNames, }) => {
    const className = 'module-conversation-hero__membership';
    if (conversationType !== 'direct') {
        return null;
    }
    if (isMe) {
        return react_1.default.createElement("div", { className: className }, i18n('noteToSelfHero'));
    }
    if (sharedGroupNames.length > 0) {
        return (react_1.default.createElement("div", { className: className },
            react_1.default.createElement(SharedGroupNames_1.SharedGroupNames, { i18n: i18n, nameClassName: `${className}__name`, sharedGroupNames: sharedGroupNames })));
    }
    if (acceptedMessageRequest) {
        if (phoneNumber) {
            return null;
        }
        return react_1.default.createElement("div", { className: className }, i18n('no-groups-in-common'));
    }
    return (react_1.default.createElement("div", { className: "module-conversation-hero__message-request-warning" },
        react_1.default.createElement("div", { className: "module-conversation-hero__message-request-warning__message" }, i18n('no-groups-in-common-warning')),
        react_1.default.createElement(Button_1.Button, { onClick: onClickMessageRequestWarning, size: Button_1.ButtonSize.Small, variant: Button_1.ButtonVariant.SecondaryAffirmative }, i18n('MessageRequestWarning__learn-more'))));
};
const ConversationHero = ({ i18n, about, acceptedMessageRequest, avatarPath, badge, color, conversationType, groupDescription, isMe, membersCount, sharedGroupNames = [], name, phoneNumber, profileName, theme, title, onHeightChange, unblurAvatar, unblurredAvatarPath, updateSharedGroups, }) => {
    const firstRenderRef = (0, react_1.useRef)(true);
    const [isShowingMessageRequestWarning, setIsShowingMessageRequestWarning] = (0, react_1.useState)(false);
    const closeMessageRequestWarning = () => {
        setIsShowingMessageRequestWarning(false);
    };
    (0, react_1.useEffect)(() => {
        // Kick off the expensive hydration of the current sharedGroupNames
        updateSharedGroups();
    }, [updateSharedGroups]);
    const sharedGroupNamesStringified = JSON.stringify(sharedGroupNames);
    (0, react_1.useEffect)(() => {
        const isFirstRender = firstRenderRef.current;
        if (isFirstRender) {
            firstRenderRef.current = false;
            return;
        }
        log.info('ConversationHero: calling onHeightChange');
        onHeightChange();
    }, [
        about,
        conversationType,
        groupDescription,
        isMe,
        membersCount,
        name,
        onHeightChange,
        phoneNumber,
        profileName,
        title,
        sharedGroupNamesStringified,
    ]);
    let avatarBlur;
    let avatarOnClick;
    if ((0, shouldBlurAvatar_1.shouldBlurAvatar)({
        acceptedMessageRequest,
        avatarPath,
        isMe,
        sharedGroupNames,
        unblurredAvatarPath,
    })) {
        avatarBlur = Avatar_1.AvatarBlur.BlurPictureWithClickToView;
        avatarOnClick = unblurAvatar;
    }
    else {
        avatarBlur = Avatar_1.AvatarBlur.NoBlur;
    }
    const phoneNumberOnly = Boolean(!name && !profileName && conversationType === 'direct');
    /* eslint-disable no-nested-ternary */
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("div", { className: "module-conversation-hero" },
            react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, badge: badge, blur: avatarBlur, className: "module-conversation-hero__avatar", color: color, conversationType: conversationType, i18n: i18n, isMe: isMe, name: name, noteToSelf: isMe, onClick: avatarOnClick, profileName: profileName, sharedGroupNames: sharedGroupNames, size: 112, theme: theme, title: title }),
            react_1.default.createElement("h1", { className: "module-conversation-hero__profile-name" }, isMe ? i18n('noteToSelf') : react_1.default.createElement(ContactName_1.ContactName, { title: title })),
            about && !isMe && (react_1.default.createElement("div", { className: "module-about__container" },
                react_1.default.createElement(About_1.About, { text: about }))),
            !isMe ? (react_1.default.createElement("div", { className: "module-conversation-hero__with" }, groupDescription ? (react_1.default.createElement(GroupDescription_1.GroupDescription, { i18n: i18n, title: title, text: groupDescription })) : membersCount === 1 ? (i18n('ConversationHero--members-1')) : membersCount !== undefined ? (i18n('ConversationHero--members', [`${membersCount}`])) : phoneNumberOnly ? null : (phoneNumber))) : null,
            renderMembershipRow({
                acceptedMessageRequest,
                conversationType,
                i18n,
                isMe,
                onClickMessageRequestWarning() {
                    setIsShowingMessageRequestWarning(true);
                },
                phoneNumber,
                sharedGroupNames,
            })),
        isShowingMessageRequestWarning && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onClose: closeMessageRequestWarning, actions: [
                {
                    text: i18n('MessageRequestWarning__dialog__learn-even-more'),
                    action: () => {
                        (0, openLinkInWebBrowser_1.openLinkInWebBrowser)('https://support.signal.org/hc/articles/360007459591');
                        closeMessageRequestWarning();
                    },
                },
            ] }, i18n('MessageRequestWarning__dialog__details')))));
    /* eslint-enable no-nested-ternary */
};
exports.ConversationHero = ConversationHero;
