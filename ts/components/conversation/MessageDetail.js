"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageDetail = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const moment_1 = __importDefault(require("moment"));
const lodash_1 = require("lodash");
const Avatar_1 = require("../Avatar");
const ContactName_1 = require("./ContactName");
const Message_1 = require("./Message");
const mapUtil_1 = require("../../util/mapUtil");
const MessageSendState_1 = require("../../messages/MessageSendState");
const _util_1 = require("../_util");
const log = __importStar(require("../../logging/log"));
const Timestamp_1 = require("./Timestamp");
const contactSortCollator = new Intl.Collator();
const _keyForError = (error) => {
    return `${error.name}-${error.message}`;
};
class MessageDetail extends react_1.default.Component {
    constructor() {
        super(...arguments);
        this.focusRef = react_1.default.createRef();
        this.messageContainerRef = react_1.default.createRef();
    }
    componentDidMount() {
        // When this component is created, it's initially not part of the DOM, and then it's
        //   added off-screen and animated in. This ensures that the focus takes.
        setTimeout(() => {
            if (this.focusRef.current) {
                this.focusRef.current.focus();
            }
        });
    }
    renderAvatar(contact) {
        const { getPreferredBadge, i18n, theme } = this.props;
        const { acceptedMessageRequest, avatarPath, badges, color, isMe, name, phoneNumber, profileName, sharedGroupNames, title, unblurredAvatarPath, } = contact;
        return (react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: acceptedMessageRequest, avatarPath: avatarPath, badge: getPreferredBadge(badges), color: color, conversationType: "direct", i18n: i18n, isMe: isMe, name: name, phoneNumber: phoneNumber, profileName: profileName, theme: theme, title: title, sharedGroupNames: sharedGroupNames, size: Avatar_1.AvatarSize.THIRTY_SIX, unblurredAvatarPath: unblurredAvatarPath }));
    }
    renderContact(contact) {
        const { i18n, showSafetyNumber } = this.props;
        const errors = contact.errors || [];
        const errorComponent = contact.isOutgoingKeyError ? (react_1.default.createElement("div", { className: "module-message-detail__contact__error-buttons" },
            react_1.default.createElement("button", { type: "button", className: "module-message-detail__contact__show-safety-number", onClick: () => showSafetyNumber(contact.id) }, i18n('showSafetyNumber')))) : null;
        const unidentifiedDeliveryComponent = contact.isUnidentifiedDelivery ? (react_1.default.createElement("div", { className: "module-message-detail__contact__unidentified-delivery-icon" })) : null;
        return (react_1.default.createElement("div", { key: contact.id, className: "module-message-detail__contact" },
            this.renderAvatar(contact),
            react_1.default.createElement("div", { className: "module-message-detail__contact__text" },
                react_1.default.createElement("div", { className: "module-message-detail__contact__name" },
                    react_1.default.createElement(ContactName_1.ContactName, { title: contact.title })),
                errors.map(error => (react_1.default.createElement("div", { key: _keyForError(error), className: "module-message-detail__contact__error" }, error.message)))),
            errorComponent,
            unidentifiedDeliveryComponent,
            contact.statusTimestamp && (react_1.default.createElement(Timestamp_1.Timestamp, { extended: true, i18n: i18n, module: "module-message-detail__status-timestamp", timestamp: contact.statusTimestamp }))));
    }
    renderContactGroup(sendStatus, contacts) {
        const { i18n } = this.props;
        if (!contacts || !contacts.length) {
            return null;
        }
        const i18nKey = sendStatus === undefined ? 'from' : `MessageDetailsHeader--${sendStatus}`;
        const sortedContacts = [...contacts].sort((a, b) => contactSortCollator.compare(a.title, b.title));
        return (react_1.default.createElement("div", { key: i18nKey, className: "module-message-detail__contact-group" },
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message-detail__contact-group__header', sendStatus &&
                    `module-message-detail__contact-group__header--${sendStatus}`) }, i18n(i18nKey)),
            sortedContacts.map(contact => this.renderContact(contact))));
    }
    renderContacts() {
        // This assumes that the list either contains one sender (a status of `undefined`) or
        //   1+ contacts with `SendStatus`es, but it doesn't check that assumption.
        const { contacts } = this.props;
        const contactsBySendStatus = (0, mapUtil_1.groupBy)(contacts, contact => contact.status);
        return (react_1.default.createElement("div", { className: "module-message-detail__contact-container" }, [
            undefined,
            MessageSendState_1.SendStatus.Failed,
            MessageSendState_1.SendStatus.Viewed,
            MessageSendState_1.SendStatus.Read,
            MessageSendState_1.SendStatus.Delivered,
            MessageSendState_1.SendStatus.Sent,
            MessageSendState_1.SendStatus.Pending,
        ].map(sendStatus => this.renderContactGroup(sendStatus, contactsBySendStatus.get(sendStatus)))));
    }
    render() {
        const { errors, message, receivedAt, sentAt, checkForAccount, clearSelectedMessage, contactNameColor, displayTapToViewMessage, doubleCheckMissingQuoteReference, i18n, interactionMode, kickOffAttachmentDownload, markAttachmentAsCorrupted, markViewed, openConversation, openLink, reactToMessage, renderAudioAttachment, renderEmojiPicker, renderReactionPicker, replyToMessage, retrySend, showContactDetail, showContactModal, showExpiredIncomingTapToViewToast, showExpiredOutgoingTapToViewToast, showForwardMessageModal, showVisualAttachment, theme, } = this.props;
        return (
        // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
        react_1.default.createElement("div", { className: "module-message-detail", tabIndex: 0, ref: this.focusRef },
            react_1.default.createElement("div", { className: "module-message-detail__message-container", ref: this.messageContainerRef },
                react_1.default.createElement(Message_1.Message, Object.assign({}, message, { renderingContext: "conversation/MessageDetail", checkForAccount: checkForAccount, clearSelectedMessage: clearSelectedMessage, contactNameColor: contactNameColor, containerElementRef: this.messageContainerRef, containerWidthBreakpoint: _util_1.WidthBreakpoint.Wide, deleteMessage: () => log.warn('MessageDetail: deleteMessage called!'), deleteMessageForEveryone: () => log.warn('MessageDetail: deleteMessageForEveryone called!'), disableMenu: true, disableScroll: true, displayLimit: Number.MAX_SAFE_INTEGER, displayTapToViewMessage: displayTapToViewMessage, downloadAttachment: () => log.warn('MessageDetail: deleteMessageForEveryone called!'), doubleCheckMissingQuoteReference: doubleCheckMissingQuoteReference, i18n: i18n, interactionMode: interactionMode, kickOffAttachmentDownload: kickOffAttachmentDownload, markAttachmentAsCorrupted: markAttachmentAsCorrupted, markViewed: markViewed, messageExpanded: lodash_1.noop, onHeightChange: lodash_1.noop, openConversation: openConversation, openLink: openLink, reactToMessage: reactToMessage, renderAudioAttachment: renderAudioAttachment, renderEmojiPicker: renderEmojiPicker, renderReactionPicker: renderReactionPicker, replyToMessage: replyToMessage, retrySend: retrySend, showForwardMessageModal: showForwardMessageModal, scrollToQuotedMessage: () => {
                        log.warn('MessageDetail: scrollToQuotedMessage called!');
                    }, showContactDetail: showContactDetail, showContactModal: showContactModal, showExpiredIncomingTapToViewToast: showExpiredIncomingTapToViewToast, showExpiredOutgoingTapToViewToast: showExpiredOutgoingTapToViewToast, showMessageDetail: () => {
                        log.warn('MessageDetail: deleteMessageForEveryone called!');
                    }, showVisualAttachment: showVisualAttachment, theme: theme }))),
            react_1.default.createElement("table", { className: "module-message-detail__info" },
                react_1.default.createElement("tbody", null,
                    (errors || []).map(error => (react_1.default.createElement("tr", { key: _keyForError(error) },
                        react_1.default.createElement("td", { className: "module-message-detail__label" }, i18n('error')),
                        react_1.default.createElement("td", null,
                            ' ',
                            react_1.default.createElement("span", { className: "error-message" }, error.message),
                            ' ')))),
                    react_1.default.createElement("tr", null,
                        react_1.default.createElement("td", { className: "module-message-detail__label" }, i18n('sent')),
                        react_1.default.createElement("td", null,
                            (0, moment_1.default)(sentAt).format('LLLL'),
                            ' ',
                            react_1.default.createElement("span", { className: "module-message-detail__unix-timestamp" },
                                "(",
                                sentAt,
                                ")"))),
                    receivedAt ? (react_1.default.createElement("tr", null,
                        react_1.default.createElement("td", { className: "module-message-detail__label" }, i18n('received')),
                        react_1.default.createElement("td", null,
                            (0, moment_1.default)(receivedAt).format('LLLL'),
                            ' ',
                            react_1.default.createElement("span", { className: "module-message-detail__unix-timestamp" },
                                "(",
                                receivedAt,
                                ")")))) : null)),
            this.renderContacts()));
    }
}
exports.MessageDetail = MessageDetail;
