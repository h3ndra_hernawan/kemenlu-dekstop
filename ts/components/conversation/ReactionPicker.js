"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReactionPicker = void 0;
const React = __importStar(require("react"));
const lib_1 = require("../emoji/lib");
const useRestoreFocus_1 = require("../../hooks/useRestoreFocus");
const ReactionPickerPicker_1 = require("../ReactionPickerPicker");
exports.ReactionPicker = React.forwardRef(({ i18n, onClose, onPick, onSetSkinTone, openCustomizePreferredReactionsModal, preferredReactionEmoji, renderEmojiPicker, selected, style, }, ref) => {
    const [pickingOther, setPickingOther] = React.useState(false);
    // Handle escape key
    React.useEffect(() => {
        const handler = (e) => {
            if (onClose && e.key === 'Escape') {
                onClose();
            }
        };
        document.addEventListener('keydown', handler);
        return () => {
            document.removeEventListener('keydown', handler);
        };
    }, [onClose]);
    // Handle EmojiPicker::onPickEmoji
    const onPickEmoji = React.useCallback(({ shortName, skinTone: pickedSkinTone }) => {
        onPick((0, lib_1.convertShortName)(shortName, pickedSkinTone));
    }, [onPick]);
    // Focus first button and restore focus on unmount
    const [focusRef] = (0, useRestoreFocus_1.useRestoreFocus)();
    if (pickingOther) {
        return renderEmojiPicker({
            onClickSettings: openCustomizePreferredReactionsModal,
            onClose,
            onPickEmoji,
            onSetSkinTone,
            ref,
            style,
        });
    }
    const otherSelected = selected && !preferredReactionEmoji.includes(selected);
    let moreButton;
    if (otherSelected) {
        moreButton = (React.createElement(ReactionPickerPicker_1.ReactionPickerPickerEmojiButton, { emoji: selected, onClick: () => {
                onPick(selected);
            }, isSelected: true, title: i18n('Reactions--remove') }));
    }
    else {
        moreButton = (React.createElement(ReactionPickerPicker_1.ReactionPickerPickerMoreButton, { i18n: i18n, onClick: () => {
                setPickingOther(true);
            } }));
    }
    // This logic is here to avoid selecting duplicate emoji.
    let hasSelectedSomething = false;
    return (React.createElement(ReactionPickerPicker_1.ReactionPickerPicker, { isSomethingSelected: typeof selected === 'number', pickerStyle: ReactionPickerPicker_1.ReactionPickerPickerStyle.Picker, ref: ref, style: style },
        preferredReactionEmoji.map((emoji, index) => {
            const maybeFocusRef = index === 0 ? focusRef : undefined;
            const isSelected = !hasSelectedSomething && emoji === selected;
            if (isSelected) {
                hasSelectedSomething = true;
            }
            return (React.createElement(ReactionPickerPicker_1.ReactionPickerPickerEmojiButton, { emoji: emoji, isSelected: isSelected, 
                // The index is the only thing that uniquely identifies the emoji, because
                //   there can be duplicates in the list.
                // eslint-disable-next-line react/no-array-index-key
                key: index, onClick: () => {
                    onPick(emoji);
                }, ref: maybeFocusRef }));
        }),
        moreButton));
});
