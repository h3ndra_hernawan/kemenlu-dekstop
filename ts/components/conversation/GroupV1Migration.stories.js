"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable-next-line max-classes-per-file */
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const lodash_1 = require("lodash");
const addon_knobs_1 = require("@storybook/addon-knobs");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const GroupV1Migration_1 = require("./GroupV1Migration");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const contact1 = (0, getDefaultConversation_1.getDefaultConversation)({
    title: 'Alice',
    phoneNumber: '+1 (300) 555-000',
    id: 'guid-1',
});
const contact2 = (0, getDefaultConversation_1.getDefaultConversation)({
    title: 'Bob',
    phoneNumber: '+1 (300) 555-000',
    id: 'guid-2',
});
const createProps = (overrideProps = {}) => ({
    areWeInvited: (0, addon_knobs_1.boolean)('areWeInvited', (0, lodash_1.isBoolean)(overrideProps.areWeInvited) ? overrideProps.areWeInvited : false),
    droppedMembers: overrideProps.droppedMembers || [contact1],
    i18n,
    invitedMembers: overrideProps.invitedMembers || [contact2],
});
const stories = (0, react_1.storiesOf)('Components/Conversation/GroupV1Migration', module);
stories.add('You were invited', () => (React.createElement(GroupV1Migration_1.GroupV1Migration, Object.assign({}, createProps({
    areWeInvited: true,
})))));
stories.add('Single dropped and single invited member', () => (React.createElement(GroupV1Migration_1.GroupV1Migration, Object.assign({}, createProps()))));
stories.add('Multiple dropped and invited members', () => (React.createElement(GroupV1Migration_1.GroupV1Migration, Object.assign({}, createProps({
    invitedMembers: [contact1, contact2],
    droppedMembers: [contact1, contact2],
})))));
stories.add('Just invited members', () => (React.createElement(GroupV1Migration_1.GroupV1Migration, Object.assign({}, createProps({
    invitedMembers: [contact1, contact1, contact2, contact2],
    droppedMembers: [],
})))));
stories.add('Just dropped members', () => (React.createElement(GroupV1Migration_1.GroupV1Migration, Object.assign({}, createProps({
    invitedMembers: [],
    droppedMembers: [contact1, contact1, contact2, contact2],
})))));
stories.add('No dropped or invited members', () => (React.createElement(GroupV1Migration_1.GroupV1Migration, Object.assign({}, createProps({
    invitedMembers: [],
    droppedMembers: [],
})))));
