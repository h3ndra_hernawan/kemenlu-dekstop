"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const AtMentionify_1 = require("./AtMentionify");
const story = (0, react_1.storiesOf)('Components/Conversation/AtMentionify', module);
const createProps = (overrideProps = {}) => ({
    bodyRanges: overrideProps.bodyRanges,
    direction: (0, addon_knobs_1.select)('direction', { incoming: 'incoming', outgoing: 'outgoing' }, overrideProps.direction || 'incoming'),
    openConversation: (0, addon_actions_1.action)('openConversation'),
    text: (0, addon_knobs_1.text)('text', overrideProps.text || ''),
});
story.add('No @mentions', () => {
    const props = createProps({
        text: 'Hello World',
    });
    return React.createElement(AtMentionify_1.AtMentionify, Object.assign({}, props));
});
story.add('Multiple @Mentions', () => {
    const bodyRanges = [
        {
            start: 4,
            length: 1,
            mentionUuid: 'abc',
            replacementText: 'Professor Farnsworth',
        },
        {
            start: 2,
            length: 1,
            mentionUuid: 'def',
            replacementText: 'Philip J Fry',
        },
        {
            start: 0,
            length: 1,
            mentionUuid: 'xyz',
            replacementText: 'Yancy Fry',
        },
    ];
    const props = createProps({
        bodyRanges,
        direction: 'outgoing',
        text: AtMentionify_1.AtMentionify.preprocessMentions('\uFFFC \uFFFC \uFFFC', bodyRanges),
    });
    return React.createElement(AtMentionify_1.AtMentionify, Object.assign({}, props));
});
story.add('Complex @mentions', () => {
    const bodyRanges = [
        {
            start: 80,
            length: 1,
            mentionUuid: 'ioe',
            replacementText: 'Cereal Killer',
        },
        {
            start: 78,
            length: 1,
            mentionUuid: 'fdr',
            replacementText: 'Acid Burn',
        },
        {
            start: 4,
            length: 1,
            mentionUuid: 'ope',
            replacementText: 'Zero Cool',
        },
    ];
    const props = createProps({
        bodyRanges,
        text: AtMentionify_1.AtMentionify.preprocessMentions('Hey \uFFFC\nCheck out https://www.signal.org I think you will really like it 😍\n\ncc \uFFFC \uFFFC', bodyRanges),
    });
    return React.createElement(AtMentionify_1.AtMentionify, Object.assign({}, props));
});
