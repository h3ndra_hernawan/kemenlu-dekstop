"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable-next-line max-classes-per-file */
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const setupI18n_1 = require("../../util/setupI18n");
const UUID_1 = require("../../types/UUID");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const protobuf_1 = require("../../protobuf");
const GroupV2Change_1 = require("./GroupV2Change");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const OUR_ID = UUID_1.UUID.generate().toString();
const CONTACT_A = UUID_1.UUID.generate().toString();
const CONTACT_B = UUID_1.UUID.generate().toString();
const CONTACT_C = UUID_1.UUID.generate().toString();
const ADMIN_A = UUID_1.UUID.generate().toString();
const INVITEE_A = UUID_1.UUID.generate().toString();
const AccessControlEnum = protobuf_1.SignalService.AccessControl.AccessRequired;
const RoleEnum = protobuf_1.SignalService.Member.Role;
const renderContact = (conversationId) => (React.createElement(React.Fragment, { key: conversationId }, `Conversation(${conversationId})`));
const renderChange = (change, groupName) => (React.createElement(GroupV2Change_1.GroupV2Change, { change: change, groupName: groupName, i18n: i18n, ourUuid: OUR_ID, renderContact: renderContact }));
(0, react_1.storiesOf)('Components/Conversation/GroupV2Change', module)
    .add('Multiple', () => {
    return (React.createElement(React.Fragment, null, renderChange({
        from: CONTACT_A,
        details: [
            {
                type: 'title',
                newTitle: 'Saturday Running',
            },
            {
                type: 'avatar',
                removed: false,
            },
            {
                type: 'description',
                description: 'This is a long description.\n\nWe need a dialog to view it all!\n\nIt has a link to https://example.com',
            },
            {
                type: 'member-add',
                uuid: OUR_ID,
            },
            {
                type: 'description',
                description: 'Another description',
            },
            {
                type: 'member-privilege',
                uuid: OUR_ID,
                newPrivilege: RoleEnum.ADMINISTRATOR,
            },
        ],
    })));
})
    .add('Create', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'create',
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'create',
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'create',
                },
            ],
        })));
})
    .add('Title', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'title',
                    newTitle: 'Saturday Running',
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'title',
                    newTitle: 'Saturday Running',
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'title',
                    newTitle: 'Saturday Running',
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'title',
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'title',
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'title',
                },
            ],
        })));
})
    .add('Avatar', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'avatar',
                    removed: false,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'avatar',
                    removed: false,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'avatar',
                    removed: false,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'avatar',
                    removed: true,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'avatar',
                    removed: true,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'avatar',
                    removed: true,
                },
            ],
        })));
})
    .add('Access (Attributes)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'access-attributes',
                    newPrivilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'access-attributes',
                    newPrivilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'access-attributes',
                    newPrivilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'access-attributes',
                    newPrivilege: AccessControlEnum.MEMBER,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'access-attributes',
                    newPrivilege: AccessControlEnum.MEMBER,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'access-attributes',
                    newPrivilege: AccessControlEnum.MEMBER,
                },
            ],
        })));
})
    .add('Access (Members)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'access-members',
                    newPrivilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'access-members',
                    newPrivilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'access-members',
                    newPrivilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'access-members',
                    newPrivilege: AccessControlEnum.MEMBER,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'access-members',
                    newPrivilege: AccessControlEnum.MEMBER,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'access-members',
                    newPrivilege: AccessControlEnum.MEMBER,
                },
            ],
        })));
})
    .add('Access (Invite Link)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'access-invite-link',
                    newPrivilege: AccessControlEnum.ANY,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'access-invite-link',
                    newPrivilege: AccessControlEnum.ANY,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'access-invite-link',
                    newPrivilege: AccessControlEnum.ANY,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'access-invite-link',
                    newPrivilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'access-invite-link',
                    newPrivilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'access-invite-link',
                    newPrivilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        })));
})
    .add('Member Add', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-add',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-add',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-add',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-add',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: CONTACT_B,
            details: [
                {
                    type: 'member-add',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-add',
                    uuid: CONTACT_A,
                },
            ],
        })));
})
    .add('Member Add (from invited)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: OUR_ID,
                    inviter: CONTACT_B,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: OUR_ID,
                    inviter: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: CONTACT_A,
                    inviter: CONTACT_B,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: CONTACT_B,
                    inviter: CONTACT_C,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: CONTACT_A,
                    inviter: CONTACT_B,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: OUR_ID,
                    inviter: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: CONTACT_A,
                    inviter: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: CONTACT_A,
                    inviter: CONTACT_B,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-add-from-invite',
                    uuid: CONTACT_A,
                },
            ],
        })));
})
    .add('Member Add (from link)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-add-from-link',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-add-from-link',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-add-from-link',
                    uuid: CONTACT_A,
                },
            ],
        })));
})
    .add('Member Add (from admin approval)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'member-add-from-admin-approval',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-add-from-admin-approval',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-add-from-admin-approval',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'member-add-from-admin-approval',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-add-from-admin-approval',
                    uuid: CONTACT_A,
                },
            ],
        })));
})
    .add('Member Remove', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-remove',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-remove',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-remove',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-remove',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-remove',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: CONTACT_B,
            details: [
                {
                    type: 'member-remove',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-remove',
                    uuid: CONTACT_A,
                },
            ],
        })));
})
    .add('Member Privilege', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-privilege',
                    uuid: OUR_ID,
                    newPrivilege: RoleEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-privilege',
                    uuid: OUR_ID,
                    newPrivilege: RoleEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-privilege',
                    uuid: CONTACT_A,
                    newPrivilege: RoleEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'member-privilege',
                    uuid: CONTACT_A,
                    newPrivilege: RoleEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-privilege',
                    uuid: CONTACT_A,
                    newPrivilege: RoleEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'member-privilege',
                    uuid: OUR_ID,
                    newPrivilege: RoleEnum.DEFAULT,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-privilege',
                    uuid: OUR_ID,
                    newPrivilege: RoleEnum.DEFAULT,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'member-privilege',
                    uuid: CONTACT_A,
                    newPrivilege: RoleEnum.DEFAULT,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'member-privilege',
                    uuid: CONTACT_A,
                    newPrivilege: RoleEnum.DEFAULT,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'member-privilege',
                    uuid: CONTACT_A,
                    newPrivilege: RoleEnum.DEFAULT,
                },
            ],
        })));
})
    .add('Pending Add - one', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'pending-add-one',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'pending-add-one',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'pending-add-one',
                    uuid: INVITEE_A,
                },
            ],
        }),
        renderChange({
            from: CONTACT_B,
            details: [
                {
                    type: 'pending-add-one',
                    uuid: INVITEE_A,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'pending-add-one',
                    uuid: INVITEE_A,
                },
            ],
        })));
})
    .add('Pending Add - many', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'pending-add-many',
                    count: 5,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'pending-add-many',
                    count: 5,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'pending-add-many',
                    count: 5,
                },
            ],
        })));
})
    .add('Pending Remove - one', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: INVITEE_A,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                    inviter: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                    inviter: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                    inviter: OUR_ID,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                    inviter: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: INVITEE_A,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                },
            ],
        }),
        renderChange({
            from: INVITEE_A,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                    inviter: CONTACT_B,
                },
            ],
        }),
        renderChange({
            from: CONTACT_B,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: OUR_ID,
                    inviter: CONTACT_B,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: CONTACT_B,
                    inviter: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: CONTACT_C,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                    inviter: CONTACT_B,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                    inviter: CONTACT_B,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                    inviter: CONTACT_B,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                },
            ],
        }),
        renderChange({
            from: CONTACT_B,
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'pending-remove-one',
                    uuid: INVITEE_A,
                },
            ],
        })));
})
    .add('Pending Remove - many', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'pending-remove-many',
                    count: 5,
                    inviter: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'pending-remove-many',
                    count: 5,
                    inviter: OUR_ID,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'pending-remove-many',
                    count: 5,
                    inviter: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'pending-remove-many',
                    count: 5,
                    inviter: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'pending-remove-many',
                    count: 5,
                    inviter: CONTACT_A,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'pending-remove-many',
                    count: 5,
                    inviter: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'pending-remove-many',
                    count: 5,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'pending-remove-many',
                    count: 5,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'pending-remove-many',
                    count: 5,
                },
            ],
        })));
})
    .add('Admin Approval (Add)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            details: [
                {
                    type: 'admin-approval-add-one',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'admin-approval-add-one',
                    uuid: CONTACT_A,
                },
            ],
        })));
})
    .add('Admin Approval (Remove)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'admin-approval-remove-one',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'admin-approval-remove-one',
                    uuid: OUR_ID,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'admin-approval-remove-one',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: CONTACT_A,
            details: [
                {
                    type: 'admin-approval-remove-one',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'admin-approval-remove-one',
                    uuid: CONTACT_A,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'admin-approval-remove-one',
                    uuid: CONTACT_A,
                },
            ],
        })));
})
    .add('Group Link (Add)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'group-link-add',
                    privilege: AccessControlEnum.ANY,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'group-link-add',
                    privilege: AccessControlEnum.ANY,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'group-link-add',
                    privilege: AccessControlEnum.ANY,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'group-link-add',
                    privilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'group-link-add',
                    privilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'group-link-add',
                    privilege: AccessControlEnum.ADMINISTRATOR,
                },
            ],
        })));
})
    .add('Group Link (Reset)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'group-link-reset',
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'group-link-reset',
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'group-link-reset',
                },
            ],
        })));
})
    .add('Group Link (Remove)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'group-link-remove',
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'group-link-remove',
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'group-link-remove',
                },
            ],
        })));
})
    .add('Description (Remove)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    removed: true,
                    type: 'description',
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    removed: true,
                    type: 'description',
                },
            ],
        }),
        renderChange({
            details: [
                {
                    removed: true,
                    type: 'description',
                },
            ],
        })));
})
    .add('Description (Change)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'description',
                    description: 'This is a long description.\n\nWe need a dialog to view it all!\n\nIt has a link to https://example.com',
                },
            ],
        }, 'We do hikes 🌲'),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'description',
                    description: 'This is a long description.\n\nWe need a dialog to view it all!\n\nIt has a link to https://example.com',
                },
            ],
        }, 'We do hikes 🌲'),
        renderChange({
            details: [
                {
                    type: 'description',
                    description: 'This is a long description.\n\nWe need a dialog to view it all!\n\nIt has a link to https://example.com',
                },
            ],
        }, 'We do hikes 🌲')));
})
    .add('Announcement Group (Change)', () => {
    return (React.createElement(React.Fragment, null,
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'announcements-only',
                    announcementsOnly: true,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'announcements-only',
                    announcementsOnly: true,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'announcements-only',
                    announcementsOnly: true,
                },
            ],
        }),
        renderChange({
            from: OUR_ID,
            details: [
                {
                    type: 'announcements-only',
                    announcementsOnly: false,
                },
            ],
        }),
        renderChange({
            from: ADMIN_A,
            details: [
                {
                    type: 'announcements-only',
                    announcementsOnly: false,
                },
            ],
        }),
        renderChange({
            details: [
                {
                    type: 'announcements-only',
                    announcementsOnly: false,
                },
            ],
        })));
});
