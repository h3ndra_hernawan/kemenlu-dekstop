"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactSpoofingReviewDialog = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const MessageRequestActionsConfirmation_1 = require("./MessageRequestActionsConfirmation");
const contactSpoofing_1 = require("../../util/contactSpoofing");
const Modal_1 = require("../Modal");
const RemoveGroupMemberConfirmationDialog_1 = require("./RemoveGroupMemberConfirmationDialog");
const ContactSpoofingReviewDialogPerson_1 = require("./ContactSpoofingReviewDialogPerson");
const Button_1 = require("../Button");
const Intl_1 = require("../Intl");
const Emojify_1 = require("./Emojify");
const assert_1 = require("../../util/assert");
const missingCaseError_1 = require("../../util/missingCaseError");
const isInSystemContacts_1 = require("../../util/isInSystemContacts");
var ConfirmationStateType;
(function (ConfirmationStateType) {
    ConfirmationStateType[ConfirmationStateType["ConfirmingDelete"] = 0] = "ConfirmingDelete";
    ConfirmationStateType[ConfirmationStateType["ConfirmingBlock"] = 1] = "ConfirmingBlock";
    ConfirmationStateType[ConfirmationStateType["ConfirmingGroupRemoval"] = 2] = "ConfirmingGroupRemoval";
})(ConfirmationStateType || (ConfirmationStateType = {}));
const ContactSpoofingReviewDialog = props => {
    const { i18n, onBlock, onBlockAndReportSpam, onClose, onDelete, onShowContactModal, onUnblock, removeMember, } = props;
    const [confirmationState, setConfirmationState] = (0, react_1.useState)();
    if (confirmationState) {
        const { affectedConversation, type } = confirmationState;
        switch (type) {
            case ConfirmationStateType.ConfirmingDelete:
            case ConfirmationStateType.ConfirmingBlock:
                return (react_1.default.createElement(MessageRequestActionsConfirmation_1.MessageRequestActionsConfirmation, { i18n: i18n, onBlock: () => {
                        onBlock(affectedConversation.id);
                    }, onBlockAndReportSpam: () => {
                        onBlockAndReportSpam(affectedConversation.id);
                    }, onUnblock: () => {
                        onUnblock(affectedConversation.id);
                    }, onDelete: () => {
                        onDelete(affectedConversation.id);
                    }, title: affectedConversation.title, conversationType: "direct", state: type === ConfirmationStateType.ConfirmingDelete
                        ? MessageRequestActionsConfirmation_1.MessageRequestState.deleting
                        : MessageRequestActionsConfirmation_1.MessageRequestState.blocking, onChangeState: messageRequestState => {
                        switch (messageRequestState) {
                            case MessageRequestActionsConfirmation_1.MessageRequestState.blocking:
                                setConfirmationState({
                                    type: ConfirmationStateType.ConfirmingBlock,
                                    affectedConversation,
                                });
                                break;
                            case MessageRequestActionsConfirmation_1.MessageRequestState.deleting:
                                setConfirmationState({
                                    type: ConfirmationStateType.ConfirmingDelete,
                                    affectedConversation,
                                });
                                break;
                            case MessageRequestActionsConfirmation_1.MessageRequestState.unblocking:
                                (0, assert_1.assert)(false, 'Got unexpected MessageRequestState.unblocking state. Clearing confiration state');
                                setConfirmationState(undefined);
                                break;
                            case MessageRequestActionsConfirmation_1.MessageRequestState.default:
                                setConfirmationState(undefined);
                                break;
                            default:
                                throw (0, missingCaseError_1.missingCaseError)(messageRequestState);
                        }
                    } }));
            case ConfirmationStateType.ConfirmingGroupRemoval:
                return (react_1.default.createElement(RemoveGroupMemberConfirmationDialog_1.RemoveGroupMemberConfirmationDialog, { conversation: affectedConversation, i18n: i18n, onClose: () => {
                        setConfirmationState(undefined);
                    }, onRemove: () => {
                        removeMember(affectedConversation.id);
                    } }));
            default:
                throw (0, missingCaseError_1.missingCaseError)(type);
        }
    }
    let title;
    let contents;
    switch (props.type) {
        case contactSpoofing_1.ContactSpoofingType.DirectConversationWithSameTitle: {
            const { possiblyUnsafeConversation, safeConversation } = props;
            (0, assert_1.assert)(possiblyUnsafeConversation.type === 'direct', '<ContactSpoofingReviewDialog> expected a direct conversation for the "possibly unsafe" conversation');
            (0, assert_1.assert)(safeConversation.type === 'direct', '<ContactSpoofingReviewDialog> expected a direct conversation for the "safe" conversation');
            title = i18n('ContactSpoofingReviewDialog__title');
            contents = (react_1.default.createElement(react_1.default.Fragment, null,
                react_1.default.createElement("p", null, i18n('ContactSpoofingReviewDialog__description')),
                react_1.default.createElement("h2", null, i18n('ContactSpoofingReviewDialog__possibly-unsafe-title')),
                react_1.default.createElement(ContactSpoofingReviewDialogPerson_1.ContactSpoofingReviewDialogPerson, { conversation: possiblyUnsafeConversation, i18n: i18n },
                    react_1.default.createElement("div", { className: "module-ContactSpoofingReviewDialog__buttons" },
                        react_1.default.createElement(Button_1.Button, { variant: Button_1.ButtonVariant.SecondaryDestructive, onClick: () => {
                                setConfirmationState({
                                    type: ConfirmationStateType.ConfirmingDelete,
                                    affectedConversation: possiblyUnsafeConversation,
                                });
                            } }, i18n('MessageRequests--delete')),
                        react_1.default.createElement(Button_1.Button, { variant: Button_1.ButtonVariant.SecondaryDestructive, onClick: () => {
                                setConfirmationState({
                                    type: ConfirmationStateType.ConfirmingBlock,
                                    affectedConversation: possiblyUnsafeConversation,
                                });
                            } }, i18n('MessageRequests--block')))),
                react_1.default.createElement("hr", null),
                react_1.default.createElement("h2", null, i18n('ContactSpoofingReviewDialog__safe-title')),
                react_1.default.createElement(ContactSpoofingReviewDialogPerson_1.ContactSpoofingReviewDialogPerson, { conversation: safeConversation, i18n: i18n, onClick: () => {
                        onShowContactModal(safeConversation.id);
                    } })));
            break;
        }
        case contactSpoofing_1.ContactSpoofingType.MultipleGroupMembersWithSameTitle: {
            const { areWeAdmin, collisionInfoByTitle } = props;
            const unsortedConversationInfos = (0, lodash_1.concat)(
            // This empty array exists to appease Lodash's type definitions.
            [], ...Object.values(collisionInfoByTitle));
            const conversationInfos = (0, lodash_1.orderBy)(unsortedConversationInfos, [
                // We normally use an `Intl.Collator` to sort by title. We do this instead, as
                //   we only really care about stability (not perfect ordering).
                'title',
                'id',
            ]);
            title = i18n('ContactSpoofingReviewDialog__group__title');
            contents = (react_1.default.createElement(react_1.default.Fragment, null,
                react_1.default.createElement("p", null, i18n('ContactSpoofingReviewDialog__group__description', [
                    conversationInfos.length.toString(),
                ])),
                react_1.default.createElement("h2", null, i18n('ContactSpoofingReviewDialog__group__members-header')),
                conversationInfos.map((conversationInfo, index) => {
                    let button;
                    if (areWeAdmin) {
                        button = (react_1.default.createElement(Button_1.Button, { variant: Button_1.ButtonVariant.SecondaryAffirmative, onClick: () => {
                                setConfirmationState({
                                    type: ConfirmationStateType.ConfirmingGroupRemoval,
                                    affectedConversation: conversationInfo.conversation,
                                });
                            } }, i18n('RemoveGroupMemberConfirmation__remove-button')));
                    }
                    else if (conversationInfo.conversation.isBlocked) {
                        button = (react_1.default.createElement(Button_1.Button, { variant: Button_1.ButtonVariant.SecondaryAffirmative, onClick: () => {
                                onUnblock(conversationInfo.conversation.id);
                            } }, i18n('MessageRequests--unblock')));
                    }
                    else if (!(0, isInSystemContacts_1.isInSystemContacts)(conversationInfo.conversation)) {
                        button = (react_1.default.createElement(Button_1.Button, { variant: Button_1.ButtonVariant.SecondaryDestructive, onClick: () => {
                                setConfirmationState({
                                    type: ConfirmationStateType.ConfirmingBlock,
                                    affectedConversation: conversationInfo.conversation,
                                });
                            } }, i18n('MessageRequests--block')));
                    }
                    const { oldName } = conversationInfo;
                    const newName = conversationInfo.conversation.profileName ||
                        conversationInfo.conversation.title;
                    return (react_1.default.createElement(react_1.default.Fragment, null,
                        index !== 0 && react_1.default.createElement("hr", null),
                        react_1.default.createElement(ContactSpoofingReviewDialogPerson_1.ContactSpoofingReviewDialogPerson, { key: conversationInfo.conversation.id, conversation: conversationInfo.conversation, i18n: i18n },
                            Boolean(oldName) && oldName !== newName && (react_1.default.createElement("div", { className: "module-ContactSpoofingReviewDialogPerson__info__property module-ContactSpoofingReviewDialogPerson__info__property--callout" },
                                react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "ContactSpoofingReviewDialog__group__name-change-info", components: {
                                        oldName: react_1.default.createElement(Emojify_1.Emojify, { text: oldName }),
                                        newName: react_1.default.createElement(Emojify_1.Emojify, { text: newName }),
                                    } }))),
                            button && (react_1.default.createElement("div", { className: "module-ContactSpoofingReviewDialog__buttons" }, button)))));
                })));
            break;
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(props);
    }
    return (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, i18n: i18n, moduleClassName: "module-ContactSpoofingReviewDialog", onClose: onClose, title: title }, contents));
};
exports.ContactSpoofingReviewDialog = ContactSpoofingReviewDialog;
