"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const MessageRequestActions_1 = require("./MessageRequestActions");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const getBaseProps = (isGroup = false) => ({
    i18n,
    conversationType: isGroup ? 'group' : 'direct',
    firstName: (0, addon_knobs_1.text)('firstName', 'Cayce'),
    title: isGroup
        ? (0, addon_knobs_1.text)('title', 'NYC Rock Climbers')
        : (0, addon_knobs_1.text)('title', 'Cayce Bollard'),
    onBlock: (0, addon_actions_1.action)('block'),
    onDelete: (0, addon_actions_1.action)('delete'),
    onBlockAndReportSpam: (0, addon_actions_1.action)('blockAndReportSpam'),
    onUnblock: (0, addon_actions_1.action)('unblock'),
    onAccept: (0, addon_actions_1.action)('accept'),
});
(0, react_1.storiesOf)('Components/Conversation/MessageRequestActions', module)
    .add('Direct', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(MessageRequestActions_1.MessageRequestActions, Object.assign({}, getBaseProps()))));
})
    .add('Direct (Blocked)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(MessageRequestActions_1.MessageRequestActions, Object.assign({}, getBaseProps(), { isBlocked: true }))));
})
    .add('Group', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(MessageRequestActions_1.MessageRequestActions, Object.assign({}, getBaseProps(true)))));
})
    .add('Group (Blocked)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(MessageRequestActions_1.MessageRequestActions, Object.assign({}, getBaseProps(true), { isBlocked: true }))));
});
