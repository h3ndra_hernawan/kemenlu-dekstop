"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const MessageBody_1 = require("./MessageBody");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/MessageBody', module);
const createProps = (overrideProps = {}) => ({
    bodyRanges: overrideProps.bodyRanges,
    disableJumbomoji: (0, addon_knobs_1.boolean)('disableJumbomoji', overrideProps.disableJumbomoji || false),
    disableLinks: (0, addon_knobs_1.boolean)('disableLinks', overrideProps.disableLinks || false),
    direction: 'incoming',
    i18n,
    text: (0, addon_knobs_1.text)('text', overrideProps.text || ''),
    textPending: (0, addon_knobs_1.boolean)('textPending', overrideProps.textPending || false),
});
story.add('Links Enabled', () => {
    const props = createProps({
        text: 'Check out https://www.signal.org',
    });
    return React.createElement(MessageBody_1.MessageBody, Object.assign({}, props));
});
story.add('Links Disabled', () => {
    const props = createProps({
        disableLinks: true,
        text: 'Check out https://www.signal.org',
    });
    return React.createElement(MessageBody_1.MessageBody, Object.assign({}, props));
});
story.add('Emoji Size Based On Count', () => {
    const props = createProps();
    return (React.createElement(React.Fragment, null,
        React.createElement(MessageBody_1.MessageBody, Object.assign({}, props, { text: "\uD83D\uDE39" })),
        React.createElement("br", null),
        React.createElement(MessageBody_1.MessageBody, Object.assign({}, props, { text: "\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39" })),
        React.createElement("br", null),
        React.createElement(MessageBody_1.MessageBody, Object.assign({}, props, { text: "\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39" })),
        React.createElement("br", null),
        React.createElement(MessageBody_1.MessageBody, Object.assign({}, props, { text: "\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39" })),
        React.createElement("br", null),
        React.createElement(MessageBody_1.MessageBody, Object.assign({}, props, { text: "\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39\uD83D\uDE39" }))));
});
story.add('Jumbomoji Enabled', () => {
    const props = createProps({
        text: '😹',
    });
    return React.createElement(MessageBody_1.MessageBody, Object.assign({}, props));
});
story.add('Jumbomoji Disabled', () => {
    const props = createProps({
        disableJumbomoji: true,
        text: '😹',
    });
    return React.createElement(MessageBody_1.MessageBody, Object.assign({}, props));
});
story.add('Jumbomoji Disabled by Text', () => {
    const props = createProps({
        text: 'not a jumbo kitty 😹',
    });
    return React.createElement(MessageBody_1.MessageBody, Object.assign({}, props));
});
story.add('Text Pending', () => {
    const props = createProps({
        text: 'Check out https://www.signal.org',
        textPending: true,
    });
    return React.createElement(MessageBody_1.MessageBody, Object.assign({}, props));
});
story.add('@Mention', () => {
    const props = createProps({
        bodyRanges: [
            {
                start: 5,
                length: 1,
                mentionUuid: 'tuv',
                replacementText: 'Bender B Rodriguez 🤖',
            },
        ],
        text: 'Like \uFFFC once said: My story is a lot like yours, only more interesting because it involves robots',
    });
    return React.createElement(MessageBody_1.MessageBody, Object.assign({}, props));
});
story.add('Multiple @Mentions', () => {
    const props = createProps({
        // These are intentionally in a mixed order to test how we deal with that
        bodyRanges: [
            {
                start: 2,
                length: 1,
                mentionUuid: 'def',
                replacementText: 'Philip J Fry',
            },
            {
                start: 4,
                length: 1,
                mentionUuid: 'abc',
                replacementText: 'Professor Farnsworth',
            },
            {
                start: 0,
                length: 1,
                mentionUuid: 'xyz',
                replacementText: 'Yancy Fry',
            },
        ],
        text: '\uFFFC \uFFFC \uFFFC',
    });
    return React.createElement(MessageBody_1.MessageBody, Object.assign({}, props));
});
story.add('Complex MessageBody', () => {
    const props = createProps({
        bodyRanges: [
            // These are intentionally in a mixed order to test how we deal with that
            {
                start: 78,
                length: 1,
                mentionUuid: 'wer',
                replacementText: 'Acid Burn',
            },
            {
                start: 80,
                length: 1,
                mentionUuid: 'xox',
                replacementText: 'Cereal Killer',
            },
            {
                start: 4,
                length: 1,
                mentionUuid: 'ldo',
                replacementText: 'Zero Cool',
            },
        ],
        direction: 'outgoing',
        text: 'Hey \uFFFC\nCheck out https://www.signal.org I think you will really like it 😍\n\ncc \uFFFC \uFFFC',
    });
    return React.createElement(MessageBody_1.MessageBody, Object.assign({}, props));
});
