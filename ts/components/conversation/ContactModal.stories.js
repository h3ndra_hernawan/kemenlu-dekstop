"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const ContactModal_1 = require("./ContactModal");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const getFakeBadge_1 = require("../../test-both/helpers/getFakeBadge");
const Util_1 = require("../../types/Util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ContactModal', module);
const defaultContact = (0, getDefaultConversation_1.getDefaultConversation)({
    id: 'abcdef',
    areWeAdmin: false,
    title: 'Pauline Oliveros',
    phoneNumber: '(333) 444-5515',
    about: '👍 Free to chat',
});
const createProps = (overrideProps = {}) => ({
    areWeAdmin: (0, addon_knobs_1.boolean)('areWeAdmin', overrideProps.areWeAdmin || false),
    badges: overrideProps.badges || [],
    contact: overrideProps.contact || defaultContact,
    hideContactModal: (0, addon_actions_1.action)('hideContactModal'),
    i18n,
    isAdmin: (0, addon_knobs_1.boolean)('isAdmin', overrideProps.isAdmin || false),
    isMember: (0, addon_knobs_1.boolean)('isMember', overrideProps.isMember || true),
    openConversationInternal: (0, addon_actions_1.action)('openConversationInternal'),
    removeMemberFromGroup: (0, addon_actions_1.action)('removeMemberFromGroup'),
    theme: Util_1.ThemeType.light,
    toggleSafetyNumberModal: (0, addon_actions_1.action)('toggleSafetyNumberModal'),
    toggleAdmin: (0, addon_actions_1.action)('toggleAdmin'),
    updateConversationModelSharedGroups: (0, addon_actions_1.action)('updateConversationModelSharedGroups'),
});
story.add('As non-admin', () => {
    const props = createProps({
        areWeAdmin: false,
    });
    return React.createElement(ContactModal_1.ContactModal, Object.assign({}, props));
});
story.add('As admin', () => {
    const props = createProps({
        areWeAdmin: true,
    });
    return React.createElement(ContactModal_1.ContactModal, Object.assign({}, props));
});
story.add('As admin, viewing non-member of group', () => {
    const props = createProps({
        isMember: false,
    });
    return React.createElement(ContactModal_1.ContactModal, Object.assign({}, props));
});
story.add('Without phone number', () => {
    const props = createProps({
        contact: Object.assign(Object.assign({}, defaultContact), { phoneNumber: undefined }),
    });
    return React.createElement(ContactModal_1.ContactModal, Object.assign({}, props));
});
story.add('Viewing self', () => {
    const props = createProps({
        contact: Object.assign(Object.assign({}, defaultContact), { isMe: true }),
    });
    return React.createElement(ContactModal_1.ContactModal, Object.assign({}, props));
});
story.add('With badges', () => {
    const props = createProps({
        badges: (0, getFakeBadge_1.getFakeBadges)(2),
    });
    return React.createElement(ContactModal_1.ContactModal, Object.assign({}, props));
});
