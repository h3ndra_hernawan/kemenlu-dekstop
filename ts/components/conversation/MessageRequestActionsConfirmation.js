"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageRequestActionsConfirmation = exports.MessageRequestState = void 0;
const React = __importStar(require("react"));
const ContactName_1 = require("./ContactName");
const ConfirmationDialog_1 = require("../ConfirmationDialog");
const Intl_1 = require("../Intl");
var MessageRequestState;
(function (MessageRequestState) {
    MessageRequestState[MessageRequestState["blocking"] = 0] = "blocking";
    MessageRequestState[MessageRequestState["deleting"] = 1] = "deleting";
    MessageRequestState[MessageRequestState["unblocking"] = 2] = "unblocking";
    MessageRequestState[MessageRequestState["default"] = 3] = "default";
})(MessageRequestState = exports.MessageRequestState || (exports.MessageRequestState = {}));
const MessageRequestActionsConfirmation = ({ conversationType, i18n, onBlock, onBlockAndReportSpam, onChangeState, onDelete, onUnblock, state, title, }) => {
    if (state === MessageRequestState.blocking) {
        return (React.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onClose: () => {
                onChangeState(MessageRequestState.default);
            }, title: React.createElement(Intl_1.Intl, { i18n: i18n, id: `MessageRequests--block-${conversationType}-confirm-title`, components: [React.createElement(ContactName_1.ContactName, { key: "name", title: title })] }), actions: [
                ...(conversationType === 'direct'
                    ? [
                        {
                            text: i18n('MessageRequests--block-and-report-spam'),
                            action: onBlockAndReportSpam,
                            style: 'negative',
                        },
                    ]
                    : []),
                {
                    text: i18n('MessageRequests--block'),
                    action: onBlock,
                    style: 'negative',
                },
            ] }, i18n(`MessageRequests--block-${conversationType}-confirm-body`)));
    }
    if (state === MessageRequestState.unblocking) {
        return (React.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onClose: () => {
                onChangeState(MessageRequestState.default);
            }, title: React.createElement(Intl_1.Intl, { i18n: i18n, id: "MessageRequests--unblock-confirm-title", components: [React.createElement(ContactName_1.ContactName, { key: "name", title: title })] }), actions: [
                {
                    text: i18n('MessageRequests--unblock'),
                    action: onUnblock,
                    style: 'affirmative',
                },
            ] }, i18n(`MessageRequests--unblock-${conversationType}-confirm-body`)));
    }
    if (state === MessageRequestState.deleting) {
        return (React.createElement(ConfirmationDialog_1.ConfirmationDialog, { i18n: i18n, onClose: () => {
                onChangeState(MessageRequestState.default);
            }, title: React.createElement(Intl_1.Intl, { i18n: i18n, id: `MessageRequests--delete-${conversationType}-confirm-title`, components: [React.createElement(ContactName_1.ContactName, { key: "name", title: title })] }), actions: [
                {
                    text: i18n(`MessageRequests--delete-${conversationType}`),
                    action: onDelete,
                    style: 'negative',
                },
            ] }, i18n(`MessageRequests--delete-${conversationType}-confirm-body`)));
    }
    return null;
};
exports.MessageRequestActionsConfirmation = MessageRequestActionsConfirmation;
