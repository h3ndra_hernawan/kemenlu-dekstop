"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageMetadata = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const ExpireTimer_1 = require("./ExpireTimer");
const Timestamp_1 = require("./Timestamp");
const Spinner_1 = require("../Spinner");
const MessageMetadata = props => {
    const { deletedForEveryone, direction, expirationLength, expirationTimestamp, hasText, i18n, id, isShowingImage, isSticker, isTapToViewExpired, showMessageDetail, status, textPending, timestamp, } = props;
    const withImageNoCaption = Boolean(!isSticker && !hasText && isShowingImage);
    const metadataDirection = isSticker ? undefined : direction;
    let timestampNode;
    {
        const isError = status === 'error' && direction === 'outgoing';
        const isPartiallySent = status === 'partial-sent' && direction === 'outgoing';
        const isPaused = status === 'paused';
        if (isError || isPartiallySent || isPaused) {
            let statusInfo;
            if (isError) {
                statusInfo = i18n('sendFailed');
            }
            else if (isPaused) {
                statusInfo = i18n('sendPaused');
            }
            else {
                statusInfo = (react_1.default.createElement("button", { type: "button", className: "module-message__metadata__tapable", onClick: (event) => {
                        event.stopPropagation();
                        event.preventDefault();
                        showMessageDetail(id);
                    } }, i18n('partiallySent')));
            }
            timestampNode = (react_1.default.createElement("span", { className: (0, classnames_1.default)({
                    'module-message__metadata__date': true,
                    'module-message__metadata__date--with-sticker': isSticker,
                    [`module-message__metadata__date--${direction}`]: !isSticker,
                    'module-message__metadata__date--with-image-no-caption': withImageNoCaption,
                }) }, statusInfo));
        }
        else {
            timestampNode = (react_1.default.createElement(Timestamp_1.Timestamp, { i18n: i18n, timestamp: timestamp, extended: true, direction: metadataDirection, withImageNoCaption: withImageNoCaption, withSticker: isSticker, withTapToViewExpired: isTapToViewExpired, module: "module-message__metadata__date" }));
        }
    }
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__metadata', `module-message__metadata--${direction}`, withImageNoCaption
            ? 'module-message__metadata--with-image-no-caption'
            : null) },
        timestampNode,
        expirationLength && expirationTimestamp ? (react_1.default.createElement(ExpireTimer_1.ExpireTimer, { direction: metadataDirection, expirationLength: expirationLength, expirationTimestamp: expirationTimestamp, withImageNoCaption: withImageNoCaption, withSticker: isSticker, withTapToViewExpired: isTapToViewExpired })) : null,
        textPending ? (react_1.default.createElement("div", { className: "module-message__metadata__spinner-container" },
            react_1.default.createElement(Spinner_1.Spinner, { svgSize: "small", size: "14px", direction: direction }))) : null,
        !deletedForEveryone &&
            !textPending &&
            direction === 'outgoing' &&
            status !== 'error' &&
            status !== 'partial-sent' ? (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-message__metadata__status-icon', `module-message__metadata__status-icon--${status}`, isSticker
                ? 'module-message__metadata__status-icon--with-sticker'
                : null, withImageNoCaption
                ? 'module-message__metadata__status-icon--with-image-no-caption'
                : null, isTapToViewExpired
                ? 'module-message__metadata__status-icon--with-tap-to-view-expired'
                : null) })) : null));
};
exports.MessageMetadata = MessageMetadata;
