"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const Linkify_1 = require("./Linkify");
const story = (0, react_1.storiesOf)('Components/Conversation/Linkify', module);
const createProps = (overrideProps = {}) => ({
    renderNonLink: overrideProps.renderNonLink,
    text: (0, addon_knobs_1.text)('text', overrideProps.text || ''),
});
story.add('Only Link', () => {
    const props = createProps({
        text: 'https://www.signal.org',
    });
    return React.createElement(Linkify_1.Linkify, Object.assign({}, props));
});
story.add('Links with Text', () => {
    const props = createProps({
        text: 'you should see this: https://www.signal.org - it is good. Also: https://placekitten.com!',
    });
    return React.createElement(Linkify_1.Linkify, Object.assign({}, props));
});
story.add('Links with Emoji without space', () => {
    const props = createProps({
        text: '👍https://www.signal.org😎',
    });
    return React.createElement(Linkify_1.Linkify, Object.assign({}, props));
});
story.add('Links with Emoji and Text', () => {
    const props = createProps({
        text: 'https://example.com ⚠️ 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ https://example.com',
    });
    return React.createElement(Linkify_1.Linkify, Object.assign({}, props));
});
story.add('No Link', () => {
    const props = createProps({
        text: 'I am fond of cats',
    });
    return React.createElement(Linkify_1.Linkify, Object.assign({}, props));
});
story.add('Blocked Protocols', () => {
    const props = createProps({
        text: 'smailto:someone@somewhere.com - ftp://something.com - //local/share - \\localshare',
    });
    return React.createElement(Linkify_1.Linkify, Object.assign({}, props));
});
story.add('Missing protocols', () => {
    const props = createProps({
        text: 'I love example.com. I also love кц.рф. I also love مثال.تونس. But I do not love test.example.',
    });
    return React.createElement(Linkify_1.Linkify, Object.assign({}, props));
});
story.add('Custom Text Render', () => {
    const props = createProps({
        text: 'you should see this: https://www.signal.org - it is good. Also: https://placekitten.com!',
        renderNonLink: ({ text: theText, key }) => (React.createElement("div", { key: key, style: { backgroundColor: 'aquamarine' } }, theText)),
    });
    return React.createElement(Linkify_1.Linkify, Object.assign({}, props));
});
