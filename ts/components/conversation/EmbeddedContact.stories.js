"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const EmbeddedContact_1 = require("./EmbeddedContact");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const EmbeddedContact_2 = require("../../types/EmbeddedContact");
const MIME_1 = require("../../types/MIME");
const fakeAttachment_1 = require("../../test-both/helpers/fakeAttachment");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/EmbeddedContact', module);
const createProps = (overrideProps = {}) => ({
    contact: overrideProps.contact || {},
    i18n,
    isIncoming: (0, addon_knobs_1.boolean)('isIncoming', overrideProps.isIncoming || false),
    onClick: (0, addon_actions_1.action)('onClick'),
    tabIndex: (0, addon_knobs_1.number)('tabIndex', overrideProps.tabIndex || 0),
    withContentAbove: (0, addon_knobs_1.boolean)('withContentAbove', overrideProps.withContentAbove || false),
    withContentBelow: (0, addon_knobs_1.boolean)('withContentBelow', overrideProps.withContentBelow || false),
});
const fullContact = {
    avatar: {
        avatar: (0, fakeAttachment_1.fakeAttachment)({
            path: '/fixtures/giphy-GVNvOUpeYmI7e.gif',
            contentType: MIME_1.IMAGE_GIF,
        }),
        isProfile: true,
    },
    email: [
        {
            value: 'jerjor@fakemail.com',
            type: EmbeddedContact_2.ContactFormType.HOME,
        },
    ],
    name: {
        givenName: 'Jerry',
        familyName: 'Jordan',
        prefix: 'Dr.',
        suffix: 'Jr.',
        middleName: 'James',
        displayName: 'Jerry Jordan',
    },
    number: [
        {
            value: '555-444-2323',
            type: EmbeddedContact_2.ContactFormType.HOME,
        },
    ],
};
story.add('Full Contact', () => {
    const props = createProps({
        contact: fullContact,
    });
    return React.createElement(EmbeddedContact_1.EmbeddedContact, Object.assign({}, props));
});
story.add('Only Email', () => {
    const props = createProps({
        contact: {
            email: fullContact.email,
        },
    });
    return React.createElement(EmbeddedContact_1.EmbeddedContact, Object.assign({}, props));
});
story.add('Given Name', () => {
    const props = createProps({
        contact: {
            name: {
                givenName: 'Jerry',
            },
        },
    });
    return React.createElement(EmbeddedContact_1.EmbeddedContact, Object.assign({}, props));
});
story.add('Organization', () => {
    const props = createProps({
        contact: {
            organization: 'Company 5',
        },
    });
    return React.createElement(EmbeddedContact_1.EmbeddedContact, Object.assign({}, props));
});
story.add('Given + Family Name', () => {
    const props = createProps({
        contact: {
            name: {
                givenName: 'Jerry',
                familyName: 'FamilyName',
            },
        },
    });
    return React.createElement(EmbeddedContact_1.EmbeddedContact, Object.assign({}, props));
});
story.add('Family Name', () => {
    const props = createProps({
        contact: {
            name: {
                familyName: 'FamilyName',
            },
        },
    });
    return React.createElement(EmbeddedContact_1.EmbeddedContact, Object.assign({}, props));
});
story.add('Loading Avatar', () => {
    const props = createProps({
        contact: {
            name: {
                displayName: 'Jerry Jordan',
            },
            avatar: {
                avatar: (0, fakeAttachment_1.fakeAttachment)({
                    pending: true,
                    contentType: MIME_1.IMAGE_GIF,
                }),
                isProfile: true,
            },
        },
    });
    return React.createElement(EmbeddedContact_1.EmbeddedContact, Object.assign({}, props));
});
story.add('Incoming', () => {
    const props = createProps({
        contact: {
            name: fullContact.name,
        },
        isIncoming: true,
    });
    // Wrapped in a <div> to provide a background for light color of text
    return (React.createElement("div", { style: { backgroundColor: 'darkgreen' } },
        React.createElement(EmbeddedContact_1.EmbeddedContact, Object.assign({}, props))));
});
story.add('Content Above and Below', () => {
    const props = createProps({
        withContentAbove: true,
        withContentBelow: true,
    });
    return (React.createElement(React.Fragment, null,
        React.createElement("div", null, "Content Above"),
        React.createElement(EmbeddedContact_1.EmbeddedContact, Object.assign({}, props)),
        React.createElement("div", null, "Content Below")));
});
