"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemoveGroupMemberConfirmationDialog = void 0;
const react_1 = __importDefault(require("react"));
const ConfirmationDialog_1 = require("../ConfirmationDialog");
const Intl_1 = require("../Intl");
const ContactName_1 = require("./ContactName");
const RemoveGroupMemberConfirmationDialog = ({ conversation, i18n, onClose, onRemove }) => (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
        {
            action: onRemove,
            text: i18n('RemoveGroupMemberConfirmation__remove-button'),
            style: 'negative',
        },
    ], i18n: i18n, onClose: onClose, title: react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "RemoveGroupMemberConfirmation__description", components: {
            name: react_1.default.createElement(ContactName_1.ContactName, { title: conversation.title }),
        } }) }));
exports.RemoveGroupMemberConfirmationDialog = RemoveGroupMemberConfirmationDialog;
