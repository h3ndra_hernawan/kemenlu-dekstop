"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorBoundary = void 0;
const react_1 = __importDefault(require("react"));
const Errors = __importStar(require("../../types/errors"));
const log = __importStar(require("../../logging/log"));
const CSS_MODULE = 'module-error-boundary-notification';
class ErrorBoundary extends react_1.default.PureComponent {
    constructor(props) {
        super(props);
        this.state = { error: undefined };
    }
    static getDerivedStateFromError(error) {
        log.error('ErrorBoundary: captured rendering error', Errors.toLogFormat(error));
        return { error };
    }
    render() {
        const { error } = this.state;
        const { i18n, children } = this.props;
        if (!error) {
            return children;
        }
        return (react_1.default.createElement("div", { className: CSS_MODULE, onClick: this.onClick.bind(this), onKeyDown: this.onKeyDown.bind(this), role: "button", tabIndex: 0 },
            react_1.default.createElement("div", { className: `${CSS_MODULE}__icon-container` },
                react_1.default.createElement("div", { className: `${CSS_MODULE}__icon` })),
            react_1.default.createElement("div", { className: `${CSS_MODULE}__message` }, i18n('ErrorBoundaryNotification__text'))));
    }
    onClick(event) {
        event.stopPropagation();
        event.preventDefault();
        this.onAction();
    }
    onKeyDown(event) {
        if (event.key !== 'Enter' && event.key !== ' ') {
            return;
        }
        event.stopPropagation();
        event.preventDefault();
        this.onAction();
    }
    onAction() {
        const { showDebugLog } = this.props;
        showDebugLog();
    }
}
exports.ErrorBoundary = ErrorBoundary;
