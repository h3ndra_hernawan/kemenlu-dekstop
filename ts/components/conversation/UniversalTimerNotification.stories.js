"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const UniversalTimerNotification_1 = require("./UniversalTimerNotification");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const expireTimers_1 = require("../../test-both/util/expireTimers");
const story = (0, react_2.storiesOf)('Components/UniversalTimerNotification', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
expireTimers_1.EXPIRE_TIMERS.forEach(({ value: ms, label }) => {
    story.add(`Initial value: ${label}`, () => {
        return react_1.default.createElement(UniversalTimerNotification_1.UniversalTimerNotification, { i18n: i18n, expireTimer: ms / 1000 });
    });
});
