"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const react_1 = require("@storybook/react");
const ReactionViewer_1 = require("./ReactionViewer");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ReactionViewer', module);
const createProps = (overrideProps = {}) => ({
    i18n,
    onClose: (0, addon_actions_1.action)('onClose'),
    pickedReaction: overrideProps.pickedReaction,
    reactions: overrideProps.reactions || [],
    style: overrideProps.style,
});
story.add('All Reactions', () => {
    const props = createProps({
        reactions: [
            {
                emoji: '❤️',
                timestamp: 1,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552671',
                    phoneNumber: '+14155552671',
                    profileName: 'Ameila Briggs',
                    title: 'Amelia',
                }),
            },
            {
                emoji: '❤️',
                timestamp: 2,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552672',
                    name: 'Adam Burrel',
                    title: 'Adam',
                }),
            },
            {
                emoji: '❤️',
                timestamp: 3,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552673',
                    name: 'Rick Owens',
                    title: 'Rick',
                }),
            },
            {
                emoji: '❤️',
                timestamp: 4,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552674',
                    name: 'Bojack Horseman',
                    title: 'Bojack',
                }),
            },
            {
                emoji: '👍',
                timestamp: 9,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552678',
                    phoneNumber: '+14155552678',
                    profileName: 'Adam Burrel',
                    title: 'Adam',
                }),
            },
            {
                emoji: '👎',
                timestamp: 10,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552673',
                    name: 'Rick Owens',
                    title: 'Rick',
                }),
            },
            {
                emoji: '😂',
                timestamp: 11,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552674',
                    name: 'Bojack Horseman',
                    title: 'Bojack',
                }),
            },
            {
                emoji: '😮',
                timestamp: 12,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552675',
                    name: 'Cayce Pollard',
                    title: 'Cayce',
                }),
            },
            {
                emoji: '😢',
                timestamp: 13,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552676',
                    name: 'Foo McBarrington',
                    title: 'Foo',
                }),
            },
            {
                emoji: '😡',
                timestamp: 14,
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552676',
                    name: 'Foo McBarrington',
                    title: 'Foo',
                }),
            },
        ],
    });
    return React.createElement(ReactionViewer_1.ReactionViewer, Object.assign({}, props));
});
story.add('Picked Reaction', () => {
    const props = createProps({
        pickedReaction: '❤️',
        reactions: [
            {
                emoji: '❤️',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552671',
                    name: 'Amelia Briggs',
                    isMe: true,
                    title: 'Amelia',
                }),
                timestamp: Date.now(),
            },
            {
                emoji: '👍',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552671',
                    phoneNumber: '+14155552671',
                    profileName: 'Joel Ferrari',
                    title: 'Joel',
                }),
                timestamp: Date.now(),
            },
        ],
    });
    return React.createElement(ReactionViewer_1.ReactionViewer, Object.assign({}, props));
});
story.add('Picked Missing Reaction', () => {
    const props = createProps({
        pickedReaction: '😡',
        reactions: [
            {
                emoji: '❤️',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552671',
                    name: 'Amelia Briggs',
                    isMe: true,
                    title: 'Amelia',
                }),
                timestamp: Date.now(),
            },
            {
                emoji: '👍',
                from: (0, getDefaultConversation_1.getDefaultConversation)({
                    id: '+14155552671',
                    phoneNumber: '+14155552671',
                    profileName: 'Joel Ferrari',
                    title: 'Joel',
                }),
                timestamp: Date.now(),
            },
        ],
    });
    return React.createElement(ReactionViewer_1.ReactionViewer, Object.assign({}, props));
});
const skinTones = [
    '\u{1F3FB}',
    '\u{1F3FC}',
    '\u{1F3FD}',
    '\u{1F3FE}',
    '\u{1F3FF}',
];
const thumbsUpHands = skinTones.map(skinTone => `👍${skinTone}`);
const okHands = skinTones.map(skinTone => `👌${skinTone}`).reverse();
const createReaction = (emoji, name, timestamp = Date.now()) => ({
    emoji,
    from: (0, getDefaultConversation_1.getDefaultConversation)({
        id: '+14155552671',
        name,
        title: name,
    }),
    timestamp,
});
story.add('Reaction Skin Tones', () => {
    const props = createProps({
        pickedReaction: '😡',
        reactions: [
            ...thumbsUpHands.map((emoji, n) => createReaction(emoji, `Thumbs Up ${n + 1}`, Date.now() + n * 1000)),
            ...okHands.map((emoji, n) => createReaction(emoji, `Ok Hand ${n + 1}`, Date.now() + n * 1000)),
        ],
    });
    return React.createElement(ReactionViewer_1.ReactionViewer, Object.assign({}, props));
});
