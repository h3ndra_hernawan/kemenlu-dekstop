"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const TypingBubble_1 = require("./TypingBubble");
const Colors_1 = require("../../types/Colors");
const getFakeBadge_1 = require("../../test-both/helpers/getFakeBadge");
const Util_1 = require("../../types/Util");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/TypingBubble', module);
const createProps = (overrideProps = {}) => ({
    acceptedMessageRequest: true,
    badge: overrideProps.badge,
    isMe: false,
    i18n,
    color: (0, addon_knobs_1.select)('color', Colors_1.AvatarColors.reduce((m, c) => (Object.assign(Object.assign({}, m), { [c]: c })), {}), overrideProps.color || Colors_1.AvatarColors[0]),
    avatarPath: (0, addon_knobs_1.text)('avatarPath', overrideProps.avatarPath || ''),
    title: '',
    profileName: (0, addon_knobs_1.text)('profileName', overrideProps.profileName || ''),
    conversationType: (0, addon_knobs_1.select)('conversationType', { group: 'group', direct: 'direct' }, overrideProps.conversationType || 'direct'),
    sharedGroupNames: [],
    theme: Util_1.ThemeType.light,
});
story.add('Direct', () => {
    const props = createProps();
    return React.createElement(TypingBubble_1.TypingBubble, Object.assign({}, props));
});
story.add('Group', () => {
    const props = createProps({ conversationType: 'group' });
    return React.createElement(TypingBubble_1.TypingBubble, Object.assign({}, props));
});
story.add('Group (with badge)', () => {
    const props = createProps({
        badge: (0, getFakeBadge_1.getFakeBadge)(),
        conversationType: 'group',
    });
    return React.createElement(TypingBubble_1.TypingBubble, Object.assign({}, props));
});
