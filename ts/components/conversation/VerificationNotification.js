"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerificationNotification = void 0;
const react_1 = __importDefault(require("react"));
const SystemMessage_1 = require("./SystemMessage");
const ContactName_1 = require("./ContactName");
const Intl_1 = require("../Intl");
const missingCaseError_1 = require("../../util/missingCaseError");
class VerificationNotification extends react_1.default.Component {
    getStringId() {
        const { isLocal, type } = this.props;
        switch (type) {
            case 'markVerified':
                return isLocal
                    ? 'youMarkedAsVerified'
                    : 'youMarkedAsVerifiedOtherDevice';
            case 'markNotVerified':
                return isLocal
                    ? 'youMarkedAsNotVerified'
                    : 'youMarkedAsNotVerifiedOtherDevice';
            default:
                throw (0, missingCaseError_1.missingCaseError)(type);
        }
    }
    renderContents() {
        const { contact, i18n } = this.props;
        const id = this.getStringId();
        return (react_1.default.createElement(Intl_1.Intl, { id: id, components: [
                react_1.default.createElement(ContactName_1.ContactName, { key: "external-1", title: contact.title, module: "module-verification-notification__contact" }),
            ], i18n: i18n }));
    }
    render() {
        const { type } = this.props;
        const icon = type === 'markVerified' ? 'verified' : 'verified-not';
        return react_1.default.createElement(SystemMessage_1.SystemMessage, { icon: icon, contents: this.renderContents() });
    }
}
exports.VerificationNotification = VerificationNotification;
