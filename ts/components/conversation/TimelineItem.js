"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimelineItem = void 0;
const react_1 = __importDefault(require("react"));
const lodash_1 = require("lodash");
const Message_1 = require("./Message");
const CallingNotification_1 = require("./CallingNotification");
const ChatSessionRefreshedNotification_1 = require("./ChatSessionRefreshedNotification");
const DeliveryIssueNotification_1 = require("./DeliveryIssueNotification");
const LinkNotification_1 = require("./LinkNotification");
const ChangeNumberNotification_1 = require("./ChangeNumberNotification");
const InlineNotificationWrapper_1 = require("./InlineNotificationWrapper");
const UnsupportedMessage_1 = require("./UnsupportedMessage");
const TimerNotification_1 = require("./TimerNotification");
const SafetyNumberNotification_1 = require("./SafetyNumberNotification");
const VerificationNotification_1 = require("./VerificationNotification");
const GroupNotification_1 = require("./GroupNotification");
const GroupV2Change_1 = require("./GroupV2Change");
const GroupV1Migration_1 = require("./GroupV1Migration");
const ResetSessionNotification_1 = require("./ResetSessionNotification");
const ProfileChangeNotification_1 = require("./ProfileChangeNotification");
const log = __importStar(require("../../logging/log"));
class TimelineItem extends react_1.default.PureComponent {
    render() {
        const { containerElementRef, conversationId, id, isSelected, item, i18n, theme, messageSizeChanged, nextItem, renderContact, renderUniversalTimerNotification, returnToActiveCall, selectMessage, startCallingLobby, } = this.props;
        if (!item) {
            log.warn(`TimelineItem: item ${id} provided was falsey`);
            return null;
        }
        if (item.type === 'message') {
            return (react_1.default.createElement(Message_1.Message, Object.assign({}, (0, lodash_1.omit)(this.props, ['item']), item.data, { containerElementRef: containerElementRef, i18n: i18n, theme: theme, renderingContext: "conversation/TimelineItem" })));
        }
        let notification;
        if (item.type === 'unsupportedMessage') {
            notification = (react_1.default.createElement(UnsupportedMessage_1.UnsupportedMessage, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else if (item.type === 'callHistory') {
            notification = (react_1.default.createElement(CallingNotification_1.CallingNotification, Object.assign({ conversationId: conversationId, i18n: i18n, messageId: id, messageSizeChanged: messageSizeChanged, nextItem: nextItem, returnToActiveCall: returnToActiveCall, startCallingLobby: startCallingLobby }, item.data)));
        }
        else if (item.type === 'chatSessionRefreshed') {
            notification = (react_1.default.createElement(ChatSessionRefreshedNotification_1.ChatSessionRefreshedNotification, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else if (item.type === 'deliveryIssue') {
            notification = (react_1.default.createElement(DeliveryIssueNotification_1.DeliveryIssueNotification, Object.assign({}, item.data, this.props, { i18n: i18n })));
        }
        else if (item.type === 'linkNotification') {
            notification = react_1.default.createElement(LinkNotification_1.LinkNotification, { i18n: i18n });
        }
        else if (item.type === 'timerNotification') {
            notification = (react_1.default.createElement(TimerNotification_1.TimerNotification, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else if (item.type === 'universalTimerNotification') {
            notification = renderUniversalTimerNotification();
        }
        else if (item.type === 'changeNumberNotification') {
            notification = (react_1.default.createElement(ChangeNumberNotification_1.ChangeNumberNotification, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else if (item.type === 'safetyNumberNotification') {
            notification = (react_1.default.createElement(SafetyNumberNotification_1.SafetyNumberNotification, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else if (item.type === 'verificationNotification') {
            notification = (react_1.default.createElement(VerificationNotification_1.VerificationNotification, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else if (item.type === 'groupNotification') {
            notification = (react_1.default.createElement(GroupNotification_1.GroupNotification, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else if (item.type === 'groupV2Change') {
            notification = (react_1.default.createElement(GroupV2Change_1.GroupV2Change, Object.assign({ renderContact: renderContact }, item.data, { i18n: i18n })));
        }
        else if (item.type === 'groupV1Migration') {
            notification = (react_1.default.createElement(GroupV1Migration_1.GroupV1Migration, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else if (item.type === 'resetSessionNotification') {
            notification = (react_1.default.createElement(ResetSessionNotification_1.ResetSessionNotification, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else if (item.type === 'profileChange') {
            notification = (react_1.default.createElement(ProfileChangeNotification_1.ProfileChangeNotification, Object.assign({}, this.props, item.data, { i18n: i18n })));
        }
        else {
            // Weird, yes, but the idea is to get a compile error when we aren't comprehensive
            //   with our if/else checks above, but also log out the type we don't understand if
            //   we encounter it at runtime.
            const unknownItem = item;
            const asItem = unknownItem;
            throw new Error(`TimelineItem: Unknown type: ${asItem.type}`);
        }
        return (react_1.default.createElement(InlineNotificationWrapper_1.InlineNotificationWrapper, { id: id, conversationId: conversationId, isSelected: isSelected, selectMessage: selectMessage }, notification));
    }
}
exports.TimelineItem = TimelineItem;
