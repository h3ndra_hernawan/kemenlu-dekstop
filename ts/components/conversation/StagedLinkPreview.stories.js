"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const MIME_1 = require("../../types/MIME");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const StagedLinkPreview_1 = require("./StagedLinkPreview");
const LONG_TITLE = "This is a super-sweet site. And it's got some really amazing content in store for you if you just click that link. Can you click that link for me?";
const LONG_DESCRIPTION = "You're gonna love this description. Not only does it have a lot of characters, but it will also be truncated in the UI. How cool is that??";
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/StagedLinkPreview', module);
// eslint-disable-next-line @typescript-eslint/no-explicit-any
story.addDecorator(addon_knobs_1.withKnobs({ escapeHTML: false }));
const createAttachment = (props = {}) => ({
    contentType: (0, MIME_1.stringToMIMEType)((0, addon_knobs_1.text)('attachment contentType', props.contentType || '')),
    fileName: (0, addon_knobs_1.text)('attachment fileName', props.fileName || ''),
    url: (0, addon_knobs_1.text)('attachment url', props.url || ''),
    size: 24325,
});
const createProps = (overrideProps = {}) => ({
    title: (0, addon_knobs_1.text)('title', typeof overrideProps.title === 'string'
        ? overrideProps.title
        : 'This is a super-sweet site'),
    description: (0, addon_knobs_1.text)('description', typeof overrideProps.description === 'string'
        ? overrideProps.description
        : 'This is a description'),
    date: (0, addon_knobs_1.date)('date', new Date(overrideProps.date || 0)),
    domain: (0, addon_knobs_1.text)('domain', overrideProps.domain || 'signal.org'),
    image: overrideProps.image,
    i18n,
    onClose: (0, addon_actions_1.action)('onClose'),
});
story.add('Loading', () => {
    const props = createProps({ domain: '' });
    return React.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, props));
});
story.add('No Image', () => {
    return React.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, createProps()));
});
story.add('Image', () => {
    const props = createProps({
        image: createAttachment({
            url: '/fixtures/kitten-4-112-112.jpg',
            contentType: (0, MIME_1.stringToMIMEType)('image/jpeg'),
        }),
    });
    return React.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, props));
});
story.add('Image, No Title Or Description', () => {
    const props = createProps({
        title: '',
        description: '',
        domain: 'instagram.com',
        image: createAttachment({
            url: '/fixtures/kitten-4-112-112.jpg',
            contentType: (0, MIME_1.stringToMIMEType)('image/jpeg'),
        }),
    });
    return React.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, props));
});
story.add('No Image, Long Title With Description', () => {
    const props = createProps({
        title: LONG_TITLE,
    });
    return React.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, props));
});
story.add('No Image, Long Title Without Description', () => {
    const props = createProps({
        title: LONG_TITLE,
        description: '',
    });
    return React.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, props));
});
story.add('Image, Long Title Without Description', () => {
    const props = createProps({
        title: LONG_TITLE,
        image: createAttachment({
            url: '/fixtures/kitten-4-112-112.jpg',
            contentType: (0, MIME_1.stringToMIMEType)('image/jpeg'),
        }),
    });
    return React.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, props));
});
story.add('Image, Long Title And Description', () => {
    const props = createProps({
        title: LONG_TITLE,
        description: LONG_DESCRIPTION,
        image: createAttachment({
            url: '/fixtures/kitten-4-112-112.jpg',
            contentType: (0, MIME_1.stringToMIMEType)('image/jpeg'),
        }),
    });
    return React.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, props));
});
story.add('Everything: image, title, description, and date', () => {
    const props = createProps({
        title: LONG_TITLE,
        description: LONG_DESCRIPTION,
        date: Date.now(),
        image: createAttachment({
            url: '/fixtures/kitten-4-112-112.jpg',
            contentType: (0, MIME_1.stringToMIMEType)('image/jpeg'),
        }),
    });
    return React.createElement(StagedLinkPreview_1.StagedLinkPreview, Object.assign({}, props));
});
