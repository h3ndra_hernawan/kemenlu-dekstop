"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const ConversationHero_1 = require("./ConversationHero");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const StorybookThemeContext_1 = require("../../../.storybook/StorybookThemeContext");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const getAbout = () => (0, addon_knobs_1.text)('about', '👍 Free to chat');
const getTitle = () => (0, addon_knobs_1.text)('name', 'Cayce Bollard');
const getName = () => (0, addon_knobs_1.text)('name', 'Cayce Bollard');
const getProfileName = () => (0, addon_knobs_1.text)('profileName', 'Cayce Bollard (profile)');
const getAvatarPath = () => (0, addon_knobs_1.text)('avatarPath', '/fixtures/kitten-4-112-112.jpg');
const getPhoneNumber = () => (0, addon_knobs_1.text)('phoneNumber', '+1 (646) 327-2700');
const updateSharedGroups = (0, addon_actions_1.action)('updateSharedGroups');
const Wrapper = (props) => {
    const theme = React.useContext(StorybookThemeContext_1.StorybookThemeContext);
    return React.createElement(ConversationHero_1.ConversationHero, Object.assign({}, props, { theme: theme }));
};
(0, react_1.storiesOf)('Components/Conversation/ConversationHero', module)
    .add('Direct (Five Other Groups)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { about: getAbout(), acceptedMessageRequest: true, i18n: i18n, isMe: false, title: getTitle(), avatarPath: getAvatarPath(), name: getName(), profileName: getProfileName(), phoneNumber: getPhoneNumber(), conversationType: "direct", updateSharedGroups: updateSharedGroups, sharedGroupNames: [
                'NYC Rock Climbers',
                'Dinner Party',
                'Friends 🌿',
                'Fourth',
                'Fifth',
            ], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Direct (Four Other Groups)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { about: getAbout(), acceptedMessageRequest: true, i18n: i18n, isMe: false, title: getTitle(), avatarPath: getAvatarPath(), name: getName(), profileName: getProfileName(), phoneNumber: getPhoneNumber(), conversationType: "direct", updateSharedGroups: updateSharedGroups, sharedGroupNames: [
                'NYC Rock Climbers',
                'Dinner Party',
                'Friends 🌿',
                'Fourth',
            ], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Direct (Three Other Groups)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { about: getAbout(), acceptedMessageRequest: true, i18n: i18n, isMe: false, title: getTitle(), avatarPath: getAvatarPath(), name: getName(), profileName: getProfileName(), phoneNumber: getPhoneNumber(), conversationType: "direct", updateSharedGroups: updateSharedGroups, sharedGroupNames: ['NYC Rock Climbers', 'Dinner Party', 'Friends 🌿'], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Direct (Two Other Groups)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { about: getAbout(), acceptedMessageRequest: true, i18n: i18n, isMe: false, title: getTitle(), avatarPath: getAvatarPath(), name: getName(), profileName: getProfileName(), phoneNumber: getPhoneNumber(), conversationType: "direct", updateSharedGroups: updateSharedGroups, sharedGroupNames: ['NYC Rock Climbers', 'Dinner Party'], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Direct (One Other Group)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { about: getAbout(), acceptedMessageRequest: true, i18n: i18n, isMe: false, title: getTitle(), avatarPath: getAvatarPath(), name: getName(), profileName: getProfileName(), phoneNumber: getPhoneNumber(), conversationType: "direct", updateSharedGroups: updateSharedGroups, sharedGroupNames: ['NYC Rock Climbers'], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Direct (No Groups, Name)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { about: getAbout(), acceptedMessageRequest: true, i18n: i18n, isMe: false, title: getTitle(), avatarPath: getAvatarPath(), name: getName(), profileName: (0, addon_knobs_1.text)('profileName', ''), phoneNumber: getPhoneNumber(), conversationType: "direct", updateSharedGroups: updateSharedGroups, sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Direct (No Groups, Just Profile)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { about: getAbout(), acceptedMessageRequest: true, i18n: i18n, isMe: false, title: (0, addon_knobs_1.text)('title', 'Cayce Bollard (profile)'), avatarPath: getAvatarPath(), name: (0, addon_knobs_1.text)('name', ''), profileName: getProfileName(), phoneNumber: getPhoneNumber(), conversationType: "direct", updateSharedGroups: updateSharedGroups, sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Direct (No Groups, Just Phone Number)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { about: getAbout(), acceptedMessageRequest: true, i18n: i18n, isMe: false, title: (0, addon_knobs_1.text)('title', '+1 (646) 327-2700'), avatarPath: getAvatarPath(), name: (0, addon_knobs_1.text)('name', ''), profileName: (0, addon_knobs_1.text)('profileName', ''), phoneNumber: getPhoneNumber(), conversationType: "direct", updateSharedGroups: updateSharedGroups, sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Direct (No Groups, No Data)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { i18n: i18n, isMe: false, title: (0, addon_knobs_1.text)('title', 'Unknown contact'), acceptedMessageRequest: true, avatarPath: getAvatarPath(), name: (0, addon_knobs_1.text)('name', ''), profileName: (0, addon_knobs_1.text)('profileName', ''), phoneNumber: (0, addon_knobs_1.text)('phoneNumber', ''), conversationType: "direct", sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), updateSharedGroups: updateSharedGroups, onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Direct (No Groups, No Data, Not Accepted)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { i18n: i18n, isMe: false, title: (0, addon_knobs_1.text)('title', 'Unknown contact'), acceptedMessageRequest: false, avatarPath: getAvatarPath(), name: (0, addon_knobs_1.text)('name', ''), profileName: (0, addon_knobs_1.text)('profileName', ''), phoneNumber: (0, addon_knobs_1.text)('phoneNumber', ''), conversationType: "direct", sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), updateSharedGroups: updateSharedGroups, onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Group (many members)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { acceptedMessageRequest: true, i18n: i18n, isMe: false, title: (0, addon_knobs_1.text)('title', 'NYC Rock Climbers'), name: (0, addon_knobs_1.text)('groupName', 'NYC Rock Climbers'), conversationType: "group", membersCount: (0, addon_knobs_1.number)('membersCount', 22), sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), updateSharedGroups: updateSharedGroups, onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Group (one member)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { acceptedMessageRequest: true, i18n: i18n, isMe: false, title: (0, addon_knobs_1.text)('title', 'NYC Rock Climbers'), name: (0, addon_knobs_1.text)('groupName', 'NYC Rock Climbers'), conversationType: "group", membersCount: 1, sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), updateSharedGroups: updateSharedGroups, onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Group (zero members)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { acceptedMessageRequest: true, i18n: i18n, isMe: false, title: (0, addon_knobs_1.text)('title', 'NYC Rock Climbers'), name: (0, addon_knobs_1.text)('groupName', 'NYC Rock Climbers'), conversationType: "group", groupDescription: "This is a group for all the rock climbers of NYC", membersCount: 0, sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), updateSharedGroups: updateSharedGroups, onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Group (long group description)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { acceptedMessageRequest: true, i18n: i18n, isMe: false, title: (0, addon_knobs_1.text)('title', 'NYC Rock Climbers'), name: (0, addon_knobs_1.text)('groupName', 'NYC Rock Climbers'), conversationType: "group", groupDescription: "This is a group for all the rock climbers of NYC. We really like to climb rocks and these NYC people climb any rock. No rock is too small or too big to be climbed. We will ascend upon all rocks, and not just in NYC, in the whole world. We are just getting started, NYC is just the beginning, watch out rocks in the galaxy. Kuiper belt I'm looking at you. We will put on a space suit and climb all your rocks. No rock is near nor far for the rock climbers of NYC.", membersCount: 0, sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), updateSharedGroups: updateSharedGroups, onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Group (No name)', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { acceptedMessageRequest: true, i18n: i18n, isMe: false, title: (0, addon_knobs_1.text)('title', 'Unknown group'), name: (0, addon_knobs_1.text)('groupName', ''), conversationType: "group", membersCount: 0, sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), updateSharedGroups: updateSharedGroups, onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
})
    .add('Note to Self', () => {
    return (React.createElement("div", { style: { width: '480px' } },
        React.createElement(Wrapper, { acceptedMessageRequest: true, i18n: i18n, isMe: true, title: getTitle(), conversationType: "direct", phoneNumber: getPhoneNumber(), sharedGroupNames: [], unblurAvatar: (0, addon_actions_1.action)('unblurAvatar'), updateSharedGroups: updateSharedGroups, onHeightChange: (0, addon_actions_1.action)('onHeightChange') })));
});
