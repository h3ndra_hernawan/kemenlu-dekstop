"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SafetyNumberNotification = void 0;
const react_1 = __importDefault(require("react"));
const Button_1 = require("../Button");
const SystemMessage_1 = require("./SystemMessage");
const ContactName_1 = require("./ContactName");
const Intl_1 = require("../Intl");
const SafetyNumberNotification = ({ contact, isGroup, i18n, showIdentity, }) => {
    const changeKey = isGroup
        ? 'safetyNumberChangedGroup'
        : 'safetyNumberChanged';
    return (react_1.default.createElement(SystemMessage_1.SystemMessage, { icon: "safety-number", contents: react_1.default.createElement(Intl_1.Intl, { id: changeKey, components: [
                react_1.default.createElement("span", { key: "external-1", className: "module-safety-number-notification__contact" },
                    react_1.default.createElement(ContactName_1.ContactName, { title: contact.title, module: "module-safety-number-notification__contact" })),
            ], i18n: i18n }), button: react_1.default.createElement(Button_1.Button, { onClick: () => {
                showIdentity(contact.id);
            }, size: Button_1.ButtonSize.Small, variant: Button_1.ButtonVariant.SystemMessage }, i18n('verifyNewNumber')) }));
};
exports.SafetyNumberNotification = SafetyNumberNotification;
