"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImageGrid = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Attachment_1 = require("../../types/Attachment");
const Image_1 = require("./Image");
const GAP = 1;
const ImageGrid = ({ attachments, bottomOverlay, i18n, isSticker, stickerSize, onError, onClick, tabIndex, theme, withContentAbove, withContentBelow, }) => {
    const curveTopLeft = !withContentAbove;
    const curveTopRight = curveTopLeft;
    const curveBottom = !withContentBelow;
    const curveBottomLeft = curveBottom;
    const curveBottomRight = curveBottom;
    const withBottomOverlay = Boolean(bottomOverlay && curveBottom);
    if (!attachments || !attachments.length) {
        return null;
    }
    if (attachments.length === 1 || !(0, Attachment_1.areAllAttachmentsVisual)(attachments)) {
        const { height, width } = (0, Attachment_1.getImageDimensions)(attachments[0], isSticker ? stickerSize : undefined);
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-image-grid', 'module-image-grid--one-image', isSticker ? 'module-image-grid--with-sticker' : null) },
            react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[0], i18n), i18n: i18n, theme: theme, blurHash: attachments[0].blurHash, bottomOverlay: withBottomOverlay, noBorder: isSticker, noBackground: isSticker, curveTopLeft: curveTopLeft, curveTopRight: curveTopRight, curveBottomLeft: curveBottomLeft, curveBottomRight: curveBottomRight, attachment: attachments[0], playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[0]), height: height, width: width, url: (0, Attachment_1.getUrl)(attachments[0]), tabIndex: tabIndex, onClick: onClick, onError: onError })));
    }
    if (attachments.length === 2) {
        return (react_1.default.createElement("div", { className: "module-image-grid" },
            react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[0], i18n), i18n: i18n, theme: theme, attachment: attachments[0], blurHash: attachments[0].blurHash, bottomOverlay: withBottomOverlay, noBorder: false, curveTopLeft: curveTopLeft, curveBottomLeft: curveBottomLeft, playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[0]), height: 150, width: 150, cropWidth: GAP, url: (0, Attachment_1.getThumbnailUrl)(attachments[0]), onClick: onClick, onError: onError }),
            react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[1], i18n), i18n: i18n, theme: theme, blurHash: attachments[1].blurHash, bottomOverlay: withBottomOverlay, noBorder: false, curveTopRight: curveTopRight, curveBottomRight: curveBottomRight, playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[1]), height: 150, width: 150, attachment: attachments[1], url: (0, Attachment_1.getThumbnailUrl)(attachments[1]), onClick: onClick, onError: onError })));
    }
    if (attachments.length === 3) {
        return (react_1.default.createElement("div", { className: "module-image-grid" },
            react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[0], i18n), i18n: i18n, theme: theme, blurHash: attachments[0].blurHash, bottomOverlay: withBottomOverlay, noBorder: false, curveTopLeft: curveTopLeft, curveBottomLeft: curveBottomLeft, attachment: attachments[0], playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[0]), height: 200, width: 200, cropWidth: GAP, url: (0, Attachment_1.getUrl)(attachments[0]), onClick: onClick, onError: onError }),
            react_1.default.createElement("div", { className: "module-image-grid__column" },
                react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[1], i18n), i18n: i18n, theme: theme, blurHash: attachments[1].blurHash, curveTopRight: curveTopRight, height: 100, width: 100, cropHeight: GAP, attachment: attachments[1], playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[1]), url: (0, Attachment_1.getThumbnailUrl)(attachments[1]), onClick: onClick, onError: onError }),
                react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[2], i18n), i18n: i18n, theme: theme, blurHash: attachments[2].blurHash, bottomOverlay: withBottomOverlay, noBorder: false, curveBottomRight: curveBottomRight, height: 100, width: 100, attachment: attachments[2], playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[2]), url: (0, Attachment_1.getThumbnailUrl)(attachments[2]), onClick: onClick, onError: onError }))));
    }
    if (attachments.length === 4) {
        return (react_1.default.createElement("div", { className: "module-image-grid" },
            react_1.default.createElement("div", { className: "module-image-grid__column" },
                react_1.default.createElement("div", { className: "module-image-grid__row" },
                    react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[0], i18n), i18n: i18n, theme: theme, blurHash: attachments[0].blurHash, curveTopLeft: curveTopLeft, noBorder: false, attachment: attachments[0], playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[0]), height: 150, width: 150, cropHeight: GAP, cropWidth: GAP, url: (0, Attachment_1.getThumbnailUrl)(attachments[0]), onClick: onClick, onError: onError }),
                    react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[1], i18n), i18n: i18n, theme: theme, blurHash: attachments[1].blurHash, curveTopRight: curveTopRight, playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[1]), noBorder: false, height: 150, width: 150, cropHeight: GAP, attachment: attachments[1], url: (0, Attachment_1.getThumbnailUrl)(attachments[1]), onClick: onClick, onError: onError })),
                react_1.default.createElement("div", { className: "module-image-grid__row" },
                    react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[2], i18n), i18n: i18n, theme: theme, blurHash: attachments[2].blurHash, bottomOverlay: withBottomOverlay, noBorder: false, curveBottomLeft: curveBottomLeft, playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[2]), height: 150, width: 150, cropWidth: GAP, attachment: attachments[2], url: (0, Attachment_1.getThumbnailUrl)(attachments[2]), onClick: onClick, onError: onError }),
                    react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[3], i18n), i18n: i18n, theme: theme, blurHash: attachments[3].blurHash, bottomOverlay: withBottomOverlay, noBorder: false, curveBottomRight: curveBottomRight, playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[3]), height: 150, width: 150, attachment: attachments[3], url: (0, Attachment_1.getThumbnailUrl)(attachments[3]), onClick: onClick, onError: onError })))));
    }
    const moreMessagesOverlay = attachments.length > 5;
    const moreMessagesOverlayText = moreMessagesOverlay
        ? `+${attachments.length - 5}`
        : undefined;
    return (react_1.default.createElement("div", { className: "module-image-grid" },
        react_1.default.createElement("div", { className: "module-image-grid__column" },
            react_1.default.createElement("div", { className: "module-image-grid__row" },
                react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[0], i18n), i18n: i18n, theme: theme, blurHash: attachments[0].blurHash, curveTopLeft: curveTopLeft, attachment: attachments[0], playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[0]), height: 150, width: 150, cropWidth: GAP, url: (0, Attachment_1.getThumbnailUrl)(attachments[0]), onClick: onClick, onError: onError }),
                react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[1], i18n), i18n: i18n, theme: theme, blurHash: attachments[1].blurHash, curveTopRight: curveTopRight, playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[1]), height: 150, width: 150, attachment: attachments[1], url: (0, Attachment_1.getThumbnailUrl)(attachments[1]), onClick: onClick, onError: onError })),
            react_1.default.createElement("div", { className: "module-image-grid__row" },
                react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[2], i18n), i18n: i18n, theme: theme, blurHash: attachments[2].blurHash, bottomOverlay: withBottomOverlay, noBorder: isSticker, curveBottomLeft: curveBottomLeft, playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[2]), height: 100, width: 100, cropWidth: GAP, attachment: attachments[2], url: (0, Attachment_1.getThumbnailUrl)(attachments[2]), onClick: onClick, onError: onError }),
                react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[3], i18n), i18n: i18n, theme: theme, blurHash: attachments[3].blurHash, bottomOverlay: withBottomOverlay, noBorder: isSticker, playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[3]), height: 100, width: 100, cropWidth: GAP, attachment: attachments[3], url: (0, Attachment_1.getThumbnailUrl)(attachments[3]), onClick: onClick, onError: onError }),
                react_1.default.createElement(Image_1.Image, { alt: (0, Attachment_1.getAlt)(attachments[4], i18n), i18n: i18n, theme: theme, blurHash: attachments[4].blurHash, bottomOverlay: withBottomOverlay, noBorder: isSticker, curveBottomRight: curveBottomRight, playIconOverlay: (0, Attachment_1.isVideoAttachment)(attachments[4]), height: 100, width: 100, darkOverlay: moreMessagesOverlay, overlayText: moreMessagesOverlayText, attachment: attachments[4], url: (0, Attachment_1.getThumbnailUrl)(attachments[4]), onClick: onClick, onError: onError })))));
};
exports.ImageGrid = ImageGrid;
