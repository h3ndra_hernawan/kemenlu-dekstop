"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const ContactDetail_1 = require("./ContactDetail");
const EmbeddedContact_1 = require("../../types/EmbeddedContact");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const MIME_1 = require("../../types/MIME");
const fakeAttachment_1 = require("../../test-both/helpers/fakeAttachment");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/ContactDetail', module);
const createProps = (overrideProps = {}) => ({
    contact: overrideProps.contact || {},
    hasSignalAccount: (0, addon_knobs_1.boolean)('hasSignalAccount', overrideProps.hasSignalAccount || false),
    i18n,
    onSendMessage: (0, addon_actions_1.action)('onSendMessage'),
});
const fullContact = {
    address: [
        {
            type: EmbeddedContact_1.AddressType.HOME,
            street: '555 Main St.',
            city: 'Boston',
            region: 'MA',
            postcode: '33333',
            pobox: '2323-444',
            country: 'US',
            neighborhood: 'Garden Place',
        },
        {
            type: EmbeddedContact_1.AddressType.WORK,
            street: '333 Another St.',
            city: 'Boston',
            region: 'MA',
            postcode: '33344',
            pobox: '2424-555',
            country: 'US',
            neighborhood: 'Factory Place',
        },
        {
            type: EmbeddedContact_1.AddressType.CUSTOM,
            street: '111 Dream St.',
            city: 'Miami',
            region: 'FL',
            postcode: '44232',
            pobox: '111-333',
            country: 'US',
            neighborhood: 'BeachVille',
            label: 'vacation',
        },
        {
            type: EmbeddedContact_1.AddressType.CUSTOM,
            street: '333 Fake St.',
            city: 'Boston',
            region: 'MA',
            postcode: '33345',
            pobox: '123-444',
            country: 'US',
            neighborhood: 'Downtown',
        },
    ],
    avatar: {
        avatar: (0, fakeAttachment_1.fakeAttachment)({
            path: '/fixtures/giphy-GVNvOUpeYmI7e.gif',
            contentType: MIME_1.IMAGE_GIF,
        }),
        isProfile: true,
    },
    email: [
        {
            value: 'jerjor@fakemail.com',
            type: EmbeddedContact_1.ContactFormType.HOME,
        },
        {
            value: 'jerry.jordan@fakeco.com',
            type: EmbeddedContact_1.ContactFormType.WORK,
        },
        {
            value: 'jj@privatething.net',
            type: EmbeddedContact_1.ContactFormType.CUSTOM,
            label: 'private',
        },
        {
            value: 'jordan@another.net',
            type: EmbeddedContact_1.ContactFormType.CUSTOM,
        },
    ],
    name: {
        givenName: 'Jerry',
        familyName: 'Jordan',
        prefix: 'Dr.',
        suffix: 'Jr.',
        middleName: 'James',
        displayName: 'Jerry Jordan',
    },
    number: [
        {
            value: '555-444-2323',
            type: EmbeddedContact_1.ContactFormType.HOME,
        },
        {
            value: '555-444-3232',
            type: EmbeddedContact_1.ContactFormType.WORK,
        },
        {
            value: '555-666-3232',
            type: EmbeddedContact_1.ContactFormType.MOBILE,
        },
        {
            value: '333-666-3232',
            type: EmbeddedContact_1.ContactFormType.CUSTOM,
            label: 'special',
        },
        {
            value: '333-777-3232',
            type: EmbeddedContact_1.ContactFormType.CUSTOM,
        },
    ],
};
story.add('Fully Filled Out', () => {
    const props = createProps({
        contact: fullContact,
        hasSignalAccount: true,
    });
    return React.createElement(ContactDetail_1.ContactDetail, Object.assign({}, props));
});
story.add('Only Email', () => {
    const props = createProps({
        contact: {
            email: [
                {
                    value: 'jerjor@fakemail.com',
                    type: EmbeddedContact_1.ContactFormType.HOME,
                },
            ],
        },
        hasSignalAccount: true,
    });
    return React.createElement(ContactDetail_1.ContactDetail, Object.assign({}, props));
});
story.add('Given Name', () => {
    const props = createProps({
        contact: {
            name: {
                givenName: 'Jerry',
            },
        },
        hasSignalAccount: true,
    });
    return React.createElement(ContactDetail_1.ContactDetail, Object.assign({}, props));
});
story.add('Organization', () => {
    const props = createProps({
        contact: {
            organization: 'Company 5',
        },
        hasSignalAccount: true,
    });
    return React.createElement(ContactDetail_1.ContactDetail, Object.assign({}, props));
});
story.add('Given + Family Name', () => {
    const props = createProps({
        contact: {
            name: {
                givenName: 'Jerry',
                familyName: 'FamilyName',
            },
        },
        hasSignalAccount: true,
    });
    return React.createElement(ContactDetail_1.ContactDetail, Object.assign({}, props));
});
story.add('Family Name', () => {
    const props = createProps({
        contact: {
            name: {
                familyName: 'FamilyName',
            },
        },
        hasSignalAccount: true,
    });
    return React.createElement(ContactDetail_1.ContactDetail, Object.assign({}, props));
});
story.add('Loading Avatar', () => {
    const props = createProps({
        contact: {
            avatar: {
                avatar: (0, fakeAttachment_1.fakeAttachment)({
                    contentType: MIME_1.IMAGE_GIF,
                    pending: true,
                }),
                isProfile: true,
            },
        },
        hasSignalAccount: true,
    });
    return React.createElement(ContactDetail_1.ContactDetail, Object.assign({}, props));
});
story.add('Empty with Account', () => {
    const props = createProps({
        hasSignalAccount: true,
    });
    return React.createElement(ContactDetail_1.ContactDetail, Object.assign({}, props));
});
story.add('Empty without Account', () => {
    const props = createProps({
        hasSignalAccount: false,
    });
    return React.createElement(ContactDetail_1.ContactDetail, Object.assign({}, props));
});
