"use strict";
// Copyright 2019-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LastSeenIndicator = void 0;
const react_1 = __importDefault(require("react"));
const LastSeenIndicator = ({ count, i18n }) => {
    const message = count === 1
        ? i18n('unreadMessage')
        : i18n('unreadMessages', [String(count)]);
    return (react_1.default.createElement("div", { className: "module-last-seen-indicator" },
        react_1.default.createElement("div", { className: "module-last-seen-indicator__bar" }),
        react_1.default.createElement("div", { className: "module-last-seen-indicator__text" }, message)));
};
exports.LastSeenIndicator = LastSeenIndicator;
