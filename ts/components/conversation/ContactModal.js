"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactModal = void 0;
const react_1 = __importStar(require("react"));
const missingCaseError_1 = require("../../util/missingCaseError");
const About_1 = require("./About");
const Avatar_1 = require("../Avatar");
const AvatarLightbox_1 = require("../AvatarLightbox");
const Modal_1 = require("../Modal");
const BadgeDialog_1 = require("../BadgeDialog");
const SharedGroupNames_1 = require("../SharedGroupNames");
const ConfirmationDialog_1 = require("../ConfirmationDialog");
const shouldShowBadges_1 = require("../../badges/shouldShowBadges");
var ContactModalView;
(function (ContactModalView) {
    ContactModalView[ContactModalView["Default"] = 0] = "Default";
    ContactModalView[ContactModalView["ShowingAvatar"] = 1] = "ShowingAvatar";
    ContactModalView[ContactModalView["ShowingBadges"] = 2] = "ShowingBadges";
})(ContactModalView || (ContactModalView = {}));
const ContactModal = ({ areWeAdmin, badges, contact, conversationId, hideContactModal, i18n, isAdmin, isMember, openConversationInternal, removeMemberFromGroup, theme, toggleAdmin, toggleSafetyNumberModal, updateConversationModelSharedGroups, }) => {
    if (!contact) {
        throw new Error('Contact modal opened without a matching contact');
    }
    const [view, setView] = (0, react_1.useState)(ContactModalView.Default);
    const [confirmToggleAdmin, setConfirmToggleAdmin] = (0, react_1.useState)(false);
    (0, react_1.useEffect)(() => {
        if (conversationId) {
            // Kick off the expensive hydration of the current sharedGroupNames
            updateConversationModelSharedGroups(conversationId);
        }
    }, [conversationId, updateConversationModelSharedGroups]);
    switch (view) {
        case ContactModalView.Default: {
            const preferredBadge = badges[0];
            return (react_1.default.createElement(Modal_1.Modal, { moduleClassName: "ContactModal__modal", hasXButton: true, i18n: i18n, onClose: hideContactModal },
                react_1.default.createElement("div", { className: "ContactModal" },
                    react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: contact.acceptedMessageRequest, avatarPath: contact.avatarPath, badge: preferredBadge, color: contact.color, conversationType: "direct", i18n: i18n, isMe: contact.isMe, name: contact.name, profileName: contact.profileName, sharedGroupNames: contact.sharedGroupNames, size: 96, theme: theme, title: contact.title, unblurredAvatarPath: contact.unblurredAvatarPath, onClick: () => {
                            setView(preferredBadge && (0, shouldShowBadges_1.shouldShowBadges)()
                                ? ContactModalView.ShowingBadges
                                : ContactModalView.ShowingAvatar);
                        } }),
                    react_1.default.createElement("div", { className: "ContactModal__name" }, contact.title),
                    react_1.default.createElement("div", { className: "module-about__container" },
                        react_1.default.createElement(About_1.About, { text: contact.about })),
                    contact.phoneNumber && (react_1.default.createElement("div", { className: "ContactModal__info" }, contact.phoneNumber)),
                    !contact.isMe && (react_1.default.createElement("div", { className: "ContactModal__info" },
                        react_1.default.createElement(SharedGroupNames_1.SharedGroupNames, { i18n: i18n, sharedGroupNames: contact.sharedGroupNames || [] }))),
                    react_1.default.createElement("div", { className: "ContactModal__button-container" },
                        react_1.default.createElement("button", { type: "button", className: "ContactModal__button ContactModal__send-message", onClick: () => {
                                hideContactModal();
                                openConversationInternal({ conversationId: contact.id });
                            } },
                            react_1.default.createElement("div", { className: "ContactModal__bubble-icon" },
                                react_1.default.createElement("div", { className: "ContactModal__send-message__bubble-icon" })),
                            react_1.default.createElement("span", null, i18n('ContactModal--message'))),
                        !contact.isMe && (react_1.default.createElement("button", { type: "button", className: "ContactModal__button ContactModal__safety-number", onClick: () => {
                                hideContactModal();
                                toggleSafetyNumberModal(contact.id);
                            } },
                            react_1.default.createElement("div", { className: "ContactModal__bubble-icon" },
                                react_1.default.createElement("div", { className: "ContactModal__safety-number__bubble-icon" })),
                            react_1.default.createElement("span", null, i18n('showSafetyNumber')))),
                        !contact.isMe && areWeAdmin && isMember && conversationId && (react_1.default.createElement(react_1.default.Fragment, null,
                            react_1.default.createElement("button", { type: "button", className: "ContactModal__button ContactModal__make-admin", onClick: () => setConfirmToggleAdmin(true) },
                                react_1.default.createElement("div", { className: "ContactModal__bubble-icon" },
                                    react_1.default.createElement("div", { className: "ContactModal__make-admin__bubble-icon" })),
                                isAdmin ? (react_1.default.createElement("span", null, i18n('ContactModal--rm-admin'))) : (react_1.default.createElement("span", null, i18n('ContactModal--make-admin')))),
                            react_1.default.createElement("button", { type: "button", className: "ContactModal__button ContactModal__remove-from-group", onClick: () => removeMemberFromGroup(conversationId, contact.id) },
                                react_1.default.createElement("div", { className: "ContactModal__bubble-icon" },
                                    react_1.default.createElement("div", { className: "ContactModal__remove-from-group__bubble-icon" })),
                                react_1.default.createElement("span", null, i18n('ContactModal--remove-from-group')))))),
                    confirmToggleAdmin && conversationId && (react_1.default.createElement(ConfirmationDialog_1.ConfirmationDialog, { actions: [
                            {
                                action: () => toggleAdmin(conversationId, contact.id),
                                text: isAdmin
                                    ? i18n('ContactModal--rm-admin')
                                    : i18n('ContactModal--make-admin'),
                            },
                        ], i18n: i18n, onClose: () => setConfirmToggleAdmin(false) }, isAdmin
                        ? i18n('ContactModal--rm-admin-info', [contact.title])
                        : i18n('ContactModal--make-admin-info', [contact.title]))))));
        }
        case ContactModalView.ShowingAvatar:
            return (react_1.default.createElement(AvatarLightbox_1.AvatarLightbox, { avatarColor: contact.color, avatarPath: contact.avatarPath, conversationTitle: contact.title, i18n: i18n, onClose: () => setView(ContactModalView.Default) }));
        case ContactModalView.ShowingBadges:
            return (react_1.default.createElement(BadgeDialog_1.BadgeDialog, { badges: badges, firstName: contact.firstName, i18n: i18n, onClose: () => setView(ContactModalView.Default), title: contact.title }));
        default:
            throw (0, missingCaseError_1.missingCaseError)(view);
    }
};
exports.ContactModal = ContactModal;
