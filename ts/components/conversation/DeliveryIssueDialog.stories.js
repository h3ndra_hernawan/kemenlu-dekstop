"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const DeliveryIssueDialog_1 = require("./DeliveryIssueDialog");
const getDefaultConversation_1 = require("../../test-both/helpers/getDefaultConversation");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const sender = (0, getDefaultConversation_1.getDefaultConversation)();
(0, react_1.storiesOf)('Components/Conversation/DeliveryIssueDialog', module).add('Default', () => {
    return (React.createElement(DeliveryIssueDialog_1.DeliveryIssueDialog, { i18n: i18n, sender: sender, inGroup: false, learnMoreAboutDeliveryIssue: (0, addon_actions_1.action)('learnMoreAboutDeliveryIssue'), onClose: (0, addon_actions_1.action)('onClose') }));
});
(0, react_1.storiesOf)('Components/Conversation/DeliveryIssueDialog', module).add('In Group', () => {
    return (React.createElement(DeliveryIssueDialog_1.DeliveryIssueDialog, { i18n: i18n, sender: sender, inGroup: true, learnMoreAboutDeliveryIssue: (0, addon_actions_1.action)('learnMoreAboutDeliveryIssue'), onClose: (0, addon_actions_1.action)('onClose') }));
});
