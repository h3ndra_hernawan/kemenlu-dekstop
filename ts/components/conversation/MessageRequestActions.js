"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageRequestActions = void 0;
const React = __importStar(require("react"));
const ContactName_1 = require("./ContactName");
const Button_1 = require("../Button");
const MessageRequestActionsConfirmation_1 = require("./MessageRequestActionsConfirmation");
const Intl_1 = require("../Intl");
const MessageRequestActions = ({ conversationType, firstName, i18n, isBlocked, onAccept, onBlock, onBlockAndReportSpam, onDelete, onUnblock, title, }) => {
    const [mrState, setMrState] = React.useState(MessageRequestActionsConfirmation_1.MessageRequestState.default);
    return (React.createElement(React.Fragment, null,
        mrState !== MessageRequestActionsConfirmation_1.MessageRequestState.default ? (React.createElement(MessageRequestActionsConfirmation_1.MessageRequestActionsConfirmation, { i18n: i18n, onBlock: onBlock, onBlockAndReportSpam: onBlockAndReportSpam, onUnblock: onUnblock, onDelete: onDelete, title: title, conversationType: conversationType, state: mrState, onChangeState: setMrState })) : null,
        React.createElement("div", { className: "module-message-request-actions" },
            React.createElement("p", { className: "module-message-request-actions__message" },
                React.createElement(Intl_1.Intl, { i18n: i18n, id: `MessageRequests--message-${conversationType}${isBlocked ? '-blocked' : ''}`, components: [
                        React.createElement("strong", { key: "name", className: "module-message-request-actions__message__name" },
                            React.createElement(ContactName_1.ContactName, { firstName: firstName, title: title, preferFirstName: true })),
                    ] })),
            React.createElement("div", { className: "module-message-request-actions__buttons" },
                React.createElement(Button_1.Button, { onClick: () => {
                        setMrState(MessageRequestActionsConfirmation_1.MessageRequestState.deleting);
                    }, variant: Button_1.ButtonVariant.SecondaryDestructive }, i18n('MessageRequests--delete')),
                isBlocked ? (React.createElement(Button_1.Button, { onClick: () => {
                        setMrState(MessageRequestActionsConfirmation_1.MessageRequestState.unblocking);
                    }, variant: Button_1.ButtonVariant.SecondaryAffirmative }, i18n('MessageRequests--unblock'))) : (React.createElement(Button_1.Button, { onClick: () => {
                        setMrState(MessageRequestActionsConfirmation_1.MessageRequestState.blocking);
                    }, variant: Button_1.ButtonVariant.SecondaryDestructive }, i18n('MessageRequests--block'))),
                !isBlocked ? (React.createElement(Button_1.Button, { onClick: onAccept, variant: Button_1.ButtonVariant.SecondaryAffirmative }, i18n('MessageRequests--accept'))) : null))));
};
exports.MessageRequestActions = MessageRequestActions;
