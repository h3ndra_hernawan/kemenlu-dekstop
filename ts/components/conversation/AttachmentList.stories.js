"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_actions_1 = require("@storybook/addon-actions");
const react_1 = require("@storybook/react");
const AttachmentList_1 = require("./AttachmentList");
const MIME_1 = require("../../types/MIME");
const setupI18n_1 = require("../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../_locales/en/messages.json"));
const fakeAttachment_1 = require("../../test-both/helpers/fakeAttachment");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/AttachmentList', module);
const createProps = (overrideProps = {}) => ({
    attachments: overrideProps.attachments || [],
    i18n,
    onAddAttachment: (0, addon_actions_1.action)('onAddAttachment'),
    onClickAttachment: (0, addon_actions_1.action)('onClickAttachment'),
    onClose: (0, addon_actions_1.action)('onClose'),
    onCloseAttachment: (0, addon_actions_1.action)('onCloseAttachment'),
});
story.add('One File', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
            }),
        ],
    });
    return React.createElement(AttachmentList_1.AttachmentList, Object.assign({}, props));
});
story.add('Multiple Visual Attachments', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
            }),
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                fileName: 'pixabay-Soap-Bubble-7141.mp4',
                url: '/fixtures/kitten-4-112-112.jpg',
                screenshotPath: '/fixtures/kitten-4-112-112.jpg',
            }),
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: MIME_1.IMAGE_GIF,
                fileName: 'giphy-GVNv0UpeYm17e',
                url: '/fixtures/giphy-GVNvOUpeYmI7e.gif',
            }),
        ],
    });
    return React.createElement(AttachmentList_1.AttachmentList, Object.assign({}, props));
});
story.add('Multiple with Non-Visual Types', () => {
    const props = createProps({
        attachments: [
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
            }),
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: (0, MIME_1.stringToMIMEType)('text/plain'),
                fileName: 'lorem-ipsum.txt',
                url: '/fixtures/lorem-ipsum.txt',
            }),
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: MIME_1.AUDIO_MP3,
                fileName: 'incompetech-com-Agnus-Dei-X.mp3',
                url: '/fixtures/incompetech-com-Agnus-Dei-X.mp3',
            }),
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: MIME_1.VIDEO_MP4,
                fileName: 'pixabay-Soap-Bubble-7141.mp4',
                url: '/fixtures/kitten-4-112-112.jpg',
                screenshotPath: '/fixtures/kitten-4-112-112.jpg',
            }),
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: MIME_1.IMAGE_GIF,
                fileName: 'giphy-GVNv0UpeYm17e',
                url: '/fixtures/giphy-GVNvOUpeYmI7e.gif',
            }),
        ],
    });
    return React.createElement(AttachmentList_1.AttachmentList, Object.assign({}, props));
});
story.add('Empty List', () => {
    const props = createProps();
    return React.createElement(AttachmentList_1.AttachmentList, Object.assign({}, props));
});
