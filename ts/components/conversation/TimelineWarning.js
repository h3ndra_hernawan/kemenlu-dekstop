"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimelineWarning = void 0;
const react_1 = __importDefault(require("react"));
const CLASS_NAME = 'module-TimelineWarning';
const ICON_CONTAINER_CLASS_NAME = `${CLASS_NAME}__icon-container`;
const GENERIC_ICON_CLASS_NAME = `${CLASS_NAME}__generic-icon`;
const TEXT_CLASS_NAME = `${CLASS_NAME}__text`;
const LINK_CLASS_NAME = `${TEXT_CLASS_NAME}__link`;
const CLOSE_BUTTON_CLASS_NAME = `${CLASS_NAME}__close-button`;
function TimelineWarning({ children, i18n, onClose, }) {
    return (react_1.default.createElement("div", { className: CLASS_NAME },
        children,
        react_1.default.createElement("button", { "aria-label": i18n('close'), className: CLOSE_BUTTON_CLASS_NAME, onClick: onClose, type: "button" })));
}
exports.TimelineWarning = TimelineWarning;
TimelineWarning.IconContainer = ({ children, }) => (react_1.default.createElement("div", { className: ICON_CONTAINER_CLASS_NAME }, children));
TimelineWarning.GenericIcon = () => react_1.default.createElement("div", { className: GENERIC_ICON_CLASS_NAME });
TimelineWarning.Text = ({ children, }) => (react_1.default.createElement("div", { className: TEXT_CLASS_NAME }, children));
TimelineWarning.Link = ({ children, onClick, }) => (react_1.default.createElement("button", { className: LINK_CLASS_NAME, onClick: onClick, type: "button" }, children));
