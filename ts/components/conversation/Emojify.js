"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Emojify = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const emoji_1 = require("../../util/emoji");
const missingCaseError_1 = require("../../util/missingCaseError");
const lib_1 = require("../emoji/lib");
// Some of this logic taken from emoji-js/replacement
// the DOM structure for this getImageTag should match the other emoji implementations:
// ts/components/emoji/Emoji.tsx
// ts/quill/emoji/blot.tsx
function getImageTag({ match, sizeClass, key, }) {
    const img = (0, lib_1.emojiToImage)(match);
    if (!img) {
        return match;
    }
    return (react_1.default.createElement("img", { key: key, src: img, "aria-label": match, className: (0, classnames_1.default)('emoji', sizeClass), alt: match }));
}
class Emojify extends react_1.default.Component {
    render() {
        const { text, sizeClass, renderNonEmoji } = this.props;
        // We have to do this, because renderNonEmoji is not required in our Props object,
        //  but it is always provided via defaultProps.
        if (!renderNonEmoji) {
            return null;
        }
        return (0, emoji_1.splitByEmoji)(text).map(({ type, value: match }, index) => {
            if (type === 'emoji') {
                return getImageTag({ match, sizeClass, key: index });
            }
            if (type === 'text') {
                return renderNonEmoji({ text: match, key: index });
            }
            throw (0, missingCaseError_1.missingCaseError)(type);
        });
    }
}
exports.Emojify = Emojify;
Emojify.defaultProps = {
    renderNonEmoji: ({ text }) => text,
};
