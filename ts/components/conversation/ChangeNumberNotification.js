"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChangeNumberNotification = void 0;
const react_1 = __importDefault(require("react"));
const Intl_1 = require("../Intl");
const SystemMessage_1 = require("./SystemMessage");
const Timestamp_1 = require("./Timestamp");
const Emojify_1 = require("./Emojify");
const ChangeNumberNotification = props => {
    const { i18n, sender, timestamp } = props;
    return (react_1.default.createElement(SystemMessage_1.SystemMessage, { contents: react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(Intl_1.Intl, { id: "ChangeNumber--notification", components: {
                    sender: react_1.default.createElement(Emojify_1.Emojify, { text: sender.firstName || sender.title }),
                }, i18n: i18n }),
            "\u00A0\u00B7\u00A0",
            react_1.default.createElement(Timestamp_1.Timestamp, { i18n: i18n, timestamp: timestamp })), icon: "phone" }));
};
exports.ChangeNumberNotification = ChangeNumberNotification;
