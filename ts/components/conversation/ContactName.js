"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactName = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const Emojify_1 = require("./Emojify");
const getClassNamesFor_1 = require("../../util/getClassNamesFor");
const ContactName = ({ contactNameColor, firstName, module, preferFirstName, title, }) => {
    const getClassName = (0, getClassNamesFor_1.getClassNamesFor)('module-contact-name', module);
    let text;
    if (preferFirstName) {
        text = firstName || title || '';
    }
    else {
        text = title || '';
    }
    return (react_1.default.createElement("span", { className: (0, classnames_1.default)(getClassName(''), contactNameColor ? getClassName(`--${contactNameColor}`) : null), dir: "auto" },
        react_1.default.createElement(Emojify_1.Emojify, { text: text })));
};
exports.ContactName = ContactName;
