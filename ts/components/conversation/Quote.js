"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Quote = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const classnames_1 = __importDefault(require("classnames"));
const MIME = __importStar(require("../../types/MIME"));
const GoogleChrome = __importStar(require("../../util/GoogleChrome"));
const MessageBody_1 = require("./MessageBody");
const ContactName_1 = require("./ContactName");
const getTextWithMentions_1 = require("../../util/getTextWithMentions");
const getCustomColorStyle_1 = require("../../util/getCustomColorStyle");
function validateQuote(quote) {
    if (quote.text) {
        return true;
    }
    if (quote.rawAttachment) {
        return true;
    }
    return false;
}
// Long message attachments should not be shown.
function getAttachment(rawAttachment) {
    return rawAttachment && !MIME.isLongMessage(rawAttachment.contentType)
        ? rawAttachment
        : undefined;
}
function getObjectUrl(thumbnail) {
    if (thumbnail && thumbnail.objectUrl) {
        return thumbnail.objectUrl;
    }
    return undefined;
}
function getTypeLabel({ i18n, isViewOnce = false, contentType, isVoiceMessage, }) {
    if (GoogleChrome.isVideoTypeSupported(contentType)) {
        if (isViewOnce) {
            return i18n('message--getDescription--disappearing-video');
        }
        return i18n('video');
    }
    if (GoogleChrome.isImageTypeSupported(contentType)) {
        if (isViewOnce) {
            return i18n('message--getDescription--disappearing-photo');
        }
        return i18n('photo');
    }
    if (isViewOnce) {
        return i18n('message--getDescription--disappearing-media');
    }
    if (MIME.isAudio(contentType) && isVoiceMessage) {
        return i18n('voiceMessage');
    }
    return MIME.isAudio(contentType) ? i18n('audio') : undefined;
}
class Quote extends react_1.default.Component {
    constructor(props) {
        super(props);
        this.handleKeyDown = (event) => {
            const { onClick } = this.props;
            // This is important to ensure that using this quote to navigate to the referenced
            //   message doesn't also trigger its parent message's keydown.
            if (onClick && (event.key === 'Enter' || event.key === ' ')) {
                event.preventDefault();
                event.stopPropagation();
                onClick();
            }
        };
        this.handleClick = (event) => {
            const { onClick } = this.props;
            if (onClick) {
                event.preventDefault();
                event.stopPropagation();
                onClick();
            }
        };
        this.handleImageError = () => {
            window.console.info('Message: Image failed to load; failing over to placeholder');
            this.setState({
                imageBroken: true,
            });
        };
        this.state = {
            imageBroken: false,
        };
    }
    componentDidMount() {
        const { doubleCheckMissingQuoteReference, referencedMessageNotFound } = this.props;
        if (referencedMessageNotFound) {
            doubleCheckMissingQuoteReference === null || doubleCheckMissingQuoteReference === void 0 ? void 0 : doubleCheckMissingQuoteReference();
        }
    }
    renderImage(url, icon) {
        const iconElement = icon ? (react_1.default.createElement("div", { className: "module-quote__icon-container__inner" },
            react_1.default.createElement("div", { className: "module-quote__icon-container__circle-background" },
                react_1.default.createElement("div", { className: (0, classnames_1.default)('module-quote__icon-container__icon', `module-quote__icon-container__icon--${icon}`) })))) : null;
        return (react_1.default.createElement(ThumbnailImage, { src: url, onError: this.handleImageError }, iconElement));
    }
    renderIcon(icon) {
        return (react_1.default.createElement("div", { className: "module-quote__icon-container" },
            react_1.default.createElement("div", { className: "module-quote__icon-container__inner" },
                react_1.default.createElement("div", { className: "module-quote__icon-container__circle-background" },
                    react_1.default.createElement("div", { className: (0, classnames_1.default)('module-quote__icon-container__icon', `module-quote__icon-container__icon--${icon}`) })))));
    }
    renderGenericFile() {
        const { rawAttachment, isIncoming } = this.props;
        const attachment = getAttachment(rawAttachment);
        if (!attachment) {
            return null;
        }
        const { fileName, contentType } = attachment;
        const isGenericFile = !GoogleChrome.isVideoTypeSupported(contentType) &&
            !GoogleChrome.isImageTypeSupported(contentType) &&
            !MIME.isAudio(contentType);
        if (!isGenericFile) {
            return null;
        }
        return (react_1.default.createElement("div", { className: "module-quote__generic-file" },
            react_1.default.createElement("div", { className: "module-quote__generic-file__icon" }),
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-quote__generic-file__text', isIncoming ? 'module-quote__generic-file__text--incoming' : null) }, fileName)));
    }
    renderIconContainer() {
        const { rawAttachment, isViewOnce } = this.props;
        const { imageBroken } = this.state;
        const attachment = getAttachment(rawAttachment);
        if (!attachment) {
            return null;
        }
        const { contentType, thumbnail } = attachment;
        const objectUrl = getObjectUrl(thumbnail);
        if (isViewOnce) {
            return this.renderIcon('view-once');
        }
        if (GoogleChrome.isVideoTypeSupported(contentType)) {
            return objectUrl && !imageBroken
                ? this.renderImage(objectUrl, 'play')
                : this.renderIcon('movie');
        }
        if (GoogleChrome.isImageTypeSupported(contentType)) {
            return objectUrl && !imageBroken
                ? this.renderImage(objectUrl)
                : this.renderIcon('image');
        }
        if (MIME.isAudio(contentType)) {
            return this.renderIcon('microphone');
        }
        return null;
    }
    renderText() {
        const { bodyRanges, i18n, text, rawAttachment, isIncoming, isViewOnce } = this.props;
        if (text) {
            const quoteText = bodyRanges
                ? (0, getTextWithMentions_1.getTextWithMentions)(bodyRanges, text)
                : text;
            return (react_1.default.createElement("div", { dir: "auto", className: (0, classnames_1.default)('module-quote__primary__text', isIncoming ? 'module-quote__primary__text--incoming' : null) },
                react_1.default.createElement(MessageBody_1.MessageBody, { disableLinks: true, disableJumbomoji: true, text: quoteText, i18n: i18n })));
        }
        const attachment = getAttachment(rawAttachment);
        if (!attachment) {
            return null;
        }
        const { contentType, isVoiceMessage } = attachment;
        const typeLabel = getTypeLabel({
            i18n,
            isViewOnce,
            contentType,
            isVoiceMessage,
        });
        if (typeLabel) {
            return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-quote__primary__type-label', isIncoming ? 'module-quote__primary__type-label--incoming' : null) }, typeLabel));
        }
        return null;
    }
    renderClose() {
        const { i18n, onClose } = this.props;
        if (!onClose) {
            return null;
        }
        const clickHandler = (e) => {
            e.stopPropagation();
            e.preventDefault();
            onClose();
        };
        const keyDownHandler = (e) => {
            if (e.key === 'Enter' || e.key === ' ') {
                e.stopPropagation();
                e.preventDefault();
                onClose();
            }
        };
        // We need the container to give us the flexibility to implement the iOS design.
        return (react_1.default.createElement("div", { className: "module-quote__close-container" },
            react_1.default.createElement("div", { tabIndex: 0, 
                // We can't be a button because the overall quote is a button; can't nest them
                role: "button", className: "module-quote__close-button", "aria-label": i18n('close'), onKeyDown: keyDownHandler, onClick: clickHandler })));
    }
    renderAuthor() {
        const { authorTitle, i18n, isFromMe, isIncoming } = this.props;
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-quote__primary__author', isIncoming ? 'module-quote__primary__author--incoming' : null) }, isFromMe ? i18n('you') : react_1.default.createElement(ContactName_1.ContactName, { title: authorTitle })));
    }
    renderReferenceWarning() {
        const { conversationColor, customColor, i18n, isIncoming, referencedMessageNotFound, } = this.props;
        if (!referencedMessageNotFound) {
            return null;
        }
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-quote__reference-warning', isIncoming
                ? `module-quote--incoming-${conversationColor}`
                : `module-quote--outgoing-${conversationColor}`), style: Object.assign({}, (0, getCustomColorStyle_1.getCustomColorStyle)(customColor, true)) },
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-quote__reference-warning__icon', isIncoming
                    ? 'module-quote__reference-warning__icon--incoming'
                    : null) }),
            react_1.default.createElement("div", { className: (0, classnames_1.default)('module-quote__reference-warning__text', isIncoming
                    ? 'module-quote__reference-warning__text--incoming'
                    : null) }, i18n('originalMessageNotFound'))));
    }
    render() {
        const { conversationColor, customColor, isIncoming, onClick, referencedMessageNotFound, } = this.props;
        if (!validateQuote(this.props)) {
            return null;
        }
        return (react_1.default.createElement("div", { className: "module-quote-container" },
            react_1.default.createElement("button", { type: "button", onClick: this.handleClick, onKeyDown: this.handleKeyDown, className: (0, classnames_1.default)('module-quote', isIncoming ? 'module-quote--incoming' : 'module-quote--outgoing', isIncoming
                    ? `module-quote--incoming-${conversationColor}`
                    : `module-quote--outgoing-${conversationColor}`, !onClick ? 'module-quote--no-click' : null, referencedMessageNotFound
                    ? 'module-quote--with-reference-warning'
                    : null), style: Object.assign({}, (0, getCustomColorStyle_1.getCustomColorStyle)(customColor, true)) },
                react_1.default.createElement("div", { className: "module-quote__primary" },
                    this.renderAuthor(),
                    this.renderGenericFile(),
                    this.renderText()),
                this.renderIconContainer(),
                this.renderClose()),
            this.renderReferenceWarning()));
    }
}
exports.Quote = Quote;
function ThumbnailImage({ src, onError, children, }) {
    const imageRef = (0, react_1.useRef)(new Image());
    const [loadedSrc, setLoadedSrc] = (0, react_1.useState)(null);
    (0, react_1.useEffect)(() => {
        const image = new Image();
        image.onload = () => {
            setLoadedSrc(src);
        };
        image.src = src;
        imageRef.current = image;
        return () => {
            image.onload = lodash_1.noop;
        };
    }, [src]);
    (0, react_1.useEffect)(() => {
        setLoadedSrc(null);
    }, [src]);
    (0, react_1.useEffect)(() => {
        const image = imageRef.current;
        image.onerror = onError;
        return () => {
            image.onerror = lodash_1.noop;
        };
    }, [onError]);
    return (react_1.default.createElement("div", { className: "module-quote__icon-container", style: loadedSrc ? { backgroundImage: `url('${encodeURI(loadedSrc)}')` } : {} }, children));
}
