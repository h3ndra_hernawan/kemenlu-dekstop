"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmptyState = void 0;
const react_1 = __importDefault(require("react"));
const EmptyState = ({ label }) => (react_1.default.createElement("div", { className: "module-empty-state" }, label));
exports.EmptyState = EmptyState;
