"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaGridItem = void 0;
const react_1 = __importDefault(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const GoogleChrome_1 = require("../../../util/GoogleChrome");
const log = __importStar(require("../../../logging/log"));
class MediaGridItem extends react_1.default.Component {
    constructor(props) {
        super(props);
        this.state = {
            imageBroken: false,
        };
        this.onImageErrorBound = this.onImageError.bind(this);
    }
    onImageError() {
        log.info('MediaGridItem: Image failed to load; failing over to placeholder');
        this.setState({
            imageBroken: true,
        });
    }
    renderContent() {
        const { mediaItem, i18n } = this.props;
        const { imageBroken } = this.state;
        const { attachment, contentType } = mediaItem;
        if (!attachment) {
            return null;
        }
        if (contentType && (0, GoogleChrome_1.isImageTypeSupported)(contentType)) {
            if (imageBroken || !mediaItem.thumbnailObjectUrl) {
                return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-media-grid-item__icon', 'module-media-grid-item__icon-image') }));
            }
            return (react_1.default.createElement("img", { alt: i18n('lightboxImageAlt'), className: "module-media-grid-item__image", src: mediaItem.thumbnailObjectUrl, onError: this.onImageErrorBound }));
        }
        if (contentType && (0, GoogleChrome_1.isVideoTypeSupported)(contentType)) {
            if (imageBroken || !mediaItem.thumbnailObjectUrl) {
                return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-media-grid-item__icon', 'module-media-grid-item__icon-video') }));
            }
            return (react_1.default.createElement("div", { className: "module-media-grid-item__image-container" },
                react_1.default.createElement("img", { alt: i18n('lightboxImageAlt'), className: "module-media-grid-item__image", src: mediaItem.thumbnailObjectUrl, onError: this.onImageErrorBound }),
                react_1.default.createElement("div", { className: "module-media-grid-item__circle-overlay" },
                    react_1.default.createElement("div", { className: "module-media-grid-item__play-overlay" }))));
        }
        return (react_1.default.createElement("div", { className: (0, classnames_1.default)('module-media-grid-item__icon', 'module-media-grid-item__icon-generic') }));
    }
    render() {
        const { onClick } = this.props;
        return (react_1.default.createElement("button", { type: "button", className: "module-media-grid-item", onClick: onClick }, this.renderContent()));
    }
}
exports.MediaGridItem = MediaGridItem;
