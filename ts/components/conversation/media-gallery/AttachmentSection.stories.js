"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createPreparedMediaItems = exports.createRandomMedia = exports.createRandomDocuments = exports.days = exports.now = void 0;
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const lodash_1 = require("lodash");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const AttachmentSection_1 = require("./AttachmentSection");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/MediaGallery/AttachmentSection', module);
// eslint-disable-next-line @typescript-eslint/no-explicit-any
story.addDecorator(addon_knobs_1.withKnobs({ escapeHTML: false }));
exports.now = Date.now();
const DAY_MS = 24 * 60 * 60 * 1000;
const days = (n) => n * DAY_MS;
exports.days = days;
const tokens = ['foo', 'bar', 'baz', 'qux', 'quux'];
const contentTypes = {
    gif: 'image/gif',
    jpg: 'image/jpeg',
    png: 'image/png',
    mp4: 'video/mp4',
    docx: 'application/text',
    pdf: 'application/pdf',
    txt: 'application/text',
};
const createRandomFile = (startTime, timeWindow, fileExtension) => {
    const contentType = contentTypes[fileExtension];
    const fileName = `${(0, lodash_1.sample)(tokens)}${(0, lodash_1.sample)(tokens)}.${fileExtension}`;
    return {
        contentType,
        message: {
            conversationId: '123',
            id: (0, lodash_1.random)(exports.now).toString(),
            received_at: Math.floor(Math.random() * 10),
            received_at_ms: (0, lodash_1.random)(startTime, startTime + timeWindow),
            attachments: [],
            sent_at: Date.now(),
        },
        attachment: {
            url: '',
            fileName,
            size: (0, lodash_1.random)(1000, 1000 * 1000 * 50),
            contentType,
        },
        index: 0,
        thumbnailObjectUrl: `https://placekitten.com/${(0, lodash_1.random)(50, 150)}/${(0, lodash_1.random)(50, 150)}`,
    };
};
const createRandomFiles = (startTime, timeWindow, fileExtensions) => (0, lodash_1.range)((0, lodash_1.random)(5, 10)).map(() => createRandomFile(startTime, timeWindow, (0, lodash_1.sample)(fileExtensions)));
const createRandomDocuments = (startTime, timeWindow) => createRandomFiles(startTime, timeWindow, ['docx', 'pdf', 'txt']);
exports.createRandomDocuments = createRandomDocuments;
const createRandomMedia = (startTime, timeWindow) => createRandomFiles(startTime, timeWindow, ['mp4', 'jpg', 'png', 'gif']);
exports.createRandomMedia = createRandomMedia;
const createPreparedMediaItems = (fn) => (0, lodash_1.sortBy)([
    ...fn(exports.now, (0, exports.days)(1)),
    ...fn(exports.now - (0, exports.days)(1), (0, exports.days)(1)),
    ...fn(exports.now - (0, exports.days)(3), (0, exports.days)(3)),
    ...fn(exports.now - (0, exports.days)(30), (0, exports.days)(15)),
    ...fn(exports.now - (0, exports.days)(365), (0, exports.days)(300)),
], (item) => -item.message.received_at);
exports.createPreparedMediaItems = createPreparedMediaItems;
const createProps = (overrideProps = {}) => ({
    i18n,
    header: (0, addon_knobs_1.text)('header', 'Today'),
    type: (0, addon_knobs_1.select)('type', { media: 'media', documents: 'documents' }, overrideProps.type || 'media'),
    mediaItems: overrideProps.mediaItems || [],
});
story.add('Documents', () => {
    const mediaItems = (0, exports.createRandomDocuments)(exports.now, (0, exports.days)(1));
    const props = createProps({ mediaItems, type: 'documents' });
    return React.createElement(AttachmentSection_1.AttachmentSection, Object.assign({}, props));
});
story.add('Media', () => {
    const mediaItems = (0, exports.createRandomMedia)(exports.now, (0, exports.days)(1));
    const props = createProps({ mediaItems, type: 'media' });
    return React.createElement(AttachmentSection_1.AttachmentSection, Object.assign({}, props));
});
