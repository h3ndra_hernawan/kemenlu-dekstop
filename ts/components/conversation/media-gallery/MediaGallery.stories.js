"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const AttachmentSection_stories_1 = require("./AttachmentSection.stories");
const MediaGallery_1 = require("./MediaGallery");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/MediaGallery/MediaGallery', module);
const createProps = (overrideProps = {}) => ({
    i18n,
    onItemClick: (0, addon_actions_1.action)('onItemClick'),
    documents: overrideProps.documents || [],
    media: overrideProps.media || [],
});
story.add('Populated', () => {
    const documents = (0, AttachmentSection_stories_1.createRandomDocuments)(AttachmentSection_stories_1.now, (0, AttachmentSection_stories_1.days)(1)).slice(0, 1);
    const media = (0, AttachmentSection_stories_1.createPreparedMediaItems)(AttachmentSection_stories_1.createRandomMedia);
    const props = createProps({ documents, media });
    return React.createElement(MediaGallery_1.MediaGallery, Object.assign({}, props));
});
story.add('No Documents', () => {
    const media = (0, AttachmentSection_stories_1.createPreparedMediaItems)(AttachmentSection_stories_1.createRandomMedia);
    const props = createProps({ media });
    return React.createElement(MediaGallery_1.MediaGallery, Object.assign({}, props));
});
story.add('No Media', () => {
    const documents = (0, AttachmentSection_stories_1.createPreparedMediaItems)(AttachmentSection_stories_1.createRandomDocuments);
    const props = createProps({ documents });
    return React.createElement(MediaGallery_1.MediaGallery, Object.assign({}, props));
});
story.add('One Each', () => {
    const media = (0, AttachmentSection_stories_1.createRandomMedia)(AttachmentSection_stories_1.now, (0, AttachmentSection_stories_1.days)(1)).slice(0, 1);
    const documents = (0, AttachmentSection_stories_1.createRandomDocuments)(AttachmentSection_stories_1.now, (0, AttachmentSection_stories_1.days)(1)).slice(0, 1);
    const props = createProps({ documents, media });
    return React.createElement(MediaGallery_1.MediaGallery, Object.assign({}, props));
});
story.add('Empty', () => {
    const props = createProps();
    return React.createElement(MediaGallery_1.MediaGallery, Object.assign({}, props));
});
