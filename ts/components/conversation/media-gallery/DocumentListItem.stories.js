"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const DocumentListItem_1 = require("./DocumentListItem");
const story = (0, react_1.storiesOf)('Components/Conversation/MediaGallery/DocumentListItem', module);
// eslint-disable-next-line @typescript-eslint/no-explicit-any
story.addDecorator(addon_knobs_1.withKnobs({ escapeHTML: false }));
story.add('Single', () => (React.createElement(DocumentListItem_1.DocumentListItem, { timestamp: (0, addon_knobs_1.date)('timestamp', new Date()), fileName: (0, addon_knobs_1.text)('fileName', 'meow.jpg'), fileSize: (0, addon_knobs_1.number)('fileSize', 1024 * 1000 * 2), shouldShowSeparator: (0, addon_knobs_1.boolean)('shouldShowSeparator', false), onClick: (0, addon_actions_1.action)('onClick') })));
story.add('Multiple', () => {
    const items = [
        {
            fileName: 'meow.jpg',
            fileSize: 1024 * 1000 * 2,
            timestamp: Date.now(),
        },
        {
            fileName: 'rickroll.mp4',
            fileSize: 1024 * 1000 * 8,
            timestamp: Date.now() - 24 * 60 * 60 * 1000,
        },
        {
            fileName: 'kitten.gif',
            fileSize: 1024 * 1000 * 1.2,
            timestamp: Date.now() - 14 * 24 * 60 * 60 * 1000,
            shouldShowSeparator: false,
        },
    ];
    return items.map(item => (React.createElement(DocumentListItem_1.DocumentListItem, Object.assign({ key: item.fileName, onClick: (0, addon_actions_1.action)('onClick') }, item))));
});
