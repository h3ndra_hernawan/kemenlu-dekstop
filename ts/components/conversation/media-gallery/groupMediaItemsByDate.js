"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.groupMediaItemsByDate = void 0;
const moment_1 = __importDefault(require("moment"));
const lodash_1 = require("lodash");
const log = __importStar(require("../../../logging/log"));
const getMessageTimestamp_1 = require("../../../util/getMessageTimestamp");
const missingCaseError_1 = require("../../../util/missingCaseError");
const groupMediaItemsByDate = (timestamp, mediaItems) => {
    const referenceDateTime = moment_1.default.utc(timestamp);
    const sortedMediaItem = (0, lodash_1.sortBy)(mediaItems, mediaItem => {
        const { message } = mediaItem;
        return -message.received_at;
    });
    const messagesWithSection = sortedMediaItem.map(withSection(referenceDateTime));
    const groupedMediaItem = (0, lodash_1.groupBy)(messagesWithSection, 'type');
    const yearMonthMediaItem = Object.values((0, lodash_1.groupBy)(groupedMediaItem.yearMonth, 'order')).reverse();
    return (0, lodash_1.compact)([
        toSection(groupedMediaItem.today),
        toSection(groupedMediaItem.yesterday),
        toSection(groupedMediaItem.thisWeek),
        toSection(groupedMediaItem.thisMonth),
        ...yearMonthMediaItem.map(toSection),
    ]);
};
exports.groupMediaItemsByDate = groupMediaItemsByDate;
const toSection = (messagesWithSection) => {
    if (!messagesWithSection || messagesWithSection.length === 0) {
        return undefined;
    }
    const firstMediaItemWithSection = messagesWithSection[0];
    if (!firstMediaItemWithSection) {
        return undefined;
    }
    const mediaItems = messagesWithSection.map(messageWithSection => messageWithSection.mediaItem);
    switch (firstMediaItemWithSection.type) {
        case 'today':
        case 'yesterday':
        case 'thisWeek':
        case 'thisMonth':
            return {
                type: firstMediaItemWithSection.type,
                mediaItems,
            };
        case 'yearMonth':
            return {
                type: firstMediaItemWithSection.type,
                year: firstMediaItemWithSection.year,
                month: firstMediaItemWithSection.month,
                mediaItems,
            };
        default:
            log.error((0, missingCaseError_1.missingCaseError)(firstMediaItemWithSection));
            return undefined;
    }
};
const withSection = (referenceDateTime) => (mediaItem) => {
    const today = (0, moment_1.default)(referenceDateTime).startOf('day');
    const yesterday = (0, moment_1.default)(referenceDateTime)
        .subtract(1, 'day')
        .startOf('day');
    const thisWeek = (0, moment_1.default)(referenceDateTime).startOf('isoWeek');
    const thisMonth = (0, moment_1.default)(referenceDateTime).startOf('month');
    const { message } = mediaItem;
    const mediaItemReceivedDate = moment_1.default.utc((0, getMessageTimestamp_1.getMessageTimestamp)(message));
    if (mediaItemReceivedDate.isAfter(today)) {
        return {
            order: 0,
            type: 'today',
            mediaItem,
        };
    }
    if (mediaItemReceivedDate.isAfter(yesterday)) {
        return {
            order: 1,
            type: 'yesterday',
            mediaItem,
        };
    }
    if (mediaItemReceivedDate.isAfter(thisWeek)) {
        return {
            order: 2,
            type: 'thisWeek',
            mediaItem,
        };
    }
    if (mediaItemReceivedDate.isAfter(thisMonth)) {
        return {
            order: 3,
            type: 'thisMonth',
            mediaItem,
        };
    }
    const month = mediaItemReceivedDate.month();
    const year = mediaItemReceivedDate.year();
    return {
        order: year * 100 + month,
        type: 'yearMonth',
        month,
        year,
        mediaItem,
    };
};
