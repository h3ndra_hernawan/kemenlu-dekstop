"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../../../util/setupI18n");
const messages_json_1 = __importDefault(require("../../../../_locales/en/messages.json"));
const MIME_1 = require("../../../types/MIME");
const MediaGridItem_1 = require("./MediaGridItem");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Conversation/MediaGallery/MediaGridItem', module);
// eslint-disable-next-line @typescript-eslint/no-explicit-any
story.addDecorator(addon_knobs_1.withKnobs({ escapeHTML: false }));
const createProps = (overrideProps) => ({
    i18n,
    mediaItem: overrideProps.mediaItem,
    onClick: (0, addon_actions_1.action)('onClick'),
});
const createMediaItem = (overrideProps = {}) => ({
    thumbnailObjectUrl: (0, addon_knobs_1.text)('thumbnailObjectUrl', overrideProps.thumbnailObjectUrl || ''),
    contentType: (0, MIME_1.stringToMIMEType)((0, addon_knobs_1.text)('contentType', overrideProps.contentType || '')),
    index: 0,
    attachment: {},
    message: {
        attachments: [],
        conversationId: '1234',
        id: 'id',
        received_at: Date.now(),
        received_at_ms: Date.now(),
        sent_at: Date.now(),
    },
});
story.add('Image', () => {
    const mediaItem = createMediaItem({
        thumbnailObjectUrl: '/fixtures/kitten-1-64-64.jpg',
        contentType: (0, MIME_1.stringToMIMEType)('image/jpeg'),
    });
    const props = createProps({
        mediaItem,
    });
    return React.createElement(MediaGridItem_1.MediaGridItem, Object.assign({}, props));
});
story.add('Video', () => {
    const mediaItem = createMediaItem({
        thumbnailObjectUrl: '/fixtures/kitten-2-64-64.jpg',
        contentType: (0, MIME_1.stringToMIMEType)('video/mp4'),
    });
    const props = createProps({
        mediaItem,
    });
    return React.createElement(MediaGridItem_1.MediaGridItem, Object.assign({}, props));
});
story.add('Missing Image', () => {
    const mediaItem = createMediaItem({
        contentType: (0, MIME_1.stringToMIMEType)('image/jpeg'),
    });
    const props = createProps({
        mediaItem,
    });
    return React.createElement(MediaGridItem_1.MediaGridItem, Object.assign({}, props));
});
story.add('Missing Video', () => {
    const mediaItem = createMediaItem({
        contentType: (0, MIME_1.stringToMIMEType)('video/mp4'),
    });
    const props = createProps({
        mediaItem,
    });
    return React.createElement(MediaGridItem_1.MediaGridItem, Object.assign({}, props));
});
story.add('Broken Image', () => {
    const mediaItem = createMediaItem({
        thumbnailObjectUrl: '/missing-fixtures/nope.jpg',
        contentType: (0, MIME_1.stringToMIMEType)('image/jpeg'),
    });
    const props = createProps({
        mediaItem,
    });
    return React.createElement(MediaGridItem_1.MediaGridItem, Object.assign({}, props));
});
story.add('Broken Video', () => {
    const mediaItem = createMediaItem({
        thumbnailObjectUrl: '/missing-fixtures/nope.mp4',
        contentType: (0, MIME_1.stringToMIMEType)('video/mp4'),
    });
    const props = createProps({
        mediaItem,
    });
    return React.createElement(MediaGridItem_1.MediaGridItem, Object.assign({}, props));
});
story.add('Other ContentType', () => {
    const mediaItem = createMediaItem({
        contentType: (0, MIME_1.stringToMIMEType)('application/text'),
    });
    const props = createProps({
        mediaItem,
    });
    return React.createElement(MediaGridItem_1.MediaGridItem, Object.assign({}, props));
});
