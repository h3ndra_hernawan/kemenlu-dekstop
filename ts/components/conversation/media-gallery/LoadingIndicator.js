"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadingIndicator = void 0;
const react_1 = __importDefault(require("react"));
const LoadingIndicator = () => {
    return (react_1.default.createElement("div", { className: "loading-widget" },
        react_1.default.createElement("div", { className: "container" },
            react_1.default.createElement("span", { className: "dot" }),
            react_1.default.createElement("span", { className: "dot" }),
            react_1.default.createElement("span", { className: "dot" }))));
};
exports.LoadingIndicator = LoadingIndicator;
