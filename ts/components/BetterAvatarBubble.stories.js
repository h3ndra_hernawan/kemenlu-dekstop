"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const Colors_1 = require("../types/Colors");
const BetterAvatarBubble_1 = require("./BetterAvatarBubble");
const setupI18n_1 = require("../util/setupI18n");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    children: overrideProps.children,
    color: overrideProps.color,
    i18n,
    isSelected: Boolean(overrideProps.isSelected),
    onDelete: (0, addon_actions_1.action)('onDelete'),
    onSelect: (0, addon_actions_1.action)('onSelect'),
    style: overrideProps.style,
});
const story = (0, react_2.storiesOf)('Components/BetterAvatarBubble', module);
story.add('Children', () => (react_1.default.createElement(BetterAvatarBubble_1.BetterAvatarBubble, Object.assign({}, createProps({
    children: react_1.default.createElement("div", null, "HI"),
    color: Colors_1.AvatarColors[8],
})))));
story.add('Selected', () => (react_1.default.createElement(BetterAvatarBubble_1.BetterAvatarBubble, Object.assign({}, createProps({
    color: Colors_1.AvatarColors[1],
    isSelected: true,
})))));
story.add('Style', () => (react_1.default.createElement(BetterAvatarBubble_1.BetterAvatarBubble, Object.assign({}, createProps({
    style: {
        height: 120,
        width: 120,
    },
    color: Colors_1.AvatarColors[2],
})))));
