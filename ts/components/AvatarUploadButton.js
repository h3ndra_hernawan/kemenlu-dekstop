"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvatarUploadButton = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const processImageFile_1 = require("../util/processImageFile");
const AvatarUploadButton = ({ className, i18n, onChange, }) => {
    const fileInputRef = (0, react_1.useRef)(null);
    const [processingFile, setProcessingFile] = (0, react_1.useState)();
    (0, react_1.useEffect)(() => {
        if (!processingFile) {
            return lodash_1.noop;
        }
        let shouldCancel = false;
        (async () => {
            let newAvatar;
            try {
                newAvatar = await (0, processImageFile_1.processImageFile)(processingFile);
            }
            catch (err) {
                // Processing errors should be rare; if they do, we silently fail. In an ideal
                //   world, we may want to show a toast instead.
                return;
            }
            if (shouldCancel) {
                return;
            }
            setProcessingFile(undefined);
            onChange(newAvatar);
        })();
        return () => {
            shouldCancel = true;
        };
    }, [onChange, processingFile]);
    const onInputChange = event => {
        const file = event.target.files && event.target.files[0];
        if (file) {
            setProcessingFile(file);
        }
    };
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("button", { className: className, onClick: () => {
                const fileInput = fileInputRef.current;
                if (fileInput) {
                    // Setting the value to empty so that onChange always fires in case
                    // you add multiple photos.
                    fileInput.value = '';
                    fileInput.click();
                }
            }, type: "button" }, i18n('photo')),
        react_1.default.createElement("input", { accept: ".gif,.jpg,.jpeg,.png,.webp,image/gif,image/jpeg,image/png,image/webp", hidden: true, onChange: onInputChange, ref: fileInputRef, type: "file" })));
};
exports.AvatarUploadButton = AvatarUploadButton;
