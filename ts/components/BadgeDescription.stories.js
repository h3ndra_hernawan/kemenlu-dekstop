"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const BadgeDescription_1 = require("./BadgeDescription");
const story = (0, react_2.storiesOf)('Components/BadgeDescription', module);
story.add('Normal name', () => (react_1.default.createElement(BadgeDescription_1.BadgeDescription, { template: "{short_name} is here! Hello, {short_name}! {short_name}, I think you're great. This is not replaced: {not_replaced}", firstName: "Alice", title: "Should not be seen" })));
story.add('Name with RTL overrides', () => (react_1.default.createElement(BadgeDescription_1.BadgeDescription, { template: "Hello, {short_name}! {short_name}, I think you're great.", title: 'Flip-\u202eflop' })));
