"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const AvatarModalButtons_1 = require("./AvatarModalButtons");
const setupI18n_1 = require("../util/setupI18n");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    hasChanges: Boolean(overrideProps.hasChanges),
    i18n,
    onCancel: (0, addon_actions_1.action)('onCancel'),
    onSave: (0, addon_actions_1.action)('onSave'),
});
const story = (0, react_2.storiesOf)('Components/AvatarModalButtons', module);
story.add('Has changes', () => (react_1.default.createElement(AvatarModalButtons_1.AvatarModalButtons, Object.assign({}, createProps({
    hasChanges: true,
})))));
story.add('No changes', () => react_1.default.createElement(AvatarModalButtons_1.AvatarModalButtons, Object.assign({}, createProps())));
