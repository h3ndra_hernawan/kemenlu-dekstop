"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddGroupMemberErrorDialog = exports.AddGroupMemberErrorDialogMode = void 0;
const react_1 = __importDefault(require("react"));
const Alert_1 = require("./Alert");
const Intl_1 = require("./Intl");
const ContactName_1 = require("./conversation/ContactName");
const missingCaseError_1 = require("../util/missingCaseError");
var AddGroupMemberErrorDialogMode;
(function (AddGroupMemberErrorDialogMode) {
    AddGroupMemberErrorDialogMode[AddGroupMemberErrorDialogMode["CantAddContact"] = 0] = "CantAddContact";
    AddGroupMemberErrorDialogMode[AddGroupMemberErrorDialogMode["MaximumGroupSize"] = 1] = "MaximumGroupSize";
    AddGroupMemberErrorDialogMode[AddGroupMemberErrorDialogMode["RecommendedMaximumGroupSize"] = 2] = "RecommendedMaximumGroupSize";
})(AddGroupMemberErrorDialogMode = exports.AddGroupMemberErrorDialogMode || (exports.AddGroupMemberErrorDialogMode = {}));
const AddGroupMemberErrorDialog = props => {
    const { i18n, onClose } = props;
    let title;
    let body;
    switch (props.mode) {
        case AddGroupMemberErrorDialogMode.CantAddContact: {
            const { contact } = props;
            title = i18n('chooseGroupMembers__cant-add-member__title');
            body = (react_1.default.createElement(Intl_1.Intl, { i18n: i18n, id: "chooseGroupMembers__cant-add-member__body", components: [react_1.default.createElement(ContactName_1.ContactName, { key: "name", title: contact.title })] }));
            break;
        }
        case AddGroupMemberErrorDialogMode.MaximumGroupSize: {
            const { maximumNumberOfContacts } = props;
            title = i18n('chooseGroupMembers__maximum-group-size__title');
            body = i18n('chooseGroupMembers__maximum-group-size__body', [
                maximumNumberOfContacts.toString(),
            ]);
            break;
        }
        case AddGroupMemberErrorDialogMode.RecommendedMaximumGroupSize: {
            const { recommendedMaximumNumberOfContacts } = props;
            title = i18n('chooseGroupMembers__maximum-recommended-group-size__title');
            body = i18n('chooseGroupMembers__maximum-recommended-group-size__body', [recommendedMaximumNumberOfContacts.toString()]);
            break;
        }
        default:
            throw (0, missingCaseError_1.missingCaseError)(props);
    }
    return react_1.default.createElement(Alert_1.Alert, { body: body, i18n: i18n, onClose: onClose, title: title });
};
exports.AddGroupMemberErrorDialog = AddGroupMemberErrorDialog;
