"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeftPaneSearchInput = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const refMerger_1 = require("../util/refMerger");
const Avatar_1 = require("./Avatar");
exports.LeftPaneSearchInput = (0, react_1.forwardRef)(({ disabled, i18n, onBlur, onChangeValue, onClear, searchConversation, value, }, outerRef) => {
    const inputRef = (0, react_1.useRef)(null);
    const emptyOrClear = searchConversation && value ? () => onChangeValue('') : onClear;
    const label = searchConversation
        ? i18n('searchIn', [
            searchConversation.isMe
                ? i18n('noteToSelf')
                : searchConversation.title,
        ])
        : i18n('search');
    return (react_1.default.createElement("div", { className: "LeftPaneSearchInput" },
        searchConversation ? (
        // Clicking the non-X part of the pill should focus the input but have a normal
        //   cursor. This effectively simulates `pointer-events: none` while still
        //   letting us change the cursor.
        // eslint-disable-next-line max-len
        // eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions
        react_1.default.createElement("div", { className: "LeftPaneSearchInput__in-conversation-pill", onClick: () => {
                var _a;
                (_a = inputRef.current) === null || _a === void 0 ? void 0 : _a.focus();
            } },
            react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: searchConversation.acceptedMessageRequest, avatarPath: searchConversation.avatarPath, color: searchConversation.color, conversationType: searchConversation.type, i18n: i18n, isMe: searchConversation.isMe, noteToSelf: searchConversation.isMe, sharedGroupNames: searchConversation.sharedGroupNames, size: Avatar_1.AvatarSize.SIXTEEN, title: searchConversation.title, unblurredAvatarPath: searchConversation.unblurredAvatarPath }),
            react_1.default.createElement("button", { "aria-label": i18n('clearSearch'), className: "LeftPaneSearchInput__in-conversation-pill__x-button", onClick: onClear, type: "button" }))) : (react_1.default.createElement("div", { className: "LeftPaneSearchInput__icon" })),
        react_1.default.createElement("input", { "aria-label": label, className: (0, classnames_1.default)('LeftPaneSearchInput__input', value && 'LeftPaneSearchInput__input--with-text', searchConversation && 'LeftPaneSearchInput__input--in-conversation'), dir: "auto", disabled: disabled, onBlur: onBlur, onChange: event => {
                onChangeValue(event.currentTarget.value);
            }, onKeyDown: event => {
                const { ctrlKey, key } = event;
                // On Linux, this key combo selects all text.
                if (window.platform === 'linux' && ctrlKey && key === '/') {
                    event.preventDefault();
                    event.stopPropagation();
                }
                else if (key === 'Escape') {
                    emptyOrClear();
                    event.preventDefault();
                    event.stopPropagation();
                }
            }, placeholder: label, ref: (0, refMerger_1.refMerger)(inputRef, outerRef), value: value }),
        value && (react_1.default.createElement("button", { "aria-label": i18n('cancel'), className: "LeftPaneSearchInput__cancel", onClick: emptyOrClear, tabIndex: -1, type: "button" }))));
});
