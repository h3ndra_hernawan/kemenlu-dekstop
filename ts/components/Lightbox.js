"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Lightbox = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const moment_1 = __importDefault(require("moment"));
const react_dom_1 = require("react-dom");
const lodash_1 = require("lodash");
const web_1 = require("@react-spring/web");
const GoogleChrome = __importStar(require("../util/GoogleChrome"));
const Attachment_1 = require("../types/Attachment");
const Avatar_1 = require("./Avatar");
const MIME_1 = require("../types/MIME");
const formatDuration_1 = require("../util/formatDuration");
const useRestoreFocus_1 = require("../hooks/useRestoreFocus");
const log = __importStar(require("../logging/log"));
const ZOOM_SCALE = 3;
const INITIAL_IMAGE_TRANSFORM = {
    scale: 1,
    translateX: 0,
    translateY: 0,
    config: {
        clamp: true,
        friction: 20,
        mass: 0.5,
        tension: 350,
    },
};
function Lightbox({ children, close, getConversation, media, i18n, isViewOnce = false, onForward, onSave, selectedIndex: initialSelectedIndex = 0, }) {
    const [root, setRoot] = react_1.default.useState();
    const [selectedIndex, setSelectedIndex] = (0, react_1.useState)(initialSelectedIndex);
    const [videoElement, setVideoElement] = (0, react_1.useState)(null);
    const [videoTime, setVideoTime] = (0, react_1.useState)();
    const [isZoomed, setIsZoomed] = (0, react_1.useState)(false);
    const containerRef = (0, react_1.useRef)(null);
    const [focusRef] = (0, useRestoreFocus_1.useRestoreFocus)();
    const animateRef = (0, react_1.useRef)(null);
    const dragCacheRef = (0, react_1.useRef)();
    const imageRef = (0, react_1.useRef)(null);
    const zoomCacheRef = (0, react_1.useRef)();
    const onPrevious = (0, react_1.useCallback)((event) => {
        event.preventDefault();
        event.stopPropagation();
        if (isZoomed) {
            return;
        }
        setSelectedIndex(prevSelectedIndex => Math.max(prevSelectedIndex - 1, 0));
    }, [isZoomed]);
    const onNext = (0, react_1.useCallback)((event) => {
        event.preventDefault();
        event.stopPropagation();
        if (isZoomed) {
            return;
        }
        setSelectedIndex(prevSelectedIndex => Math.min(prevSelectedIndex + 1, media.length - 1));
    }, [isZoomed, media]);
    const onTimeUpdate = (0, react_1.useCallback)(() => {
        if (!videoElement) {
            return;
        }
        setVideoTime(videoElement.currentTime);
    }, [setVideoTime, videoElement]);
    const handleSave = (event) => {
        event.stopPropagation();
        event.preventDefault();
        const mediaItem = media[selectedIndex];
        const { attachment, message, index } = mediaItem;
        onSave === null || onSave === void 0 ? void 0 : onSave({ attachment, message, index });
    };
    const handleForward = (event) => {
        event.preventDefault();
        event.stopPropagation();
        close();
        const mediaItem = media[selectedIndex];
        onForward === null || onForward === void 0 ? void 0 : onForward(mediaItem.message.id);
    };
    const onKeyDown = (0, react_1.useCallback)((event) => {
        switch (event.key) {
            case 'Escape': {
                close();
                event.preventDefault();
                event.stopPropagation();
                break;
            }
            case 'ArrowLeft':
                onPrevious(event);
                break;
            case 'ArrowRight':
                onNext(event);
                break;
            default:
        }
    }, [close, onNext, onPrevious]);
    const onClose = (event) => {
        event.stopPropagation();
        event.preventDefault();
        close();
    };
    const playVideo = (0, react_1.useCallback)(() => {
        if (!videoElement) {
            return;
        }
        if (videoElement.paused) {
            videoElement.play();
        }
        else {
            videoElement.pause();
        }
    }, [videoElement]);
    (0, react_1.useEffect)(() => {
        const div = document.createElement('div');
        document.body.appendChild(div);
        setRoot(div);
        return () => {
            document.body.removeChild(div);
            setRoot(undefined);
        };
    }, []);
    (0, react_1.useEffect)(() => {
        const useCapture = true;
        document.addEventListener('keydown', onKeyDown, useCapture);
        return () => {
            document.removeEventListener('keydown', onKeyDown, useCapture);
        };
    }, [onKeyDown]);
    const { attachment, contentType, loop = false, objectURL, message, } = media[selectedIndex] || {};
    const isAttachmentGIF = (0, Attachment_1.isGIF)(attachment ? [attachment] : undefined);
    (0, react_1.useEffect)(() => {
        playVideo();
        if (!videoElement || !isViewOnce) {
            return lodash_1.noop;
        }
        if (isAttachmentGIF) {
            return lodash_1.noop;
        }
        videoElement.addEventListener('timeupdate', onTimeUpdate);
        return () => {
            videoElement.removeEventListener('timeupdate', onTimeUpdate);
        };
    }, [isViewOnce, isAttachmentGIF, onTimeUpdate, playVideo, videoElement]);
    const [{ scale, translateX, translateY }, springApi] = (0, web_1.useSpring)(() => INITIAL_IMAGE_TRANSFORM);
    const maxBoundsLimiter = (0, react_1.useCallback)((x, y) => {
        const zoomCache = zoomCacheRef.current;
        if (!zoomCache) {
            return [0, 0];
        }
        const { maxX, maxY } = zoomCache;
        const posX = Math.min(maxX, Math.max(-maxX, x));
        const posY = Math.min(maxY, Math.max(-maxY, y));
        return [posX, posY];
    }, []);
    const positionImage = (0, react_1.useCallback)((ev) => {
        const zoomCache = zoomCacheRef.current;
        if (!zoomCache) {
            return;
        }
        const { screenWidth, screenHeight } = zoomCache;
        const offsetX = screenWidth / 2 - ev.clientX;
        const offsetY = screenHeight / 2 - ev.clientY;
        const posX = offsetX * ZOOM_SCALE;
        const posY = offsetY * ZOOM_SCALE;
        const [x, y] = maxBoundsLimiter(posX, posY);
        springApi.start({
            scale: ZOOM_SCALE,
            translateX: x,
            translateY: y,
        });
    }, [maxBoundsLimiter, springApi]);
    const handleTouchStart = (0, react_1.useCallback)((ev) => {
        const [touch] = ev.touches;
        dragCacheRef.current = {
            startX: touch.clientX,
            startY: touch.clientY,
            translateX: translateX.get(),
            translateY: translateY.get(),
        };
    }, [translateY, translateX]);
    const handleTouchMove = (0, react_1.useCallback)((ev) => {
        const dragCache = dragCacheRef.current;
        if (!dragCache) {
            return;
        }
        const [touch] = ev.touches;
        const deltaX = touch.clientX - dragCache.startX;
        const deltaY = touch.clientY - dragCache.startY;
        const x = dragCache.translateX + deltaX;
        const y = dragCache.translateY + deltaY;
        springApi.start({
            scale: ZOOM_SCALE,
            translateX: x,
            translateY: y,
        });
    }, [springApi]);
    const zoomButtonHandler = (0, react_1.useCallback)((ev) => {
        ev.preventDefault();
        ev.stopPropagation();
        const imageNode = imageRef.current;
        const animateNode = animateRef.current;
        if (!imageNode || !animateNode) {
            return;
        }
        if (!isZoomed) {
            zoomCacheRef.current = {
                maxX: imageNode.offsetWidth,
                maxY: imageNode.offsetHeight,
                screenHeight: window.innerHeight,
                screenWidth: window.innerWidth,
            };
            const { height, left, top, width } = animateNode.getBoundingClientRect();
            const offsetX = ev.clientX - left - width / 2;
            const offsetY = ev.clientY - top - height / 2;
            const posX = -offsetX * ZOOM_SCALE + translateX.get();
            const posY = -offsetY * ZOOM_SCALE + translateY.get();
            const [x, y] = maxBoundsLimiter(posX, posY);
            springApi.start({
                scale: ZOOM_SCALE,
                translateX: x,
                translateY: y,
            });
            setIsZoomed(true);
        }
        else {
            springApi.start(INITIAL_IMAGE_TRANSFORM);
            setIsZoomed(false);
        }
    }, [isZoomed, maxBoundsLimiter, translateX, translateY, springApi]);
    (0, react_1.useEffect)(() => {
        const animateNode = animateRef.current;
        let hasListener = false;
        if (animateNode && isZoomed) {
            hasListener = true;
            document.addEventListener('mousemove', positionImage);
            document.addEventListener('touchmove', handleTouchMove);
            document.addEventListener('touchstart', handleTouchStart);
        }
        return () => {
            if (hasListener) {
                document.removeEventListener('mousemove', positionImage);
                document.removeEventListener('touchmove', handleTouchMove);
                document.removeEventListener('touchstart', handleTouchStart);
            }
        };
    }, [handleTouchMove, handleTouchStart, isZoomed, positionImage]);
    const caption = attachment === null || attachment === void 0 ? void 0 : attachment.caption;
    let content;
    if (!contentType) {
        content = react_1.default.createElement(react_1.default.Fragment, null, children);
    }
    else {
        const isImageTypeSupported = GoogleChrome.isImageTypeSupported(contentType);
        const isVideoTypeSupported = GoogleChrome.isVideoTypeSupported(contentType);
        const isUnsupportedImageType = !isImageTypeSupported && (0, MIME_1.isImage)(contentType);
        const isUnsupportedVideoType = !isVideoTypeSupported && (0, MIME_1.isVideo)(contentType);
        if (isImageTypeSupported) {
            if (objectURL) {
                content = (react_1.default.createElement("button", { className: "Lightbox__zoom-button", onClick: zoomButtonHandler, type: "button" },
                    react_1.default.createElement("img", { alt: i18n('lightboxImageAlt'), className: "Lightbox__object", onContextMenu: (ev) => {
                            // These are the only image types supported by Electron's NativeImage
                            if (ev &&
                                contentType !== MIME_1.IMAGE_PNG &&
                                !/image\/jpe?g/g.test(contentType)) {
                                ev.preventDefault();
                            }
                        }, src: objectURL, ref: imageRef })));
            }
            else {
                content = (react_1.default.createElement("button", { "aria-label": i18n('lightboxImageAlt'), className: (0, classnames_1.default)({
                        Lightbox__object: true,
                        Lightbox__unsupported: true,
                        'Lightbox__unsupported--missing': true,
                    }), onClick: onClose, type: "button" }));
            }
        }
        else if (isVideoTypeSupported) {
            const shouldLoop = loop || isAttachmentGIF || isViewOnce;
            content = (react_1.default.createElement("video", { className: "Lightbox__object", controls: !shouldLoop, key: objectURL, loop: shouldLoop, ref: setVideoElement },
                react_1.default.createElement("source", { src: objectURL })));
        }
        else if (isUnsupportedImageType || isUnsupportedVideoType) {
            content = (react_1.default.createElement("button", { "aria-label": i18n('unsupportedAttachment'), className: (0, classnames_1.default)({
                    Lightbox__object: true,
                    Lightbox__unsupported: true,
                    'Lightbox__unsupported--image': isUnsupportedImageType,
                    'Lightbox__unsupported--video': isUnsupportedVideoType,
                }), onClick: onClose, type: "button" }));
        }
        else {
            log.info('Lightbox: Unexpected content type', { contentType });
            content = (react_1.default.createElement("button", { "aria-label": i18n('unsupportedAttachment'), className: "Lightbox__object Lightbox__unsupported Lightbox__unsupported--file", onClick: onClose, type: "button" }));
        }
    }
    const hasNext = !isZoomed && selectedIndex < media.length - 1;
    const hasPrevious = !isZoomed && selectedIndex > 0;
    return root
        ? (0, react_dom_1.createPortal)(react_1.default.createElement("div", { className: (0, classnames_1.default)('Lightbox Lightbox__container', {
                'Lightbox__container--zoom': isZoomed,
            }), onClick: (event) => {
                event.stopPropagation();
                event.preventDefault();
                close();
            }, onKeyUp: (event) => {
                if ((containerRef && event.target !== containerRef.current) ||
                    event.keyCode !== 27) {
                    return;
                }
                close();
            }, ref: containerRef, role: "presentation" },
            react_1.default.createElement("div", { className: "Lightbox__animated" },
                react_1.default.createElement("div", { className: "Lightbox__main-container", tabIndex: -1, ref: focusRef },
                    react_1.default.createElement("div", { className: "Lightbox__header" },
                        getConversation ? (react_1.default.createElement(LightboxHeader, { getConversation: getConversation, i18n: i18n, message: message })) : (react_1.default.createElement("div", null)),
                        react_1.default.createElement("div", { className: "Lightbox__controls" },
                            onForward ? (react_1.default.createElement("button", { "aria-label": i18n('forwardMessage'), className: "Lightbox__button Lightbox__button--forward", onClick: handleForward, type: "button" })) : null,
                            onSave ? (react_1.default.createElement("button", { "aria-label": i18n('save'), className: "Lightbox__button Lightbox__button--save", onClick: handleSave, type: "button" })) : null,
                            react_1.default.createElement("button", { "aria-label": i18n('close'), className: "Lightbox__button Lightbox__button--close", onClick: close, type: "button" }))),
                    react_1.default.createElement(web_1.animated.div, { className: (0, classnames_1.default)('Lightbox__object--container', {
                            'Lightbox__object--container--zoom': isZoomed,
                        }), ref: animateRef, style: {
                            transform: (0, web_1.to)([scale, translateX, translateY], (s, x, y) => `translate(${x}px, ${y}px) scale(${s})`),
                        } }, content),
                    hasPrevious && (react_1.default.createElement("div", { className: "Lightbox__nav-prev" },
                        react_1.default.createElement("button", { "aria-label": i18n('previous'), className: "Lightbox__button Lightbox__button--previous", onClick: onPrevious, type: "button" }))),
                    hasNext && (react_1.default.createElement("div", { className: "Lightbox__nav-next" },
                        react_1.default.createElement("button", { "aria-label": i18n('next'), className: "Lightbox__button Lightbox__button--next", onClick: onNext, type: "button" })))),
                react_1.default.createElement("div", { className: "Lightbox__footer" },
                    isViewOnce && videoTime ? (react_1.default.createElement("div", { className: "Lightbox__timestamp" }, (0, formatDuration_1.formatDuration)(videoTime))) : null,
                    caption ? (react_1.default.createElement("div", { className: "Lightbox__caption" }, caption)) : null,
                    media.length > 1 && (react_1.default.createElement("div", { className: "Lightbox__thumbnails--container" },
                        react_1.default.createElement("div", { className: "Lightbox__thumbnails", style: {
                                marginLeft: 0 - (selectedIndex * 64 + selectedIndex * 8 + 32),
                            } }, media.map((item, index) => (react_1.default.createElement("button", { className: (0, classnames_1.default)({
                                Lightbox__thumbnail: true,
                                'Lightbox__thumbnail--selected': index === selectedIndex,
                            }), key: item.thumbnailObjectUrl, type: "button", onClick: (event) => {
                                event.stopPropagation();
                                event.preventDefault();
                                setSelectedIndex(index);
                            } }, item.thumbnailObjectUrl ? (react_1.default.createElement("img", { alt: i18n('lightboxImageAlt'), src: item.thumbnailObjectUrl })) : (react_1.default.createElement("div", { className: "Lightbox__thumbnail--unavailable" }))))))))))), root)
        : null;
}
exports.Lightbox = Lightbox;
function LightboxHeader({ getConversation, i18n, message, }) {
    const conversation = getConversation(message.conversationId);
    return (react_1.default.createElement("div", { className: "Lightbox__header--container" },
        react_1.default.createElement("div", { className: "Lightbox__header--avatar" },
            react_1.default.createElement(Avatar_1.Avatar, { acceptedMessageRequest: conversation.acceptedMessageRequest, avatarPath: conversation.avatarPath, color: conversation.color, conversationType: conversation.type, i18n: i18n, isMe: conversation.isMe, name: conversation.name, phoneNumber: conversation.e164, profileName: conversation.profileName, sharedGroupNames: conversation.sharedGroupNames, size: Avatar_1.AvatarSize.THIRTY_TWO, title: conversation.title, unblurredAvatarPath: conversation.unblurredAvatarPath })),
        react_1.default.createElement("div", { className: "Lightbox__header--content" },
            react_1.default.createElement("div", { className: "Lightbox__header--name" }, conversation.title),
            react_1.default.createElement("div", { className: "Lightbox__header--timestamp" }, (0, moment_1.default)(message.received_at_ms).format('L LT')))));
}
