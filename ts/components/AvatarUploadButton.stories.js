"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const AvatarUploadButton_1 = require("./AvatarUploadButton");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    className: overrideProps.className || '',
    i18n,
    onChange: (0, addon_actions_1.action)('onChange'),
});
const story = (0, react_2.storiesOf)('Components/AvatarUploadButton', module);
story.add('Default', () => react_1.default.createElement(AvatarUploadButton_1.AvatarUploadButton, Object.assign({}, createProps())));
