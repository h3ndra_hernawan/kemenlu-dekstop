"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const getFakeBadge_1 = require("../test-both/helpers/getFakeBadge");
const iterables_1 = require("../util/iterables");
const BadgeImageTheme_1 = require("../badges/BadgeImageTheme");
const BadgeDialog_1 = require("./BadgeDialog");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_2.storiesOf)('Components/BadgeDialog', module);
const defaultProps = {
    badges: (0, getFakeBadge_1.getFakeBadges)(3),
    firstName: 'Alice',
    i18n,
    onClose: (0, addon_actions_1.action)('onClose'),
    title: 'Alice Levine',
};
story.add('No badges (closed immediately)', () => (react_1.default.createElement(BadgeDialog_1.BadgeDialog, Object.assign({}, defaultProps, { badges: [] }))));
story.add('One badge', () => (react_1.default.createElement(BadgeDialog_1.BadgeDialog, Object.assign({}, defaultProps, { badges: (0, getFakeBadge_1.getFakeBadges)(1) }))));
story.add('Badge with no image (should be impossible)', () => (react_1.default.createElement(BadgeDialog_1.BadgeDialog, Object.assign({}, defaultProps, { badges: [
        Object.assign(Object.assign({}, (0, getFakeBadge_1.getFakeBadge)()), { images: [] }),
    ] }))));
story.add('Badge with pending image', () => (react_1.default.createElement(BadgeDialog_1.BadgeDialog, Object.assign({}, defaultProps, { badges: [
        Object.assign(Object.assign({}, (0, getFakeBadge_1.getFakeBadge)()), { images: Array(4).fill((0, iterables_1.zipObject)(Object.values(BadgeImageTheme_1.BadgeImageTheme), (0, iterables_1.repeat)({ url: 'https://example.com/ignored.svg' }))) }),
    ] }))));
story.add('Badge with only one, low-detail image', () => (react_1.default.createElement(BadgeDialog_1.BadgeDialog, Object.assign({}, defaultProps, { badges: [
        Object.assign(Object.assign({}, (0, getFakeBadge_1.getFakeBadge)()), { images: [
                (0, iterables_1.zipObject)(Object.values(BadgeImageTheme_1.BadgeImageTheme), (0, iterables_1.repeat)({
                    localPath: '/fixtures/orange-heart.svg',
                    url: 'https://example.com/ignored.svg',
                })),
                ...Array(3).fill((0, iterables_1.zipObject)(Object.values(BadgeImageTheme_1.BadgeImageTheme), (0, iterables_1.repeat)({ url: 'https://example.com/ignored.svg' }))),
            ] }),
    ] }))));
story.add('Five badges', () => (react_1.default.createElement(BadgeDialog_1.BadgeDialog, Object.assign({}, defaultProps, { badges: (0, getFakeBadge_1.getFakeBadges)(5) }))));
story.add('Many badges', () => (react_1.default.createElement(BadgeDialog_1.BadgeDialog, Object.assign({}, defaultProps, { badges: (0, getFakeBadge_1.getFakeBadges)(50) }))));
