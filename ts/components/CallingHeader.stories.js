"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_knobs_1 = require("@storybook/addon-knobs");
const addon_actions_1 = require("@storybook/addon-actions");
const CallingHeader_1 = require("./CallingHeader");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const createProps = (overrideProps = {}) => ({
    i18n,
    isGroupCall: (0, addon_knobs_1.boolean)('isGroupCall', Boolean(overrideProps.isGroupCall)),
    message: overrideProps.message,
    participantCount: (0, addon_knobs_1.number)('participantCount', overrideProps.participantCount || 0),
    showParticipantsList: (0, addon_knobs_1.boolean)('showParticipantsList', Boolean(overrideProps.showParticipantsList)),
    title: overrideProps.title || 'With Someone',
    toggleParticipants: () => (0, addon_actions_1.action)('toggle-participants'),
    togglePip: () => (0, addon_actions_1.action)('toggle-pip'),
    toggleSettings: () => (0, addon_actions_1.action)('toggle-settings'),
});
const story = (0, react_1.storiesOf)('Components/CallingHeader', module);
story.add('Default', () => React.createElement(CallingHeader_1.CallingHeader, Object.assign({}, createProps())));
story.add('Lobby style', () => (React.createElement(CallingHeader_1.CallingHeader, Object.assign({}, createProps(), { title: undefined, togglePip: undefined, onCancel: (0, addon_actions_1.action)('onClose') }))));
story.add('With Participants', () => (React.createElement(CallingHeader_1.CallingHeader, Object.assign({}, createProps({
    isGroupCall: true,
    participantCount: 10,
})))));
story.add('With Participants (shown)', () => (React.createElement(CallingHeader_1.CallingHeader, Object.assign({}, createProps({
    isGroupCall: true,
    participantCount: 10,
    showParticipantsList: true,
})))));
story.add('Long Title', () => (React.createElement(CallingHeader_1.CallingHeader, Object.assign({}, createProps({
    title: 'What do I got to, what do I got to do to wake you up? To shake you up, to break the structure up?',
})))));
story.add('Title with message', () => (React.createElement(CallingHeader_1.CallingHeader, Object.assign({}, createProps({
    title: 'Hello world',
    message: 'Goodbye earth',
})))));
