"use strict";
// Copyright 2015-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DebugLogWindow = void 0;
const react_1 = __importStar(require("react"));
const copy_text_to_clipboard_1 = __importDefault(require("copy-text-to-clipboard"));
const log = __importStar(require("../logging/log"));
const Button_1 = require("./Button");
const Spinner_1 = require("./Spinner");
const ToastDebugLogError_1 = require("./ToastDebugLogError");
const ToastLinkCopied_1 = require("./ToastLinkCopied");
const ToastLoadingFullLogs_1 = require("./ToastLoadingFullLogs");
const openLinkInWebBrowser_1 = require("../util/openLinkInWebBrowser");
const useEscapeHandling_1 = require("../hooks/useEscapeHandling");
var LoadState;
(function (LoadState) {
    LoadState[LoadState["NotStarted"] = 0] = "NotStarted";
    LoadState[LoadState["Started"] = 1] = "Started";
    LoadState[LoadState["Loaded"] = 2] = "Loaded";
    LoadState[LoadState["Submitting"] = 3] = "Submitting";
})(LoadState || (LoadState = {}));
var ToastType;
(function (ToastType) {
    ToastType[ToastType["Copied"] = 0] = "Copied";
    ToastType[ToastType["Error"] = 1] = "Error";
    ToastType[ToastType["Loading"] = 2] = "Loading";
})(ToastType || (ToastType = {}));
const DebugLogWindow = ({ closeWindow, downloadLog, i18n, fetchLogs, uploadLogs, }) => {
    const [loadState, setLoadState] = (0, react_1.useState)(LoadState.NotStarted);
    const [logText, setLogText] = (0, react_1.useState)();
    const [publicLogURL, setPublicLogURL] = (0, react_1.useState)();
    const [textAreaValue, setTextAreaValue] = (0, react_1.useState)(i18n('loading'));
    const [toastType, setToastType] = (0, react_1.useState)();
    (0, useEscapeHandling_1.useEscapeHandling)(closeWindow);
    (0, react_1.useEffect)(() => {
        setLoadState(LoadState.Started);
        let shouldCancel = false;
        async function doFetchLogs() {
            const fetchedLogText = await fetchLogs();
            if (shouldCancel) {
                return;
            }
            setToastType(ToastType.Loading);
            setLogText(fetchedLogText);
            setLoadState(LoadState.Loaded);
            // This number is somewhat arbitrary; we want to show enough that it's
            // clear that we need to scroll, but not so many that things get slow.
            const linesToShow = Math.ceil(Math.min(window.innerHeight, 2000) / 5);
            const value = fetchedLogText.split(/\n/g, linesToShow).join('\n');
            setTextAreaValue(value);
            setToastType(undefined);
        }
        doFetchLogs();
        return () => {
            shouldCancel = true;
        };
    }, [fetchLogs]);
    const handleSubmit = async (ev) => {
        ev.preventDefault();
        const text = logText;
        if (!text || text.length === 0) {
            return;
        }
        setLoadState(LoadState.Submitting);
        try {
            const publishedLogURL = await uploadLogs(text);
            setPublicLogURL(publishedLogURL);
        }
        catch (error) {
            log.error('DebugLogWindow error:', error && error.stack ? error.stack : error);
            setLoadState(LoadState.Loaded);
            setToastType(ToastType.Error);
        }
    };
    function closeToast() {
        setToastType(undefined);
    }
    let toastElement;
    if (toastType === ToastType.Loading) {
        toastElement = react_1.default.createElement(ToastLoadingFullLogs_1.ToastLoadingFullLogs, { i18n: i18n, onClose: closeToast });
    }
    else if (toastType === ToastType.Copied) {
        toastElement = react_1.default.createElement(ToastLinkCopied_1.ToastLinkCopied, { i18n: i18n, onClose: closeToast });
    }
    else if (toastType === ToastType.Error) {
        toastElement = react_1.default.createElement(ToastDebugLogError_1.ToastDebugLogError, { i18n: i18n, onClose: closeToast });
    }
    if (publicLogURL) {
        const copyLog = (ev) => {
            ev.preventDefault();
            (0, copy_text_to_clipboard_1.default)(publicLogURL);
            setToastType(ToastType.Copied);
        };
        return (react_1.default.createElement("div", { className: "DebugLogWindow" },
            react_1.default.createElement("div", null,
                react_1.default.createElement("div", { className: "DebugLogWindow__title" }, i18n('debugLogSuccess')),
                react_1.default.createElement("p", { className: "DebugLogWindow__subtitle" }, i18n('debugLogSuccessNextSteps'))),
            react_1.default.createElement("div", { className: "DebugLogWindow__container" },
                react_1.default.createElement("input", { className: "DebugLogWindow__link", readOnly: true, type: "text", value: publicLogURL })),
            react_1.default.createElement("div", { className: "DebugLogWindow__footer" },
                react_1.default.createElement(Button_1.Button, { onClick: () => {
                        (0, openLinkInWebBrowser_1.openLinkInWebBrowser)('https://support.signal.org/hc/requests/new');
                    }, variant: Button_1.ButtonVariant.Secondary }, i18n('reportIssue')),
                react_1.default.createElement(Button_1.Button, { onClick: copyLog }, i18n('debugLogCopy'))),
            toastElement));
    }
    const canSubmit = Boolean(logText) && loadState !== LoadState.Submitting;
    const canSave = Boolean(logText);
    const isLoading = loadState === LoadState.Started || loadState === LoadState.Submitting;
    return (react_1.default.createElement("div", { className: "DebugLogWindow" },
        react_1.default.createElement("div", null,
            react_1.default.createElement("div", { className: "DebugLogWindow__title" }, i18n('submitDebugLog')),
            react_1.default.createElement("p", { className: "DebugLogWindow__subtitle" }, i18n('debugLogExplanation'))),
        react_1.default.createElement("div", { className: "DebugLogWindow__container" }, isLoading ? (react_1.default.createElement(Spinner_1.Spinner, { svgSize: "normal" })) : (react_1.default.createElement("textarea", { className: "DebugLogWindow__textarea", readOnly: true, rows: 5, spellCheck: false, value: textAreaValue }))),
        react_1.default.createElement("div", { className: "DebugLogWindow__footer" },
            react_1.default.createElement(Button_1.Button, { disabled: !canSave, onClick: () => {
                    if (logText) {
                        downloadLog(logText);
                    }
                }, variant: Button_1.ButtonVariant.Secondary }, i18n('debugLogSave')),
            react_1.default.createElement(Button_1.Button, { disabled: !canSubmit, onClick: handleSubmit }, i18n('submit'))),
        toastElement));
};
exports.DebugLogWindow = DebugLogWindow;
