"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadgeDialog = void 0;
const react_1 = __importStar(require("react"));
const classnames_1 = __importDefault(require("classnames"));
const assert_1 = require("../util/assert");
const BadgeCategory_1 = require("../badges/BadgeCategory");
const Modal_1 = require("./Modal");
const Button_1 = require("./Button");
const BadgeDescription_1 = require("./BadgeDescription");
const BadgeImage_1 = require("./BadgeImage");
const BadgeCarouselIndex_1 = require("./BadgeCarouselIndex");
const BadgeSustainerInstructionsDialog_1 = require("./BadgeSustainerInstructionsDialog");
function BadgeDialog(props) {
    const { badges, i18n, onClose } = props;
    const [isShowingInstructions, setIsShowingInstructions] = (0, react_1.useState)(false);
    const hasBadges = badges.length > 0;
    (0, react_1.useEffect)(() => {
        if (!hasBadges && !isShowingInstructions) {
            onClose();
        }
    }, [hasBadges, isShowingInstructions, onClose]);
    if (isShowingInstructions) {
        return (react_1.default.createElement(BadgeSustainerInstructionsDialog_1.BadgeSustainerInstructionsDialog, { i18n: i18n, onClose: () => setIsShowingInstructions(false) }));
    }
    return hasBadges ? (react_1.default.createElement(BadgeDialogWithBadges, Object.assign({}, props, { onShowInstructions: () => setIsShowingInstructions(true) }))) : null;
}
exports.BadgeDialog = BadgeDialog;
function BadgeDialogWithBadges({ badges, firstName, i18n, onClose, onShowInstructions, title, }) {
    const firstBadge = badges[0];
    (0, assert_1.strictAssert)(firstBadge, '<BadgeDialogWithBadges> got an empty array of badges');
    const [currentBadgeId, setCurrentBadgeId] = (0, react_1.useState)(firstBadge.id);
    let currentBadge;
    let currentBadgeIndex = badges.findIndex(b => b.id === currentBadgeId);
    if (currentBadgeIndex === -1) {
        currentBadgeIndex = 0;
        currentBadge = firstBadge;
    }
    else {
        currentBadge = badges[currentBadgeIndex];
    }
    const setCurrentBadgeIndex = (index) => {
        const newBadge = badges[index];
        (0, assert_1.strictAssert)(newBadge, '<BadgeDialog> tried to select a nonexistent badge');
        setCurrentBadgeId(newBadge.id);
    };
    const navigate = (change) => {
        setCurrentBadgeIndex(currentBadgeIndex + change);
    };
    return (react_1.default.createElement(Modal_1.Modal, { hasXButton: true, moduleClassName: "BadgeDialog", i18n: i18n, onClose: onClose },
        react_1.default.createElement("div", { className: "BadgeDialog__contents" },
            react_1.default.createElement("button", { "aria-label": i18n('previous'), className: "BadgeDialog__nav BadgeDialog__nav--previous", disabled: currentBadgeIndex === 0, onClick: () => navigate(-1), type: "button" }),
            react_1.default.createElement("div", { className: "BadgeDialog__main" },
                react_1.default.createElement(BadgeImage_1.BadgeImage, { badge: currentBadge, size: 160 }),
                react_1.default.createElement("div", { className: "BadgeDialog__name" }, currentBadge.name),
                react_1.default.createElement("div", { className: "BadgeDialog__description" },
                    react_1.default.createElement(BadgeDescription_1.BadgeDescription, { firstName: firstName, template: currentBadge.descriptionTemplate, title: title })),
                react_1.default.createElement(Button_1.Button, { className: (0, classnames_1.default)('BadgeDialog__instructions-button', currentBadge.category !== BadgeCategory_1.BadgeCategory.Donor &&
                        'BadgeDialog__instructions-button--hidden'), onClick: onShowInstructions, size: Button_1.ButtonSize.Large }, i18n('BadgeDialog__become-a-sustainer-button')),
                react_1.default.createElement(BadgeCarouselIndex_1.BadgeCarouselIndex, { currentIndex: currentBadgeIndex, totalCount: badges.length })),
            react_1.default.createElement("button", { "aria-label": i18n('next'), className: "BadgeDialog__nav BadgeDialog__nav--next", disabled: currentBadgeIndex === badges.length - 1, onClick: () => navigate(1), type: "button" }))));
}
