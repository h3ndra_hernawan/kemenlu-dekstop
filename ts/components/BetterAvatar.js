"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BetterAvatar = void 0;
const react_1 = __importStar(require("react"));
const lodash_1 = require("lodash");
const BetterAvatarBubble_1 = require("./BetterAvatarBubble");
const Spinner_1 = require("./Spinner");
const avatarDataToBytes_1 = require("../util/avatarDataToBytes");
const BetterAvatar = ({ avatarData, i18n, isSelected, onClick, onDelete, size = 48, }) => {
    const [avatarBuffer, setAvatarBuffer] = (0, react_1.useState)(avatarData.buffer);
    const [avatarURL, setAvatarURL] = (0, react_1.useState)(undefined);
    (0, react_1.useEffect)(() => {
        let shouldCancel = false;
        async function makeAvatar() {
            const buffer = await (0, avatarDataToBytes_1.avatarDataToBytes)(avatarData);
            if (!shouldCancel) {
                setAvatarBuffer(buffer);
            }
        }
        // If we don't have this we'll get lots of flashing because avatarData
        // changes too much. Once we have a buffer set we don't need to reload.
        if (avatarBuffer) {
            return lodash_1.noop;
        }
        makeAvatar();
        return () => {
            shouldCancel = true;
        };
    }, [avatarBuffer, avatarData]);
    // Convert avatar's Uint8Array to a URL object
    (0, react_1.useEffect)(() => {
        if (avatarBuffer) {
            const url = URL.createObjectURL(new Blob([avatarBuffer]));
            setAvatarURL(url);
        }
    }, [avatarBuffer]);
    // Clean up any remaining object URLs
    (0, react_1.useEffect)(() => {
        return () => {
            if (avatarURL) {
                URL.revokeObjectURL(avatarURL);
            }
        };
    }, [avatarURL]);
    const isEditable = Boolean(avatarData.color);
    const handleDelete = !avatarData.icon
        ? (ev) => {
            ev.preventDefault();
            ev.stopPropagation();
            onDelete();
        }
        : undefined;
    return (react_1.default.createElement(BetterAvatarBubble_1.BetterAvatarBubble, { i18n: i18n, isSelected: isSelected, onDelete: handleDelete, onSelect: () => {
            onClick(avatarBuffer);
        }, style: {
            backgroundImage: avatarURL ? `url(${avatarURL})` : undefined,
            backgroundSize: size,
            // +8 so that the size is the acutal size we want, 8 is the invisible
            // padding around the bubble to make room for the selection border
            height: size + 8,
            width: size + 8,
        } },
        isEditable && isSelected && (react_1.default.createElement("div", { className: "BetterAvatarBubble--editable" })),
        !avatarURL && (react_1.default.createElement("div", { className: "module-Avatar__spinner-container" },
            react_1.default.createElement(Spinner_1.Spinner, { size: `${size - 8}px`, svgSize: "small", direction: "on-avatar" })))));
};
exports.BetterAvatar = BetterAvatar;
