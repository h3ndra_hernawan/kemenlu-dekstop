"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const MIME_1 = require("../types/MIME");
const CompositionArea_1 = require("./CompositionArea");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const fakeAttachment_1 = require("../test-both/helpers/fakeAttachment");
const Fixtures_1 = require("../storybook/Fixtures");
const Util_1 = require("../types/Util");
const audioRecorder_1 = require("../state/ducks/audioRecorder");
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/CompositionArea', module);
// necessary for the add attachment button to render properly
story.addDecorator(storyFn => React.createElement("div", { className: "file-input" }, storyFn()));
const createProps = (overrideProps = {}) => ({
    addAttachment: (0, addon_actions_1.action)('addAttachment'),
    addPendingAttachment: (0, addon_actions_1.action)('addPendingAttachment'),
    conversationId: '123',
    i18n,
    onSendMessage: (0, addon_actions_1.action)('onSendMessage'),
    processAttachments: (0, addon_actions_1.action)('processAttachments'),
    removeAttachment: (0, addon_actions_1.action)('removeAttachment'),
    theme: Util_1.ThemeType.light,
    // AttachmentList
    draftAttachments: overrideProps.draftAttachments || [],
    onClearAttachments: (0, addon_actions_1.action)('onClearAttachments'),
    // AudioCapture
    cancelRecording: (0, addon_actions_1.action)('cancelRecording'),
    completeRecording: (0, addon_actions_1.action)('completeRecording'),
    errorRecording: (0, addon_actions_1.action)('errorRecording'),
    recordingState: (0, addon_knobs_1.select)('recordingState', audioRecorder_1.RecordingState, overrideProps.recordingState || audioRecorder_1.RecordingState.Idle),
    startRecording: (0, addon_actions_1.action)('startRecording'),
    // StagedLinkPreview
    linkPreviewLoading: Boolean(overrideProps.linkPreviewLoading),
    linkPreviewResult: overrideProps.linkPreviewResult,
    onCloseLinkPreview: (0, addon_actions_1.action)('onCloseLinkPreview'),
    // Quote
    quotedMessageProps: overrideProps.quotedMessageProps,
    onClickQuotedMessage: (0, addon_actions_1.action)('onClickQuotedMessage'),
    setQuotedMessage: (0, addon_actions_1.action)('setQuotedMessage'),
    // MediaQualitySelector
    onSelectMediaQuality: (0, addon_actions_1.action)('onSelectMediaQuality'),
    shouldSendHighQualityAttachments: Boolean(overrideProps.shouldSendHighQualityAttachments),
    // CompositionInput
    onEditorStateChange: (0, addon_actions_1.action)('onEditorStateChange'),
    onTextTooLong: (0, addon_actions_1.action)('onTextTooLong'),
    draftText: overrideProps.draftText || undefined,
    clearQuotedMessage: (0, addon_actions_1.action)('clearQuotedMessage'),
    getQuotedMessage: (0, addon_actions_1.action)('getQuotedMessage'),
    scrollToBottom: (0, addon_actions_1.action)('scrollToBottom'),
    sortedGroupMembers: [],
    // EmojiButton
    onPickEmoji: (0, addon_actions_1.action)('onPickEmoji'),
    onSetSkinTone: (0, addon_actions_1.action)('onSetSkinTone'),
    recentEmojis: [],
    skinTone: 1,
    // StickerButton
    knownPacks: overrideProps.knownPacks || [],
    receivedPacks: [],
    installedPacks: [],
    blessedPacks: [],
    recentStickers: [],
    clearInstalledStickerPack: (0, addon_actions_1.action)('clearInstalledStickerPack'),
    onClickAddPack: (0, addon_actions_1.action)('onClickAddPack'),
    onPickSticker: (0, addon_actions_1.action)('onPickSticker'),
    clearShowIntroduction: (0, addon_actions_1.action)('clearShowIntroduction'),
    showPickerHint: false,
    clearShowPickerHint: (0, addon_actions_1.action)('clearShowPickerHint'),
    // Message Requests
    conversationType: 'direct',
    onAccept: (0, addon_actions_1.action)('onAccept'),
    onBlock: (0, addon_actions_1.action)('onBlock'),
    onBlockAndReportSpam: (0, addon_actions_1.action)('onBlockAndReportSpam'),
    onDelete: (0, addon_actions_1.action)('onDelete'),
    onUnblock: (0, addon_actions_1.action)('onUnblock'),
    messageRequestsEnabled: (0, addon_knobs_1.boolean)('messageRequestsEnabled', overrideProps.messageRequestsEnabled || false),
    title: '',
    // GroupV1 Disabled Actions
    onStartGroupMigration: (0, addon_actions_1.action)('onStartGroupMigration'),
    // GroupV2
    announcementsOnly: (0, addon_knobs_1.boolean)('announcementsOnly', Boolean(overrideProps.announcementsOnly)),
    areWeAdmin: (0, addon_knobs_1.boolean)('areWeAdmin', Boolean(overrideProps.areWeAdmin)),
    groupAdmins: [],
    openConversation: (0, addon_actions_1.action)('openConversation'),
    onCancelJoinRequest: (0, addon_actions_1.action)('onCancelJoinRequest'),
    // SMS-only
    isSMSOnly: overrideProps.isSMSOnly || false,
    isFetchingUUID: overrideProps.isFetchingUUID || false,
});
story.add('Default', () => {
    const props = createProps();
    return React.createElement(CompositionArea_1.CompositionArea, Object.assign({}, props));
});
story.add('Starting Text', () => {
    const props = createProps({
        draftText: "here's some starting text",
    });
    return React.createElement(CompositionArea_1.CompositionArea, Object.assign({}, props));
});
story.add('Sticker Button', () => {
    const props = createProps({
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        knownPacks: [{}],
    });
    return React.createElement(CompositionArea_1.CompositionArea, Object.assign({}, props));
});
story.add('Message Request', () => {
    const props = createProps({
        messageRequestsEnabled: true,
    });
    return React.createElement(CompositionArea_1.CompositionArea, Object.assign({}, props));
});
story.add('SMS-only fetching UUID', () => {
    const props = createProps({
        isSMSOnly: true,
        isFetchingUUID: true,
    });
    return React.createElement(CompositionArea_1.CompositionArea, Object.assign({}, props));
});
story.add('SMS-only', () => {
    const props = createProps({
        isSMSOnly: true,
    });
    return React.createElement(CompositionArea_1.CompositionArea, Object.assign({}, props));
});
story.add('Attachments', () => {
    const props = createProps({
        draftAttachments: [
            (0, fakeAttachment_1.fakeDraftAttachment)({
                contentType: MIME_1.IMAGE_JPEG,
                url: Fixtures_1.landscapeGreenUrl,
            }),
        ],
    });
    return React.createElement(CompositionArea_1.CompositionArea, Object.assign({}, props));
});
story.add('Announcements Only group', () => (React.createElement(CompositionArea_1.CompositionArea, Object.assign({}, createProps({
    announcementsOnly: true,
    areWeAdmin: false,
})))));
