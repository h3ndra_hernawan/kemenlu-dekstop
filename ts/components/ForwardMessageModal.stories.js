"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_1 = require("@storybook/react");
const addon_actions_1 = require("@storybook/addon-actions");
const addon_knobs_1 = require("@storybook/addon-knobs");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const ForwardMessageModal_1 = require("./ForwardMessageModal");
const MIME_1 = require("../types/MIME");
const getDefaultConversation_1 = require("../test-both/helpers/getDefaultConversation");
const setupI18n_1 = require("../util/setupI18n");
const StorybookThemeContext_1 = require("../../.storybook/StorybookThemeContext");
const createDraftAttachment = (props = {}) => ({
    pending: false,
    path: 'fileName.jpg',
    contentType: (0, MIME_1.stringToMIMEType)((0, addon_knobs_1.text)('attachment contentType', props.contentType || '')),
    fileName: (0, addon_knobs_1.text)('attachment fileName', props.fileName || ''),
    screenshotPath: props.pending === false ? props.screenshotPath : undefined,
    url: (0, addon_knobs_1.text)('attachment url', props.pending === false ? props.url || '' : ''),
    size: 3433,
});
const story = (0, react_1.storiesOf)('Components/ForwardMessageModal', module);
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const LONG_TITLE = "This is a super-sweet site. And it's got some really amazing content in store for you if you just click that link. Can you click that link for me?";
const LONG_DESCRIPTION = "You're gonna love this description. Not only does it have a lot of characters, but it will also be truncated in the UI. How cool is that??";
const candidateConversations = Array.from(Array(100), () => (0, getDefaultConversation_1.getDefaultConversation)());
const useProps = (overrideProps = {}) => ({
    attachments: overrideProps.attachments,
    conversationId: 'conversation-id',
    candidateConversations,
    doForwardMessage: (0, addon_actions_1.action)('doForwardMessage'),
    i18n,
    isSticker: Boolean(overrideProps.isSticker),
    linkPreview: overrideProps.linkPreview,
    messageBody: (0, addon_knobs_1.text)('messageBody', overrideProps.messageBody || ''),
    onClose: (0, addon_actions_1.action)('onClose'),
    onEditorStateChange: (0, addon_actions_1.action)('onEditorStateChange'),
    onPickEmoji: (0, addon_actions_1.action)('onPickEmoji'),
    onTextTooLong: (0, addon_actions_1.action)('onTextTooLong'),
    onSetSkinTone: (0, addon_actions_1.action)('onSetSkinTone'),
    recentEmojis: [],
    removeLinkPreview: (0, addon_actions_1.action)('removeLinkPreview'),
    skinTone: 0,
    theme: React.useContext(StorybookThemeContext_1.StorybookThemeContext),
});
story.add('Modal', () => {
    return React.createElement(ForwardMessageModal_1.ForwardMessageModal, Object.assign({}, useProps()));
});
story.add('with text', () => {
    return React.createElement(ForwardMessageModal_1.ForwardMessageModal, Object.assign({}, useProps({ messageBody: 'sup' })));
});
story.add('a sticker', () => {
    return React.createElement(ForwardMessageModal_1.ForwardMessageModal, Object.assign({}, useProps({ isSticker: true })));
});
story.add('link preview', () => {
    return (React.createElement(ForwardMessageModal_1.ForwardMessageModal, Object.assign({}, useProps({
        linkPreview: {
            description: LONG_DESCRIPTION,
            date: Date.now(),
            domain: 'https://www.signal.org',
            url: 'signal.org',
            image: createDraftAttachment({
                url: '/fixtures/kitten-4-112-112.jpg',
                contentType: MIME_1.IMAGE_JPEG,
            }),
            isStickerPack: false,
            title: LONG_TITLE,
        },
        messageBody: 'signal.org',
    }))));
});
story.add('media attachments', () => {
    return (React.createElement(ForwardMessageModal_1.ForwardMessageModal, Object.assign({}, useProps({
        attachments: [
            createDraftAttachment({
                pending: true,
            }),
            createDraftAttachment({
                contentType: MIME_1.IMAGE_JPEG,
                fileName: 'tina-rolf-269345-unsplash.jpg',
                url: '/fixtures/tina-rolf-269345-unsplash.jpg',
            }),
            createDraftAttachment({
                contentType: MIME_1.VIDEO_MP4,
                fileName: 'pixabay-Soap-Bubble-7141.mp4',
                url: '/fixtures/pixabay-Soap-Bubble-7141.mp4',
                screenshotPath: '/fixtures/kitten-4-112-112.jpg',
            }),
        ],
        messageBody: 'cats',
    }))));
});
story.add('announcement only groups non-admin', () => (React.createElement(ForwardMessageModal_1.ForwardMessageModal, Object.assign({}, useProps(), { candidateConversations: [
        (0, getDefaultConversation_1.getDefaultConversation)({
            announcementsOnly: true,
            areWeAdmin: false,
        }),
    ] }))));
