"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Alert = void 0;
const react_1 = __importDefault(require("react"));
const Button_1 = require("./Button");
const Modal_1 = require("./Modal");
const Alert = ({ body, i18n, onClose, title, }) => (react_1.default.createElement(Modal_1.Modal, { i18n: i18n, onClose: onClose, title: title },
    body,
    react_1.default.createElement(Modal_1.Modal.ButtonFooter, null,
        react_1.default.createElement(Button_1.Button, { onClick: onClose }, i18n('Confirmation--confirm')))));
exports.Alert = Alert;
