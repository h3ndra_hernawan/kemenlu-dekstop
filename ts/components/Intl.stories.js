"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const addon_knobs_1 = require("@storybook/addon-knobs");
const react_1 = require("@storybook/react");
const Intl_1 = require("./Intl");
const setupI18n_1 = require("../util/setupI18n");
const messages_json_1 = __importDefault(require("../../_locales/en/messages.json"));
const i18n = (0, setupI18n_1.setupI18n)('en', messages_json_1.default);
const story = (0, react_1.storiesOf)('Components/Intl', module);
const createProps = (overrideProps = {}) => ({
    i18n,
    id: (0, addon_knobs_1.text)('id', overrideProps.id || 'deleteAndRestart'),
    components: overrideProps.components,
    renderText: overrideProps.renderText,
});
story.add('No Replacements', () => {
    const props = createProps({
        id: 'deleteAndRestart',
    });
    return React.createElement(Intl_1.Intl, Object.assign({}, props));
});
story.add('Single String Replacement', () => {
    const props = createProps({
        id: 'leftTheGroup',
        components: ['Theodora'],
    });
    return React.createElement(Intl_1.Intl, Object.assign({}, props));
});
story.add('Single Tag Replacement', () => {
    const props = createProps({
        id: 'leftTheGroup',
        components: [
            React.createElement("button", { type: "button", key: "a-button" }, "Theodora"),
        ],
    });
    return React.createElement(Intl_1.Intl, Object.assign({}, props));
});
story.add('Multiple String Replacement', () => {
    const props = createProps({
        id: 'changedRightAfterVerify',
        components: {
            name1: 'Fred',
            name2: 'The Fredster',
        },
    });
    return React.createElement(Intl_1.Intl, Object.assign({}, props));
});
story.add('Multiple Tag Replacement', () => {
    const props = createProps({
        id: 'changedRightAfterVerify',
        components: {
            name1: React.createElement("b", null, "Fred"),
            name2: React.createElement("b", null, "The Fredster"),
        },
    });
    return React.createElement(Intl_1.Intl, Object.assign({}, props));
});
story.add('Custom Render', () => {
    const props = createProps({
        id: 'deleteAndRestart',
        renderText: ({ text: theText, key }) => (React.createElement("div", { style: { backgroundColor: 'purple', color: 'orange' }, key: key }, theText)),
    });
    return React.createElement(Intl_1.Intl, Object.assign({}, props));
});
