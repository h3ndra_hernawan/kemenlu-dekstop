"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvatarEditor = void 0;
const react_1 = __importStar(require("react"));
const AvatarIconEditor_1 = require("./AvatarIconEditor");
const AvatarModalButtons_1 = require("./AvatarModalButtons");
const AvatarPreview_1 = require("./AvatarPreview");
const AvatarTextEditor_1 = require("./AvatarTextEditor");
const AvatarUploadButton_1 = require("./AvatarUploadButton");
const BetterAvatar_1 = require("./BetterAvatar");
const avatarDataToBytes_1 = require("../util/avatarDataToBytes");
const createAvatarData_1 = require("../util/createAvatarData");
const isSameAvatarData_1 = require("../util/isSameAvatarData");
const missingCaseError_1 = require("../util/missingCaseError");
var EditMode;
(function (EditMode) {
    EditMode["Main"] = "Main";
    EditMode["Custom"] = "Custom";
    EditMode["Text"] = "Text";
})(EditMode || (EditMode = {}));
const AvatarEditor = ({ avatarColor, avatarPath, avatarValue, conversationId, conversationTitle, deleteAvatarFromDisk, i18n, isGroup, onCancel, onSave, userAvatarData, replaceAvatar, saveAvatarToDisk, }) => {
    const [provisionalSelectedAvatar, setProvisionalSelectedAvatar] = (0, react_1.useState)();
    const [avatarPreview, setAvatarPreview] = (0, react_1.useState)(avatarValue);
    const [initialAvatar, setInitialAvatar] = (0, react_1.useState)(avatarValue);
    const [localAvatarData, setLocalAvatarData] = (0, react_1.useState)(userAvatarData.slice());
    const [editMode, setEditMode] = (0, react_1.useState)(EditMode.Main);
    const getSelectedAvatar = (0, react_1.useCallback)(avatarToFind => localAvatarData.find(avatarData => (0, isSameAvatarData_1.isSameAvatarData)(avatarData, avatarToFind)), [localAvatarData]);
    const selectedAvatar = getSelectedAvatar(provisionalSelectedAvatar);
    // Caching the Uint8Array produced into avatarData as buffer because
    // that function is a little expensive to run and so we don't flicker the UI.
    (0, react_1.useEffect)(() => {
        let shouldCancel = false;
        async function cacheAvatars() {
            const newAvatarData = await Promise.all(userAvatarData.map(async (avatarData) => {
                if (avatarData.buffer) {
                    return avatarData;
                }
                const buffer = await (0, avatarDataToBytes_1.avatarDataToBytes)(avatarData);
                return Object.assign(Object.assign({}, avatarData), { buffer });
            }));
            if (!shouldCancel) {
                setLocalAvatarData(newAvatarData);
            }
        }
        cacheAvatars();
        return () => {
            shouldCancel = true;
        };
    }, [setLocalAvatarData, userAvatarData]);
    // This function optimistcally updates userAvatarData so we don't have to
    // wait for saveAvatarToDisk to finish before displaying something to the
    // user. As a bonus the component fully works in storybook!
    const updateAvatarDataList = (0, react_1.useCallback)((newAvatarData, staleAvatarData) => {
        const existingAvatarData = staleAvatarData
            ? localAvatarData.filter(avatarData => avatarData !== staleAvatarData)
            : localAvatarData;
        if (newAvatarData) {
            setAvatarPreview(newAvatarData.buffer);
            setLocalAvatarData([newAvatarData, ...existingAvatarData]);
            setProvisionalSelectedAvatar(newAvatarData);
        }
        else {
            setLocalAvatarData(existingAvatarData);
            if ((0, isSameAvatarData_1.isSameAvatarData)(selectedAvatar, staleAvatarData)) {
                setAvatarPreview(undefined);
                setProvisionalSelectedAvatar(undefined);
            }
        }
    }, [
        localAvatarData,
        selectedAvatar,
        setAvatarPreview,
        setLocalAvatarData,
        setProvisionalSelectedAvatar,
    ]);
    const handleAvatarLoaded = (0, react_1.useCallback)(avatarBuffer => {
        setAvatarPreview(avatarBuffer);
        setInitialAvatar(avatarBuffer);
    }, []);
    const hasChanges = initialAvatar !== avatarPreview;
    let content;
    if (editMode === EditMode.Main) {
        content = (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("div", { className: "AvatarEditor__preview" },
                react_1.default.createElement(AvatarPreview_1.AvatarPreview, { avatarColor: avatarColor, avatarPath: avatarPath, avatarValue: avatarPreview, conversationTitle: conversationTitle, i18n: i18n, isGroup: isGroup, onAvatarLoaded: handleAvatarLoaded, onClear: () => {
                        setAvatarPreview(undefined);
                        setProvisionalSelectedAvatar(undefined);
                    } }),
                react_1.default.createElement("div", { className: "AvatarEditor__top-buttons" },
                    react_1.default.createElement(AvatarUploadButton_1.AvatarUploadButton, { className: "AvatarEditor__button AvatarEditor__button--photo", i18n: i18n, onChange: newAvatar => {
                            const avatarData = (0, createAvatarData_1.createAvatarData)({
                                buffer: newAvatar,
                                // This is so that the newly created avatar gets an X
                                imagePath: 'TMP',
                            });
                            saveAvatarToDisk(avatarData, conversationId);
                            updateAvatarDataList(avatarData);
                        } }),
                    react_1.default.createElement("button", { className: "AvatarEditor__button AvatarEditor__button--text", onClick: () => {
                            setProvisionalSelectedAvatar(undefined);
                            setEditMode(EditMode.Text);
                        }, type: "button" }, i18n('text')))),
            react_1.default.createElement("hr", { className: "AvatarEditor__divider" }),
            react_1.default.createElement("div", { className: "AvatarEditor__avatar-selector-title" }, i18n('AvatarEditor--choose')),
            react_1.default.createElement("div", { className: "AvatarEditor__avatars" }, localAvatarData.map(avatarData => (react_1.default.createElement(BetterAvatar_1.BetterAvatar, { avatarData: avatarData, key: avatarData.id, i18n: i18n, isSelected: (0, isSameAvatarData_1.isSameAvatarData)(avatarData, selectedAvatar), onClick: avatarBuffer => {
                    if ((0, isSameAvatarData_1.isSameAvatarData)(avatarData, selectedAvatar)) {
                        if (avatarData.text) {
                            setEditMode(EditMode.Text);
                        }
                        else if (avatarData.icon) {
                            setEditMode(EditMode.Custom);
                        }
                    }
                    else {
                        setAvatarPreview(avatarBuffer);
                        setProvisionalSelectedAvatar(avatarData);
                    }
                }, onDelete: () => {
                    updateAvatarDataList(undefined, avatarData);
                    deleteAvatarFromDisk(avatarData, conversationId);
                } })))),
            react_1.default.createElement(AvatarModalButtons_1.AvatarModalButtons, { hasChanges: hasChanges, i18n: i18n, onCancel: onCancel, onSave: () => {
                    if (selectedAvatar) {
                        replaceAvatar(selectedAvatar, selectedAvatar, conversationId);
                    }
                    onSave(avatarPreview);
                } })));
    }
    else if (editMode === EditMode.Text) {
        content = (react_1.default.createElement(AvatarTextEditor_1.AvatarTextEditor, { avatarData: selectedAvatar, i18n: i18n, onCancel: () => {
                setEditMode(EditMode.Main);
                if (selectedAvatar) {
                    return;
                }
                // The selected avatar was cleared when we entered text mode so we
                // need to find if one is actually selected if it matches the current
                // preview.
                const actualAvatarSelected = localAvatarData.find(avatarData => avatarData.buffer === avatarPreview);
                if (actualAvatarSelected) {
                    setProvisionalSelectedAvatar(actualAvatarSelected);
                }
            }, onDone: (avatarBuffer, avatarData) => {
                const newAvatarData = Object.assign(Object.assign({}, avatarData), { buffer: avatarBuffer });
                updateAvatarDataList(newAvatarData, selectedAvatar);
                setEditMode(EditMode.Main);
                replaceAvatar(newAvatarData, selectedAvatar, conversationId);
            } }));
    }
    else if (editMode === EditMode.Custom) {
        if (!selectedAvatar) {
            throw new Error('No selected avatar and editMode is custom');
        }
        content = (react_1.default.createElement(AvatarIconEditor_1.AvatarIconEditor, { avatarData: selectedAvatar, i18n: i18n, onClose: avatarData => {
                if (avatarData) {
                    updateAvatarDataList(avatarData, selectedAvatar);
                    replaceAvatar(avatarData, selectedAvatar, conversationId);
                }
                setEditMode(EditMode.Main);
            } }));
    }
    else {
        throw (0, missingCaseError_1.missingCaseError)(editMode);
    }
    return react_1.default.createElement("div", { className: "AvatarEditor" }, content);
};
exports.AvatarEditor = AvatarEditor;
