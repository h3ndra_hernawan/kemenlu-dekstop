"use strict";
// Copyright 2015-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Bytes = __importStar(require("../Bytes"));
const Curve = __importStar(require("../Curve"));
const Crypto_1 = require("../Crypto");
describe('Crypto', () => {
    describe('encrypting and decrypting profile data', () => {
        const NAME_PADDED_LENGTH = 53;
        describe('encrypting and decrypting profile names', () => {
            it('pads, encrypts, decrypts, and unpads a short string', () => {
                const name = 'Alice';
                const buffer = Bytes.fromString(name);
                const key = (0, Crypto_1.getRandomBytes)(32);
                const encrypted = (0, Crypto_1.encryptProfileItemWithPadding)(buffer, key, Crypto_1.PaddedLengths.Name);
                chai_1.assert.equal(encrypted.byteLength, NAME_PADDED_LENGTH + 16 + 12);
                const { given, family } = (0, Crypto_1.decryptProfileName)(Bytes.toBase64(encrypted), key);
                chai_1.assert.strictEqual(family, null);
                chai_1.assert.strictEqual(Bytes.toString(given), name);
            });
            it('handles a given name of the max, 53 characters', () => {
                const name = '01234567890123456789012345678901234567890123456789123';
                const buffer = Bytes.fromString(name);
                const key = (0, Crypto_1.getRandomBytes)(32);
                const encrypted = (0, Crypto_1.encryptProfileItemWithPadding)(buffer, key, Crypto_1.PaddedLengths.Name);
                chai_1.assert.equal(encrypted.byteLength, NAME_PADDED_LENGTH + 16 + 12);
                const { given, family } = (0, Crypto_1.decryptProfileName)(Bytes.toBase64(encrypted), key);
                chai_1.assert.strictEqual(Bytes.toString(given), name);
                chai_1.assert.strictEqual(family, null);
            });
            it('handles family/given name of the max, 53 characters', () => {
                const name = '01234567890123456789\u000001234567890123456789012345678912';
                const buffer = Bytes.fromString(name);
                const key = (0, Crypto_1.getRandomBytes)(32);
                const encrypted = (0, Crypto_1.encryptProfileItemWithPadding)(buffer, key, Crypto_1.PaddedLengths.Name);
                chai_1.assert.equal(encrypted.byteLength, NAME_PADDED_LENGTH + 16 + 12);
                const { given, family } = (0, Crypto_1.decryptProfileName)(Bytes.toBase64(encrypted), key);
                chai_1.assert.strictEqual(Bytes.toString(given), '01234567890123456789');
                chai_1.assert.strictEqual(family && Bytes.toString(family), '01234567890123456789012345678912');
            });
            it('handles a string with family/given name', () => {
                const name = 'Alice\0Jones';
                const buffer = Bytes.fromString(name);
                const key = (0, Crypto_1.getRandomBytes)(32);
                const encrypted = (0, Crypto_1.encryptProfileItemWithPadding)(buffer, key, Crypto_1.PaddedLengths.Name);
                chai_1.assert.equal(encrypted.byteLength, NAME_PADDED_LENGTH + 16 + 12);
                const { given, family } = (0, Crypto_1.decryptProfileName)(Bytes.toBase64(encrypted), key);
                chai_1.assert.strictEqual(Bytes.toString(given), 'Alice');
                chai_1.assert.strictEqual(family && Bytes.toString(family), 'Jones');
            });
            it('works for empty string', () => {
                const name = Bytes.fromString('');
                const key = (0, Crypto_1.getRandomBytes)(32);
                const encrypted = (0, Crypto_1.encryptProfileItemWithPadding)(name, key, Crypto_1.PaddedLengths.Name);
                chai_1.assert.equal(encrypted.byteLength, NAME_PADDED_LENGTH + 16 + 12);
                const { given, family } = (0, Crypto_1.decryptProfileName)(Bytes.toBase64(encrypted), key);
                chai_1.assert.strictEqual(family, null);
                chai_1.assert.strictEqual(given.byteLength, 0);
                chai_1.assert.strictEqual(Bytes.toString(given), '');
            });
        });
        describe('encrypting and decrypting profile avatars', () => {
            it('encrypts and decrypts', async () => {
                const buffer = Bytes.fromString('This is an avatar');
                const key = (0, Crypto_1.getRandomBytes)(32);
                const encrypted = (0, Crypto_1.encryptProfile)(buffer, key);
                (0, chai_1.assert)(encrypted.byteLength === buffer.byteLength + 16 + 12);
                const decrypted = (0, Crypto_1.decryptProfile)(encrypted, key);
                (0, chai_1.assert)((0, Crypto_1.constantTimeEqual)(buffer, decrypted));
            });
            it('throws when decrypting with the wrong key', () => {
                const buffer = Bytes.fromString('This is an avatar');
                const key = (0, Crypto_1.getRandomBytes)(32);
                const badKey = (0, Crypto_1.getRandomBytes)(32);
                const encrypted = (0, Crypto_1.encryptProfile)(buffer, key);
                (0, chai_1.assert)(encrypted.byteLength === buffer.byteLength + 16 + 12);
                chai_1.assert.throws(() => (0, Crypto_1.decryptProfile)(encrypted, badKey), 'Failed to decrypt profile data. Most likely the profile key has changed.');
            });
        });
    });
    describe('generateRegistrationId', () => {
        it('generates an integer between 0 and 16383 (inclusive)', () => {
            for (let i = 0; i < 100; i += 1) {
                const id = (0, Crypto_1.generateRegistrationId)();
                chai_1.assert.isAtLeast(id, 0);
                chai_1.assert.isAtMost(id, 16383);
                (0, chai_1.assert)(Number.isInteger(id));
            }
        });
    });
    describe('deriveSecrets', () => {
        it('derives key parts via HKDF', () => {
            const input = (0, Crypto_1.getRandomBytes)(32);
            const salt = (0, Crypto_1.getRandomBytes)(32);
            const info = Bytes.fromString('Hello world');
            const result = (0, Crypto_1.deriveSecrets)(input, salt, info);
            chai_1.assert.lengthOf(result, 3);
            result.forEach(part => {
                // This is a smoke test; HKDF is tested as part of @signalapp/signal-client.
                chai_1.assert.instanceOf(part, Uint8Array);
                chai_1.assert.strictEqual(part.byteLength, 32);
            });
        });
    });
    describe('accessKey/profileKey', () => {
        it('verification roundtrips', () => {
            const profileKey = (0, Crypto_1.getRandomBytes)(32);
            const accessKey = (0, Crypto_1.deriveAccessKey)(profileKey);
            const verifier = (0, Crypto_1.getAccessKeyVerifier)(accessKey);
            const correct = (0, Crypto_1.verifyAccessKey)(accessKey, verifier);
            chai_1.assert.strictEqual(correct, true);
        });
    });
    describe('deriveMasterKeyFromGroupV1', () => {
        const vectors = [
            {
                gv1: '00000000000000000000000000000000',
                masterKey: 'dbde68f4ee9169081f8814eabc65523fea1359235c8cfca32b69e31dce58b039',
            },
            {
                gv1: '000102030405060708090a0b0c0d0e0f',
                masterKey: '70884f78f07a94480ee36b67a4b5e975e92e4a774561e3df84c9076e3be4b9bf',
            },
            {
                gv1: '7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f',
                masterKey: 'e69bf7c183b288b4ea5745b7c52b651a61e57769fafde683a6fdf1240f1905f2',
            },
            {
                gv1: 'ffffffffffffffffffffffffffffffff',
                masterKey: 'dd3a7de23d10f18b64457fbeedc76226c112a730e4b76112e62c36c4432eb37d',
            },
        ];
        vectors.forEach((vector, index) => {
            it(`vector ${index}`, () => {
                const gv1 = Bytes.fromHex(vector.gv1);
                const expectedHex = vector.masterKey;
                const actual = (0, Crypto_1.deriveMasterKeyFromGroupV1)(gv1);
                const actualHex = Bytes.toHex(actual);
                chai_1.assert.strictEqual(actualHex, expectedHex);
            });
        });
    });
    describe('symmetric encryption', () => {
        it('roundtrips', () => {
            const message = 'this is my message';
            const plaintext = Bytes.fromString(message);
            const key = (0, Crypto_1.getRandomBytes)(32);
            const encrypted = (0, Crypto_1.encryptSymmetric)(key, plaintext);
            const decrypted = (0, Crypto_1.decryptSymmetric)(key, encrypted);
            const equal = (0, Crypto_1.constantTimeEqual)(plaintext, decrypted);
            if (!equal) {
                throw new Error('The output and input did not match!');
            }
        });
        it('roundtrip fails if nonce is modified', () => {
            const message = 'this is my message';
            const plaintext = Bytes.fromString(message);
            const key = (0, Crypto_1.getRandomBytes)(32);
            const encrypted = (0, Crypto_1.encryptSymmetric)(key, plaintext);
            encrypted[2] += 2;
            try {
                (0, Crypto_1.decryptSymmetric)(key, encrypted);
            }
            catch (error) {
                chai_1.assert.strictEqual(error.message, 'decryptSymmetric: Failed to decrypt; MAC verification failed');
                return;
            }
            throw new Error('Expected error to be thrown');
        });
        it('roundtrip fails if mac is modified', () => {
            const message = 'this is my message';
            const plaintext = Bytes.fromString(message);
            const key = (0, Crypto_1.getRandomBytes)(32);
            const encrypted = (0, Crypto_1.encryptSymmetric)(key, plaintext);
            encrypted[encrypted.length - 3] += 2;
            try {
                (0, Crypto_1.decryptSymmetric)(key, encrypted);
            }
            catch (error) {
                chai_1.assert.strictEqual(error.message, 'decryptSymmetric: Failed to decrypt; MAC verification failed');
                return;
            }
            throw new Error('Expected error to be thrown');
        });
        it('roundtrip fails if encrypted contents are modified', () => {
            const message = 'this is my message';
            const plaintext = Bytes.fromString(message);
            const key = (0, Crypto_1.getRandomBytes)(32);
            const encrypted = (0, Crypto_1.encryptSymmetric)(key, plaintext);
            encrypted[35] += 9;
            try {
                (0, Crypto_1.decryptSymmetric)(key, encrypted);
            }
            catch (error) {
                chai_1.assert.strictEqual(error.message, 'decryptSymmetric: Failed to decrypt; MAC verification failed');
                return;
            }
            throw new Error('Expected error to be thrown');
        });
    });
    describe('encrypted device name', () => {
        it('roundtrips', () => {
            const deviceName = 'v1.19.0 on Windows 10';
            const identityKey = Curve.generateKeyPair();
            const encrypted = (0, Crypto_1.encryptDeviceName)(deviceName, identityKey.pubKey);
            const decrypted = (0, Crypto_1.decryptDeviceName)(encrypted, identityKey.privKey);
            chai_1.assert.strictEqual(decrypted, deviceName);
        });
        it('fails if iv is changed', () => {
            const deviceName = 'v1.19.0 on Windows 10';
            const identityKey = Curve.generateKeyPair();
            const encrypted = (0, Crypto_1.encryptDeviceName)(deviceName, identityKey.pubKey);
            encrypted.syntheticIv = (0, Crypto_1.getRandomBytes)(16);
            try {
                (0, Crypto_1.decryptDeviceName)(encrypted, identityKey.privKey);
            }
            catch (error) {
                chai_1.assert.strictEqual(error.message, 'decryptDeviceName: synthetic IV did not match');
            }
        });
    });
    describe('verifyHmacSha256', () => {
        it('rejects if their MAC is too short', () => {
            const key = (0, Crypto_1.getRandomBytes)(32);
            const plaintext = Bytes.fromString('Hello world');
            const ourMac = (0, Crypto_1.hmacSha256)(key, plaintext);
            const theirMac = ourMac.slice(0, -1);
            let error;
            try {
                (0, Crypto_1.verifyHmacSha256)(plaintext, key, theirMac, ourMac.byteLength);
            }
            catch (err) {
                error = err;
            }
            chai_1.assert.instanceOf(error, Error);
            chai_1.assert.strictEqual(error.message, 'Bad MAC length');
        });
        it('rejects if their MAC is too long', () => {
            const key = (0, Crypto_1.getRandomBytes)(32);
            const plaintext = Bytes.fromString('Hello world');
            const ourMac = (0, Crypto_1.hmacSha256)(key, plaintext);
            const theirMac = Bytes.concatenate([ourMac, new Uint8Array([0xff])]);
            let error;
            try {
                (0, Crypto_1.verifyHmacSha256)(plaintext, key, theirMac, ourMac.byteLength);
            }
            catch (err) {
                error = err;
            }
            chai_1.assert.instanceOf(error, Error);
            chai_1.assert.strictEqual(error.message, 'Bad MAC length');
        });
        it('rejects if our MAC is shorter than the specified length', () => {
            const key = (0, Crypto_1.getRandomBytes)(32);
            const plaintext = Bytes.fromString('Hello world');
            const ourMac = (0, Crypto_1.hmacSha256)(key, plaintext);
            const theirMac = ourMac;
            let error;
            try {
                (0, Crypto_1.verifyHmacSha256)(plaintext, key, theirMac, ourMac.byteLength + 1);
            }
            catch (err) {
                error = err;
            }
            chai_1.assert.instanceOf(error, Error);
            chai_1.assert.strictEqual(error.message, 'Bad MAC length');
        });
        it("rejects if the MACs don't match", () => {
            const plaintext = Bytes.fromString('Hello world');
            const ourKey = (0, Crypto_1.getRandomBytes)(32);
            const ourMac = (0, Crypto_1.hmacSha256)(ourKey, plaintext);
            const theirKey = (0, Crypto_1.getRandomBytes)(32);
            const theirMac = (0, Crypto_1.hmacSha256)(theirKey, plaintext);
            let error;
            try {
                (0, Crypto_1.verifyHmacSha256)(plaintext, ourKey, theirMac, ourMac.byteLength);
            }
            catch (err) {
                error = err;
            }
            chai_1.assert.instanceOf(error, Error);
            chai_1.assert.strictEqual(error.message, 'Bad MAC');
        });
        it('resolves with undefined if the MACs match exactly', () => {
            const key = (0, Crypto_1.getRandomBytes)(32);
            const plaintext = Bytes.fromString('Hello world');
            const theirMac = (0, Crypto_1.hmacSha256)(key, plaintext);
            const result = (0, Crypto_1.verifyHmacSha256)(plaintext, key, theirMac, theirMac.byteLength);
            chai_1.assert.isUndefined(result);
        });
        it('resolves with undefined if the first `length` bytes of the MACs match', () => {
            const key = (0, Crypto_1.getRandomBytes)(32);
            const plaintext = Bytes.fromString('Hello world');
            const theirMac = (0, Crypto_1.hmacSha256)(key, plaintext).slice(0, -5);
            const result = (0, Crypto_1.verifyHmacSha256)(plaintext, key, theirMac, theirMac.byteLength);
            chai_1.assert.isUndefined(result);
        });
    });
    describe('uuidToBytes', () => {
        it('converts valid UUIDs to Uint8Arrays', () => {
            const expectedResult = new Uint8Array([
                0x22, 0x6e, 0x44, 0x02, 0x7f, 0xfc, 0x45, 0x43, 0x85, 0xc9, 0x46, 0x22,
                0xc5, 0x0a, 0x5b, 0x14,
            ]);
            chai_1.assert.deepEqual((0, Crypto_1.uuidToBytes)('226e4402-7ffc-4543-85c9-4622c50a5b14'), expectedResult);
            chai_1.assert.deepEqual((0, Crypto_1.uuidToBytes)('226E4402-7FFC-4543-85C9-4622C50A5B14'), expectedResult);
        });
        it('returns an empty Uint8Array for strings of the wrong length', () => {
            chai_1.assert.deepEqual((0, Crypto_1.uuidToBytes)(''), new Uint8Array(0));
            chai_1.assert.deepEqual((0, Crypto_1.uuidToBytes)('abc'), new Uint8Array(0));
            chai_1.assert.deepEqual((0, Crypto_1.uuidToBytes)('032deadf0d5e4ee78da28e75b1dfb284'), new Uint8Array(0));
            chai_1.assert.deepEqual((0, Crypto_1.uuidToBytes)('deaed5eb-d983-456a-a954-9ad7a006b271aaaaaaaaaa'), new Uint8Array(0));
        });
    });
    describe('bytesToUuid', () => {
        it('converts valid Uint8Arrays to UUID strings', () => {
            const buf = new Uint8Array([
                0x22, 0x6e, 0x44, 0x02, 0x7f, 0xfc, 0x45, 0x43, 0x85, 0xc9, 0x46, 0x22,
                0xc5, 0x0a, 0x5b, 0x14,
            ]);
            chai_1.assert.deepEqual((0, Crypto_1.bytesToUuid)(buf), '226e4402-7ffc-4543-85c9-4622c50a5b14');
        });
        it('returns undefined if passed an all-zero buffer', () => {
            chai_1.assert.isUndefined((0, Crypto_1.bytesToUuid)(new Uint8Array(16)));
        });
        it('returns undefined if passed the wrong number of bytes', () => {
            chai_1.assert.isUndefined((0, Crypto_1.bytesToUuid)(new Uint8Array(0)));
            chai_1.assert.isUndefined((0, Crypto_1.bytesToUuid)(new Uint8Array([0x22])));
            chai_1.assert.isUndefined((0, Crypto_1.bytesToUuid)(new Uint8Array(Array(17).fill(0x22))));
        });
    });
});
