"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const lodash_1 = require("lodash");
const conversations_1 = require("../models/conversations");
const UUID_1 = require("../types/UUID");
const routineProfileRefresh_1 = require("../routineProfileRefresh");
const getProfileStub = __importStar(require("../util/getProfile"));
describe('routineProfileRefresh', () => {
    let sinonSandbox;
    beforeEach(() => {
        sinonSandbox = sinon.createSandbox();
        sinonSandbox.stub(getProfileStub, 'getProfile').resolves(undefined);
    });
    afterEach(() => {
        sinonSandbox.restore();
    });
    function makeConversation(overrideAttributes = {}) {
        const result = new conversations_1.ConversationModel(Object.assign({ accessKey: UUID_1.UUID.generate().toString(), active_at: Date.now(), draftAttachments: [], draftBodyRanges: [], draftTimestamp: null, id: UUID_1.UUID.generate().toString(), inbox_position: 0, isPinned: false, lastMessageDeletedForEveryone: false, lastMessageStatus: 'sent', left: false, markedUnread: false, messageCount: 2, messageCountBeforeMessageRequests: 0, messageRequestResponseType: 0, muteExpiresAt: 0, profileAvatar: undefined, profileKeyCredential: UUID_1.UUID.generate().toString(), profileKeyVersion: '', profileSharing: true, quotedMessageId: null, sealedSender: 1, sentMessageCount: 1, sharedGroupNames: [], timestamp: Date.now(), type: 'private', uuid: UUID_1.UUID.generate().toString(), version: 2 }, overrideAttributes));
        return result;
    }
    function makeGroup(groupMembers) {
        const result = makeConversation({ type: 'group' });
        // This is easier than setting up all of the scaffolding for `getMembers`.
        sinonSandbox.stub(result, 'getMembers').returns(groupMembers);
        return result;
    }
    function makeStorage(lastAttemptAt) {
        return {
            get: sinonSandbox
                .stub()
                .withArgs('lastAttemptedToRefreshProfilesAt')
                .returns(lastAttemptAt),
            put: sinonSandbox.stub().resolves(undefined),
        };
    }
    it('does nothing when the last refresh time is less than 12 hours ago', async () => {
        const conversation1 = makeConversation();
        const conversation2 = makeConversation();
        const storage = makeStorage(Date.now() - 1234);
        await (0, routineProfileRefresh_1.routineProfileRefresh)({
            allConversations: [conversation1, conversation2],
            ourConversationId: UUID_1.UUID.generate().toString(),
            storage,
        });
        sinon.assert.notCalled(getProfileStub.getProfile);
        sinon.assert.notCalled(storage.put);
    });
    it('asks conversations to get their profiles', async () => {
        const conversation1 = makeConversation();
        const conversation2 = makeConversation();
        await (0, routineProfileRefresh_1.routineProfileRefresh)({
            allConversations: [conversation1, conversation2],
            ourConversationId: UUID_1.UUID.generate().toString(),
            storage: makeStorage(),
        });
        sinon.assert.calledWith(getProfileStub.getProfile, conversation1.get('uuid'), conversation1.get('e164'));
        sinon.assert.calledWith(getProfileStub.getProfile, conversation2.get('uuid'), conversation2.get('e164'));
    });
    it("skips conversations that haven't been active in 30 days", async () => {
        const recentlyActive = makeConversation();
        const inactive = makeConversation({
            active_at: Date.now() - 31 * 24 * 60 * 60 * 1000,
        });
        const neverActive = makeConversation({ active_at: undefined });
        await (0, routineProfileRefresh_1.routineProfileRefresh)({
            allConversations: [recentlyActive, inactive, neverActive],
            ourConversationId: UUID_1.UUID.generate().toString(),
            storage: makeStorage(),
        });
        sinon.assert.calledOnce(getProfileStub.getProfile);
        sinon.assert.calledWith(getProfileStub.getProfile, recentlyActive.get('uuid'), recentlyActive.get('e164'));
        sinon.assert.neverCalledWith(getProfileStub.getProfile, inactive.get('uuid'), inactive.get('e164'));
        sinon.assert.neverCalledWith(getProfileStub.getProfile, neverActive.get('uuid'), neverActive.get('e164'));
    });
    it('skips your own conversation', async () => {
        const notMe = makeConversation();
        const me = makeConversation();
        await (0, routineProfileRefresh_1.routineProfileRefresh)({
            allConversations: [notMe, me],
            ourConversationId: me.id,
            storage: makeStorage(),
        });
        sinon.assert.calledWith(getProfileStub.getProfile, notMe.get('uuid'), notMe.get('e164'));
        sinon.assert.neverCalledWith(getProfileStub.getProfile, me.get('uuid'), me.get('e164'));
    });
    it('skips conversations that were refreshed in the last hour', async () => {
        const neverRefreshed = makeConversation();
        const recentlyFetched = makeConversation({
            profileLastFetchedAt: Date.now() - 59 * 60 * 1000,
        });
        await (0, routineProfileRefresh_1.routineProfileRefresh)({
            allConversations: [neverRefreshed, recentlyFetched],
            ourConversationId: UUID_1.UUID.generate().toString(),
            storage: makeStorage(),
        });
        sinon.assert.calledOnce(getProfileStub.getProfile);
        sinon.assert.calledWith(getProfileStub.getProfile, neverRefreshed.get('uuid'), neverRefreshed.get('e164'));
        sinon.assert.neverCalledWith(getProfileStub.getProfile, recentlyFetched.get('uuid'), recentlyFetched.get('e164'));
    });
    it('"digs into" the members of an active group', async () => {
        const privateConversation = makeConversation();
        const recentlyActiveGroupMember = makeConversation();
        const inactiveGroupMember = makeConversation({
            active_at: Date.now() - 31 * 24 * 60 * 60 * 1000,
        });
        const memberWhoHasRecentlyRefreshed = makeConversation({
            profileLastFetchedAt: Date.now() - 59 * 60 * 1000,
        });
        const groupConversation = makeGroup([
            recentlyActiveGroupMember,
            inactiveGroupMember,
            memberWhoHasRecentlyRefreshed,
        ]);
        await (0, routineProfileRefresh_1.routineProfileRefresh)({
            allConversations: [
                privateConversation,
                recentlyActiveGroupMember,
                inactiveGroupMember,
                memberWhoHasRecentlyRefreshed,
                groupConversation,
            ],
            ourConversationId: UUID_1.UUID.generate().toString(),
            storage: makeStorage(),
        });
        sinon.assert.calledWith(getProfileStub.getProfile, privateConversation.get('uuid'), privateConversation.get('e164'));
        sinon.assert.calledWith(getProfileStub.getProfile, recentlyActiveGroupMember.get('uuid'), recentlyActiveGroupMember.get('e164'));
        sinon.assert.calledWith(getProfileStub.getProfile, inactiveGroupMember.get('uuid'), inactiveGroupMember.get('e164'));
        sinon.assert.neverCalledWith(getProfileStub.getProfile, memberWhoHasRecentlyRefreshed.get('uuid'), memberWhoHasRecentlyRefreshed.get('e164'));
        sinon.assert.neverCalledWith(getProfileStub.getProfile, groupConversation.get('uuid'), groupConversation.get('e164'));
    });
    it('only refreshes profiles for the 50 most recently active direct conversations', async () => {
        const me = makeConversation();
        const activeConversations = (0, lodash_1.times)(40, () => makeConversation());
        const inactiveGroupMembers = (0, lodash_1.times)(10, () => makeConversation({
            active_at: Date.now() - 999 * 24 * 60 * 60 * 1000,
        }));
        const recentlyActiveGroup = makeGroup(inactiveGroupMembers);
        const shouldNotBeIncluded = [
            // Recently-active groups with no other members
            makeGroup([]),
            makeGroup([me]),
            // Old direct conversations
            ...(0, lodash_1.times)(3, () => makeConversation({
                active_at: Date.now() - 365 * 24 * 60 * 60 * 1000,
            })),
            // Old groups
            ...(0, lodash_1.times)(3, () => makeGroup(inactiveGroupMembers)),
        ];
        await (0, routineProfileRefresh_1.routineProfileRefresh)({
            allConversations: [
                me,
                ...activeConversations,
                recentlyActiveGroup,
                ...inactiveGroupMembers,
                ...shouldNotBeIncluded,
            ],
            ourConversationId: me.id,
            storage: makeStorage(),
        });
        [...activeConversations, ...inactiveGroupMembers].forEach(conversation => {
            sinon.assert.calledWith(getProfileStub.getProfile, conversation.get('uuid'), conversation.get('e164'));
        });
        [me, ...shouldNotBeIncluded].forEach(conversation => {
            sinon.assert.neverCalledWith(getProfileStub.getProfile, conversation.get('uuid'), conversation.get('e164'));
        });
    });
});
