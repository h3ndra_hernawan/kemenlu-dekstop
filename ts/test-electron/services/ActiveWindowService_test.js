"use strict";
// Copyright 2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const events_1 = require("events");
const ActiveWindowService_1 = require("../../services/ActiveWindowService");
describe('ActiveWindowService', () => {
    const fakeIpcEvent = {};
    beforeEach(function beforeEach() {
        this.clock = sinon.useFakeTimers({ now: 1000 });
    });
    afterEach(function afterEach() {
        this.clock.restore();
    });
    function createFakeDocument() {
        return document.createElement('div');
    }
    it('is inactive at the start', () => {
        const service = new ActiveWindowService_1.ActiveWindowService();
        service.initialize(createFakeDocument(), new events_1.EventEmitter());
        chai_1.assert.isFalse(service.isActive());
    });
    it('becomes active after focusing', () => {
        const fakeIpc = new events_1.EventEmitter();
        const service = new ActiveWindowService_1.ActiveWindowService();
        service.initialize(createFakeDocument(), fakeIpc);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
        chai_1.assert.isTrue(service.isActive());
    });
    it('becomes inactive after 15 seconds without interaction', function test() {
        const fakeIpc = new events_1.EventEmitter();
        const service = new ActiveWindowService_1.ActiveWindowService();
        service.initialize(createFakeDocument(), fakeIpc);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
        this.clock.tick(5000);
        chai_1.assert.isTrue(service.isActive());
        this.clock.tick(9999);
        chai_1.assert.isTrue(service.isActive());
        this.clock.tick(1);
        chai_1.assert.isFalse(service.isActive());
    });
    ['click', 'keydown', 'mousedown', 'mousemove', 'touchstart', 'wheel'].forEach((eventName) => {
        it(`is inactive even in the face of ${eventName} events if unfocused`, function test() {
            const fakeDocument = createFakeDocument();
            const fakeIpc = new events_1.EventEmitter();
            const service = new ActiveWindowService_1.ActiveWindowService();
            service.initialize(fakeDocument, fakeIpc);
            fakeIpc.emit('set-window-focus', fakeIpcEvent, false);
            fakeDocument.dispatchEvent(new Event(eventName));
            chai_1.assert.isFalse(service.isActive());
        });
        it(`stays active if focused and receiving ${eventName} events`, function test() {
            const fakeDocument = createFakeDocument();
            const fakeIpc = new events_1.EventEmitter();
            const service = new ActiveWindowService_1.ActiveWindowService();
            service.initialize(fakeDocument, fakeIpc);
            fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
            fakeDocument.dispatchEvent(new Event(eventName));
            chai_1.assert.isTrue(service.isActive());
            this.clock.tick(8000);
            fakeDocument.dispatchEvent(new Event(eventName));
            chai_1.assert.isTrue(service.isActive());
            this.clock.tick(8000);
            fakeDocument.dispatchEvent(new Event(eventName));
            chai_1.assert.isTrue(service.isActive());
        });
    });
    it('calls callbacks when going from unfocused to focused', () => {
        const fakeIpc = new events_1.EventEmitter();
        const service = new ActiveWindowService_1.ActiveWindowService();
        service.initialize(createFakeDocument(), fakeIpc);
        const callback = sinon.stub();
        service.registerForActive(callback);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
        sinon.assert.calledOnce(callback);
    });
    it('calls callbacks when receiving a click event after being focused', function test() {
        const fakeDocument = createFakeDocument();
        const fakeIpc = new events_1.EventEmitter();
        const service = new ActiveWindowService_1.ActiveWindowService();
        service.initialize(fakeDocument, fakeIpc);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
        this.clock.tick(20000);
        const callback = sinon.stub();
        service.registerForActive(callback);
        fakeDocument.dispatchEvent(new Event('click'));
        sinon.assert.calledOnce(callback);
    });
    it('only calls callbacks every 5 seconds; it is throttled', function test() {
        const fakeIpc = new events_1.EventEmitter();
        const service = new ActiveWindowService_1.ActiveWindowService();
        service.initialize(createFakeDocument(), fakeIpc);
        const callback = sinon.stub();
        service.registerForActive(callback);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, false);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, false);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, false);
        sinon.assert.calledOnce(callback);
        this.clock.tick(15000);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
        sinon.assert.calledTwice(callback);
    });
    it('can remove callbacks', () => {
        const fakeDocument = createFakeDocument();
        const fakeIpc = new events_1.EventEmitter();
        const service = new ActiveWindowService_1.ActiveWindowService();
        service.initialize(fakeDocument, fakeIpc);
        const callback = sinon.stub();
        service.registerForActive(callback);
        service.unregisterForActive(callback);
        fakeIpc.emit('set-window-focus', fakeIpcEvent, true);
        sinon.assert.notCalled(callback);
    });
});
