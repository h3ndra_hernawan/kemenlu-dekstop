"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// We allow `any`s because it's arduous to set up "real" WebAPIs and storages.
/* eslint-disable @typescript-eslint/no-explicit-any */
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const uuid_1 = require("uuid");
const long_1 = __importDefault(require("long"));
const durations = __importStar(require("../../util/durations"));
const Bytes = __importStar(require("../../Bytes"));
const protobuf_1 = require("../../protobuf");
const senderCertificate_1 = require("../../services/senderCertificate");
var SenderCertificate = protobuf_1.SignalService.SenderCertificate;
describe('SenderCertificateService', () => {
    const FIFTEEN_MINUTES = 15 * durations.MINUTE;
    let fakeValidCertificate;
    let fakeValidEncodedCertificate;
    let fakeValidCertificateExpiry;
    let fakeServer;
    let fakeNavigator;
    let fakeWindow;
    let fakeStorage;
    function initializeTestService() {
        const result = new senderCertificate_1.SenderCertificateService();
        result.initialize({
            server: fakeServer,
            navigator: fakeNavigator,
            onlineEventTarget: fakeWindow,
            storage: fakeStorage,
        });
        return result;
    }
    beforeEach(() => {
        fakeValidCertificate = new SenderCertificate();
        fakeValidCertificateExpiry = Date.now() + 604800000;
        const certificate = new SenderCertificate.Certificate();
        certificate.expires = long_1.default.fromNumber(fakeValidCertificateExpiry);
        fakeValidCertificate.certificate =
            SenderCertificate.Certificate.encode(certificate).finish();
        fakeValidEncodedCertificate =
            SenderCertificate.encode(fakeValidCertificate).finish();
        fakeServer = {
            getSenderCertificate: sinon.stub().resolves({
                certificate: Bytes.toBase64(fakeValidEncodedCertificate),
            }),
        };
        fakeNavigator = { onLine: true };
        fakeWindow = {
            addEventListener: sinon.stub(),
            dispatchEvent: sinon.stub(),
            removeEventListener: sinon.stub(),
        };
        fakeStorage = {
            get: sinon.stub(),
            put: sinon.stub().resolves(),
            remove: sinon.stub().resolves(),
        };
        fakeStorage.get.withArgs('uuid_id').returns(`${(0, uuid_1.v4)()}.2`);
        fakeStorage.get.withArgs('password').returns('abc123');
    });
    describe('get', () => {
        it('returns valid yes-E164 certificates from storage if they exist', async () => {
            const cert = {
                expires: Date.now() + 123456,
                serialized: new Uint8Array(2),
            };
            fakeStorage.get.withArgs('senderCertificate').returns(cert);
            const service = initializeTestService();
            chai_1.assert.strictEqual(await service.get(0 /* WithE164 */), cert);
            sinon.assert.notCalled(fakeStorage.put);
        });
        it('returns valid no-E164 certificates from storage if they exist', async () => {
            const cert = {
                expires: Date.now() + 123456,
                serialized: new Uint8Array(2),
            };
            fakeStorage.get.withArgs('senderCertificateNoE164').returns(cert);
            const service = initializeTestService();
            chai_1.assert.strictEqual(await service.get(1 /* WithoutE164 */), cert);
            sinon.assert.notCalled(fakeStorage.put);
        });
        it('returns and stores a newly-fetched yes-E164 certificate if none was in storage', async () => {
            const service = initializeTestService();
            chai_1.assert.deepEqual(await service.get(0 /* WithE164 */), {
                expires: fakeValidCertificateExpiry - FIFTEEN_MINUTES,
                serialized: fakeValidEncodedCertificate,
            });
            sinon.assert.calledWithMatch(fakeStorage.put, 'senderCertificate', {
                expires: fakeValidCertificateExpiry - FIFTEEN_MINUTES,
                serialized: Buffer.from(fakeValidEncodedCertificate),
            });
            sinon.assert.calledWith(fakeServer.getSenderCertificate, false);
        });
        it('returns and stores a newly-fetched no-E164 certificate if none was in storage', async () => {
            const service = initializeTestService();
            chai_1.assert.deepEqual(await service.get(1 /* WithoutE164 */), {
                expires: fakeValidCertificateExpiry - FIFTEEN_MINUTES,
                serialized: fakeValidEncodedCertificate,
            });
            sinon.assert.calledWithMatch(fakeStorage.put, 'senderCertificateNoE164', {
                expires: fakeValidCertificateExpiry - FIFTEEN_MINUTES,
                serialized: Buffer.from(fakeValidEncodedCertificate),
            });
            sinon.assert.calledWith(fakeServer.getSenderCertificate, true);
        });
        it('fetches new certificates if the value in storage has already expired', async () => {
            const service = initializeTestService();
            fakeStorage.get.withArgs('senderCertificate').returns({
                expires: Date.now() - 1000,
                serialized: new Uint8Array(2),
            });
            await service.get(0 /* WithE164 */);
            sinon.assert.called(fakeServer.getSenderCertificate);
        });
        it('fetches new certificates if the value in storage is invalid', async () => {
            const service = initializeTestService();
            fakeStorage.get.withArgs('senderCertificate').returns({
                serialized: 'not an uint8array',
            });
            await service.get(0 /* WithE164 */);
            sinon.assert.called(fakeServer.getSenderCertificate);
        });
        it('only hits the server once per certificate type when requesting many times', async () => {
            const service = initializeTestService();
            await Promise.all([
                service.get(0 /* WithE164 */),
                service.get(1 /* WithoutE164 */),
                service.get(0 /* WithE164 */),
                service.get(1 /* WithoutE164 */),
                service.get(0 /* WithE164 */),
                service.get(1 /* WithoutE164 */),
                service.get(0 /* WithE164 */),
                service.get(1 /* WithoutE164 */),
            ]);
            sinon.assert.calledTwice(fakeServer.getSenderCertificate);
        });
        it('hits the server again after a request has completed', async () => {
            const service = initializeTestService();
            await service.get(0 /* WithE164 */);
            sinon.assert.calledOnce(fakeServer.getSenderCertificate);
            await service.get(0 /* WithE164 */);
            sinon.assert.calledTwice(fakeServer.getSenderCertificate);
        });
        it('returns undefined if the request to the server fails', async () => {
            const service = initializeTestService();
            fakeServer.getSenderCertificate.rejects(new Error('uh oh'));
            chai_1.assert.isUndefined(await service.get(0 /* WithE164 */));
        });
        it('returns undefined if the server returns an already-expired certificate', async () => {
            const service = initializeTestService();
            const expiredCertificate = new SenderCertificate();
            const certificate = new SenderCertificate.Certificate();
            certificate.expires = long_1.default.fromNumber(Date.now() - 1000);
            expiredCertificate.certificate =
                SenderCertificate.Certificate.encode(certificate).finish();
            fakeServer.getSenderCertificate.resolves({
                certificate: Bytes.toBase64(SenderCertificate.encode(expiredCertificate).finish()),
            });
            chai_1.assert.isUndefined(await service.get(0 /* WithE164 */));
        });
        it('clear waits for any outstanding requests then erases storage', async () => {
            let count = 0;
            fakeServer = {
                getSenderCertificate: sinon.spy(async () => {
                    await new Promise(resolve => setTimeout(resolve, 500));
                    count += 1;
                    return {
                        certificate: Bytes.toBase64(fakeValidEncodedCertificate),
                    };
                }),
            };
            const service = initializeTestService();
            service.get(0 /* WithE164 */);
            service.get(1 /* WithoutE164 */);
            await service.clear();
            chai_1.assert.equal(count, 2);
            chai_1.assert.isUndefined(fakeStorage.get('senderCertificate'));
            chai_1.assert.isUndefined(fakeStorage.get('senderCertificateNoE164'));
        });
    });
});
