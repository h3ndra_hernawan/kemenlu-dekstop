"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const os_1 = __importDefault(require("os"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const Attachments = __importStar(require("../../windows/attachments"));
const Bytes = __importStar(require("../../Bytes"));
const PREFIX_LENGTH = 2;
const NUM_SEPARATORS = 1;
const NAME_LENGTH = 64;
const PATH_LENGTH = PREFIX_LENGTH + NUM_SEPARATORS + NAME_LENGTH;
describe('Attachments', () => {
    const USER_DATA = window.SignalContext.getPath('userData');
    let tempRootDirectory;
    before(() => {
        tempRootDirectory = fs_1.default.mkdtempSync(path_1.default.join(os_1.default.tmpdir(), 'Signal'));
    });
    after(async () => {
        await fs_extra_1.default.remove(tempRootDirectory);
    });
    describe('createReader', () => {
        it('should read file from disk', async () => {
            const tempDirectory = path_1.default.join(tempRootDirectory, 'Attachments_createReader');
            const relativePath = Attachments.getRelativePath(Attachments.createName());
            const fullPath = path_1.default.join(tempDirectory, relativePath);
            const input = Bytes.fromString('test string');
            const inputBuffer = Buffer.from(input);
            await fs_extra_1.default.ensureFile(fullPath);
            await fs_extra_1.default.writeFile(fullPath, inputBuffer);
            const output = await Attachments.createReader(tempDirectory)(relativePath);
            chai_1.assert.deepEqual(input, output);
        });
        it('throws if relative path goes higher than root', async () => {
            const tempDirectory = path_1.default.join(tempRootDirectory, 'Attachments_createReader');
            const relativePath = '../../parent';
            await chai_1.assert.isRejected(Attachments.createReader(tempDirectory)(relativePath), 'Invalid relative path');
        });
    });
    describe('copyIntoAttachmentsDirectory', () => {
        let filesToRemove;
        const getFakeAttachmentsDirectory = () => {
            const result = path_1.default.join(USER_DATA, `fake-attachments-${Date.now()}-${Math.random()
                .toString()
                .substring(2)}`);
            filesToRemove.push(result);
            return result;
        };
        // These tests use the `userData` path. In `electron-mocha`, these are temporary
        //   directories; no need to be concerned about messing with the "real" directory.
        before(() => {
            filesToRemove = [];
        });
        after(async () => {
            await Promise.all(filesToRemove.map(toRemove => fs_extra_1.default.remove(toRemove)));
            filesToRemove = [];
        });
        it('throws if passed a non-string', () => {
            chai_1.assert.throws(() => {
                Attachments.copyIntoAttachmentsDirectory(1234);
            }, TypeError);
            chai_1.assert.throws(() => {
                Attachments.copyIntoAttachmentsDirectory(null);
            }, TypeError);
        });
        it('returns a function that rejects if the source path is not a string', async () => {
            const copier = Attachments.copyIntoAttachmentsDirectory(await getFakeAttachmentsDirectory());
            await chai_1.assert.isRejected(copier(123));
        });
        it('returns a function that rejects if the source path is not in the user config directory', async () => {
            const copier = Attachments.copyIntoAttachmentsDirectory(await getFakeAttachmentsDirectory());
            await chai_1.assert.isRejected(copier(path_1.default.join(tempRootDirectory, 'hello.txt')), "'sourcePath' must be relative to the user config directory");
        });
        it('returns a function that copies the source path into the attachments directory and returns its path and size', async () => {
            const attachmentsPath = await getFakeAttachmentsDirectory();
            const someOtherPath = path_1.default.join(USER_DATA, 'somethingElse');
            await fs_extra_1.default.outputFile(someOtherPath, 'hello world');
            filesToRemove.push(someOtherPath);
            const copier = Attachments.copyIntoAttachmentsDirectory(attachmentsPath);
            const { path: relativePath, size } = await copier(someOtherPath);
            const absolutePath = path_1.default.join(attachmentsPath, relativePath);
            chai_1.assert.notEqual(someOtherPath, absolutePath);
            chai_1.assert.strictEqual(await fs_1.default.promises.readFile(absolutePath, 'utf8'), 'hello world');
            chai_1.assert.strictEqual(size, 'hello world'.length);
        });
    });
    describe('createWriterForExisting', () => {
        it('should write file to disk on given path and return path', async () => {
            const input = Bytes.fromString('test string');
            const tempDirectory = path_1.default.join(tempRootDirectory, 'Attachments_createWriterForExisting');
            const relativePath = Attachments.getRelativePath(Attachments.createName());
            const attachment = {
                path: relativePath,
                data: input,
            };
            const outputPath = await Attachments.createWriterForExisting(tempDirectory)(attachment);
            const output = await fs_extra_1.default.readFile(path_1.default.join(tempDirectory, outputPath));
            chai_1.assert.equal(outputPath, relativePath);
            const inputBuffer = Buffer.from(input);
            chai_1.assert.deepEqual(inputBuffer, output);
        });
        it('throws if relative path goes higher than root', async () => {
            const input = Bytes.fromString('test string');
            const tempDirectory = path_1.default.join(tempRootDirectory, 'Attachments_createWriterForExisting');
            const relativePath = '../../parent';
            const attachment = {
                path: relativePath,
                data: input,
            };
            try {
                await Attachments.createWriterForExisting(tempDirectory)(attachment);
            }
            catch (error) {
                chai_1.assert.strictEqual(error.message, 'Invalid relative path');
                return;
            }
            throw new Error('Expected an error');
        });
    });
    describe('createWriterForNew', () => {
        it('should write file to disk and return path', async () => {
            const input = Bytes.fromString('test string');
            const tempDirectory = path_1.default.join(tempRootDirectory, 'Attachments_createWriterForNew');
            const outputPath = await Attachments.createWriterForNew(tempDirectory)(input);
            const output = await fs_extra_1.default.readFile(path_1.default.join(tempDirectory, outputPath));
            chai_1.assert.lengthOf(outputPath, PATH_LENGTH);
            const inputBuffer = Buffer.from(input);
            chai_1.assert.deepEqual(inputBuffer, output);
        });
    });
    describe('createAbsolutePathGetter', () => {
        const isWindows = process.platform === 'win32';
        it('combines root and relative path', () => {
            const root = isWindows ? 'C:\\temp' : '/tmp';
            const relative = 'ab/abcdef';
            const pathGetter = Attachments.createAbsolutePathGetter(root);
            const absolutePath = pathGetter(relative);
            chai_1.assert.strictEqual(absolutePath, isWindows ? 'C:\\temp\\ab\\abcdef' : '/tmp/ab/abcdef');
        });
        it('throws if relative path goes higher than root', () => {
            const root = isWindows ? 'C:\\temp' : 'tmp';
            const relative = '../../ab/abcdef';
            const pathGetter = Attachments.createAbsolutePathGetter(root);
            try {
                pathGetter(relative);
            }
            catch (error) {
                chai_1.assert.strictEqual(error.message, 'Invalid relative path');
                return;
            }
            throw new Error('Expected an error');
        });
    });
    describe('createName', () => {
        it('should return random file name with correct length', () => {
            chai_1.assert.lengthOf(Attachments.createName(), NAME_LENGTH);
        });
        it('can include a suffix', () => {
            const result = Attachments.createName('.txt');
            chai_1.assert.lengthOf(result, NAME_LENGTH + '.txt'.length);
            (0, chai_1.assert)(result.endsWith('.txt'));
        });
    });
    describe('getRelativePath', () => {
        it('should return correct path', () => {
            const name = '608ce3bc536edbf7637a6aeb6040bdfec49349140c0dd43e97c7ce263b15ff7e';
            chai_1.assert.lengthOf(Attachments.getRelativePath(name), PATH_LENGTH);
        });
    });
    describe('createDeleter', () => {
        it('should delete file from disk', async () => {
            const tempDirectory = path_1.default.join(tempRootDirectory, 'Attachments_createDeleter');
            const relativePath = Attachments.getRelativePath(Attachments.createName());
            const fullPath = path_1.default.join(tempDirectory, relativePath);
            const input = Bytes.fromString('test string');
            const inputBuffer = Buffer.from(input);
            await fs_extra_1.default.ensureFile(fullPath);
            await fs_extra_1.default.writeFile(fullPath, inputBuffer);
            await Attachments.createDeleter(tempDirectory)(relativePath);
            const existsFile = await fs_extra_1.default.pathExists(fullPath);
            chai_1.assert.isFalse(existsFile);
        });
        it('throws if relative path goes higher than root', async () => {
            const tempDirectory = path_1.default.join(tempRootDirectory, 'Attachments_createDeleter');
            const relativePath = '../../parent';
            try {
                await Attachments.createDeleter(tempDirectory)(relativePath);
            }
            catch (error) {
                chai_1.assert.strictEqual(error.message, 'Invalid relative path');
                return;
            }
            throw new Error('Expected an error');
        });
    });
});
