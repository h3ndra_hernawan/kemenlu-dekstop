"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const events_1 = require("events");
const createNativeThemeListener_1 = require("../../context/createNativeThemeListener");
class FakeIPC extends events_1.EventEmitter {
    constructor(state) {
        super();
        this.state = state;
    }
    sendSync(channel) {
        chai_1.assert.strictEqual(channel, 'native-theme:init');
        return this.state;
    }
    send() {
        throw new Error('This should not be called. It is only here to satisfy the interface');
    }
}
describe('NativeThemeListener', () => {
    const holder = { systemTheme: 'dark' };
    it('syncs the initial native theme', () => {
        const dark = (0, createNativeThemeListener_1.createNativeThemeListener)(new FakeIPC({
            shouldUseDarkColors: true,
        }), holder);
        chai_1.assert.strictEqual(holder.systemTheme, 'dark');
        chai_1.assert.strictEqual(dark.getSystemTheme(), 'dark');
        const light = (0, createNativeThemeListener_1.createNativeThemeListener)(new FakeIPC({
            shouldUseDarkColors: false,
        }), holder);
        chai_1.assert.strictEqual(holder.systemTheme, 'light');
        chai_1.assert.strictEqual(light.getSystemTheme(), 'light');
    });
    it('should react to native theme changes', () => {
        const ipc = new FakeIPC({
            shouldUseDarkColors: true,
        });
        const listener = (0, createNativeThemeListener_1.createNativeThemeListener)(ipc, holder);
        ipc.emit('native-theme:changed', null, {
            shouldUseDarkColors: false,
        });
        chai_1.assert.strictEqual(holder.systemTheme, 'light');
        chai_1.assert.strictEqual(listener.getSystemTheme(), 'light');
    });
    it('should notify subscribers of native theme changes', done => {
        const ipc = new FakeIPC({
            shouldUseDarkColors: true,
        });
        const listener = (0, createNativeThemeListener_1.createNativeThemeListener)(ipc, holder);
        listener.subscribe(state => {
            chai_1.assert.isFalse(state.shouldUseDarkColors);
            done();
        });
        ipc.emit('native-theme:changed', null, {
            shouldUseDarkColors: false,
        });
    });
});
