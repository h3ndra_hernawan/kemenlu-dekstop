"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const crypto_1 = __importDefault(require("crypto"));
const Crypto_1 = require("../../Crypto");
describe('SignalContext.Crypto', () => {
    describe('hash', () => {
        it('returns SHA512 hash of the input', () => {
            const result = (0, Crypto_1.hash)(Crypto_1.HashType.size512, Buffer.from('signal'));
            chai_1.assert.strictEqual(Buffer.from(result).toString('base64'), 'WxneQjrfSlY95Bi+SAzDAr2cf3mxUXePeNYn6DILN4a8NFr9VelTbP5tGHdthi+' +
                'mrJLqMZd1I6w8CxCnmJ/OFw==');
        });
    });
    describe('sign', () => {
        it('returns hmac SHA256 hash of the input', () => {
            const result = (0, Crypto_1.sign)(Buffer.from('secret'), Buffer.from('signal'));
            chai_1.assert.strictEqual(Buffer.from(result).toString('base64'), '5ewbITW27c1F7dluF9KwGcVQSxmZp6mpVhPj3ww1Sh8=');
        });
    });
    describe('encrypt+decrypt', () => {
        it('returns original input', () => {
            const iv = crypto_1.default.randomBytes(16);
            const key = crypto_1.default.randomBytes(32);
            const input = Buffer.from('plaintext');
            const ciphertext = (0, Crypto_1.encrypt)(Crypto_1.CipherType.AES256CBC, {
                key,
                iv,
                plaintext: input,
            });
            const plaintext = (0, Crypto_1.decrypt)(Crypto_1.CipherType.AES256CBC, {
                key,
                iv,
                ciphertext,
            });
            chai_1.assert.strictEqual(Buffer.from(plaintext).toString(), 'plaintext');
        });
    });
});
