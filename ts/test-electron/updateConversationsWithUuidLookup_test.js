"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/no-non-null-assertion */
const chai_1 = require("chai");
const sinon_1 = __importDefault(require("sinon"));
const conversations_1 = require("../models/conversations");
const UUID_1 = require("../types/UUID");
const updateConversationsWithUuidLookup_1 = require("../updateConversationsWithUuidLookup");
describe('updateConversationsWithUuidLookup', () => {
    class FakeConversationController {
        constructor(conversations = []) {
            this.conversations = conversations;
        }
        get(id) {
            return this.conversations.find(conversation => conversation.id === id ||
                conversation.get('e164') === id ||
                conversation.get('uuid') === id);
        }
        ensureContactIds({ e164, uuid: uuidFromServer, highTrust, }) {
            (0, chai_1.assert)(e164, 'FakeConversationController is not set up for this case (E164 must be provided)');
            (0, chai_1.assert)(uuidFromServer, 'FakeConversationController is not set up for this case (UUID must be provided)');
            (0, chai_1.assert)(highTrust, 'FakeConversationController is not set up for this case (must be "high trust")');
            const normalizedUuid = uuidFromServer.toLowerCase();
            const convoE164 = this.get(e164);
            const convoUuid = this.get(normalizedUuid);
            (0, chai_1.assert)(convoE164 || convoUuid, 'FakeConversationController is not set up for this case (at least one conversation should be found)');
            if (convoE164 && convoUuid) {
                if (convoE164 === convoUuid) {
                    return convoUuid.get('id');
                }
                convoE164.unset('e164');
                convoUuid.updateE164(e164);
                return convoUuid.get('id');
            }
            if (convoE164 && !convoUuid) {
                convoE164.updateUuid(normalizedUuid);
                return convoE164.get('id');
            }
            chai_1.assert.fail('FakeConversationController should never get here');
            return undefined;
        }
    }
    function createConversation(attributes = {}) {
        return new conversations_1.ConversationModel(Object.assign({ id: UUID_1.UUID.generate().toString(), inbox_position: 0, isPinned: false, lastMessageDeletedForEveryone: false, markedUnread: false, messageCount: 1, profileSharing: true, sentMessageCount: 0, type: 'private', version: 0 }, attributes));
    }
    let sinonSandbox;
    let fakeGetUuidsForE164s;
    let fakeMessaging;
    beforeEach(() => {
        sinonSandbox = sinon_1.default.createSandbox();
        sinonSandbox.stub(window.Signal.Data, 'updateConversation');
        fakeGetUuidsForE164s = sinonSandbox.stub().resolves({});
        fakeMessaging = { getUuidsForE164s: fakeGetUuidsForE164s };
    });
    afterEach(() => {
        sinonSandbox.restore();
    });
    it('does nothing when called with an empty array', async () => {
        await (0, updateConversationsWithUuidLookup_1.updateConversationsWithUuidLookup)({
            conversationController: new FakeConversationController(),
            conversations: [],
            messaging: fakeMessaging,
        });
        sinon_1.default.assert.notCalled(fakeMessaging.getUuidsForE164s);
    });
    it('does nothing when called with an array of conversations that lack E164s', async () => {
        await (0, updateConversationsWithUuidLookup_1.updateConversationsWithUuidLookup)({
            conversationController: new FakeConversationController(),
            conversations: [
                createConversation(),
                createConversation({ uuid: UUID_1.UUID.generate().toString() }),
            ],
            messaging: fakeMessaging,
        });
        sinon_1.default.assert.notCalled(fakeMessaging.getUuidsForE164s);
    });
    it('updates conversations with their UUID', async () => {
        const conversation1 = createConversation({ e164: '+13215559876' });
        const conversation2 = createConversation({
            e164: '+16545559876',
            uuid: UUID_1.UUID.generate().toString(), // should be overwritten
        });
        const uuid1 = UUID_1.UUID.generate().toString();
        const uuid2 = UUID_1.UUID.generate().toString();
        fakeGetUuidsForE164s.resolves({
            '+13215559876': uuid1,
            '+16545559876': uuid2,
        });
        await (0, updateConversationsWithUuidLookup_1.updateConversationsWithUuidLookup)({
            conversationController: new FakeConversationController([
                conversation1,
                conversation2,
            ]),
            conversations: [conversation1, conversation2],
            messaging: fakeMessaging,
        });
        chai_1.assert.strictEqual(conversation1.get('uuid'), uuid1);
        chai_1.assert.strictEqual(conversation2.get('uuid'), uuid2);
    });
    it("marks conversations unregistered if we didn't have a UUID for them and the server also doesn't have one", async () => {
        const conversation = createConversation({ e164: '+13215559876' });
        chai_1.assert.isUndefined(conversation.get('discoveredUnregisteredAt'), 'Test was not set up correctly');
        fakeGetUuidsForE164s.resolves({ '+13215559876': null });
        await (0, updateConversationsWithUuidLookup_1.updateConversationsWithUuidLookup)({
            conversationController: new FakeConversationController([conversation]),
            conversations: [conversation],
            messaging: fakeMessaging,
        });
        chai_1.assert.approximately(conversation.get('discoveredUnregisteredAt') || 0, Date.now(), 5000);
    });
    it("doesn't mark conversations unregistered if we already had a UUID for them, even if the server doesn't return one", async () => {
        const existingUuid = UUID_1.UUID.generate().toString();
        const conversation = createConversation({
            e164: '+13215559876',
            uuid: existingUuid,
        });
        chai_1.assert.isUndefined(conversation.get('discoveredUnregisteredAt'), 'Test was not set up correctly');
        fakeGetUuidsForE164s.resolves({ '+13215559876': null });
        await (0, updateConversationsWithUuidLookup_1.updateConversationsWithUuidLookup)({
            conversationController: new FakeConversationController([conversation]),
            conversations: [conversation],
            messaging: fakeMessaging,
        });
        chai_1.assert.strictEqual(conversation.get('uuid'), existingUuid);
        chai_1.assert.isUndefined(conversation.get('discoveredUnregisteredAt'));
    });
});
