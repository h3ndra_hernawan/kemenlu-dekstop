"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const background_1 = require("../background");
describe('#isOverHourIntoPast', () => {
    it('returns false for now', () => {
        chai_1.assert.isFalse((0, background_1.isOverHourIntoPast)(Date.now()));
    });
    it('returns false for 5 minutes ago', () => {
        const fiveMinutesAgo = Date.now() - 5 * 60 * 1000;
        chai_1.assert.isFalse((0, background_1.isOverHourIntoPast)(fiveMinutesAgo));
    });
    it('returns true for 65 minutes ago', () => {
        const sixtyFiveMinutesAgo = Date.now() - 65 * 60 * 1000;
        chai_1.assert.isTrue((0, background_1.isOverHourIntoPast)(sixtyFiveMinutesAgo));
    });
});
describe('#cleanupSessionResets', () => {
    it('leaves empty object alone', () => {
        window.storage.put('sessionResets', {});
        (0, background_1.cleanupSessionResets)();
        const actual = window.storage.get('sessionResets');
        const expected = {};
        chai_1.assert.deepEqual(actual, expected);
    });
    it('filters out any timestamp older than one hour', () => {
        const startValue = {
            one: Date.now() - 1,
            two: Date.now(),
            three: Date.now() - 65 * 60 * 1000,
        };
        window.storage.put('sessionResets', startValue);
        (0, background_1.cleanupSessionResets)();
        const actual = window.storage.get('sessionResets');
        const expected = window._.pick(startValue, ['one', 'two']);
        chai_1.assert.deepEqual(actual, expected);
    });
    it('filters out falsey items', () => {
        const startValue = {
            one: 0,
            two: Date.now(),
        };
        window.storage.put('sessionResets', startValue);
        (0, background_1.cleanupSessionResets)();
        const actual = window.storage.get('sessionResets');
        const expected = window._.pick(startValue, ['two']);
        chai_1.assert.deepEqual(actual, expected);
        chai_1.assert.deepEqual(Object.keys(startValue), ['two']);
    });
});
