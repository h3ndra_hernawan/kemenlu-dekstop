"use strict";
// Copyright 2015-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/no-empty-function */
const chai_1 = require("chai");
const uuid_1 = require("uuid");
const MessageReceiver_1 = __importDefault(require("../textsecure/MessageReceiver"));
const WebsocketResources_1 = require("../textsecure/WebsocketResources");
const protobuf_1 = require("../protobuf");
const Crypto = __importStar(require("../Crypto"));
describe('MessageReceiver', () => {
    const number = '+19999999999';
    const uuid = 'aaaaaaaa-bbbb-4ccc-9ddd-eeeeeeeeeeee';
    const deviceId = 1;
    let oldUuid;
    let oldDeviceId;
    beforeEach(async () => {
        var _a;
        oldUuid = (_a = window.storage.user.getUuid()) === null || _a === void 0 ? void 0 : _a.toString();
        oldDeviceId = window.storage.user.getDeviceId();
        await window.storage.user.setUuidAndDeviceId((0, uuid_1.v4)(), 2);
        await window.storage.protocol.hydrateCaches();
    });
    afterEach(async () => {
        if (oldUuid !== undefined && oldDeviceId !== undefined) {
            await window.storage.user.setUuidAndDeviceId(oldUuid, oldDeviceId);
        }
        await window.storage.protocol.removeAllUnprocessed();
    });
    describe('connecting', () => {
        it('generates decryption-error event when it cannot decrypt', async () => {
            const messageReceiver = new MessageReceiver_1.default({
                server: {},
                storage: window.storage,
                serverTrustRoot: 'AAAAAAAA',
            });
            const body = protobuf_1.SignalService.Envelope.encode({
                type: protobuf_1.SignalService.Envelope.Type.CIPHERTEXT,
                source: number,
                sourceUuid: uuid,
                sourceDevice: deviceId,
                timestamp: Date.now(),
                content: Crypto.getRandomBytes(200),
            }).finish();
            messageReceiver.handleRequest(new WebsocketResources_1.IncomingWebSocketRequest({
                id: 1,
                verb: 'PUT',
                path: '/api/v1/message',
                body,
                headers: [],
            }, (_) => { }));
            await new Promise(resolve => {
                messageReceiver.addEventListener('decryption-error', (error) => {
                    chai_1.assert.strictEqual(error.decryptionError.senderUuid, uuid);
                    chai_1.assert.strictEqual(error.decryptionError.senderDevice, deviceId);
                    resolve();
                });
            });
            await messageReceiver.drain();
        });
    });
});
