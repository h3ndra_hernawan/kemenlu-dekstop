"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Bytes = __importStar(require("../Bytes"));
const Crypto_1 = require("../Crypto");
const Curve_1 = require("../Curve");
describe('Curve', () => {
    it('verifySignature roundtrip', () => {
        const message = Buffer.from('message');
        const { pubKey, privKey } = (0, Curve_1.generateKeyPair)();
        const signature = (0, Curve_1.calculateSignature)(privKey, message);
        const verified = (0, Curve_1.verifySignature)(pubKey, message, signature);
        chai_1.assert.isTrue(verified);
    });
    it('calculateAgreement roundtrip', () => {
        const alice = (0, Curve_1.generateKeyPair)();
        const bob = (0, Curve_1.generateKeyPair)();
        const sharedSecretAlice = (0, Curve_1.calculateAgreement)(bob.pubKey, alice.privKey);
        const sharedSecretBob = (0, Curve_1.calculateAgreement)(alice.pubKey, bob.privKey);
        chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(sharedSecretAlice, sharedSecretBob));
    });
    describe('#isNonNegativeInteger', () => {
        it('returns false for -1, Infinity, NaN, a string, etc.', () => {
            chai_1.assert.isFalse((0, Curve_1.isNonNegativeInteger)(-1));
            chai_1.assert.isFalse((0, Curve_1.isNonNegativeInteger)(NaN));
            chai_1.assert.isFalse((0, Curve_1.isNonNegativeInteger)(Infinity));
            chai_1.assert.isFalse((0, Curve_1.isNonNegativeInteger)('woo!'));
        });
        it('returns true for 0 and positive integgers', () => {
            chai_1.assert.isTrue((0, Curve_1.isNonNegativeInteger)(0));
            chai_1.assert.isTrue((0, Curve_1.isNonNegativeInteger)(1));
            chai_1.assert.isTrue((0, Curve_1.isNonNegativeInteger)(3));
            chai_1.assert.isTrue((0, Curve_1.isNonNegativeInteger)(400000));
        });
    });
    describe('#generateSignedPrekey', () => {
        it('geernates proper signature for created signed prekeys', () => {
            const keyId = 4;
            const identityKeyPair = (0, Curve_1.generateKeyPair)();
            const signedPreKey = (0, Curve_1.generateSignedPreKey)(identityKeyPair, keyId);
            chai_1.assert.equal(keyId, signedPreKey.keyId);
            const verified = (0, Curve_1.verifySignature)(identityKeyPair.pubKey, signedPreKey.keyPair.pubKey, signedPreKey.signature);
            chai_1.assert.isTrue(verified);
        });
    });
    describe('#generatePrekey', () => {
        it('returns keys of the right length', () => {
            const keyId = 7;
            const preKey = (0, Curve_1.generatePreKey)(keyId);
            chai_1.assert.equal(keyId, preKey.keyId);
            chai_1.assert.equal(33, preKey.keyPair.pubKey.byteLength);
            chai_1.assert.equal(32, preKey.keyPair.privKey.byteLength);
        });
    });
    describe('#createKeyPair', () => {
        it('does not modify unclamped private key', () => {
            const initialHex = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
            const privateKey = Bytes.fromHex(initialHex);
            const copyOfPrivateKey = new Uint8Array(privateKey);
            chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(privateKey, copyOfPrivateKey), 'initial copy check');
            const keyPair = (0, Curve_1.createKeyPair)(privateKey);
            chai_1.assert.equal(32, keyPair.privKey.byteLength);
            chai_1.assert.equal(33, keyPair.pubKey.byteLength);
            // The original incoming key is not modified
            chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(privateKey, copyOfPrivateKey), 'second copy check');
            // But the keypair that comes out has indeed been updated
            chai_1.assert.notEqual(initialHex, Bytes.toHex(keyPair.privKey), 'keypair check');
            chai_1.assert.isFalse((0, Crypto_1.constantTimeEqual)(keyPair.privKey, privateKey), 'keypair vs incoming value');
        });
        it('does not modify clamped private key', () => {
            const initialHex = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
            const privateKey = Bytes.fromHex(initialHex);
            (0, Curve_1.clampPrivateKey)(privateKey);
            const postClampHex = Bytes.toHex(privateKey);
            const copyOfPrivateKey = new Uint8Array(privateKey);
            chai_1.assert.notEqual(postClampHex, initialHex, 'initial clamp check');
            chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(privateKey, copyOfPrivateKey), 'initial copy check');
            const keyPair = (0, Curve_1.createKeyPair)(privateKey);
            chai_1.assert.equal(32, keyPair.privKey.byteLength);
            chai_1.assert.equal(33, keyPair.pubKey.byteLength);
            // The original incoming key is not modified
            chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(privateKey, copyOfPrivateKey), 'second copy check');
            // The keypair that comes out hasn't been updated either
            chai_1.assert.equal(postClampHex, Bytes.toHex(keyPair.privKey), 'keypair check');
            chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(privateKey, keyPair.privKey), 'keypair vs incoming value');
        });
    });
});
