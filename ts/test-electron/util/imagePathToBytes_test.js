"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
//
const chai_1 = require("chai");
const path_1 = __importDefault(require("path"));
const imagePathToBytes_1 = require("../../util/imagePathToBytes");
describe('imagePathToBytes', () => {
    it('converts an image to an Bytes', async () => {
        const avatarPath = path_1.default.join(__dirname, '../../../fixtures/kitten-3-64-64.jpg');
        const buffer = await (0, imagePathToBytes_1.imagePathToBytes)(avatarPath);
        chai_1.assert.isDefined(buffer);
        (0, chai_1.assert)(buffer instanceof Uint8Array);
    });
});
