"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const MIME_1 = require("../../types/MIME");
const sniffImageMimeType_1 = require("../../util/sniffImageMimeType");
const canvasToBlob_1 = require("../../util/canvasToBlob");
describe('canvasToBlob', () => {
    let canvas;
    beforeEach(() => {
        canvas = document.createElement('canvas');
        canvas.width = 100;
        canvas.height = 200;
        const context = canvas.getContext('2d');
        if (!context) {
            throw new Error('Test setup error: cannot get canvas rendering context');
        }
        context.fillStyle = '#ff9900';
        context.fillRect(10, 10, 20, 20);
    });
    it('converts a canvas to an Blob, JPEG by default', async () => {
        const result = await (0, canvasToBlob_1.canvasToBlob)(canvas);
        chai_1.assert.strictEqual((0, sniffImageMimeType_1.sniffImageMimeType)(new Uint8Array(await result.arrayBuffer())), MIME_1.IMAGE_JPEG);
        // These are just smoke tests.
        chai_1.assert.instanceOf(result, Blob);
        chai_1.assert.isAtLeast(result.size, 50);
    });
    it('can convert a canvas to a PNG Blob', async () => {
        const result = await (0, canvasToBlob_1.canvasToBlob)(canvas, MIME_1.IMAGE_PNG);
        chai_1.assert.strictEqual((0, sniffImageMimeType_1.sniffImageMimeType)(new Uint8Array(await result.arrayBuffer())), MIME_1.IMAGE_PNG);
    });
});
