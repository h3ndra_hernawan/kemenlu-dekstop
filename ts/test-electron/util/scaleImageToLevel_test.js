"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const blueimp_load_image_1 = __importDefault(require("blueimp-load-image"));
const MIME_1 = require("../../types/MIME");
const scaleImageToLevel_1 = require("../../util/scaleImageToLevel");
describe('scaleImageToLevel', () => {
    // NOTE: These tests are incomplete.
    async function getBlob(path) {
        const response = await fetch(path);
        return response.blob();
    }
    it("doesn't scale images that are already small enough", async () => {
        const testCases = [
            {
                path: '../fixtures/kitten-1-64-64.jpg',
                contentType: MIME_1.IMAGE_JPEG,
                expectedWidth: 64,
                expectedHeight: 64,
            },
            {
                path: '../fixtures/20x200-yellow.png',
                contentType: MIME_1.IMAGE_PNG,
                expectedWidth: 20,
                expectedHeight: 200,
            },
        ];
        await Promise.all(testCases.map(async ({ path, contentType, expectedWidth, expectedHeight }) => {
            const blob = await getBlob(path);
            const scaled = await (0, scaleImageToLevel_1.scaleImageToLevel)(blob, contentType, true);
            const data = await (0, blueimp_load_image_1.default)(scaled.blob, { orientation: true });
            const { originalWidth: width, originalHeight: height } = data;
            chai_1.assert.strictEqual(width, expectedWidth);
            chai_1.assert.strictEqual(height, expectedHeight);
            chai_1.assert.strictEqual(scaled.contentType, contentType);
            chai_1.assert.strictEqual(scaled.blob.type, contentType);
        }));
    });
    it('removes EXIF data from small images', async () => {
        const original = await getBlob('../fixtures/kitten-2-64-64.jpg');
        chai_1.assert.isDefined((await (0, blueimp_load_image_1.default)(original, { meta: true, orientation: true })).exif, 'Test setup failure: expected fixture to have EXIF data');
        const scaled = await (0, scaleImageToLevel_1.scaleImageToLevel)(original, MIME_1.IMAGE_JPEG, true);
        chai_1.assert.isUndefined((await (0, blueimp_load_image_1.default)(scaled.blob, { meta: true, orientation: true })).exif);
    });
});
