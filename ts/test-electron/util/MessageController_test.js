"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const messages_1 = require("../../models/messages");
const MessageController_1 = require("../../util/MessageController");
describe('MessageController', () => {
    describe('filterBySentAt', () => {
        it('returns an empty iterable if no messages match', () => {
            const mc = new MessageController_1.MessageController();
            chai_1.assert.isEmpty([...mc.filterBySentAt(123)]);
        });
        it('returns all messages that match the timestamp', () => {
            const mc = new MessageController_1.MessageController();
            const message1 = new messages_1.MessageModel({
                conversationId: 'xyz',
                id: 'abc',
                received_at: 1,
                sent_at: 1234,
                timestamp: 9999,
                type: 'incoming',
            });
            const message2 = new messages_1.MessageModel({
                conversationId: 'xyz',
                id: 'def',
                received_at: 2,
                sent_at: 1234,
                timestamp: 9999,
                type: 'outgoing',
            });
            const message3 = new messages_1.MessageModel({
                conversationId: 'xyz',
                id: 'ignored',
                received_at: 3,
                sent_at: 5678,
                timestamp: 9999,
                type: 'outgoing',
            });
            mc.register(message1.id, message1);
            mc.register(message2.id, message2);
            // We deliberately register this message twice for testing.
            mc.register(message2.id, message2);
            mc.register(message3.id, message3);
            chai_1.assert.sameMembers([...mc.filterBySentAt(1234)], [message1, message2]);
            mc.unregister(message2.id);
            chai_1.assert.sameMembers([...mc.filterBySentAt(1234)], [message1]);
        });
    });
});
