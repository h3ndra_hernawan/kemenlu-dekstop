"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sendToGroup_1 = require("../../util/sendToGroup");
describe('sendToGroup', () => {
    describe('#_analyzeSenderKeyDevices', () => {
        function getDefaultDeviceList() {
            return [
                {
                    identifier: 'ident-guid-one',
                    id: 1,
                    registrationId: 11,
                },
                {
                    identifier: 'ident-guid-one',
                    id: 2,
                    registrationId: 22,
                },
                {
                    identifier: 'ident-guid-two',
                    id: 2,
                    registrationId: 33,
                },
            ];
        }
        it('returns nothing if new and previous lists are the same', () => {
            const memberDevices = getDefaultDeviceList();
            const devicesForSend = getDefaultDeviceList();
            const { newToMemberDevices, newToMemberUuids, removedFromMemberDevices, removedFromMemberUuids, } = (0, sendToGroup_1._analyzeSenderKeyDevices)(memberDevices, devicesForSend);
            chai_1.assert.isEmpty(newToMemberDevices);
            chai_1.assert.isEmpty(newToMemberUuids);
            chai_1.assert.isEmpty(removedFromMemberDevices);
            chai_1.assert.isEmpty(removedFromMemberUuids);
        });
        it('returns set of new devices', () => {
            const memberDevices = getDefaultDeviceList();
            const devicesForSend = getDefaultDeviceList();
            memberDevices.pop();
            memberDevices.pop();
            const { newToMemberDevices, newToMemberUuids, removedFromMemberDevices, removedFromMemberUuids, } = (0, sendToGroup_1._analyzeSenderKeyDevices)(memberDevices, devicesForSend);
            chai_1.assert.deepEqual(newToMemberDevices, [
                {
                    identifier: 'ident-guid-one',
                    id: 2,
                    registrationId: 22,
                },
                {
                    identifier: 'ident-guid-two',
                    id: 2,
                    registrationId: 33,
                },
            ]);
            chai_1.assert.deepEqual(newToMemberUuids, ['ident-guid-one', 'ident-guid-two']);
            chai_1.assert.isEmpty(removedFromMemberDevices);
            chai_1.assert.isEmpty(removedFromMemberUuids);
        });
        it('returns set of removed devices', () => {
            const memberDevices = getDefaultDeviceList();
            const devicesForSend = getDefaultDeviceList();
            devicesForSend.pop();
            devicesForSend.pop();
            const { newToMemberDevices, newToMemberUuids, removedFromMemberDevices, removedFromMemberUuids, } = (0, sendToGroup_1._analyzeSenderKeyDevices)(memberDevices, devicesForSend);
            chai_1.assert.isEmpty(newToMemberDevices);
            chai_1.assert.isEmpty(newToMemberUuids);
            chai_1.assert.deepEqual(removedFromMemberDevices, [
                {
                    identifier: 'ident-guid-one',
                    id: 2,
                    registrationId: 22,
                },
                {
                    identifier: 'ident-guid-two',
                    id: 2,
                    registrationId: 33,
                },
            ]);
            chai_1.assert.deepEqual(removedFromMemberUuids, [
                'ident-guid-one',
                'ident-guid-two',
            ]);
        });
        it('returns empty removals if partial send', () => {
            const memberDevices = getDefaultDeviceList();
            const devicesForSend = getDefaultDeviceList();
            devicesForSend.pop();
            devicesForSend.pop();
            const isPartialSend = true;
            const { newToMemberDevices, newToMemberUuids, removedFromMemberDevices, removedFromMemberUuids, } = (0, sendToGroup_1._analyzeSenderKeyDevices)(memberDevices, devicesForSend, isPartialSend);
            chai_1.assert.isEmpty(newToMemberDevices);
            chai_1.assert.isEmpty(newToMemberUuids);
            chai_1.assert.isEmpty(removedFromMemberDevices);
            chai_1.assert.isEmpty(removedFromMemberUuids);
        });
    });
    describe('#_waitForAll', () => {
        it('returns nothing if new and previous lists are the same', async () => {
            const task1 = () => Promise.resolve(1);
            const task2 = () => Promise.resolve(2);
            const task3 = () => Promise.resolve(3);
            const result = await (0, sendToGroup_1._waitForAll)({
                tasks: [task1, task2, task3],
                maxConcurrency: 1,
            });
            chai_1.assert.deepEqual(result, [1, 2, 3]);
        });
    });
});
