"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Bytes = __importStar(require("../../Bytes"));
const Crypto_1 = require("../../Crypto");
const UUID_1 = require("../../types/UUID");
const encryptProfileData_1 = require("../../util/encryptProfileData");
describe('encryptProfileData', () => {
    it('encrypts and decrypts properly', async () => {
        const keyBuffer = (0, Crypto_1.getRandomBytes)(32);
        const conversation = {
            aboutEmoji: '🐢',
            aboutText: 'I like turtles',
            familyName: 'Kid',
            firstName: 'Zombie',
            profileKey: Bytes.toBase64(keyBuffer),
            uuid: UUID_1.UUID.generate().toString(),
            // To satisfy TS
            acceptedMessageRequest: true,
            badges: [],
            id: '',
            isMe: true,
            sharedGroupNames: [],
            title: '',
            type: 'direct',
        };
        const [encrypted] = await (0, encryptProfileData_1.encryptProfileData)(conversation);
        chai_1.assert.isDefined(encrypted.version);
        chai_1.assert.isDefined(encrypted.name);
        chai_1.assert.isDefined(encrypted.commitment);
        const decryptedProfileNameBytes = (0, Crypto_1.decryptProfileName)(encrypted.name, keyBuffer);
        chai_1.assert.equal(Bytes.toString(decryptedProfileNameBytes.given), conversation.firstName);
        if (decryptedProfileNameBytes.family) {
            chai_1.assert.equal(Bytes.toString(decryptedProfileNameBytes.family), conversation.familyName);
        }
        else {
            chai_1.assert.isDefined(decryptedProfileNameBytes.family);
        }
        if (encrypted.about) {
            const decryptedAboutBytes = (0, Crypto_1.decryptProfile)(Bytes.fromBase64(encrypted.about), keyBuffer);
            chai_1.assert.equal(Bytes.toString((0, Crypto_1.trimForDisplay)(decryptedAboutBytes)), conversation.aboutText);
        }
        else {
            chai_1.assert.isDefined(encrypted.about);
        }
        if (encrypted.aboutEmoji) {
            const decryptedAboutEmojiBytes = await (0, Crypto_1.decryptProfile)(Bytes.fromBase64(encrypted.aboutEmoji), keyBuffer);
            chai_1.assert.equal(Bytes.toString((0, Crypto_1.trimForDisplay)(decryptedAboutEmojiBytes)), conversation.aboutEmoji);
        }
        else {
            chai_1.assert.isDefined(encrypted.aboutEmoji);
        }
    });
});
