"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const waitForOnline_1 = require("../../util/waitForOnline");
describe('waitForOnline', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });
    afterEach(() => {
        sandbox.restore();
    });
    function getFakeWindow() {
        const result = new EventTarget();
        sinon.stub(result, 'addEventListener');
        sinon.stub(result, 'removeEventListener');
        return result;
    }
    it("resolves immediately if you're online", async () => {
        const fakeNavigator = { onLine: true };
        const fakeWindow = getFakeWindow();
        await (0, waitForOnline_1.waitForOnline)(fakeNavigator, fakeWindow);
        sinon.assert.notCalled(fakeWindow.addEventListener);
        sinon.assert.notCalled(fakeWindow.removeEventListener);
    });
    it("if you're offline, resolves as soon as you're online (and cleans up listeners)", async () => {
        const fakeNavigator = { onLine: false };
        const fakeWindow = getFakeWindow();
        fakeWindow.addEventListener
            .withArgs('online')
            .callsFake((_eventName, callback) => {
            setTimeout(callback, 0);
        });
        let done = false;
        const promise = (async () => {
            await (0, waitForOnline_1.waitForOnline)(fakeNavigator, fakeWindow);
            done = true;
        })();
        chai_1.assert.isFalse(done);
        await promise;
        chai_1.assert.isTrue(done);
        sinon.assert.calledOnce(fakeWindow.addEventListener);
        sinon.assert.calledOnce(fakeWindow.removeEventListener);
    });
    it("resolves immediately if you're online when passed a timeout", async () => {
        const fakeNavigator = { onLine: true };
        const fakeWindow = getFakeWindow();
        await (0, waitForOnline_1.waitForOnline)(fakeNavigator, fakeWindow, { timeout: 1234 });
        sinon.assert.notCalled(fakeWindow.addEventListener);
        sinon.assert.notCalled(fakeWindow.removeEventListener);
    });
    it("resolves immediately if you're online even if passed a timeout of 0", async () => {
        const fakeNavigator = { onLine: true };
        const fakeWindow = getFakeWindow();
        await (0, waitForOnline_1.waitForOnline)(fakeNavigator, fakeWindow, { timeout: 0 });
        sinon.assert.notCalled(fakeWindow.addEventListener);
        sinon.assert.notCalled(fakeWindow.removeEventListener);
    });
    it("if you're offline, resolves as soon as you're online if it happens before the timeout", async () => {
        const clock = sandbox.useFakeTimers();
        const fakeNavigator = { onLine: false };
        const fakeWindow = getFakeWindow();
        fakeWindow.addEventListener
            .withArgs('online')
            .callsFake((_eventName, callback) => {
            setTimeout(callback, 1000);
        });
        let done = false;
        (async () => {
            await (0, waitForOnline_1.waitForOnline)(fakeNavigator, fakeWindow, { timeout: 9999 });
            done = true;
        })();
        await clock.tickAsync(600);
        chai_1.assert.isFalse(done);
        await clock.tickAsync(500);
        chai_1.assert.isTrue(done);
    });
    it('rejects if too much time has passed, and cleans up listeners', async () => {
        const clock = sandbox.useFakeTimers();
        const fakeNavigator = { onLine: false };
        const fakeWindow = getFakeWindow();
        fakeWindow.addEventListener
            .withArgs('online')
            .callsFake((_eventName, callback) => {
            setTimeout(callback, 9999);
        });
        const promise = (0, waitForOnline_1.waitForOnline)(fakeNavigator, fakeWindow, {
            timeout: 100,
        });
        await clock.tickAsync(500);
        await chai_1.assert.isRejected(promise);
        sinon.assert.calledOnce(fakeWindow.removeEventListener);
    });
    it('rejects if offline and passed a timeout of 0', async () => {
        const fakeNavigator = { onLine: false };
        const fakeWindow = getFakeWindow();
        fakeWindow.addEventListener
            .withArgs('online')
            .callsFake((_eventName, callback) => {
            setTimeout(callback, 9999);
        });
        const promise = (0, waitForOnline_1.waitForOnline)(fakeNavigator, fakeWindow, { timeout: 0 });
        await chai_1.assert.isRejected(promise);
    });
});
