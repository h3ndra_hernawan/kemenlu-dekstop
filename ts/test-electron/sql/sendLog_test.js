"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
const chai_1 = require("chai");
const Client_1 = __importDefault(require("../../sql/Client"));
const Crypto_1 = require("../../Crypto");
const { _getAllSentProtoMessageIds, _getAllSentProtoRecipients, deleteSentProtoByMessageId, deleteSentProtoRecipient, deleteSentProtosOlderThan, getAllSentProtos, getSentProtoByRecipient, insertProtoRecipients, insertSentProto, removeAllSentProtos, removeMessage, saveMessage, } = Client_1.default;
describe('sendLog', () => {
    beforeEach(async () => {
        await removeAllSentProtos();
    });
    it('roundtrips with insertSentProto/getAllSentProtos', async () => {
        const bytes = (0, Crypto_1.getRandomBytes)(128);
        const timestamp = Date.now();
        const proto = {
            contentHint: 1,
            proto: bytes,
            timestamp,
        };
        await insertSentProto(proto, {
            messageIds: [(0, uuid_1.v4)()],
            recipients: {
                [(0, uuid_1.v4)()]: [1, 2],
            },
        });
        const allProtos = await getAllSentProtos();
        chai_1.assert.lengthOf(allProtos, 1);
        const actual = allProtos[0];
        chai_1.assert.strictEqual(actual.contentHint, proto.contentHint);
        chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(actual.proto, proto.proto));
        chai_1.assert.strictEqual(actual.timestamp, proto.timestamp);
        await removeAllSentProtos();
        chai_1.assert.lengthOf(await getAllSentProtos(), 0);
    });
    it('cascades deletes into both tables with foreign keys', async () => {
        chai_1.assert.lengthOf(await getAllSentProtos(), 0);
        chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 0);
        chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 0);
        const bytes = (0, Crypto_1.getRandomBytes)(128);
        const timestamp = Date.now();
        const proto = {
            contentHint: 1,
            proto: bytes,
            timestamp,
        };
        await insertSentProto(proto, {
            messageIds: [(0, uuid_1.v4)(), (0, uuid_1.v4)()],
            recipients: {
                [(0, uuid_1.v4)()]: [1, 2],
                [(0, uuid_1.v4)()]: [1],
            },
        });
        chai_1.assert.lengthOf(await getAllSentProtos(), 1);
        chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 2);
        chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 3);
        await removeAllSentProtos();
        chai_1.assert.lengthOf(await getAllSentProtos(), 0);
        chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 0);
        chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 0);
    });
    it('trigger deletes payload when referenced message is deleted', async () => {
        const id = (0, uuid_1.v4)();
        const timestamp = Date.now();
        await saveMessage({
            id,
            body: 'some text',
            conversationId: (0, uuid_1.v4)(),
            received_at: timestamp,
            sent_at: timestamp,
            timestamp,
            type: 'outgoing',
        }, { forceSave: true });
        const bytes = (0, Crypto_1.getRandomBytes)(128);
        const proto = {
            contentHint: 1,
            proto: bytes,
            timestamp,
        };
        await insertSentProto(proto, {
            messageIds: [id],
            recipients: {
                [(0, uuid_1.v4)()]: [1, 2],
            },
        });
        const allProtos = await getAllSentProtos();
        chai_1.assert.lengthOf(allProtos, 1);
        const actual = allProtos[0];
        chai_1.assert.strictEqual(actual.timestamp, proto.timestamp);
        await removeMessage(id, { Message: window.Whisper.Message });
        chai_1.assert.lengthOf(await getAllSentProtos(), 0);
    });
    describe('#insertSentProto', () => {
        it('supports adding duplicates', async () => {
            const timestamp = Date.now();
            const messageIds = [(0, uuid_1.v4)()];
            const recipients = {
                [(0, uuid_1.v4)()]: [1],
            };
            const proto1 = {
                contentHint: 7,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            const proto2 = {
                contentHint: 9,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            chai_1.assert.lengthOf(await getAllSentProtos(), 0);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 0);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 0);
            await insertSentProto(proto1, { messageIds, recipients });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 1);
            await insertSentProto(proto2, { messageIds, recipients });
            chai_1.assert.lengthOf(await getAllSentProtos(), 2);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 2);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 2);
        });
    });
    describe('#insertProtoRecipients', () => {
        it('handles duplicates, adding new recipients if needed', async () => {
            const timestamp = Date.now();
            const messageIds = [(0, uuid_1.v4)()];
            const proto = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            chai_1.assert.lengthOf(await getAllSentProtos(), 0);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 0);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 0);
            const id = await insertSentProto(proto, {
                messageIds,
                recipients: {
                    [(0, uuid_1.v4)()]: [1],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 1);
            const recipientUuid = (0, uuid_1.v4)();
            await insertProtoRecipients({
                id,
                recipientUuid,
                deviceIds: [1, 2],
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 3);
        });
    });
    describe('#deleteSentProtosOlderThan', () => {
        it('deletes all older timestamps', async () => {
            const timestamp = Date.now();
            const proto1 = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp: timestamp + 10,
            };
            const proto2 = {
                contentHint: 2,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            const proto3 = {
                contentHint: 0,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp: timestamp - 15,
            };
            await insertSentProto(proto1, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [(0, uuid_1.v4)()]: [1],
                },
            });
            await insertSentProto(proto2, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [(0, uuid_1.v4)()]: [1, 2],
                },
            });
            await insertSentProto(proto3, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [(0, uuid_1.v4)()]: [1, 2, 3],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 3);
            await deleteSentProtosOlderThan(timestamp);
            const allProtos = await getAllSentProtos();
            chai_1.assert.lengthOf(allProtos, 2);
            const actual1 = allProtos[0];
            chai_1.assert.strictEqual(actual1.contentHint, proto1.contentHint);
            chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(actual1.proto, proto1.proto));
            chai_1.assert.strictEqual(actual1.timestamp, proto1.timestamp);
            const actual2 = allProtos[1];
            chai_1.assert.strictEqual(actual2.contentHint, proto2.contentHint);
            chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(actual2.proto, proto2.proto));
            chai_1.assert.strictEqual(actual2.timestamp, proto2.timestamp);
        });
    });
    describe('#deleteSentProtoByMessageId', () => {
        it('deletes all records releated to that messageId', async () => {
            chai_1.assert.lengthOf(await getAllSentProtos(), 0);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 0);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 0);
            const messageId = (0, uuid_1.v4)();
            const timestamp = Date.now();
            const proto1 = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            const proto2 = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp: timestamp - 10,
            };
            const proto3 = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp: timestamp - 20,
            };
            await insertSentProto(proto1, {
                messageIds: [messageId, (0, uuid_1.v4)()],
                recipients: {
                    [(0, uuid_1.v4)()]: [1, 2],
                    [(0, uuid_1.v4)()]: [1],
                },
            });
            await insertSentProto(proto2, {
                messageIds: [messageId],
                recipients: {
                    [(0, uuid_1.v4)()]: [1],
                },
            });
            await insertSentProto(proto3, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [(0, uuid_1.v4)()]: [1],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 3);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 4);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 5);
            await deleteSentProtoByMessageId(messageId);
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 1);
        });
    });
    describe('#deleteSentProtoRecipient', () => {
        it('does not delete payload if recipient remains', async () => {
            const timestamp = Date.now();
            const recipientUuid1 = (0, uuid_1.v4)();
            const recipientUuid2 = (0, uuid_1.v4)();
            const proto = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            await insertSentProto(proto, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [recipientUuid1]: [1, 2],
                    [recipientUuid2]: [1],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 3);
            await deleteSentProtoRecipient({
                timestamp,
                recipientUuid: recipientUuid1,
                deviceId: 1,
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 2);
        });
        it('deletes payload if no recipients remain', async () => {
            const timestamp = Date.now();
            const recipientUuid1 = (0, uuid_1.v4)();
            const recipientUuid2 = (0, uuid_1.v4)();
            const proto = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            await insertSentProto(proto, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [recipientUuid1]: [1, 2],
                    [recipientUuid2]: [1],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 3);
            await deleteSentProtoRecipient({
                timestamp,
                recipientUuid: recipientUuid1,
                deviceId: 1,
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 2);
            await deleteSentProtoRecipient({
                timestamp,
                recipientUuid: recipientUuid1,
                deviceId: 2,
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 1);
            await deleteSentProtoRecipient({
                timestamp,
                recipientUuid: recipientUuid2,
                deviceId: 1,
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 0);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 0);
        });
        it('deletes multiple recipients in a single transaction', async () => {
            const timestamp = Date.now();
            const recipientUuid1 = (0, uuid_1.v4)();
            const recipientUuid2 = (0, uuid_1.v4)();
            const proto = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            await insertSentProto(proto, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [recipientUuid1]: [1, 2],
                    [recipientUuid2]: [1],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 3);
            await deleteSentProtoRecipient([
                {
                    timestamp,
                    recipientUuid: recipientUuid1,
                    deviceId: 1,
                },
                {
                    timestamp,
                    recipientUuid: recipientUuid1,
                    deviceId: 2,
                },
                {
                    timestamp,
                    recipientUuid: recipientUuid2,
                    deviceId: 1,
                },
            ]);
            chai_1.assert.lengthOf(await getAllSentProtos(), 0);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 0);
        });
    });
    describe('#getSentProtoByRecipient', () => {
        it('returns matching payload', async () => {
            const timestamp = Date.now();
            const recipientUuid = (0, uuid_1.v4)();
            const messageIds = [(0, uuid_1.v4)(), (0, uuid_1.v4)()];
            const proto = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            await insertSentProto(proto, {
                messageIds,
                recipients: {
                    [recipientUuid]: [1, 2],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 2);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 2);
            const actual = await getSentProtoByRecipient({
                now: timestamp,
                timestamp,
                recipientUuid,
            });
            if (!actual) {
                throw new Error('Failed to fetch proto!');
            }
            chai_1.assert.strictEqual(actual.contentHint, proto.contentHint);
            chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(actual.proto, proto.proto));
            chai_1.assert.strictEqual(actual.timestamp, proto.timestamp);
            chai_1.assert.sameMembers(actual.messageIds, messageIds);
        });
        it('returns matching payload with no messageIds', async () => {
            const timestamp = Date.now();
            const recipientUuid = (0, uuid_1.v4)();
            const proto = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            await insertSentProto(proto, {
                messageIds: [],
                recipients: {
                    [recipientUuid]: [1, 2],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 2);
            chai_1.assert.lengthOf(await _getAllSentProtoMessageIds(), 0);
            const actual = await getSentProtoByRecipient({
                now: timestamp,
                timestamp,
                recipientUuid,
            });
            if (!actual) {
                throw new Error('Failed to fetch proto!');
            }
            chai_1.assert.strictEqual(actual.contentHint, proto.contentHint);
            chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(actual.proto, proto.proto));
            chai_1.assert.strictEqual(actual.timestamp, proto.timestamp);
            chai_1.assert.deepEqual(actual.messageIds, []);
        });
        it('returns nothing if payload does not have recipient', async () => {
            const timestamp = Date.now();
            const recipientUuid = (0, uuid_1.v4)();
            const proto = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            await insertSentProto(proto, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [recipientUuid]: [1, 2],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 2);
            const actual = await getSentProtoByRecipient({
                now: timestamp,
                timestamp,
                recipientUuid: (0, uuid_1.v4)(),
            });
            chai_1.assert.isUndefined(actual);
        });
        it('returns nothing if timestamp does not match', async () => {
            const timestamp = Date.now();
            const recipientUuid = (0, uuid_1.v4)();
            const proto = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            await insertSentProto(proto, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [recipientUuid]: [1, 2],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 2);
            const actual = await getSentProtoByRecipient({
                now: timestamp,
                timestamp: timestamp + 1,
                recipientUuid,
            });
            chai_1.assert.isUndefined(actual);
        });
        it('returns nothing if timestamp proto is too old', async () => {
            const TWO_DAYS = 2 * 24 * 60 * 60 * 1000;
            const timestamp = Date.now();
            const recipientUuid = (0, uuid_1.v4)();
            const proto = {
                contentHint: 1,
                proto: (0, Crypto_1.getRandomBytes)(128),
                timestamp,
            };
            await insertSentProto(proto, {
                messageIds: [(0, uuid_1.v4)()],
                recipients: {
                    [recipientUuid]: [1, 2],
                },
            });
            chai_1.assert.lengthOf(await getAllSentProtos(), 1);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 2);
            const actual = await getSentProtoByRecipient({
                now: timestamp + TWO_DAYS,
                timestamp,
                recipientUuid,
            });
            chai_1.assert.isUndefined(actual);
            chai_1.assert.lengthOf(await getAllSentProtos(), 0);
            chai_1.assert.lengthOf(await _getAllSentProtoRecipients(), 0);
        });
    });
});
