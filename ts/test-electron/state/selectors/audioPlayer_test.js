"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const audioPlayer_1 = require("../../../state/ducks/audioPlayer");
const noop_1 = require("../../../state/ducks/noop");
const audioPlayer_2 = require("../../../state/selectors/audioPlayer");
const reducer_1 = require("../../../state/reducer");
describe('state/selectors/audioPlayer', () => {
    const getEmptyRootState = () => {
        return (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
    };
    describe('isPaused', () => {
        it('returns true if state.audioPlayer.activeAudioID is undefined', () => {
            const state = getEmptyRootState();
            chai_1.assert.isTrue((0, audioPlayer_2.isPaused)(state));
        });
        it('returns false if state.audioPlayer.activeAudioID is not undefined', () => {
            const state = getEmptyRootState();
            const updated = (0, reducer_1.reducer)(state, audioPlayer_1.actions.setActiveAudioID('id', 'context'));
            chai_1.assert.isFalse((0, audioPlayer_2.isPaused)(updated));
        });
    });
});
