"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const reducer_1 = require("../../../state/reducer");
const noop_1 = require("../../../state/ducks/noop");
const Calling_1 = require("../../../types/Calling");
const calling_1 = require("../../../state/selectors/calling");
const calling_2 = require("../../../state/ducks/calling");
describe('state/selectors/calling', () => {
    const getEmptyRootState = () => (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
    const getCallingState = (calling) => (Object.assign(Object.assign({}, getEmptyRootState()), { calling }));
    const stateWithDirectCall = Object.assign(Object.assign({}, (0, calling_2.getEmptyState)()), { callsByConversation: {
            'fake-direct-call-conversation-id': {
                callMode: Calling_1.CallMode.Direct,
                conversationId: 'fake-direct-call-conversation-id',
                callState: Calling_1.CallState.Accepted,
                isIncoming: false,
                isVideoCall: false,
                hasRemoteVideo: false,
            },
        } });
    const stateWithActiveDirectCall = Object.assign(Object.assign({}, stateWithDirectCall), { activeCallState: {
            conversationId: 'fake-direct-call-conversation-id',
            hasLocalAudio: true,
            hasLocalVideo: false,
            isInSpeakerView: false,
            showParticipantsList: false,
            safetyNumberChangedUuids: [],
            outgoingRing: true,
            pip: false,
            settingsDialogOpen: false,
        } });
    const incomingDirectCall = {
        callMode: Calling_1.CallMode.Direct,
        conversationId: 'fake-direct-call-conversation-id',
        callState: Calling_1.CallState.Ringing,
        isIncoming: true,
        isVideoCall: false,
        hasRemoteVideo: false,
    };
    const stateWithIncomingDirectCall = Object.assign(Object.assign({}, (0, calling_2.getEmptyState)()), { callsByConversation: {
            'fake-direct-call-conversation-id': incomingDirectCall,
        } });
    const incomingGroupCall = {
        callMode: Calling_1.CallMode.Group,
        conversationId: 'fake-group-call-conversation-id',
        connectionState: Calling_1.GroupCallConnectionState.NotConnected,
        joinState: Calling_1.GroupCallJoinState.NotJoined,
        peekInfo: {
            uuids: ['c75b51da-d484-4674-9b2c-cc11de00e227'],
            creatorUuid: 'c75b51da-d484-4674-9b2c-cc11de00e227',
            maxDevices: Infinity,
            deviceCount: 1,
        },
        remoteParticipants: [],
        ringId: BigInt(123),
        ringerUuid: 'c75b51da-d484-4674-9b2c-cc11de00e227',
    };
    const stateWithIncomingGroupCall = Object.assign(Object.assign({}, (0, calling_2.getEmptyState)()), { callsByConversation: {
            'fake-group-call-conversation-id': incomingGroupCall,
        } });
    describe('getCallsByConversation', () => {
        it('returns state.calling.callsByConversation', () => {
            chai_1.assert.deepEqual((0, calling_1.getCallsByConversation)(getEmptyRootState()), {});
            chai_1.assert.deepEqual((0, calling_1.getCallsByConversation)(getCallingState(stateWithDirectCall)), {
                'fake-direct-call-conversation-id': {
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: 'fake-direct-call-conversation-id',
                    callState: Calling_1.CallState.Accepted,
                    isIncoming: false,
                    isVideoCall: false,
                    hasRemoteVideo: false,
                },
            });
        });
    });
    describe('getCallSelector', () => {
        it('returns a selector that returns undefined if selecting a conversation with no call', () => {
            chai_1.assert.isUndefined((0, calling_1.getCallSelector)(getEmptyRootState())('conversation-id'));
        });
        it("returns a selector that returns a conversation's call", () => {
            chai_1.assert.deepEqual((0, calling_1.getCallSelector)(getCallingState(stateWithDirectCall))('fake-direct-call-conversation-id'), {
                callMode: Calling_1.CallMode.Direct,
                conversationId: 'fake-direct-call-conversation-id',
                callState: Calling_1.CallState.Accepted,
                isIncoming: false,
                isVideoCall: false,
                hasRemoteVideo: false,
            });
        });
    });
    describe('getIncomingCall', () => {
        it('returns undefined if there are no calls', () => {
            chai_1.assert.isUndefined((0, calling_1.getIncomingCall)(getEmptyRootState()));
        });
        it('returns undefined if there is no incoming call', () => {
            chai_1.assert.isUndefined((0, calling_1.getIncomingCall)(getCallingState(stateWithDirectCall)));
            chai_1.assert.isUndefined((0, calling_1.getIncomingCall)(getCallingState(stateWithActiveDirectCall)));
        });
        it('returns undefined if there is a group call with no peeked participants', () => {
            const state = Object.assign(Object.assign({}, stateWithIncomingGroupCall), { callsByConversation: {
                    'fake-group-call-conversation-id': Object.assign(Object.assign({}, incomingGroupCall), { peekInfo: {
                            uuids: [],
                            maxDevices: Infinity,
                            deviceCount: 1,
                        } }),
                } });
            chai_1.assert.isUndefined((0, calling_1.getIncomingCall)(getCallingState(state)));
        });
        it('returns an incoming direct call', () => {
            chai_1.assert.deepEqual((0, calling_1.getIncomingCall)(getCallingState(stateWithIncomingDirectCall)), incomingDirectCall);
        });
        it('returns an incoming group call', () => {
            chai_1.assert.deepEqual((0, calling_1.getIncomingCall)(getCallingState(stateWithIncomingGroupCall)), incomingGroupCall);
        });
    });
    describe('isInCall', () => {
        it('returns should be false if we are not in a call', () => {
            chai_1.assert.isFalse((0, calling_1.isInCall)(getEmptyRootState()));
        });
        it('should be true if we are in a call', () => {
            chai_1.assert.isTrue((0, calling_1.isInCall)(getCallingState(stateWithActiveDirectCall)));
        });
    });
});
