"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const moment = __importStar(require("moment"));
const uuid_1 = require("uuid");
const MessageSendState_1 = require("../../../messages/MessageSendState");
const message_1 = require("../../../state/selectors/message");
describe('state/selectors/messages', () => {
    let ourConversationId;
    beforeEach(() => {
        ourConversationId = (0, uuid_1.v4)();
    });
    describe('canDeleteForEveryone', () => {
        it('returns false for incoming messages', () => {
            const message = {
                type: 'incoming',
                sent_at: Date.now() - 1000,
            };
            chai_1.assert.isFalse((0, message_1.canDeleteForEveryone)(message));
        });
        it('returns false for messages that were already deleted for everyone', () => {
            const message = {
                type: 'outgoing',
                deletedForEveryone: true,
                sent_at: Date.now() - 1000,
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Read,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Delivered,
                        updatedAt: Date.now(),
                    },
                },
            };
            chai_1.assert.isFalse((0, message_1.canDeleteForEveryone)(message));
        });
        it('returns false for messages that were are too old to delete', () => {
            const message = {
                type: 'outgoing',
                sent_at: Date.now() - moment.duration(4, 'hours').asMilliseconds(),
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Read,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Delivered,
                        updatedAt: Date.now(),
                    },
                },
            };
            chai_1.assert.isFalse((0, message_1.canDeleteForEveryone)(message));
        });
        it('returns false for messages that failed to send to anyone', () => {
            const message = {
                type: 'outgoing',
                sent_at: Date.now() - 1000,
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Failed,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Failed,
                        updatedAt: Date.now(),
                    },
                },
            };
            chai_1.assert.isFalse((0, message_1.canDeleteForEveryone)(message));
        });
        it('returns true for messages that meet all criteria for deletion', () => {
            const message = {
                type: 'outgoing',
                sent_at: Date.now() - 1000,
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Failed,
                        updatedAt: Date.now(),
                    },
                },
            };
            chai_1.assert.isTrue((0, message_1.canDeleteForEveryone)(message));
        });
    });
    describe('canReact', () => {
        const defaultConversation = {
            id: (0, uuid_1.v4)(),
            type: 'direct',
            title: 'Test conversation',
            isMe: false,
            sharedGroupNames: [],
            acceptedMessageRequest: true,
            badges: [],
        };
        it('returns false for disabled v1 groups', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'incoming',
            };
            const getConversationById = () => (Object.assign(Object.assign({}, defaultConversation), { type: 'group', isGroupV1AndDisabled: true }));
            chai_1.assert.isFalse((0, message_1.canReact)(message, ourConversationId, getConversationById));
        });
        // NOTE: This is missing a test for mandatory profile sharing.
        it('returns false if the message was deleted for everyone', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'incoming',
                deletedForEveryone: true,
            };
            const getConversationById = () => defaultConversation;
            chai_1.assert.isFalse((0, message_1.canReact)(message, ourConversationId, getConversationById));
        });
        it('returns false for outgoing messages that have not been sent', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'outgoing',
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                },
            };
            const getConversationById = () => defaultConversation;
            chai_1.assert.isFalse((0, message_1.canReact)(message, ourConversationId, getConversationById));
        });
        it('returns true for outgoing messages that are only sent to yourself', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'outgoing',
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                },
            };
            const getConversationById = () => defaultConversation;
            chai_1.assert.isTrue((0, message_1.canReact)(message, ourConversationId, getConversationById));
        });
        it('returns true for outgoing messages that have been sent to at least one person', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'outgoing',
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                },
            };
            const getConversationById = () => (Object.assign(Object.assign({}, defaultConversation), { type: 'group' }));
            chai_1.assert.isTrue((0, message_1.canReact)(message, ourConversationId, getConversationById));
        });
        it('returns true for incoming messages', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'incoming',
            };
            const getConversationById = () => defaultConversation;
            chai_1.assert.isTrue((0, message_1.canReact)(message, ourConversationId, getConversationById));
        });
    });
    describe('canReply', () => {
        const defaultConversation = {
            id: (0, uuid_1.v4)(),
            type: 'direct',
            title: 'Test conversation',
            isMe: false,
            sharedGroupNames: [],
            acceptedMessageRequest: true,
            badges: [],
        };
        it('returns false for disabled v1 groups', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'incoming',
            };
            const getConversationById = () => (Object.assign(Object.assign({}, defaultConversation), { type: 'group', isGroupV1AndDisabled: true }));
            chai_1.assert.isFalse((0, message_1.canReply)(message, ourConversationId, getConversationById));
        });
        // NOTE: This is missing a test for mandatory profile sharing.
        it('returns false if the message was deleted for everyone', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'incoming',
                deletedForEveryone: true,
            };
            const getConversationById = () => defaultConversation;
            chai_1.assert.isFalse((0, message_1.canReply)(message, ourConversationId, getConversationById));
        });
        it('returns false for outgoing messages that have not been sent', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'outgoing',
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                },
            };
            const getConversationById = () => defaultConversation;
            chai_1.assert.isFalse((0, message_1.canReply)(message, ourConversationId, getConversationById));
        });
        it('returns true for outgoing messages that are only sent to yourself', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'outgoing',
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                },
            };
            const getConversationById = () => defaultConversation;
            chai_1.assert.isTrue((0, message_1.canReply)(message, ourConversationId, getConversationById));
        });
        it('returns true for outgoing messages that have been sent to at least one person', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'outgoing',
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                },
            };
            const getConversationById = () => (Object.assign(Object.assign({}, defaultConversation), { type: 'group' }));
            chai_1.assert.isTrue((0, message_1.canReply)(message, ourConversationId, getConversationById));
        });
        it('returns true for incoming messages', () => {
            const message = {
                conversationId: 'fake-conversation-id',
                type: 'incoming',
            };
            const getConversationById = () => defaultConversation;
            chai_1.assert.isTrue((0, message_1.canReply)(message, ourConversationId, getConversationById));
        });
    });
    describe('getMessagePropStatus', () => {
        const createMessage = (overrides) => (Object.assign({ type: 'outgoing' }, overrides));
        it('returns undefined for incoming messages', () => {
            const message = createMessage({ type: 'incoming' });
            chai_1.assert.isUndefined((0, message_1.getMessagePropStatus)(message, ourConversationId));
        });
        it('returns "paused" for messages with challenges', () => {
            const challengeError = Object.assign(new Error('a challenge'), {
                name: 'SendMessageChallengeError',
                retryAfter: 123,
                data: {},
            });
            const message = createMessage({ errors: [challengeError] });
            chai_1.assert.strictEqual((0, message_1.getMessagePropStatus)(message, ourConversationId), 'paused');
        });
        it('returns "partial-sent" if the message has errors but was sent to at least one person', () => {
            const message = createMessage({
                errors: [new Error('whoopsie')],
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Delivered,
                        updatedAt: Date.now(),
                    },
                },
            });
            chai_1.assert.strictEqual((0, message_1.getMessagePropStatus)(message, ourConversationId), 'partial-sent');
        });
        it('returns "error" if the message has errors and has not been sent', () => {
            const message = createMessage({
                errors: [new Error('whoopsie')],
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                },
            });
            chai_1.assert.strictEqual((0, message_1.getMessagePropStatus)(message, ourConversationId), 'error');
        });
        it('returns "viewed" if the message is just for you and has been sent', () => {
            const message = createMessage({
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                },
            });
            chai_1.assert.strictEqual((0, message_1.getMessagePropStatus)(message, ourConversationId), 'viewed');
        });
        it('returns "viewed" if the message was viewed by at least one person', () => {
            const message = createMessage({
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Viewed,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Read,
                        updatedAt: Date.now(),
                    },
                },
            });
            chai_1.assert.strictEqual((0, message_1.getMessagePropStatus)(message, ourConversationId), 'viewed');
        });
        it('returns "read" if the message was read by at least one person', () => {
            const message = createMessage({
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Read,
                        updatedAt: Date.now(),
                    },
                },
            });
            chai_1.assert.strictEqual((0, message_1.getMessagePropStatus)(message, ourConversationId), 'read');
        });
        it('returns "delivered" if the message was delivered to at least one person, but no "higher"', () => {
            const message = createMessage({
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Delivered,
                        updatedAt: Date.now(),
                    },
                },
            });
            chai_1.assert.strictEqual((0, message_1.getMessagePropStatus)(message, ourConversationId), 'delivered');
        });
        it('returns "sent" if the message was sent to at least one person, but no "higher"', () => {
            const message = createMessage({
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                },
            });
            chai_1.assert.strictEqual((0, message_1.getMessagePropStatus)(message, ourConversationId), 'sent');
        });
        it('returns "sending" if the message has not been sent yet, even if it has been synced to yourself', () => {
            const message = createMessage({
                sendStateByConversationId: {
                    [ourConversationId]: {
                        status: MessageSendState_1.SendStatus.Sent,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                    [(0, uuid_1.v4)()]: {
                        status: MessageSendState_1.SendStatus.Pending,
                        updatedAt: Date.now(),
                    },
                },
            });
            chai_1.assert.strictEqual((0, message_1.getMessagePropStatus)(message, ourConversationId), 'sending');
        });
    });
    describe('isEndSession', () => {
        it('checks if it is end of the session', () => {
            chai_1.assert.isFalse((0, message_1.isEndSession)({}));
            chai_1.assert.isFalse((0, message_1.isEndSession)({ flags: undefined }));
            chai_1.assert.isFalse((0, message_1.isEndSession)({ flags: 0 }));
            chai_1.assert.isFalse((0, message_1.isEndSession)({ flags: 2 }));
            chai_1.assert.isFalse((0, message_1.isEndSession)({ flags: 4 }));
            chai_1.assert.isTrue((0, message_1.isEndSession)({ flags: 1 }));
        });
    });
    describe('isGroupUpdate', () => {
        it('checks if is group update', () => {
            chai_1.assert.isFalse((0, message_1.isGroupUpdate)({}));
            chai_1.assert.isFalse((0, message_1.isGroupUpdate)({ group_update: undefined }));
            chai_1.assert.isTrue((0, message_1.isGroupUpdate)({ group_update: { left: 'You' } }));
        });
    });
    describe('isIncoming', () => {
        it('checks if is incoming message', () => {
            chai_1.assert.isFalse((0, message_1.isIncoming)({ type: 'outgoing' }));
            chai_1.assert.isFalse((0, message_1.isIncoming)({ type: 'call-history' }));
            chai_1.assert.isTrue((0, message_1.isIncoming)({ type: 'incoming' }));
        });
    });
    describe('isOutgoing', () => {
        it('checks if is outgoing message', () => {
            chai_1.assert.isFalse((0, message_1.isOutgoing)({ type: 'incoming' }));
            chai_1.assert.isFalse((0, message_1.isOutgoing)({ type: 'call-history' }));
            chai_1.assert.isTrue((0, message_1.isOutgoing)({ type: 'outgoing' }));
        });
    });
});
