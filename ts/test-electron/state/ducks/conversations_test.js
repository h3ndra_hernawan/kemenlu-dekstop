"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const uuid_1 = require("uuid");
const lodash_1 = require("lodash");
const fp_1 = require("lodash/fp");
const reducer_1 = require("../../../state/reducer");
const noop_1 = require("../../../state/ducks/noop");
const conversationsEnums_1 = require("../../../state/ducks/conversationsEnums");
const conversations_1 = require("../../../state/ducks/conversations");
const MessageReadStatus_1 = require("../../../messages/MessageReadStatus");
const contactSpoofing_1 = require("../../../util/contactSpoofing");
const Calling_1 = require("../../../types/Calling");
const UUID_1 = require("../../../types/UUID");
const groups = __importStar(require("../../../groups"));
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const Avatar_1 = require("../../../types/Avatar");
const defaultComposerStates_1 = require("../../../test-both/helpers/defaultComposerStates");
const { cantAddContactToGroup, clearGroupCreationError, clearInvitedUuidsForNewlyCreatedGroup, closeCantAddContactToGroupModal, closeContactSpoofingReview, closeMaximumGroupSizeModal, closeRecommendedGroupSizeModal, createGroup, messageSizeChanged, messageStoppedByMissingVerification, openConversationInternal, repairNewestMessage, repairOldestMessage, setComposeGroupAvatar, setComposeGroupName, setComposeSearchTerm, setPreJoinConversation, showArchivedConversations, showInbox, startComposing, showChooseGroupMembers, startSettingGroupMetadata, resetAllChatColors, reviewGroupMemberNameCollision, reviewMessageRequestNameCollision, toggleConversationInChooseMembers, } = conversations_1.actions;
describe('both/state/ducks/conversations', () => {
    const getEmptyRootState = () => (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
    let sinonSandbox;
    let createGroupStub;
    beforeEach(() => {
        sinonSandbox = sinon.createSandbox();
        sinonSandbox.stub(window.Whisper.events, 'trigger');
        createGroupStub = sinonSandbox.stub(groups, 'createGroupV2');
    });
    afterEach(() => {
        sinonSandbox.restore();
    });
    describe('helpers', () => {
        describe('getConversationCallMode', () => {
            const fakeConversation = (0, getDefaultConversation_1.getDefaultConversation)();
            it("returns CallMode.None if you've left the conversation", () => {
                chai_1.assert.strictEqual((0, conversations_1.getConversationCallMode)(Object.assign(Object.assign({}, fakeConversation), { left: true })), Calling_1.CallMode.None);
            });
            it("returns CallMode.None if you've blocked the other person", () => {
                chai_1.assert.strictEqual((0, conversations_1.getConversationCallMode)(Object.assign(Object.assign({}, fakeConversation), { isBlocked: true })), Calling_1.CallMode.None);
            });
            it("returns CallMode.None if you haven't accepted message requests", () => {
                chai_1.assert.strictEqual((0, conversations_1.getConversationCallMode)(Object.assign(Object.assign({}, fakeConversation), { acceptedMessageRequest: false })), Calling_1.CallMode.None);
            });
            it('returns CallMode.None if the conversation is Note to Self', () => {
                chai_1.assert.strictEqual((0, conversations_1.getConversationCallMode)(Object.assign(Object.assign({}, fakeConversation), { isMe: true })), Calling_1.CallMode.None);
            });
            it('returns CallMode.None for v1 groups', () => {
                chai_1.assert.strictEqual((0, conversations_1.getConversationCallMode)(Object.assign(Object.assign({}, fakeConversation), { type: 'group', groupVersion: 1, sharedGroupNames: [] })), Calling_1.CallMode.None);
                chai_1.assert.strictEqual((0, conversations_1.getConversationCallMode)(Object.assign(Object.assign({}, fakeConversation), { type: 'group', sharedGroupNames: [] })), Calling_1.CallMode.None);
            });
            it('returns CallMode.Direct if the conversation is a normal direct conversation', () => {
                chai_1.assert.strictEqual((0, conversations_1.getConversationCallMode)(fakeConversation), Calling_1.CallMode.Direct);
            });
            it('returns CallMode.Group if the conversation is a v2 group', () => {
                chai_1.assert.strictEqual((0, conversations_1.getConversationCallMode)(Object.assign(Object.assign({}, fakeConversation), { type: 'group', groupVersion: 2, sharedGroupNames: [] })), Calling_1.CallMode.Group);
            });
        });
        describe('updateConversationLookups', () => {
            it('does not change lookups if no conversations provided', () => {
                const state = (0, conversations_1.getEmptyState)();
                const result = (0, conversations_1.updateConversationLookups)(undefined, undefined, state);
                chai_1.assert.strictEqual(state.conversationsByE164, result.conversationsByE164);
                chai_1.assert.strictEqual(state.conversationsByUuid, result.conversationsByUuid);
                chai_1.assert.strictEqual(state.conversationsByGroupId, result.conversationsByGroupId);
            });
            it('adds and removes e164-only contact', () => {
                const removed = (0, getDefaultConversation_1.getDefaultConversation)({
                    id: 'id-removed',
                    e164: 'e164-removed',
                    uuid: undefined,
                });
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationsByE164: {
                        'e164-removed': removed,
                    } });
                const added = (0, getDefaultConversation_1.getDefaultConversation)({
                    id: 'id-added',
                    e164: 'e164-added',
                    uuid: undefined,
                });
                const expected = {
                    'e164-added': added,
                };
                const actual = (0, conversations_1.updateConversationLookups)(added, removed, state);
                chai_1.assert.deepEqual(actual.conversationsByE164, expected);
                chai_1.assert.strictEqual(state.conversationsByUuid, actual.conversationsByUuid);
                chai_1.assert.strictEqual(state.conversationsByGroupId, actual.conversationsByGroupId);
            });
            it('adds and removes uuid-only contact', () => {
                const removed = (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
                    id: 'id-removed',
                    e164: undefined,
                });
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationsByuuid: {
                        [removed.uuid]: removed,
                    } });
                const added = (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
                    id: 'id-added',
                    e164: undefined,
                });
                const expected = {
                    [added.uuid]: added,
                };
                const actual = (0, conversations_1.updateConversationLookups)(added, removed, state);
                chai_1.assert.strictEqual(state.conversationsByE164, actual.conversationsByE164);
                chai_1.assert.deepEqual(actual.conversationsByUuid, expected);
                chai_1.assert.strictEqual(state.conversationsByGroupId, actual.conversationsByGroupId);
            });
            it('adds and removes groupId-only contact', () => {
                const removed = (0, getDefaultConversation_1.getDefaultConversation)({
                    id: 'id-removed',
                    groupId: 'groupId-removed',
                    e164: undefined,
                    uuid: undefined,
                });
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationsBygroupId: {
                        'groupId-removed': removed,
                    } });
                const added = (0, getDefaultConversation_1.getDefaultConversation)({
                    id: 'id-added',
                    groupId: 'groupId-added',
                    e164: undefined,
                    uuid: undefined,
                });
                const expected = {
                    'groupId-added': added,
                };
                const actual = (0, conversations_1.updateConversationLookups)(added, removed, state);
                chai_1.assert.strictEqual(state.conversationsByE164, actual.conversationsByE164);
                chai_1.assert.strictEqual(state.conversationsByUuid, actual.conversationsByUuid);
                chai_1.assert.deepEqual(actual.conversationsByGroupId, expected);
            });
        });
    });
    describe('reducer', () => {
        const time = Date.now();
        const previousTime = time - 1;
        const conversationId = 'conversation-guid-1';
        const messageId = 'message-guid-1';
        const messageIdTwo = 'message-guid-2';
        const messageIdThree = 'message-guid-3';
        const sourceUuid = UUID_1.UUID.generate().toString();
        function getDefaultMessage(id) {
            return {
                attachments: [],
                conversationId: 'conversationId',
                id,
                received_at: previousTime,
                sent_at: previousTime,
                source: 'source',
                sourceUuid,
                timestamp: previousTime,
                type: 'incoming',
                readStatus: MessageReadStatus_1.ReadStatus.Read,
            };
        }
        function getDefaultConversationMessage() {
            return {
                heightChangeMessageIds: [],
                isLoadingMessages: false,
                messageIds: [],
                metrics: {
                    totalUnread: 0,
                },
                resetCounter: 0,
                scrollToBottomCounter: 0,
                scrollToMessageCounter: 0,
            };
        }
        describe('openConversationInternal', () => {
            it("returns a thunk that triggers a 'showConversation' event when passed a conversation ID", () => {
                const dispatch = sinon.spy();
                openConversationInternal({ conversationId: 'abc123' })(dispatch, getEmptyRootState, null);
                sinon.assert.calledOnce(window.Whisper.events.trigger);
                sinon.assert.calledWith(window.Whisper.events.trigger, 'showConversation', 'abc123', undefined);
            });
            it("returns a thunk that triggers a 'showConversation' event when passed a conversation ID and message ID", () => {
                const dispatch = sinon.spy();
                openConversationInternal({
                    conversationId: 'abc123',
                    messageId: 'xyz987',
                })(dispatch, getEmptyRootState, null);
                sinon.assert.calledOnce(window.Whisper.events.trigger);
                sinon.assert.calledWith(window.Whisper.events.trigger, 'showConversation', 'abc123', 'xyz987');
            });
            it("returns a thunk that doesn't dispatch any actions by default", () => {
                const dispatch = sinon.spy();
                openConversationInternal({ conversationId: 'abc123' })(dispatch, getEmptyRootState, null);
                sinon.assert.notCalled(dispatch);
            });
            it('dispatches a SWITCH_TO_ASSOCIATED_VIEW action if called with a flag', () => {
                const dispatch = sinon.spy();
                openConversationInternal({
                    conversationId: 'abc123',
                    switchToAssociatedView: true,
                })(dispatch, getEmptyRootState, null);
                sinon.assert.calledWith(dispatch, {
                    type: 'SWITCH_TO_ASSOCIATED_VIEW',
                    payload: { conversationId: 'abc123' },
                });
            });
            describe('SWITCH_TO_ASSOCIATED_VIEW', () => {
                let action;
                beforeEach(() => {
                    const dispatch = sinon.spy();
                    openConversationInternal({
                        conversationId: 'fake-conversation-id',
                        switchToAssociatedView: true,
                    })(dispatch, getEmptyRootState, null);
                    [action] = dispatch.getCall(0).args;
                });
                it('shows the inbox if the conversation is not archived', () => {
                    const conversation = (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'fake-conversation-id',
                    });
                    const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                            [conversation.id]: conversation,
                        } });
                    const result = (0, conversations_1.reducer)(state, action);
                    chai_1.assert.isUndefined(result.composer);
                    chai_1.assert.isFalse(result.showArchived);
                });
                it('shows the archive if the conversation is archived', () => {
                    const conversation = (0, getDefaultConversation_1.getDefaultConversation)({
                        id: 'fake-conversation-id',
                        isArchived: true,
                    });
                    const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                            [conversation.id]: conversation,
                        } });
                    const result = (0, conversations_1.reducer)(state, action);
                    chai_1.assert.isUndefined(result.composer);
                    chai_1.assert.isTrue(result.showArchived);
                });
                it('does nothing if the conversation is not found', () => {
                    const state = (0, conversations_1.getEmptyState)();
                    const result = (0, conversations_1.reducer)(state, action);
                    chai_1.assert.strictEqual(result, state);
                });
            });
        });
        describe('CANT_ADD_CONTACT_TO_GROUP', () => {
            it('marks the conversation ID as "cannot add"', () => {
                var _a;
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState });
                const action = cantAddContactToGroup('abc123');
                const result = (0, conversations_1.reducer)(state, action);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.ChooseGroupMembers &&
                    result.composer.cantAddContactIdForModal === 'abc123');
            });
        });
        describe('CLEAR_GROUP_CREATION_ERROR', () => {
            it('clears the group creation error', () => {
                var _a;
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { hasError: true }) });
                const action = clearGroupCreationError();
                const result = (0, conversations_1.reducer)(state, action);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata &&
                    result.composer.hasError === false);
            });
        });
        describe('CLEAR_INVITED_UUIDS_FOR_NEWLY_CREATED_GROUP', () => {
            it('clears the list of invited conversation UUIDs', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { invitedUuidsForNewlyCreatedGroup: [
                        UUID_1.UUID.generate().toString(),
                        UUID_1.UUID.generate().toString(),
                    ] });
                const action = clearInvitedUuidsForNewlyCreatedGroup();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isUndefined(result.invitedUuidsForNewlyCreatedGroup);
            });
        });
        describe('CLOSE_CANT_ADD_CONTACT_TO_GROUP_MODAL', () => {
            it('closes the "cannot add contact" modal"', () => {
                var _a;
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { cantAddContactIdForModal: 'abc123' }) });
                const action = closeCantAddContactToGroupModal();
                const result = (0, conversations_1.reducer)(state, action);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.ChooseGroupMembers &&
                    result.composer.cantAddContactIdForModal === undefined, 'Expected the contact ID to be cleared');
            });
        });
        describe('CLOSE_CONTACT_SPOOFING_REVIEW', () => {
            it('closes the contact spoofing review modal if it was open', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { contactSpoofingReview: {
                        type: contactSpoofing_1.ContactSpoofingType.DirectConversationWithSameTitle,
                        safeConversationId: 'abc123',
                    } });
                const action = closeContactSpoofingReview();
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isUndefined(actual.contactSpoofingReview);
            });
            it("does nothing if the modal wasn't already open", () => {
                const state = (0, conversations_1.getEmptyState)();
                const action = closeContactSpoofingReview();
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(actual, state);
            });
        });
        describe('CLOSE_MAXIMUM_GROUP_SIZE_MODAL', () => {
            it('closes the maximum group size modal if it was open', () => {
                var _a;
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { maximumGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Showing }) });
                const action = closeMaximumGroupSizeModal();
                const result = (0, conversations_1.reducer)(state, action);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.ChooseGroupMembers &&
                    result.composer.maximumGroupSizeModalState ===
                        conversationsEnums_1.OneTimeModalState.Shown, 'Expected the modal to be closed');
            });
            it('does nothing if the maximum group size modal was never shown', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState });
                const action = closeMaximumGroupSizeModal();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.strictEqual(result, state);
            });
            it('does nothing if the maximum group size modal already closed', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { maximumGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown }) });
                const action = closeMaximumGroupSizeModal();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.strictEqual(result, state);
            });
        });
        describe('CLOSE_RECOMMENDED_GROUP_SIZE_MODAL', () => {
            it('closes the recommended group size modal if it was open', () => {
                var _a;
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Showing }) });
                const action = closeRecommendedGroupSizeModal();
                const result = (0, conversations_1.reducer)(state, action);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.ChooseGroupMembers &&
                    result.composer.recommendedGroupSizeModalState ===
                        conversationsEnums_1.OneTimeModalState.Shown, 'Expected the modal to be closed');
            });
            it('does nothing if the recommended group size modal was never shown', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState });
                const action = closeRecommendedGroupSizeModal();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.strictEqual(result, state);
            });
            it('does nothing if the recommended group size modal already closed', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown }) });
                const action = closeRecommendedGroupSizeModal();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.strictEqual(result, state);
            });
        });
        describe('createGroup', () => {
            const conversationsState = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { selectedConversationIds: ['abc123'], groupName: 'Foo Bar Group', groupAvatar: new Uint8Array([1, 2, 3]) }) });
            it('immediately dispatches a CREATE_GROUP_PENDING action, which puts the composer in a loading state', () => {
                var _a;
                const dispatch = sinon.spy();
                createGroup()(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { conversations: conversationsState })), null);
                sinon.assert.calledOnce(dispatch);
                sinon.assert.calledWith(dispatch, { type: 'CREATE_GROUP_PENDING' });
                const action = dispatch.getCall(0).args[0];
                const result = (0, conversations_1.reducer)(conversationsState, action);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata &&
                    result.composer.isCreating &&
                    !result.composer.hasError);
            });
            it('calls groups.createGroupV2', async () => {
                await createGroup()(sinon.spy(), () => (Object.assign(Object.assign({}, getEmptyRootState()), { conversations: conversationsState })), null);
                sinon.assert.calledOnce(createGroupStub);
                sinon.assert.calledWith(createGroupStub, {
                    name: 'Foo Bar Group',
                    avatar: new Uint8Array([1, 2, 3]),
                    avatars: [],
                    expireTimer: 0,
                    conversationIds: ['abc123'],
                });
            });
            it("trims the group's title before calling groups.createGroupV2", async () => {
                await createGroup()(sinon.spy(), () => (Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, conversationsState), { composer: Object.assign(Object.assign({}, conversationsState.composer), { groupName: '  To  Trim \t' }) }) })), null);
                sinon.assert.calledWith(createGroupStub, sinon.match({ name: 'To  Trim' }));
            });
            it('dispatches a CREATE_GROUP_REJECTED action if group creation fails, which marks the state with an error', async () => {
                var _a;
                createGroupStub.rejects(new Error('uh oh'));
                const dispatch = sinon.spy();
                const createGroupPromise = createGroup()(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { conversations: conversationsState })), null);
                const pendingAction = dispatch.getCall(0).args[0];
                const stateAfterPending = (0, conversations_1.reducer)(conversationsState, pendingAction);
                await createGroupPromise;
                sinon.assert.calledTwice(dispatch);
                sinon.assert.calledWith(dispatch, { type: 'CREATE_GROUP_REJECTED' });
                const rejectedAction = dispatch.getCall(1).args[0];
                const result = (0, conversations_1.reducer)(stateAfterPending, rejectedAction);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata &&
                    !result.composer.isCreating &&
                    result.composer.hasError);
            });
            it("when rejecting, does nothing to the left pane if it's no longer in this composer state", async () => {
                createGroupStub.rejects(new Error('uh oh'));
                const dispatch = sinon.spy();
                const createGroupPromise = createGroup()(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { conversations: conversationsState })), null);
                await createGroupPromise;
                const state = (0, conversations_1.getEmptyState)();
                const rejectedAction = dispatch.getCall(1).args[0];
                const result = (0, conversations_1.reducer)(state, rejectedAction);
                chai_1.assert.strictEqual(result, state);
            });
            it('dispatches a CREATE_GROUP_FULFILLED event (which updates the newly-created conversation IDs), triggers a showConversation event and switches to the associated conversation on success', async () => {
                const abc = UUID_1.UUID.fromPrefix('abc').toString();
                createGroupStub.resolves({
                    id: '9876',
                    get: (key) => {
                        if (key !== 'pendingMembersV2') {
                            throw new Error('This getter is not set up for this test');
                        }
                        return [{ uuid: abc }];
                    },
                });
                const dispatch = sinon.spy();
                await createGroup()(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { conversations: conversationsState })), null);
                sinon.assert.calledWith(window.Whisper.events.trigger, 'showConversation', '9876', undefined);
                sinon.assert.calledWith(dispatch, {
                    type: 'CREATE_GROUP_FULFILLED',
                    payload: { invitedUuids: [abc] },
                });
                const fulfilledAction = dispatch.getCall(1).args[0];
                const result = (0, conversations_1.reducer)(conversationsState, fulfilledAction);
                chai_1.assert.deepEqual(result.invitedUuidsForNewlyCreatedGroup, [abc]);
                sinon.assert.calledWith(dispatch, {
                    type: 'SWITCH_TO_ASSOCIATED_VIEW',
                    payload: { conversationId: '9876' },
                });
            });
        });
        describe('MESSAGE_SIZE_CHANGED', () => {
            const stateWithActiveConversation = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { messagesByConversation: {
                    [conversationId]: {
                        heightChangeMessageIds: [],
                        isLoadingMessages: false,
                        isNearBottom: true,
                        messageIds: [messageId],
                        metrics: { totalUnread: 0 },
                        resetCounter: 0,
                        scrollToBottomCounter: 0,
                        scrollToMessageCounter: 0,
                    },
                }, messagesLookup: {
                    [messageId]: getDefaultMessage(messageId),
                } });
            it('does nothing if no conversation is active', () => {
                const state = (0, conversations_1.getEmptyState)();
                chai_1.assert.strictEqual((0, conversations_1.reducer)(state, messageSizeChanged('messageId', 'convoId')), state);
            });
            it('does nothing if a different conversation is active', () => {
                chai_1.assert.deepEqual((0, conversations_1.reducer)(stateWithActiveConversation, messageSizeChanged(messageId, 'another-conversation-guid')), stateWithActiveConversation);
            });
            it('adds the message ID to the list of messages with changed heights', () => {
                var _a;
                const result = (0, conversations_1.reducer)(stateWithActiveConversation, messageSizeChanged(messageId, conversationId));
                chai_1.assert.sameMembers(((_a = result.messagesByConversation[conversationId]) === null || _a === void 0 ? void 0 : _a.heightChangeMessageIds) || [], [messageId]);
            });
            it("doesn't add duplicates to the list of changed-heights messages", () => {
                var _a;
                const state = (0, fp_1.set)(['messagesByConversation', conversationId, 'heightChangeMessageIds'], [messageId], stateWithActiveConversation);
                const result = (0, conversations_1.reducer)(state, messageSizeChanged(messageId, conversationId));
                chai_1.assert.sameMembers(((_a = result.messagesByConversation[conversationId]) === null || _a === void 0 ? void 0 : _a.heightChangeMessageIds) || [], [messageId]);
            });
        });
        describe('MESSAGE_STOPPED_BY_MISSING_VERIFICATION', () => {
            it('adds messages that need conversation verification, removing duplicates', () => {
                const first = (0, conversations_1.reducer)((0, conversations_1.getEmptyState)(), messageStoppedByMissingVerification('message 1', ['convo 1']));
                const second = (0, conversations_1.reducer)(first, messageStoppedByMissingVerification('message 1', ['convo 2']));
                const third = (0, conversations_1.reducer)(second, messageStoppedByMissingVerification('message 2', [
                    'convo 1',
                    'convo 3',
                ]));
                chai_1.assert.deepStrictEqual(third.outboundMessagesPendingConversationVerification, {
                    'convo 1': ['message 1', 'message 2'],
                    'convo 2': ['message 1'],
                    'convo 3': ['message 2'],
                });
            });
        });
        describe('REPAIR_NEWEST_MESSAGE', () => {
            it('updates newest', () => {
                const action = repairNewestMessage(conversationId);
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { messagesLookup: {
                        [messageId]: Object.assign(Object.assign({}, getDefaultMessage(messageId)), { received_at: time, sent_at: time }),
                    }, messagesByConversation: {
                        [conversationId]: Object.assign(Object.assign({}, getDefaultConversationMessage()), { messageIds: [messageIdThree, messageIdTwo, messageId], metrics: {
                                totalUnread: 0,
                            } }),
                    } });
                const expected = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { messagesLookup: {
                        [messageId]: Object.assign(Object.assign({}, getDefaultMessage(messageId)), { received_at: time, sent_at: time }),
                    }, messagesByConversation: {
                        [conversationId]: Object.assign(Object.assign({}, getDefaultConversationMessage()), { messageIds: [messageIdThree, messageIdTwo, messageId], metrics: {
                                totalUnread: 0,
                                newest: {
                                    id: messageId,
                                    received_at: time,
                                    sent_at: time,
                                },
                            } }),
                    } });
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(actual, expected);
            });
            it('clears newest', () => {
                const action = repairNewestMessage(conversationId);
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { messagesLookup: {
                        [messageId]: Object.assign(Object.assign({}, getDefaultMessage(messageId)), { received_at: time }),
                    }, messagesByConversation: {
                        [conversationId]: Object.assign(Object.assign({}, getDefaultConversationMessage()), { messageIds: [], metrics: {
                                totalUnread: 0,
                                newest: {
                                    id: messageId,
                                    received_at: time,
                                },
                            } }),
                    } });
                const expected = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { messagesLookup: {
                        [messageId]: Object.assign(Object.assign({}, getDefaultMessage(messageId)), { received_at: time }),
                    }, messagesByConversation: {
                        [conversationId]: Object.assign(Object.assign({}, getDefaultConversationMessage()), { messageIds: [], metrics: {
                                newest: undefined,
                                totalUnread: 0,
                            } }),
                    } });
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(actual, expected);
            });
            it('returns state if conversation not present', () => {
                const action = repairNewestMessage(conversationId);
                const state = (0, conversations_1.getEmptyState)();
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.equal(actual, state);
            });
        });
        describe('REPAIR_OLDEST_MESSAGE', () => {
            it('updates oldest', () => {
                const action = repairOldestMessage(conversationId);
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { messagesLookup: {
                        [messageId]: Object.assign(Object.assign({}, getDefaultMessage(messageId)), { received_at: time, sent_at: time }),
                    }, messagesByConversation: {
                        [conversationId]: Object.assign(Object.assign({}, getDefaultConversationMessage()), { messageIds: [messageId, messageIdTwo, messageIdThree], metrics: {
                                totalUnread: 0,
                            } }),
                    } });
                const expected = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { messagesLookup: {
                        [messageId]: Object.assign(Object.assign({}, getDefaultMessage(messageId)), { received_at: time, sent_at: time }),
                    }, messagesByConversation: {
                        [conversationId]: Object.assign(Object.assign({}, getDefaultConversationMessage()), { messageIds: [messageId, messageIdTwo, messageIdThree], metrics: {
                                totalUnread: 0,
                                oldest: {
                                    id: messageId,
                                    received_at: time,
                                    sent_at: time,
                                },
                            } }),
                    } });
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(actual, expected);
            });
            it('clears oldest', () => {
                const action = repairOldestMessage(conversationId);
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { messagesLookup: {
                        [messageId]: Object.assign(Object.assign({}, getDefaultMessage(messageId)), { received_at: time }),
                    }, messagesByConversation: {
                        [conversationId]: Object.assign(Object.assign({}, getDefaultConversationMessage()), { messageIds: [], metrics: {
                                totalUnread: 0,
                                oldest: {
                                    id: messageId,
                                    received_at: time,
                                },
                            } }),
                    } });
                const expected = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { messagesLookup: {
                        [messageId]: Object.assign(Object.assign({}, getDefaultMessage(messageId)), { received_at: time }),
                    }, messagesByConversation: {
                        [conversationId]: Object.assign(Object.assign({}, getDefaultConversationMessage()), { messageIds: [], metrics: {
                                oldest: undefined,
                                totalUnread: 0,
                            } }),
                    } });
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(actual, expected);
            });
            it('returns state if conversation not present', () => {
                const action = repairOldestMessage(conversationId);
                const state = (0, conversations_1.getEmptyState)();
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.equal(actual, state);
            });
        });
        describe('REVIEW_GROUP_MEMBER_NAME_COLLISION', () => {
            it('starts reviewing a group member name collision', () => {
                const state = (0, conversations_1.getEmptyState)();
                const action = reviewGroupMemberNameCollision('abc123');
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(actual.contactSpoofingReview, {
                    type: contactSpoofing_1.ContactSpoofingType.MultipleGroupMembersWithSameTitle,
                    groupConversationId: 'abc123',
                });
            });
        });
        describe('REVIEW_MESSAGE_REQUEST_NAME_COLLISION', () => {
            it('starts reviewing a message request name collision', () => {
                const state = (0, conversations_1.getEmptyState)();
                const action = reviewMessageRequestNameCollision({
                    safeConversationId: 'def',
                });
                const actual = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(actual.contactSpoofingReview, {
                    type: contactSpoofing_1.ContactSpoofingType.DirectConversationWithSameTitle,
                    safeConversationId: 'def',
                });
            });
        });
        describe('SET_COMPOSE_GROUP_AVATAR', () => {
            it("can clear the composer's group avatar", () => {
                var _a;
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { groupAvatar: new Uint8Array(2) }) });
                const action = setComposeGroupAvatar(undefined);
                const result = (0, conversations_1.reducer)(state, action);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata &&
                    result.composer.groupAvatar === undefined);
            });
            it("can set the composer's group avatar", () => {
                var _a;
                const avatar = new Uint8Array([1, 2, 3]);
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultSetGroupMetadataComposerState });
                const action = setComposeGroupAvatar(avatar);
                const result = (0, conversations_1.reducer)(state, action);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata &&
                    result.composer.groupAvatar === avatar);
            });
        });
        describe('SET_COMPOSE_GROUP_NAME', () => {
            it("can set the composer's group name", () => {
                var _a;
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultSetGroupMetadataComposerState });
                const action = setComposeGroupName('bing bong');
                const result = (0, conversations_1.reducer)(state, action);
                (0, chai_1.assert)(((_a = result.composer) === null || _a === void 0 ? void 0 : _a.step) === conversationsEnums_1.ComposerStep.SetGroupMetadata &&
                    result.composer.groupName === 'bing bong');
            });
        });
        describe('SET_COMPOSE_SEARCH_TERM', () => {
            it('updates the contact search term', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultStartDirectConversationComposerState });
                const result = (0, conversations_1.reducer)(state, setComposeSearchTerm('foo bar'));
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultStartDirectConversationComposerState), { searchTerm: 'foo bar' }));
            });
        });
        describe('SET_PRE_JOIN_CONVERSATION', () => {
            const startState = Object.assign({}, (0, conversations_1.getEmptyState)());
            it('starts with empty value', () => {
                chai_1.assert.isUndefined(startState.preJoinConversation);
            });
            it('sets value as provided', () => {
                const preJoinConversation = {
                    title: 'Pre-join group!',
                    memberCount: 4,
                    approvalRequired: false,
                };
                const stateWithData = (0, conversations_1.reducer)(startState, setPreJoinConversation(preJoinConversation));
                chai_1.assert.deepEqual(stateWithData.preJoinConversation, preJoinConversation);
                const resetState = (0, conversations_1.reducer)(stateWithData, setPreJoinConversation(undefined));
                chai_1.assert.isUndefined(resetState.preJoinConversation);
            });
        });
        describe('SHOW_ARCHIVED_CONVERSATIONS', () => {
            it('is a no-op when already at the archive', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { showArchived: true });
                const action = showArchivedConversations();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isTrue(result.showArchived);
                chai_1.assert.isUndefined(result.composer);
            });
            it('switches from the inbox to the archive', () => {
                const state = (0, conversations_1.getEmptyState)();
                const action = showArchivedConversations();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isTrue(result.showArchived);
                chai_1.assert.isUndefined(result.composer);
            });
            it('switches from the composer to the archive', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultStartDirectConversationComposerState });
                const action = showArchivedConversations();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isTrue(result.showArchived);
                chai_1.assert.isUndefined(result.composer);
            });
        });
        describe('SHOW_INBOX', () => {
            it('is a no-op when already at the inbox', () => {
                const state = (0, conversations_1.getEmptyState)();
                const action = showInbox();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.isUndefined(result.composer);
            });
            it('switches from the archive to the inbox', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { showArchived: true });
                const action = showInbox();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.isUndefined(result.composer);
            });
            it('switches from the composer to the inbox', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultStartDirectConversationComposerState });
                const action = showInbox();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.isUndefined(result.composer);
            });
        });
        describe('START_COMPOSING', () => {
            it('does nothing if on the first step of the composer', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultStartDirectConversationComposerState });
                const action = startComposing();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.deepEqual(result.composer, defaultComposerStates_1.defaultStartDirectConversationComposerState);
            });
            it('if on the second step of the composer, goes back to the first step, clearing the search term', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { searchTerm: 'to be cleared' }) });
                const action = startComposing();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.deepEqual(result.composer, defaultComposerStates_1.defaultStartDirectConversationComposerState);
            });
            it('if on the third step of the composer, goes back to the first step, clearing everything', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultSetGroupMetadataComposerState });
                const action = startComposing();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.deepEqual(result.composer, defaultComposerStates_1.defaultStartDirectConversationComposerState);
            });
            it('switches from the inbox to the composer', () => {
                const state = (0, conversations_1.getEmptyState)();
                const action = startComposing();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.deepEqual(result.composer, defaultComposerStates_1.defaultStartDirectConversationComposerState);
            });
            it('switches from the archive to the inbox', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { showArchived: true });
                const action = startComposing();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.deepEqual(result.composer, defaultComposerStates_1.defaultStartDirectConversationComposerState);
            });
        });
        describe('SHOW_CHOOSE_GROUP_MEMBERS', () => {
            it('switches to the second step of the composer if on the first step', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultStartDirectConversationComposerState), { searchTerm: 'to be cleared' }) });
                const action = showChooseGroupMembers();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { userAvatarData: (0, Avatar_1.getDefaultAvatars)(true) }));
            });
            it('does nothing if already on the second step of the composer', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState });
                const action = showChooseGroupMembers();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.strictEqual(result, state);
            });
            it('returns to the second step if on the third step of the composer', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { groupName: 'Foo Bar Group', groupAvatar: new Uint8Array([4, 2]) }) });
                const action = showChooseGroupMembers();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { groupName: 'Foo Bar Group', groupAvatar: new Uint8Array([4, 2]) }));
            });
            it('switches from the inbox to the second step of the composer', () => {
                const state = (0, conversations_1.getEmptyState)();
                const action = showChooseGroupMembers();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { userAvatarData: (0, Avatar_1.getDefaultAvatars)(true) }));
            });
            it('switches from the archive to the second step of the composer', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { showArchived: true });
                const action = showChooseGroupMembers();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.isFalse(result.showArchived);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { userAvatarData: (0, Avatar_1.getDefaultAvatars)(true) }));
            });
        });
        describe('START_SETTING_GROUP_METADATA', () => {
            it('moves from the second to the third step of the composer', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: ['abc', 'def'] }) });
                const action = startSettingGroupMetadata();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { selectedConversationIds: ['abc', 'def'] }));
            });
            it('maintains state when going from the second to third steps of the composer, if the second step already had some data (likely from a previous visit)', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { searchTerm: 'foo bar', selectedConversationIds: ['abc', 'def'], groupName: 'Foo Bar Group', groupAvatar: new Uint8Array([6, 9]) }) });
                const action = startSettingGroupMetadata();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultSetGroupMetadataComposerState), { selectedConversationIds: ['abc', 'def'], groupName: 'Foo Bar Group', groupAvatar: new Uint8Array([6, 9]) }));
            });
            it('does nothing if already on the third step of the composer', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultSetGroupMetadataComposerState });
                const action = startSettingGroupMetadata();
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.strictEqual(result, state);
            });
        });
        describe('TOGGLE_CONVERSATION_IN_CHOOSE_MEMBERS', () => {
            function getAction(id, conversationsState) {
                const dispatch = sinon.spy();
                toggleConversationInChooseMembers(id)(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { conversations: conversationsState })), null);
                return dispatch.getCall(0).args[0];
            }
            let remoteConfigGetValueStub;
            beforeEach(() => {
                remoteConfigGetValueStub = sinonSandbox
                    .stub(window.Signal.RemoteConfig, 'getValue')
                    .withArgs('global.groupsv2.maxGroupSize')
                    .returns('22')
                    .withArgs('global.groupsv2.groupSizeHardLimit')
                    .returns('33');
            });
            it('adds conversation IDs to the list', () => {
                const zero = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState });
                const one = (0, conversations_1.reducer)(zero, getAction('abc', zero));
                const two = (0, conversations_1.reducer)(one, getAction('def', one));
                chai_1.assert.deepEqual(two.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: ['abc', 'def'] }));
            });
            it('removes conversation IDs from the list', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: ['abc', 'def'] }) });
                const action = getAction('abc', state);
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: ['def'] }));
            });
            it('shows the recommended group size modal when first crossing the maximum recommended group size', () => {
                const oldSelectedConversationIds = (0, lodash_1.times)(21, () => (0, uuid_1.v4)());
                const newUuid = (0, uuid_1.v4)();
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: oldSelectedConversationIds }) });
                const action = getAction(newUuid, state);
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: [...oldSelectedConversationIds, newUuid], recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Showing }));
            });
            it("doesn't show the recommended group size modal twice", () => {
                const oldSelectedConversationIds = (0, lodash_1.times)(21, () => (0, uuid_1.v4)());
                const newUuid = (0, uuid_1.v4)();
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: oldSelectedConversationIds, recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown }) });
                const action = getAction(newUuid, state);
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: [...oldSelectedConversationIds, newUuid], recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown }));
            });
            it('defaults the maximum recommended size to 151', () => {
                [undefined, 'xyz'].forEach(value => {
                    remoteConfigGetValueStub
                        .withArgs('global.groupsv2.maxGroupSize')
                        .returns(value);
                    const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState });
                    const action = getAction((0, uuid_1.v4)(), state);
                    chai_1.assert.strictEqual(action.payload.maxRecommendedGroupSize, 151);
                });
            });
            it('shows the maximum group size modal when first reaching the maximum group size', () => {
                const oldSelectedConversationIds = (0, lodash_1.times)(31, () => (0, uuid_1.v4)());
                const newUuid = (0, uuid_1.v4)();
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: oldSelectedConversationIds, recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown, maximumGroupSizeModalState: conversationsEnums_1.OneTimeModalState.NeverShown }) });
                const action = getAction(newUuid, state);
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: [...oldSelectedConversationIds, newUuid], recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown, maximumGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Showing }));
            });
            it("doesn't show the maximum group size modal twice", () => {
                const oldSelectedConversationIds = (0, lodash_1.times)(31, () => (0, uuid_1.v4)());
                const newUuid = (0, uuid_1.v4)();
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: oldSelectedConversationIds, recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown, maximumGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown }) });
                const action = getAction(newUuid, state);
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(result.composer, Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: [...oldSelectedConversationIds, newUuid], recommendedGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown, maximumGroupSizeModalState: conversationsEnums_1.OneTimeModalState.Shown }));
            });
            it('cannot select more than the maximum number of conversations', () => {
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: Object.assign(Object.assign({}, defaultComposerStates_1.defaultChooseGroupMembersComposerState), { selectedConversationIds: (0, lodash_1.times)(1000, () => (0, uuid_1.v4)()) }) });
                const action = getAction((0, uuid_1.v4)(), state);
                const result = (0, conversations_1.reducer)(state, action);
                chai_1.assert.deepEqual(result, state);
            });
            it('defaults the maximum group size to 1001 if the recommended maximum is smaller', () => {
                [undefined, 'xyz'].forEach(value => {
                    remoteConfigGetValueStub
                        .withArgs('global.groupsv2.maxGroupSize')
                        .returns('2')
                        .withArgs('global.groupsv2.groupSizeHardLimit')
                        .returns(value);
                    const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState });
                    const action = getAction((0, uuid_1.v4)(), state);
                    chai_1.assert.strictEqual(action.payload.maxGroupSize, 1001);
                });
            });
            it('defaults the maximum group size to (recommended maximum + 1) if the recommended maximum is more than 1001', () => {
                remoteConfigGetValueStub
                    .withArgs('global.groupsv2.maxGroupSize')
                    .returns('1234')
                    .withArgs('global.groupsv2.groupSizeHardLimit')
                    .returns('2');
                const state = Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { composer: defaultComposerStates_1.defaultChooseGroupMembersComposerState });
                const action = getAction((0, uuid_1.v4)(), state);
                chai_1.assert.strictEqual(action.payload.maxGroupSize, 1235);
            });
        });
    });
    describe('COLORS_CHANGED', () => {
        const abc = (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
            id: 'abc',
            conversationColor: 'wintergreen',
        });
        const def = (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
            id: 'def',
            conversationColor: 'infrared',
        });
        const ghi = (0, getDefaultConversation_1.getDefaultConversation)({
            id: 'ghi',
            e164: 'ghi',
            conversationColor: 'ember',
        });
        const jkl = (0, getDefaultConversation_1.getDefaultConversation)({
            id: 'jkl',
            groupId: 'jkl',
            conversationColor: 'plum',
        });
        const getState = () => (Object.assign(Object.assign({}, getEmptyRootState()), { conversations: Object.assign(Object.assign({}, (0, conversations_1.getEmptyState)()), { conversationLookup: {
                    abc,
                    def,
                    ghi,
                    jkl,
                }, conversationsByUuid: {
                    abc,
                    def,
                }, conversationsByE164: {
                    ghi,
                }, conversationsByGroupId: {
                    jkl,
                } }) }));
        it('resetAllChatColors', async () => {
            const dispatch = sinon.spy();
            await resetAllChatColors()(dispatch, getState, null);
            const [action] = dispatch.getCall(0).args;
            const nextState = (0, conversations_1.reducer)(getState().conversations, action);
            sinon.assert.calledOnce(dispatch);
            chai_1.assert.isUndefined(nextState.conversationLookup.abc.conversationColor);
            chai_1.assert.isUndefined(nextState.conversationLookup.def.conversationColor);
            chai_1.assert.isUndefined(nextState.conversationLookup.ghi.conversationColor);
            chai_1.assert.isUndefined(nextState.conversationLookup.jkl.conversationColor);
            chai_1.assert.isUndefined(nextState.conversationsByUuid[abc.uuid].conversationColor);
            chai_1.assert.isUndefined(nextState.conversationsByUuid[def.uuid].conversationColor);
            chai_1.assert.isUndefined(nextState.conversationsByE164.ghi.conversationColor);
            chai_1.assert.isUndefined(nextState.conversationsByGroupId.jkl.conversationColor);
            window.storage.remove('defaultConversationColor');
        });
    });
});
