"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const reducer_1 = require("../../../state/reducer");
const noop_1 = require("../../../state/ducks/noop");
const calling_1 = require("../../../state/ducks/calling");
const calling_2 = require("../../../services/calling");
const Calling_1 = require("../../../types/Calling");
const UUID_1 = require("../../../types/UUID");
describe('calling duck', () => {
    const stateWithDirectCall = Object.assign(Object.assign({}, (0, calling_1.getEmptyState)()), { callsByConversation: {
            'fake-direct-call-conversation-id': {
                callMode: Calling_1.CallMode.Direct,
                conversationId: 'fake-direct-call-conversation-id',
                callState: Calling_1.CallState.Accepted,
                isIncoming: false,
                isVideoCall: false,
                hasRemoteVideo: false,
            },
        } });
    const stateWithActiveDirectCall = Object.assign(Object.assign({}, stateWithDirectCall), { activeCallState: {
            conversationId: 'fake-direct-call-conversation-id',
            hasLocalAudio: true,
            hasLocalVideo: false,
            isInSpeakerView: false,
            showParticipantsList: false,
            safetyNumberChangedUuids: [],
            outgoingRing: true,
            pip: false,
            settingsDialogOpen: false,
        } });
    const stateWithIncomingDirectCall = Object.assign(Object.assign({}, (0, calling_1.getEmptyState)()), { callsByConversation: {
            'fake-direct-call-conversation-id': {
                callMode: Calling_1.CallMode.Direct,
                conversationId: 'fake-direct-call-conversation-id',
                callState: Calling_1.CallState.Ringing,
                isIncoming: true,
                isVideoCall: false,
                hasRemoteVideo: false,
            },
        } });
    const creatorUuid = UUID_1.UUID.generate().toString();
    const differentCreatorUuid = UUID_1.UUID.generate().toString();
    const remoteUuid = UUID_1.UUID.generate().toString();
    const ringerUuid = UUID_1.UUID.generate().toString();
    const stateWithGroupCall = Object.assign(Object.assign({}, (0, calling_1.getEmptyState)()), { callsByConversation: {
            'fake-group-call-conversation-id': {
                callMode: Calling_1.CallMode.Group,
                conversationId: 'fake-group-call-conversation-id',
                connectionState: Calling_1.GroupCallConnectionState.Connected,
                joinState: Calling_1.GroupCallJoinState.NotJoined,
                peekInfo: {
                    uuids: [creatorUuid],
                    creatorUuid,
                    eraId: 'xyz',
                    maxDevices: 16,
                    deviceCount: 1,
                },
                remoteParticipants: [
                    {
                        uuid: remoteUuid,
                        demuxId: 123,
                        hasRemoteAudio: true,
                        hasRemoteVideo: true,
                        presenting: false,
                        sharingScreen: false,
                        videoAspectRatio: 4 / 3,
                    },
                ],
            },
        } });
    const stateWithIncomingGroupCall = Object.assign(Object.assign({}, stateWithGroupCall), { callsByConversation: Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation), { 'fake-group-call-conversation-id': Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation['fake-group-call-conversation-id']), { ringId: BigInt(123), ringerUuid: UUID_1.UUID.generate().toString() }) }) });
    const stateWithActiveGroupCall = Object.assign(Object.assign({}, stateWithGroupCall), { activeCallState: {
            conversationId: 'fake-group-call-conversation-id',
            hasLocalAudio: true,
            hasLocalVideo: false,
            isInSpeakerView: false,
            showParticipantsList: false,
            safetyNumberChangedUuids: [],
            outgoingRing: false,
            pip: false,
            settingsDialogOpen: false,
        } });
    const ourUuid = UUID_1.UUID.generate().toString();
    const getEmptyRootState = () => {
        const rootState = (0, reducer_1.reducer)(undefined, (0, noop_1.noopAction)());
        return Object.assign(Object.assign({}, rootState), { user: Object.assign(Object.assign({}, rootState.user), { ourUuid }) });
    };
    beforeEach(function beforeEach() {
        this.sandbox = sinon.createSandbox();
    });
    afterEach(function afterEach() {
        this.sandbox.restore();
    });
    describe('actions', () => {
        describe('getPresentingSources', () => {
            beforeEach(function beforeEach() {
                this.callingServiceGetPresentingSources = this.sandbox
                    .stub(calling_2.calling, 'getPresentingSources')
                    .resolves([
                    {
                        id: 'foo.bar',
                        name: 'Foo Bar',
                        thumbnail: 'xyz',
                    },
                ]);
            });
            it('retrieves sources from the calling service', async function test() {
                const { getPresentingSources } = calling_1.actions;
                const dispatch = sinon.spy();
                await getPresentingSources()(dispatch, getEmptyRootState, null);
                sinon.assert.calledOnce(this.callingServiceGetPresentingSources);
            });
            it('dispatches SET_PRESENTING_SOURCES', async function test() {
                const { getPresentingSources } = calling_1.actions;
                const dispatch = sinon.spy();
                await getPresentingSources()(dispatch, getEmptyRootState, null);
                sinon.assert.calledOnce(dispatch);
                sinon.assert.calledWith(dispatch, {
                    type: 'calling/SET_PRESENTING_SOURCES',
                    payload: [
                        {
                            id: 'foo.bar',
                            name: 'Foo Bar',
                            thumbnail: 'xyz',
                        },
                    ],
                });
            });
        });
        describe('remoteSharingScreenChange', () => {
            it("updates whether someone's screen is being shared", () => {
                const { remoteSharingScreenChange } = calling_1.actions;
                const payload = {
                    conversationId: 'fake-direct-call-conversation-id',
                    isSharingScreen: true,
                };
                const state = Object.assign({}, stateWithActiveDirectCall);
                const nextState = (0, calling_1.reducer)(state, remoteSharingScreenChange(payload));
                const expectedState = Object.assign(Object.assign({}, stateWithActiveDirectCall), { callsByConversation: {
                        'fake-direct-call-conversation-id': Object.assign(Object.assign({}, stateWithActiveDirectCall.callsByConversation['fake-direct-call-conversation-id']), { isSharingScreen: true }),
                    } });
                chai_1.assert.deepEqual(nextState, expectedState);
            });
        });
        describe('setPresenting', () => {
            beforeEach(function beforeEach() {
                this.callingServiceSetPresenting = this.sandbox.stub(calling_2.calling, 'setPresenting');
            });
            it('calls setPresenting on the calling service', function test() {
                const { setPresenting } = calling_1.actions;
                const dispatch = sinon.spy();
                const presentedSource = {
                    id: 'window:786',
                    name: 'Application',
                };
                const getState = () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: Object.assign({}, stateWithActiveGroupCall) }));
                setPresenting(presentedSource)(dispatch, getState, null);
                sinon.assert.calledOnce(this.callingServiceSetPresenting);
                sinon.assert.calledWith(this.callingServiceSetPresenting, 'fake-group-call-conversation-id', false, presentedSource);
            });
            it('dispatches SET_PRESENTING', () => {
                const { setPresenting } = calling_1.actions;
                const dispatch = sinon.spy();
                const presentedSource = {
                    id: 'window:786',
                    name: 'Application',
                };
                const getState = () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: Object.assign({}, stateWithActiveGroupCall) }));
                setPresenting(presentedSource)(dispatch, getState, null);
                sinon.assert.calledOnce(dispatch);
                sinon.assert.calledWith(dispatch, {
                    type: 'calling/SET_PRESENTING',
                    payload: presentedSource,
                });
            });
            it('turns off presenting when no value is passed in', () => {
                var _a, _b;
                const dispatch = sinon.spy();
                const { setPresenting } = calling_1.actions;
                const presentedSource = {
                    id: 'window:786',
                    name: 'Application',
                };
                const getState = () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: Object.assign({}, stateWithActiveGroupCall) }));
                setPresenting(presentedSource)(dispatch, getState, null);
                const action = dispatch.getCall(0).args[0];
                const nextState = (0, calling_1.reducer)(getState().calling, action);
                chai_1.assert.isDefined(nextState.activeCallState);
                chai_1.assert.equal((_a = nextState.activeCallState) === null || _a === void 0 ? void 0 : _a.presentingSource, presentedSource);
                chai_1.assert.isUndefined((_b = nextState.activeCallState) === null || _b === void 0 ? void 0 : _b.presentingSourcesAvailable);
            });
            it('sets the presenting value when one is passed in', () => {
                var _a, _b;
                const dispatch = sinon.spy();
                const { setPresenting } = calling_1.actions;
                const getState = () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: Object.assign({}, stateWithActiveGroupCall) }));
                setPresenting()(dispatch, getState, null);
                const action = dispatch.getCall(0).args[0];
                const nextState = (0, calling_1.reducer)(getState().calling, action);
                chai_1.assert.isDefined(nextState.activeCallState);
                chai_1.assert.isUndefined((_a = nextState.activeCallState) === null || _a === void 0 ? void 0 : _a.presentingSource);
                chai_1.assert.isUndefined((_b = nextState.activeCallState) === null || _b === void 0 ? void 0 : _b.presentingSourcesAvailable);
            });
        });
        describe('acceptCall', () => {
            const { acceptCall } = calling_1.actions;
            beforeEach(function beforeEach() {
                this.callingServiceAccept = this.sandbox
                    .stub(calling_2.calling, 'acceptDirectCall')
                    .resolves();
                this.callingServiceJoin = this.sandbox
                    .stub(calling_2.calling, 'joinGroupCall')
                    .resolves();
            });
            describe('accepting a direct call', () => {
                const getState = () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: stateWithIncomingDirectCall }));
                it('dispatches an ACCEPT_CALL_PENDING action', async () => {
                    const dispatch = sinon.spy();
                    await acceptCall({
                        conversationId: 'fake-direct-call-conversation-id',
                        asVideoCall: true,
                    })(dispatch, getState, null);
                    sinon.assert.calledOnce(dispatch);
                    sinon.assert.calledWith(dispatch, {
                        type: 'calling/ACCEPT_CALL_PENDING',
                        payload: {
                            conversationId: 'fake-direct-call-conversation-id',
                            asVideoCall: true,
                        },
                    });
                    await acceptCall({
                        conversationId: 'fake-direct-call-conversation-id',
                        asVideoCall: false,
                    })(dispatch, getState, null);
                    sinon.assert.calledTwice(dispatch);
                    sinon.assert.calledWith(dispatch, {
                        type: 'calling/ACCEPT_CALL_PENDING',
                        payload: {
                            conversationId: 'fake-direct-call-conversation-id',
                            asVideoCall: false,
                        },
                    });
                });
                it('asks the calling service to accept the call', async function test() {
                    const dispatch = sinon.spy();
                    await acceptCall({
                        conversationId: 'fake-direct-call-conversation-id',
                        asVideoCall: true,
                    })(dispatch, getState, null);
                    sinon.assert.calledOnce(this.callingServiceAccept);
                    sinon.assert.calledWith(this.callingServiceAccept, 'fake-direct-call-conversation-id', true);
                    await acceptCall({
                        conversationId: 'fake-direct-call-conversation-id',
                        asVideoCall: false,
                    })(dispatch, getState, null);
                    sinon.assert.calledTwice(this.callingServiceAccept);
                    sinon.assert.calledWith(this.callingServiceAccept, 'fake-direct-call-conversation-id', false);
                });
                it('updates the active call state with ACCEPT_CALL_PENDING', async () => {
                    const dispatch = sinon.spy();
                    await acceptCall({
                        conversationId: 'fake-direct-call-conversation-id',
                        asVideoCall: true,
                    })(dispatch, getState, null);
                    const action = dispatch.getCall(0).args[0];
                    const result = (0, calling_1.reducer)(stateWithIncomingDirectCall, action);
                    chai_1.assert.deepEqual(result.activeCallState, {
                        conversationId: 'fake-direct-call-conversation-id',
                        hasLocalAudio: true,
                        hasLocalVideo: true,
                        isInSpeakerView: false,
                        showParticipantsList: false,
                        safetyNumberChangedUuids: [],
                        outgoingRing: false,
                        pip: false,
                        settingsDialogOpen: false,
                    });
                });
            });
            describe('accepting a group call', () => {
                const getState = () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: stateWithIncomingGroupCall }));
                it('dispatches an ACCEPT_CALL_PENDING action', async () => {
                    const dispatch = sinon.spy();
                    await acceptCall({
                        conversationId: 'fake-group-call-conversation-id',
                        asVideoCall: true,
                    })(dispatch, getState, null);
                    sinon.assert.calledOnce(dispatch);
                    sinon.assert.calledWith(dispatch, {
                        type: 'calling/ACCEPT_CALL_PENDING',
                        payload: {
                            conversationId: 'fake-group-call-conversation-id',
                            asVideoCall: true,
                        },
                    });
                    await acceptCall({
                        conversationId: 'fake-group-call-conversation-id',
                        asVideoCall: false,
                    })(dispatch, getState, null);
                    sinon.assert.calledTwice(dispatch);
                    sinon.assert.calledWith(dispatch, {
                        type: 'calling/ACCEPT_CALL_PENDING',
                        payload: {
                            conversationId: 'fake-group-call-conversation-id',
                            asVideoCall: false,
                        },
                    });
                });
                it('asks the calling service to join the call', async function test() {
                    const dispatch = sinon.spy();
                    await acceptCall({
                        conversationId: 'fake-group-call-conversation-id',
                        asVideoCall: true,
                    })(dispatch, getState, null);
                    sinon.assert.calledOnce(this.callingServiceJoin);
                    sinon.assert.calledWith(this.callingServiceJoin, 'fake-group-call-conversation-id', true, true);
                    await acceptCall({
                        conversationId: 'fake-group-call-conversation-id',
                        asVideoCall: false,
                    })(dispatch, getState, null);
                    sinon.assert.calledTwice(this.callingServiceJoin);
                    sinon.assert.calledWith(this.callingServiceJoin, 'fake-group-call-conversation-id', true, false);
                });
                it('updates the active call state with ACCEPT_CALL_PENDING', async () => {
                    const dispatch = sinon.spy();
                    await acceptCall({
                        conversationId: 'fake-group-call-conversation-id',
                        asVideoCall: true,
                    })(dispatch, getState, null);
                    const action = dispatch.getCall(0).args[0];
                    const result = (0, calling_1.reducer)(stateWithIncomingGroupCall, action);
                    chai_1.assert.deepEqual(result.activeCallState, {
                        conversationId: 'fake-group-call-conversation-id',
                        hasLocalAudio: true,
                        hasLocalVideo: true,
                        isInSpeakerView: false,
                        showParticipantsList: false,
                        safetyNumberChangedUuids: [],
                        outgoingRing: false,
                        pip: false,
                        settingsDialogOpen: false,
                    });
                });
            });
        });
        describe('cancelCall', () => {
            const { cancelCall } = calling_1.actions;
            beforeEach(function beforeEach() {
                this.callingServiceStopCallingLobby = this.sandbox.stub(calling_2.calling, 'stopCallingLobby');
            });
            it('stops the calling lobby for that conversation', function test() {
                cancelCall({ conversationId: '123' });
                sinon.assert.calledOnce(this.callingServiceStopCallingLobby);
                sinon.assert.calledWith(this.callingServiceStopCallingLobby, '123');
            });
            it('completely removes an active direct call from the state', () => {
                const result = (0, calling_1.reducer)(stateWithActiveDirectCall, cancelCall({ conversationId: 'fake-direct-call-conversation-id' }));
                chai_1.assert.notProperty(result.callsByConversation, 'fake-direct-call-conversation-id');
                chai_1.assert.isUndefined(result.activeCallState);
            });
            it('removes the active group call, but leaves it in the state', () => {
                const result = (0, calling_1.reducer)(stateWithActiveGroupCall, cancelCall({ conversationId: 'fake-group-call-conversation-id' }));
                chai_1.assert.property(result.callsByConversation, 'fake-group-call-conversation-id');
                chai_1.assert.isUndefined(result.activeCallState);
            });
        });
        describe('cancelIncomingGroupCallRing', () => {
            const { cancelIncomingGroupCallRing } = calling_1.actions;
            it('does nothing if there is no associated group call', () => {
                const state = (0, calling_1.getEmptyState)();
                const action = cancelIncomingGroupCallRing({
                    conversationId: 'garbage',
                    ringId: BigInt(1),
                });
                const result = (0, calling_1.reducer)(state, action);
                chai_1.assert.strictEqual(result, state);
            });
            it("does nothing if the ring to cancel isn't the same one", () => {
                const action = cancelIncomingGroupCallRing({
                    conversationId: 'fake-group-call-conversation-id',
                    ringId: BigInt(999),
                });
                const result = (0, calling_1.reducer)(stateWithIncomingGroupCall, action);
                chai_1.assert.strictEqual(result, stateWithIncomingGroupCall);
            });
            it("removes the call from the state if it's not connected", () => {
                const state = Object.assign(Object.assign({}, stateWithGroupCall), { callsByConversation: Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation), { 'fake-group-call-conversation-id': Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation['fake-group-call-conversation-id']), { connectionState: Calling_1.GroupCallConnectionState.NotConnected, ringId: BigInt(123), ringerUuid: UUID_1.UUID.generate().toString() }) }) });
                const action = cancelIncomingGroupCallRing({
                    conversationId: 'fake-group-call-conversation-id',
                    ringId: BigInt(123),
                });
                const result = (0, calling_1.reducer)(state, action);
                chai_1.assert.notProperty(result.callsByConversation, 'fake-group-call-conversation-id');
            });
            it("removes the ring state, but not the call, if it's connected", () => {
                const action = cancelIncomingGroupCallRing({
                    conversationId: 'fake-group-call-conversation-id',
                    ringId: BigInt(123),
                });
                const result = (0, calling_1.reducer)(stateWithIncomingGroupCall, action);
                const call = result.callsByConversation['fake-group-call-conversation-id'];
                // It'd be nice to do this with an assert, but Chai doesn't understand it.
                if ((call === null || call === void 0 ? void 0 : call.callMode) !== Calling_1.CallMode.Group) {
                    throw new Error('Expected to find a group call');
                }
                chai_1.assert.isUndefined(call.ringId);
                chai_1.assert.isUndefined(call.ringerUuid);
            });
        });
        describe('declineCall', () => {
            const { declineCall } = calling_1.actions;
            let declineDirectCall;
            let declineGroupCall;
            beforeEach(function beforeEach() {
                declineDirectCall = this.sandbox.stub(calling_2.calling, 'declineDirectCall');
                declineGroupCall = this.sandbox.stub(calling_2.calling, 'declineGroupCall');
            });
            describe('declining a direct call', () => {
                const getState = () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: stateWithIncomingDirectCall }));
                it('dispatches a DECLINE_DIRECT_CALL action', () => {
                    const dispatch = sinon.spy();
                    declineCall({ conversationId: 'fake-direct-call-conversation-id' })(dispatch, getState, null);
                    sinon.assert.calledOnce(dispatch);
                    sinon.assert.calledWith(dispatch, {
                        type: 'calling/DECLINE_DIRECT_CALL',
                        payload: {
                            conversationId: 'fake-direct-call-conversation-id',
                        },
                    });
                });
                it('asks the calling service to decline the call', () => {
                    const dispatch = sinon.spy();
                    declineCall({ conversationId: 'fake-direct-call-conversation-id' })(dispatch, getState, null);
                    sinon.assert.calledOnce(declineDirectCall);
                    sinon.assert.calledWith(declineDirectCall, 'fake-direct-call-conversation-id');
                });
                it('removes the call from the state', () => {
                    const dispatch = sinon.spy();
                    declineCall({ conversationId: 'fake-direct-call-conversation-id' })(dispatch, getState, null);
                    const action = dispatch.getCall(0).args[0];
                    const result = (0, calling_1.reducer)(stateWithIncomingGroupCall, action);
                    chai_1.assert.notProperty(result.callsByConversation, 'fake-direct-call-conversation-id');
                });
            });
            describe('declining a group call', () => {
                const getState = () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: stateWithIncomingGroupCall }));
                it('dispatches a CANCEL_INCOMING_GROUP_CALL_RING action', () => {
                    const dispatch = sinon.spy();
                    declineCall({ conversationId: 'fake-group-call-conversation-id' })(dispatch, getState, null);
                    sinon.assert.calledOnce(dispatch);
                    sinon.assert.calledWith(dispatch, {
                        type: 'calling/CANCEL_INCOMING_GROUP_CALL_RING',
                        payload: {
                            conversationId: 'fake-group-call-conversation-id',
                            ringId: BigInt(123),
                        },
                    });
                });
                it('asks the calling service to decline the call', () => {
                    const dispatch = sinon.spy();
                    declineCall({ conversationId: 'fake-group-call-conversation-id' })(dispatch, getState, null);
                    sinon.assert.calledOnce(declineGroupCall);
                    sinon.assert.calledWith(declineGroupCall, 'fake-group-call-conversation-id', BigInt(123));
                });
                // NOTE: The state effects of this action are tested with
                //   `cancelIncomingGroupCallRing`.
            });
        });
        describe('groupCallStateChange', () => {
            const { groupCallStateChange } = calling_1.actions;
            function getAction(...args) {
                const dispatch = sinon.spy();
                groupCallStateChange(...args)(dispatch, getEmptyRootState, null);
                return dispatch.getCall(0).args[0];
            }
            it('ignores non-connected calls with no peeked participants', () => {
                const result = (0, calling_1.reducer)((0, calling_1.getEmptyState)(), getAction({
                    conversationId: 'abc123',
                    connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    hasLocalAudio: false,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: [],
                        maxDevices: 16,
                        deviceCount: 0,
                    },
                    remoteParticipants: [],
                }));
                chai_1.assert.deepEqual(result, (0, calling_1.getEmptyState)());
            });
            it('removes the call from the map of conversations if the call is not connected and has no peeked participants or ringer', () => {
                const result = (0, calling_1.reducer)(stateWithGroupCall, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    hasLocalAudio: false,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: [],
                        maxDevices: 16,
                        deviceCount: 0,
                    },
                    remoteParticipants: [],
                }));
                chai_1.assert.notProperty(result.callsByConversation, 'fake-group-call-conversation-id');
            });
            it('removes the call from the map of conversations if the call is not connected and has 1 peeked participant: you', () => {
                const result = (0, calling_1.reducer)(stateWithGroupCall, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    hasLocalAudio: false,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: [ourUuid],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [],
                }));
                chai_1.assert.notProperty(result.callsByConversation, 'fake-group-call-conversation-id');
            });
            it('drops the active call if it is disconnected with no peeked participants', () => {
                const result = (0, calling_1.reducer)(stateWithActiveGroupCall, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    hasLocalAudio: false,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: [],
                        maxDevices: 16,
                        deviceCount: 0,
                    },
                    remoteParticipants: [],
                }));
                chai_1.assert.isUndefined(result.activeCallState);
            });
            it('drops the active call if it is disconnected with 1 peeked participant (you)', () => {
                const result = (0, calling_1.reducer)(stateWithActiveGroupCall, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    hasLocalAudio: false,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: [ourUuid],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [],
                }));
                chai_1.assert.isUndefined(result.activeCallState);
            });
            it('saves a new call to the map of conversations', () => {
                const result = (0, calling_1.reducer)((0, calling_1.getEmptyState)(), getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joining,
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: [creatorUuid],
                        creatorUuid,
                        eraId: 'xyz',
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 123,
                            hasRemoteAudio: true,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 4 / 3,
                        },
                    ],
                }));
                chai_1.assert.deepEqual(result.callsByConversation['fake-group-call-conversation-id'], {
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joining,
                    peekInfo: {
                        uuids: [creatorUuid],
                        creatorUuid,
                        eraId: 'xyz',
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 123,
                            hasRemoteAudio: true,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 4 / 3,
                        },
                    ],
                });
            });
            it('saves a new call to the map of conversations if the call is disconnected by has peeked participants that are not you', () => {
                const result = (0, calling_1.reducer)(stateWithGroupCall, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    hasLocalAudio: false,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [],
                }));
                chai_1.assert.deepEqual(result.callsByConversation['fake-group-call-conversation-id'], {
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [],
                });
            });
            it('saves a call to the map of conversations if the call had a ringer, even if it was otherwise ignorable', () => {
                const result = (0, calling_1.reducer)(stateWithIncomingGroupCall, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    hasLocalAudio: false,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: [],
                        maxDevices: 16,
                        deviceCount: 0,
                    },
                    remoteParticipants: [],
                }));
                chai_1.assert.isDefined(result.callsByConversation['fake-group-call-conversation-id']);
            });
            it('updates a call in the map of conversations', () => {
                const result = (0, calling_1.reducer)(stateWithGroupCall, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joined,
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 456,
                            hasRemoteAudio: false,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 16 / 9,
                        },
                    ],
                }));
                chai_1.assert.deepEqual(result.callsByConversation['fake-group-call-conversation-id'], {
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joined,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 456,
                            hasRemoteAudio: false,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 16 / 9,
                        },
                    ],
                });
            });
            it("keeps the existing ring state if you haven't joined the call", () => {
                const state = Object.assign(Object.assign({}, stateWithGroupCall), { callsByConversation: Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation), { 'fake-group-call-conversation-id': Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation['fake-group-call-conversation-id']), { ringId: BigInt(456), ringerUuid }) }) });
                const result = (0, calling_1.reducer)(state, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 456,
                            hasRemoteAudio: false,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 16 / 9,
                        },
                    ],
                }));
                chai_1.assert.include(result.callsByConversation['fake-group-call-conversation-id'], {
                    callMode: Calling_1.CallMode.Group,
                    ringId: BigInt(456),
                    ringerUuid,
                });
            });
            it("removes the ring state if you've joined the call", () => {
                const state = Object.assign(Object.assign({}, stateWithGroupCall), { callsByConversation: Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation), { 'fake-group-call-conversation-id': Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation['fake-group-call-conversation-id']), { ringId: BigInt(456), ringerUuid }) }) });
                const result = (0, calling_1.reducer)(state, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joined,
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 456,
                            hasRemoteAudio: false,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 16 / 9,
                        },
                    ],
                }));
                chai_1.assert.notProperty(result.callsByConversation['fake-group-call-conversation-id'], 'ringId');
                chai_1.assert.notProperty(result.callsByConversation['fake-group-call-conversation-id'], 'ringerUuid');
            });
            it("if no call is active, doesn't touch the active call state", () => {
                const result = (0, calling_1.reducer)(stateWithGroupCall, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joined,
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 456,
                            hasRemoteAudio: false,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 16 / 9,
                        },
                    ],
                }));
                chai_1.assert.isUndefined(result.activeCallState);
            });
            it("if the call is not active, doesn't touch the active call state", () => {
                const result = (0, calling_1.reducer)(stateWithActiveGroupCall, getAction({
                    conversationId: 'another-fake-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joined,
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 456,
                            hasRemoteAudio: false,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 16 / 9,
                        },
                    ],
                }));
                chai_1.assert.deepEqual(result.activeCallState, {
                    conversationId: 'fake-group-call-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                    isInSpeakerView: false,
                    showParticipantsList: false,
                    safetyNumberChangedUuids: [],
                    outgoingRing: false,
                    pip: false,
                    settingsDialogOpen: false,
                });
            });
            it('if the call is active, updates the active call state', () => {
                var _a, _b, _c;
                const result = (0, calling_1.reducer)(stateWithActiveGroupCall, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joined,
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 456,
                            hasRemoteAudio: false,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 16 / 9,
                        },
                    ],
                }));
                chai_1.assert.strictEqual((_a = result.activeCallState) === null || _a === void 0 ? void 0 : _a.conversationId, 'fake-group-call-conversation-id');
                chai_1.assert.isTrue((_b = result.activeCallState) === null || _b === void 0 ? void 0 : _b.hasLocalAudio);
                chai_1.assert.isTrue((_c = result.activeCallState) === null || _c === void 0 ? void 0 : _c.hasLocalVideo);
            });
            it("doesn't stop ringing if nobody is in the call", () => {
                var _a;
                const state = Object.assign(Object.assign({}, stateWithActiveGroupCall), { activeCallState: Object.assign(Object.assign({}, stateWithActiveGroupCall.activeCallState), { outgoingRing: true }) });
                const result = (0, calling_1.reducer)(state, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joined,
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    peekInfo: {
                        uuids: [],
                        maxDevices: 16,
                        deviceCount: 0,
                    },
                    remoteParticipants: [],
                }));
                chai_1.assert.isTrue((_a = result.activeCallState) === null || _a === void 0 ? void 0 : _a.outgoingRing);
            });
            it('stops ringing if someone enters the call', () => {
                var _a;
                const state = Object.assign(Object.assign({}, stateWithActiveGroupCall), { activeCallState: Object.assign(Object.assign({}, stateWithActiveGroupCall.activeCallState), { outgoingRing: true }) });
                const result = (0, calling_1.reducer)(state, getAction({
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.Joined,
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    peekInfo: {
                        uuids: ['1b9e4d42-1f56-45c5-b6f4-d1be5a54fefa'],
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [],
                }));
                chai_1.assert.isFalse((_a = result.activeCallState) === null || _a === void 0 ? void 0 : _a.outgoingRing);
            });
        });
        describe('peekNotConnectedGroupCall', () => {
            const { peekNotConnectedGroupCall } = calling_1.actions;
            beforeEach(function beforeEach() {
                this.callingServicePeekGroupCall = this.sandbox.stub(calling_2.calling, 'peekGroupCall');
                this.callingServiceUpdateCallHistoryForGroupCall = this.sandbox.stub(calling_2.calling, 'updateCallHistoryForGroupCall');
                this.clock = this.sandbox.useFakeTimers();
            });
            describe('thunk', () => {
                function noopTest(connectionState) {
                    return async function test() {
                        const dispatch = sinon.spy();
                        await peekNotConnectedGroupCall({
                            conversationId: 'fake-group-call-conversation-id',
                        })(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: Object.assign(Object.assign({}, stateWithGroupCall), { callsByConversation: {
                                    'fake-group-call-conversation-id': Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation['fake-group-call-conversation-id']), { connectionState }),
                                } }) })), null);
                        sinon.assert.notCalled(dispatch);
                        sinon.assert.notCalled(this.callingServicePeekGroupCall);
                    };
                }
                it('no-ops if trying to peek at a connecting group call', noopTest(Calling_1.GroupCallConnectionState.Connecting));
                it('no-ops if trying to peek at a connected group call', noopTest(Calling_1.GroupCallConnectionState.Connected));
                it('no-ops if trying to peek at a reconnecting group call', noopTest(Calling_1.GroupCallConnectionState.Reconnecting));
                // These tests are incomplete.
            });
        });
        describe('returnToActiveCall', () => {
            const { returnToActiveCall } = calling_1.actions;
            it('does nothing if not in PiP mode', () => {
                const result = (0, calling_1.reducer)(stateWithActiveDirectCall, returnToActiveCall());
                chai_1.assert.deepEqual(result, stateWithActiveDirectCall);
            });
            it('closes the PiP', () => {
                const state = Object.assign(Object.assign({}, stateWithActiveDirectCall), { activeCallState: Object.assign(Object.assign({}, stateWithActiveDirectCall.activeCallState), { pip: true }) });
                const result = (0, calling_1.reducer)(state, returnToActiveCall());
                chai_1.assert.deepEqual(result, stateWithActiveDirectCall);
            });
        });
        describe('receiveIncomingGroupCall', () => {
            const { receiveIncomingGroupCall } = calling_1.actions;
            it('does nothing if the call was already ringing', () => {
                const action = receiveIncomingGroupCall({
                    conversationId: 'fake-group-call-conversation-id',
                    ringId: BigInt(456),
                    ringerUuid,
                });
                const result = (0, calling_1.reducer)(stateWithIncomingGroupCall, action);
                chai_1.assert.strictEqual(result, stateWithIncomingGroupCall);
            });
            it('does nothing if the call was already joined', () => {
                const state = Object.assign(Object.assign({}, stateWithGroupCall), { callsByConversation: Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation), { 'fake-group-call-conversation-id': Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation['fake-group-call-conversation-id']), { joinState: Calling_1.GroupCallJoinState.Joined }) }) });
                const action = receiveIncomingGroupCall({
                    conversationId: 'fake-group-call-conversation-id',
                    ringId: BigInt(456),
                    ringerUuid,
                });
                const result = (0, calling_1.reducer)(state, action);
                chai_1.assert.strictEqual(result, state);
            });
            it('creates a new group call if one did not exist', () => {
                const action = receiveIncomingGroupCall({
                    conversationId: 'fake-group-call-conversation-id',
                    ringId: BigInt(456),
                    ringerUuid,
                });
                const result = (0, calling_1.reducer)((0, calling_1.getEmptyState)(), action);
                chai_1.assert.deepEqual(result.callsByConversation['fake-group-call-conversation-id'], {
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-group-call-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.NotConnected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    peekInfo: {
                        uuids: [],
                        maxDevices: Infinity,
                        deviceCount: 0,
                    },
                    remoteParticipants: [],
                    ringId: BigInt(456),
                    ringerUuid,
                });
            });
            it('attaches ring state to an existing call', () => {
                const action = receiveIncomingGroupCall({
                    conversationId: 'fake-group-call-conversation-id',
                    ringId: BigInt(456),
                    ringerUuid,
                });
                const result = (0, calling_1.reducer)(stateWithGroupCall, action);
                chai_1.assert.include(result.callsByConversation['fake-group-call-conversation-id'], {
                    ringId: BigInt(456),
                    ringerUuid,
                });
            });
        });
        describe('setLocalAudio', () => {
            const { setLocalAudio } = calling_1.actions;
            beforeEach(function beforeEach() {
                this.callingServiceSetOutgoingAudio = this.sandbox.stub(calling_2.calling, 'setOutgoingAudio');
            });
            it('dispatches a SET_LOCAL_AUDIO_FULFILLED action', () => {
                const dispatch = sinon.spy();
                setLocalAudio({ enabled: true })(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: stateWithActiveDirectCall })), null);
                sinon.assert.calledOnce(dispatch);
                sinon.assert.calledWith(dispatch, {
                    type: 'calling/SET_LOCAL_AUDIO_FULFILLED',
                    payload: { enabled: true },
                });
            });
            it('updates the outgoing audio for the active call', function test() {
                const dispatch = sinon.spy();
                setLocalAudio({ enabled: false })(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: stateWithActiveDirectCall })), null);
                sinon.assert.calledOnce(this.callingServiceSetOutgoingAudio);
                sinon.assert.calledWith(this.callingServiceSetOutgoingAudio, 'fake-direct-call-conversation-id', false);
                setLocalAudio({ enabled: true })(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: stateWithActiveDirectCall })), null);
                sinon.assert.calledTwice(this.callingServiceSetOutgoingAudio);
                sinon.assert.calledWith(this.callingServiceSetOutgoingAudio, 'fake-direct-call-conversation-id', true);
            });
            it('updates the local audio state with SET_LOCAL_AUDIO_FULFILLED', () => {
                var _a;
                const dispatch = sinon.spy();
                setLocalAudio({ enabled: false })(dispatch, () => (Object.assign(Object.assign({}, getEmptyRootState()), { calling: stateWithActiveDirectCall })), null);
                const action = dispatch.getCall(0).args[0];
                const result = (0, calling_1.reducer)(stateWithActiveDirectCall, action);
                chai_1.assert.isFalse((_a = result.activeCallState) === null || _a === void 0 ? void 0 : _a.hasLocalAudio);
            });
        });
        describe('setOutgoingRing', () => {
            const { setOutgoingRing } = calling_1.actions;
            it('enables a desire to ring', () => {
                var _a;
                const action = setOutgoingRing(true);
                const result = (0, calling_1.reducer)(stateWithActiveGroupCall, action);
                chai_1.assert.isTrue((_a = result.activeCallState) === null || _a === void 0 ? void 0 : _a.outgoingRing);
            });
            it('disables a desire to ring', () => {
                var _a;
                const action = setOutgoingRing(false);
                const result = (0, calling_1.reducer)(stateWithActiveDirectCall, action);
                chai_1.assert.isFalse((_a = result.activeCallState) === null || _a === void 0 ? void 0 : _a.outgoingRing);
            });
        });
        describe('showCallLobby', () => {
            const { showCallLobby } = calling_1.actions;
            it('saves a direct call and makes it active', () => {
                const result = (0, calling_1.reducer)((0, calling_1.getEmptyState)(), showCallLobby({
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: 'fake-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                }));
                chai_1.assert.deepEqual(result.callsByConversation['fake-conversation-id'], {
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: 'fake-conversation-id',
                    isIncoming: false,
                    isVideoCall: true,
                });
                chai_1.assert.deepEqual(result.activeCallState, {
                    conversationId: 'fake-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    isInSpeakerView: false,
                    showParticipantsList: false,
                    safetyNumberChangedUuids: [],
                    pip: false,
                    settingsDialogOpen: false,
                    outgoingRing: true,
                });
            });
            it('saves a group call and makes it active', () => {
                var _a, _b;
                const result = (0, calling_1.reducer)((0, calling_1.getEmptyState)(), showCallLobby({
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    isConversationTooBigToRing: false,
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    peekInfo: {
                        uuids: [creatorUuid],
                        creatorUuid,
                        eraId: 'xyz',
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 123,
                            hasRemoteAudio: true,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 4 / 3,
                        },
                    ],
                }));
                chai_1.assert.deepEqual(result.callsByConversation['fake-conversation-id'], {
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-conversation-id',
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    peekInfo: {
                        uuids: [creatorUuid],
                        creatorUuid,
                        eraId: 'xyz',
                        maxDevices: 16,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 123,
                            hasRemoteAudio: true,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 4 / 3,
                        },
                    ],
                });
                chai_1.assert.deepEqual((_a = result.activeCallState) === null || _a === void 0 ? void 0 : _a.conversationId, 'fake-conversation-id');
                chai_1.assert.isFalse((_b = result.activeCallState) === null || _b === void 0 ? void 0 : _b.outgoingRing);
            });
            it('chooses fallback peek info if none is sent and there is no existing call', () => {
                const result = (0, calling_1.reducer)((0, calling_1.getEmptyState)(), showCallLobby({
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-group-call-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    isConversationTooBigToRing: false,
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    peekInfo: undefined,
                    remoteParticipants: [],
                }));
                const call = result.callsByConversation['fake-group-call-conversation-id'];
                chai_1.assert.deepEqual((call === null || call === void 0 ? void 0 : call.callMode) === Calling_1.CallMode.Group && call.peekInfo, {
                    uuids: [],
                    maxDevices: Infinity,
                    deviceCount: 0,
                });
            });
            it("doesn't overwrite an existing group call's peek info if none was sent", () => {
                const result = (0, calling_1.reducer)(stateWithGroupCall, showCallLobby({
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-group-call-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    isConversationTooBigToRing: false,
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    peekInfo: undefined,
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 123,
                            hasRemoteAudio: true,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 4 / 3,
                        },
                    ],
                }));
                const call = result.callsByConversation['fake-group-call-conversation-id'];
                chai_1.assert.deepEqual((call === null || call === void 0 ? void 0 : call.callMode) === Calling_1.CallMode.Group && call.peekInfo, {
                    uuids: [creatorUuid],
                    creatorUuid,
                    eraId: 'xyz',
                    maxDevices: 16,
                    deviceCount: 1,
                });
            });
            it("can overwrite an existing group call's peek info", () => {
                const result = (0, calling_1.reducer)(stateWithGroupCall, showCallLobby({
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-group-call-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    isConversationTooBigToRing: false,
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    peekInfo: {
                        uuids: [differentCreatorUuid],
                        creatorUuid: differentCreatorUuid,
                        eraId: 'abc',
                        maxDevices: 5,
                        deviceCount: 1,
                    },
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 123,
                            hasRemoteAudio: true,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 4 / 3,
                        },
                    ],
                }));
                const call = result.callsByConversation['fake-group-call-conversation-id'];
                chai_1.assert.deepEqual((call === null || call === void 0 ? void 0 : call.callMode) === Calling_1.CallMode.Group && call.peekInfo, {
                    uuids: [differentCreatorUuid],
                    creatorUuid: differentCreatorUuid,
                    eraId: 'abc',
                    maxDevices: 5,
                    deviceCount: 1,
                });
            });
            it("doesn't overwrite an existing group call's ring state if it was set previously", () => {
                const result = (0, calling_1.reducer)(Object.assign(Object.assign({}, stateWithGroupCall), { callsByConversation: {
                        'fake-group-call-conversation-id': Object.assign(Object.assign({}, stateWithGroupCall.callsByConversation['fake-group-call-conversation-id']), { ringId: BigInt(987), ringerUuid }),
                    } }), showCallLobby({
                    callMode: Calling_1.CallMode.Group,
                    conversationId: 'fake-group-call-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: true,
                    isConversationTooBigToRing: false,
                    connectionState: Calling_1.GroupCallConnectionState.Connected,
                    joinState: Calling_1.GroupCallJoinState.NotJoined,
                    peekInfo: undefined,
                    remoteParticipants: [
                        {
                            uuid: remoteUuid,
                            demuxId: 123,
                            hasRemoteAudio: true,
                            hasRemoteVideo: true,
                            presenting: false,
                            sharingScreen: false,
                            videoAspectRatio: 4 / 3,
                        },
                    ],
                }));
                const call = result.callsByConversation['fake-group-call-conversation-id'];
                // It'd be nice to do this with an assert, but Chai doesn't understand it.
                if ((call === null || call === void 0 ? void 0 : call.callMode) !== Calling_1.CallMode.Group) {
                    throw new Error('Expected to find a group call');
                }
                chai_1.assert.strictEqual(call.ringId, BigInt(987));
                chai_1.assert.strictEqual(call.ringerUuid, ringerUuid);
            });
        });
        describe('startCall', () => {
            const { startCall } = calling_1.actions;
            beforeEach(function beforeEach() {
                this.callingStartOutgoingDirectCall = this.sandbox.stub(calling_2.calling, 'startOutgoingDirectCall');
                this.callingJoinGroupCall = this.sandbox
                    .stub(calling_2.calling, 'joinGroupCall')
                    .resolves();
            });
            it('asks the calling service to start an outgoing direct call', async function test() {
                const dispatch = sinon.spy();
                await startCall({
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: '123',
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                })(dispatch, getEmptyRootState, null);
                sinon.assert.calledOnce(this.callingStartOutgoingDirectCall);
                sinon.assert.calledWith(this.callingStartOutgoingDirectCall, '123', true, false);
                sinon.assert.notCalled(this.callingJoinGroupCall);
            });
            it('asks the calling service to join a group call', async function test() {
                const dispatch = sinon.spy();
                await startCall({
                    callMode: Calling_1.CallMode.Group,
                    conversationId: '123',
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                })(dispatch, getEmptyRootState, null);
                sinon.assert.calledOnce(this.callingJoinGroupCall);
                sinon.assert.calledWith(this.callingJoinGroupCall, '123', true, false);
                sinon.assert.notCalled(this.callingStartOutgoingDirectCall);
            });
            it('saves direct calls and makes them active', () => {
                const dispatch = sinon.spy();
                startCall({
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: 'fake-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                })(dispatch, getEmptyRootState, null);
                const action = dispatch.getCall(0).args[0];
                const result = (0, calling_1.reducer)((0, calling_1.getEmptyState)(), action);
                chai_1.assert.deepEqual(result.callsByConversation['fake-conversation-id'], {
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: 'fake-conversation-id',
                    callState: Calling_1.CallState.Prering,
                    isIncoming: false,
                    isVideoCall: false,
                });
                chai_1.assert.deepEqual(result.activeCallState, {
                    conversationId: 'fake-conversation-id',
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                    isInSpeakerView: false,
                    showParticipantsList: false,
                    safetyNumberChangedUuids: [],
                    pip: false,
                    settingsDialogOpen: false,
                    outgoingRing: true,
                });
            });
            it("doesn't dispatch any actions for group calls", () => {
                const dispatch = sinon.spy();
                startCall({
                    callMode: Calling_1.CallMode.Group,
                    conversationId: '123',
                    hasLocalAudio: true,
                    hasLocalVideo: false,
                })(dispatch, getEmptyRootState, null);
                sinon.assert.notCalled(dispatch);
            });
        });
        describe('toggleSettings', () => {
            const { toggleSettings } = calling_1.actions;
            it('toggles the settings dialog', () => {
                var _a, _b, _c;
                const afterOneToggle = (0, calling_1.reducer)(stateWithActiveDirectCall, toggleSettings());
                const afterTwoToggles = (0, calling_1.reducer)(afterOneToggle, toggleSettings());
                const afterThreeToggles = (0, calling_1.reducer)(afterTwoToggles, toggleSettings());
                chai_1.assert.isTrue((_a = afterOneToggle.activeCallState) === null || _a === void 0 ? void 0 : _a.settingsDialogOpen);
                chai_1.assert.isFalse((_b = afterTwoToggles.activeCallState) === null || _b === void 0 ? void 0 : _b.settingsDialogOpen);
                chai_1.assert.isTrue((_c = afterThreeToggles.activeCallState) === null || _c === void 0 ? void 0 : _c.settingsDialogOpen);
            });
        });
        describe('toggleParticipants', () => {
            const { toggleParticipants } = calling_1.actions;
            it('toggles the participants list', () => {
                var _a, _b, _c;
                const afterOneToggle = (0, calling_1.reducer)(stateWithActiveDirectCall, toggleParticipants());
                const afterTwoToggles = (0, calling_1.reducer)(afterOneToggle, toggleParticipants());
                const afterThreeToggles = (0, calling_1.reducer)(afterTwoToggles, toggleParticipants());
                chai_1.assert.isTrue((_a = afterOneToggle.activeCallState) === null || _a === void 0 ? void 0 : _a.showParticipantsList);
                chai_1.assert.isFalse((_b = afterTwoToggles.activeCallState) === null || _b === void 0 ? void 0 : _b.showParticipantsList);
                chai_1.assert.isTrue((_c = afterThreeToggles.activeCallState) === null || _c === void 0 ? void 0 : _c.showParticipantsList);
            });
        });
        describe('togglePip', () => {
            const { togglePip } = calling_1.actions;
            it('toggles the PiP', () => {
                var _a, _b, _c;
                const afterOneToggle = (0, calling_1.reducer)(stateWithActiveDirectCall, togglePip());
                const afterTwoToggles = (0, calling_1.reducer)(afterOneToggle, togglePip());
                const afterThreeToggles = (0, calling_1.reducer)(afterTwoToggles, togglePip());
                chai_1.assert.isTrue((_a = afterOneToggle.activeCallState) === null || _a === void 0 ? void 0 : _a.pip);
                chai_1.assert.isFalse((_b = afterTwoToggles.activeCallState) === null || _b === void 0 ? void 0 : _b.pip);
                chai_1.assert.isTrue((_c = afterThreeToggles.activeCallState) === null || _c === void 0 ? void 0 : _c.pip);
            });
        });
        describe('toggleSpeakerView', () => {
            const { toggleSpeakerView } = calling_1.actions;
            it('toggles speaker view', () => {
                var _a, _b, _c;
                const afterOneToggle = (0, calling_1.reducer)(stateWithActiveGroupCall, toggleSpeakerView());
                const afterTwoToggles = (0, calling_1.reducer)(afterOneToggle, toggleSpeakerView());
                const afterThreeToggles = (0, calling_1.reducer)(afterTwoToggles, toggleSpeakerView());
                chai_1.assert.isTrue((_a = afterOneToggle.activeCallState) === null || _a === void 0 ? void 0 : _a.isInSpeakerView);
                chai_1.assert.isFalse((_b = afterTwoToggles.activeCallState) === null || _b === void 0 ? void 0 : _b.isInSpeakerView);
                chai_1.assert.isTrue((_c = afterThreeToggles.activeCallState) === null || _c === void 0 ? void 0 : _c.isInSpeakerView);
            });
        });
    });
    describe('helpers', () => {
        describe('getActiveCall', () => {
            it('returns undefined if there are no calls', () => {
                chai_1.assert.isUndefined((0, calling_1.getActiveCall)((0, calling_1.getEmptyState)()));
            });
            it('returns undefined if there is no active call', () => {
                chai_1.assert.isUndefined((0, calling_1.getActiveCall)(stateWithDirectCall));
            });
            it('returns the active call', () => {
                chai_1.assert.deepEqual((0, calling_1.getActiveCall)(stateWithActiveDirectCall), {
                    callMode: Calling_1.CallMode.Direct,
                    conversationId: 'fake-direct-call-conversation-id',
                    callState: Calling_1.CallState.Accepted,
                    isIncoming: false,
                    isVideoCall: false,
                    hasRemoteVideo: false,
                });
            });
        });
        describe('isAnybodyElseInGroupCall', () => {
            const fakePeekInfo = (uuids) => ({
                uuids,
                maxDevices: 5,
                deviceCount: uuids.length,
            });
            it('returns false if the peek info has no participants', () => {
                chai_1.assert.isFalse((0, calling_1.isAnybodyElseInGroupCall)(fakePeekInfo([]), remoteUuid));
            });
            it('returns false if the peek info has one participant, you', () => {
                chai_1.assert.isFalse((0, calling_1.isAnybodyElseInGroupCall)(fakePeekInfo([creatorUuid]), creatorUuid));
            });
            it('returns true if the peek info has one participant, someone else', () => {
                chai_1.assert.isTrue((0, calling_1.isAnybodyElseInGroupCall)(fakePeekInfo([creatorUuid]), remoteUuid));
            });
            it('returns true if the peek info has two participants, you and someone else', () => {
                chai_1.assert.isTrue((0, calling_1.isAnybodyElseInGroupCall)(fakePeekInfo([creatorUuid, remoteUuid]), remoteUuid));
            });
        });
    });
});
