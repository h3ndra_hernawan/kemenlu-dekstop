"use strict";
// Copyright 2015-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable
     no-new,
     @typescript-eslint/no-empty-function,
     @typescript-eslint/no-explicit-any
     */
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const events_1 = __importDefault(require("events"));
const long_1 = __importDefault(require("long"));
const dropNull_1 = require("../util/dropNull");
const protobuf_1 = require("../protobuf");
const WebsocketResources_1 = __importDefault(require("../textsecure/WebsocketResources"));
describe('WebSocket-Resource', () => {
    class FakeSocket extends events_1.default {
        sendBytes(_) { }
        close() { }
    }
    const NOW = Date.now();
    beforeEach(function beforeEach() {
        this.sandbox = sinon.createSandbox();
        this.clock = this.sandbox.useFakeTimers({
            now: NOW,
        });
        this.sandbox
            .stub(window.SignalContext.timers, 'setTimeout')
            .callsFake(setTimeout);
        this.sandbox
            .stub(window.SignalContext.timers, 'clearTimeout')
            .callsFake(clearTimeout);
    });
    afterEach(function afterEach() {
        this.sandbox.restore();
    });
    describe('requests and responses', () => {
        it('receives requests and sends responses', done => {
            // mock socket
            const requestId = new long_1.default(0xdeadbeef, 0x7fffffff);
            const socket = new FakeSocket();
            sinon.stub(socket, 'sendBytes').callsFake((data) => {
                var _a, _b, _c;
                const message = protobuf_1.SignalService.WebSocketMessage.decode(data);
                chai_1.assert.strictEqual(message.type, protobuf_1.SignalService.WebSocketMessage.Type.RESPONSE);
                chai_1.assert.strictEqual((_a = message.response) === null || _a === void 0 ? void 0 : _a.message, 'OK');
                chai_1.assert.strictEqual((_b = message.response) === null || _b === void 0 ? void 0 : _b.status, 200);
                const id = (_c = message.response) === null || _c === void 0 ? void 0 : _c.id;
                if (long_1.default.isLong(id)) {
                    (0, chai_1.assert)(id.equals(requestId));
                }
                else {
                    (0, chai_1.assert)(false, `id should be Long, got ${id}`);
                }
                done();
            });
            // actual test
            new WebsocketResources_1.default(socket, {
                handleRequest(request) {
                    chai_1.assert.strictEqual(request.verb, 'PUT');
                    chai_1.assert.strictEqual(request.path, '/some/path');
                    chai_1.assert.deepEqual(request.body, new Uint8Array([1, 2, 3]));
                    request.respond(200, 'OK');
                },
            });
            // mock socket request
            socket.emit('message', {
                type: 'binary',
                binaryData: protobuf_1.SignalService.WebSocketMessage.encode({
                    type: protobuf_1.SignalService.WebSocketMessage.Type.REQUEST,
                    request: {
                        id: requestId,
                        verb: 'PUT',
                        path: '/some/path',
                        body: new Uint8Array([1, 2, 3]),
                    },
                }).finish(),
            });
        });
        it('sends requests and receives responses', async () => {
            // mock socket and request handler
            let requestId;
            const socket = new FakeSocket();
            sinon.stub(socket, 'sendBytes').callsFake((data) => {
                var _a, _b, _c, _d;
                const message = protobuf_1.SignalService.WebSocketMessage.decode(data);
                chai_1.assert.strictEqual(message.type, protobuf_1.SignalService.WebSocketMessage.Type.REQUEST);
                chai_1.assert.strictEqual((_a = message.request) === null || _a === void 0 ? void 0 : _a.verb, 'PUT');
                chai_1.assert.strictEqual((_b = message.request) === null || _b === void 0 ? void 0 : _b.path, '/some/path');
                chai_1.assert.deepEqual((_c = message.request) === null || _c === void 0 ? void 0 : _c.body, new Uint8Array([1, 2, 3]));
                requestId = (0, dropNull_1.dropNull)((_d = message.request) === null || _d === void 0 ? void 0 : _d.id);
            });
            // actual test
            const resource = new WebsocketResources_1.default(socket);
            const promise = resource.sendRequest({
                verb: 'PUT',
                path: '/some/path',
                body: new Uint8Array([1, 2, 3]),
            });
            // mock socket response
            socket.emit('message', {
                type: 'binary',
                binaryData: protobuf_1.SignalService.WebSocketMessage.encode({
                    type: protobuf_1.SignalService.WebSocketMessage.Type.RESPONSE,
                    response: { id: requestId, message: 'OK', status: 200 },
                }).finish(),
            });
            const { status, message } = await promise;
            chai_1.assert.strictEqual(message, 'OK');
            chai_1.assert.strictEqual(status, 200);
        });
    });
    describe('close', () => {
        it('closes the connection', done => {
            const socket = new FakeSocket();
            sinon.stub(socket, 'close').callsFake(() => done());
            const resource = new WebsocketResources_1.default(socket);
            resource.close();
        });
        it('force closes the connection', function test(done) {
            const socket = new FakeSocket();
            const resource = new WebsocketResources_1.default(socket);
            resource.close();
            resource.addEventListener('close', () => done());
            // Wait 5 seconds to forcefully close the connection
            this.clock.next();
        });
    });
    describe('with a keepalive config', () => {
        it('sends keepalives once a minute', function test(done) {
            const socket = new FakeSocket();
            sinon.stub(socket, 'sendBytes').callsFake(data => {
                var _a, _b;
                const message = protobuf_1.SignalService.WebSocketMessage.decode(data);
                chai_1.assert.strictEqual(message.type, protobuf_1.SignalService.WebSocketMessage.Type.REQUEST);
                chai_1.assert.strictEqual((_a = message.request) === null || _a === void 0 ? void 0 : _a.verb, 'GET');
                chai_1.assert.strictEqual((_b = message.request) === null || _b === void 0 ? void 0 : _b.path, '/v1/keepalive');
                done();
            });
            new WebsocketResources_1.default(socket, {
                keepalive: { path: '/v1/keepalive' },
            });
            this.clock.next();
        });
        it('uses / as a default path', function test(done) {
            const socket = new FakeSocket();
            sinon.stub(socket, 'sendBytes').callsFake(data => {
                var _a, _b;
                const message = protobuf_1.SignalService.WebSocketMessage.decode(data);
                chai_1.assert.strictEqual(message.type, protobuf_1.SignalService.WebSocketMessage.Type.REQUEST);
                chai_1.assert.strictEqual((_a = message.request) === null || _a === void 0 ? void 0 : _a.verb, 'GET');
                chai_1.assert.strictEqual((_b = message.request) === null || _b === void 0 ? void 0 : _b.path, '/');
                done();
            });
            new WebsocketResources_1.default(socket, {
                keepalive: true,
            });
            this.clock.next();
        });
        it('optionally disconnects if no response', function thisNeeded1(done) {
            const socket = new FakeSocket();
            sinon.stub(socket, 'close').callsFake(() => done());
            new WebsocketResources_1.default(socket, {
                keepalive: true,
            });
            // One to trigger send
            this.clock.next();
            // Another to trigger send timeout
            this.clock.next();
        });
        it('optionally disconnects if suspended', function thisNeeded1(done) {
            const socket = new FakeSocket();
            sinon.stub(socket, 'close').callsFake(() => done());
            new WebsocketResources_1.default(socket, {
                keepalive: true,
            });
            // Just skip one hour immediately
            this.clock.setSystemTime(NOW + 3600 * 1000);
            this.clock.next();
        });
        it('allows resetting the keepalive timer', function thisNeeded2(done) {
            const startTime = Date.now();
            const socket = new FakeSocket();
            sinon.stub(socket, 'sendBytes').callsFake(data => {
                var _a, _b;
                const message = protobuf_1.SignalService.WebSocketMessage.decode(data);
                chai_1.assert.strictEqual(message.type, protobuf_1.SignalService.WebSocketMessage.Type.REQUEST);
                chai_1.assert.strictEqual((_a = message.request) === null || _a === void 0 ? void 0 : _a.verb, 'GET');
                chai_1.assert.strictEqual((_b = message.request) === null || _b === void 0 ? void 0 : _b.path, '/');
                chai_1.assert.strictEqual(Date.now(), startTime + 60000, 'keepalive time should be one minute');
                done();
            });
            const resource = new WebsocketResources_1.default(socket, {
                keepalive: true,
            });
            setTimeout(() => {
                var _a;
                (_a = resource.keepalive) === null || _a === void 0 ? void 0 : _a.reset();
            }, 5000);
            // Trigger setTimeout above
            this.clock.next();
            // Trigger sendBytes
            this.clock.next();
        });
    });
});
