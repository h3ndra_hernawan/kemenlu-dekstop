"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const scrollToBottom_1 = require("../util/scrollToBottom");
describe('scrollToBottom', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = document.createElement('div');
        document.body.appendChild(sandbox);
    });
    afterEach(() => {
        sandbox.remove();
    });
    it("sets the element's scrollTop to the element's scrollHeight", () => {
        const el = document.createElement('div');
        el.innerText = 'a'.repeat(50000);
        Object.assign(el.style, {
            height: '50px',
            overflow: 'scroll',
            whiteSpace: 'wrap',
            width: '100px',
            wordBreak: 'break-word',
        });
        sandbox.appendChild(el);
        chai_1.assert.strictEqual(el.scrollTop, 0, 'Test is not set up correctly. Element is already scrolled');
        chai_1.assert.isAtLeast(el.scrollHeight, 50, 'Test is not set up correctly. scrollHeight is too low');
        (0, scrollToBottom_1.scrollToBottom)(el);
        chai_1.assert.isAtLeast(el.scrollTop, el.scrollHeight - 50);
    });
});
