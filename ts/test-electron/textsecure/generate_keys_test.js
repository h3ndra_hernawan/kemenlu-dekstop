"use strict";
// Copyright 2015-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Bytes_1 = require("../../Bytes");
const Crypto_1 = require("../../Crypto");
const Curve_1 = require("../../Curve");
const AccountManager_1 = __importDefault(require("../../textsecure/AccountManager"));
const UUID_1 = require("../../types/UUID");
const { textsecure } = window;
const assertEqualBuffers = (a, b) => {
    chai_1.assert.isTrue((0, Crypto_1.constantTimeEqual)(a, b));
};
describe('Key generation', function thisNeeded() {
    const count = 10;
    const ourUuid = new UUID_1.UUID('aaaaaaaa-bbbb-4ccc-9ddd-eeeeeeeeeeee');
    this.timeout(count * 2000);
    function itStoresPreKey(keyId) {
        it(`prekey ${keyId} is valid`, async () => {
            const keyPair = await textsecure.storage.protocol.loadPreKey(ourUuid, keyId);
            (0, chai_1.assert)(keyPair, `PreKey ${keyId} not found`);
        });
    }
    function itStoresSignedPreKey(keyId) {
        it(`signed prekey ${keyId} is valid`, async () => {
            const keyPair = await textsecure.storage.protocol.loadSignedPreKey(ourUuid, keyId);
            (0, chai_1.assert)(keyPair, `SignedPreKey ${keyId} not found`);
        });
    }
    async function validateResultKey(resultKey) {
        const keyPair = await textsecure.storage.protocol.loadPreKey(ourUuid, resultKey.keyId);
        if (!keyPair) {
            throw new Error(`PreKey ${resultKey.keyId} not found`);
        }
        assertEqualBuffers(resultKey.publicKey, keyPair.publicKey().serialize());
    }
    async function validateResultSignedKey(resultSignedKey) {
        const keyPair = await textsecure.storage.protocol.loadSignedPreKey(ourUuid, resultSignedKey.keyId);
        if (!keyPair) {
            throw new Error(`SignedPreKey ${resultSignedKey.keyId} not found`);
        }
        assertEqualBuffers(resultSignedKey.publicKey, keyPair.publicKey().serialize());
    }
    before(async () => {
        const keyPair = (0, Curve_1.generateKeyPair)();
        await textsecure.storage.put('identityKeyMap', {
            [ourUuid.toString()]: {
                privKey: (0, Bytes_1.toBase64)(keyPair.privKey),
                pubKey: (0, Bytes_1.toBase64)(keyPair.pubKey),
            },
        });
        await textsecure.storage.user.setUuidAndDeviceId(ourUuid.toString(), 1);
        await textsecure.storage.protocol.hydrateCaches();
    });
    after(async () => {
        await textsecure.storage.protocol.clearPreKeyStore();
        await textsecure.storage.protocol.clearSignedPreKeysStore();
    });
    describe('the first time', () => {
        let result;
        before(async () => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const accountManager = new AccountManager_1.default({});
            result = await accountManager.generateKeys(count);
        });
        for (let i = 1; i <= count; i += 1) {
            itStoresPreKey(i);
        }
        itStoresSignedPreKey(1);
        it(`result contains ${count} preKeys`, () => {
            chai_1.assert.isArray(result.preKeys);
            chai_1.assert.lengthOf(result.preKeys, count);
            for (let i = 0; i < count; i += 1) {
                chai_1.assert.isObject(result.preKeys[i]);
            }
        });
        it('result contains the correct keyIds', () => {
            for (let i = 0; i < count; i += 1) {
                chai_1.assert.strictEqual(result.preKeys[i].keyId, i + 1);
            }
        });
        it('result contains the correct public keys', async () => {
            await Promise.all(result.preKeys.map(validateResultKey));
        });
        it('returns a signed prekey', () => {
            chai_1.assert.strictEqual(result.signedPreKey.keyId, 1);
            chai_1.assert.instanceOf(result.signedPreKey.signature, Uint8Array);
            return validateResultSignedKey(result.signedPreKey);
        });
    });
    describe('the second time', () => {
        let result;
        before(async () => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const accountManager = new AccountManager_1.default({});
            result = await accountManager.generateKeys(count);
        });
        for (let i = 1; i <= 2 * count; i += 1) {
            itStoresPreKey(i);
        }
        itStoresSignedPreKey(1);
        itStoresSignedPreKey(2);
        it(`result contains ${count} preKeys`, () => {
            chai_1.assert.isArray(result.preKeys);
            chai_1.assert.lengthOf(result.preKeys, count);
            for (let i = 0; i < count; i += 1) {
                chai_1.assert.isObject(result.preKeys[i]);
            }
        });
        it('result contains the correct keyIds', () => {
            for (let i = 1; i <= count; i += 1) {
                chai_1.assert.strictEqual(result.preKeys[i - 1].keyId, i + count);
            }
        });
        it('result contains the correct public keys', async () => {
            await Promise.all(result.preKeys.map(validateResultKey));
        });
        it('returns a signed prekey', () => {
            chai_1.assert.strictEqual(result.signedPreKey.keyId, 2);
            chai_1.assert.instanceOf(result.signedPreKey.signature, Uint8Array);
            return validateResultSignedKey(result.signedPreKey);
        });
    });
    describe('the third time', () => {
        let result;
        before(async () => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const accountManager = new AccountManager_1.default({});
            result = await accountManager.generateKeys(count);
        });
        for (let i = 1; i <= 3 * count; i += 1) {
            itStoresPreKey(i);
        }
        itStoresSignedPreKey(2);
        itStoresSignedPreKey(3);
        it(`result contains ${count} preKeys`, () => {
            chai_1.assert.isArray(result.preKeys);
            chai_1.assert.lengthOf(result.preKeys, count);
            for (let i = 0; i < count; i += 1) {
                chai_1.assert.isObject(result.preKeys[i]);
            }
        });
        it('result contains the correct keyIds', () => {
            for (let i = 1; i <= count; i += 1) {
                chai_1.assert.strictEqual(result.preKeys[i - 1].keyId, i + 2 * count);
            }
        });
        it('result contains the correct public keys', async () => {
            await Promise.all(result.preKeys.map(validateResultKey));
        });
        it('result contains a signed prekey', () => {
            chai_1.assert.strictEqual(result.signedPreKey.keyId, 3);
            chai_1.assert.instanceOf(result.signedPreKey.signature, Uint8Array);
            return validateResultSignedKey(result.signedPreKey);
        });
    });
});
