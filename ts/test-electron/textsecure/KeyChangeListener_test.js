"use strict";
// Copyright 2017-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const uuid_1 = require("uuid");
const Crypto_1 = require("../../Crypto");
const Address_1 = require("../../types/Address");
const UUID_1 = require("../../types/UUID");
const SignalProtocolStore_1 = require("../../SignalProtocolStore");
const KeyChangeListener = __importStar(require("../../textsecure/KeyChangeListener"));
const { Whisper } = window;
describe('KeyChangeListener', () => {
    let oldNumberId;
    let oldUuidId;
    const ourUuid = (0, uuid_1.v4)();
    const uuidWithKeyChange = (0, uuid_1.v4)();
    const address = Address_1.Address.create(uuidWithKeyChange, 1);
    const oldKey = (0, Crypto_1.getRandomBytes)(33);
    const newKey = (0, Crypto_1.getRandomBytes)(33);
    let store;
    before(async () => {
        window.ConversationController.reset();
        await window.ConversationController.load();
        const { storage } = window.textsecure;
        oldNumberId = storage.get('number_id');
        oldUuidId = storage.get('uuid_id');
        await storage.put('number_id', '+14155555556.2');
        await storage.put('uuid_id', `${ourUuid}.2`);
    });
    after(async () => {
        await window.Signal.Data.removeAll();
        const { storage } = window.textsecure;
        await storage.fetch();
        if (oldNumberId) {
            await storage.put('number_id', oldNumberId);
        }
        if (oldUuidId) {
            await storage.put('uuid_id', oldUuidId);
        }
    });
    let convo;
    beforeEach(async () => {
        window.ConversationController.reset();
        await window.ConversationController.load();
        convo = window.ConversationController.dangerouslyCreateAndAdd({
            id: uuidWithKeyChange,
            type: 'private',
        });
        await window.Signal.Data.saveConversation(convo.attributes);
        store = new SignalProtocolStore_1.SignalProtocolStore();
        await store.hydrateCaches();
        KeyChangeListener.init(store);
        return store.saveIdentity(address, oldKey);
    });
    afterEach(async () => {
        await window.Signal.Data.removeAllMessagesInConversation(convo.id, {
            logId: uuidWithKeyChange,
            MessageCollection: Whisper.MessageCollection,
        });
        await window.Signal.Data.removeConversation(convo.id, {
            Conversation: Whisper.Conversation,
        });
        await store.removeIdentityKey(new UUID_1.UUID(uuidWithKeyChange));
    });
    describe('When we have a conversation with this contact', () => {
        it('generates a key change notice in the private conversation with this contact', done => {
            const original = convo.addKeyChange;
            convo.addKeyChange = async (keyChangedId) => {
                chai_1.assert.equal(uuidWithKeyChange, keyChangedId.toString());
                convo.addKeyChange = original;
                done();
            };
            store.saveIdentity(address, newKey);
        });
    });
    describe('When we have a group with this contact', () => {
        let groupConvo;
        beforeEach(async () => {
            groupConvo = window.ConversationController.dangerouslyCreateAndAdd({
                id: 'groupId',
                type: 'group',
                members: [convo.id],
            });
            await window.Signal.Data.saveConversation(groupConvo.attributes);
        });
        afterEach(async () => {
            await window.Signal.Data.removeAllMessagesInConversation(groupConvo.id, {
                logId: uuidWithKeyChange,
                MessageCollection: Whisper.MessageCollection,
            });
            await window.Signal.Data.removeConversation(groupConvo.id, {
                Conversation: Whisper.Conversation,
            });
        });
        it('generates a key change notice in the group conversation with this contact', done => {
            const original = groupConvo.addKeyChange;
            groupConvo.addKeyChange = async (keyChangedId) => {
                chai_1.assert.equal(uuidWithKeyChange, keyChangedId.toString());
                groupConvo.addKeyChange = original;
                done();
            };
            store.saveIdentity(address, newKey);
        });
    });
});
