"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const node_fetch_1 = require("node-fetch");
const sinon = __importStar(require("sinon"));
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const abort_controller_1 = __importDefault(require("abort-controller"));
const MIME_1 = require("../../types/MIME");
const log = __importStar(require("../../logging/log"));
const linkPreviewFetch_1 = require("../../linkPreviews/linkPreviewFetch");
describe('link preview fetching', () => {
    // We'll use this to create a fake `fetch`. We'll want to call `.resolves` or
    //   `.rejects` on it (meaning that it needs to be a Sinon Stub type), but we'll also
    //   want it to be a fake `fetch`. `any` seems like the best "supertype" there.
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function stub() {
        return sinon.stub();
    }
    let sandbox;
    let warn;
    beforeEach(() => {
        sandbox = sinon.createSandbox();
        warn = sandbox.stub(log, 'warn');
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('fetchLinkPreviewMetadata', () => {
        const makeHtml = (stuffInHead = []) => `
    <!doctype html>
    <html>
      <head>${stuffInHead.join('\n')}</head>
      <body>should be ignored</body>
    </html>
    `;
        const makeResponse = ({ status = 200, headers = {}, body = makeHtml(['<title>test title</title>']), url = 'https://example.com', } = {}) => {
            let bodyLength;
            let bodyStream;
            if (!body) {
                bodyLength = 0;
                bodyStream = null;
            }
            else if (typeof body === 'string') {
                const asBytes = new TextEncoder().encode(body);
                bodyLength = asBytes.length;
                bodyStream = (function stream() {
                    return __asyncGenerator(this, arguments, function* stream_1() {
                        yield yield __await(asBytes);
                    });
                })();
            }
            else if (body instanceof Uint8Array) {
                bodyLength = body.length;
                bodyStream = (function stream() {
                    return __asyncGenerator(this, arguments, function* stream_2() {
                        yield yield __await(body);
                    });
                })();
            }
            else {
                bodyLength = null;
                bodyStream = body;
            }
            const headersObj = new Headers();
            Object.entries(Object.assign({ 'Content-Type': 'text/html; charset=utf-8', 'Content-Length': bodyLength === null ? null : String(bodyLength) }, headers)).forEach(([headerName, headerValue]) => {
                if (headerValue) {
                    headersObj.set(headerName, headerValue);
                }
            });
            return {
                headers: headersObj,
                body: bodyStream,
                ok: status >= 200 && status <= 299,
                status,
                url,
            };
        };
        it('handles the "kitchen sink" of results', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<meta property="og:title" content="test title">',
                    '<meta property="og:description" content="test description">',
                    '<meta property="og:image" content="https://example.com/image.jpg">',
                    '<meta property="og:published_time" content="2020-04-20T12:34:56.009Z">',
                ]),
            }));
            chai_1.assert.deepEqual(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), {
                title: 'test title',
                description: 'test description',
                date: 1587386096009,
                imageHref: 'https://example.com/image.jpg',
            });
        });
        it('handles image href sources in the correct order', async () => {
            const orderedImageHrefSources = [
                {
                    tag: '<meta property="og:image" content="https://example.com/og-image.jpg">',
                    expectedHref: 'https://example.com/og-image.jpg',
                },
                {
                    tag: '<meta property="og:image:url" content="https://example.com/og-image-url.jpg">',
                    expectedHref: 'https://example.com/og-image-url.jpg',
                },
                {
                    tag: '<link rel="apple-touch-icon" href="https://example.com/apple-touch-icon.jpg">',
                    expectedHref: 'https://example.com/apple-touch-icon.jpg',
                },
                {
                    tag: '<link rel="apple-touch-icon-precomposed" href="https://example.com/apple-touch-icon-precomposed.jpg">',
                    expectedHref: 'https://example.com/apple-touch-icon-precomposed.jpg',
                },
                {
                    tag: '<link rel="shortcut icon" href="https://example.com/shortcut-icon.jpg">',
                    expectedHref: 'https://example.com/shortcut-icon.jpg',
                },
                {
                    tag: '<link rel="icon" href="https://example.com/icon.jpg">',
                    expectedHref: 'https://example.com/icon.jpg',
                },
            ];
            for (let i = orderedImageHrefSources.length - 1; i >= 0; i -= 1) {
                const imageTags = orderedImageHrefSources
                    .slice(i)
                    .map(({ tag }) => tag)
                    // Reverse the array to make sure that we're prioritizing properly,
                    //   instead of just using whichever comes first.
                    .reverse();
                const fakeFetch = stub().resolves(makeResponse({
                    body: makeHtml([
                        '<meta property="og:title" content="test title">',
                        ...imageTags,
                    ]),
                }));
                // eslint-disable-next-line no-await-in-loop
                const val = await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal);
                chai_1.assert.propertyVal(val, 'imageHref', orderedImageHrefSources[i].expectedHref);
            }
        });
        it('logs no warnings if everything goes smoothly', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<meta property="og:title" content="test title">',
                    '<meta property="og:description" content="test description">',
                    '<meta property="og:image" content="https://example.com/image.jpg">',
                    '<meta property="og:published_time" content="2020-04-20T12:34:56.009Z">',
                ]),
            }));
            await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal);
            sinon.assert.notCalled(warn);
        });
        it('sends WhatsApp as the User-Agent for compatibility', async () => {
            const fakeFetch = stub().resolves(makeResponse());
            await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal);
            sinon.assert.calledWith(fakeFetch, 'https://example.com', sinon.match({
                headers: {
                    'User-Agent': 'WhatsApp/2',
                },
            }));
        });
        it('returns null if the request fails', async () => {
            const fakeFetch = stub().rejects(new Error('Test request failure'));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal));
            sinon.assert.calledOnce(warn);
            sinon.assert.calledWith(warn, 'fetchLinkPreviewMetadata: failed to fetch link preview HTML; bailing');
        });
        it("returns null if the response status code isn't 2xx", async () => {
            await Promise.all([100, 304, 400, 404, 500, 0, -200].map(async (status) => {
                const fakeFetch = stub().resolves(makeResponse({ status }));
                chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal));
                sinon.assert.calledWith(warn, `fetchLinkPreviewMetadata: got a ${status} status code; bailing`);
            }));
        });
        it("doesn't use fetch's automatic redirection behavior", async () => {
            const fakeFetch = stub().resolves(makeResponse());
            await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal);
            sinon.assert.calledWith(fakeFetch, 'https://example.com', sinon.match({ redirect: 'manual' }));
        });
        [301, 302, 303, 307, 308].forEach(status => {
            it(`handles ${status} redirects`, async () => {
                const fakeFetch = stub();
                fakeFetch.onFirstCall().resolves(makeResponse({
                    status,
                    headers: { Location: 'https://example.com/2' },
                    body: null,
                }));
                fakeFetch.onSecondCall().resolves(makeResponse());
                chai_1.assert.deepEqual(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), {
                    title: 'test title',
                    description: null,
                    date: null,
                    imageHref: null,
                });
                sinon.assert.calledTwice(fakeFetch);
                sinon.assert.calledWith(fakeFetch.getCall(0), 'https://example.com');
                sinon.assert.calledWith(fakeFetch.getCall(1), 'https://example.com/2');
            });
            it(`returns null when seeing a ${status} status with no Location header`, async () => {
                const fakeFetch = stub().resolves(makeResponse({ status }));
                chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal));
            });
        });
        it('handles relative redirects', async () => {
            const fakeFetch = stub();
            fakeFetch.onFirstCall().resolves(makeResponse({
                status: 301,
                headers: { Location: '/2/' },
                body: null,
            }));
            fakeFetch.onSecondCall().resolves(makeResponse({
                status: 301,
                headers: { Location: '3' },
                body: null,
            }));
            fakeFetch.onThirdCall().resolves(makeResponse());
            chai_1.assert.deepEqual(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), {
                title: 'test title',
                description: null,
                date: null,
                imageHref: null,
            });
            sinon.assert.calledThrice(fakeFetch);
            sinon.assert.calledWith(fakeFetch.getCall(0), 'https://example.com');
            sinon.assert.calledWith(fakeFetch.getCall(1), 'https://example.com/2/');
            sinon.assert.calledWith(fakeFetch.getCall(2), 'https://example.com/2/3');
        });
        it('returns null if redirecting to an insecure HTTP URL', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                status: 301,
                headers: { Location: 'http://example.com' },
                body: null,
            }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal));
            sinon.assert.calledOnce(fakeFetch);
        });
        it("returns null if there's a redirection loop", async () => {
            const fakeFetch = stub();
            fakeFetch.onFirstCall().resolves(makeResponse({
                status: 301,
                headers: { Location: '/2/' },
                body: null,
            }));
            fakeFetch.onSecondCall().resolves(makeResponse({
                status: 301,
                headers: { Location: '/start' },
                body: null,
            }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com/start', new abort_controller_1.default().signal));
            sinon.assert.calledTwice(fakeFetch);
        });
        it('returns null if redirecting more than 20 times', async () => {
            const fakeFetch = stub().callsFake(async () => makeResponse({
                status: 301,
                headers: { Location: `/${Math.random()}` },
                body: null,
            }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com/start', new abort_controller_1.default().signal));
            sinon.assert.callCount(fakeFetch, 20);
        });
        it('returns null if the response has no body', async () => {
            const fakeFetch = stub().resolves(makeResponse({ body: null }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal));
            sinon.assert.calledWith(warn, 'fetchLinkPreviewMetadata: no response body; bailing');
        });
        it('returns null if the result body is too short', async () => {
            const fakeFetch = stub().resolves(makeResponse({ body: '<title>' }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal));
            sinon.assert.calledOnce(warn);
            sinon.assert.calledWith(warn, 'fetchLinkPreviewMetadata: Content-Length is too short; bailing');
        });
        it('returns null if the result is meant to be downloaded', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                headers: { 'Content-Disposition': 'attachment' },
            }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal));
            sinon.assert.calledOnce(warn);
            sinon.assert.calledWith(warn, 'fetchLinkPreviewMetadata: Content-Disposition header is not inline; bailing');
        });
        it('allows an explicitly inline Content-Disposition header', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                headers: { 'Content-Disposition': 'inline' },
            }));
            chai_1.assert.deepEqual(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), {
                title: 'test title',
                description: null,
                date: null,
                imageHref: null,
            });
        });
        it('returns null if the Content-Type is not HTML', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                headers: { 'Content-Type': 'text/plain' },
            }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal));
            sinon.assert.calledOnce(warn);
            sinon.assert.calledWith(warn, 'fetchLinkPreviewMetadata: Content-Type is not HTML; bailing');
        });
        it('accepts non-lowercase Content-Type headers', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                headers: { 'Content-Type': 'TEXT/HTML; chArsEt=utf-8' },
            }));
            chai_1.assert.deepEqual(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), {
                title: 'test title',
                description: null,
                date: null,
                imageHref: null,
            });
        });
        it('parses the response as UTF-8 if the body contains a byte order mark', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                headers: {
                    'Content-Type': 'text/html',
                },
                body: (function body() {
                    return __asyncGenerator(this, arguments, function* body_1() {
                        yield yield __await(new Uint8Array([0xef, 0xbb, 0xbf]));
                        yield yield __await(new TextEncoder().encode('<!doctype html><title>\u{1F389}</title>'));
                    });
                })(),
            }));
            chai_1.assert.deepEqual(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), {
                title: '🎉',
                description: null,
                date: null,
                imageHref: null,
            });
        });
        it('respects the UTF-8 byte order mark above the Content-Type header', async () => {
            const bom = new Uint8Array([0xef, 0xbb, 0xbf]);
            const titleHtml = new TextEncoder().encode('<title>\u{1F389}</title>');
            const fakeFetch = stub().resolves(makeResponse({
                headers: {
                    'Content-Type': 'text/html; charset=latin1',
                },
                body: (function body() {
                    return __asyncGenerator(this, arguments, function* body_2() {
                        yield yield __await(bom);
                        yield yield __await(titleHtml);
                    });
                })(),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'title', '🎉');
        });
        it('respects the UTF-8 byte order mark above a <meta http-equiv> in the document', async () => {
            const bom = new Uint8Array([0xef, 0xbb, 0xbf]);
            const titleHtml = new TextEncoder().encode('<title>\u{1F389}</title>');
            const endHeadHtml = new TextEncoder().encode('</head>');
            const fakeFetch = stub().resolves(makeResponse({
                headers: {
                    'Content-Type': 'text/html',
                },
                body: (function body() {
                    return __asyncGenerator(this, arguments, function* body_3() {
                        yield yield __await(bom);
                        yield yield __await(new TextEncoder().encode('<!doctype html><head><meta http-equiv="content-type" content="text/html; charset=latin1">'));
                        yield yield __await(titleHtml);
                        yield yield __await(endHeadHtml);
                    });
                })(),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'title', '🎉');
        });
        it('respects the UTF-8 byte order mark above a <meta charset> in the document', async () => {
            const bom = new Uint8Array([0xef, 0xbb, 0xbf]);
            const titleHtml = new TextEncoder().encode('<title>\u{1F389}</title>');
            const endHeadHtml = new TextEncoder().encode('</head>');
            const fakeFetch = stub().resolves(makeResponse({
                headers: {
                    'Content-Type': 'text/html',
                },
                body: (function body() {
                    return __asyncGenerator(this, arguments, function* body_4() {
                        yield yield __await(bom);
                        yield yield __await(new TextEncoder().encode('<!doctype html><head><meta charset="utf-8">'));
                        yield yield __await(titleHtml);
                        yield yield __await(endHeadHtml);
                    });
                })(),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'title', '🎉');
        });
        it('respects the Content-Type header above anything in the HTML', async () => {
            const titleHtml = new TextEncoder().encode('<title>\u{1F389}</title>');
            const endHeadHtml = new TextEncoder().encode('</head>');
            {
                const fakeFetch = stub().resolves(makeResponse({
                    headers: {
                        'Content-Type': 'text/html; charset=utf-8',
                    },
                    body: (function body() {
                        return __asyncGenerator(this, arguments, function* body_5() {
                            yield yield __await(new TextEncoder().encode('<!doctype html><head><meta http-equiv="content-type" content="text/html; charset=latin1">'));
                            yield yield __await(titleHtml);
                            yield yield __await(endHeadHtml);
                        });
                    })(),
                }));
                chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'title', '🎉');
            }
            {
                const fakeFetch = stub().resolves(makeResponse({
                    headers: {
                        'Content-Type': 'text/html; charset=utf-8',
                    },
                    body: (function body() {
                        return __asyncGenerator(this, arguments, function* body_6() {
                            yield yield __await(new TextEncoder().encode('<!doctype html><head><meta charset="utf-8">'));
                            yield yield __await(titleHtml);
                            yield yield __await(endHeadHtml);
                        });
                    })(),
                }));
                chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'title', '🎉');
            }
        });
        it('prefers the Content-Type http-equiv in the HTML above <meta charset>', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                headers: {
                    'Content-Type': 'text/html',
                },
                body: makeHtml([
                    '<meta http-equiv="content-type" content="text/html; charset=utf8">',
                    '<meta charset="latin1">',
                    '<title>\u{1F389}</title>',
                ]),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'title', '🎉');
        });
        it('parses non-UTF8 encodings', async () => {
            const titleBytes = new Uint8Array([0x61, 0x71, 0x75, 0xed]);
            chai_1.assert.notDeepEqual(new TextDecoder('utf8').decode(titleBytes), new TextDecoder('latin1').decode(titleBytes), 'Test data was not set up correctly');
            const fakeFetch = stub().resolves(makeResponse({
                headers: {
                    'Content-Type': 'text/html; charset=latin1',
                },
                body: (function body() {
                    return __asyncGenerator(this, arguments, function* body_7() {
                        yield yield __await(new TextEncoder().encode('<title>'));
                        yield yield __await(titleBytes);
                        yield yield __await(new TextEncoder().encode('</title>'));
                    });
                })(),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'title', 'aquí');
        });
        it('handles incomplete bodies', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: (function body() {
                    return __asyncGenerator(this, arguments, function* body_8() {
                        yield yield __await(new TextEncoder().encode('<!doctype html><head><title>foo bar</title><meta'));
                        throw new Error('Test request error');
                    });
                })(),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'title', 'foo bar');
            sinon.assert.calledOnce(warn);
            sinon.assert.calledWith(warn, 'getHtmlDocument: error when reading body; continuing with what we got');
        });
        it('stops reading the body after cancelation', async () => {
            const shouldNeverBeCalled = sinon.stub();
            const abortController = new abort_controller_1.default();
            const fakeFetch = stub().resolves(makeResponse({
                body: (function body() {
                    return __asyncGenerator(this, arguments, function* body_9() {
                        yield yield __await(new TextEncoder().encode('<!doctype html><head>'));
                        abortController.abort();
                        yield yield __await(new TextEncoder().encode('<title>should be dropped</title>'));
                        shouldNeverBeCalled();
                    });
                })(),
            }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', abortController.signal));
            sinon.assert.notCalled(shouldNeverBeCalled);
        });
        it('stops reading bodies after 1000 kilobytes', async function test() {
            const shouldNeverBeCalled = sinon.stub();
            const fakeFetch = stub().resolves(makeResponse({
                body: (function body() {
                    return __asyncGenerator(this, arguments, function* body_10() {
                        yield yield __await(new TextEncoder().encode('<!doctype html><head><title>foo bar</title>'));
                        const spaces = new Uint8Array(250 * 1024).fill(32);
                        yield yield __await(spaces);
                        yield yield __await(spaces);
                        yield yield __await(spaces);
                        yield yield __await(spaces);
                        yield yield __await(spaces);
                        shouldNeverBeCalled();
                        yield yield __await(new TextEncoder().encode('<meta property="og:description" content="should be ignored">'));
                    });
                })(),
            }));
            chai_1.assert.deepEqual(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), {
                title: 'foo bar',
                description: null,
                date: null,
                imageHref: null,
            });
            sinon.assert.notCalled(shouldNeverBeCalled);
        });
        it("returns null if the HTML doesn't contain a title, even if it contains other values", async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<meta property="og:description" content="ignored">',
                    '<meta property="og:image" content="https://example.com/ignored.jpg">',
                    `<meta property="og:published_time" content="${new Date().toISOString()}">`,
                ]),
            }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal));
            sinon.assert.calledOnce(warn);
            sinon.assert.calledWith(warn, "parseMetadata: HTML document doesn't have a title; bailing");
        });
        it('prefers og:title to document.title', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<title>ignored</title>',
                    '<meta property="og:title" content="foo bar">',
                ]),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'title', 'foo bar');
        });
        it('prefers og:description to <meta name="description">', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<title>foo</title>',
                    '<meta name="description" content="ignored">',
                    '<meta property="og:description" content="bar">',
                ]),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'description', 'bar');
        });
        it('parses <meta name="description">', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<title>foo</title>',
                    '<meta name="description" content="bar">',
                ]),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'description', 'bar');
        });
        it('ignores empty descriptions', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<title>foo</title>',
                    '<meta property="og:description" content="">',
                ]),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'description', null);
        });
        it('parses absolute image URLs', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<title>foo</title>',
                    '<meta property="og:image" content="https://example.com/image.jpg">',
                ]),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'imageHref', 'https://example.com/image.jpg');
        });
        it('parses relative image URLs', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<title>foo</title>',
                    '<meta property="og:image" content="assets/image.jpg">',
                ]),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'imageHref', 'https://example.com/assets/image.jpg');
        });
        it('relative image URL resolution is relative to the final URL after redirects, not the original URL', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<title>foo</title>',
                    '<meta property="og:image" content="image.jpg">',
                ]),
                url: 'https://bar.example/assets/',
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://foo.example', new abort_controller_1.default().signal), 'imageHref', 'https://bar.example/assets/image.jpg');
        });
        it('ignores empty image URLs', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<title>foo</title>',
                    '<meta property="og:image" content="">',
                ]),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'imageHref', null);
        });
        it('ignores blank image URLs', async () => {
            const fakeFetch = stub().resolves(makeResponse({
                body: makeHtml([
                    '<title>foo</title>',
                    '<meta property="og:image" content="  ">',
                ]),
            }));
            chai_1.assert.propertyVal(await (0, linkPreviewFetch_1.fetchLinkPreviewMetadata)(fakeFetch, 'https://example.com', new abort_controller_1.default().signal), 'imageHref', null);
        });
    });
    describe('fetchLinkPreviewImage', () => {
        const readFixture = async (filename) => {
            const result = await fs.promises.readFile(path.join(__dirname, '..', '..', '..', 'fixtures', filename));
            (0, chai_1.assert)(result.length > 10, `Test failed to read fixture ${filename}`);
            return result;
        };
        [
            {
                title: 'JPEG',
                contentType: 'image/jpeg',
                fixtureFilename: 'kitten-1-64-64.jpg',
            },
            {
                title: 'PNG',
                contentType: 'image/png',
                fixtureFilename: 'freepngs-2cd43b_bed7d1327e88454487397574d87b64dc_mv2.png',
            },
            {
                title: 'GIF',
                contentType: 'image/gif',
                fixtureFilename: 'giphy-GVNvOUpeYmI7e.gif',
            },
            {
                title: 'WEBP',
                contentType: 'image/webp',
                fixtureFilename: '512x515-thumbs-up-lincoln.webp',
            },
            {
                title: 'ICO',
                contentType: 'image/x-icon',
                fixtureFilename: 'kitten-1-64-64.ico',
            },
        ].forEach(({ title, contentType, fixtureFilename }) => {
            it(`handles ${title} images`, async () => {
                const fixture = await readFixture(fixtureFilename);
                const fakeFetch = stub().resolves(new node_fetch_1.Response(fixture, {
                    headers: {
                        'Content-Type': contentType,
                        'Content-Length': fixture.length.toString(),
                    },
                }));
                chai_1.assert.deepEqual(await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', new abort_controller_1.default().signal), {
                    data: fixture,
                    contentType: (0, MIME_1.stringToMIMEType)(contentType),
                });
            });
        });
        it('returns null if the request fails', async () => {
            const fakeFetch = stub().rejects(new Error('Test request failure'));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', new abort_controller_1.default().signal));
            sinon.assert.calledOnce(warn);
            sinon.assert.calledWith(warn, 'fetchLinkPreviewImage: failed to fetch image; bailing');
        });
        it("returns null if the response status code isn't 2xx", async () => {
            const fixture = await readFixture('kitten-1-64-64.jpg');
            await Promise.all([400, 404, 500, 598].map(async (status) => {
                const fakeFetch = stub().resolves(new node_fetch_1.Response(fixture, {
                    status,
                    headers: {
                        'Content-Type': 'image/jpeg',
                        'Content-Length': fixture.length.toString(),
                    },
                }));
                chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', new abort_controller_1.default().signal));
                sinon.assert.calledWith(warn, `fetchLinkPreviewImage: got a ${status} status code; bailing`);
            }));
        });
        // Most of the redirect behavior is tested above.
        it('handles 301 redirects', async () => {
            const fixture = await readFixture('kitten-1-64-64.jpg');
            const fakeFetch = stub();
            fakeFetch.onFirstCall().resolves(new node_fetch_1.Response(Buffer.from(''), {
                status: 301,
                headers: {
                    Location: '/result.jpg',
                },
            }));
            fakeFetch.onSecondCall().resolves(new node_fetch_1.Response(fixture, {
                headers: {
                    'Content-Type': MIME_1.IMAGE_JPEG,
                    'Content-Length': fixture.length.toString(),
                },
            }));
            chai_1.assert.deepEqual(await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', new abort_controller_1.default().signal), {
                data: fixture,
                contentType: MIME_1.IMAGE_JPEG,
            });
            sinon.assert.calledTwice(fakeFetch);
            sinon.assert.calledWith(fakeFetch.getCall(0), 'https://example.com/img');
            sinon.assert.calledWith(fakeFetch.getCall(1), 'https://example.com/result.jpg');
        });
        it('returns null if the response is too small', async () => {
            const fakeFetch = stub().resolves(new node_fetch_1.Response(await readFixture('kitten-1-64-64.jpg'), {
                headers: {
                    'Content-Type': 'image/jpeg',
                    'Content-Length': '2',
                },
            }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', new abort_controller_1.default().signal));
            sinon.assert.calledOnce(warn);
            sinon.assert.calledWith(warn, 'fetchLinkPreviewImage: Content-Length is too short; bailing');
        });
        it('returns null if the response is too large', async () => {
            const fakeFetch = stub().resolves(new node_fetch_1.Response(await readFixture('kitten-1-64-64.jpg'), {
                headers: {
                    'Content-Type': 'image/jpeg',
                    'Content-Length': '123456789',
                },
            }));
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', new abort_controller_1.default().signal));
            sinon.assert.calledOnce(warn);
            sinon.assert.calledWith(warn, 'fetchLinkPreviewImage: Content-Length is too large or is unset; bailing');
        });
        it('returns null if the Content-Type is not a valid image', async () => {
            const fixture = await readFixture('kitten-1-64-64.jpg');
            await Promise.all(['', 'image/tiff', 'video/mp4', 'text/plain', 'application/html'].map(async (contentType) => {
                const fakeFetch = stub().resolves(new node_fetch_1.Response(fixture, {
                    headers: {
                        'Content-Type': contentType,
                        'Content-Length': fixture.length.toString(),
                    },
                }));
                chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', new abort_controller_1.default().signal));
                sinon.assert.calledWith(warn, 'fetchLinkPreviewImage: Content-Type is not an image; bailing');
            }));
        });
        it('sends WhatsApp as the User-Agent for compatibility', async () => {
            const fakeFetch = stub().resolves(new node_fetch_1.Response(Buffer.from('')));
            await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', new abort_controller_1.default().signal);
            sinon.assert.calledWith(fakeFetch, 'https://example.com/img', sinon.match({
                headers: {
                    'User-Agent': 'WhatsApp/2',
                },
            }));
        });
        it("doesn't read the image if the request was aborted before reading started", async () => {
            const abortController = new abort_controller_1.default();
            const fixture = await readFixture('kitten-1-64-64.jpg');
            const fakeFetch = stub().callsFake(() => {
                const response = new node_fetch_1.Response(fixture, {
                    headers: {
                        'Content-Type': 'image/jpeg',
                        'Content-Length': fixture.length.toString(),
                    },
                });
                sinon
                    .stub(response, 'buffer')
                    .rejects(new Error('Should not be called'));
                sinon.stub(response, 'blob').rejects(new Error('Should not be called'));
                sinon.stub(response, 'text').rejects(new Error('Should not be called'));
                sinon.stub(response, 'body').get(() => {
                    throw new Error('Should not be accessed');
                });
                abortController.abort();
                return response;
            });
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', abortController.signal));
        });
        it('returns null if the request was aborted after the image was read', async () => {
            const abortController = new abort_controller_1.default();
            const fixture = await readFixture('kitten-1-64-64.jpg');
            const fakeFetch = stub().callsFake(() => {
                const response = new node_fetch_1.Response(fixture, {
                    headers: {
                        'Content-Type': 'image/jpeg',
                        'Content-Length': fixture.length.toString(),
                    },
                });
                const oldBufferMethod = response.buffer.bind(response);
                sinon.stub(response, 'buffer').callsFake(async () => {
                    const data = await oldBufferMethod();
                    abortController.abort();
                    return data;
                });
                return response;
            });
            chai_1.assert.isNull(await (0, linkPreviewFetch_1.fetchLinkPreviewImage)(fakeFetch, 'https://example.com/img', abortController.signal));
        });
    });
});
