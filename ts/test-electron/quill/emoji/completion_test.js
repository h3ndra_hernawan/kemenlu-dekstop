"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon_1 = __importDefault(require("sinon"));
const completion_1 = require("../../../quill/emoji/completion");
describe('emojiCompletion', () => {
    let emojiCompletion;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let mockQuill;
    beforeEach(function beforeEach() {
        mockQuill = {
            getLeaf: sinon_1.default.stub(),
            getSelection: sinon_1.default.stub(),
            keyboard: {
                addBinding: sinon_1.default.stub(),
            },
            on: sinon_1.default.stub(),
            setSelection: sinon_1.default.stub(),
            updateContents: sinon_1.default.stub(),
        };
        const options = {
            onPickEmoji: sinon_1.default.stub(),
            setEmojiPickerElement: sinon_1.default.stub(),
            skinTone: 0,
        };
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        emojiCompletion = new completion_1.EmojiCompletion(mockQuill, options);
        // Stub rendering to avoid missing DOM until we bring in Enzyme
        emojiCompletion.render = sinon_1.default.stub();
    });
    describe('getCurrentLeafTextPartitions', () => {
        it('returns left and right text', () => {
            mockQuill.getSelection.returns({ index: 0, length: 0 });
            const blot = {
                text: ':smile:',
            };
            mockQuill.getLeaf.returns([blot, 2]);
            const [leftLeafText, rightLeafText] = emojiCompletion.getCurrentLeafTextPartitions();
            chai_1.assert.equal(leftLeafText, ':s');
            chai_1.assert.equal(rightLeafText, 'mile:');
        });
    });
    describe('onTextChange', () => {
        let insertEmojiStub;
        beforeEach(function beforeEach() {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            emojiCompletion.results = [{ short_name: 'joy' }];
            emojiCompletion.index = 5;
            insertEmojiStub = sinon_1.default
                .stub(emojiCompletion, 'insertEmoji')
                .callThrough();
        });
        afterEach(function afterEach() {
            insertEmojiStub.restore();
        });
        describe('given an emoji is not starting (no colon)', () => {
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index: 3,
                    length: 0,
                });
                const blot = {
                    text: 'smi',
                };
                mockQuill.getLeaf.returns([blot, 3]);
                emojiCompletion.onTextChange();
            });
            it('does not show results', () => {
                chai_1.assert.equal(emojiCompletion.results.length, 0);
            });
        });
        describe('given a colon in a string (but not an emoji)', () => {
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index: 5,
                    length: 0,
                });
                const blot = {
                    text: '10:30',
                };
                mockQuill.getLeaf.returns([blot, 5]);
                emojiCompletion.onTextChange();
            });
            it('does not show results', () => {
                chai_1.assert.equal(emojiCompletion.results.length, 0);
            });
        });
        describe('given an emoji is starting but does not have 2 characters', () => {
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index: 2,
                    length: 0,
                });
                const blot = {
                    text: ':s',
                };
                mockQuill.getLeaf.returns([blot, 2]);
                emojiCompletion.onTextChange();
            });
            it('does not show results', () => {
                chai_1.assert.equal(emojiCompletion.results.length, 0);
            });
        });
        describe('given an emoji is starting but does not match a short name', () => {
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index: 4,
                    length: 0,
                });
                const blot = {
                    text: ':smy',
                };
                mockQuill.getLeaf.returns([blot, 4]);
                emojiCompletion.onTextChange();
            });
            it('does not show results', () => {
                chai_1.assert.equal(emojiCompletion.results.length, 0);
            });
        });
        describe('given an emoji is starting and matches short names', () => {
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index: 4,
                    length: 0,
                });
                const blot = {
                    text: ':smi',
                };
                mockQuill.getLeaf.returns([blot, 4]);
                emojiCompletion.onTextChange();
            });
            it('stores the results and renders', () => {
                chai_1.assert.equal(emojiCompletion.results.length, 10);
                chai_1.assert.equal(emojiCompletion.render.called, true);
            });
        });
        describe('given an emoji was just completed', () => {
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index: 7,
                    length: 0,
                });
            });
            describe('and given it matches a short name', () => {
                const text = ':smile:';
                beforeEach(function beforeEach() {
                    const blot = {
                        text,
                    };
                    mockQuill.getLeaf.returns([blot, 7]);
                    emojiCompletion.onTextChange();
                });
                it('inserts the emoji at the current cursor position', () => {
                    const [emoji, index, range] = insertEmojiStub.args[0];
                    chai_1.assert.equal(emoji.short_name, 'smile');
                    chai_1.assert.equal(index, 0);
                    chai_1.assert.equal(range, 7);
                });
                it('does not show results', () => {
                    chai_1.assert.equal(emojiCompletion.results.length, 0);
                });
            });
            describe('and given it matches a short name inside a larger string', () => {
                const text = 'have a :smile: nice day';
                beforeEach(function beforeEach() {
                    const blot = {
                        text,
                    };
                    mockQuill.getSelection.returns({
                        index: 13,
                        length: 0,
                    });
                    mockQuill.getLeaf.returns([blot, 13]);
                    emojiCompletion.onTextChange();
                });
                it('inserts the emoji at the current cursor position', () => {
                    const [emoji, index, range] = insertEmojiStub.args[0];
                    chai_1.assert.equal(emoji.short_name, 'smile');
                    chai_1.assert.equal(index, 7);
                    chai_1.assert.equal(range, 7);
                });
                it('does not show results', () => {
                    chai_1.assert.equal(emojiCompletion.results.length, 0);
                });
                it('sets the quill selection to the right cursor position', () => {
                    const [index, range] = mockQuill.setSelection.args[0];
                    chai_1.assert.equal(index, 8);
                    chai_1.assert.equal(range, 0);
                });
            });
            describe('and given it does not match a short name', () => {
                const text = ':smyle:';
                beforeEach(function beforeEach() {
                    const blot = {
                        text,
                    };
                    mockQuill.getLeaf.returns([blot, 7]);
                    emojiCompletion.onTextChange();
                });
                it('does not show results', () => {
                    chai_1.assert.equal(emojiCompletion.results.length, 0);
                });
            });
        });
        describe('given an emoji was just completed from inside the colons', () => {
            const validEmoji = ':smile:';
            const invalidEmoji = ':smyle:';
            const middleCursorIndex = validEmoji.length - 3;
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index: middleCursorIndex,
                    length: 0,
                });
            });
            describe('and given it matches a short name', () => {
                beforeEach(function beforeEach() {
                    const blot = {
                        text: validEmoji,
                    };
                    mockQuill.getLeaf.returns([blot, middleCursorIndex]);
                    emojiCompletion.onTextChange();
                });
                it('inserts the emoji at the current cursor position', () => {
                    const [emoji, index, range] = insertEmojiStub.args[0];
                    chai_1.assert.equal(emoji.short_name, 'smile');
                    chai_1.assert.equal(index, 0);
                    chai_1.assert.equal(range, validEmoji.length);
                });
                it('does not show results', () => {
                    chai_1.assert.equal(emojiCompletion.results.length, 0);
                });
            });
            describe('and given it does not match a short name', () => {
                beforeEach(function beforeEach() {
                    const blot = {
                        text: invalidEmoji,
                    };
                    mockQuill.getLeaf.returns([blot, middleCursorIndex]);
                    emojiCompletion.onTextChange();
                });
                it('does not show results', () => {
                    chai_1.assert.equal(emojiCompletion.results.length, 0);
                });
            });
        });
        describe('given a completeable emoji and colon was just pressed', () => {
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index: 6,
                    length: 0,
                });
            });
            describe('and given it matches a short name', () => {
                const text = ':smile';
                beforeEach(function beforeEach() {
                    const blot = {
                        text,
                    };
                    mockQuill.getLeaf.returns([blot, 6]);
                    emojiCompletion.onTextChange(true);
                });
                it('inserts the emoji at the current cursor position', () => {
                    const [emoji, index, range] = insertEmojiStub.args[0];
                    chai_1.assert.equal(emoji.short_name, 'smile');
                    chai_1.assert.equal(index, 0);
                    chai_1.assert.equal(range, 6);
                });
                it('does not show results', () => {
                    chai_1.assert.equal(emojiCompletion.results.length, 0);
                });
            });
        });
    });
    describe('completeEmoji', () => {
        let insertEmojiStub;
        beforeEach(function beforeEach() {
            emojiCompletion.results = [
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                { short_name: 'smile' },
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                { short_name: 'smile_cat' },
            ];
            emojiCompletion.index = 1;
            insertEmojiStub = sinon_1.default.stub(emojiCompletion, 'insertEmoji');
        });
        describe('given a valid token', () => {
            const text = ':smi';
            const index = text.length;
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index,
                    length: 0,
                });
                const blot = {
                    text,
                };
                mockQuill.getLeaf.returns([blot, index]);
                emojiCompletion.completeEmoji();
            });
            it('inserts the currently selected emoji at the current cursor position', () => {
                const [emoji, insertIndex, range] = insertEmojiStub.args[0];
                chai_1.assert.equal(emoji.short_name, 'smile_cat');
                chai_1.assert.equal(insertIndex, 0);
                chai_1.assert.equal(range, text.length);
            });
        });
        describe('given a valid token is not present', () => {
            const text = 'smi';
            const index = text.length;
            beforeEach(function beforeEach() {
                mockQuill.getSelection.returns({
                    index,
                    length: 0,
                });
                const blot = {
                    text,
                };
                mockQuill.getLeaf.returns([blot, index]);
                emojiCompletion.completeEmoji();
            });
            it('does not insert anything', () => {
                chai_1.assert.equal(insertEmojiStub.called, false);
            });
        });
    });
});
