"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const quill_delta_1 = __importDefault(require("quill-delta"));
const sinon_1 = __importDefault(require("sinon"));
const completion_1 = require("../../../quill/mentions/completion");
const memberRepository_1 = require("../../../quill/memberRepository");
const getDefaultConversation_1 = require("../../../test-both/helpers/getDefaultConversation");
const me = (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
    id: '666777',
    title: 'Fred Savage',
    firstName: 'Fred',
    profileName: 'Fred S.',
    type: 'direct',
    lastUpdated: Date.now(),
    markedUnread: false,
    areWeAdmin: false,
    isMe: true,
});
const members = [
    (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
        id: '555444',
        title: 'Mahershala Ali',
        firstName: 'Mahershala',
        profileName: 'Mahershala A.',
        type: 'direct',
        lastUpdated: Date.now(),
        markedUnread: false,
        areWeAdmin: false,
    }),
    (0, getDefaultConversation_1.getDefaultConversationWithUuid)({
        id: '333222',
        title: 'Shia LaBeouf',
        firstName: 'Shia',
        profileName: 'Shia L.',
        type: 'direct',
        lastUpdated: Date.now(),
        markedUnread: false,
        areWeAdmin: false,
    }),
    me,
];
describe('MentionCompletion', () => {
    let mockQuill;
    let mentionCompletion;
    beforeEach(function beforeEach() {
        const memberRepositoryRef = {
            current: new memberRepository_1.MemberRepository(members),
        };
        const options = {
            i18n: Object.assign(sinon_1.default.stub(), { getLocale: sinon_1.default.stub() }),
            me,
            memberRepositoryRef,
            setMentionPickerElement: sinon_1.default.stub(),
        };
        mockQuill = {
            getContents: sinon_1.default.stub(),
            getLeaf: sinon_1.default.stub(),
            getSelection: sinon_1.default.stub(),
            keyboard: { addBinding: sinon_1.default.stub() },
            on: sinon_1.default.stub(),
            setSelection: sinon_1.default.stub(),
            updateContents: sinon_1.default.stub(),
        };
        mentionCompletion = new completion_1.MentionCompletion(mockQuill, options);
        sinon_1.default.stub(mentionCompletion, 'render');
    });
    describe('onTextChange', () => {
        let possiblyShowMemberResultsStub;
        beforeEach(() => {
            possiblyShowMemberResultsStub = sinon_1.default.stub(mentionCompletion, 'possiblyShowMemberResults');
        });
        describe('given a change that should show members', () => {
            const newContents = new quill_delta_1.default().insert('@a');
            beforeEach(() => {
                var _a;
                (_a = mockQuill.getContents) === null || _a === void 0 ? void 0 : _a.returns(newContents);
                possiblyShowMemberResultsStub.returns(members);
            });
            it('shows member results', () => {
                mentionCompletion.onTextChange();
                chai_1.assert.equal(mentionCompletion.results, members);
                chai_1.assert.equal(mentionCompletion.index, 0);
            });
        });
        describe('given a change that should clear results', () => {
            const newContents = new quill_delta_1.default().insert('foo ');
            let clearResultsStub;
            beforeEach(() => {
                var _a;
                mentionCompletion.results = members;
                (_a = mockQuill.getContents) === null || _a === void 0 ? void 0 : _a.returns(newContents);
                possiblyShowMemberResultsStub.returns([]);
                clearResultsStub = sinon_1.default.stub(mentionCompletion, 'clearResults');
            });
            it('clears member results', () => {
                mentionCompletion.onTextChange();
                chai_1.assert.equal(clearResultsStub.called, true);
            });
        });
    });
    describe('completeMention', () => {
        describe('given a completable mention', () => {
            let insertMentionStub;
            beforeEach(() => {
                var _a, _b;
                mentionCompletion.results = members;
                (_a = mockQuill.getSelection) === null || _a === void 0 ? void 0 : _a.returns({ index: 5 });
                (_b = mockQuill.getLeaf) === null || _b === void 0 ? void 0 : _b.returns([{ text: '@shia' }, 5]);
                insertMentionStub = sinon_1.default.stub(mentionCompletion, 'insertMention');
            });
            it('inserts the currently selected mention at the current cursor position', () => {
                mentionCompletion.completeMention(1);
                const [member, distanceFromCursor, adjustCursorAfterBy, withTrailingSpace,] = insertMentionStub.getCall(0).args;
                chai_1.assert.equal(member, members[1]);
                chai_1.assert.equal(distanceFromCursor, 0);
                chai_1.assert.equal(adjustCursorAfterBy, 5);
                chai_1.assert.equal(withTrailingSpace, true);
            });
            it('can infer the member to complete with', () => {
                mentionCompletion.index = 1;
                mentionCompletion.completeMention();
                const [member, distanceFromCursor, adjustCursorAfterBy, withTrailingSpace,] = insertMentionStub.getCall(0).args;
                chai_1.assert.equal(member, members[1]);
                chai_1.assert.equal(distanceFromCursor, 0);
                chai_1.assert.equal(adjustCursorAfterBy, 5);
                chai_1.assert.equal(withTrailingSpace, true);
            });
            describe('from the middle of a string', () => {
                beforeEach(() => {
                    var _a, _b;
                    (_a = mockQuill.getSelection) === null || _a === void 0 ? void 0 : _a.returns({ index: 9 });
                    (_b = mockQuill.getLeaf) === null || _b === void 0 ? void 0 : _b.returns([{ text: 'foo @shia bar' }, 9]);
                });
                it('inserts correctly', () => {
                    mentionCompletion.completeMention(1);
                    const [member, distanceFromCursor, adjustCursorAfterBy, withTrailingSpace,] = insertMentionStub.getCall(0).args;
                    chai_1.assert.equal(member, members[1]);
                    chai_1.assert.equal(distanceFromCursor, 4);
                    chai_1.assert.equal(adjustCursorAfterBy, 5);
                    chai_1.assert.equal(withTrailingSpace, true);
                });
            });
            describe('given a completable mention starting with a capital letter', () => {
                const text = '@Sh';
                const index = text.length;
                beforeEach(function beforeEach() {
                    var _a, _b;
                    (_a = mockQuill.getSelection) === null || _a === void 0 ? void 0 : _a.returns({ index });
                    const blot = {
                        text,
                    };
                    (_b = mockQuill.getLeaf) === null || _b === void 0 ? void 0 : _b.returns([blot, index]);
                    mentionCompletion.completeMention(1);
                });
                it('inserts the currently selected mention at the current cursor position', () => {
                    const [member, distanceFromCursor, adjustCursorAfterBy, withTrailingSpace,] = insertMentionStub.getCall(0).args;
                    chai_1.assert.equal(member, members[1]);
                    chai_1.assert.equal(distanceFromCursor, 0);
                    chai_1.assert.equal(adjustCursorAfterBy, 3);
                    chai_1.assert.equal(withTrailingSpace, true);
                });
            });
        });
    });
});
