"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.constantTimeEqual = exports.getRandomBytes = exports.decrypt = exports.encrypt = exports.hash = exports.sign = exports.decryptProfileName = exports.encryptProfileItemWithPadding = exports.decryptProfile = exports.encryptProfile = exports.encryptAttachment = exports.decryptAttachment = exports.trimForDisplay = exports.splitUuids = exports.bytesToUuid = exports.uuidToBytes = exports.encryptCdsDiscoveryRequest = exports.getBytes = exports.getFirstBytes = exports.intsToByteHighAndLow = exports.highBitsToInt = exports.getZeroes = exports.getRandomValue = exports.sha256 = exports.decryptAesGcm = exports.encryptAesGcm = exports.decryptAesCtr = exports.encryptAesCtr = exports.decryptAes256CbcPkcsPadding = exports.encryptAes256CbcPkcsPadding = exports.verifyHmacSha256 = exports.hmacSha256 = exports.decryptSymmetric = exports.encryptSymmetric = exports.verifyAccessKey = exports.getAccessKeyVerifier = exports.deriveAccessKey = exports.deriveStorageItemKey = exports.deriveStorageManifestKey = exports.decryptDeviceName = exports.encryptDeviceName = exports.computeHash = exports.deriveMasterKeyFromGroupV1 = exports.deriveSecrets = exports.deriveStickerPackKey = exports.generateRegistrationId = exports.PaddedLengths = exports.CipherType = exports.HashType = void 0;
const buffer_1 = require("buffer");
const p_props_1 = __importDefault(require("p-props"));
const lodash_1 = require("lodash");
const long_1 = __importDefault(require("long"));
const signal_client_1 = require("@signalapp/signal-client");
const Bytes = __importStar(require("./Bytes"));
const Curve_1 = require("./Curve");
const log = __importStar(require("./logging/log"));
const Crypto_1 = require("./types/Crypto");
Object.defineProperty(exports, "HashType", { enumerable: true, get: function () { return Crypto_1.HashType; } });
Object.defineProperty(exports, "CipherType", { enumerable: true, get: function () { return Crypto_1.CipherType; } });
const errors_1 = require("./types/errors");
const UUID_1 = require("./types/UUID");
const PROFILE_IV_LENGTH = 12; // bytes
const PROFILE_KEY_LENGTH = 32; // bytes
// bytes
exports.PaddedLengths = {
    Name: [53, 257],
    About: [128, 254, 512],
    AboutEmoji: [32],
    PaymentAddress: [554],
};
// Generate a number between zero and 16383
function generateRegistrationId() {
    const id = new Uint16Array(getRandomBytes(2))[0];
    // eslint-disable-next-line no-bitwise
    return id & 0x3fff;
}
exports.generateRegistrationId = generateRegistrationId;
function deriveStickerPackKey(packKey) {
    const salt = getZeroes(32);
    const info = Bytes.fromString('Sticker Pack');
    const [part1, part2] = deriveSecrets(packKey, salt, info);
    return Bytes.concatenate([part1, part2]);
}
exports.deriveStickerPackKey = deriveStickerPackKey;
function deriveSecrets(input, salt, info) {
    const hkdf = signal_client_1.HKDF.new(3);
    const output = hkdf.deriveSecrets(3 * 32, buffer_1.Buffer.from(input), buffer_1.Buffer.from(info), buffer_1.Buffer.from(salt));
    return [output.slice(0, 32), output.slice(32, 64), output.slice(64, 96)];
}
exports.deriveSecrets = deriveSecrets;
function deriveMasterKeyFromGroupV1(groupV1Id) {
    const salt = getZeroes(32);
    const info = Bytes.fromString('GV2 Migration');
    const [part1] = deriveSecrets(groupV1Id, salt, info);
    return part1;
}
exports.deriveMasterKeyFromGroupV1 = deriveMasterKeyFromGroupV1;
function computeHash(data) {
    return Bytes.toBase64(hash(Crypto_1.HashType.size512, data));
}
exports.computeHash = computeHash;
function encryptDeviceName(deviceName, identityPublic) {
    const plaintext = Bytes.fromString(deviceName);
    const ephemeralKeyPair = (0, Curve_1.generateKeyPair)();
    const masterSecret = (0, Curve_1.calculateAgreement)(identityPublic, ephemeralKeyPair.privKey);
    const key1 = hmacSha256(masterSecret, Bytes.fromString('auth'));
    const syntheticIv = getFirstBytes(hmacSha256(key1, plaintext), 16);
    const key2 = hmacSha256(masterSecret, Bytes.fromString('cipher'));
    const cipherKey = hmacSha256(key2, syntheticIv);
    const counter = getZeroes(16);
    const ciphertext = encryptAesCtr(cipherKey, plaintext, counter);
    return {
        ephemeralPublic: ephemeralKeyPair.pubKey,
        syntheticIv,
        ciphertext,
    };
}
exports.encryptDeviceName = encryptDeviceName;
function decryptDeviceName({ ephemeralPublic, syntheticIv, ciphertext }, identityPrivate) {
    const masterSecret = (0, Curve_1.calculateAgreement)(ephemeralPublic, identityPrivate);
    const key2 = hmacSha256(masterSecret, Bytes.fromString('cipher'));
    const cipherKey = hmacSha256(key2, syntheticIv);
    const counter = getZeroes(16);
    const plaintext = decryptAesCtr(cipherKey, ciphertext, counter);
    const key1 = hmacSha256(masterSecret, Bytes.fromString('auth'));
    const ourSyntheticIv = getFirstBytes(hmacSha256(key1, plaintext), 16);
    if (!constantTimeEqual(ourSyntheticIv, syntheticIv)) {
        throw new Error('decryptDeviceName: synthetic IV did not match');
    }
    return Bytes.toString(plaintext);
}
exports.decryptDeviceName = decryptDeviceName;
function deriveStorageManifestKey(storageServiceKey, version) {
    return hmacSha256(storageServiceKey, Bytes.fromString(`Manifest_${version}`));
}
exports.deriveStorageManifestKey = deriveStorageManifestKey;
function deriveStorageItemKey(storageServiceKey, itemID) {
    return hmacSha256(storageServiceKey, Bytes.fromString(`Item_${itemID}`));
}
exports.deriveStorageItemKey = deriveStorageItemKey;
function deriveAccessKey(profileKey) {
    const iv = getZeroes(12);
    const plaintext = getZeroes(16);
    const accessKey = encryptAesGcm(profileKey, iv, plaintext);
    return getFirstBytes(accessKey, 16);
}
exports.deriveAccessKey = deriveAccessKey;
function getAccessKeyVerifier(accessKey) {
    const plaintext = getZeroes(32);
    return hmacSha256(accessKey, plaintext);
}
exports.getAccessKeyVerifier = getAccessKeyVerifier;
function verifyAccessKey(accessKey, theirVerifier) {
    const ourVerifier = getAccessKeyVerifier(accessKey);
    if (constantTimeEqual(ourVerifier, theirVerifier)) {
        return true;
    }
    return false;
}
exports.verifyAccessKey = verifyAccessKey;
const IV_LENGTH = 16;
const MAC_LENGTH = 16;
const NONCE_LENGTH = 16;
function encryptSymmetric(key, plaintext) {
    const iv = getZeroes(IV_LENGTH);
    const nonce = getRandomBytes(NONCE_LENGTH);
    const cipherKey = hmacSha256(key, nonce);
    const macKey = hmacSha256(key, cipherKey);
    const ciphertext = encryptAes256CbcPkcsPadding(cipherKey, plaintext, iv);
    const mac = getFirstBytes(hmacSha256(macKey, ciphertext), MAC_LENGTH);
    return Bytes.concatenate([nonce, ciphertext, mac]);
}
exports.encryptSymmetric = encryptSymmetric;
function decryptSymmetric(key, data) {
    const iv = getZeroes(IV_LENGTH);
    const nonce = getFirstBytes(data, NONCE_LENGTH);
    const ciphertext = getBytes(data, NONCE_LENGTH, data.byteLength - NONCE_LENGTH - MAC_LENGTH);
    const theirMac = getBytes(data, data.byteLength - MAC_LENGTH, MAC_LENGTH);
    const cipherKey = hmacSha256(key, nonce);
    const macKey = hmacSha256(key, cipherKey);
    const ourMac = getFirstBytes(hmacSha256(macKey, ciphertext), MAC_LENGTH);
    if (!constantTimeEqual(theirMac, ourMac)) {
        throw new Error('decryptSymmetric: Failed to decrypt; MAC verification failed');
    }
    return decryptAes256CbcPkcsPadding(cipherKey, ciphertext, iv);
}
exports.decryptSymmetric = decryptSymmetric;
// Encryption
function hmacSha256(key, plaintext) {
    return sign(key, plaintext);
}
exports.hmacSha256 = hmacSha256;
// We use part of the constantTimeEqual algorithm from below here, but we allow ourMac
//   to be longer than the passed-in length. This allows easy comparisons against
//   arbitrary MAC lengths.
function verifyHmacSha256(plaintext, key, theirMac, length) {
    const ourMac = hmacSha256(key, plaintext);
    if (theirMac.byteLength !== length || ourMac.byteLength < length) {
        throw new Error('Bad MAC length');
    }
    let result = 0;
    for (let i = 0; i < theirMac.byteLength; i += 1) {
        // eslint-disable-next-line no-bitwise
        result |= ourMac[i] ^ theirMac[i];
    }
    if (result !== 0) {
        throw new Error('Bad MAC');
    }
}
exports.verifyHmacSha256 = verifyHmacSha256;
function encryptAes256CbcPkcsPadding(key, plaintext, iv) {
    return encrypt(Crypto_1.CipherType.AES256CBC, {
        key,
        plaintext,
        iv,
    });
}
exports.encryptAes256CbcPkcsPadding = encryptAes256CbcPkcsPadding;
function decryptAes256CbcPkcsPadding(key, ciphertext, iv) {
    return decrypt(Crypto_1.CipherType.AES256CBC, {
        key,
        ciphertext,
        iv,
    });
}
exports.decryptAes256CbcPkcsPadding = decryptAes256CbcPkcsPadding;
function encryptAesCtr(key, plaintext, counter) {
    return encrypt(Crypto_1.CipherType.AES256CTR, {
        key,
        plaintext,
        iv: counter,
    });
}
exports.encryptAesCtr = encryptAesCtr;
function decryptAesCtr(key, ciphertext, counter) {
    return decrypt(Crypto_1.CipherType.AES256CTR, {
        key,
        ciphertext,
        iv: counter,
    });
}
exports.decryptAesCtr = decryptAesCtr;
function encryptAesGcm(key, iv, plaintext, aad) {
    return encrypt(Crypto_1.CipherType.AES256GCM, {
        key,
        plaintext,
        iv,
        aad,
    });
}
exports.encryptAesGcm = encryptAesGcm;
function decryptAesGcm(key, iv, ciphertext) {
    return decrypt(Crypto_1.CipherType.AES256GCM, {
        key,
        ciphertext,
        iv,
    });
}
exports.decryptAesGcm = decryptAesGcm;
// Hashing
function sha256(data) {
    return hash(Crypto_1.HashType.size256, data);
}
exports.sha256 = sha256;
// Utility
function getRandomValue(low, high) {
    const diff = high - low;
    const bytes = getRandomBytes(1);
    // Because high and low are inclusive
    const mod = diff + 1;
    return (bytes[0] % mod) + low;
}
exports.getRandomValue = getRandomValue;
function getZeroes(n) {
    return new Uint8Array(n);
}
exports.getZeroes = getZeroes;
function highBitsToInt(byte) {
    // eslint-disable-next-line no-bitwise
    return (byte & 0xff) >> 4;
}
exports.highBitsToInt = highBitsToInt;
function intsToByteHighAndLow(highValue, lowValue) {
    // eslint-disable-next-line no-bitwise
    return ((highValue << 4) | lowValue) & 0xff;
}
exports.intsToByteHighAndLow = intsToByteHighAndLow;
function getFirstBytes(data, n) {
    return data.subarray(0, n);
}
exports.getFirstBytes = getFirstBytes;
function getBytes(data, start, n) {
    return data.subarray(start, start + n);
}
exports.getBytes = getBytes;
function _getMacAndData(ciphertext) {
    const dataLength = ciphertext.byteLength - MAC_LENGTH;
    const data = getBytes(ciphertext, 0, dataLength);
    const mac = getBytes(ciphertext, dataLength, MAC_LENGTH);
    return { data, mac };
}
async function encryptCdsDiscoveryRequest(attestations, phoneNumbers) {
    const nonce = getRandomBytes(32);
    const numbersArray = buffer_1.Buffer.concat(phoneNumbers.map(number => {
        // Long.fromString handles numbers with or without a leading '+'
        return new Uint8Array(long_1.default.fromString(number).toBytesBE());
    }));
    // We've written to the array, so offset === byteLength; we need to reset it. Then we'll
    //   have access to everything in the array when we generate an Uint8Array from it.
    const queryDataPlaintext = Bytes.concatenate([nonce, numbersArray]);
    const queryDataKey = getRandomBytes(32);
    const commitment = sha256(queryDataPlaintext);
    const iv = getRandomBytes(12);
    const queryDataCiphertext = encryptAesGcm(queryDataKey, iv, queryDataPlaintext);
    const { data: queryDataCiphertextData, mac: queryDataCiphertextMac } = _getMacAndData(queryDataCiphertext);
    const envelopes = await (0, p_props_1.default)(attestations, async ({ clientKey, requestId }) => {
        const envelopeIv = getRandomBytes(12);
        const ciphertext = encryptAesGcm(clientKey, envelopeIv, queryDataKey, requestId);
        const { data, mac } = _getMacAndData(ciphertext);
        return {
            requestId: Bytes.toBase64(requestId),
            data: Bytes.toBase64(data),
            iv: Bytes.toBase64(envelopeIv),
            mac: Bytes.toBase64(mac),
        };
    });
    return {
        addressCount: phoneNumbers.length,
        commitment: Bytes.toBase64(commitment),
        data: Bytes.toBase64(queryDataCiphertextData),
        iv: Bytes.toBase64(iv),
        mac: Bytes.toBase64(queryDataCiphertextMac),
        envelopes,
    };
}
exports.encryptCdsDiscoveryRequest = encryptCdsDiscoveryRequest;
function uuidToBytes(uuid) {
    if (uuid.length !== 36) {
        log.warn('uuidToBytes: received a string of invalid length. ' +
            'Returning an empty Uint8Array');
        return new Uint8Array(0);
    }
    return Uint8Array.from((0, lodash_1.chunk)(uuid.replace(/-/g, ''), 2).map(pair => parseInt(pair.join(''), 16)));
}
exports.uuidToBytes = uuidToBytes;
function bytesToUuid(bytes) {
    if (bytes.byteLength !== 16) {
        log.warn('bytesToUuid: received an Uint8Array of invalid length. ' +
            'Returning undefined');
        return undefined;
    }
    const uuids = splitUuids(bytes);
    if (uuids.length === 1) {
        return uuids[0] || undefined;
    }
    return undefined;
}
exports.bytesToUuid = bytesToUuid;
function splitUuids(buffer) {
    const uuids = [];
    for (let i = 0; i < buffer.byteLength; i += 16) {
        const bytes = getBytes(buffer, i, 16);
        const hex = Bytes.toHex(bytes);
        const chunks = [
            hex.substring(0, 8),
            hex.substring(8, 12),
            hex.substring(12, 16),
            hex.substring(16, 20),
            hex.substring(20),
        ];
        const uuid = chunks.join('-');
        if (uuid !== '00000000-0000-0000-0000-000000000000') {
            uuids.push(UUID_1.UUID.cast(uuid));
        }
        else {
            uuids.push(null);
        }
    }
    return uuids;
}
exports.splitUuids = splitUuids;
function trimForDisplay(padded) {
    let paddingEnd = 0;
    for (paddingEnd; paddingEnd < padded.length; paddingEnd += 1) {
        if (padded[paddingEnd] === 0x00) {
            break;
        }
    }
    return padded.slice(0, paddingEnd);
}
exports.trimForDisplay = trimForDisplay;
function verifyDigest(data, theirDigest) {
    const ourDigest = sha256(data);
    let result = 0;
    for (let i = 0; i < theirDigest.byteLength; i += 1) {
        // eslint-disable-next-line no-bitwise
        result |= ourDigest[i] ^ theirDigest[i];
    }
    if (result !== 0) {
        throw new Error('Bad digest');
    }
}
function decryptAttachment(encryptedBin, keys, theirDigest) {
    if (keys.byteLength !== 64) {
        throw new Error('Got invalid length attachment keys');
    }
    if (encryptedBin.byteLength < 16 + 32) {
        throw new Error('Got invalid length attachment');
    }
    const aesKey = keys.slice(0, 32);
    const macKey = keys.slice(32, 64);
    const iv = encryptedBin.slice(0, 16);
    const ciphertext = encryptedBin.slice(16, encryptedBin.byteLength - 32);
    const ivAndCiphertext = encryptedBin.slice(0, encryptedBin.byteLength - 32);
    const mac = encryptedBin.slice(encryptedBin.byteLength - 32, encryptedBin.byteLength);
    verifyHmacSha256(ivAndCiphertext, macKey, mac, 32);
    if (theirDigest) {
        verifyDigest(encryptedBin, theirDigest);
    }
    return decryptAes256CbcPkcsPadding(aesKey, ciphertext, iv);
}
exports.decryptAttachment = decryptAttachment;
function encryptAttachment(plaintext, keys, iv) {
    if (!(plaintext instanceof Uint8Array)) {
        throw new TypeError(`\`plaintext\` must be an \`Uint8Array\`; got: ${typeof plaintext}`);
    }
    if (keys.byteLength !== 64) {
        throw new Error('Got invalid length attachment keys');
    }
    if (iv.byteLength !== 16) {
        throw new Error('Got invalid length attachment iv');
    }
    const aesKey = keys.slice(0, 32);
    const macKey = keys.slice(32, 64);
    const ciphertext = encryptAes256CbcPkcsPadding(aesKey, plaintext, iv);
    const ivAndCiphertext = Bytes.concatenate([iv, ciphertext]);
    const mac = hmacSha256(macKey, ivAndCiphertext);
    const encryptedBin = Bytes.concatenate([ivAndCiphertext, mac]);
    const digest = sha256(encryptedBin);
    return {
        ciphertext: encryptedBin,
        digest,
    };
}
exports.encryptAttachment = encryptAttachment;
function encryptProfile(data, key) {
    const iv = getRandomBytes(PROFILE_IV_LENGTH);
    if (key.byteLength !== PROFILE_KEY_LENGTH) {
        throw new Error('Got invalid length profile key');
    }
    if (iv.byteLength !== PROFILE_IV_LENGTH) {
        throw new Error('Got invalid length profile iv');
    }
    const ciphertext = encryptAesGcm(key, iv, data);
    return Bytes.concatenate([iv, ciphertext]);
}
exports.encryptProfile = encryptProfile;
function decryptProfile(data, key) {
    if (data.byteLength < 12 + 16 + 1) {
        throw new Error(`Got too short input: ${data.byteLength}`);
    }
    const iv = data.slice(0, PROFILE_IV_LENGTH);
    const ciphertext = data.slice(PROFILE_IV_LENGTH, data.byteLength);
    if (key.byteLength !== PROFILE_KEY_LENGTH) {
        throw new Error('Got invalid length profile key');
    }
    if (iv.byteLength !== PROFILE_IV_LENGTH) {
        throw new Error('Got invalid length profile iv');
    }
    try {
        return decryptAesGcm(key, iv, ciphertext);
    }
    catch (_) {
        throw new errors_1.ProfileDecryptError('Failed to decrypt profile data. ' +
            'Most likely the profile key has changed.');
    }
}
exports.decryptProfile = decryptProfile;
function encryptProfileItemWithPadding(item, profileKey, paddedLengths) {
    const paddedLength = paddedLengths.find((length) => item.byteLength <= length);
    if (!paddedLength) {
        throw new Error('Oversized value');
    }
    const padded = new Uint8Array(paddedLength);
    padded.set(new Uint8Array(item));
    return encryptProfile(padded, profileKey);
}
exports.encryptProfileItemWithPadding = encryptProfileItemWithPadding;
function decryptProfileName(encryptedProfileName, key) {
    const data = Bytes.fromBase64(encryptedProfileName);
    const padded = decryptProfile(data, key);
    // Given name is the start of the string to the first null character
    let givenEnd;
    for (givenEnd = 0; givenEnd < padded.length; givenEnd += 1) {
        if (padded[givenEnd] === 0x00) {
            break;
        }
    }
    // Family name is the next chunk of non-null characters after that first null
    let familyEnd;
    for (familyEnd = givenEnd + 1; familyEnd < padded.length; familyEnd += 1) {
        if (padded[familyEnd] === 0x00) {
            break;
        }
    }
    const foundFamilyName = familyEnd > givenEnd + 1;
    return {
        given: padded.slice(0, givenEnd),
        family: foundFamilyName ? padded.slice(givenEnd + 1, familyEnd) : null,
    };
}
exports.decryptProfileName = decryptProfileName;
//
// SignalContext APIs
//
const { crypto } = window.SignalContext;
function sign(key, data) {
    return crypto.sign(key, data);
}
exports.sign = sign;
function hash(type, data) {
    return crypto.hash(type, data);
}
exports.hash = hash;
function encrypt(...args) {
    return crypto.encrypt(...args);
}
exports.encrypt = encrypt;
function decrypt(...args) {
    return crypto.decrypt(...args);
}
exports.decrypt = decrypt;
function getRandomBytes(size) {
    return crypto.getRandomBytes(size);
}
exports.getRandomBytes = getRandomBytes;
function constantTimeEqual(left, right) {
    return crypto.constantTimeEqual(left, right);
}
exports.constantTimeEqual = constantTimeEqual;
