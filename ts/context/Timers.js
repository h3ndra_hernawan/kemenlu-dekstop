"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.Timers = void 0;
const timers_1 = require("timers");
class Timers {
    constructor() {
        this.counter = 0;
        this.timers = new Map();
    }
    setTimeout(callback, delay) {
        let id;
        do {
            id = this.counter;
            // eslint-disable-next-line no-bitwise
            this.counter = (this.counter + 1) >>> 0;
        } while (this.timers.has(id));
        const timer = (0, timers_1.setTimeout)(() => {
            this.timers.delete(id);
            callback();
        }, delay);
        this.timers.set(id, timer);
        return { id };
    }
    clearTimeout({ id }) {
        const timer = this.timers.get(id);
        if (timer === undefined) {
            return;
        }
        this.timers.delete(id);
        return (0, timers_1.clearTimeout)(timer);
    }
}
exports.Timers = Timers;
