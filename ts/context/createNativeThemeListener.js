"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
/* eslint-disable no-restricted-syntax */
Object.defineProperty(exports, "__esModule", { value: true });
exports.createNativeThemeListener = void 0;
function createNativeThemeListener(ipc, holder) {
    const subscribers = new Array();
    let theme = ipc.sendSync('native-theme:init');
    let systemTheme;
    function update() {
        const nextSystemTheme = theme.shouldUseDarkColors ? 'dark' : 'light';
        // eslint-disable-next-line no-param-reassign
        holder.systemTheme = nextSystemTheme;
        return nextSystemTheme;
    }
    function subscribe(fn) {
        subscribers.push(fn);
    }
    ipc.on('native-theme:changed', (_event, change) => {
        theme = change;
        systemTheme = update();
        for (const fn of subscribers) {
            fn(change);
        }
    });
    systemTheme = update();
    return {
        getSystemTheme: () => systemTheme,
        subscribe,
        update,
    };
}
exports.createNativeThemeListener = createNativeThemeListener;
