"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.Bytes = void 0;
const buffer_1 = require("buffer");
class Bytes {
    fromBase64(value) {
        return buffer_1.Buffer.from(value, 'base64');
    }
    fromHex(value) {
        return buffer_1.Buffer.from(value, 'hex');
    }
    // TODO(indutny): deprecate it
    fromBinary(value) {
        return buffer_1.Buffer.from(value, 'binary');
    }
    fromString(value) {
        return buffer_1.Buffer.from(value);
    }
    toBase64(data) {
        return buffer_1.Buffer.from(data).toString('base64');
    }
    toHex(data) {
        return buffer_1.Buffer.from(data).toString('hex');
    }
    // TODO(indutny): deprecate it
    toBinary(data) {
        return buffer_1.Buffer.from(data).toString('binary');
    }
    toString(data) {
        return buffer_1.Buffer.from(data).toString();
    }
    byteLength(value) {
        return buffer_1.Buffer.byteLength(value);
    }
    concatenate(list) {
        return buffer_1.Buffer.concat(list);
    }
    isEmpty(data) {
        if (!data) {
            return true;
        }
        return data.length === 0;
    }
    isNotEmpty(data) {
        return !this.isEmpty(data);
    }
    areEqual(a, b) {
        if (!a || !b) {
            return !a && !b;
        }
        return buffer_1.Buffer.compare(a, b) === 0;
    }
}
exports.Bytes = Bytes;
