"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Crypto = void 0;
const buffer_1 = require("buffer");
const crypto_1 = __importDefault(require("crypto"));
const assert_1 = require("../util/assert");
const Crypto_1 = require("../types/Crypto");
const AUTH_TAG_SIZE = 16;
class Crypto {
    sign(key, data) {
        return crypto_1.default
            .createHmac('sha256', buffer_1.Buffer.from(key))
            .update(buffer_1.Buffer.from(data))
            .digest();
    }
    hash(type, data) {
        return crypto_1.default.createHash(type).update(buffer_1.Buffer.from(data)).digest();
    }
    encrypt(cipherType, { key, plaintext, iv, aad, }) {
        if (cipherType === Crypto_1.CipherType.AES256GCM) {
            const gcm = crypto_1.default.createCipheriv(cipherType, buffer_1.Buffer.from(key), buffer_1.Buffer.from(iv));
            if (aad) {
                gcm.setAAD(aad);
            }
            const first = gcm.update(buffer_1.Buffer.from(plaintext));
            const last = gcm.final();
            const tag = gcm.getAuthTag();
            (0, assert_1.strictAssert)(tag.length === AUTH_TAG_SIZE, 'Invalid auth tag size');
            return buffer_1.Buffer.concat([first, last, tag]);
        }
        (0, assert_1.strictAssert)(aad === undefined, `AAD is not supported for: ${cipherType}`);
        const cipher = crypto_1.default.createCipheriv(cipherType, buffer_1.Buffer.from(key), buffer_1.Buffer.from(iv));
        return buffer_1.Buffer.concat([
            cipher.update(buffer_1.Buffer.from(plaintext)),
            cipher.final(),
        ]);
    }
    decrypt(cipherType, { key, ciphertext, iv, aad, }) {
        let decipher;
        let input = buffer_1.Buffer.from(ciphertext);
        if (cipherType === Crypto_1.CipherType.AES256GCM) {
            const gcm = crypto_1.default.createDecipheriv(cipherType, buffer_1.Buffer.from(key), buffer_1.Buffer.from(iv));
            if (input.length < AUTH_TAG_SIZE) {
                throw new Error('Invalid GCM ciphertext');
            }
            const tag = input.slice(input.length - AUTH_TAG_SIZE);
            input = input.slice(0, input.length - AUTH_TAG_SIZE);
            gcm.setAuthTag(tag);
            if (aad) {
                gcm.setAAD(aad);
            }
            decipher = gcm;
        }
        else {
            (0, assert_1.strictAssert)(aad === undefined, `AAD is not supported for: ${cipherType}`);
            decipher = crypto_1.default.createDecipheriv(cipherType, buffer_1.Buffer.from(key), buffer_1.Buffer.from(iv));
        }
        return buffer_1.Buffer.concat([decipher.update(input), decipher.final()]);
    }
    getRandomBytes(size) {
        return crypto_1.default.randomBytes(size);
    }
    constantTimeEqual(left, right) {
        return crypto_1.default.timingSafeEqual(buffer_1.Buffer.from(left), buffer_1.Buffer.from(right));
    }
}
exports.Crypto = Crypto;
