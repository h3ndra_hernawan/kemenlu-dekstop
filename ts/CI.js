"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CI = void 0;
const electron_1 = require("electron");
const explodePromise_1 = require("./util/explodePromise");
const durations_1 = require("./util/durations");
const log = __importStar(require("./logging/log"));
class CI {
    constructor(deviceName) {
        this.deviceName = deviceName;
        this.eventListeners = new Map();
        this.completedEvents = new Map();
        electron_1.ipcRenderer.on('ci:event', (_, event, data) => {
            this.handleEvent(event, data);
        });
    }
    async waitForEvent(event, timeout = 60 * durations_1.SECOND) {
        const pendingCompleted = this.completedEvents.get(event) || [];
        const pending = pendingCompleted.shift();
        if (pending) {
            log.info(`CI: resolving pending result for ${event}`, pending);
            if (pendingCompleted.length === 0) {
                this.completedEvents.delete(event);
            }
            return pending;
        }
        log.info(`CI: waiting for event ${event}`);
        const { resolve, reject, promise } = (0, explodePromise_1.explodePromise)();
        const timer = setTimeout(() => {
            reject(new Error('Timed out'));
        }, timeout);
        let list = this.eventListeners.get(event);
        if (!list) {
            list = [];
            this.eventListeners.set(event, list);
        }
        list.push((value) => {
            clearTimeout(timer);
            resolve(value);
        });
        return promise;
    }
    setProvisioningURL(url) {
        this.handleEvent('provisioning-url', url);
    }
    handleEvent(event, data) {
        const list = this.eventListeners.get(event) || [];
        const resolve = list.shift();
        if (resolve) {
            if (list.length === 0) {
                this.eventListeners.delete(event);
            }
            log.info(`CI: got event ${event} with data`, data);
            resolve(data);
            return;
        }
        log.info(`CI: postponing event ${event}`);
        let resultList = this.completedEvents.get(event);
        if (!resultList) {
            resultList = [];
            this.completedEvents.set(event, resultList);
        }
        resultList.push(data);
    }
}
exports.CI = CI;
