"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getValue = exports.isEnabled = exports.maybeRefreshRemoteConfig = exports.refreshRemoteConfig = exports.onChange = exports.initRemoteConfig = void 0;
const lodash_1 = require("lodash");
const log = __importStar(require("./logging/log"));
let config = {};
const listeners = {};
async function initRemoteConfig(server) {
    config = window.storage.get('remoteConfig') || {};
    await (0, exports.maybeRefreshRemoteConfig)(server);
}
exports.initRemoteConfig = initRemoteConfig;
function onChange(key, fn) {
    const keyListeners = (0, lodash_1.get)(listeners, key, []);
    keyListeners.push(fn);
    listeners[key] = keyListeners;
    return () => {
        listeners[key] = listeners[key].filter(l => l !== fn);
    };
}
exports.onChange = onChange;
const refreshRemoteConfig = async (server) => {
    const now = Date.now();
    const newConfig = await server.getConfig();
    // Process new configuration in light of the old configuration
    // The old configuration is not set as the initial value in reduce because
    // flags may have been deleted
    const oldConfig = config;
    config = newConfig.reduce((acc, { name, enabled, value }) => {
        const previouslyEnabled = (0, lodash_1.get)(oldConfig, [name, 'enabled'], false);
        const previousValue = (0, lodash_1.get)(oldConfig, [name, 'value'], undefined);
        // If a flag was previously not enabled and is now enabled,
        // record the time it was enabled
        const enabledAt = previouslyEnabled && enabled ? now : (0, lodash_1.get)(oldConfig, [name, 'enabledAt']);
        const configValue = {
            name: name,
            enabled,
            enabledAt,
            value,
        };
        const hasChanged = previouslyEnabled !== enabled || previousValue !== configValue.value;
        // If enablement changes at all, notify listeners
        const currentListeners = listeners[name] || [];
        if (hasChanged) {
            log.info(`Remote Config: Flag ${name} has changed`);
            currentListeners.forEach(listener => {
                listener(configValue);
            });
        }
        // Return new configuration object
        return Object.assign(Object.assign({}, acc), { [name]: configValue });
    }, {});
    window.storage.put('remoteConfig', config);
};
exports.refreshRemoteConfig = refreshRemoteConfig;
exports.maybeRefreshRemoteConfig = (0, lodash_1.throttle)(exports.refreshRemoteConfig, 
// Only fetch remote configuration if the last fetch was more than two hours ago
2 * 60 * 60 * 1000, { trailing: false });
function isEnabled(name) {
    return (0, lodash_1.get)(config, [name, 'enabled'], false);
}
exports.isEnabled = isEnabled;
function getValue(name) {
    return (0, lodash_1.get)(config, [name, 'value'], undefined);
}
exports.getValue = getValue;
