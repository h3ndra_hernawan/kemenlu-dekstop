"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ViewSyncs = void 0;
/* eslint-disable max-classes-per-file */
const backbone_1 = require("backbone");
const MessageReadStatus_1 = require("../messages/MessageReadStatus");
const MessageUpdater_1 = require("../services/MessageUpdater");
const message_1 = require("../state/selectors/message");
const notifications_1 = require("../services/notifications");
const log = __importStar(require("../logging/log"));
class ViewSyncModel extends backbone_1.Model {
}
let singleton;
class ViewSyncs extends backbone_1.Collection {
    static getSingleton() {
        if (!singleton) {
            singleton = new ViewSyncs();
        }
        return singleton;
    }
    forMessage(message) {
        const senderId = window.ConversationController.ensureContactIds({
            e164: message.get('source'),
            uuid: message.get('sourceUuid'),
        });
        const syncs = this.filter(item => {
            return (item.get('senderId') === senderId &&
                item.get('timestamp') === message.get('sent_at'));
        });
        if (syncs.length) {
            log.info(`Found ${syncs.length} early view sync(s) for message ${message.get('sent_at')}`);
            this.remove(syncs);
        }
        return syncs;
    }
    async onSync(sync) {
        try {
            const messages = await window.Signal.Data.getMessagesBySentAt(sync.get('timestamp'), {
                MessageCollection: window.Whisper.MessageCollection,
            });
            const found = messages.find(item => {
                const senderId = window.ConversationController.ensureContactIds({
                    e164: item.get('source'),
                    uuid: item.get('sourceUuid'),
                });
                return (0, message_1.isIncoming)(item.attributes) && senderId === sync.get('senderId');
            });
            if (!found) {
                log.info('Nothing found for view sync', sync.get('senderId'), sync.get('senderE164'), sync.get('senderUuid'), sync.get('timestamp'));
                return;
            }
            notifications_1.notificationService.removeBy({ messageId: found.id });
            const message = window.MessageController.register(found.id, found);
            if (message.get('readStatus') !== MessageReadStatus_1.ReadStatus.Viewed) {
                message.set((0, MessageUpdater_1.markViewed)(message.attributes, sync.get('viewedAt')));
            }
            this.remove(sync);
        }
        catch (error) {
            log.error('ViewSyncs.onSync error:', error && error.stack ? error.stack : error);
        }
    }
}
exports.ViewSyncs = ViewSyncs;
