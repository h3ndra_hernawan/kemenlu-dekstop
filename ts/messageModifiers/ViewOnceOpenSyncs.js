"use strict";
// Copyright 2019-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ViewOnceOpenSyncs = void 0;
/* eslint-disable max-classes-per-file */
const backbone_1 = require("backbone");
const log = __importStar(require("../logging/log"));
class ViewOnceOpenSyncModel extends backbone_1.Model {
}
let singleton;
class ViewOnceOpenSyncs extends backbone_1.Collection {
    static getSingleton() {
        if (!singleton) {
            singleton = new ViewOnceOpenSyncs();
        }
        return singleton;
    }
    forMessage(message) {
        const syncBySourceUuid = this.find(item => {
            return (item.get('sourceUuid') === message.get('sourceUuid') &&
                item.get('timestamp') === message.get('sent_at'));
        });
        if (syncBySourceUuid) {
            log.info('Found early view once open sync for message');
            this.remove(syncBySourceUuid);
            return syncBySourceUuid;
        }
        const syncBySource = this.find(item => {
            return (item.get('source') === message.get('source') &&
                item.get('timestamp') === message.get('sent_at'));
        });
        if (syncBySource) {
            log.info('Found early view once open sync for message');
            this.remove(syncBySource);
            return syncBySource;
        }
        return null;
    }
    async onSync(sync) {
        try {
            const messages = await window.Signal.Data.getMessagesBySentAt(sync.get('timestamp'), {
                MessageCollection: window.Whisper.MessageCollection,
            });
            const found = messages.find(item => {
                const itemSourceUuid = item.get('sourceUuid');
                const syncSourceUuid = sync.get('sourceUuid');
                const itemSource = item.get('source');
                const syncSource = sync.get('source');
                return Boolean((itemSourceUuid &&
                    syncSourceUuid &&
                    itemSourceUuid === syncSourceUuid) ||
                    (itemSource && syncSource && itemSource === syncSource));
            });
            const syncSource = sync.get('source');
            const syncSourceUuid = sync.get('sourceUuid');
            const syncTimestamp = sync.get('timestamp');
            const wasMessageFound = Boolean(found);
            log.info('Receive view once open sync:', {
                syncSource,
                syncSourceUuid,
                syncTimestamp,
                wasMessageFound,
            });
            if (!found) {
                return;
            }
            const message = window.MessageController.register(found.id, found);
            await message.markViewOnceMessageViewed({ fromSync: true });
            this.remove(sync);
        }
        catch (error) {
            log.error('ViewOnceOpenSyncs.onSync error:', error && error.stack ? error.stack : error);
        }
    }
}
exports.ViewOnceOpenSyncs = ViewOnceOpenSyncs;
