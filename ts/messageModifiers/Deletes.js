"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Deletes = exports.DeleteModel = void 0;
/* eslint-disable max-classes-per-file */
const backbone_1 = require("backbone");
const log = __importStar(require("../logging/log"));
class DeleteModel extends backbone_1.Model {
}
exports.DeleteModel = DeleteModel;
let singleton;
class Deletes extends backbone_1.Collection {
    static getSingleton() {
        if (!singleton) {
            singleton = new Deletes();
        }
        return singleton;
    }
    forMessage(message) {
        const matchingDeletes = this.filter(item => {
            return (item.get('targetSentTimestamp') === message.get('sent_at') &&
                item.get('fromId') === message.getContactId());
        });
        if (matchingDeletes.length > 0) {
            log.info('Found early DOE for message');
            this.remove(matchingDeletes);
            return matchingDeletes;
        }
        return [];
    }
    async onDelete(del) {
        try {
            // The conversation the deleted message was in; we have to find it in the database
            //   to to figure that out.
            const targetConversation = await window.ConversationController.getConversationForTargetMessage(del.get('fromId'), del.get('targetSentTimestamp'));
            if (!targetConversation) {
                log.info('No target conversation for DOE', del.get('fromId'), del.get('targetSentTimestamp'));
                return;
            }
            // Do not await, since this can deadlock the queue
            targetConversation.queueJob('Deletes.onDelete', async () => {
                log.info('Handling DOE for', del.get('targetSentTimestamp'));
                const messages = await window.Signal.Data.getMessagesBySentAt(del.get('targetSentTimestamp'), {
                    MessageCollection: window.Whisper.MessageCollection,
                });
                const targetMessage = messages.find(m => del.get('fromId') === m.getContactId());
                if (!targetMessage) {
                    log.info('No message for DOE', del.get('fromId'), del.get('targetSentTimestamp'));
                    return;
                }
                const message = window.MessageController.register(targetMessage.id, targetMessage);
                await window.Signal.Util.deleteForEveryone(message, del);
                this.remove(del);
            });
        }
        catch (error) {
            log.error('Deletes.onDelete error:', error && error.stack ? error.stack : error);
        }
    }
}
exports.Deletes = Deletes;
