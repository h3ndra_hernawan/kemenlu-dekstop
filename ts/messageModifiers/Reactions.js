"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Reactions = exports.ReactionModel = void 0;
/* eslint-disable max-classes-per-file */
const backbone_1 = require("backbone");
const message_1 = require("../state/selectors/message");
const log = __importStar(require("../logging/log"));
class ReactionModel extends backbone_1.Model {
}
exports.ReactionModel = ReactionModel;
let singleton;
class Reactions extends backbone_1.Collection {
    static getSingleton() {
        if (!singleton) {
            singleton = new Reactions();
        }
        return singleton;
    }
    forMessage(message) {
        if ((0, message_1.isOutgoing)(message.attributes)) {
            const outgoingReactions = this.filter(item => item.get('targetTimestamp') === message.get('sent_at'));
            if (outgoingReactions.length > 0) {
                log.info('Found early reaction for outgoing message');
                this.remove(outgoingReactions);
                return outgoingReactions;
            }
        }
        const senderId = message.getContactId();
        const sentAt = message.get('sent_at');
        const reactionsBySource = this.filter(re => {
            const targetSenderId = window.ConversationController.ensureContactIds({
                uuid: re.get('targetAuthorUuid'),
            });
            const targetTimestamp = re.get('targetTimestamp');
            return targetSenderId === senderId && targetTimestamp === sentAt;
        });
        if (reactionsBySource.length > 0) {
            log.info('Found early reaction for message');
            this.remove(reactionsBySource);
            return reactionsBySource;
        }
        return [];
    }
    async onReaction(reaction) {
        try {
            // The conversation the target message was in; we have to find it in the database
            //   to to figure that out.
            const targetConversationId = window.ConversationController.ensureContactIds({
                uuid: reaction.get('targetAuthorUuid'),
            });
            if (!targetConversationId) {
                throw new Error('onReaction: No conversationId returned from ensureContactIds!');
            }
            const targetConversation = await window.ConversationController.getConversationForTargetMessage(targetConversationId, reaction.get('targetTimestamp'));
            if (!targetConversation) {
                log.info('No target conversation for reaction', reaction.get('targetAuthorUuid'), reaction.get('targetTimestamp'));
                return undefined;
            }
            // awaiting is safe since `onReaction` is never called from inside the queue
            await targetConversation.queueJob('Reactions.onReaction', async () => {
                log.info('Handling reaction for', reaction.get('targetTimestamp'));
                const messages = await window.Signal.Data.getMessagesBySentAt(reaction.get('targetTimestamp'), {
                    MessageCollection: window.Whisper.MessageCollection,
                });
                // Message is fetched inside the conversation queue so we have the
                // most recent data
                const targetMessage = messages.find(m => {
                    const contact = m.getContact();
                    if (!contact) {
                        return false;
                    }
                    const mcid = contact.get('id');
                    const recid = window.ConversationController.ensureContactIds({
                        uuid: reaction.get('targetAuthorUuid'),
                    });
                    return mcid === recid;
                });
                if (!targetMessage) {
                    log.info('No message for reaction', reaction.get('targetAuthorUuid'), reaction.get('targetTimestamp'));
                    // Since we haven't received the message for which we are removing a
                    // reaction, we can just remove those pending reactions
                    if (reaction.get('remove')) {
                        this.remove(reaction);
                        const oldReaction = this.where({
                            targetAuthorUuid: reaction.get('targetAuthorUuid'),
                            targetTimestamp: reaction.get('targetTimestamp'),
                            emoji: reaction.get('emoji'),
                        });
                        oldReaction.forEach(r => this.remove(r));
                    }
                    return;
                }
                const message = window.MessageController.register(targetMessage.id, targetMessage);
                await message.handleReaction(reaction);
                this.remove(reaction);
            });
        }
        catch (error) {
            log.error('Reactions.onReaction error:', error && error.stack ? error.stack : error);
        }
    }
}
exports.Reactions = Reactions;
