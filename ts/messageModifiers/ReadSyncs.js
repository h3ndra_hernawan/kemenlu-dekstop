"use strict";
// Copyright 2017-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReadSyncs = void 0;
/* eslint-disable max-classes-per-file */
const backbone_1 = require("backbone");
const message_1 = require("../state/selectors/message");
const isMessageUnread_1 = require("../util/isMessageUnread");
const notifications_1 = require("../services/notifications");
const log = __importStar(require("../logging/log"));
class ReadSyncModel extends backbone_1.Model {
}
let singleton;
async function maybeItIsAReactionReadSync(sync) {
    const readReaction = await window.Signal.Data.markReactionAsRead(sync.get('senderUuid'), Number(sync.get('timestamp')));
    if (!readReaction) {
        log.info('Nothing found for read sync', sync.get('senderId'), sync.get('sender'), sync.get('senderUuid'), sync.get('timestamp'));
        return;
    }
    notifications_1.notificationService.removeBy({
        conversationId: readReaction.conversationId,
        emoji: readReaction.emoji,
        targetAuthorUuid: readReaction.targetAuthorUuid,
        targetTimestamp: readReaction.targetTimestamp,
    });
}
class ReadSyncs extends backbone_1.Collection {
    static getSingleton() {
        if (!singleton) {
            singleton = new ReadSyncs();
        }
        return singleton;
    }
    forMessage(message) {
        const senderId = window.ConversationController.ensureContactIds({
            e164: message.get('source'),
            uuid: message.get('sourceUuid'),
        });
        const sync = this.find(item => {
            return (item.get('senderId') === senderId &&
                item.get('timestamp') === message.get('sent_at'));
        });
        if (sync) {
            log.info(`Found early read sync for message ${sync.get('timestamp')}`);
            this.remove(sync);
            return sync;
        }
        return null;
    }
    async onSync(sync) {
        try {
            const messages = await window.Signal.Data.getMessagesBySentAt(sync.get('timestamp'), {
                MessageCollection: window.Whisper.MessageCollection,
            });
            const found = messages.find(item => {
                const senderId = window.ConversationController.ensureContactIds({
                    e164: item.get('source'),
                    uuid: item.get('sourceUuid'),
                });
                return (0, message_1.isIncoming)(item.attributes) && senderId === sync.get('senderId');
            });
            if (!found) {
                await maybeItIsAReactionReadSync(sync);
                return;
            }
            notifications_1.notificationService.removeBy({ messageId: found.id });
            const message = window.MessageController.register(found.id, found);
            const readAt = Math.min(sync.get('readAt'), Date.now());
            // If message is unread, we mark it read. Otherwise, we update the expiration
            //   timer to the time specified by the read sync if it's earlier than
            //   the previous read time.
            if ((0, isMessageUnread_1.isMessageUnread)(message.attributes)) {
                // TODO DESKTOP-1509: use MessageUpdater.markRead once this is TS
                message.markRead(readAt, { skipSave: true });
                const updateConversation = () => {
                    var _a;
                    // onReadMessage may result in messages older than this one being
                    //   marked read. We want those messages to have the same expire timer
                    //   start time as this one, so we pass the readAt value through.
                    (_a = message.getConversation()) === null || _a === void 0 ? void 0 : _a.onReadMessage(message, readAt);
                };
                if (window.startupProcessingQueue) {
                    const conversation = message.getConversation();
                    if (conversation) {
                        window.startupProcessingQueue.add(conversation.get('id'), message.get('sent_at'), updateConversation);
                    }
                }
                else {
                    updateConversation();
                }
            }
            else {
                const now = Date.now();
                const existingTimestamp = message.get('expirationStartTimestamp');
                const expirationStartTimestamp = Math.min(now, Math.min(existingTimestamp || now, readAt || now));
                message.set({ expirationStartTimestamp });
            }
            window.Signal.Util.queueUpdateMessage(message.attributes);
            this.remove(sync);
        }
        catch (error) {
            log.error('ReadSyncs.onSync error:', error && error.stack ? error.stack : error);
        }
    }
}
exports.ReadSyncs = ReadSyncs;
