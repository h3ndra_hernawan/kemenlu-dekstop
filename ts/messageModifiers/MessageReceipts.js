"use strict";
// Copyright 2016-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageReceipts = exports.MessageReceiptType = void 0;
/* eslint-disable max-classes-per-file */
const lodash_1 = require("lodash");
const backbone_1 = require("backbone");
const message_1 = require("../state/selectors/message");
const whatTypeOfConversation_1 = require("../util/whatTypeOfConversation");
const getOwn_1 = require("../util/getOwn");
const missingCaseError_1 = require("../util/missingCaseError");
const waitBatcher_1 = require("../util/waitBatcher");
const MessageSendState_1 = require("../messages/MessageSendState");
const Client_1 = __importDefault(require("../sql/Client"));
const log = __importStar(require("../logging/log"));
const { deleteSentProtoRecipient } = Client_1.default;
var MessageReceiptType;
(function (MessageReceiptType) {
    MessageReceiptType["Delivery"] = "Delivery";
    MessageReceiptType["Read"] = "Read";
    MessageReceiptType["View"] = "View";
})(MessageReceiptType = exports.MessageReceiptType || (exports.MessageReceiptType = {}));
class MessageReceiptModel extends backbone_1.Model {
}
let singleton;
const deleteSentProtoBatcher = (0, waitBatcher_1.createWaitBatcher)({
    name: 'deleteSentProtoBatcher',
    wait: 250,
    maxSize: 30,
    async processBatch(items) {
        log.info(`MessageReceipts: Batching ${items.length} sent proto recipients deletes`);
        await deleteSentProtoRecipient(items);
    },
});
async function getTargetMessage(sourceId, sourceUuid, messages) {
    if (messages.length === 0) {
        return null;
    }
    const message = messages.find(item => (0, message_1.isOutgoing)(item.attributes) && sourceId === item.get('conversationId'));
    if (message) {
        return window.MessageController.register(message.id, message);
    }
    const groups = await window.Signal.Data.getAllGroupsInvolvingUuid(sourceUuid, {
        ConversationCollection: window.Whisper.ConversationCollection,
    });
    const ids = groups.pluck('id');
    ids.push(sourceId);
    const target = messages.find(item => (0, message_1.isOutgoing)(item.attributes) && ids.includes(item.get('conversationId')));
    if (!target) {
        return null;
    }
    return window.MessageController.register(target.id, target);
}
const wasDeliveredWithSealedSender = (conversationId, message) => (message.get('unidentifiedDeliveries') || []).some(identifier => window.ConversationController.getConversationId(identifier) ===
    conversationId);
class MessageReceipts extends backbone_1.Collection {
    static getSingleton() {
        if (!singleton) {
            singleton = new MessageReceipts();
        }
        return singleton;
    }
    forMessage(conversation, message) {
        if (!(0, message_1.isOutgoing)(message.attributes)) {
            return [];
        }
        let ids;
        if ((0, whatTypeOfConversation_1.isDirectConversation)(conversation.attributes)) {
            ids = [conversation.id];
        }
        else {
            ids = conversation.getMemberIds();
        }
        const receipts = this.filter(receipt => receipt.get('messageSentAt') === message.get('sent_at') &&
            ids.includes(receipt.get('sourceConversationId')));
        if (receipts.length) {
            log.info('Found early receipts for message');
            this.remove(receipts);
        }
        return receipts;
    }
    async onReceipt(receipt) {
        var _a;
        const type = receipt.get('type');
        const messageSentAt = receipt.get('messageSentAt');
        const receiptTimestamp = receipt.get('receiptTimestamp');
        const sourceConversationId = receipt.get('sourceConversationId');
        const sourceUuid = receipt.get('sourceUuid');
        try {
            const messages = await window.Signal.Data.getMessagesBySentAt(messageSentAt, {
                MessageCollection: window.Whisper.MessageCollection,
            });
            const message = await getTargetMessage(sourceConversationId, sourceUuid, messages);
            if (!message) {
                log.info('No message for receipt', type, sourceConversationId, messageSentAt);
                return;
            }
            const oldSendStateByConversationId = message.get('sendStateByConversationId') || {};
            const oldSendState = (_a = (0, getOwn_1.getOwn)(oldSendStateByConversationId, sourceConversationId)) !== null && _a !== void 0 ? _a : { status: MessageSendState_1.SendStatus.Sent, updatedAt: undefined };
            let sendActionType;
            switch (type) {
                case MessageReceiptType.Delivery:
                    sendActionType = MessageSendState_1.SendActionType.GotDeliveryReceipt;
                    break;
                case MessageReceiptType.Read:
                    sendActionType = MessageSendState_1.SendActionType.GotReadReceipt;
                    break;
                case MessageReceiptType.View:
                    sendActionType = MessageSendState_1.SendActionType.GotViewedReceipt;
                    break;
                default:
                    throw (0, missingCaseError_1.missingCaseError)(type);
            }
            const newSendState = (0, MessageSendState_1.sendStateReducer)(oldSendState, {
                type: sendActionType,
                updatedAt: receiptTimestamp,
            });
            // The send state may not change. For example, this can happen if we get a read
            //   receipt before a delivery receipt.
            if (!(0, lodash_1.isEqual)(oldSendState, newSendState)) {
                message.set('sendStateByConversationId', Object.assign(Object.assign({}, oldSendStateByConversationId), { [sourceConversationId]: newSendState }));
                window.Signal.Util.queueUpdateMessage(message.attributes);
                // notify frontend listeners
                const conversation = window.ConversationController.get(message.get('conversationId'));
                const updateLeftPane = conversation
                    ? conversation.debouncedUpdateLastMessage
                    : undefined;
                if (updateLeftPane) {
                    updateLeftPane();
                }
            }
            if ((type === MessageReceiptType.Delivery &&
                wasDeliveredWithSealedSender(sourceConversationId, message)) ||
                type === MessageReceiptType.Read) {
                const recipient = window.ConversationController.get(sourceConversationId);
                const recipientUuid = recipient === null || recipient === void 0 ? void 0 : recipient.get('uuid');
                const deviceId = receipt.get('sourceDevice');
                if (recipientUuid && deviceId) {
                    await deleteSentProtoBatcher.add({
                        timestamp: messageSentAt,
                        recipientUuid,
                        deviceId,
                    });
                }
                else {
                    log.warn(`MessageReceipts.onReceipt: Missing uuid or deviceId for deliveredTo ${sourceConversationId}`);
                }
            }
            this.remove(receipt);
        }
        catch (error) {
            log.error('MessageReceipts.onReceipt error:', error && error.stack ? error.stack : error);
        }
    }
}
exports.MessageReceipts = MessageReceipts;
