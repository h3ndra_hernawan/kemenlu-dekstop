"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageRequests = void 0;
/* eslint-disable max-classes-per-file */
const backbone_1 = require("backbone");
const log = __importStar(require("../logging/log"));
class MessageRequestModel extends backbone_1.Model {
}
let singleton;
class MessageRequests extends backbone_1.Collection {
    static getSingleton() {
        if (!singleton) {
            singleton = new MessageRequests();
        }
        return singleton;
    }
    forConversation(conversation) {
        if (conversation.get('e164')) {
            const syncByE164 = this.findWhere({
                threadE164: conversation.get('e164'),
            });
            if (syncByE164) {
                log.info(`Found early message request response for E164 ${conversation.idForLogging()}`);
                this.remove(syncByE164);
                return syncByE164;
            }
        }
        if (conversation.get('uuid')) {
            const syncByUuid = this.findWhere({
                threadUuid: conversation.get('uuid'),
            });
            if (syncByUuid) {
                log.info(`Found early message request response for UUID ${conversation.idForLogging()}`);
                this.remove(syncByUuid);
                return syncByUuid;
            }
        }
        // V1 Group
        if (conversation.get('groupId')) {
            const syncByGroupId = this.findWhere({
                groupId: conversation.get('groupId'),
            });
            if (syncByGroupId) {
                log.info(`Found early message request response for group v1 ID ${conversation.idForLogging()}`);
                this.remove(syncByGroupId);
                return syncByGroupId;
            }
        }
        // V2 group
        if (conversation.get('groupId')) {
            const syncByGroupId = this.findWhere({
                groupV2Id: conversation.get('groupId'),
            });
            if (syncByGroupId) {
                log.info(`Found early message request response for group v2 ID ${conversation.idForLogging()}`);
                this.remove(syncByGroupId);
                return syncByGroupId;
            }
        }
        return null;
    }
    async onResponse(sync) {
        try {
            const threadE164 = sync.get('threadE164');
            const threadUuid = sync.get('threadUuid');
            const groupId = sync.get('groupId');
            const groupV2Id = sync.get('groupV2Id');
            let conversation;
            // We multiplex between GV1/GV2 groups here, but we don't kick off migrations
            if (groupV2Id) {
                conversation = window.ConversationController.get(groupV2Id);
            }
            if (!conversation && groupId) {
                conversation = window.ConversationController.get(groupId);
            }
            if (!conversation && (threadE164 || threadUuid)) {
                conversation = window.ConversationController.get(window.ConversationController.ensureContactIds({
                    e164: threadE164,
                    uuid: threadUuid,
                }));
            }
            if (!conversation) {
                log.warn(`Received message request response for unknown conversation: groupv2(${groupV2Id}) group(${groupId}) ${threadUuid} ${threadE164}`);
                return;
            }
            conversation.applyMessageRequestResponse(sync.get('type'), {
                fromSync: true,
            });
            this.remove(sync);
        }
        catch (error) {
            log.error('MessageRequests.onResponse error:', error && error.stack ? error.stack : error);
        }
    }
}
exports.MessageRequests = MessageRequests;
