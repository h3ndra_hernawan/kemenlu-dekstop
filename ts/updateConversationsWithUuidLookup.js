"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateConversationsWithUuidLookup = void 0;
const assert_1 = require("./util/assert");
const getOwn_1 = require("./util/getOwn");
const isNotNil_1 = require("./util/isNotNil");
async function updateConversationsWithUuidLookup({ conversationController, conversations, messaging, }) {
    const e164s = conversations
        .map(conversation => conversation.get('e164'))
        .filter(isNotNil_1.isNotNil);
    if (!e164s.length) {
        return;
    }
    const serverLookup = await messaging.getUuidsForE164s(e164s);
    conversations.forEach(conversation => {
        const e164 = conversation.get('e164');
        if (!e164) {
            return;
        }
        let finalConversation;
        const uuidFromServer = (0, getOwn_1.getOwn)(serverLookup, e164);
        if (uuidFromServer) {
            const finalConversationId = conversationController.ensureContactIds({
                e164,
                uuid: uuidFromServer,
                highTrust: true,
            });
            const maybeFinalConversation = conversationController.get(finalConversationId);
            (0, assert_1.assert)(maybeFinalConversation, 'updateConversationsWithUuidLookup: expected a conversation to be found or created');
            finalConversation = maybeFinalConversation;
        }
        else {
            finalConversation = conversation;
        }
        if (!finalConversation.get('e164') || !finalConversation.get('uuid')) {
            finalConversation.setUnregistered();
        }
    });
}
exports.updateConversationsWithUuidLookup = updateConversationsWithUuidLookup;
