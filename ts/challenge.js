"use strict";
// Copyright 2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChallengeHandler = exports.getChallengeURL = exports.RetryMode = void 0;
const assert_1 = require("./util/assert");
const isNotNil_1 = require("./util/isNotNil");
const timestamp_1 = require("./util/timestamp");
const parseRetryAfter_1 = require("./util/parseRetryAfter");
const environment_1 = require("./environment");
const Errors_1 = require("./textsecure/Errors");
const log = __importStar(require("./logging/log"));
var RetryMode;
(function (RetryMode) {
    RetryMode["Retry"] = "Retry";
    RetryMode["NoImmediateRetry"] = "NoImmediateRetry";
})(RetryMode = exports.RetryMode || (exports.RetryMode = {}));
const DEFAULT_EXPIRE_AFTER = 24 * 3600 * 1000; // one day
const MAX_RETRIES = 5;
const CAPTCHA_URL = 'https://signalcaptchas.org/challenge/generate.html';
const CAPTCHA_STAGING_URL = 'https://signalcaptchas.org/staging/challenge/generate.html';
function shouldRetrySend(message) {
    const error = message.getLastChallengeError();
    if (!error || error.retryAfter <= Date.now()) {
        return true;
    }
    return false;
}
function getChallengeURL() {
    if ((0, environment_1.getEnvironment)() === environment_1.Environment.Staging) {
        return CAPTCHA_STAGING_URL;
    }
    return CAPTCHA_URL;
}
exports.getChallengeURL = getChallengeURL;
// Note that even though this is a class - only one instance of
// `ChallengeHandler` should be in memory at the same time because they could
// overwrite each others storage data.
class ChallengeHandler {
    constructor(options) {
        this.options = options;
        this.isLoaded = false;
        this.seq = 0;
        this.isOnline = false;
        this.responseHandlers = new Map();
        this.trackedMessages = new Map();
        this.retryTimers = new Map();
        this.pendingRetries = new Set();
        this.retryCountById = new Map();
    }
    async load() {
        if (this.isLoaded) {
            return;
        }
        this.isLoaded = true;
        const stored = this.options.storage.get('challenge:retry-message-ids') || [];
        log.info(`challenge: loading ${stored.length} messages`);
        const entityMap = new Map();
        for (const entity of stored) {
            entityMap.set(entity.messageId, entity);
        }
        const retryIds = new Set(stored.map(({ messageId }) => messageId));
        const maybeMessages = await Promise.all(Array.from(retryIds).map(async (messageId) => this.options.getMessageById(messageId)));
        const messages = maybeMessages.filter(isNotNil_1.isNotNil);
        log.info(`challenge: loaded ${messages.length} messages`);
        await Promise.all(messages.map(async (message) => {
            const entity = entityMap.get(message.id);
            if (!entity) {
                log.error('challenge: unexpected missing entity ' +
                    `for ${message.idForLogging()}`);
                return;
            }
            const expireAfter = this.options.expireAfter || DEFAULT_EXPIRE_AFTER;
            if ((0, timestamp_1.isOlderThan)(entity.createdAt, expireAfter)) {
                log.info(`challenge: expired entity for ${message.idForLogging()}`);
                return;
            }
            // The initialization order is following:
            //
            // 1. `.load()` when the `window.storage` is ready
            // 2. `.onOnline()` when we connected to the server
            //
            // Wait for `.onOnline()` to trigger the retries instead of triggering
            // them here immediately (if the message is ready to be retried).
            await this.register(message, RetryMode.NoImmediateRetry, entity);
        }));
    }
    async onOffline() {
        this.isOnline = false;
        log.info('challenge: offline');
    }
    async onOnline() {
        this.isOnline = true;
        const pending = Array.from(this.pendingRetries.values());
        this.pendingRetries.clear();
        log.info(`challenge: online, retrying ${pending.length} messages`);
        // Retry messages that matured while we were offline
        await Promise.all(pending.map(message => this.retryOne(message)));
        await this.retrySend();
    }
    async register(message, retry = RetryMode.Retry, entity) {
        if (this.isRegistered(message)) {
            log.info(`challenge: message already registered ${message.idForLogging()}`);
            return;
        }
        this.trackedMessages.set(message.id, {
            message,
            createdAt: entity ? entity.createdAt : Date.now(),
        });
        await this.persist();
        // Message is already retryable - initiate new send
        if (retry === RetryMode.Retry && shouldRetrySend(message)) {
            log.info(`challenge: sending message immediately ${message.idForLogging()}`);
            await this.retryOne(message);
            return;
        }
        const error = message.getLastChallengeError();
        if (!error) {
            log.error('Unexpected message without challenge error');
            return;
        }
        const waitTime = Math.max(0, error.retryAfter - Date.now());
        const oldTimer = this.retryTimers.get(message.id);
        if (oldTimer) {
            clearTimeout(oldTimer);
        }
        this.retryTimers.set(message.id, setTimeout(() => {
            this.retryTimers.delete(message.id);
            this.retryOne(message);
        }, waitTime));
        log.info(`challenge: tracking ${message.idForLogging()} ` +
            `with waitTime=${waitTime}`);
        if (!error.data.options || !error.data.options.includes('recaptcha')) {
            log.error(`challenge: unexpected options ${JSON.stringify(error.data.options)}`);
        }
        if (!error.data.token) {
            log.error(`challenge: no token in challenge error ${JSON.stringify(error.data)}`);
        }
        else if (message.isNormalBubble()) {
            // Display challenge dialog only for core messages
            // (e.g. text, attachment, embedded contact, or sticker)
            //
            // Note: not waiting on this call intentionally since it waits for
            // challenge to be fully completed.
            this.solve(error.data.token);
        }
        else {
            log.info(`challenge: not a bubble message ${message.idForLogging()}`);
        }
    }
    onResponse(response) {
        const handler = this.responseHandlers.get(response.seq);
        if (!handler) {
            return;
        }
        this.responseHandlers.delete(response.seq);
        handler.resolve(response.data);
    }
    async unregister(message) {
        log.info(`challenge: unregistered ${message.idForLogging()}`);
        this.trackedMessages.delete(message.id);
        this.pendingRetries.delete(message);
        const timer = this.retryTimers.get(message.id);
        this.retryTimers.delete(message.id);
        if (timer) {
            clearTimeout(timer);
        }
        await this.persist();
    }
    async persist() {
        (0, assert_1.assert)(this.isLoaded, 'ChallengeHandler has to be loaded before persisting new data');
        await this.options.storage.put('challenge:retry-message-ids', Array.from(this.trackedMessages.entries()).map(([messageId, { createdAt }]) => {
            return { messageId, createdAt };
        }));
    }
    isRegistered(message) {
        return this.trackedMessages.has(message.id);
    }
    async retrySend(force = false) {
        log.info(`challenge: retrySend force=${force}`);
        const retries = Array.from(this.trackedMessages.values())
            .map(({ message }) => message)
            // Sort messages in `sent_at` order
            .sort((a, b) => a.get('sent_at') - b.get('sent_at'))
            .filter(message => force || shouldRetrySend(message))
            .map(message => this.retryOne(message));
        await Promise.all(retries);
    }
    async retryOne(message) {
        // Send is already pending
        if (!this.isRegistered(message)) {
            return;
        }
        // We are not online
        if (!this.isOnline) {
            this.pendingRetries.add(message);
            return;
        }
        const retryCount = this.retryCountById.get(message.id) || 0;
        log.info(`challenge: retrying sending ${message.idForLogging()}, ` +
            `retry count: ${retryCount}`);
        if (retryCount === MAX_RETRIES) {
            log.info(`challenge: dropping message ${message.idForLogging()}, ` +
                'too many failed retries');
            // Keep the message registered so that we'll retry sending it on app
            // restart.
            return;
        }
        await this.unregister(message);
        let sent = false;
        const onSent = () => {
            sent = true;
        };
        message.on('sent', onSent);
        try {
            await message.retrySend();
        }
        catch (error) {
            log.error(`challenge: failed to send ${message.idForLogging()} due to ` +
                `error: ${error && error.stack}`);
        }
        finally {
            message.off('sent', onSent);
        }
        if (sent) {
            log.info(`challenge: message ${message.idForLogging()} sent`);
            this.retryCountById.delete(message.id);
            if (this.trackedMessages.size === 0) {
                this.options.setChallengeStatus('idle');
            }
        }
        else {
            log.info(`challenge: message ${message.idForLogging()} not sent`);
            this.retryCountById.set(message.id, retryCount + 1);
            await this.register(message, RetryMode.NoImmediateRetry);
        }
    }
    async solve(token) {
        const request = { seq: this.seq };
        this.seq += 1;
        this.options.setChallengeStatus('required');
        this.options.requestChallenge(request);
        this.challengeToken = token || '';
        const response = await new Promise((resolve, reject) => {
            this.responseHandlers.set(request.seq, { token, resolve, reject });
        });
        // Another `.solve()` has completed earlier than us
        if (this.challengeToken === undefined) {
            return;
        }
        const lastToken = this.challengeToken;
        this.challengeToken = undefined;
        this.options.setChallengeStatus('pending');
        log.info('challenge: sending challenge to server');
        try {
            await this.sendChallengeResponse({
                type: 'recaptcha',
                token: lastToken,
                captcha: response.captcha,
            });
        }
        catch (error) {
            log.error(`challenge: challenge failure, error: ${error && error.stack}`);
            this.options.setChallengeStatus('required');
            return;
        }
        log.info('challenge: challenge success. force sending');
        this.options.setChallengeStatus('idle');
        this.retrySend(true);
    }
    async sendChallengeResponse(data) {
        try {
            await this.options.sendChallengeResponse(data);
        }
        catch (error) {
            if (!(error instanceof Errors_1.HTTPError) ||
                error.code !== 413 ||
                !error.responseHeaders) {
                this.options.onChallengeFailed();
                throw error;
            }
            const retryAfter = (0, parseRetryAfter_1.parseRetryAfter)(error.responseHeaders['retry-after']);
            log.info(`challenge: retry after ${retryAfter}ms`);
            this.options.onChallengeFailed(retryAfter);
            return;
        }
        this.options.onChallengeSolved();
    }
}
exports.ChallengeHandler = ChallengeHandler;
