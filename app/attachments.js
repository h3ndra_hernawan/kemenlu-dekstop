"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAllDraftAttachments = exports.deleteAllBadges = exports.deleteAllStickers = exports.deleteAll = exports.clearTempPath = exports.getBuiltInImages = exports.getAllDraftAttachments = exports.getAllStickers = exports.getAllAttachments = void 0;
const path_1 = require("path");
const fast_glob_1 = __importDefault(require("fast-glob"));
const glob_1 = __importDefault(require("glob"));
const pify_1 = __importDefault(require("pify"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const lodash_1 = require("lodash");
const normalize_path_1 = __importDefault(require("normalize-path"));
const attachments_1 = require("../ts/util/attachments");
__exportStar(require("../ts/util/attachments"), exports);
const getAllAttachments = async (userDataPath) => {
    const dir = (0, attachments_1.getPath)(userDataPath);
    const pattern = (0, normalize_path_1.default)((0, path_1.join)(dir, '**', '*'));
    const files = await (0, fast_glob_1.default)(pattern, { onlyFiles: true });
    return (0, lodash_1.map)(files, file => (0, path_1.relative)(dir, file));
};
exports.getAllAttachments = getAllAttachments;
const getAllBadgeImageFiles = async (userDataPath) => {
    const dir = (0, attachments_1.getBadgesPath)(userDataPath);
    const pattern = (0, normalize_path_1.default)((0, path_1.join)(dir, '**', '*'));
    const files = await (0, fast_glob_1.default)(pattern, { onlyFiles: true });
    return (0, lodash_1.map)(files, file => (0, path_1.relative)(dir, file));
};
const getAllStickers = async (userDataPath) => {
    const dir = (0, attachments_1.getStickersPath)(userDataPath);
    const pattern = (0, normalize_path_1.default)((0, path_1.join)(dir, '**', '*'));
    const files = await (0, fast_glob_1.default)(pattern, { onlyFiles: true });
    return (0, lodash_1.map)(files, file => (0, path_1.relative)(dir, file));
};
exports.getAllStickers = getAllStickers;
const getAllDraftAttachments = async (userDataPath) => {
    const dir = (0, attachments_1.getDraftPath)(userDataPath);
    const pattern = (0, normalize_path_1.default)((0, path_1.join)(dir, '**', '*'));
    const files = await (0, fast_glob_1.default)(pattern, { onlyFiles: true });
    return (0, lodash_1.map)(files, file => (0, path_1.relative)(dir, file));
};
exports.getAllDraftAttachments = getAllDraftAttachments;
const getBuiltInImages = async () => {
    const dir = (0, path_1.join)(__dirname, '../images');
    const pattern = (0, path_1.join)(dir, '**', '*.svg');
    // Note: we cannot use fast-glob here because, inside of .asar files, readdir will not
    //   honor the withFileTypes flag: https://github.com/electron/electron/issues/19074
    const files = await (0, pify_1.default)(glob_1.default)(pattern, { nodir: true });
    return (0, lodash_1.map)(files, file => (0, path_1.relative)(dir, file));
};
exports.getBuiltInImages = getBuiltInImages;
const clearTempPath = (userDataPath) => {
    const tempPath = (0, attachments_1.getTempPath)(userDataPath);
    return fs_extra_1.default.emptyDir(tempPath);
};
exports.clearTempPath = clearTempPath;
const deleteAll = async ({ userDataPath, attachments, }) => {
    const deleteFromDisk = (0, attachments_1.createDeleter)((0, attachments_1.getPath)(userDataPath));
    for (let index = 0, max = attachments.length; index < max; index += 1) {
        const file = attachments[index];
        // eslint-disable-next-line no-await-in-loop
        await deleteFromDisk(file);
    }
    console.log(`deleteAll: deleted ${attachments.length} files`);
};
exports.deleteAll = deleteAll;
const deleteAllStickers = async ({ userDataPath, stickers, }) => {
    const deleteFromDisk = (0, attachments_1.createDeleter)((0, attachments_1.getStickersPath)(userDataPath));
    for (let index = 0, max = stickers.length; index < max; index += 1) {
        const file = stickers[index];
        // eslint-disable-next-line no-await-in-loop
        await deleteFromDisk(file);
    }
    console.log(`deleteAllStickers: deleted ${stickers.length} files`);
};
exports.deleteAllStickers = deleteAllStickers;
const deleteAllBadges = async ({ userDataPath, pathsToKeep, }) => {
    const deleteFromDisk = (0, attachments_1.createDeleter)((0, attachments_1.getBadgesPath)(userDataPath));
    let filesDeleted = 0;
    for (const file of await getAllBadgeImageFiles(userDataPath)) {
        if (!pathsToKeep.has(file)) {
            // eslint-disable-next-line no-await-in-loop
            await deleteFromDisk(file);
            filesDeleted += 1;
        }
    }
    console.log(`deleteAllBadges: deleted ${filesDeleted} files`);
};
exports.deleteAllBadges = deleteAllBadges;
const deleteAllDraftAttachments = async ({ userDataPath, attachments, }) => {
    const deleteFromDisk = (0, attachments_1.createDeleter)((0, attachments_1.getDraftPath)(userDataPath));
    for (let index = 0, max = attachments.length; index < max; index += 1) {
        const file = attachments[index];
        // eslint-disable-next-line no-await-in-loop
        await deleteFromDisk(file);
    }
    console.log(`deleteAllDraftAttachments: deleted ${attachments.length} files`);
};
exports.deleteAllDraftAttachments = deleteAllDraftAttachments;
