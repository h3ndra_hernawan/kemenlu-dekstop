"use strict";
// Copyright 2020-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.setup = exports.getLanguages = void 0;
const electron_1 = require("electron");
const os_locale_1 = require("os-locale");
const lodash_1 = require("lodash");
const url_1 = require("url");
const url_2 = require("../ts/util/url");
function getLanguages(userLocale, availableLocales) {
    const baseLocale = userLocale.split('-')[0];
    // Attempt to find the exact locale
    const candidateLocales = (0, lodash_1.uniq)([userLocale, baseLocale]).filter(l => availableLocales.includes(l));
    if (candidateLocales.length > 0) {
        return candidateLocales;
    }
    // If no languages were found then just return all locales that start with the
    // base
    return (0, lodash_1.uniq)(availableLocales.filter(l => l.startsWith(baseLocale)));
}
exports.getLanguages = getLanguages;
const setup = (browserWindow, messages) => {
    const { session } = browserWindow.webContents;
    const userLocale = (0, os_locale_1.sync)().replace(/_/g, '-');
    const availableLocales = session.availableSpellCheckerLanguages;
    const languages = getLanguages(userLocale, availableLocales);
    console.log(`spellcheck: user locale: ${userLocale}`);
    console.log('spellcheck: available spellchecker languages: ', availableLocales);
    console.log('spellcheck: setting languages to: ', languages);
    session.setSpellCheckerLanguages(languages);
    browserWindow.webContents.on('context-menu', (_event, params) => {
        const { editFlags } = params;
        const isMisspelled = Boolean(params.misspelledWord);
        const isLink = Boolean(params.linkURL);
        const isImage = params.mediaType === 'image' && params.hasImageContents && params.srcURL;
        const showMenu = params.isEditable || editFlags.canCopy || isLink || isImage;
        // Popup editor menu
        if (showMenu) {
            const template = [];
            if (isMisspelled) {
                if (params.dictionarySuggestions.length > 0) {
                    template.push(...params.dictionarySuggestions.map(label => ({
                        label,
                        click: () => {
                            browserWindow.webContents.replaceMisspelling(label);
                        },
                    })));
                }
                else {
                    template.push({
                        label: messages.contextMenuNoSuggestions.message,
                        enabled: false,
                    });
                }
                template.push({ type: 'separator' });
            }
            if (params.isEditable) {
                if (editFlags.canUndo) {
                    template.push({ label: messages.editMenuUndo.message, role: 'undo' });
                }
                // This is only ever `true` if undo was triggered via the context menu
                // (not ctrl/cmd+z)
                if (editFlags.canRedo) {
                    template.push({ label: messages.editMenuRedo.message, role: 'redo' });
                }
                if (editFlags.canUndo || editFlags.canRedo) {
                    template.push({ type: 'separator' });
                }
                if (editFlags.canCut) {
                    template.push({ label: messages.editMenuCut.message, role: 'cut' });
                }
            }
            if (editFlags.canCopy || isLink || isImage) {
                let click;
                let label;
                if (isLink) {
                    click = () => {
                        electron_1.clipboard.writeText(params.linkURL);
                    };
                    label = messages.contextMenuCopyLink.message;
                }
                else if (isImage) {
                    click = () => {
                        const parsedSrcUrl = (0, url_2.maybeParseUrl)(params.srcURL);
                        if (!parsedSrcUrl || parsedSrcUrl.protocol !== 'file:') {
                            return;
                        }
                        const image = electron_1.nativeImage.createFromPath((0, url_1.fileURLToPath)(params.srcURL));
                        electron_1.clipboard.writeImage(image);
                    };
                    label = messages.contextMenuCopyImage.message;
                }
                else {
                    label = messages.editMenuCopy.message;
                }
                template.push({
                    label,
                    role: isLink || isImage ? undefined : 'copy',
                    click,
                });
            }
            if (editFlags.canPaste && !isImage) {
                template.push({ label: messages.editMenuPaste.message, role: 'paste' });
            }
            if (editFlags.canPaste && !isImage) {
                template.push({
                    label: messages.editMenuPasteAndMatchStyle.message,
                    role: 'pasteAndMatchStyle',
                });
            }
            // Only enable select all in editors because select all in non-editors
            // results in all the UI being selected
            if (editFlags.canSelectAll && params.isEditable) {
                template.push({
                    label: messages.editMenuSelectAll.message,
                    role: 'selectAll',
                });
            }
            const menu = electron_1.Menu.buildFromTemplate(template);
            menu.popup({
                window: browserWindow,
            });
        }
    });
};
exports.setup = setup;
