"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialize = void 0;
const electron_1 = require("electron");
const rimraf = __importStar(require("rimraf"));
const attachments_1 = require("../ts/util/attachments");
let initialized = false;
const ERASE_ATTACHMENTS_KEY = 'erase-attachments';
const ERASE_STICKERS_KEY = 'erase-stickers';
const ERASE_TEMP_KEY = 'erase-temp';
const ERASE_DRAFTS_KEY = 'erase-drafts';
const CLEANUP_ORPHANED_ATTACHMENTS_KEY = 'cleanup-orphaned-attachments';
function initialize({ configDir, cleanupOrphanedAttachments, }) {
    if (initialized) {
        throw new Error('initialze: Already initialized!');
    }
    initialized = true;
    const attachmentsDir = (0, attachments_1.getPath)(configDir);
    const stickersDir = (0, attachments_1.getStickersPath)(configDir);
    const tempDir = (0, attachments_1.getTempPath)(configDir);
    const draftDir = (0, attachments_1.getDraftPath)(configDir);
    electron_1.ipcMain.on(ERASE_TEMP_KEY, event => {
        try {
            rimraf.sync(tempDir);
            event.sender.send(`${ERASE_TEMP_KEY}-done`);
        }
        catch (error) {
            const errorForDisplay = error && error.stack ? error.stack : error;
            console.log(`erase temp error: ${errorForDisplay}`);
            event.sender.send(`${ERASE_TEMP_KEY}-done`, error);
        }
    });
    electron_1.ipcMain.on(ERASE_ATTACHMENTS_KEY, event => {
        try {
            rimraf.sync(attachmentsDir);
            event.sender.send(`${ERASE_ATTACHMENTS_KEY}-done`);
        }
        catch (error) {
            const errorForDisplay = error && error.stack ? error.stack : error;
            console.log(`erase attachments error: ${errorForDisplay}`);
            event.sender.send(`${ERASE_ATTACHMENTS_KEY}-done`, error);
        }
    });
    electron_1.ipcMain.on(ERASE_STICKERS_KEY, event => {
        try {
            rimraf.sync(stickersDir);
            event.sender.send(`${ERASE_STICKERS_KEY}-done`);
        }
        catch (error) {
            const errorForDisplay = error && error.stack ? error.stack : error;
            console.log(`erase stickers error: ${errorForDisplay}`);
            event.sender.send(`${ERASE_STICKERS_KEY}-done`, error);
        }
    });
    electron_1.ipcMain.on(ERASE_DRAFTS_KEY, event => {
        try {
            rimraf.sync(draftDir);
            event.sender.send(`${ERASE_DRAFTS_KEY}-done`);
        }
        catch (error) {
            const errorForDisplay = error && error.stack ? error.stack : error;
            console.log(`erase drafts error: ${errorForDisplay}`);
            event.sender.send(`${ERASE_DRAFTS_KEY}-done`, error);
        }
    });
    electron_1.ipcMain.on(CLEANUP_ORPHANED_ATTACHMENTS_KEY, async (event) => {
        try {
            await cleanupOrphanedAttachments();
            event.sender.send(`${CLEANUP_ORPHANED_ATTACHMENTS_KEY}-done`);
        }
        catch (error) {
            const errorForDisplay = error && error.stack ? error.stack : error;
            console.log(`cleanup orphaned attachments error: ${errorForDisplay}`);
            event.sender.send(`${CLEANUP_ORPHANED_ATTACHMENTS_KEY}-done`, error);
        }
    });
}
exports.initialize = initialize;
