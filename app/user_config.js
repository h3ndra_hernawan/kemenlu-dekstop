"use strict";
// Copyright 2017-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.set = exports.remove = exports.get = exports.userConfig = void 0;
const path_1 = require("path");
const electron_1 = require("electron");
const base_config_1 = require("./base_config");
const config_1 = __importDefault(require("./config"));
// Use separate data directory for benchmarks & development
if (config_1.default.has('storagePath')) {
    electron_1.app.setPath('userData', String(config_1.default.get('storagePath')));
}
else if (config_1.default.has('storageProfile')) {
    const userData = (0, path_1.join)(electron_1.app.getPath('appData'), `Signal-${config_1.default.get('storageProfile')}`);
    electron_1.app.setPath('userData', userData);
}
console.log(`userData: ${electron_1.app.getPath('userData')}`);
const userDataPath = electron_1.app.getPath('userData');
const targetPath = (0, path_1.join)(userDataPath, 'config.json');
exports.userConfig = (0, base_config_1.start)('user', targetPath);
exports.get = exports.userConfig.get.bind(exports.userConfig);
exports.remove = exports.userConfig.remove.bind(exports.userConfig);
exports.set = exports.userConfig.set.bind(exports.userConfig);
