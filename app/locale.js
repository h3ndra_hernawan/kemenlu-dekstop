"use strict";
// Copyright 2017-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.load = void 0;
const path_1 = require("path");
const fs_1 = require("fs");
const lodash_1 = require("lodash");
const setupI18n_1 = require("../ts/util/setupI18n");
function normalizeLocaleName(locale) {
    if (/^en-/.test(locale)) {
        return 'en';
    }
    return locale;
}
function getLocaleMessages(locale) {
    const onDiskLocale = locale.replace('-', '_');
    const targetFile = (0, path_1.join)(__dirname, '..', '_locales', onDiskLocale, 'messages.json');
    return JSON.parse((0, fs_1.readFileSync)(targetFile, 'utf-8'));
}
function load({ appLocale, logger, }) {
    if (!appLocale) {
        throw new TypeError('`appLocale` is required');
    }
    if (!logger || !logger.error) {
        throw new TypeError('`logger.error` is required');
    }
    const english = getLocaleMessages('en');
    // Load locale - if we can't load messages for the current locale, we
    // default to 'en'
    //
    // possible locales:
    // https://github.com/electron/electron/blob/master/docs/api/locales.md
    let localeName = normalizeLocaleName(appLocale);
    let messages;
    try {
        messages = getLocaleMessages(localeName);
        // We start with english, then overwrite that with anything present in locale
        messages = (0, lodash_1.merge)(english, messages);
    }
    catch (e) {
        logger.error(`Problem loading messages for locale ${localeName} ${e.stack}`);
        logger.error('Falling back to en locale');
        localeName = 'en';
        messages = english;
    }
    const i18n = (0, setupI18n_1.setupI18n)(appLocale, messages);
    return {
        i18n,
        name: localeName,
        messages,
    };
}
exports.load = load;
