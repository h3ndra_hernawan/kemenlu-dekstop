"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialize = void 0;
const electron_1 = require("electron");
const user_config_1 = require("./user_config");
const ephemeral_config_1 = require("./ephemeral_config");
let sql;
let initialized = false;
const SQL_CHANNEL_KEY = 'sql-channel';
const ERASE_SQL_KEY = 'erase-sql-key';
function initialize(mainSQL) {
    if (initialized) {
        throw new Error('sqlChannels: already initialized!');
    }
    initialized = true;
    sql = mainSQL;
    electron_1.ipcMain.on(SQL_CHANNEL_KEY, async (event, jobId, callName, ...args) => {
        try {
            if (!sql) {
                throw new Error(`${SQL_CHANNEL_KEY}: Not yet initialized!`);
            }
            const result = await sql.sqlCall(callName, args);
            event.sender.send(`${SQL_CHANNEL_KEY}-done`, jobId, null, result);
        }
        catch (error) {
            const errorForDisplay = error && error.stack ? error.stack : error;
            console.log(`sql channel error with call ${callName}: ${errorForDisplay}`);
            if (!event.sender.isDestroyed()) {
                event.sender.send(`${SQL_CHANNEL_KEY}-done`, jobId, errorForDisplay);
            }
        }
    });
    electron_1.ipcMain.on(ERASE_SQL_KEY, async (event) => {
        try {
            (0, user_config_1.remove)();
            (0, ephemeral_config_1.remove)();
            event.sender.send(`${ERASE_SQL_KEY}-done`);
        }
        catch (error) {
            const errorForDisplay = error && error.stack ? error.stack : error;
            console.log(`sql-erase error: ${errorForDisplay}`);
            event.sender.send(`${ERASE_SQL_KEY}-done`, error);
        }
    });
}
exports.initialize = initialize;
