"use strict";
// Copyright 2017-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.readyForShutdown = exports.markReadyForShutdown = exports.shouldQuit = exports.markShouldQuit = void 0;
let shouldQuitFlag = false;
function markShouldQuit() {
    shouldQuitFlag = true;
}
exports.markShouldQuit = markShouldQuit;
function shouldQuit() {
    return shouldQuitFlag;
}
exports.shouldQuit = shouldQuit;
let isReadyForShutdown = false;
function markReadyForShutdown() {
    isReadyForShutdown = true;
}
exports.markReadyForShutdown = markReadyForShutdown;
function readyForShutdown() {
    return isReadyForShutdown;
}
exports.readyForShutdown = readyForShutdown;
