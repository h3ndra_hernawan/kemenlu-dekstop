"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addHandler = exports.updateLocale = void 0;
const electron_1 = require("electron");
const Errors = __importStar(require("../ts/types/errors"));
const privacy_1 = require("../ts/util/privacy");
const reallyJsonStringify_1 = require("../ts/util/reallyJsonStringify");
// We use hard-coded strings until we're able to update these strings from the locale.
let quitText = 'Quit';
let copyErrorAndQuitText = 'Copy error and quit';
function handleError(prefix, error) {
    if (console._error) {
        console._error(`${prefix}:`, Errors.toLogFormat(error));
    }
    console.error(`${prefix}:`, Errors.toLogFormat(error));
    if (electron_1.app.isReady()) {
        // title field is not shown on macOS, so we don't use it
        const buttonIndex = electron_1.dialog.showMessageBoxSync({
            buttons: [quitText, copyErrorAndQuitText],
            defaultId: 0,
            detail: (0, privacy_1.redactAll)(error.stack || ''),
            message: prefix,
            noLink: true,
            type: 'error',
        });
        if (buttonIndex === 1) {
            electron_1.clipboard.writeText(`${prefix}\n\n${(0, privacy_1.redactAll)(error.stack || '')}`);
        }
    }
    else {
        electron_1.dialog.showErrorBox(prefix, error.stack || '');
    }
    electron_1.app.exit(1);
}
const updateLocale = (messages) => {
    quitText = messages.quit.message;
    copyErrorAndQuitText = messages.copyErrorAndQuit.message;
};
exports.updateLocale = updateLocale;
function _getError(reason) {
    if (reason instanceof Error) {
        return reason;
    }
    const errorString = (0, reallyJsonStringify_1.reallyJsonStringify)(reason);
    return new Error(`Promise rejected with a non-error: ${errorString}`);
}
const addHandler = () => {
    process.on('uncaughtException', (reason) => {
        handleError('Unhandled Error', _getError(reason));
    });
    process.on('unhandledRejection', (reason) => {
        handleError('Unhandled Promise Rejection', _getError(reason));
    });
};
exports.addHandler = addHandler;
