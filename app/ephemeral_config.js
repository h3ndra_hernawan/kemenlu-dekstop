"use strict";
// Copyright 2018-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.set = exports.remove = exports.get = exports.ephemeralConfig = void 0;
const path_1 = require("path");
const electron_1 = require("electron");
const base_config_1 = require("./base_config");
const userDataPath = electron_1.app.getPath('userData');
const targetPath = (0, path_1.join)(userDataPath, 'ephemeral.json');
exports.ephemeralConfig = (0, base_config_1.start)('ephemeral', targetPath, {
    allowMalformedOnStartup: true,
});
exports.get = exports.ephemeralConfig.get.bind(exports.ephemeralConfig);
exports.remove = exports.ephemeralConfig.remove.bind(exports.ephemeralConfig);
exports.set = exports.ephemeralConfig.set.bind(exports.ephemeralConfig);
