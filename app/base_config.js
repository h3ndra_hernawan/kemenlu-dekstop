"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.start = void 0;
const fs_1 = require("fs");
const lodash_1 = require("lodash");
const ENCODING = 'utf8';
function start(name, targetPath, options) {
    let cachedValue;
    let incomingJson;
    try {
        incomingJson = (0, fs_1.readFileSync)(targetPath, ENCODING);
        cachedValue = incomingJson ? JSON.parse(incomingJson) : undefined;
        console.log(`config/get: Successfully read ${name} config file`);
        if (!cachedValue) {
            console.log(`config/get: ${name} config value was falsy, cache is now empty object`);
            cachedValue = Object.create(null);
        }
    }
    catch (error) {
        if (!(options === null || options === void 0 ? void 0 : options.allowMalformedOnStartup) && error.code !== 'ENOENT') {
            throw error;
        }
        if (incomingJson) {
            console.log(`config/get: ${name} config file was malformed, starting afresh`);
        }
        else {
            console.log(`config/get: Did not find ${name} config file (or it was empty), cache is now empty object`);
        }
        cachedValue = Object.create(null);
    }
    function ourGet(keyPath) {
        return (0, lodash_1.get)(cachedValue, keyPath);
    }
    function ourSet(keyPath, value) {
        if (!cachedValue) {
            throw new Error('ourSet: no cachedValue!');
        }
        (0, lodash_1.set)(cachedValue, keyPath, value);
        console.log(`config/set: Saving ${name} config to disk`);
        const outgoingJson = JSON.stringify(cachedValue, null, '  ');
        (0, fs_1.writeFileSync)(targetPath, outgoingJson, ENCODING);
    }
    function remove() {
        console.log(`config/remove: Deleting ${name} config from disk`);
        (0, fs_1.unlinkSync)(targetPath);
        cachedValue = Object.create(null);
    }
    return {
        set: ourSet,
        get: ourGet,
        remove,
        _getCachedValue: () => cachedValue,
    };
}
exports.start = start;
