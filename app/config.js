"use strict";
// Copyright 2017-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const electron_1 = require("electron");
const environment_1 = require("../ts/environment");
// In production mode, NODE_ENV cannot be customized by the user
if (electron_1.app.isPackaged) {
    (0, environment_1.setEnvironment)(environment_1.Environment.Production);
}
else {
    (0, environment_1.setEnvironment)((0, environment_1.parseEnvironment)(process.env.NODE_ENV || 'development'));
}
// Set environment vars to configure node-config before requiring it
process.env.NODE_ENV = (0, environment_1.getEnvironment)();
process.env.NODE_CONFIG_DIR = (0, path_1.join)(__dirname, '..', 'config');
if ((0, environment_1.getEnvironment)() === environment_1.Environment.Production) {
    // harden production config against the local env
    process.env.NODE_CONFIG = '';
    process.env.NODE_CONFIG_STRICT_MODE = 'true';
    process.env.HOSTNAME = '';
    process.env.NODE_APP_INSTANCE = '';
    process.env.ALLOW_CONFIG_MUTATIONS = '';
    process.env.SUPPRESS_NO_CONFIG_WARNING = '';
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '';
    process.env.SIGNAL_ENABLE_HTTP = '';
}
// We load config after we've made our modifications to NODE_ENV
// eslint-disable-next-line import/order, import/first
const config_1 = __importDefault(require("config"));
// Log resulting env vars in use by config
[
    'NODE_ENV',
    'NODE_CONFIG_DIR',
    'NODE_CONFIG',
    'ALLOW_CONFIG_MUTATIONS',
    'HOSTNAME',
    'NODE_APP_INSTANCE',
    'SUPPRESS_NO_CONFIG_WARNING',
    'SIGNAL_ENABLE_HTTP',
].forEach(s => {
    console.log(`${s} ${config_1.default.util.getEnv(s)}`);
});
exports.default = config_1.default;
