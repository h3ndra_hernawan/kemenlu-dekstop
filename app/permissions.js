"use strict";
// Copyright 2018-2020 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
Object.defineProperty(exports, "__esModule", { value: true });
exports.installPermissionsHandler = void 0;
const PERMISSIONS = {
    // Allowed
    fullscreen: true,
    notifications: true,
    // Off by default, can be enabled by user
    media: false,
    // Not allowed
    geolocation: false,
    midiSysex: false,
    openExternal: false,
    pointerLock: false,
};
function _createPermissionHandler(userConfig) {
    return (_webContents, permission, callback, details) => {
        var _a, _b, _c, _d;
        // We default 'media' permission to false, but the user can override that for
        // the microphone and camera.
        if (permission === 'media') {
            if (((_a = details.mediaTypes) === null || _a === void 0 ? void 0 : _a.includes('audio')) ||
                ((_b = details.mediaTypes) === null || _b === void 0 ? void 0 : _b.includes('video'))) {
                if (((_c = details.mediaTypes) === null || _c === void 0 ? void 0 : _c.includes('audio')) &&
                    userConfig.get('mediaPermissions')) {
                    callback(true);
                    return;
                }
                if (((_d = details.mediaTypes) === null || _d === void 0 ? void 0 : _d.includes('video')) &&
                    userConfig.get('mediaCameraPermissions')) {
                    callback(true);
                    return;
                }
                callback(false);
                return;
            }
            // If it doesn't have 'video' or 'audio', it's probably screenshare.
            // TODO: DESKTOP-1611
            callback(true);
            return;
        }
        if (PERMISSIONS[permission]) {
            console.log(`Approving request for permission '${permission}'`);
            callback(true);
            return;
        }
        console.log(`Denying request for permission '${permission}'`);
        callback(false);
    };
}
function installPermissionsHandler({ session, userConfig, }) {
    // Setting the permission request handler to null first forces any permissions to be
    //   requested again. Without this, revoked permissions might still be available if
    //   they've already been used successfully.
    session.defaultSession.setPermissionRequestHandler(null);
    session.defaultSession.setPermissionRequestHandler(_createPermissionHandler(userConfig));
}
exports.installPermissionsHandler = installPermissionsHandler;
