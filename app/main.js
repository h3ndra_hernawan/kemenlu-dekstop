"use strict";
// Copyright 2017-2021 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.windowConfigSchema = void 0;
const path_1 = require("path");
const url_1 = require("url");
const os = __importStar(require("os"));
const fs_extra_1 = require("fs-extra");
const crypto_1 = require("crypto");
const pify_1 = __importDefault(require("pify"));
const normalize_path_1 = __importDefault(require("normalize-path"));
const fast_glob_1 = __importDefault(require("fast-glob"));
const p_queue_1 = __importDefault(require("p-queue"));
const lodash_1 = require("lodash");
const electron_1 = require("electron");
const zod_1 = require("zod");
const package_json_1 = __importDefault(require("../package.json"));
const GlobalErrors = __importStar(require("./global_errors"));
const spell_check_1 = require("./spell_check");
const privacy_1 = require("../ts/util/privacy");
const assert_1 = require("../ts/util/assert");
const consoleLogger_1 = require("../ts/util/consoleLogger");
require("./startup_config");
const config_1 = __importDefault(require("./config"));
const environment_1 = require("../ts/environment");
// Very important to put before the single instance check, since it is based on the
//   userData directory. (see requestSingleInstanceLock below)
const userConfig = __importStar(require("./user_config"));
// We generally want to pull in our own modules after this point, after the user
//   data directory has been set.
const attachments = __importStar(require("./attachments"));
const attachmentChannel = __importStar(require("./attachment_channel"));
const bounce = __importStar(require("../ts/services/bounce"));
const updater = __importStar(require("../ts/updater/index"));
const SystemTrayService_1 = require("./SystemTrayService");
const SystemTraySettingCache_1 = require("./SystemTraySettingCache");
const SystemTraySetting_1 = require("../ts/types/SystemTraySetting");
const ephemeralConfig = __importStar(require("./ephemeral_config"));
const logging = __importStar(require("../ts/logging/main_process_logging"));
const main_1 = require("../ts/sql/main");
const sqlChannels = __importStar(require("./sql_channel"));
const windowState = __importStar(require("./window_state"));
const menu_1 = require("./menu");
const protocol_filter_1 = require("./protocol_filter");
const OS = __importStar(require("../ts/OS"));
const version_1 = require("../ts/util/version");
const sgnlHref_1 = require("../ts/util/sgnlHref");
const toggleMaximizedBrowserWindow_1 = require("../ts/util/toggleMaximizedBrowserWindow");
const Settings_1 = require("../ts/types/Settings");
const challengeMain_1 = require("../ts/main/challengeMain");
const NativeThemeNotifier_1 = require("../ts/main/NativeThemeNotifier");
const powerChannel_1 = require("../ts/main/powerChannel");
const settingsChannel_1 = require("../ts/main/settingsChannel");
const url_2 = require("../ts/util/url");
const heicConverterMain_1 = require("../ts/workers/heicConverterMain");
const locale_1 = require("./locale");
const animationSettings = electron_1.systemPreferences.getAnimationSettings();
const getRealPath = (0, pify_1.default)(fs_extra_1.realpath);
// Keep a global reference of the window object, if you don't, the window will
//   be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let mainWindowCreated = false;
let loadingWindow;
const activeWindows = new Set();
function getMainWindow() {
    return mainWindow;
}
const development = (0, environment_1.getEnvironment)() === environment_1.Environment.Development ||
    (0, environment_1.getEnvironment)() === environment_1.Environment.Staging;
const enableCI = config_1.default.get('enableCI');
const sql = new main_1.MainSQL();
const heicConverter = (0, heicConverterMain_1.getHeicConverter)();
let systemTrayService;
const systemTraySettingCache = new SystemTraySettingCache_1.SystemTraySettingCache(sql, ephemeralConfig, process.argv, electron_1.app.getVersion());
const challengeHandler = new challengeMain_1.ChallengeMainHandler();
const nativeThemeNotifier = new NativeThemeNotifier_1.NativeThemeNotifier();
nativeThemeNotifier.initialize();
let sqlInitTimeStart = 0;
let sqlInitTimeEnd = 0;
let appStartInitialSpellcheckSetting = true;
const defaultWebPrefs = {
    devTools: process.argv.some(arg => arg === '--enable-dev-tools') ||
        (0, environment_1.getEnvironment)() !== environment_1.Environment.Production ||
        !(0, version_1.isProduction)(electron_1.app.getVersion()),
};
async function getSpellCheckSetting() {
    const fastValue = ephemeralConfig.get('spell-check');
    if (fastValue !== undefined) {
        getLogger().info('got fast spellcheck setting', fastValue);
        return fastValue;
    }
    const json = await sql.sqlCall('getItemById', ['spell-check']);
    // Default to `true` if setting doesn't exist yet
    const slowValue = json ? json.value : true;
    ephemeralConfig.set('spell-check', slowValue);
    getLogger().info('got slow spellcheck setting', slowValue);
    return slowValue;
}
function showWindow() {
    if (!mainWindow) {
        return;
    }
    // Using focus() instead of show() seems to be important on Windows when our window
    //   has been docked using Aero Snap/Snap Assist. A full .show() call here will cause
    //   the window to reposition:
    //   https://github.com/signalapp/Signal-Desktop/issues/1429
    if (mainWindow.isVisible()) {
        mainWindow.focus();
    }
    else {
        mainWindow.show();
    }
}
// This code runs before the 'ready' event fires, so we don't have our logging
//   infrastructure in place yet. So we use console.log directly.
/* eslint-disable no-console */
if (!process.mas) {
    console.log('making app single instance');
    const gotLock = electron_1.app.requestSingleInstanceLock();
    if (!gotLock) {
        console.log('quitting; we are the second instance');
        electron_1.app.exit();
    }
    else {
        electron_1.app.on('second-instance', (_e, argv) => {
            // Someone tried to run a second instance, we should focus our window
            if (mainWindow) {
                if (mainWindow.isMinimized()) {
                    mainWindow.restore();
                }
                showWindow();
            }
            if (!logger) {
                console.log('second-instance: logger not initialized; skipping further checks');
                return;
            }
            const incomingCaptchaHref = getIncomingCaptchaHref(argv);
            if (incomingCaptchaHref) {
                const { captcha } = (0, sgnlHref_1.parseCaptchaHref)(incomingCaptchaHref, getLogger());
                challengeHandler.handleCaptcha(captcha);
                return true;
            }
            // Are they trying to open a sgnl:// href?
            const incomingHref = getIncomingHref(argv);
            if (incomingHref) {
                handleSgnlHref(incomingHref);
            }
            // Handled
            return true;
        });
    }
}
/* eslint-enable no-console */
const windowFromUserConfig = userConfig.get('window');
const windowFromEphemeral = ephemeralConfig.get('window');
exports.windowConfigSchema = zod_1.z.object({
    maximized: zod_1.z.boolean().optional(),
    autoHideMenuBar: zod_1.z.boolean().optional(),
    fullscreen: zod_1.z.boolean().optional(),
    width: zod_1.z.number(),
    height: zod_1.z.number(),
    x: zod_1.z.number(),
    y: zod_1.z.number(),
});
let windowConfig;
const windowConfigParsed = exports.windowConfigSchema.safeParse(windowFromEphemeral || windowFromUserConfig);
if (windowConfigParsed.success) {
    windowConfig = windowConfigParsed.data;
}
if (windowFromUserConfig) {
    userConfig.set('window', null);
    ephemeralConfig.set('window', windowConfig);
}
// These will be set after app fires the 'ready' event
let logger;
let locale;
let settingsChannel;
function getLogger() {
    if (!logger) {
        console.warn('getLogger: Logger not yet initialized!');
        return consoleLogger_1.consoleLogger;
    }
    return logger;
}
function getLocale() {
    if (!locale) {
        throw new Error('getLocale: Locale not yet initialized!');
    }
    return locale;
}
function prepareFileUrl(pathSegments, moreKeys) {
    const filePath = (0, path_1.join)(...pathSegments);
    const fileUrl = (0, url_1.pathToFileURL)(filePath);
    return prepareUrl(fileUrl, moreKeys);
}
function prepareUrl(url, moreKeys) {
    return (0, url_2.setUrlSearchParams)(url, Object.assign({ name: package_json_1.default.productName, locale: locale ? locale.name : undefined, version: electron_1.app.getVersion(), buildCreation: config_1.default.get('buildCreation'), buildExpiration: config_1.default.get('buildExpiration'), serverUrl: config_1.default.get('serverUrl'), storageUrl: config_1.default.get('storageUrl'), updatesUrl: config_1.default.get('updatesUrl'), directoryUrl: config_1.default.get('directoryUrl'), directoryEnclaveId: config_1.default.get('directoryEnclaveId'), directoryTrustAnchor: config_1.default.get('directoryTrustAnchor'), directoryV2Url: config_1.default.get('directoryV2Url'), directoryV2PublicKey: config_1.default.get('directoryV2PublicKey'), directoryV2CodeHash: config_1.default.get('directoryV2CodeHash'), cdnUrl0: config_1.default.get('cdn').get('0'), cdnUrl2: config_1.default.get('cdn').get('2'), certificateAuthority: config_1.default.get('certificateAuthority'), environment: enableCI ? 'production' : (0, environment_1.getEnvironment)(), enableCI: enableCI ? 'true' : '', node_version: process.versions.node, hostname: os.hostname(), appInstance: process.env.NODE_APP_INSTANCE, proxyUrl: process.env.HTTPS_PROXY || process.env.https_proxy, contentProxyUrl: config_1.default.get('contentProxyUrl'), sfuUrl: config_1.default.get('sfuUrl'), reducedMotionSetting: animationSettings.prefersReducedMotion ? 'true' : '', serverPublicParams: config_1.default.get('serverPublicParams'), serverTrustRoot: config_1.default.get('serverTrustRoot'), appStartInitialSpellcheckSetting, userDataPath: electron_1.app.getPath('userData'), downloadsPath: electron_1.app.getPath('downloads'), homePath: electron_1.app.getPath('home') }, moreKeys)).href;
}
async function handleUrl(event, target) {
    event.preventDefault();
    const parsedUrl = (0, url_2.maybeParseUrl)(target);
    if (!parsedUrl) {
        return;
    }
    const { protocol, hostname } = parsedUrl;
    const isDevServer = process.env.SIGNAL_ENABLE_HTTP && hostname === 'localhost';
    // We only want to specially handle urls that aren't requesting the dev server
    if ((0, sgnlHref_1.isSgnlHref)(target, getLogger()) ||
        (0, sgnlHref_1.isSignalHttpsLink)(target, getLogger())) {
        handleSgnlHref(target);
        return;
    }
    if ((protocol === 'http:' || protocol === 'https:') && !isDevServer) {
        try {
            await electron_1.shell.openExternal(target);
        }
        catch (error) {
            getLogger().error(`Failed to open url: ${error.stack}`);
        }
    }
}
function handleCommonWindowEvents(window) {
    window.webContents.on('will-navigate', handleUrl);
    window.webContents.on('new-window', handleUrl);
    window.webContents.on('preload-error', (_event, preloadPath, error) => {
        getLogger().error(`Preload error in ${preloadPath}: `, error.message);
    });
    activeWindows.add(window);
    window.on('closed', () => activeWindows.delete(window));
    // Works only for mainWindow because it has `enablePreferredSizeMode`
    let lastZoomFactor = window.webContents.getZoomFactor();
    const onZoomChanged = () => {
        if (window.isDestroyed() ||
            !window.webContents ||
            window.webContents.isDestroyed()) {
            return;
        }
        const zoomFactor = window.webContents.getZoomFactor();
        if (lastZoomFactor === zoomFactor) {
            return;
        }
        settingsChannel === null || settingsChannel === void 0 ? void 0 : settingsChannel.invokeCallbackInMainWindow('persistZoomFactor', [
            zoomFactor,
        ]);
        lastZoomFactor = zoomFactor;
    };
    window.webContents.on('preferred-size-changed', onZoomChanged);
    nativeThemeNotifier.addWindow(window);
}
const DEFAULT_WIDTH = 800;
const DEFAULT_HEIGHT = 610;
const MIN_WIDTH = 680;
const MIN_HEIGHT = 550;
const BOUNDS_BUFFER = 100;
function isVisible(window, bounds) {
    const boundsX = (0, lodash_1.get)(bounds, 'x') || 0;
    const boundsY = (0, lodash_1.get)(bounds, 'y') || 0;
    const boundsWidth = (0, lodash_1.get)(bounds, 'width') || DEFAULT_WIDTH;
    const boundsHeight = (0, lodash_1.get)(bounds, 'height') || DEFAULT_HEIGHT;
    // requiring BOUNDS_BUFFER pixels on the left or right side
    const rightSideClearOfLeftBound = window.x + window.width >= boundsX + BOUNDS_BUFFER;
    const leftSideClearOfRightBound = window.x <= boundsX + boundsWidth - BOUNDS_BUFFER;
    // top can't be offscreen, and must show at least BOUNDS_BUFFER pixels at bottom
    const topClearOfUpperBound = window.y >= boundsY;
    const topClearOfLowerBound = window.y <= boundsY + boundsHeight - BOUNDS_BUFFER;
    return (rightSideClearOfLeftBound &&
        leftSideClearOfRightBound &&
        topClearOfUpperBound &&
        topClearOfLowerBound);
}
let windowIcon;
if (OS.isWindows()) {
    windowIcon = (0, path_1.join)(__dirname, '../images/kemenlu.png');
}
else if (OS.isLinux()) {
    windowIcon = (0, path_1.join)(__dirname, '../images/kemenlu.png');
}
else {
    windowIcon = (0, path_1.join)(__dirname, '../images/kemenlu.png');
}
async function createWindow() {
    const windowOptions = Object.assign({ show: false, width: DEFAULT_WIDTH, height: DEFAULT_HEIGHT, minWidth: MIN_WIDTH, minHeight: MIN_HEIGHT, autoHideMenuBar: false, titleBarStyle: (0, Settings_1.getTitleBarVisibility)() === Settings_1.TitleBarVisibility.Hidden &&
            !(0, environment_1.isTestEnvironment)((0, environment_1.getEnvironment)())
            ? 'hidden'
            : 'default', backgroundColor: (0, environment_1.isTestEnvironment)((0, environment_1.getEnvironment)())
            ? '#ffffff' // Tests should always be rendered on a white background
            : '#3a76f0', webPreferences: Object.assign(Object.assign({}, defaultWebPrefs), { nodeIntegration: false, nodeIntegrationInWorker: false, contextIsolation: false, preload: (0, path_1.join)(__dirname, enableCI || (0, environment_1.getEnvironment)() === environment_1.Environment.Production
                ? '../preload.bundle.js'
                : '../preload.js'), nativeWindowOpen: true, spellcheck: await getSpellCheckSetting(), 
            // We are evaluating background throttling in development. If we decide to
            //   move forward, we can remove this line (as `backgroundThrottling` is true by
            //   default).
            backgroundThrottling: development, enablePreferredSizeMode: true }), icon: windowIcon }, (0, lodash_1.pick)(windowConfig, ['autoHideMenuBar', 'width', 'height', 'x', 'y']));
    if (!(0, lodash_1.isNumber)(windowOptions.width) || windowOptions.width < MIN_WIDTH) {
        windowOptions.width = DEFAULT_WIDTH;
    }
    if (!(0, lodash_1.isNumber)(windowOptions.height) || windowOptions.height < MIN_HEIGHT) {
        windowOptions.height = DEFAULT_HEIGHT;
    }
    if (!(0, lodash_1.isBoolean)(windowOptions.autoHideMenuBar)) {
        delete windowOptions.autoHideMenuBar;
    }
    const startInTray = (await systemTraySettingCache.get()) ===
        SystemTraySetting_1.SystemTraySetting.MinimizeToAndStartInSystemTray;
    const visibleOnAnyScreen = (0, lodash_1.some)(electron_1.screen.getAllDisplays(), display => {
        if ((0, lodash_1.isNumber)(windowOptions.x) &&
            (0, lodash_1.isNumber)(windowOptions.y) &&
            (0, lodash_1.isNumber)(windowOptions.width) &&
            (0, lodash_1.isNumber)(windowOptions.height)) {
            return isVisible(windowOptions, (0, lodash_1.get)(display, 'bounds'));
        }
        getLogger().error("visibleOnAnyScreen: windowOptions didn't have valid bounds fields");
        return false;
    });
    if (!visibleOnAnyScreen) {
        getLogger().info('Location reset needed');
        delete windowOptions.x;
        delete windowOptions.y;
    }
    getLogger().info('Initializing BrowserWindow config:', JSON.stringify(windowOptions));
    // Create the browser window.
    mainWindow = new electron_1.BrowserWindow(windowOptions);
    if (settingsChannel) {
        settingsChannel.setMainWindow(mainWindow);
    }
    mainWindowCreated = true;
    (0, spell_check_1.setup)(mainWindow, getLocale().messages);
    if (!startInTray && windowConfig && windowConfig.maximized) {
        mainWindow.maximize();
    }
    if (!startInTray && windowConfig && windowConfig.fullscreen) {
        mainWindow.setFullScreen(true);
    }
    if (systemTrayService) {
        systemTrayService.setMainWindow(mainWindow);
    }
    function captureAndSaveWindowStats() {
        if (!mainWindow) {
            return;
        }
        const size = mainWindow.getSize();
        const position = mainWindow.getPosition();
        // so if we need to recreate the window, we have the most recent settings
        windowConfig = {
            maximized: mainWindow.isMaximized(),
            autoHideMenuBar: mainWindow.autoHideMenuBar,
            fullscreen: mainWindow.isFullScreen(),
            width: size[0],
            height: size[1],
            x: position[0],
            y: position[1],
        };
        getLogger().info('Updating BrowserWindow config: %s', JSON.stringify(windowConfig));
        ephemeralConfig.set('window', windowConfig);
    }
    const debouncedCaptureStats = (0, lodash_1.debounce)(captureAndSaveWindowStats, 500);
    mainWindow.on('resize', debouncedCaptureStats);
    mainWindow.on('move', debouncedCaptureStats);
    const setWindowFocus = () => {
        if (!mainWindow) {
            return;
        }
        mainWindow.webContents.send('set-window-focus', mainWindow.isFocused());
    };
    mainWindow.on('focus', setWindowFocus);
    mainWindow.on('blur', setWindowFocus);
    mainWindow.once('ready-to-show', setWindowFocus);
    // This is a fallback in case we drop an event for some reason.
    setInterval(setWindowFocus, 10000);
    const moreKeys = {
        isFullScreen: String(Boolean(mainWindow.isFullScreen())),
    };
    if ((0, environment_1.getEnvironment)() === environment_1.Environment.Test) {
        mainWindow.loadURL(prepareFileUrl([__dirname, '../test/index.html'], moreKeys));
    }
    else if ((0, environment_1.getEnvironment)() === environment_1.Environment.TestLib) {
        mainWindow.loadURL(prepareFileUrl([__dirname, '../libtextsecure/test/index.html'], moreKeys));
    }
    else {
        mainWindow.loadURL(prepareFileUrl([__dirname, '../background.html'], moreKeys));
    }
    if (!enableCI && config_1.default.get('openDevTools')) {
        // Open the DevTools.
        mainWindow.webContents.openDevTools();
    }
    handleCommonWindowEvents(mainWindow);
    // App dock icon bounce
    bounce.init(mainWindow);
    // Emitted when the window is about to be closed.
    // Note: We do most of our shutdown logic here because all windows are closed by
    //   Electron before the app quits.
    mainWindow.on('close', async (e) => {
        if (!mainWindow) {
            getLogger().info('close event: no main window');
            return;
        }
        getLogger().info('close event', {
            readyForShutdown: windowState.readyForShutdown(),
            shouldQuit: windowState.shouldQuit(),
        });
        // If the application is terminating, just do the default
        if ((0, environment_1.isTestEnvironment)((0, environment_1.getEnvironment)()) ||
            (windowState.readyForShutdown() && windowState.shouldQuit())) {
            return;
        }
        // Prevent the shutdown
        e.preventDefault();
        /**
         * if the user is in fullscreen mode and closes the window, not the
         * application, we need them leave fullscreen first before closing it to
         * prevent a black screen.
         *
         * issue: https://github.com/signalapp/Signal-Desktop/issues/4348
         */
        if (mainWindow.isFullScreen()) {
            mainWindow.once('leave-full-screen', () => mainWindow === null || mainWindow === void 0 ? void 0 : mainWindow.hide());
            mainWindow.setFullScreen(false);
        }
        else {
            mainWindow.hide();
        }
        // On Mac, or on other platforms when the tray icon is in use, the window
        // should be only hidden, not closed, when the user clicks the close button
        const usingTrayIcon = (0, SystemTraySetting_1.shouldMinimizeToSystemTray)(await systemTraySettingCache.get());
        if (!windowState.shouldQuit() && (usingTrayIcon || OS.isMacOS())) {
            return;
        }
        await requestShutdown();
        windowState.markReadyForShutdown();
        await sql.close();
        electron_1.app.quit();
    });
    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = undefined;
        if (settingsChannel) {
            settingsChannel.setMainWindow(mainWindow);
        }
        if (systemTrayService) {
            systemTrayService.setMainWindow(mainWindow);
        }
    });
    mainWindow.on('enter-full-screen', () => {
        if (mainWindow) {
            mainWindow.webContents.send('full-screen-change', true);
        }
    });
    mainWindow.on('leave-full-screen', () => {
        if (mainWindow) {
            mainWindow.webContents.send('full-screen-change', false);
        }
    });
    mainWindow.once('ready-to-show', async () => {
        getLogger().info('main window is ready-to-show');
        // Ignore sql errors and show the window anyway
        await sqlInitPromise;
        if (!mainWindow) {
            return;
        }
        if (!startInTray) {
            getLogger().info('showing main window');
            mainWindow.show();
        }
    });
}
// Renderer asks if we are done with the database
electron_1.ipcMain.on('database-ready', async (event) => {
    if (!sqlInitPromise) {
        getLogger().error('database-ready requested, but sqlInitPromise is falsey');
        return;
    }
    const { error } = await sqlInitPromise;
    if (error) {
        getLogger().error('database-ready requested, but got sql error', error && error.stack);
        return;
    }
    getLogger().info('sending `database-ready`');
    event.sender.send('database-ready');
});
electron_1.ipcMain.on('show-window', () => {
    showWindow();
});
electron_1.ipcMain.on('title-bar-double-click', () => {
    if (!mainWindow) {
        return;
    }
    if (OS.isMacOS()) {
        switch (electron_1.systemPreferences.getUserDefault('AppleActionOnDoubleClick', 'string')) {
            case 'Minimize':
                mainWindow.minimize();
                break;
            case 'Maximize':
                (0, toggleMaximizedBrowserWindow_1.toggleMaximizedBrowserWindow)(mainWindow);
                break;
            default:
                // If this is disabled, it'll be 'None'. If it's anything else, that's unexpected,
                //   but we'll just no-op.
                break;
        }
    }
    else {
        // This is currently only supported on macOS. This `else` branch is just here when/if
        //   we add support for other operating systems.
        (0, toggleMaximizedBrowserWindow_1.toggleMaximizedBrowserWindow)(mainWindow);
    }
});
electron_1.ipcMain.on('set-is-call-active', (_event, isCallActive) => {
    if (!mainWindow) {
        return;
    }
    // We are evaluating background throttling in development. If we decide to move
    //   forward, we can remove this check.
    if (!development) {
        return;
    }
    let backgroundThrottling;
    if (isCallActive) {
        getLogger().info('Background throttling disabled because a call is active');
        backgroundThrottling = false;
    }
    else {
        getLogger().info('Background throttling enabled because no call is active');
        backgroundThrottling = true;
    }
    mainWindow.webContents.setBackgroundThrottling(backgroundThrottling);
});
electron_1.ipcMain.on('convert-image', async (event, uuid, data) => {
    const { error, response } = await heicConverter(uuid, data);
    event.reply(`convert-image:${uuid}`, { error, response });
});
let isReadyForUpdates = false;
async function readyForUpdates() {
    if (isReadyForUpdates) {
        return;
    }
    isReadyForUpdates = true;
    // First, install requested sticker pack
    const incomingHref = getIncomingHref(process.argv);
    if (incomingHref) {
        handleSgnlHref(incomingHref);
    }
    // Second, start checking for app updates
    try {
        (0, assert_1.strictAssert)(settingsChannel !== undefined, 'SettingsChannel must be initialized');
        await updater.start(settingsChannel, getLogger(), getMainWindow);
    }
    catch (error) {
        getLogger().error('Error starting update checks:', error && error.stack ? error.stack : error);
    }
}
async function forceUpdate() {
    try {
        getLogger().info('starting force update');
        await updater.force();
    }
    catch (error) {
        getLogger().error('Error during force update:', error && error.stack ? error.stack : error);
    }
}
electron_1.ipcMain.once('ready-for-updates', readyForUpdates);
const TEN_MINUTES = 10 * 60 * 1000;
setTimeout(readyForUpdates, TEN_MINUTES);
// the support only provides a subset of languages available within the app
// so we have to list them out here and fallback to english if not included
const SUPPORT_LANGUAGES = [
    'ar',
    'bn',
    'de',
    'en-us',
    'es',
    'fr',
    'hi',
    'hi-in',
    'hc',
    'id',
    'it',
    'ja',
    'ko',
    'mr',
    'ms',
    'nl',
    'pl',
    'pt',
    'ru',
    'sv',
    'ta',
    'te',
    'tr',
    'uk',
    'ur',
    'vi',
    'zh-cn',
    'zh-tw',
];
function openContactUs() {
    const userLanguage = electron_1.app.getLocale();
    const language = SUPPORT_LANGUAGES.includes(userLanguage)
        ? userLanguage
        : 'en-us';
    // This URL needs a hardcoded language because the '?desktop' is dropped if the page
    //   auto-redirects to the proper URL
    electron_1.shell.openExternal(`https://support.signal.org/hc/${language}/requests/new?desktop`);
}
function openJoinTheBeta() {
    // If we omit the language, the site will detect the language and redirect
    electron_1.shell.openExternal('https://support.signal.org/hc/articles/360007318471');
}
function openReleaseNotes() {
    if (mainWindow && mainWindow.isVisible()) {
        mainWindow.webContents.send('show-release-notes');
        return;
    }
    electron_1.shell.openExternal(`https://github.com/signalapp/Signal-Desktop/releases/tag/v${electron_1.app.getVersion()}`);
}
function openSupportPage() {
    // If we omit the language, the site will detect the language and redirect
    electron_1.shell.openExternal('https://support.signal.org/hc/sections/360001602812');
}
function openForums() {
    electron_1.shell.openExternal('https://community.signalusers.org/');
}
function showKeyboardShortcuts() {
    if (mainWindow) {
        mainWindow.webContents.send('show-keyboard-shortcuts');
    }
}
function setupAsNewDevice() {
    if (mainWindow) {
        mainWindow.webContents.send('set-up-as-new-device');
    }
}
function setupAsStandalone() {
    if (mainWindow) {
        mainWindow.webContents.send('set-up-as-standalone');
    }
}
let screenShareWindow;
function showScreenShareWindow(sourceName) {
    if (screenShareWindow) {
        screenShareWindow.showInactive();
        return;
    }
    const width = 480;
    const display = electron_1.screen.getPrimaryDisplay();
    const options = {
        alwaysOnTop: true,
        autoHideMenuBar: true,
        backgroundColor: '#2e2e2e',
        darkTheme: true,
        frame: false,
        fullscreenable: false,
        height: 44,
        maximizable: false,
        minimizable: false,
        resizable: false,
        show: false,
        title: getLocale().i18n('screenShareWindow'),
        width,
        webPreferences: Object.assign(Object.assign({}, defaultWebPrefs), { nodeIntegration: false, nodeIntegrationInWorker: false, contextIsolation: true, preload: (0, path_1.join)(__dirname, '../ts/windows/screenShare/preload.js') }),
        x: Math.floor(display.size.width / 2) - width / 2,
        y: 24,
    };
    screenShareWindow = new electron_1.BrowserWindow(options);
    handleCommonWindowEvents(screenShareWindow);
    screenShareWindow.loadURL(prepareFileUrl([__dirname, '../screenShare.html']));
    screenShareWindow.on('closed', () => {
        screenShareWindow = undefined;
    });
    screenShareWindow.once('ready-to-show', () => {
        if (screenShareWindow) {
            screenShareWindow.showInactive();
            screenShareWindow.webContents.send('render-screen-sharing-controller', sourceName);
        }
    });
}
let aboutWindow;
function showAbout() {
    if (aboutWindow) {
        aboutWindow.show();
        return;
    }
    const options = {
        width: 500,
        height: 500,
        resizable: false,
        title: getLocale().i18n('aboutSignalDesktop'),
        autoHideMenuBar: true,
        backgroundColor: '#3a76f0',
        show: false,
        webPreferences: Object.assign(Object.assign({}, defaultWebPrefs), { nodeIntegration: false, nodeIntegrationInWorker: false, contextIsolation: true, preload: (0, path_1.join)(__dirname, '../ts/windows/about/preload.js'), nativeWindowOpen: true }),
    };
    aboutWindow = new electron_1.BrowserWindow(options);
    handleCommonWindowEvents(aboutWindow);
    aboutWindow.loadURL(prepareFileUrl([__dirname, '../about.html']));
    aboutWindow.on('closed', () => {
        aboutWindow = undefined;
    });
    aboutWindow.once('ready-to-show', () => {
        if (aboutWindow) {
            aboutWindow.show();
        }
    });
}
let settingsWindow;
function showSettingsWindow() {
    if (settingsWindow) {
        settingsWindow.show();
        return;
    }
    const options = {
        width: 700,
        height: 700,
        frame: true,
        resizable: false,
        title: getLocale().i18n('signalDesktopPreferences'),
        autoHideMenuBar: true,
        backgroundColor: '#3a76f0',
        show: false,
        webPreferences: Object.assign(Object.assign({}, defaultWebPrefs), { nodeIntegration: false, nodeIntegrationInWorker: false, contextIsolation: true, preload: (0, path_1.join)(__dirname, '../ts/windows/settings/preload.js'), nativeWindowOpen: true }),
    };
    settingsWindow = new electron_1.BrowserWindow(options);
    handleCommonWindowEvents(settingsWindow);
    settingsWindow.loadURL(prepareFileUrl([__dirname, '../settings.html']));
    settingsWindow.on('closed', () => {
        settingsWindow = undefined;
    });
    electron_1.ipcMain.once('settings-done-rendering', () => {
        if (!settingsWindow) {
            getLogger().warn('settings-done-rendering: no settingsWindow available!');
            return;
        }
        settingsWindow.show();
    });
}
async function getIsLinked() {
    try {
        const number = await sql.sqlCall('getItemById', ['number_id']);
        const password = await sql.sqlCall('getItemById', ['password']);
        return Boolean(number && password);
    }
    catch (e) {
        return false;
    }
}
let stickerCreatorWindow;
async function showStickerCreator() {
    if (!(await getIsLinked())) {
        const message = getLocale().i18n('StickerCreator--Authentication--error');
        electron_1.dialog.showMessageBox({
            type: 'warning',
            message,
        });
        return;
    }
    if (stickerCreatorWindow) {
        stickerCreatorWindow.show();
        return;
    }
    const { x = 0, y = 0 } = windowConfig || {};
    const options = {
        x: x + 100,
        y: y + 100,
        width: 800,
        minWidth: 800,
        height: 650,
        title: getLocale().i18n('signalDesktopStickerCreator'),
        autoHideMenuBar: true,
        backgroundColor: '#3a76f0',
        show: false,
        webPreferences: Object.assign(Object.assign({}, defaultWebPrefs), { nodeIntegration: false, nodeIntegrationInWorker: false, contextIsolation: false, preload: (0, path_1.join)(__dirname, '../sticker-creator/preload.js'), nativeWindowOpen: true, spellcheck: await getSpellCheckSetting() }),
    };
    stickerCreatorWindow = new electron_1.BrowserWindow(options);
    (0, spell_check_1.setup)(stickerCreatorWindow, getLocale().messages);
    handleCommonWindowEvents(stickerCreatorWindow);
    const appUrl = process.env.SIGNAL_ENABLE_HTTP
        ? prepareUrl(new URL('http://localhost:6380/sticker-creator/dist/index.html'))
        : prepareFileUrl([__dirname, '../sticker-creator/dist/index.html']);
    stickerCreatorWindow.loadURL(appUrl);
    stickerCreatorWindow.on('closed', () => {
        stickerCreatorWindow = undefined;
    });
    stickerCreatorWindow.once('ready-to-show', () => {
        if (!stickerCreatorWindow) {
            return;
        }
        stickerCreatorWindow.show();
        if (config_1.default.get('openDevTools')) {
            // Open the DevTools.
            stickerCreatorWindow.webContents.openDevTools();
        }
    });
}
let debugLogWindow;
async function showDebugLogWindow() {
    if (debugLogWindow) {
        debugLogWindow.show();
        return;
    }
    const theme = settingsChannel
        ? await settingsChannel.getSettingFromMainWindow('themeSetting')
        : undefined;
    const options = {
        width: 700,
        height: 500,
        resizable: false,
        title: getLocale().i18n('debugLog'),
        autoHideMenuBar: true,
        backgroundColor: '#3a76f0',
        show: false,
        webPreferences: Object.assign(Object.assign({}, defaultWebPrefs), { nodeIntegration: false, nodeIntegrationInWorker: false, contextIsolation: true, preload: (0, path_1.join)(__dirname, '../ts/windows/debuglog/preload.js'), nativeWindowOpen: true }),
        parent: mainWindow,
    };
    debugLogWindow = new electron_1.BrowserWindow(options);
    handleCommonWindowEvents(debugLogWindow);
    debugLogWindow.loadURL(prepareFileUrl([__dirname, '../debug_log.html'], { theme }));
    debugLogWindow.on('closed', () => {
        debugLogWindow = undefined;
    });
    debugLogWindow.once('ready-to-show', () => {
        if (debugLogWindow) {
            debugLogWindow.show();
        }
    });
}
let permissionsPopupWindow;
function showPermissionsPopupWindow(forCalling, forCamera) {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve, reject) => {
        if (permissionsPopupWindow) {
            permissionsPopupWindow.show();
            reject(new Error('Permission window already showing'));
            return;
        }
        if (!mainWindow) {
            reject(new Error('No main window'));
            return;
        }
        const theme = settingsChannel
            ? await settingsChannel.getSettingFromMainWindow('themeSetting')
            : undefined;
        const size = mainWindow.getSize();
        const options = {
            width: Math.min(400, size[0]),
            height: Math.min(150, size[1]),
            resizable: false,
            title: getLocale().i18n('allowAccess'),
            autoHideMenuBar: true,
            backgroundColor: '#3a76f0',
            show: false,
            modal: true,
            webPreferences: Object.assign(Object.assign({}, defaultWebPrefs), { nodeIntegration: false, nodeIntegrationInWorker: false, contextIsolation: true, preload: (0, path_1.join)(__dirname, '../ts/windows/permissions/preload.js'), nativeWindowOpen: true }),
            parent: mainWindow,
        };
        permissionsPopupWindow = new electron_1.BrowserWindow(options);
        handleCommonWindowEvents(permissionsPopupWindow);
        permissionsPopupWindow.loadURL(prepareFileUrl([__dirname, '../permissions_popup.html'], {
            theme,
            forCalling,
            forCamera,
        }));
        permissionsPopupWindow.on('closed', () => {
            removeDarkOverlay();
            permissionsPopupWindow = undefined;
            resolve();
        });
        permissionsPopupWindow.once('ready-to-show', () => {
            if (permissionsPopupWindow) {
                addDarkOverlay();
                permissionsPopupWindow.show();
            }
        });
    });
}
async function initializeSQL(userDataPath) {
    let key;
    const keyFromConfig = userConfig.get('key');
    if (typeof keyFromConfig === 'string') {
        key = keyFromConfig;
    }
    else if (keyFromConfig) {
        getLogger().warn("initializeSQL: got key from config, but it wasn't a string");
    }
    if (!key) {
        getLogger().info('key/initialize: Generating new encryption key, since we did not find it on disk');
        // https://www.zetetic.net/sqlcipher/sqlcipher-api/#key
        key = (0, crypto_1.randomBytes)(32).toString('hex');
        userConfig.set('key', key);
    }
    sqlInitTimeStart = Date.now();
    try {
        // This should be the first awaited call in this function, otherwise
        // `sql.sqlCall` will throw an uninitialized error instead of waiting for
        // init to finish.
        await sql.initialize({
            configDir: userDataPath,
            key,
            logger: getLogger(),
        });
    }
    catch (error) {
        if (error instanceof Error) {
            return { ok: false, error };
        }
        return {
            ok: false,
            error: new Error(`initializeSQL: Caught a non-error '${error}'`),
        };
    }
    finally {
        sqlInitTimeEnd = Date.now();
    }
    return { ok: true, error: undefined };
}
const onDatabaseError = async (error) => {
    // Prevent window from re-opening
    ready = false;
    if (mainWindow) {
        settingsChannel === null || settingsChannel === void 0 ? void 0 : settingsChannel.invokeCallbackInMainWindow('closeDB', []);
        mainWindow.close();
    }
    mainWindow = undefined;
    const buttonIndex = electron_1.dialog.showMessageBoxSync({
        buttons: [
            getLocale().i18n('copyErrorAndQuit'),
            getLocale().i18n('deleteAndRestart'),
        ],
        defaultId: 0,
        detail: (0, privacy_1.redactAll)(error),
        message: getLocale().i18n('databaseError'),
        noLink: true,
        type: 'error',
    });
    if (buttonIndex === 0) {
        electron_1.clipboard.writeText(`Database startup error:\n\n${(0, privacy_1.redactAll)(error)}`);
    }
    else {
        await sql.removeDB();
        userConfig.remove();
        electron_1.app.relaunch();
    }
    electron_1.app.exit(1);
};
const runSQLCorruptionHandler = async () => {
    // This is a glorified event handler. Normally, this promise never resolves,
    // but if there is a corruption error triggered by any query that we run
    // against the database - the promise will resolve and we will call
    // `onDatabaseError`.
    const error = await sql.whenCorrupted();
    getLogger().error('Detected sql corruption in main process. ' +
        `Restarting the application immediately. Error: ${error.message}`);
    await onDatabaseError(error.stack || error.message);
};
runSQLCorruptionHandler();
let sqlInitPromise;
electron_1.ipcMain.on('database-error', (_event, error) => {
    onDatabaseError(error);
});
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
let ready = false;
electron_1.app.on('ready', async () => {
    const userDataPath = await getRealPath(electron_1.app.getPath('userData'));
    logger = await logging.initialize(getMainWindow);
    sqlInitPromise = initializeSQL(userDataPath);
    const startTime = Date.now();
    settingsChannel = new settingsChannel_1.SettingsChannel();
    settingsChannel.install();
    // We use this event only a single time to log the startup time of the app
    // from when it's first ready until the loading screen disappears.
    electron_1.ipcMain.once('signal-app-loaded', (event, info) => {
        const { preloadTime, connectTime, processedCount } = info;
        const loadTime = Date.now() - startTime;
        const sqlInitTime = sqlInitTimeEnd - sqlInitTimeStart;
        const messageTime = loadTime - preloadTime - connectTime;
        const messagesPerSec = (processedCount * 1000) / messageTime;
        const innerLogger = getLogger();
        innerLogger.info('App loaded - time:', loadTime);
        innerLogger.info('SQL init - time:', sqlInitTime);
        innerLogger.info('Preload - time:', preloadTime);
        innerLogger.info('WebSocket connect - time:', connectTime);
        innerLogger.info('Processed count:', processedCount);
        innerLogger.info('Messages per second:', messagesPerSec);
        event.sender.send('ci:event', 'app-loaded', {
            loadTime,
            sqlInitTime,
            preloadTime,
            connectTime,
            processedCount,
            messagesPerSec,
        });
    });
    const installPath = await getRealPath(electron_1.app.getAppPath());
    (0, privacy_1.addSensitivePath)(userDataPath);
    if ((0, environment_1.getEnvironment)() !== environment_1.Environment.Test &&
        (0, environment_1.getEnvironment)() !== environment_1.Environment.TestLib) {
        (0, protocol_filter_1.installFileHandler)({
            protocol: electron_1.protocol,
            userDataPath,
            installPath,
            isWindows: OS.isWindows(),
        });
    }
    (0, protocol_filter_1.installWebHandler)({
        enableHttp: Boolean(process.env.SIGNAL_ENABLE_HTTP),
        protocol: electron_1.protocol,
    });
    logger.info('app ready');
    logger.info(`starting version ${package_json_1.default.version}`);
    // This logging helps us debug user reports about broken devices.
    {
        let getMediaAccessStatus;
        // This function is not supported on Linux, so we have a fallback.
        if (electron_1.systemPreferences.getMediaAccessStatus) {
            getMediaAccessStatus =
                electron_1.systemPreferences.getMediaAccessStatus.bind(electron_1.systemPreferences);
        }
        else {
            getMediaAccessStatus = lodash_1.noop;
        }
        logger.info('media access status', getMediaAccessStatus('microphone'), getMediaAccessStatus('camera'));
    }
    if (!locale) {
        const appLocale = (0, environment_1.getEnvironment)() === environment_1.Environment.Test ? 'en' : electron_1.app.getLocale();
        locale = (0, locale_1.load)({ appLocale, logger });
    }
    GlobalErrors.updateLocale(locale.messages);
    // If the sql initialization takes more than three seconds to complete, we
    // want to notify the user that things are happening
    const timeout = new Promise(resolve => setTimeout(resolve, 3000, 'timeout'));
    // eslint-disable-next-line more/no-then
    Promise.race([sqlInitPromise, timeout]).then(maybeTimeout => {
        if (maybeTimeout !== 'timeout') {
            return;
        }
        getLogger().info('sql.initialize is taking more than three seconds; showing loading dialog');
        loadingWindow = new electron_1.BrowserWindow({
            show: false,
            width: 300,
            height: 265,
            resizable: false,
            frame: false,
            backgroundColor: '#3a76f0',
            webPreferences: Object.assign(Object.assign({}, defaultWebPrefs), { nodeIntegration: false, contextIsolation: true, preload: (0, path_1.join)(__dirname, '../ts/windows/loading/preload.js') }),
            icon: windowIcon,
        });
        loadingWindow.once('ready-to-show', async () => {
            if (!loadingWindow) {
                return;
            }
            loadingWindow.show();
            // Wait for sql initialization to complete, but ignore errors
            await sqlInitPromise;
            loadingWindow.destroy();
            loadingWindow = undefined;
        });
        loadingWindow.loadURL(prepareFileUrl([__dirname, '../loading.html']));
    });
    try {
        await attachments.clearTempPath(userDataPath);
    }
    catch (err) {
        logger.error('main/ready: Error deleting temp dir:', err && err.stack ? err.stack : err);
    }
    // Initialize IPC channels before creating the window
    attachmentChannel.initialize({
        configDir: userDataPath,
        cleanupOrphanedAttachments,
    });
    sqlChannels.initialize(sql);
    powerChannel_1.PowerChannel.initialize({
        send(event) {
            if (!mainWindow) {
                return;
            }
            mainWindow.webContents.send(event);
        },
    });
    // Run window preloading in parallel with database initialization.
    await createWindow();
    const { error: sqlError } = await sqlInitPromise;
    if (sqlError) {
        getLogger().error('sql.initialize was unsuccessful; returning early');
        await onDatabaseError(sqlError.stack || sqlError.message);
        return;
    }
    // eslint-disable-next-line more/no-then
    appStartInitialSpellcheckSetting = await getSpellCheckSetting();
    try {
        const IDB_KEY = 'indexeddb-delete-needed';
        const item = await sql.sqlCall('getItemById', [IDB_KEY]);
        if (item && item.value) {
            await sql.sqlCall('removeIndexedDBFiles', []);
            await sql.sqlCall('removeItemById', [IDB_KEY]);
        }
    }
    catch (err) {
        getLogger().error('(ready event handler) error deleting IndexedDB:', err && err.stack ? err.stack : err);
    }
    async function cleanupOrphanedAttachments() {
        const allAttachments = await attachments.getAllAttachments(userDataPath);
        const orphanedAttachments = await sql.sqlCall('removeKnownAttachments', [
            allAttachments,
        ]);
        await attachments.deleteAll({
            userDataPath,
            attachments: orphanedAttachments,
        });
        await attachments.deleteAllBadges({
            userDataPath,
            pathsToKeep: await sql.sqlCall('getAllBadgeImageFileLocalPaths', []),
        });
        const allStickers = await attachments.getAllStickers(userDataPath);
        const orphanedStickers = await sql.sqlCall('removeKnownStickers', [
            allStickers,
        ]);
        await attachments.deleteAllStickers({
            userDataPath,
            stickers: orphanedStickers,
        });
        const allDraftAttachments = await attachments.getAllDraftAttachments(userDataPath);
        const orphanedDraftAttachments = await sql.sqlCall('removeKnownDraftAttachments', [allDraftAttachments]);
        await attachments.deleteAllDraftAttachments({
            userDataPath,
            attachments: orphanedDraftAttachments,
        });
    }
    ready = true;
    setupMenu();
    systemTrayService = new SystemTrayService_1.SystemTrayService({ messages: locale.messages });
    systemTrayService.setMainWindow(mainWindow);
    systemTrayService.setEnabled((0, SystemTraySetting_1.shouldMinimizeToSystemTray)(await systemTraySettingCache.get()));
    ensureFilePermissions([
        'config.json',
        'sql/db.sqlite',
        'sql/db.sqlite-wal',
        'sql/db.sqlite-shm',
    ]);
});
function setupMenu(options) {
    const { platform } = process;
    const menuOptions = Object.assign({ 
        // options
        development, devTools: defaultWebPrefs.devTools, includeSetup: false, isProduction: (0, version_1.isProduction)(electron_1.app.getVersion()), platform,
        // actions
        forceUpdate,
        openContactUs,
        openForums,
        openJoinTheBeta,
        openReleaseNotes,
        openSupportPage,
        setupAsNewDevice,
        setupAsStandalone,
        showAbout, showDebugLog: showDebugLogWindow, showKeyboardShortcuts, showSettings: showSettingsWindow, showStickerCreator,
        showWindow }, options);
    const template = (0, menu_1.createTemplate)(menuOptions, getLocale().messages);
    const menu = electron_1.Menu.buildFromTemplate(template);
    electron_1.Menu.setApplicationMenu(menu);
}
async function requestShutdown() {
    if (!mainWindow || !mainWindow.webContents) {
        return;
    }
    getLogger().info('requestShutdown: Requesting close of mainWindow...');
    const request = new Promise((resolve, reject) => {
        let timeout;
        if (!mainWindow) {
            resolve();
            return;
        }
        electron_1.ipcMain.once('now-ready-for-shutdown', (_event, error) => {
            getLogger().info('requestShutdown: Response received');
            if (error) {
                return reject(error);
            }
            if (timeout) {
                clearTimeout(timeout);
            }
            resolve();
        });
        mainWindow.webContents.send('get-ready-for-shutdown');
        // We'll wait two minutes, then force the app to go down. This can happen if someone
        //   exits the app before we've set everything up in preload() (so the browser isn't
        //   yet listening for these events), or if there are a whole lot of stacked-up tasks.
        // Note: two minutes is also our timeout for SQL tasks in data.js in the browser.
        timeout = setTimeout(() => {
            getLogger().error('requestShutdown: Response never received; forcing shutdown.');
            resolve();
        }, 2 * 60 * 1000);
    });
    try {
        await request;
    }
    catch (error) {
        getLogger().error('requestShutdown error:', error && error.stack ? error.stack : error);
    }
}
electron_1.app.on('before-quit', () => {
    getLogger().info('before-quit event', {
        readyForShutdown: windowState.readyForShutdown(),
        shouldQuit: windowState.shouldQuit(),
    });
    windowState.markShouldQuit();
});
// Quit when all windows are closed.
electron_1.app.on('window-all-closed', () => {
    getLogger().info('main process handling window-all-closed');
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    const shouldAutoClose = !OS.isMacOS() || (0, environment_1.isTestEnvironment)((0, environment_1.getEnvironment)());
    // Only automatically quit if the main window has been created
    // This is necessary because `window-all-closed` can be triggered by the
    // "optimizing application" window closing
    if (shouldAutoClose && mainWindowCreated) {
        electron_1.app.quit();
    }
});
electron_1.app.on('activate', () => {
    if (!ready) {
        return;
    }
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow) {
        mainWindow.show();
    }
    else {
        createWindow();
    }
});
// Defense in depth. We never intend to open webviews or windows. Prevent it completely.
electron_1.app.on('web-contents-created', (_createEvent, contents) => {
    contents.on('will-attach-webview', attachEvent => {
        attachEvent.preventDefault();
    });
    contents.on('new-window', newEvent => {
        newEvent.preventDefault();
    });
});
electron_1.app.setAsDefaultProtocolClient('sgnl');
electron_1.app.setAsDefaultProtocolClient('signalcaptcha');
electron_1.app.on('will-finish-launching', () => {
    // open-url must be set from within will-finish-launching for macOS
    // https://stackoverflow.com/a/43949291
    electron_1.app.on('open-url', (event, incomingHref) => {
        event.preventDefault();
        if ((0, sgnlHref_1.isCaptchaHref)(incomingHref, getLogger())) {
            const { captcha } = (0, sgnlHref_1.parseCaptchaHref)(incomingHref, getLogger());
            challengeHandler.handleCaptcha(captcha);
            return;
        }
        handleSgnlHref(incomingHref);
    });
});
electron_1.ipcMain.on('set-badge-count', (_event, count) => {
    electron_1.app.badgeCount = count;
});
electron_1.ipcMain.on('remove-setup-menu-items', () => {
    setupMenu();
});
electron_1.ipcMain.on('add-setup-menu-items', () => {
    setupMenu({
        includeSetup: true,
    });
});
electron_1.ipcMain.on('draw-attention', () => {
    if (!mainWindow) {
        return;
    }
    if (OS.isWindows() || OS.isLinux()) {
        mainWindow.flashFrame(true);
    }
});
electron_1.ipcMain.on('restart', () => {
    getLogger().info('Relaunching application');
    electron_1.app.relaunch();
    electron_1.app.quit();
});
electron_1.ipcMain.on('shutdown', () => {
    electron_1.app.quit();
});
electron_1.ipcMain.on('set-auto-hide-menu-bar', (_event, autoHide) => {
    if (mainWindow) {
        mainWindow.autoHideMenuBar = autoHide;
    }
});
electron_1.ipcMain.on('set-menu-bar-visibility', (_event, visibility) => {
    if (mainWindow) {
        mainWindow.setMenuBarVisibility(visibility);
    }
});
electron_1.ipcMain.on('update-system-tray-setting', (_event, rawSystemTraySetting /* : Readonly<unknown> */) => {
    const systemTraySetting = (0, SystemTraySetting_1.parseSystemTraySetting)(rawSystemTraySetting);
    systemTraySettingCache.set(systemTraySetting);
    if (systemTrayService) {
        const isEnabled = (0, SystemTraySetting_1.shouldMinimizeToSystemTray)(systemTraySetting);
        systemTrayService.setEnabled(isEnabled);
    }
});
electron_1.ipcMain.on('close-about', () => {
    if (aboutWindow) {
        aboutWindow.close();
    }
});
electron_1.ipcMain.on('close-screen-share-controller', () => {
    if (screenShareWindow) {
        screenShareWindow.close();
    }
});
electron_1.ipcMain.on('stop-screen-share', () => {
    if (mainWindow) {
        mainWindow.webContents.send('stop-screen-share');
    }
});
electron_1.ipcMain.on('show-screen-share', (_event, sourceName) => {
    showScreenShareWindow(sourceName);
});
electron_1.ipcMain.on('update-tray-icon', (_event, unreadCount) => {
    if (systemTrayService) {
        systemTrayService.setUnreadCount(unreadCount);
    }
});
// Debug Log-related IPC calls
electron_1.ipcMain.on('show-debug-log', showDebugLogWindow);
electron_1.ipcMain.on('close-debug-log', () => {
    if (debugLogWindow) {
        debugLogWindow.close();
    }
});
electron_1.ipcMain.on('show-debug-log-save-dialog', async (_event, logText) => {
    const { filePath } = await electron_1.dialog.showSaveDialog({
        defaultPath: 'debuglog.txt',
    });
    if (filePath) {
        await (0, fs_extra_1.writeFile)(filePath, logText);
    }
});
// Permissions Popup-related IPC calls
electron_1.ipcMain.handle('show-permissions-popup', async () => {
    try {
        await showPermissionsPopupWindow(false, false);
    }
    catch (error) {
        getLogger().error('show-permissions-popup error:', error && error.stack ? error.stack : error);
    }
});
electron_1.ipcMain.handle('show-calling-permissions-popup', async (_event, forCamera) => {
    try {
        await showPermissionsPopupWindow(true, forCamera);
    }
    catch (error) {
        getLogger().error('show-calling-permissions-popup error:', error && error.stack ? error.stack : error);
    }
});
electron_1.ipcMain.on('close-permissions-popup', () => {
    if (permissionsPopupWindow) {
        permissionsPopupWindow.close();
    }
});
// Settings-related IPC calls
function addDarkOverlay() {
    if (mainWindow && mainWindow.webContents) {
        mainWindow.webContents.send('add-dark-overlay');
    }
}
function removeDarkOverlay() {
    if (mainWindow && mainWindow.webContents) {
        mainWindow.webContents.send('remove-dark-overlay');
    }
}
electron_1.ipcMain.on('show-settings', showSettingsWindow);
electron_1.ipcMain.on('close-settings', () => {
    if (settingsWindow) {
        settingsWindow.close();
    }
});
electron_1.ipcMain.on('delete-all-data', () => {
    if (settingsWindow) {
        settingsWindow.close();
    }
    if (mainWindow && mainWindow.webContents) {
        mainWindow.webContents.send('delete-all-data');
    }
});
electron_1.ipcMain.on('get-built-in-images', async () => {
    if (!mainWindow) {
        getLogger().warn('ipc/get-built-in-images: No mainWindow!');
        return;
    }
    try {
        const images = await attachments.getBuiltInImages();
        mainWindow.webContents.send('get-success-built-in-images', null, images);
    }
    catch (error) {
        if (mainWindow && mainWindow.webContents) {
            mainWindow.webContents.send('get-success-built-in-images', error.message);
        }
        else {
            getLogger().error('Error handling get-built-in-images:', error.stack);
        }
    }
});
// Ingested in preload.js via a sendSync call
electron_1.ipcMain.on('locale-data', event => {
    // eslint-disable-next-line no-param-reassign
    event.returnValue = getLocale().messages;
});
electron_1.ipcMain.on('user-config-key', event => {
    // eslint-disable-next-line no-param-reassign
    event.returnValue = userConfig.get('key');
});
electron_1.ipcMain.on('get-user-data-path', event => {
    // eslint-disable-next-line no-param-reassign
    event.returnValue = electron_1.app.getPath('userData');
});
// Refresh the settings window whenever preferences change
electron_1.ipcMain.on('preferences-changed', () => {
    for (const window of activeWindows) {
        if (window.webContents) {
            window.webContents.send('preferences-changed');
        }
    }
});
function getIncomingHref(argv) {
    return argv.find(arg => (0, sgnlHref_1.isSgnlHref)(arg, getLogger()));
}
function getIncomingCaptchaHref(argv) {
    return argv.find(arg => (0, sgnlHref_1.isCaptchaHref)(arg, getLogger()));
}
function handleSgnlHref(incomingHref) {
    let command;
    let args;
    let hash;
    if ((0, sgnlHref_1.isSgnlHref)(incomingHref, getLogger())) {
        ({ command, args, hash } = (0, sgnlHref_1.parseSgnlHref)(incomingHref, getLogger()));
    }
    else if ((0, sgnlHref_1.isSignalHttpsLink)(incomingHref, getLogger())) {
        ({ command, args, hash } = (0, sgnlHref_1.parseSignalHttpsLink)(incomingHref, getLogger()));
    }
    if (mainWindow && mainWindow.webContents) {
        if (command === 'addstickers') {
            getLogger().info('Opening sticker pack from sgnl protocol link');
            const packId = args === null || args === void 0 ? void 0 : args.get('pack_id');
            const packKeyHex = args === null || args === void 0 ? void 0 : args.get('pack_key');
            const packKey = packKeyHex
                ? Buffer.from(packKeyHex, 'hex').toString('base64')
                : '';
            mainWindow.webContents.send('show-sticker-pack', { packId, packKey });
        }
        else if (command === 'signal.group' && hash) {
            getLogger().info('Showing group from sgnl protocol link');
            mainWindow.webContents.send('show-group-via-link', { hash });
        }
        else if (command === 'signal.me' && hash) {
            getLogger().info('Showing conversation from sgnl protocol link');
            mainWindow.webContents.send('show-conversation-via-signal.me', { hash });
        }
        else {
            getLogger().info('Showing warning that we cannot process link');
            mainWindow.webContents.send('unknown-sgnl-link');
        }
    }
    else {
        getLogger().error('Unhandled sgnl link');
    }
}
electron_1.ipcMain.on('install-sticker-pack', (_event, packId, packKeyHex) => {
    const packKey = Buffer.from(packKeyHex, 'hex').toString('base64');
    if (mainWindow) {
        mainWindow.webContents.send('install-sticker-pack', { packId, packKey });
    }
});
electron_1.ipcMain.on('ensure-file-permissions', async (event) => {
    await ensureFilePermissions();
    event.reply('ensure-file-permissions-done');
});
/**
 * Ensure files in the user's data directory have the proper permissions.
 * Optionally takes an array of file paths to exclusively affect.
 *
 * @param {string[]} [onlyFiles] - Only ensure permissions on these given files
 */
async function ensureFilePermissions(onlyFiles) {
    getLogger().info('Begin ensuring permissions');
    const start = Date.now();
    const userDataPath = await getRealPath(electron_1.app.getPath('userData'));
    // fast-glob uses `/` for all platforms
    const userDataGlob = (0, normalize_path_1.default)((0, path_1.join)(userDataPath, '**', '*'));
    // Determine files to touch
    const files = onlyFiles
        ? onlyFiles.map(f => (0, path_1.join)(userDataPath, f))
        : await (0, fast_glob_1.default)(userDataGlob, {
            markDirectories: true,
            onlyFiles: false,
            ignore: ['**/Singleton*'],
        });
    getLogger().info(`Ensuring file permissions for ${files.length} files`);
    // Touch each file in a queue
    const q = new p_queue_1.default({ concurrency: 5, timeout: 1000 * 60 * 2 });
    q.addAll(files.map(f => async () => {
        const isDir = f.endsWith('/');
        try {
            await (0, fs_extra_1.chmod)((0, path_1.normalize)(f), isDir ? 0o700 : 0o600);
        }
        catch (error) {
            getLogger().error('ensureFilePermissions: Error from chmod', error.message);
        }
    }));
    await q.onEmpty();
    getLogger().info(`Finish ensuring permissions in ${Date.now() - start}ms`);
}
electron_1.ipcMain.handle('get-auto-launch', async () => {
    return electron_1.app.getLoginItemSettings().openAtLogin;
});
electron_1.ipcMain.handle('set-auto-launch', async (_event, value) => {
    electron_1.app.setLoginItemSettings({ openAtLogin: Boolean(value) });
});
electron_1.ipcMain.on('show-message-box', (_event, { type, message }) => {
    electron_1.dialog.showMessageBox({ type, message });
});
electron_1.ipcMain.on('show-item-in-folder', (_event, folder) => {
    electron_1.shell.showItemInFolder(folder);
});
electron_1.ipcMain.handle('show-save-dialog', async (_event, { defaultPath }) => {
    if (!mainWindow) {
        getLogger().warn('show-save-dialog: no main window');
        return { canceled: true };
    }
    return electron_1.dialog.showSaveDialog(mainWindow, {
        defaultPath,
    });
});
