export const base: string;
export const bottom: string;
export const button: string;
export const buttonPrimary: string;
export const text: string;
export const title: string;
