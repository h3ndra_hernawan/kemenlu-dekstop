export const base: string;
export const checkbox: string;
export const checkboxChecked: string;
export const input: string;
export const label: string;
