export const base: string;
export const h1: string;
export const h2: string;
export const heading: string;
export const secondary: string;
export const text: string;
export const textCenter: string;
