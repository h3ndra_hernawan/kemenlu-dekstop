export const button: string;
export const empty: string;
export const footer: string;
export const footerRight: string;
export const main: string;
export const noMessage: string;
export const padded: string;
export const toaster: string;
