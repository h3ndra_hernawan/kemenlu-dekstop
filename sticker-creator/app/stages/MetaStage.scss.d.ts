export const coverContainer: string;
export const coverFrame: string;
export const coverFrameActive: string;
export const coverImage: string;
export const label: string;
export const main: string;
export const row: string;
